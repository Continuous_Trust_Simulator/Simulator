## Experiment Setting ========

Experiment 13

## Variable Attack Factors; Manual Vs Automatic Defence Optimisation; High & Low Nodes variations

Fixed 0

Factors 10

#Low -> Manual; High -> Automatic
Factor_1_nested 5

Factor_1_1 DeployST
Factor_1_1_low 1
Factor_1_1_high 0
Factor_1_1_fixed 1

Factor_1_2 PcrThreshold
Factor_1_2_low 3
Factor_1_2_high 2
Factor_1_2_fixed 2

Factor_1_3 PcrWeakThreshold
Factor_1_3_low 3
Factor_1_3_high 7
Factor_1_3_fixed 5

Factor_1_4 GT_MaxSize
Factor_1_4_low 5
Factor_1_4_high 7
Factor_1_4_fixed 1

Factor_1_5 SuspiciousSPBanningRank
Factor_1_5_low 15
Factor_1_5_high 5
Factor_1_5_fixed 15

#INI Normal Nodes
Factor_2_nested 3

Factor_2_1 INI_Users
Factor_2_1_low 50
Factor_2_1_high 130
Factor_2_1_fixed 50

Factor_2_2 INI_PSPs
Factor_2_2_low 8
Factor_2_2_high 20
Factor_2_2_fixed 8

Factor_2_3 INI_SPs
Factor_2_3_low 16
Factor_2_3_high 40
Factor_2_3_fixed 16

#GR Normal Nodes
Factor_3_nested 3

Factor_3_1 UsersGR
Factor_3_1_low 0.05
Factor_3_1_high 0.2
Factor_3_1_fixed 0.05

Factor_3_2 SPsGR
Factor_3_2_low 0.05
Factor_3_2_high 0.1
Factor_3_2_fixed 0.05

Factor_3_3 PSPsGR
Factor_3_3_low 0.05
Factor_3_3_high 0.1
Factor_3_3_fixed 0.05

#INI Malicious Nodes
Factor_4_nested 4

Factor_4_1 INI_MPSPs
Factor_4_1_low 4
Factor_4_1_high 10
Factor_4_1_fixed 4

Factor_4_2 INI_MSPs
Factor_4_2_low 8
Factor_4_2_high 20
Factor_4_2_fixed 8

Factor_4_3 INI_ME_MPSPs
Factor_4_3_low 5
Factor_4_3_high 7
Factor_4_3_fixed 5

Factor_4_4 INI_ME_MSPs
Factor_4_4_low 8
Factor_4_4_high 12
Factor_4_4_fixed 8

#GR Malicious
Factor_5_nested 4

Factor_5_1 MPSPsGR
Factor_5_1_low 0.05
Factor_5_1_high 0.1
Factor_5_1_fixed 0.05

Factor_5_2 ME_MPSPsGR
Factor_5_2_low 0.05
Factor_5_2_high 0.1
Factor_5_2_fixed 0.05

Factor_5_3 MSPsGR
Factor_5_3_low 0.05
Factor_5_3_high 0.1
Factor_5_3_fixed 0.05

Factor_5_4 ME_MSPsGR
Factor_5_4_low 0.05
Factor_5_4_high 0.1
Factor_5_4_fixed 0.05


Factor_6_nested 1

Factor_6_1 SPAM_Delay_Period
Factor_6_1_low 5
Factor_6_1_high 50
Factor_6_1_fixed 5

Factor_7_nested 1

Factor_7_1 SPAM_DropUser_ME_Rate
Factor_7_1_low 20
Factor_7_1_high 80
Factor_7_1_fixed 20

Factor_8_nested 1

Factor_8_1 SPAM_DropCredential_ME_Rate
Factor_8_1_low 20
Factor_8_1_high 80
Factor_8_1_fixed 20

Factor_9_nested 1

Factor_9_1 ME_N_MIN_MPSPs_toSPAM_User
Factor_9_1_low 1
Factor_9_1_high 5
Factor_9_1_fixed 5

Factor_10_nested 1

Factor_10_1 ME_N_MIN_MPSPs_toSPAM_Credential
Factor_10_1_low 1
Factor_10_1_high 5
Factor_10_1_fixed 1


#-----------------------------------
#Network settings

# This value is the number of running times for this experiment setting
replicas 3840

# This value is initial Network size = 3 + INI_Users + INI_PSPs + INI_SPs + INI_MPSPs + INI_MSPs + INI_ME + INI_ME (INI_ME_MPSPs + INI_ME_MSPs) 
networkSize 99

# This value is total number of simulation cycles
cycles 505

# This value is initial number of Users.
INI_Users 50

# This value is initial Users growth rate (could be affected by overall Network Trust).  
UsersGR 0.2

# This value is initial number of PSPs.
INI_PSPs 8

# This value is initial PSPs growth rate (could be affected by overall Network Trust).  
PSPsGR 0.1

# This value is initial number of SPs.
INI_SPs 16

# This value is initial SPs growth rate (could be affected by overall Network Trust).  
SPsGR 0.1

# This value is initial number of MPSPs.
INI_MPSPs 4

# This value is initial MPSPs growth rate.  
MPSPsGR 0.1

# This value is initial number of MSPs.
INI_MSPs 8

# This value is initial MSPs growth rate.  
MSPsGR 0.1

#This value is initial number of MEs.
INI_ME 2

#This value is initial ME growth rate.
MEGR 0

# This value is initial number of MPSPs for ME.
INI_ME_MPSPs 5

# This value is initial number of MSPs for ME.
INI_ME_MSPs 8

# This value is initial ME MPSPs growth rate.
ME_MPSPsGR 0.1

# This value is initial ME MSPs growth rate.
ME_MSPsGR 0.1

# This value is the maximum number of MPSPs for any ME
ME_MAX_MPSPs 15

# This value is the maximum number of MSPs for any ME
ME_MAX_MSPs 50

#-----------------------------------
#User settings

#This value is the initial size of the credentials array for each User
CredentialsArraySize 70

#This value is % of a User generating a new Service Request at each cycle
GenerateNewServiceRequestRate 50

#This value is % of a User generating a new Credential at each cycle (Affected by Trust on Network)
GenerateNewCredentialRate 10

#This value is % of a User generating a new strict Credential at each cycle
StrictCredentialProb 10

#This value is to indicate the initial type of the credential a user would use when joining the Network
# 0 = strict, 1 = share with high rank, 2 = share with any
IniCredentialType 0

# This value is % of initial Users' trust in the Network
TrustInNetwork 50

# This value is binary to indicate whether initial trust in the Network for new Users should be dynamic
TrustInNetworkDynamic 1

# This value is the % of times a User would ignore to report a SPAM
IgnoranceRate 20

# This value is the % of the Users' trust on the Network increase after each cycle without a report
TrustIncreaseRate 1

# This value is the % of the Users' trust on the Network decrease after each cycle * number of 
# unresolved reported cases
TrustDecreaseRate 1

# This value is the period, in cycles, of which the Users' trust on the Network decrease 
# after reporting a SPAM. This period would stop whenever the case is resolved.
TrustDecreasePeriodPostSpam 25

#This value is a boolean indicating whether a User should stop using a credential after it get SPAMED
#or wait until it get a guilty report from auditor
StopUsingCredentialAfterSpam 0

#This value is % of Trust Drop after reporting a SPAM
TrustDropPostSpam 10

#This value is % of Trust Increase after resolving a reported SPAM
TrustIncreasePostSpamResolved 5

#-----------------------------------
#Auditor settings

# This value is the frequency, in cycles, for updating the rankings of the IDP
UpdateIDPRankingsFreq 1

# This value is % weight of the whole TGT rankings (TGTg, Colluding, Weak Colluding) in TGR calculations
TGTWholeWeight 80

# This value is % weight of TGT ranking in TGR calculations
TGTWeight 20

# This value is % weight of TGT Colluding (based on AGents reports for specific Credentials) ranking in TGR calculations
TGTColludingWeight 30

# This value is % weight of TGT Weak Colluding (Based on User reports for PSPs dealt with all Credentials) ranking in TGR calculations
TGTWeakColludingWeight 50

# This value is % weight of TLRavg ranking in TGR calculations	
TLRavgWeight 8

# This value is % weight of TST ranking in TGR calculations	
TSTWeight 12

# This value is the Rank % for selecting an SP as guilty for a given case
SuspiciousSPRank 30

# This value is % of Rank similarity with the lowest ranked guilty SP for 
#selecting another SP as another potential guilty for a given case
SuspiciousSPRange 5

# This value is Rank % for baning an SP from the Network
SuspiciousSPBanningRank 15

# This value is minimum number of Users who dealt with a PSP before applying the baning rules on it
SufficientTLRus_PSP 10

# This value is minimum number of Users who dealt with an SP before applying the baning rules on it
SufficientTLRus_SP 2

# This value is % rank of any SP after it agrees to install DGU	
postDGUinstallationRank 100
	
# This value is % rank of any SP after it agrees to enable strict DGU		
postStrictDGUEnableRank 100

# This value is to indicate whether DGU measure should be deployed in the simulation session
DeployDGU 1

#ST Agents Settings

# This value is to indicate whether ST agents should be deployed in the simulation session
DeployST 0

# This value is idle time, in cycles, for an ST agent before interacting
# with the associated SP
STidleTime 1

# This value is max lifetime, in cycles, for an ST agent before being killed and maybe 
# replaced
STmaxLifeTime 80

# This value is ST status post SPAM: 1 = active, 2 = killed, 3 = killed and stop behaving as normal user 
STagentStatusPostSpam 3

# This value is number of credential the ST can use to fool colluding SPs looking for
# normal users dealing with many SPs
STunimportantCredentialsPool 5

# This value is to indicate whether ST agents should report SPAM related to unimportant credentials
STreportSpam 1

# This value is % of top TLRavg SPs to be selected for ST testing
TopTLRavgPCT_ST 20

# This value is % of bottom TLRavg SPs to be selected for ST testing
BottomTLRavgPCT_ST 20

# This value is % of top SPR records to be selected for ST testing
PSL_PCT 20

# This value is binary indicating whether PSL ST selector to be deployed
PSL_STselector 0

# This value is binary indicating whether suspicious Nodes Dir would be used as ST selector
SuspiciousNodes_STselector 0

# This value is binary indicating whether Top TLRavg rankings would be used as ST selector
TopTLRavg_STselector 1

# This value is binary indicating whether Bottom TLRavg rankings would be used as ST selector
BottomTLRavg_STselector 0

# This value is binary indicating whether PIIL ST selector to be deployed
IIR_STselector 0

#GT Agents Setting

# This value is to indicate whether GT agents should be deployed in the simulation session
DeployGT 1

# This value is to indicate whether TGT values should be averaged or only the MIN values to be considered
GT_avgTGT 0

# This value is idle time, in cycles, for a GT agent before interacting
# with the associated SP
GTidleTime 1

# This value is max lifetime, in cycles, for a GT agent before being killed and maybe 
# replaced
GTmaxLifeTime 80

# This value is GT status post SPAM: 1 = active, 2 = killed, 3 = killed and stop behaving as normal user 
GTagentStatusPostSpam 3

# This value is number of credential the GT can use to fool colluding SPs looking for
# normal users dealing with many SPs
GTunimportantCredentialsPool 5

# This value is to indicate whether GT agents should report SPAM related to unimportant credentials
GTreportSpam 1

# This value is threshold which lead TGTColluding rank to reach 0 leading to banning suspicious colluding SPx
# based on the formula: TGTcolluding(SPx) = ( (pcrThreshold - pcrNum)/ pcrThreshold) * 100 where
# pcrNum (SPx) = sum (2/record.size * recordCounter) for all colluding records coming from GT agents regarding
# a single credentials where each record contain SPx. 
PcrThreshold 2

# This value is threshold which lead TGTWeakColluding rank to reach 0 leading to banning suspicious colluding SPx
# based on the formula: TGTWeakcolluding(SPx) = ( (pcrWeakThreshold - pcrNum(SPx))/ pcrThreshold) * 100 where
# pcrNum (SPx) = sum (2/record.size * recordCounter) for all colluding records coming from all Users regarding
# all their credentials where each record contain SPx. 
PcrWeakThreshold 4

# This value is binary indicating whether GT ranks including banned SPs to be used in calculating current ranks
IgnoreOldGranks 0

# This value is binary indicating whether GT agents should continue testing suspcious Gs after they reach MIN
# possible rank (best when non-averaging is used and not ignoring old ranks)
StopTestingIfMinReached 0

# This value is binary indicating whether GT agents should only target suspicious Colluding PSPs
GT_SelectOnlyPSP 0

# This value is to indicate the max number of non PSP a GT agent would select (if GT_SelectOnlyPSP is false).
# -1 indicates none 
GT_MaxSPNum 2

# This value is the max size of targeted SPs by any GT agent
GT_MaxSize 7

# This value is % of top PIILs records to be selected for GT testing
PIIL_PCT 20

# This value is binary indicating whether PIIL GT selector to be deployed
PIIL_GTselector 1

# This value is binary indicating whether PSL GT selector to be deployed
PSL_GTselector 1

# This value is binary indicating whether suspicious Nodes Dir would be used as GT selector
SuspiciousNodes_GTselector 0

#-----------------------------------
#SPs settings

# This value is % of initial SPs' rank in the Network
SPRank 100

# This value is % of minimum usefulness rate for the useful (potential popular) SPs
usefulnessMIN 80 

# This value is % of probability of engaging in a new partnership with other SPs in any cycle
newPartnershipProb 0.2

# This value is the duration (in cycles) for any new partnership	
newPartnershipDuration 50

# This value is % of probability of an SP to share newly acquired credentials with a partner	
partnerSharingProb 30

# This value is % of probability of an SP being offered to install DGU during any cycle
OfferDGUProb 20

# This value is % of probability of an SP to accept an offer to install DGU when asked to do so by Auditor
AcceptDGUProb 40

# This value is % of probability of an SP being offered to enable strict DGU during any cycle
OfferStrictDGUProb 10

# This value is % of probability of an SP to accept an offer to enable strict DGU when asked to do so by Auditor	
AcceptStrictDGUProb 20

# This value is % of probability of an SP to accept a compulsory offer (ban if rejected) to install DGU when asked to do so by Auditor
AcceptCompulsoryDGUProb 60

# This value is % of probability of an SP to accept an compulsory offer (ban if rejected) to enable strict DGU when asked to do so by Auditor	
AcceptCompulsoryStrictDGUProb 40

#------------------------------------
#Non-Colluding MSPs settings (Simple Strategies)

SPAM_Drop_StrictCredential_Prob 1

SPAM_Delay_MSP_PCT 0
SPAM_Delay_MPSP_PCT 0
SPAM_Delay_Period 50

SPAM_Bombarding_MSP_PCT 0
SPAM_Bombarding_MPSP_PCT 0
SPAM_Bombarding_Period 50

SPAM_Drop_MSP_PCT 0
SPAM_Drop_MPSP_PCT 0
SPAM_DropUser_Rate 80
SPAM_DropCredential_Rate 80

SPAM_Delay_Bombarding_MSP_PCT 0
SPAM_Delay_Bombarding_MPSP_PCT 0

SPAM_Delay_Drop_MSP_PCT 0
SPAM_Delay_Drop_MPSP_PCT 0

SPAM_Drop_Bombarding_MSP_PCT 0
SPAM_Drop_Bombarding_MPSP_PCT 0

SPAM_Delay_Drop_Bombarding_MSP_PCT 100
SPAM_Delay_Drop_Bombarding_MPSP_PCT 100

#####

SPAM_Delay_ME_MSP_PCT 0
SPAM_Delay_ME_MPSP_PCT 0
SPAM_Delay_ME_Period 47

SPAM_Bombarding_ME_MSP_PCT 0
SPAM_Bombarding_ME_MPSP_PCT 0
SPAM_Bombarding_ME_Period 44

SPAM_Drop_ME_MSP_PCT 0
SPAM_Drop_ME_MPSP_PCT 0
SPAM_DropUser_ME_Rate 20
SPAM_DropCredential_ME_Rate 20

SPAM_Delay_Bombarding_ME_MSP_PCT 0
SPAM_Delay_Bombarding_ME_MPSP_PCT 0

SPAM_Delay_Drop_ME_MSP_PCT 0
SPAM_Delay_Drop_ME_MPSP_PCT 0

SPAM_Drop_Bombarding_ME_MSP_PCT 0
SPAM_Drop_Bombarding_ME_MPSP_PCT 0

SPAM_Delay_Drop_Bombarding_ME_MSP_PCT 100
SPAM_Delay_Drop_Bombarding_ME_MPSP_PCT 100

#-------------------------------------
#ME (Malicious Entities) settings

# This value is minimum number of MPSPs a User should deal with before trusting he is genuine
# and,hence, SPAMing him according to the deployed simple strategies.
# -1 means do not SPAM User, only its credentials if they satisfy the rules.
ME_N_MIN_MPSPs_toSPAM_User 5

# This value is minimum number of MPSPs a Credential should be used with before trusting it is genuine
# , not GT or ST agent, and,hence, SPAMing it according to the deployed simple strategies.
# -1 means do not check counter for specific credentials, just the overall counter for the user
ME_N_MIN_MPSPs_toSPAM_Credential 3

ME_Colluding_Unpopular_PCT 50

ME_Colluding_Popular_PCT 50

#==================================

# parameters of periodic execution
simulation.experiments replicas
simulation.cycles cycles

random.seed 1234567890
network.size networkSize


################### protocols ===========================

protocol.link peersim.core.IdleProtocol

protocol.RMGR_Node trust.simulator.rmgr.protocol.RMGR_Node
protocol.Super_Node trust.simulator.rmgr.protocol.Super_Node
protocol.IDP_Node trust.simulator.rmgr.protocol.idp.IDP_Node
protocol.Auditor_Node trust.simulator.rmgr.protocol.auditor.Auditor_Node
protocol.SP_Node trust.simulator.rmgr.protocol.sp.SP_Node
protocol.ME_Node trust.simulator.rmgr.protocol.me.ME_Node
protocol.User_Node trust.simulator.rmgr.protocol.user.User_Node

protocol.SimulationEngine trust.simulator.rmgr.protocol.SimulationEngine
protocol.SimulationEngine.Experiment Experiment
protocol.SimulationEngine.linkable link
protocol.SimulationEngine.RMGR_Node RMGR_Node
protocol.SimulationEngine.Super_Node Super_Node
protocol.SimulationEngine.IDP_Node IDP_Node
protocol.SimulationEngine.Auditor_Node Auditor_Node
protocol.SimulationEngine.SP_Node SP_Node
protocol.SimulationEngine.ME_Node ME_Node
protocol.SimulationEngine.User_Node User_Node
protocol.SimulationEngine.SPRank SPRank
protocol.SimulationEngine.UpdateIDPRankingsFreq UpdateIDPRankingsFreq
protocol.SimulationEngine.DeployST DeployST
protocol.SimulationEngine.DeployGT DeployGT

################### initialization ======================

init.simulationInit trust.simulator.rmgr.protocol.SimulationInitializer
init.simulationInit.RMGR_Node RMGR_Node
init.simulationInit.Super_Node Super_Node
init.simulationInit.IDP_Node IDP_Node
init.simulationInit.Auditor_Node Auditor_Node
init.simulationInit.SP_Node SP_Node
init.simulationInit.ME_Node ME_Node
init.simulationInit.User_Node User_Node

init.simulationInit.SPRank SPRank

init.simulationInit.INI_Users INI_Users
init.simulationInit.UsersGR UsersGR
init.simulationInit.INI_PSPs INI_PSPs
init.simulationInit.PSPsGR PSPsGR
init.simulationInit.INI_SPs INI_SPs
init.simulationInit.SPsGR SPsGR
init.simulationInit.INI_MPSPs INI_MPSPs
init.simulationInit.MPSPsGR MPSPsGR
init.simulationInit.INI_MSPs INI_MSPs
init.simulationInit.MSPsGR MSPsGR
init.simulationInit.INI_ME INI_ME
init.simulationInit.MEGR MEGR
init.simulationInit.INI_ME_MPSPs INI_ME_MPSPs
init.simulationInit.INI_ME_MSPs INI_ME_MSPs
init.simulationInit.ME_MPSPsGR ME_MPSPsGR
init.simulationInit.ME_MSPsGR ME_MSPsGR
init.simulationInit.ME_MAX_MPSPs ME_MAX_MPSPs
init.simulationInit.ME_MAX_MSPs ME_MAX_MSPs

init.simulationInit.usefulnessMIN usefulnessMIN
init.simulationInit.newPartnershipProb newPartnershipProb
init.simulationInit.newPartnershipDuration newPartnershipDuration
init.simulationInit.partnerSharingProb partnerSharingProb
init.simulationInit.OfferDGUProb OfferDGUProb 
init.simulationInit.OfferStrictDGUProb OfferStrictDGUProb
init.simulationInit.AcceptDGUProb AcceptDGUProb
init.simulationInit.AcceptStrictDGUProb AcceptStrictDGUProb
init.simulationInit.AcceptCompulsoryStrictDGUProb AcceptCompulsoryStrictDGUProb
init.simulationInit.AcceptCompulsoryDGUProb AcceptCompulsoryDGUProb

init.simulationInit.CredentialsArraySize CredentialsArraySize
init.simulationInit.GenerateNewCredentialRate GenerateNewCredentialRate
init.simulationInit.GenerateNewServiceRequestRate GenerateNewServiceRequestRate
init.simulationInit.StrictCredentialProb StrictCredentialProb
init.simulationInit.IniCredentialType IniCredentialType 
init.simulationInit.TrustInNetwork TrustInNetwork
init.simulationInit.IgnoranceRate IgnoranceRate
init.simulationInit.StopUsingCredentialAfterSpam StopUsingCredentialAfterSpam
init.simulationInit.TrustDropPostSpam TrustDropPostSpam
init.simulationInit.TrustIncreasePostSpamResolved TrustIncreasePostSpamResolved
init.simulationInit.TrustIncreaseRate TrustIncreaseRate
init.simulationInit.TrustDecreaseRate TrustDecreaseRate
init.simulationInit.TrustDecreasePeriodPostSpam TrustDecreasePeriodPostSpam

init.simulationInit.TGTWholeWeight TGTWholeWeight
init.simulationInit.TGTWeight TGTWeight
init.simulationInit.TGTColludingWeight TGTColludingWeight
init.simulationInit.TGTWeakColludingWeight TGTWeakColludingWeight
init.simulationInit.TLRavgWeight TLRavgWeight
init.simulationInit.TSTWeight TSTWeight

init.simulationInit.SuspiciousSPRank SuspiciousSPRank
init.simulationInit.SuspiciousSPRange SuspiciousSPRange
init.simulationInit.SuspiciousSPBanningRank SuspiciousSPBanningRank

init.simulationInit.SufficientTLRus_PSP SufficientTLRus_PSP
init.simulationInit.SufficientTLRus_SP SufficientTLRus_SP
init.simulationInit.postDGUinstallationRank postDGUinstallationRank
init.simulationInit.postStrictDGUEnableRank postStrictDGUEnableRank

init.simulationInit.DeployDGU DeployDGU

init.simulationInit.DeployST DeployST
init.simulationInit.STagentStatusPostSpam STagentStatusPostSpam
init.simulationInit.STmaxLifeTime STmaxLifeTime
init.simulationInit.TopTLRavgPCT_ST TopTLRavgPCT_ST
init.simulationInit.BottomTLRavgPCT_ST BottomTLRavgPCT_ST
init.simulationInit.PSL_STselector PSL_STselector 
init.simulationInit.SuspiciousNodes_STselector SuspiciousNodes_STselector 
init.simulationInit.TopTLRavg_STselector TopTLRavg_STselector
init.simulationInit.BottomTLRavg_STselector BottomTLRavg_STselector
init.simulationInit.IIR_STselector IIR_STselector
init.simulationInit.PSL_PCT PSL_PCT

init.simulationInit.DeployGT DeployGT
init.simulationInit.GT_avgTGT GT_avgTGT
init.simulationInit.GTagentStatusPostSpam GTagentStatusPostSpam
init.simulationInit.GTmaxLifeTime GTmaxLifeTime
init.simulationInit.PcrThreshold PcrThreshold
init.simulationInit.PcrWeakThreshold PcrWeakThreshold
init.simulationInit.IgnoreOldGranks IgnoreOldGranks
init.simulationInit.StopTestingIfMinReached StopTestingIfMinReached
init.simulationInit.GT_SelectOnlyPSP GT_SelectOnlyPSP
init.simulationInit.GT_MaxSPNum GT_MaxSPNum
init.simulationInit.GT_MaxSize GT_MaxSize
init.simulationInit.PIIL_PCT PIIL_PCT
init.simulationInit.PIIL_GTselector PIIL_GTselector
init.simulationInit.PSL_GTselector PSL_GTselector 
init.simulationInit.SuspiciousNodes_GTselector SuspiciousNodes_GTselector 

init.simulationInit.SPAM_Drop_StrictCredential_Prob SPAM_Drop_StrictCredential_Prob
init.simulationInit.SPAM_Delay_MSP_PCT SPAM_Delay_MSP_PCT
init.simulationInit.SPAM_Delay_MPSP_PCT SPAM_Delay_MPSP_PCT
init.simulationInit.SPAM_Delay_Period SPAM_Delay_Period
init.simulationInit.SPAM_Bombarding_MSP_PCT SPAM_Bombarding_MSP_PCT
init.simulationInit.SPAM_Bombarding_MPSP_PCT SPAM_Bombarding_MPSP_PCT
init.simulationInit.SPAM_Bombarding_Period SPAM_Bombarding_Period
init.simulationInit.SPAM_Drop_MSP_PCT SPAM_Drop_MSP_PCT
init.simulationInit.SPAM_Drop_MPSP_PCT SPAM_Drop_MPSP_PCT
init.simulationInit.SPAM_DropUser_Rate SPAM_DropUser_Rate
init.simulationInit.SPAM_DropCredential_Rate SPAM_DropCredential_Rate
init.simulationInit.SPAM_Delay_Bombarding_MSP_PCT SPAM_Delay_Bombarding_MSP_PCT
init.simulationInit.SPAM_Delay_Bombarding_MPSP_PCT SPAM_Delay_Bombarding_MPSP_PCT
init.simulationInit.SPAM_Delay_Drop_MSP_PCT SPAM_Delay_Drop_MSP_PCT
init.simulationInit.SPAM_Delay_Drop_MPSP_PCT SPAM_Delay_Drop_MPSP_PCT
init.simulationInit.SPAM_Drop_Bombarding_MSP_PCT SPAM_Drop_Bombarding_MSP_PCT
init.simulationInit.SPAM_Drop_Bombarding_MPSP_PCT SPAM_Drop_Bombarding_MPSP_PCT
init.simulationInit.SPAM_Delay_Drop_Bombarding_MSP_PCT SPAM_Delay_Drop_Bombarding_MSP_PCT
init.simulationInit.SPAM_Delay_Drop_Bombarding_MPSP_PCT SPAM_Delay_Drop_Bombarding_MPSP_PCT

init.simulationInit.SPAM_Delay_ME_MSP_PCT SPAM_Delay_ME_MSP_PCT
init.simulationInit.SPAM_Delay_ME_MPSP_PCT SPAM_Delay_ME_MPSP_PCT
init.simulationInit.SPAM_Delay_ME_Period SPAM_Delay_ME_Period
init.simulationInit.SPAM_Bombarding_ME_MSP_PCT SPAM_Bombarding_ME_MSP_PCT
init.simulationInit.SPAM_Bombarding_ME_MPSP_PCT SPAM_Bombarding_ME_MPSP_PCT
init.simulationInit.SPAM_Bombarding_ME_Period SPAM_Bombarding_ME_Period
init.simulationInit.SPAM_Drop_ME_MSP_PCT SPAM_Drop_ME_MSP_PCT
init.simulationInit.SPAM_Drop_ME_MPSP_PCT SPAM_Drop_ME_MPSP_PCT
init.simulationInit.SPAM_DropUser_ME_Rate SPAM_DropUser_ME_Rate
init.simulationInit.SPAM_DropCredential_ME_Rate SPAM_DropCredential_ME_Rate
init.simulationInit.SPAM_Delay_Bombarding_ME_MSP_PCT SPAM_Delay_Bombarding_ME_MSP_PCT
init.simulationInit.SPAM_Delay_Bombarding_ME_MPSP_PCT SPAM_Delay_Bombarding_ME_MPSP_PCT
init.simulationInit.SPAM_Delay_Drop_ME_MSP_PCT SPAM_Delay_Drop_ME_MSP_PCT
init.simulationInit.SPAM_Delay_Drop_ME_MPSP_PCT SPAM_Delay_Drop_ME_MPSP_PCT
init.simulationInit.SPAM_Drop_Bombarding_ME_MSP_PCT SPAM_Drop_Bombarding_ME_MSP_PCT
init.simulationInit.SPAM_Drop_Bombarding_ME_MPSP_PCT SPAM_Drop_Bombarding_ME_MPSP_PCT
init.simulationInit.SPAM_Delay_Drop_Bombarding_ME_MSP_PCT SPAM_Delay_Drop_Bombarding_ME_MSP_PCT
init.simulationInit.SPAM_Delay_Drop_Bombarding_ME_MPSP_PCT SPAM_Delay_Drop_Bombarding_ME_MPSP_PCT

init.simulationInit.ME_N_MIN_MPSPs_toSPAM_User ME_N_MIN_MPSPs_toSPAM_User
init.simulationInit.ME_N_MIN_MPSPs_toSPAM_Credential ME_N_MIN_MPSPs_toSPAM_Credential
init.simulationInit.ME_Colluding_Unpopular_PCT ME_Colluding_Unpopular_PCT
init.simulationInit.ME_Colluding_Popular_PCT ME_Colluding_Popular_PCT

control.networkGenerator RMGR_DynamicNetworkGenerator
control.networkGenerator.protocol link
control.networkGenerator.add 0
control.networkGenerator.from 1
#control.networkGenerator.maxsize 1000
control.networkGenerator.Super_Node Super_Node
control.networkGenerator.Auditor_Node Auditor_Node
control.networkGenerator.IDP_Node IDP_Node
control.networkGenerator.ME_Node ME_Node
control.networkGenerator.SPRank SPRank
control.networkGenerator.TrustInNetworkDynamic TrustInNetworkDynamic

control.networkGenerator.init.DynamicInit trust.simulator.rmgr.networkGenerator.DynamicInitializer
control.networkGenerator.init.DynamicInit.RMGR_Node RMGR_Node
control.networkGenerator.init.DynamicInit.Super_Node Super_Node
control.networkGenerator.init.DynamicInit.IDP_Node IDP_Node
control.networkGenerator.init.DynamicInit.Auditor_Node Auditor_Node
control.networkGenerator.init.DynamicInit.SP_Node SP_Node
control.networkGenerator.init.DynamicInit.ME_Node ME_Node
control.networkGenerator.init.DynamicInit.User_Node User_Node

control.networkGenerator.init.DynamicInit.SPRank SPRank

control.networkGenerator.init.DynamicInit.MEGR MEGR
control.networkGenerator.init.DynamicInit.INI_ME_MPSPs INI_ME_MPSPs
control.networkGenerator.init.DynamicInit.INI_ME_MSPs INI_ME_MSPs
control.networkGenerator.init.DynamicInit.ME_MPSPsGR ME_MPSPsGR
control.networkGenerator.init.DynamicInit.ME_MSPsGR ME_MSPsGR
control.networkGenerator.init.DynamicInit.ME_MAX_MPSPs ME_MAX_MPSPs
control.networkGenerator.init.DynamicInit.ME_MAX_MSPs ME_MAX_MSPs

control.networkGenerator.init.DynamicInit.usefulnessPCT usefulnessPCT
control.networkGenerator.init.DynamicInit.usefulnessMIN usefulnessMIN
control.networkGenerator.init.DynamicInit.newPartnershipProb newPartnershipProb
control.networkGenerator.init.DynamicInit.newPartnershipDuration newPartnershipDuration
control.networkGenerator.init.DynamicInit.partnerSharingProb partnerSharingProb
control.networkGenerator.init.DynamicInit.AcceptDGUProb AcceptDGUProb
control.networkGenerator.init.DynamicInit.AcceptStrictDGUProb AcceptStrictDGUProb
control.networkGenerator.init.DynamicInit.AcceptCompulsoryStrictDGUProb AcceptCompulsoryStrictDGUProb
control.networkGenerator.init.DynamicInit.AcceptCompulsoryDGUProb AcceptCompulsoryDGUProb

control.networkGenerator.init.DynamicInit.CredentialsArraySize CredentialsArraySize
control.networkGenerator.init.DynamicInit.GenerateNewCredentialRate GenerateNewCredentialRate
control.networkGenerator.init.DynamicInit.GenerateNewServiceRequestRate GenerateNewServiceRequestRate
control.networkGenerator.init.DynamicInit.StrictCredentialProb StrictCredentialProb
control.networkGenerator.init.DynamicInit.IniCredentialType IniCredentialType 
control.networkGenerator.init.DynamicInit.TrustInNetwork TrustInNetwork
control.networkGenerator.init.DynamicInit.IgnoranceRate IgnoranceRate
control.networkGenerator.init.DynamicInit.StopUsingCredentialAfterSpam StopUsingCredentialAfterSpam
control.networkGenerator.init.DynamicInit.TrustDropPostSpam TrustDropPostSpam
control.networkGenerator.init.DynamicInit.TrustIncreasePostSpamResolved TrustIncreasePostSpamResolved
control.networkGenerator.init.DynamicInit.TrustIncreaseRate TrustIncreaseRate
control.networkGenerator.init.DynamicInit.TrustDecreaseRate TrustDecreaseRate
control.networkGenerator.init.DynamicInit.TrustDecreasePeriodPostSpam TrustDecreasePeriodPostSpam

control.networkGenerator.init.DynamicInit.STidleTime STidleTime
control.networkGenerator.init.DynamicInit.STmaxLifeTime STmaxLifeTime
control.networkGenerator.init.DynamicInit.STunimportantCredentialsPool STunimportantCredentialsPool
control.networkGenerator.init.DynamicInit.STreportSpam STreportSpam

control.networkGenerator.init.DynamicInit.GTidleTime GTidleTime
control.networkGenerator.init.DynamicInit.GTmaxLifeTime GTmaxLifeTime
control.networkGenerator.init.DynamicInit.GTunimportantCredentialsPool GTunimportantCredentialsPool
control.networkGenerator.init.DynamicInit.GTreportSpam GTreportSpam

control.networkGenerator.init.DynamicInit.SuspiciousSPRank SuspiciousSPRank
control.networkGenerator.init.DynamicInit.SuspiciousSPRange SuspiciousSPRange
control.networkGenerator.init.DynamicInit.SuspiciousSPBanningRank SuspiciousSPBanningRank

control.networkGenerator.init.DynamicInit.SPAM_Drop_StrictCredential_Prob SPAM_Drop_StrictCredential_Prob
control.networkGenerator.init.DynamicInit.SPAM_Delay_MSP_PCT SPAM_Delay_MSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Delay_MPSP_PCT SPAM_Delay_MPSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Delay_Period SPAM_Delay_Period
control.networkGenerator.init.DynamicInit.SPAM_Bombarding_MSP_PCT SPAM_Bombarding_MSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Bombarding_MPSP_PCT SPAM_Bombarding_MPSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Bombarding_Period SPAM_Bombarding_Period
control.networkGenerator.init.DynamicInit.SPAM_Drop_MSP_PCT SPAM_Drop_MSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Drop_MPSP_PCT SPAM_Drop_MPSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_DropUser_Rate SPAM_DropUser_Rate
control.networkGenerator.init.DynamicInit.SPAM_DropCredential_Rate SPAM_DropCredential_Rate
control.networkGenerator.init.DynamicInit.SPAM_Delay_Bombarding_MSP_PCT SPAM_Delay_Bombarding_MSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Delay_Bombarding_MPSP_PCT SPAM_Delay_Bombarding_MPSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Delay_Drop_MSP_PCT SPAM_Delay_Drop_MSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Delay_Drop_MPSP_PCT SPAM_Delay_Drop_MPSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Drop_Bombarding_MSP_PCT SPAM_Drop_Bombarding_MSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Drop_Bombarding_MPSP_PCT SPAM_Drop_Bombarding_MPSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Delay_Drop_Bombarding_MSP_PCT SPAM_Delay_Drop_Bombarding_MSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Delay_Drop_Bombarding_MPSP_PCT SPAM_Delay_Drop_Bombarding_MPSP_PCT

control.networkGenerator.init.DynamicInit.SPAM_Delay_ME_MSP_PCT SPAM_Delay_ME_MSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Delay_ME_MPSP_PCT SPAM_Delay_ME_MPSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Delay_ME_Period SPAM_Delay_ME_Period
control.networkGenerator.init.DynamicInit.SPAM_Bombarding_ME_MSP_PCT SPAM_Bombarding_ME_MSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Bombarding_ME_MPSP_PCT SPAM_Bombarding_ME_MPSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Bombarding_ME_Period SPAM_Bombarding_ME_Period
control.networkGenerator.init.DynamicInit.SPAM_Drop_ME_MSP_PCT SPAM_Drop_ME_MSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Drop_ME_MPSP_PCT SPAM_Drop_ME_MPSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_DropUser_ME_Rate SPAM_DropUser_ME_Rate
control.networkGenerator.init.DynamicInit.SPAM_DropCredential_ME_Rate SPAM_DropCredential_ME_Rate
control.networkGenerator.init.DynamicInit.SPAM_Delay_Bombarding_ME_MSP_PCT SPAM_Delay_Bombarding_ME_MSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Delay_Bombarding_ME_MPSP_PCT SPAM_Delay_Bombarding_ME_MPSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Delay_Drop_ME_MSP_PCT SPAM_Delay_Drop_ME_MSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Delay_Drop_ME_MPSP_PCT SPAM_Delay_Drop_ME_MPSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Drop_Bombarding_ME_MSP_PCT SPAM_Drop_Bombarding_ME_MSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Drop_Bombarding_ME_MPSP_PCT SPAM_Drop_Bombarding_ME_MPSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Delay_Drop_Bombarding_ME_MSP_PCT SPAM_Delay_Drop_Bombarding_ME_MSP_PCT
control.networkGenerator.init.DynamicInit.SPAM_Delay_Drop_Bombarding_ME_MPSP_PCT SPAM_Delay_Drop_Bombarding_ME_MPSP_PCT

control.networkGenerator.init.DynamicInit.ME_N_MIN_MPSPs_toSPAM_User ME_N_MIN_MPSPs_toSPAM_User
control.networkGenerator.init.DynamicInit.ME_N_MIN_MPSPs_toSPAM_Credential ME_N_MIN_MPSPs_toSPAM_Credential
control.networkGenerator.init.DynamicInit.ME_Colluding_Unpopular_PCT ME_Colluding_Unpopular_PCT
control.networkGenerator.init.DynamicInit.ME_Colluding_Popular_PCT ME_Colluding_Popular_PCT

################ control ==============================

control.performance trust.simulator.rmgr.protocol.PerformanceObserver
control.performance.protocol link
control.performance.RMGR_Node RMGR_Node
control.performance.Super_Node Super_Node
control.performance.IDP_Node IDP_Node
control.performance.Auditor_Node Auditor_Node
control.performance.SP_Node SP_Node
control.performance.ME_Node ME_Node
control.performance.User_Node User_Node

control.performance.Experiment Experiment


include.init simulationInit 
include.control networkGenerator performance

