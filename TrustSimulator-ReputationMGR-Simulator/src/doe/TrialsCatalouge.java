package doe;

import java.util.HashMap;
import java.util.Map;

public class TrialsCatalouge {

	private Map<Integer, HashMap<Integer, Boolean>> catalouge;

	public int size() {
		return catalouge.size();
	}
	
	public Boolean getFactorStatus(int trial, int order)
	{
		return catalouge.get(trial).get(order);
	}
	
	public static void main(String[] args) {
		
		TrialsCatalouge catalouge = new TrialsCatalouge(6);
		
		int count = 0;
		for(int i = 0; i < catalouge.size() ; i++)
		{
			System.out.println(catalouge.getFactorStatus(i+1, 6));
			count++;
			if(count == 4)
			{
				count = 0;
				System.out.println();
			}
		}
	}

	public TrialsCatalouge(int factors)
	{
		catalouge = new HashMap<Integer, HashMap<Integer, Boolean>>();
		//HashMap<Integer, Boolean> factorsData = new HashMap<Integer, Boolean>();     
		
		
		System.out.println("FACTORS: "+factors);
		switch(factors){

		case 2:
			
			HashMap<Integer, Boolean> factorsData_2_1 = new HashMap<Integer, Boolean>();
			factorsData_2_1.put(1, false);
			factorsData_2_1.put(2, false);
			catalouge.put(1, factorsData_2_1);
			
			HashMap<Integer, Boolean> factorsData_2_2 = new HashMap<Integer, Boolean>();
			factorsData_2_2.put(1, true);
			factorsData_2_2.put(2, false);
			catalouge.put(2, factorsData_2_2);
			
			HashMap<Integer, Boolean> factorsData_2_3 = new HashMap<Integer, Boolean>();
			factorsData_2_3.put(1, false);
			factorsData_2_3.put(2, true);
			catalouge.put(3, factorsData_2_3);
			
			HashMap<Integer, Boolean> factorsData_2_4 = new HashMap<Integer, Boolean>();
			factorsData_2_4.put(1, true);
			factorsData_2_4.put(2, true);
			catalouge.put(4, factorsData_2_4);
						
			break;
		
		case 3:
			
			HashMap<Integer, Boolean> factorsData_3_1 = new HashMap<Integer, Boolean>();
			factorsData_3_1.put(1, false);
			factorsData_3_1.put(2, false);
			factorsData_3_1.put(3, false);
			catalouge.put(1, factorsData_3_1);
			
			HashMap<Integer, Boolean> factorsData_3_2 = new HashMap<Integer, Boolean>();
			factorsData_3_2.put(1, true);
			factorsData_3_2.put(2, false);
			factorsData_3_2.put(3, false);
			catalouge.put(2, factorsData_3_2);
			
			HashMap<Integer, Boolean> factorsData_3_3 = new HashMap<Integer, Boolean>();
			factorsData_3_3.put(1, false);
			factorsData_3_3.put(2, true);
			factorsData_3_3.put(3, false);
			catalouge.put(3, factorsData_3_3);
			
			HashMap<Integer, Boolean> factorsData_3_4 = new HashMap<Integer, Boolean>();
			factorsData_3_4.put(1, true);
			factorsData_3_4.put(2, true);
			factorsData_3_4.put(3, false);
			catalouge.put(4, factorsData_3_4);
			
			HashMap<Integer, Boolean> factorsData_3_5 = new HashMap<Integer, Boolean>();
			factorsData_3_5.put(1, false);
			factorsData_3_5.put(2, false);
			factorsData_3_5.put(3, true);
			catalouge.put(5, factorsData_3_5);
			
			HashMap<Integer, Boolean> factorsData_3_6 = new HashMap<Integer, Boolean>();
			factorsData_3_6.put(1, true);
			factorsData_3_6.put(2, false);
			factorsData_3_6.put(3, true);
			catalouge.put(6, factorsData_3_6);
			
			HashMap<Integer, Boolean> factorsData_3_7 = new HashMap<Integer, Boolean>();
			factorsData_3_7.put(1, false);
			factorsData_3_7.put(2, true);
			factorsData_3_7.put(3, true);
			catalouge.put(7, factorsData_3_7);
			
			HashMap<Integer, Boolean> factorsData_3_8 = new HashMap<Integer, Boolean>();
			factorsData_3_8.put(1, true);
			factorsData_3_8.put(2, true);
			factorsData_3_8.put(3, true);
			catalouge.put(8, factorsData_3_8);
			
			break;

		case 4:
			
			HashMap<Integer, Boolean> factorsData_4_1 = new HashMap<Integer, Boolean>();
			factorsData_4_1.put(1, false);
			factorsData_4_1.put(2, false);
			factorsData_4_1.put(3, false);
			factorsData_4_1.put(4, false);
			catalouge.put(1, factorsData_4_1);
			
			HashMap<Integer, Boolean> factorsData_4_2 = new HashMap<Integer, Boolean>();
			factorsData_4_2.put(1, true);
			factorsData_4_2.put(2, false);
			factorsData_4_2.put(3, false);
			factorsData_4_2.put(4, false);
			catalouge.put(2, factorsData_4_2);
			
			HashMap<Integer, Boolean> factorsData_4_3 = new HashMap<Integer, Boolean>();
			factorsData_4_3.put(1, false);
			factorsData_4_3.put(2, true);
			factorsData_4_3.put(3, false);
			factorsData_4_3.put(4, false);
			catalouge.put(3, factorsData_4_3);
			
			HashMap<Integer, Boolean> factorsData_4_4 = new HashMap<Integer, Boolean>();
			factorsData_4_4.put(1, true);
			factorsData_4_4.put(2, true);
			factorsData_4_4.put(3, false);
			factorsData_4_4.put(4, false);
			catalouge.put(4, factorsData_4_4);
			
			HashMap<Integer, Boolean> factorsData_4_5 = new HashMap<Integer, Boolean>();
			factorsData_4_5.put(1, false);
			factorsData_4_5.put(2, false);
			factorsData_4_5.put(3, true);
			factorsData_4_5.put(4, false);
			catalouge.put(5, factorsData_4_5);
			
			HashMap<Integer, Boolean> factorsData_4_6 = new HashMap<Integer, Boolean>();
			factorsData_4_6.put(1, true);
			factorsData_4_6.put(2, false);
			factorsData_4_6.put(3, true);
			factorsData_4_6.put(4, false);
			catalouge.put(6, factorsData_4_6);
			
			HashMap<Integer, Boolean> factorsData_4_7 = new HashMap<Integer, Boolean>();
			factorsData_4_7.put(1, false);
			factorsData_4_7.put(2, true);
			factorsData_4_7.put(3, true);
			factorsData_4_7.put(4, false);
			catalouge.put(7, factorsData_4_7);
			
			HashMap<Integer, Boolean> factorsData_4_8 = new HashMap<Integer, Boolean>();
			factorsData_4_8.put(1, true);
			factorsData_4_8.put(2, true);
			factorsData_4_8.put(3, true);
			factorsData_4_8.put(4, false);
			catalouge.put(8, factorsData_4_8);
			
			HashMap<Integer, Boolean> factorsData_4_9 = new HashMap<Integer, Boolean>();
			factorsData_4_9.put(1, false);
			factorsData_4_9.put(2, false);
			factorsData_4_9.put(3, false);
			factorsData_4_9.put(4, true);
			catalouge.put(9, factorsData_4_9);
			
			HashMap<Integer, Boolean> factorsData_4_10 = new HashMap<Integer, Boolean>();
			factorsData_4_10.put(1, true);
			factorsData_4_10.put(2, false);
			factorsData_4_10.put(3, false);
			factorsData_4_10.put(4, true);
			catalouge.put(10, factorsData_4_10);
			
			HashMap<Integer, Boolean> factorsData_4_11 = new HashMap<Integer, Boolean>();
			factorsData_4_11.put(1, false);
			factorsData_4_11.put(2, true);
			factorsData_4_11.put(3, false);
			factorsData_4_11.put(4, true);
			catalouge.put(11, factorsData_4_11);
			
			HashMap<Integer, Boolean> factorsData_4_12 = new HashMap<Integer, Boolean>();
			factorsData_4_12.put(1, true);
			factorsData_4_12.put(2, true);
			factorsData_4_12.put(3, false);
			factorsData_4_12.put(4, true);
			catalouge.put(12, factorsData_4_12);
			
			HashMap<Integer, Boolean> factorsData_4_13 = new HashMap<Integer, Boolean>();
			factorsData_4_13.put(1, false);
			factorsData_4_13.put(2, false);
			factorsData_4_13.put(3, true);
			factorsData_4_13.put(4, true);
			catalouge.put(13, factorsData_4_13);
			
			HashMap<Integer, Boolean> factorsData_4_14 = new HashMap<Integer, Boolean>();
			factorsData_4_14.put(1, true);
			factorsData_4_14.put(2, false);
			factorsData_4_14.put(3, true);
			factorsData_4_14.put(4, true);
			catalouge.put(14, factorsData_4_14);
			
			HashMap<Integer, Boolean> factorsData_4_15 = new HashMap<Integer, Boolean>();
			factorsData_4_15.put(1, false);
			factorsData_4_15.put(2, true);
			factorsData_4_15.put(3, true);
			factorsData_4_15.put(4, true);
			catalouge.put(15, factorsData_4_15);
			
			HashMap<Integer, Boolean> factorsData_4_16 = new HashMap<Integer, Boolean>();
			factorsData_4_16.put(1, true);
			factorsData_4_16.put(2, true);
			factorsData_4_16.put(3, true);
			factorsData_4_16.put(4, true);
			catalouge.put(16, factorsData_4_16);
			
			break;
			
	case 5:
			
			HashMap<Integer, Boolean> factorsData_5_1 = new HashMap<Integer, Boolean>();
			factorsData_5_1.put(1, false);
			factorsData_5_1.put(2, false);
			factorsData_5_1.put(3, false);
			factorsData_5_1.put(4, false);
			factorsData_5_1.put(5, true);
			catalouge.put(1, factorsData_5_1);
			
			HashMap<Integer, Boolean> factorsData_5_2 = new HashMap<Integer, Boolean>();
			factorsData_5_2.put(1, true);
			factorsData_5_2.put(2, false);
			factorsData_5_2.put(3, false);
			factorsData_5_2.put(4, false);
			factorsData_5_2.put(5, false);
			catalouge.put(2, factorsData_5_2);
			
			HashMap<Integer, Boolean> factorsData_5_3 = new HashMap<Integer, Boolean>();
			factorsData_5_3.put(1, false);
			factorsData_5_3.put(2, true);
			factorsData_5_3.put(3, false);
			factorsData_5_3.put(4, false);
			factorsData_5_3.put(5, false);
			catalouge.put(3, factorsData_5_3);
			
			HashMap<Integer, Boolean> factorsData_5_4 = new HashMap<Integer, Boolean>();
			factorsData_5_4.put(1, true);
			factorsData_5_4.put(2, true);
			factorsData_5_4.put(3, false);
			factorsData_5_4.put(4, false);
			factorsData_5_4.put(5, true);
			catalouge.put(4, factorsData_5_4);
			
			HashMap<Integer, Boolean> factorsData_5_5 = new HashMap<Integer, Boolean>();
			factorsData_5_5.put(1, false);
			factorsData_5_5.put(2, false);
			factorsData_5_5.put(3, true);
			factorsData_5_5.put(4, false);
			factorsData_5_5.put(5, false);
			catalouge.put(5, factorsData_5_5);
			
			HashMap<Integer, Boolean> factorsData_5_6 = new HashMap<Integer, Boolean>();
			factorsData_5_6.put(1, true);
			factorsData_5_6.put(2, false);
			factorsData_5_6.put(3, true);
			factorsData_5_6.put(4, false);
			factorsData_5_6.put(5, true);
			catalouge.put(6, factorsData_5_6);
			
			HashMap<Integer, Boolean> factorsData_5_7 = new HashMap<Integer, Boolean>();
			factorsData_5_7.put(1, false);
			factorsData_5_7.put(2, true);
			factorsData_5_7.put(3, true);
			factorsData_5_7.put(4, false);
			factorsData_5_7.put(5, true);
			catalouge.put(7, factorsData_5_7);
			
			HashMap<Integer, Boolean> factorsData_5_8 = new HashMap<Integer, Boolean>();
			factorsData_5_8.put(1, true);
			factorsData_5_8.put(2, true);
			factorsData_5_8.put(3, true);
			factorsData_5_8.put(4, false);
			factorsData_5_8.put(5, false);
			catalouge.put(8, factorsData_5_8);
			
			HashMap<Integer, Boolean> factorsData_5_9 = new HashMap<Integer, Boolean>();
			factorsData_5_9.put(1, false);
			factorsData_5_9.put(2, false);
			factorsData_5_9.put(3, false);
			factorsData_5_9.put(4, true);
			factorsData_5_9.put(5, false);
			catalouge.put(9, factorsData_5_9);
			
			HashMap<Integer, Boolean> factorsData_5_10 = new HashMap<Integer, Boolean>();
			factorsData_5_10.put(1, true);
			factorsData_5_10.put(2, false);
			factorsData_5_10.put(3, false);
			factorsData_5_10.put(4, true);
			factorsData_5_10.put(5, true);
			catalouge.put(10, factorsData_5_10);
			
			HashMap<Integer, Boolean> factorsData_5_11 = new HashMap<Integer, Boolean>();
			factorsData_5_11.put(1, false);
			factorsData_5_11.put(2, true);
			factorsData_5_11.put(3, false);
			factorsData_5_11.put(4, true);
			factorsData_5_11.put(5, true);
			catalouge.put(11, factorsData_5_11);
			
			HashMap<Integer, Boolean> factorsData_5_12 = new HashMap<Integer, Boolean>();
			factorsData_5_12.put(1, true);
			factorsData_5_12.put(2, true);
			factorsData_5_12.put(3, false);
			factorsData_5_12.put(4, true);
			factorsData_5_12.put(5, false);
			catalouge.put(12, factorsData_5_12);
			
			HashMap<Integer, Boolean> factorsData_5_13 = new HashMap<Integer, Boolean>();
			factorsData_5_13.put(1, false);
			factorsData_5_13.put(2, false);
			factorsData_5_13.put(3, true);
			factorsData_5_13.put(4, true);
			factorsData_5_13.put(5, true);
			catalouge.put(13, factorsData_5_13);
			
			HashMap<Integer, Boolean> factorsData_5_14 = new HashMap<Integer, Boolean>();
			factorsData_5_14.put(1, true);
			factorsData_5_14.put(2, false);
			factorsData_5_14.put(3, true);
			factorsData_5_14.put(4, true);
			factorsData_5_14.put(5, false);
			catalouge.put(14, factorsData_5_14);
			
			HashMap<Integer, Boolean> factorsData_5_15 = new HashMap<Integer, Boolean>();
			factorsData_5_15.put(1, false);
			factorsData_5_15.put(2, true);
			factorsData_5_15.put(3, true);
			factorsData_5_15.put(4, true);
			factorsData_5_15.put(5, false);
			catalouge.put(15, factorsData_5_15);
			
			HashMap<Integer, Boolean> factorsData_5_16 = new HashMap<Integer, Boolean>();
			factorsData_5_16.put(1, true);
			factorsData_5_16.put(2, true);
			factorsData_5_16.put(3, true);
			factorsData_5_16.put(4, true);
			factorsData_5_16.put(5, true);
			catalouge.put(16, factorsData_5_16);
			
			break;

			
		case 6:
			
			double countFact1_6 = 1; double countFact2_6 = 1; double countFact3_6 = 1; double countFact4_6 = 1;
			double countFact5_6 = 1;
			
			for(int i = 1; i <= 32; i++)
			{
				HashMap<Integer, Boolean> factorsData = new HashMap<Integer, Boolean>();
				
				//Factor 1 settings
				if(countFact1_6 == 1)
				{
					factorsData.put(1, false);
					countFact1_6--;
				}
				else
				{
					factorsData.put(1, true);
					countFact1_6++;
				}
				
				//Factor 2 settings
				if(countFact2_6 <= 2)
				{
					factorsData.put(2, false);
					countFact2_6++;
				}
				else
				{
					factorsData.put(2, true);
					
					if(countFact2_6 == 4) countFact2_6 = 1;
					else countFact2_6++;
				}
				
				//Factor 3 settings
				if(countFact3_6 <= 4)
				{
					factorsData.put(3, false);
					countFact3_6++;
				}
				else
				{
					factorsData.put(3, true);
					
					if(countFact3_6 == 8) countFact3_6 = 1;
					else countFact3_6++;
				}
				
				//Factor 4 settings
				if(countFact4_6 <= 8)
				{
					factorsData.put(4, false);
					countFact4_6++;
				}
				else
				{
					factorsData.put(4, true);
					
					if(countFact4_6 == 16) countFact4_6 = 1;
					else countFact4_6++;
				}
				
				//Factor 5 settings
				if(countFact5_6 <= 16)
				{
					factorsData.put(5, false);
					countFact5_6++;
				}
				else
				{
					factorsData.put(5, true);
					
					if(countFact5_6 == 32) countFact5_6 = 1;
					else countFact5_6++;
				}
				
				//Factor 6 settings
				
				int countFact6_6 = ((i-1) % 8)+1;
				
				switch(countFact6_6){
				
				case 1:
					if( i <= 8 || (i > 24 && i <= 32) )
						factorsData.put(6, false);
					else
						factorsData.put(6, true);
					break;
					
				case 2:
					if( i <= 8 || (i > 24 && i <= 32) )
						factorsData.put(6, true);
					else
						factorsData.put(6, false);
					break;
					
				case 3:
					if( i <= 8 || (i > 24 && i <= 32) )
						factorsData.put(6, true);
					else
						factorsData.put(6, false);
					break;
					
				case 4:
					if( i <= 8 || (i > 24 && i <= 32) )
						factorsData.put(6, false);
					else
						factorsData.put(6, true);
					break;
					
				case 5:
					if( i <= 8 || (i > 24 && i <= 32) )
						factorsData.put(6, true);
					else
						factorsData.put(6, false);
					break;
					
				case 6:
					if( i <= 8 || (i > 24 && i <= 32) )
						factorsData.put(6, false);
					else
						factorsData.put(6, true);
					break;
					
				case 7:
					if( i <= 8 || (i > 24 && i <= 32) )
						factorsData.put(6, false);
					else
						factorsData.put(6, true);
					break;
					
				case 8:
					if( i <= 8 || (i > 24 && i <= 32) )
						factorsData.put(6, true);
					else
						factorsData.put(6, false);
					break;										
				}
				
				catalouge.put(i, factorsData);
			}
			
			break;
			
		case 7:
			
			double countFact1 = 1; double countFact2 = 1; double countFact3 = 1; double countFact4 = 1;
			double countFact5 = 1; double countFact6 = 1; 
			
			for(int i = 1; i <= 64; i++)
			{
				HashMap<Integer, Boolean> factorsData = new HashMap<Integer, Boolean>();
				
				//Factor 1 settings
				if(countFact1 == 1)
				{
					factorsData.put(1, false);
					countFact1--;
				}
				else
				{
					factorsData.put(1, true);
					countFact1++;
				}
				
				//Factor 2 settings
				if(countFact2 <= 2)
				{
					factorsData.put(2, false);
					countFact2++;
				}
				else
				{
					factorsData.put(2, true);
					
					if(countFact2 == 4) countFact2 = 1;
					else countFact2++;
				}
				
				//Factor 3 settings
				if(countFact3 <= 4)
				{
					factorsData.put(3, false);
					countFact3++;
				}
				else
				{
					factorsData.put(3, true);
					
					if(countFact3 == 8) countFact3 = 1;
					else countFact3++;
				}
				
				//Factor 4 settings
				if(countFact4 <= 8)
				{
					factorsData.put(4, false);
					countFact4++;
				}
				else
				{
					factorsData.put(4, true);
					
					if(countFact4 == 16) countFact4 = 1;
					else countFact4++;
				}
				
				//Factor 5 settings
				if(countFact5 <= 16)
				{
					factorsData.put(5, false);
					countFact5++;
				}
				else
				{
					factorsData.put(5, true);
					
					if(countFact5 == 32) countFact5 = 1;
					else countFact5++;
				}
				
				//Factor 6 settings
				if(countFact6 <= 32)
				{
					factorsData.put(6, false);
					countFact6++;
				}
				else
				{
					factorsData.put(6, true);
					
					if(countFact6 == 64) countFact6 = 1;
					else countFact6++;
				}
				
				//Factor 7 settings
				switch(i){
				
				case 1:
					factorsData.put(7, true);
					break;
					
				case 2:
					factorsData.put(7, false);
					break;
					
				case 3:
					factorsData.put(7, false);
					break;
					
				case 4:
					factorsData.put(7, true);
					break;
					
				case 5:
					factorsData.put(7, false);
					break;
					
				case 6:
					factorsData.put(7, true);
					break;
					
				case 7:
					factorsData.put(7, true);
					break;
					
				case 8:
					factorsData.put(7, false);
					break;
					
				case 9:
					factorsData.put(7, false);
					break;
					
				case 10:
					factorsData.put(7, true);
					break;
					
				case 11:
					factorsData.put(7, true);
					break;
					
				case 12:
					factorsData.put(7, false);
					break;
					
				case 13:
					factorsData.put(7, true);
					break;
					
				case 14:
					factorsData.put(7, false);
					break;
					
				case 15:
					factorsData.put(7, false);
					break;
					
				case 16:
					factorsData.put(7, true);
					break;
					
				case 17:
					factorsData.put(7, false);
					break;
					
				case 18:
					factorsData.put(7, true);
					break;
					
				case 19:
					factorsData.put(7, true);
					break;
					
				case 20:
					factorsData.put(7, false);
					break;
					
				case 21:
					factorsData.put(7, true);
					break;
					
				case 22:
					factorsData.put(7, false);
					break;
					
				case 23:
					factorsData.put(7, false);
					break;
					
				case 24:
					factorsData.put(7, true);
					break;
					
				case 25:
					factorsData.put(7, true);
					break;
					
				case 26:
					factorsData.put(7, false);
					break;
					
				case 27:
					factorsData.put(7, false);
					break;
					
				case 28:
					factorsData.put(7, true);
					break;
					
				case 29:
					factorsData.put(7, false);
					break;
					
				case 30:
					factorsData.put(7, true);
					break;
					
				case 31:
					factorsData.put(7, true);
					break;
					
				case 32:
					factorsData.put(7, false);
					break;
					
				case 33:
					factorsData.put(7, false);
					break;
					
				case 34:
					factorsData.put(7, true);
					break;
					
				case 35:
					factorsData.put(7, true);
					break;
					
				case 36:
					factorsData.put(7, false);
					break;
					
				case 37:
					factorsData.put(7, true);
					break;
					
				case 38:
					factorsData.put(7, false);
					break;
					
				case 39:
					factorsData.put(7, false);
					break;
					
				case 40:
					factorsData.put(7, true);
					break;
					
				case 41:
					factorsData.put(7, true);
					break;
					
				case 42:
					factorsData.put(7, false);
					break;
					
				case 43:
					factorsData.put(7, false);
					break;
					
				case 44:
					factorsData.put(7, true);
					break;
					
				case 45:
					factorsData.put(7, false);
					break;
					
				case 46:
					factorsData.put(7, true);
					break;
					
				case 47:
					factorsData.put(7, true);
					break;
					
				case 48:
					factorsData.put(7, false);
					break;
					
				case 49:
					factorsData.put(7, true);
					break;
					
				case 50:
					factorsData.put(7, false);
					break;
					
				case 51:
					factorsData.put(7, false);
					break;
					
				case 52:
					factorsData.put(7, true);
					break;
					
				case 53:
					factorsData.put(7, false);
					break;
					
				case 54:
					factorsData.put(7, true);
					break;
					
				case 55:
					factorsData.put(7, true);
					break;
					
				case 56:
					factorsData.put(7, false);
					break;
					
				case 57:
					factorsData.put(7, false);
					break;
					
				case 58:
					factorsData.put(7, true);
					break;
					
				case 59:
					factorsData.put(7, true);
					break;
					
				case 60:
					factorsData.put(7, false);
					break;
					
				case 61:
					factorsData.put(7, true);
					break;
					
				case 62:
					factorsData.put(7, false);
					break;
					
				case 63:
					factorsData.put(7, false);
					break;
					
				case 64:
					factorsData.put(7, true);
					break;
				}
				
				catalouge.put(i, factorsData);
			}
			
			break;
			
		case 8:
			
			HashMap<Integer, Boolean> factorsData_8_1 = new HashMap<Integer, Boolean>();
			factorsData_8_1.put(1, false);
			factorsData_8_1.put(2, false);
			factorsData_8_1.put(3, false);
			factorsData_8_1.put(4, false);
			factorsData_8_1.put(5, false);
			factorsData_8_1.put(6, false);
			factorsData_8_1.put(7, true);
			factorsData_8_1.put(8, true);
			catalouge.put(1, factorsData_8_1);
			
			
			HashMap<Integer, Boolean> factorsData_8_2 = new HashMap<Integer, Boolean>();
			factorsData_8_2.put(1, true);
			factorsData_8_2.put(2, false);
			factorsData_8_2.put(3, false);
			factorsData_8_2.put(4, false);
			factorsData_8_2.put(5, false);
			factorsData_8_2.put(6, false);
			factorsData_8_2.put(7, false);
			factorsData_8_2.put(8, false);
			catalouge.put(2, factorsData_8_2);			
			
			
			HashMap<Integer, Boolean> factorsData_8_3 = new HashMap<Integer, Boolean>();
			factorsData_8_3.put(1, false);
			factorsData_8_3.put(2, true);
			factorsData_8_3.put(3, false);
			factorsData_8_3.put(4, false);
			factorsData_8_3.put(5, false);
			factorsData_8_3.put(6, false);
			factorsData_8_3.put(7, false);
			factorsData_8_3.put(8, false);
			catalouge.put(3, factorsData_8_3);
			
			HashMap<Integer, Boolean> factorsData_8_4 = new HashMap<Integer, Boolean>();
			factorsData_8_4.put(1, true);
			factorsData_8_4.put(2, true);
			factorsData_8_4.put(3, false);
			factorsData_8_4.put(4, false);
			factorsData_8_4.put(5, false);
			factorsData_8_4.put(6, false);
			factorsData_8_4.put(7, true);
			factorsData_8_4.put(8, true);
			catalouge.put(4, factorsData_8_4);
			
			HashMap<Integer, Boolean> factorsData_8_5 = new HashMap<Integer, Boolean>();
			factorsData_8_5.put(1, false);
			factorsData_8_5.put(2, false);
			factorsData_8_5.put(3, true);
			factorsData_8_5.put(4, false);
			factorsData_8_5.put(5, false);
			factorsData_8_5.put(6, false);
			factorsData_8_5.put(7, false);
			factorsData_8_5.put(8, true);
			catalouge.put(5, factorsData_8_5);
			
			HashMap<Integer, Boolean> factorsData_8_6 = new HashMap<Integer, Boolean>();
			factorsData_8_6.put(1, true);
			factorsData_8_6.put(2, false);
			factorsData_8_6.put(3, true);
			factorsData_8_6.put(4, false);
			factorsData_8_6.put(5, false);
			factorsData_8_6.put(6, false);
			factorsData_8_6.put(7, true);
			factorsData_8_6.put(8, false);
			catalouge.put(6, factorsData_8_6);
			
			HashMap<Integer, Boolean> factorsData_8_7 = new HashMap<Integer, Boolean>();
			factorsData_8_7.put(1, false);
			factorsData_8_7.put(2, true);
			factorsData_8_7.put(3, true);
			factorsData_8_7.put(4, false);
			factorsData_8_7.put(5, false);
			factorsData_8_7.put(6, false);
			factorsData_8_7.put(7, true);
			factorsData_8_7.put(8, false);
			catalouge.put(7, factorsData_8_7);
			
			HashMap<Integer, Boolean> factorsData_8_8 = new HashMap<Integer, Boolean>();
			factorsData_8_8.put(1, true);
			factorsData_8_8.put(2, true);
			factorsData_8_8.put(3, true);
			factorsData_8_8.put(4, false);
			factorsData_8_8.put(5, false);
			factorsData_8_8.put(6, false);
			factorsData_8_8.put(7, false);
			factorsData_8_8.put(8, true);
			catalouge.put(8, factorsData_8_8);
			
			HashMap<Integer, Boolean> factorsData_8_9 = new HashMap<Integer, Boolean>();
			factorsData_8_9.put(1, false);
			factorsData_8_9.put(2, false);
			factorsData_8_9.put(3, false);
			factorsData_8_9.put(4, true);
			factorsData_8_9.put(5, false);
			factorsData_8_9.put(6, false);
			factorsData_8_9.put(7, false);
			factorsData_8_9.put(8, true);
			catalouge.put(9, factorsData_8_9);
			
			HashMap<Integer, Boolean> factorsData_8_10 = new HashMap<Integer, Boolean>();
			factorsData_8_10.put(1, true);
			factorsData_8_10.put(2, false);
			factorsData_8_10.put(3, false);
			factorsData_8_10.put(4, true);
			factorsData_8_10.put(5, false);
			factorsData_8_10.put(6, false);
			factorsData_8_10.put(7, true);
			factorsData_8_10.put(8, false);
			catalouge.put(10, factorsData_8_10);
			
			HashMap<Integer, Boolean> factorsData_8_11 = new HashMap<Integer, Boolean>();
			factorsData_8_11.put(1, false);
			factorsData_8_11.put(2, true);
			factorsData_8_11.put(3, false);
			factorsData_8_11.put(4, true);
			factorsData_8_11.put(5, false);
			factorsData_8_11.put(6, false);
			factorsData_8_11.put(7, true);
			factorsData_8_11.put(8, false);
			catalouge.put(11, factorsData_8_11);
			
			HashMap<Integer, Boolean> factorsData_8_12 = new HashMap<Integer, Boolean>();
			factorsData_8_12.put(1, true);
			factorsData_8_12.put(2, true);
			factorsData_8_12.put(3, false);
			factorsData_8_12.put(4, true);
			factorsData_8_12.put(5, false);
			factorsData_8_12.put(6, false);
			factorsData_8_12.put(7, false);
			factorsData_8_12.put(8, true);
			catalouge.put(12, factorsData_8_12);
			
			HashMap<Integer, Boolean> factorsData_8_13 = new HashMap<Integer, Boolean>();
			factorsData_8_13.put(1, false);
			factorsData_8_13.put(2, false);
			factorsData_8_13.put(3, true);
			factorsData_8_13.put(4, true);
			factorsData_8_13.put(5, false);
			factorsData_8_13.put(6, false);
			factorsData_8_13.put(7, true);
			factorsData_8_13.put(8, true);
			catalouge.put(13, factorsData_8_13);
			
			HashMap<Integer, Boolean> factorsData_8_14 = new HashMap<Integer, Boolean>();
			factorsData_8_14.put(1, true);
			factorsData_8_14.put(2, false);
			factorsData_8_14.put(3, true);
			factorsData_8_14.put(4, true);
			factorsData_8_14.put(5, false);
			factorsData_8_14.put(6, false);
			factorsData_8_14.put(7, false);
			factorsData_8_14.put(8, false);
			catalouge.put(14, factorsData_8_14);
			
			HashMap<Integer, Boolean> factorsData_8_15 = new HashMap<Integer, Boolean>();
			factorsData_8_15.put(1, false);
			factorsData_8_15.put(2, true);
			factorsData_8_15.put(3, true);
			factorsData_8_15.put(4, true);
			factorsData_8_15.put(5, false);
			factorsData_8_15.put(6, false);
			factorsData_8_15.put(7, false);
			factorsData_8_15.put(8, false);
			catalouge.put(15, factorsData_8_15);
			
			HashMap<Integer, Boolean> factorsData_8_16 = new HashMap<Integer, Boolean>();
			factorsData_8_16.put(1, true);
			factorsData_8_16.put(2, true);
			factorsData_8_16.put(3, true);
			factorsData_8_16.put(4, true);
			factorsData_8_16.put(5, false);
			factorsData_8_16.put(6, false);
			factorsData_8_16.put(7, true);
			factorsData_8_16.put(8, true);
			catalouge.put(16, factorsData_8_16);
			
			HashMap<Integer, Boolean> factorsData_8_17 = new HashMap<Integer, Boolean>();
			factorsData_8_17.put(1, false);
			factorsData_8_17.put(2, false);
			factorsData_8_17.put(3, false);
			factorsData_8_17.put(4, false);
			factorsData_8_17.put(5, true);
			factorsData_8_17.put(6, false);
			factorsData_8_17.put(7, true);
			factorsData_8_17.put(8, false);
			catalouge.put(17, factorsData_8_17);
			
			HashMap<Integer, Boolean> factorsData_8_18 = new HashMap<Integer, Boolean>();
			factorsData_8_18.put(1, true);
			factorsData_8_18.put(2, false);
			factorsData_8_18.put(3, false);
			factorsData_8_18.put(4, false);
			factorsData_8_18.put(5, true);
			factorsData_8_18.put(6, false);
			factorsData_8_18.put(7, false);
			factorsData_8_18.put(8, true);
			catalouge.put(18, factorsData_8_18);
			
			HashMap<Integer, Boolean> factorsData_8_19 = new HashMap<Integer, Boolean>();
			factorsData_8_19.put(1, false);
			factorsData_8_19.put(2, true);
			factorsData_8_19.put(3, false);
			factorsData_8_19.put(4, false);
			factorsData_8_19.put(5, true);
			factorsData_8_19.put(6, false);
			factorsData_8_19.put(7, false);
			factorsData_8_19.put(8, true);
			catalouge.put(19, factorsData_8_19);
			
			HashMap<Integer, Boolean> factorsData_8_20 = new HashMap<Integer, Boolean>();
			factorsData_8_20.put(1, true);
			factorsData_8_20.put(2, true);
			factorsData_8_20.put(3, false);
			factorsData_8_20.put(4, false);
			factorsData_8_20.put(5, true);
			factorsData_8_20.put(6, false);
			factorsData_8_20.put(7, true);
			factorsData_8_20.put(8, false);
			catalouge.put(20, factorsData_8_20);
			
			HashMap<Integer, Boolean> factorsData_8_21 = new HashMap<Integer, Boolean>();
			factorsData_8_21.put(1, false);
			factorsData_8_21.put(2, false);
			factorsData_8_21.put(3, true);
			factorsData_8_21.put(4, false);
			factorsData_8_21.put(5, true);
			factorsData_8_21.put(6, false);
			factorsData_8_21.put(7, false);
			factorsData_8_21.put(8, false);
			catalouge.put(21, factorsData_8_21);
			
			HashMap<Integer, Boolean> factorsData_8_22 = new HashMap<Integer, Boolean>();
			factorsData_8_22.put(1, true);
			factorsData_8_22.put(2, false);
			factorsData_8_22.put(3, true);
			factorsData_8_22.put(4, false);
			factorsData_8_22.put(5, true);
			factorsData_8_22.put(6, false);
			factorsData_8_22.put(7, true);
			factorsData_8_22.put(8, true);
			catalouge.put(22, factorsData_8_22);
			
			HashMap<Integer, Boolean> factorsData_8_23 = new HashMap<Integer, Boolean>();
			factorsData_8_23.put(1, false);
			factorsData_8_23.put(2, true);
			factorsData_8_23.put(3, true);
			factorsData_8_23.put(4, false);
			factorsData_8_23.put(5, true);
			factorsData_8_23.put(6, false);
			factorsData_8_23.put(7, true);
			factorsData_8_23.put(8, true);
			catalouge.put(23, factorsData_8_23);
			
			HashMap<Integer, Boolean> factorsData_8_24 = new HashMap<Integer, Boolean>();
			factorsData_8_24.put(1, true);
			factorsData_8_24.put(2, true);
			factorsData_8_24.put(3, true);
			factorsData_8_24.put(4, false);
			factorsData_8_24.put(5, true);
			factorsData_8_24.put(6, false);
			factorsData_8_24.put(7, false);
			factorsData_8_24.put(8, false);
			catalouge.put(24, factorsData_8_24);
			
			HashMap<Integer, Boolean> factorsData_8_25 = new HashMap<Integer, Boolean>();
			factorsData_8_25.put(1, false);
			factorsData_8_25.put(2, false);
			factorsData_8_25.put(3, false);
			factorsData_8_25.put(4, true);
			factorsData_8_25.put(5, true);
			factorsData_8_25.put(6, false);
			factorsData_8_25.put(7, false);
			factorsData_8_25.put(8, false);
			catalouge.put(25, factorsData_8_25);
			
			HashMap<Integer, Boolean> factorsData_8_26 = new HashMap<Integer, Boolean>();
			factorsData_8_26.put(1, true);
			factorsData_8_26.put(2, false);
			factorsData_8_26.put(3, false);
			factorsData_8_26.put(4, true);
			factorsData_8_26.put(5, true);
			factorsData_8_26.put(6, false);
			factorsData_8_26.put(7, true);
			factorsData_8_26.put(8, true);
			catalouge.put(26, factorsData_8_26);
			
			HashMap<Integer, Boolean> factorsData_8_27 = new HashMap<Integer, Boolean>();
			factorsData_8_27.put(1, false);
			factorsData_8_27.put(2, true);
			factorsData_8_27.put(3, false);
			factorsData_8_27.put(4, true);
			factorsData_8_27.put(5, true);
			factorsData_8_27.put(6, false);
			factorsData_8_27.put(7, true);
			factorsData_8_27.put(8, true);
			catalouge.put(27, factorsData_8_27);
			
			HashMap<Integer, Boolean> factorsData_8_28 = new HashMap<Integer, Boolean>();
			factorsData_8_28.put(1, true);
			factorsData_8_28.put(2, true);
			factorsData_8_28.put(3, false);
			factorsData_8_28.put(4, true);
			factorsData_8_28.put(5, true);
			factorsData_8_28.put(6, false);
			factorsData_8_28.put(7, false);
			factorsData_8_28.put(8, false);
			catalouge.put(28, factorsData_8_28);
			
			HashMap<Integer, Boolean> factorsData_8_29 = new HashMap<Integer, Boolean>();
			factorsData_8_29.put(1, false);
			factorsData_8_29.put(2, false);
			factorsData_8_29.put(3, true);
			factorsData_8_29.put(4, true);
			factorsData_8_29.put(5, true);
			factorsData_8_29.put(6, false);
			factorsData_8_29.put(7, true);
			factorsData_8_29.put(8, false);
			catalouge.put(29, factorsData_8_29);
			
			HashMap<Integer, Boolean> factorsData_8_30 = new HashMap<Integer, Boolean>();
			factorsData_8_30.put(1, true);
			factorsData_8_30.put(2, false);
			factorsData_8_30.put(3, true);
			factorsData_8_30.put(4, true);
			factorsData_8_30.put(5, true);
			factorsData_8_30.put(6, false);
			factorsData_8_30.put(7, false);
			factorsData_8_30.put(8, true);
			catalouge.put(30, factorsData_8_30);
			
			HashMap<Integer, Boolean> factorsData_8_31 = new HashMap<Integer, Boolean>();
			factorsData_8_31.put(1, false);
			factorsData_8_31.put(2, true);
			factorsData_8_31.put(3, true);
			factorsData_8_31.put(4, true);
			factorsData_8_31.put(5, true);
			factorsData_8_31.put(6, false);
			factorsData_8_31.put(7, false);
			factorsData_8_31.put(8, true);
			catalouge.put(31, factorsData_8_31);
			
			HashMap<Integer, Boolean> factorsData_8_32 = new HashMap<Integer, Boolean>();
			factorsData_8_32.put(1, true);
			factorsData_8_32.put(2, true);
			factorsData_8_32.put(3, true);
			factorsData_8_32.put(4, true);
			factorsData_8_32.put(5, true);
			factorsData_8_32.put(6, false);
			factorsData_8_32.put(7, true);
			factorsData_8_32.put(8, false);
			catalouge.put(32, factorsData_8_32);
			
			HashMap<Integer, Boolean> factorsData_8_33 = new HashMap<Integer, Boolean>();
			factorsData_8_33.put(1, false);
			factorsData_8_33.put(2, false);
			factorsData_8_33.put(3, false);
			factorsData_8_33.put(4, false);
			factorsData_8_33.put(5, false);
			factorsData_8_33.put(6, true);
			factorsData_8_33.put(7, true);
			factorsData_8_33.put(8, false);
			catalouge.put(33, factorsData_8_33);
			
			HashMap<Integer, Boolean> factorsData_8_34 = new HashMap<Integer, Boolean>();
			factorsData_8_34.put(1, true);
			factorsData_8_34.put(2, false);
			factorsData_8_34.put(3, false);
			factorsData_8_34.put(4, false);
			factorsData_8_34.put(5, false);
			factorsData_8_34.put(6, true);
			factorsData_8_34.put(7, false);
			factorsData_8_34.put(8, true);
			catalouge.put(34, factorsData_8_34);
			
			HashMap<Integer, Boolean> factorsData_8_35 = new HashMap<Integer, Boolean>();
			factorsData_8_35.put(1, false);
			factorsData_8_35.put(2, true);
			factorsData_8_35.put(3, false);
			factorsData_8_35.put(4, false);
			factorsData_8_35.put(5, false);
			factorsData_8_35.put(6, true);
			factorsData_8_35.put(7, false);
			factorsData_8_35.put(8, true);
			catalouge.put(35, factorsData_8_35);
			
			HashMap<Integer, Boolean> factorsData_8_36 = new HashMap<Integer, Boolean>();
			factorsData_8_36.put(1, true);
			factorsData_8_36.put(2, true);
			factorsData_8_36.put(3, false);
			factorsData_8_36.put(4, false);
			factorsData_8_36.put(5, false);
			factorsData_8_36.put(6, true);
			factorsData_8_36.put(7, true);
			factorsData_8_36.put(8, false);
			catalouge.put(36, factorsData_8_36);
			
			HashMap<Integer, Boolean> factorsData_8_37 = new HashMap<Integer, Boolean>();
			factorsData_8_37.put(1, false);
			factorsData_8_37.put(2, false);
			factorsData_8_37.put(3, true);
			factorsData_8_37.put(4, false);
			factorsData_8_37.put(5, false);
			factorsData_8_37.put(6, true);
			factorsData_8_37.put(7, false);
			factorsData_8_37.put(8, false);
			catalouge.put(37, factorsData_8_37);
			
			HashMap<Integer, Boolean> factorsData_8_38 = new HashMap<Integer, Boolean>();
			factorsData_8_38.put(1, true);
			factorsData_8_38.put(2, false);
			factorsData_8_38.put(3, true);
			factorsData_8_38.put(4, false);
			factorsData_8_38.put(5, false);
			factorsData_8_38.put(6, true);
			factorsData_8_38.put(7, true);
			factorsData_8_38.put(8, true);
			catalouge.put(38, factorsData_8_38);
			
			HashMap<Integer, Boolean> factorsData_8_39 = new HashMap<Integer, Boolean>();
			factorsData_8_39.put(1, false);
			factorsData_8_39.put(2, true);
			factorsData_8_39.put(3, true);
			factorsData_8_39.put(4, false);
			factorsData_8_39.put(5, false);
			factorsData_8_39.put(6, true);
			factorsData_8_39.put(7, true);
			factorsData_8_39.put(8, true);
			catalouge.put(39, factorsData_8_39);
			
			HashMap<Integer, Boolean> factorsData_8_40 = new HashMap<Integer, Boolean>();
			factorsData_8_40.put(1, true);
			factorsData_8_40.put(2, true);
			factorsData_8_40.put(3, true);
			factorsData_8_40.put(4, false);
			factorsData_8_40.put(5, false);
			factorsData_8_40.put(6, true);
			factorsData_8_40.put(7, false);
			factorsData_8_40.put(8, false);
			catalouge.put(40, factorsData_8_40);
			
			HashMap<Integer, Boolean> factorsData_8_41 = new HashMap<Integer, Boolean>();
			factorsData_8_41.put(1, false);
			factorsData_8_41.put(2, false);
			factorsData_8_41.put(3, false);
			factorsData_8_41.put(4, true);
			factorsData_8_41.put(5, false);
			factorsData_8_41.put(6, true);
			factorsData_8_41.put(7, false);
			factorsData_8_41.put(8, false);
			catalouge.put(41, factorsData_8_41);
			
			HashMap<Integer, Boolean> factorsData_8_42 = new HashMap<Integer, Boolean>();
			factorsData_8_42.put(1, true);
			factorsData_8_42.put(2, false);
			factorsData_8_42.put(3, false);
			factorsData_8_42.put(4, true);
			factorsData_8_42.put(5, false);
			factorsData_8_42.put(6, true);
			factorsData_8_42.put(7, true);
			factorsData_8_42.put(8, true);
			catalouge.put(42, factorsData_8_42);
			
			HashMap<Integer, Boolean> factorsData_8_43 = new HashMap<Integer, Boolean>();
			factorsData_8_43.put(1, false);
			factorsData_8_43.put(2, true);
			factorsData_8_43.put(3, false);
			factorsData_8_43.put(4, true);
			factorsData_8_43.put(5, false);
			factorsData_8_43.put(6, true);
			factorsData_8_43.put(7, true);
			factorsData_8_43.put(8, true);
			catalouge.put(43, factorsData_8_43);
			
			HashMap<Integer, Boolean> factorsData_8_44 = new HashMap<Integer, Boolean>();
			factorsData_8_44.put(1, true);
			factorsData_8_44.put(2, true);
			factorsData_8_44.put(3, false);
			factorsData_8_44.put(4, true);
			factorsData_8_44.put(5, false);
			factorsData_8_44.put(6, true);
			factorsData_8_44.put(7, false);
			factorsData_8_44.put(8, false);
			catalouge.put(44, factorsData_8_44);
			
			HashMap<Integer, Boolean> factorsData_8_45 = new HashMap<Integer, Boolean>();
			factorsData_8_45.put(1, false);
			factorsData_8_45.put(2, false);
			factorsData_8_45.put(3, true);
			factorsData_8_45.put(4, true);
			factorsData_8_45.put(5, false);
			factorsData_8_45.put(6, true);
			factorsData_8_45.put(7, true);
			factorsData_8_45.put(8, false);
			catalouge.put(45, factorsData_8_45);
			
			HashMap<Integer, Boolean> factorsData_8_46 = new HashMap<Integer, Boolean>();
			factorsData_8_46.put(1, true);
			factorsData_8_46.put(2, false);
			factorsData_8_46.put(3, true);
			factorsData_8_46.put(4, true);
			factorsData_8_46.put(5, false);
			factorsData_8_46.put(6, true);
			factorsData_8_46.put(7, false);
			factorsData_8_46.put(8, true);
			catalouge.put(46, factorsData_8_46);
			
			HashMap<Integer, Boolean> factorsData_8_47 = new HashMap<Integer, Boolean>();
			factorsData_8_47.put(1, false);
			factorsData_8_47.put(2, true);
			factorsData_8_47.put(3, true);
			factorsData_8_47.put(4, true);
			factorsData_8_47.put(5, false);
			factorsData_8_47.put(6, true);
			factorsData_8_47.put(7, false);
			factorsData_8_47.put(8, true);
			catalouge.put(47, factorsData_8_47);
			
			HashMap<Integer, Boolean> factorsData_8_48 = new HashMap<Integer, Boolean>();
			factorsData_8_48.put(1, true);
			factorsData_8_48.put(2, true);
			factorsData_8_48.put(3, true);
			factorsData_8_48.put(4, true);
			factorsData_8_48.put(5, false);
			factorsData_8_48.put(6, true);
			factorsData_8_48.put(7, true);
			factorsData_8_48.put(8, false);
			catalouge.put(48, factorsData_8_48);
			
			HashMap<Integer, Boolean> factorsData_8_49 = new HashMap<Integer, Boolean>();
			factorsData_8_49.put(1, false);
			factorsData_8_49.put(2, false);
			factorsData_8_49.put(3, false);
			factorsData_8_49.put(4, false);
			factorsData_8_49.put(5, true);
			factorsData_8_49.put(6, true);
			factorsData_8_49.put(7, true);
			factorsData_8_49.put(8, true);
			catalouge.put(49, factorsData_8_49);
			
			HashMap<Integer, Boolean> factorsData_8_50 = new HashMap<Integer, Boolean>();
			factorsData_8_50.put(1, true);
			factorsData_8_50.put(2, false);
			factorsData_8_50.put(3, false);
			factorsData_8_50.put(4, false);
			factorsData_8_50.put(5, true);
			factorsData_8_50.put(6, true);
			factorsData_8_50.put(7, false);
			factorsData_8_50.put(8, false);
			catalouge.put(50, factorsData_8_50);
			
			HashMap<Integer, Boolean> factorsData_8_51 = new HashMap<Integer, Boolean>();
			factorsData_8_51.put(1, false);
			factorsData_8_51.put(2, true);
			factorsData_8_51.put(3, false);
			factorsData_8_51.put(4, false);
			factorsData_8_51.put(5, true);
			factorsData_8_51.put(6, true);
			factorsData_8_51.put(7, false);
			factorsData_8_51.put(8, false);
			catalouge.put(51, factorsData_8_51);
			
			HashMap<Integer, Boolean> factorsData_8_52 = new HashMap<Integer, Boolean>();
			factorsData_8_52.put(1, true);
			factorsData_8_52.put(2, true);
			factorsData_8_52.put(3, false);
			factorsData_8_52.put(4, false);
			factorsData_8_52.put(5, true);
			factorsData_8_52.put(6, true);
			factorsData_8_52.put(7, true);
			factorsData_8_52.put(8, true);
			catalouge.put(52, factorsData_8_52);
			
			HashMap<Integer, Boolean> factorsData_8_53 = new HashMap<Integer, Boolean>();
			factorsData_8_53.put(1, false);
			factorsData_8_53.put(2, false);
			factorsData_8_53.put(3, true);
			factorsData_8_53.put(4, false);
			factorsData_8_53.put(5, true);
			factorsData_8_53.put(6, true);
			factorsData_8_53.put(7, false);
			factorsData_8_53.put(8, true);
			catalouge.put(53, factorsData_8_53);
			
			HashMap<Integer, Boolean> factorsData_8_54 = new HashMap<Integer, Boolean>();
			factorsData_8_54.put(1, true);
			factorsData_8_54.put(2, false);
			factorsData_8_54.put(3, true);
			factorsData_8_54.put(4, false);
			factorsData_8_54.put(5, true);
			factorsData_8_54.put(6, true);
			factorsData_8_54.put(7, true);
			factorsData_8_54.put(8, false);
			catalouge.put(54, factorsData_8_54);
			
			HashMap<Integer, Boolean> factorsData_8_55 = new HashMap<Integer, Boolean>();
			factorsData_8_55.put(1, false);
			factorsData_8_55.put(2, true);
			factorsData_8_55.put(3, true);
			factorsData_8_55.put(4, false);
			factorsData_8_55.put(5, true);
			factorsData_8_55.put(6, true);
			factorsData_8_55.put(7, true);
			factorsData_8_55.put(8, false);
			catalouge.put(55, factorsData_8_55);
			
			HashMap<Integer, Boolean> factorsData_8_56 = new HashMap<Integer, Boolean>();
			factorsData_8_56.put(1, true);
			factorsData_8_56.put(2, true);
			factorsData_8_56.put(3, true);
			factorsData_8_56.put(4, false);
			factorsData_8_56.put(5, true);
			factorsData_8_56.put(6, true);
			factorsData_8_56.put(7, false);
			factorsData_8_56.put(8, true);
			catalouge.put(56, factorsData_8_56);
			
			HashMap<Integer, Boolean> factorsData_8_57 = new HashMap<Integer, Boolean>();
			factorsData_8_57.put(1, false);
			factorsData_8_57.put(2, false);
			factorsData_8_57.put(3, false);
			factorsData_8_57.put(4, true);
			factorsData_8_57.put(5, true);
			factorsData_8_57.put(6, true);
			factorsData_8_57.put(7, false);
			factorsData_8_57.put(8, true);
			catalouge.put(57, factorsData_8_57);
			
			HashMap<Integer, Boolean> factorsData_8_58 = new HashMap<Integer, Boolean>();
			factorsData_8_58.put(1, true);
			factorsData_8_58.put(2, false);
			factorsData_8_58.put(3, false);
			factorsData_8_58.put(4, true);
			factorsData_8_58.put(5, true);
			factorsData_8_58.put(6, true);
			factorsData_8_58.put(7, true);
			factorsData_8_58.put(8, false);
			catalouge.put(58, factorsData_8_58);
			
			HashMap<Integer, Boolean> factorsData_8_59 = new HashMap<Integer, Boolean>();
			factorsData_8_59.put(1, false);
			factorsData_8_59.put(2, true);
			factorsData_8_59.put(3, false);
			factorsData_8_59.put(4, true);
			factorsData_8_59.put(5, true);
			factorsData_8_59.put(6, true);
			factorsData_8_59.put(7, true);
			factorsData_8_59.put(8, false);
			catalouge.put(59, factorsData_8_59);
			
			HashMap<Integer, Boolean> factorsData_8_60 = new HashMap<Integer, Boolean>();
			factorsData_8_60.put(1, true);
			factorsData_8_60.put(2, true);
			factorsData_8_60.put(3, false);
			factorsData_8_60.put(4, true);
			factorsData_8_60.put(5, true);
			factorsData_8_60.put(6, true);
			factorsData_8_60.put(7, false);
			factorsData_8_60.put(8, true);
			catalouge.put(60, factorsData_8_60);
			
			HashMap<Integer, Boolean> factorsData_8_61 = new HashMap<Integer, Boolean>();
			factorsData_8_61.put(1, false);
			factorsData_8_61.put(2, false);
			factorsData_8_61.put(3, true);
			factorsData_8_61.put(4, true);
			factorsData_8_61.put(5, true);
			factorsData_8_61.put(6, true);
			factorsData_8_61.put(7, true);
			factorsData_8_61.put(8, true);
			catalouge.put(61, factorsData_8_61);
			
			HashMap<Integer, Boolean> factorsData_8_62 = new HashMap<Integer, Boolean>();
			factorsData_8_62.put(1, true);
			factorsData_8_62.put(2, false);
			factorsData_8_62.put(3, true);
			factorsData_8_62.put(4, true);
			factorsData_8_62.put(5, true);
			factorsData_8_62.put(6, true);
			factorsData_8_62.put(7, false);
			factorsData_8_62.put(8, false);
			catalouge.put(62, factorsData_8_62);
			
			HashMap<Integer, Boolean> factorsData_8_63 = new HashMap<Integer, Boolean>();
			factorsData_8_63.put(1, false);
			factorsData_8_63.put(2, true);
			factorsData_8_63.put(3, true);
			factorsData_8_63.put(4, true);
			factorsData_8_63.put(5, true);
			factorsData_8_63.put(6, true);
			factorsData_8_63.put(7, false);
			factorsData_8_63.put(8, false);
			catalouge.put(63, factorsData_8_63);
			
			HashMap<Integer, Boolean> factorsData_8_64 = new HashMap<Integer, Boolean>();
			factorsData_8_64.put(1, true);
			factorsData_8_64.put(2, true);
			factorsData_8_64.put(3, true);
			factorsData_8_64.put(4, true);
			factorsData_8_64.put(5, true);
			factorsData_8_64.put(6, true);
			factorsData_8_64.put(7, true);
			factorsData_8_64.put(8, true);
			catalouge.put(64, factorsData_8_64);
			
			break;

		case 9:
			
			double countFact1_9 = 1; double countFact2_9 = 1; double countFact3_9 = 1; double countFact4_9 = 1;
			double countFact5_9 = 1; double countFact6_9 = 1; double countFact7_9 = 1;
			
			for(int i = 1; i <= 128; i++)
			{
				HashMap<Integer, Boolean> factorsData = new HashMap<Integer, Boolean>();
				
				//Factor 1 settings
				if(countFact1_9 == 1)
				{
					factorsData.put(1, false);
					countFact1_9--;
				}
				else
				{
					factorsData.put(1, true);
					countFact1_9++;
				}
				
				//Factor 2 settings
				if(countFact2_9 <= 2)
				{
					factorsData.put(2, false);
					countFact2_9++;
				}
				else
				{
					factorsData.put(2, true);
					
					if(countFact2_9 == 4) countFact2_9 = 1;
					else countFact2_9++;
				}
				
				//Factor 3 settings
				if(countFact3_9 <= 4)
				{
					factorsData.put(3, false);
					countFact3_9++;
				}
				else
				{
					factorsData.put(3, true);
					
					if(countFact3_9 == 8) countFact3_9 = 1;
					else countFact3_9++;
				}
				
				//Factor 4 settings
				if(countFact4_9 <= 8)
				{
					factorsData.put(4, false);
					countFact4_9++;
				}
				else
				{
					factorsData.put(4, true);
					
					if(countFact4_9 == 16) countFact4_9 = 1;
					else countFact4_9++;
				}
				
				//Factor 5 settings
				if(countFact5_9 <= 16)
				{
					factorsData.put(5, false);
					countFact5_9++;
				}
				else
				{
					factorsData.put(5, true);
					
					if(countFact5_9 == 32) countFact5_9 = 1;
					else countFact5_9++;
				}
				
				//Factor 6 settings
				if(countFact6_9 <= 32)
				{
					factorsData.put(6, false);
					countFact6_9++;
				}
				else
				{
					factorsData.put(6, true);
					
					if(countFact6_9 == 64) countFact6_9 = 1;
					else countFact6_9++;
				}
				
				//Factor 7 settings
				if(countFact7_9 <= 64)
				{
					factorsData.put(7, false);
					countFact7_9++;
				}
				else
				{
					factorsData.put(7, true);
					
					if(countFact7_9 == 128) countFact7_9 = 1;
					else countFact7_9++;
				}
				
				//Factor 8 settings

				int countFact8_9 = ((i-1) % 8)+1;
				
				if(i <= 8 || (i > 16 && i <= 24) || (i > 40 && i <= 48) || (i > 56 && i <= 64) || (i > 72 && i <= 80) || (i > 88 && i <= 104) || (i > 112 && i <= 120) )
				{
					switch(countFact8_9){
					
					case 1:
	
						factorsData.put(8, false);
						break;
						
					case 2:
						
						factorsData.put(8, true);
						break;
						
					case 3:

						factorsData.put(8, false);
						break;
						
					case 4:

						factorsData.put(8, true);
						break;
						
					case 5:
						
						factorsData.put(8, true);
						break;
						
					case 6:

						factorsData.put(8, false);
						break;
						
					case 7:

						factorsData.put(8, true);
						break;
						
					case 8:
	
						factorsData.put(8, false);
						break;					
					}
				}
				else
				{
				
					switch(countFact8_9){
					
					case 1:
	
						factorsData.put(8, true);
						break;
						
					case 2:
						
						factorsData.put(8, false);
						break;
						
					case 3:

						factorsData.put(8, true);
						break;
						
					case 4:

						factorsData.put(8, false);
						break;
						
					case 5:
						
						factorsData.put(8, false);
						break;
						
					case 6:

						factorsData.put(8, true);
						break;
						
					case 7:

						factorsData.put(8, false);
						break;
						
					case 8:
	
						factorsData.put(8, true);
						break;					
					}
					
				}
				
				//Factor 9 settings

				int countFact9_9 = ((i-1) % 8)+1;
				
				if(i <= 16 || (i > 48 && i <= 64) || (i > 80 && i <= 112))
				{
					switch(countFact9_9){
					
					case 1:
	
						factorsData.put(9, false);
						break;
						
					case 2:
						
						factorsData.put(9, false);
						break;
						
					case 3:

						factorsData.put(9, true);
						break;
						
					case 4:

						factorsData.put(9, true);
						break;
						
					case 5:
						
						factorsData.put(9, true);
						break;
						
					case 6:

						factorsData.put(9, true);
						break;
						
					case 7:

						factorsData.put(9, false);
						break;
						
					case 8:
	
						factorsData.put(9, false);
						break;					
					}
				}
				else
				{
				
			switch(countFact9_9){
					
					case 1:
	
						factorsData.put(9, true);
						break;
						
					case 2:
						
						factorsData.put(9, true);
						break;
						
					case 3:

						factorsData.put(9, false);
						break;
						
					case 4:

						factorsData.put(9, false);
						break;
						
					case 5:
						
						factorsData.put(9, false);
						break;
						
					case 6:

						factorsData.put(9, false);
						break;
						
					case 7:

						factorsData.put(9, true);
						break;
						
					case 8:
	
						factorsData.put(9, true);
						break;					
					}
				}
				
				catalouge.put(i, factorsData);
			}
	
			break;
			
	case 10:
			
			double countFact1_10 = 1; double countFact2_10 = 1; double countFact3_10 = 1; double countFact4_10 = 1;
			double countFact5_10 = 1; double countFact6_10 = 1; double countFact7_10 = 1;
			
			for(int i = 1; i <= 128; i++)
			{
				HashMap<Integer, Boolean> factorsData = new HashMap<Integer, Boolean>();
				
				//Factor 1 settings
				if(countFact1_10 == 1)
				{
					factorsData.put(1, false);
					countFact1_10--;
				}
				else
				{
					factorsData.put(1, true);
					countFact1_10++;
				}
				
				//Factor 2 settings
				if(countFact2_10 <= 2)
				{
					factorsData.put(2, false);
					countFact2_10++;
				}
				else
				{
					factorsData.put(2, true);
					
					if(countFact2_10 == 4) countFact2_10 = 1;
					else countFact2_10++;
				}
				
				//Factor 3 settings
				if(countFact3_10 <= 4)
				{
					factorsData.put(3, false);
					countFact3_10++;
				}
				else
				{
					factorsData.put(3, true);
					
					if(countFact3_10 == 8) countFact3_10 = 1;
					else countFact3_10++;
				}
				
				//Factor 4 settings
				if(countFact4_10 <= 8)
				{
					factorsData.put(4, false);
					countFact4_10++;
				}
				else
				{
					factorsData.put(4, true);
					
					if(countFact4_10 == 16) countFact4_10 = 1;
					else countFact4_10++;
				}
				
				//Factor 5 settings
				if(countFact5_10 <= 16)
				{
					factorsData.put(5, false);
					countFact5_10++;
				}
				else
				{
					factorsData.put(5, true);
					
					if(countFact5_10 == 32) countFact5_10 = 1;
					else countFact5_10++;
				}
				
				//Factor 6 settings
				if(countFact6_10 <= 32)
				{
					factorsData.put(6, false);
					countFact6_10++;
				}
				else
				{
					factorsData.put(6, true);
					
					if(countFact6_10 == 64) countFact6_10 = 1;
					else countFact6_10++;
				}
				
				//Factor 7 settings
				if(countFact7_10 <= 64)
				{
					factorsData.put(7, false);
					countFact7_10++;
				}
				else
				{
					factorsData.put(7, true);
					
					if(countFact7_10 == 128) countFact7_10 = 1;
					else countFact7_10++;
				}
				
				//Factor 8 settings

				int countFact8_10 = ((i-1) % 8)+1;
				
				switch(countFact8_10){
				
				case 1:
					if(i <= 64)
						factorsData.put(8, true);
					else
						factorsData.put(8, false);
					break;
					
				case 2:
					if(i <= 64)
						factorsData.put(8, false);
					else
						factorsData.put(8, true);
					break;
					
				case 3:
					if(i <= 64)
						factorsData.put(8, false);
					else
						factorsData.put(8, true);
					break;
					
				case 4:
					if(i <= 64)
						factorsData.put(8, true);
					else
						factorsData.put(8, false);
					break;
					
				case 5:
					if(i <= 64)
						factorsData.put(8, false);
					else
						factorsData.put(8, true);
					break;
					
				case 6:
					if(i <= 64)
						factorsData.put(8, true);
					else
						factorsData.put(8, false);
					break;
					
				case 7:
					if(i <= 64)
						factorsData.put(8, true);
					else
						factorsData.put(8, false);
					break;
					
				case 8:
					if(i <= 64)
						factorsData.put(8, false);
					else
						factorsData.put(8, true);
					break;					
				}
				
				//Factor 9 settings

				int countFact9_10 = ((i-1) % 32)+1;
				
				switch(countFact9_10){
				
				case 1:
					factorsData.put(9, true);
					break;
					
				case 2:
					factorsData.put(9, true);
					break;
					
				case 3:
					factorsData.put(9, false);
					break;
					
				case 4:
					factorsData.put(9, false);
					break;
					
				case 5:
					factorsData.put(9, false);
					break;
					
				case 6:
					factorsData.put(9, false);
					break;
					
				case 7:
					factorsData.put(9, true);
					break;
					
				case 8:
					factorsData.put(9, true);
					break;					
					
				case 9:
					factorsData.put(9, false);
					break;
					
				case 10:
					factorsData.put(9, false);
					break;
					
				case 11:
					factorsData.put(9, true);
					break;
					
				case 12:
					factorsData.put(9, true);
					break;					
					
				case 13:
					factorsData.put(9, true);
					break;
					
				case 14:
					factorsData.put(9, true);
					break;
					
				case 15:
					factorsData.put(9, false);
					break;
					
				case 16:
					factorsData.put(9, false);
					break;
					
				case 17:
					factorsData.put(9, false);
					break;
					
				case 18:
					factorsData.put(9, false);
					break;
					
				case 19:
					factorsData.put(9, true);
					break;
					
				case 20:
					factorsData.put(9, true);
					break;					
					
				case 21:
					factorsData.put(9, true);
					break;
					
				case 22:
					factorsData.put(9, true);
					break;
					
				case 23:
					factorsData.put(9, false);
					break;
					
				case 24:
					factorsData.put(9, false);
					break;
					
				case 25:
					factorsData.put(9, true);
					break;
					
				case 26:
					factorsData.put(9, true);
					break;
					
				case 27:
					factorsData.put(9, false);
					break;
					
				case 28:
					factorsData.put(9, false);
					break;
					
				case 29:
					factorsData.put(9, false);
					break;
					
				case 30:
					factorsData.put(9, false);
					break;
					
				case 31:
					factorsData.put(9, true);
					break;
					
				case 32:
					factorsData.put(9, true);
					break;				
					
				}
				
				//Factor 10 settings

				int countFact10_10 = ((i-1) % 16)+1;
				
				switch(countFact10_10){
				
				case 1:
					if( i <= 32 || (i > 64 && i <= 96) )
						factorsData.put(10, true);
					else
						factorsData.put(10, false);
					break;
					
				case 2:
					if( i <= 32 || (i > 64 && i <= 96) )
						factorsData.put(10, false);
					else
						factorsData.put(10, true);
					break;
					
				case 3:
					if( i <= 32 || (i > 64 && i <= 96) )
						factorsData.put(10, true);
					else
						factorsData.put(10, false);
					break;
					
				case 4:
					if( i <= 32 || (i > 64 && i <= 96) )
						factorsData.put(10, false);
					else
						factorsData.put(10, true);
					break;
					
				case 5:
					if( i <= 32 || (i > 64 && i <= 96) )
						factorsData.put(10, false);
					else
						factorsData.put(10, true);
					break;
					
				case 6:
					if( i <= 32 || (i > 64 && i <= 96) )
						factorsData.put(10, true);
					else
						factorsData.put(10, false);
					break;
					
				case 7:
					if( i <= 32 || (i > 64 && i <= 96) )
						factorsData.put(10, false);
					else
						factorsData.put(10, true);
					break;
					
				case 8:
					if( i <= 32 || (i > 64 && i <= 96) )
						factorsData.put(10, true);
					else
						factorsData.put(10, false);
					break;					
					
				case 9:
					if( i <= 32 || (i > 64 && i <= 96) )
						factorsData.put(10, false);
					else
						factorsData.put(10, true);
					break;
					
				case 10:
					if( i <= 32 || (i > 64 && i <= 96) )
						factorsData.put(10, true);
					else
						factorsData.put(10, false);
					break;
					
				case 11:
					if( i <= 32 || (i > 64 && i <= 96) )
						factorsData.put(10, false);
					else
						factorsData.put(10, true);
					break;
					
				case 12:
					if( i <= 32 || (i > 64 && i <= 96) )
						factorsData.put(10, true);
					else
						factorsData.put(10, false);
					break;
					
				case 13:
					if( i <= 32 || (i > 64 && i <= 96) )
						factorsData.put(10, true);
					else
						factorsData.put(10, false);
					break;
					
				case 14:
					if( i <= 32 || (i > 64 && i <= 96) )
						factorsData.put(10, false);
					else
						factorsData.put(10, true);
					break;
					
				case 15:
					if( i <= 32 || (i > 64 && i <= 96) )
						factorsData.put(10, true);
					else
						factorsData.put(10, false);
					break;
					
				case 16:
					if( i <= 32 || (i > 64 && i <= 96) )
						factorsData.put(10, false);
					else
						factorsData.put(10, true);
					break;
				}
				
				catalouge.put(i, factorsData);
			}
			
			break;
			
	case 11:
		
		double countFact1_11 = 1; double countFact2_11 = 1; double countFact3_11 = 1; double countFact4_11 = 1;
		double countFact5_11 = 1; double countFact6_11 = 1; double countFact7_11 = 1;
		
		for(int i = 1; i <= 128; i++)
		{
			HashMap<Integer, Boolean> factorsData = new HashMap<Integer, Boolean>();
			
			//Factor 1 settings
			if(countFact1_11 == 1)
			{
				factorsData.put(1, false);
				countFact1_11--;
			}
			else
			{
				factorsData.put(1, true);
				countFact1_11++;
			}
			
			//Factor 2 settings
			if(countFact2_11 <= 2)
			{
				factorsData.put(2, false);
				countFact2_11++;
			}
			else
			{
				factorsData.put(2, true);
				
				if(countFact2_11 == 4) countFact2_11 = 1;
				else countFact2_11++;
			}
			
			//Factor 3 settings
			if(countFact3_11 <= 4)
			{
				factorsData.put(3, false);
				countFact3_11++;
			}
			else
			{
				factorsData.put(3, true);
				
				if(countFact3_11 == 8) countFact3_11 = 1;
				else countFact3_11++;
			}
			
			//Factor 4 settings
			if(countFact4_11 <= 8)
			{
				factorsData.put(4, false);
				countFact4_11++;
			}
			else
			{
				factorsData.put(4, true);
				
				if(countFact4_11 == 16) countFact4_11 = 1;
				else countFact4_11++;
			}
			
			//Factor 5 settings
			if(countFact5_11 <= 16)
			{
				factorsData.put(5, false);
				countFact5_11++;
			}
			else
			{
				factorsData.put(5, true);
				
				if(countFact5_11 == 32) countFact5_11 = 1;
				else countFact5_11++;
			}
			
			//Factor 6 settings
			if(countFact6_11 <= 32)
			{
				factorsData.put(6, false);
				countFact6_11++;
			}
			else
			{
				factorsData.put(6, true);
				
				if(countFact6_11 == 64) countFact6_11 = 1;
				else countFact6_11++;
			}
			
			//Factor 7 settings
			if(countFact7_11 <= 64)
			{
				factorsData.put(7, false);
				countFact7_11++;
			}
			else
			{
				factorsData.put(7, true);
				
				if(countFact7_11 == 128) countFact7_11 = 1;
				else countFact7_11++;
			}
			
			//Factor 8 settings

			int countFact8_11 = ((i-1) % 8)+1;
			
			switch(countFact8_11){
			
			case 1:
				if(i <= 64)
					factorsData.put(8, true);
				else
					factorsData.put(8, false);
				break;
				
			case 2:
				if(i <= 64)
					factorsData.put(8, false);
				else
					factorsData.put(8, true);
				break;
				
			case 3:
				if(i <= 64)
					factorsData.put(8, false);
				else
					factorsData.put(8, true);
				break;
				
			case 4:
				if(i <= 64)
					factorsData.put(8, true);
				else
					factorsData.put(8, false);
				break;
				
			case 5:
				if(i <= 64)
					factorsData.put(8, false);
				else
					factorsData.put(8, true);
				break;
				
			case 6:
				if(i <= 64)
					factorsData.put(8, true);
				else
					factorsData.put(8, false);
				break;
				
			case 7:
				if(i <= 64)
					factorsData.put(8, true);
				else
					factorsData.put(8, false);
				break;
				
			case 8:
				if(i <= 64)
					factorsData.put(8, false);
				else
					factorsData.put(8, true);
				break;					
			}
			
			//Factor 9 settings

			int countFact9_11 = ((i-1) % 32)+1;
			
			switch(countFact9_11){
			
			case 1:
				factorsData.put(9, true);
				break;
				
			case 2:
				factorsData.put(9, true);
				break;
				
			case 3:
				factorsData.put(9, false);
				break;
				
			case 4:
				factorsData.put(9, false);
				break;
				
			case 5:
				factorsData.put(9, false);
				break;
				
			case 6:
				factorsData.put(9, false);
				break;
				
			case 7:
				factorsData.put(9, true);
				break;
				
			case 8:
				factorsData.put(9, true);
				break;					
				
			case 9:
				factorsData.put(9, false);
				break;
				
			case 10:
				factorsData.put(9, false);
				break;
				
			case 11:
				factorsData.put(9, true);
				break;
				
			case 12:
				factorsData.put(9, true);
				break;					
				
			case 13:
				factorsData.put(9, true);
				break;
				
			case 14:
				factorsData.put(9, true);
				break;
				
			case 15:
				factorsData.put(9, false);
				break;
				
			case 16:
				factorsData.put(9, false);
				break;
				
			case 17:
				factorsData.put(9, false);
				break;
				
			case 18:
				factorsData.put(9, false);
				break;
				
			case 19:
				factorsData.put(9, true);
				break;
				
			case 20:
				factorsData.put(9, true);
				break;					
				
			case 21:
				factorsData.put(9, true);
				break;
				
			case 22:
				factorsData.put(9, true);
				break;
				
			case 23:
				factorsData.put(9, false);
				break;
				
			case 24:
				factorsData.put(9, false);
				break;
				
			case 25:
				factorsData.put(9, true);
				break;
				
			case 26:
				factorsData.put(9, true);
				break;
				
			case 27:
				factorsData.put(9, false);
				break;
				
			case 28:
				factorsData.put(9, false);
				break;
				
			case 29:
				factorsData.put(9, false);
				break;
				
			case 30:
				factorsData.put(9, false);
				break;
				
			case 31:
				factorsData.put(9, true);
				break;
				
			case 32:
				factorsData.put(9, true);
				break;				
				
			}
			
			//Factor 10 settings

			int countFact10_11 = ((i-1) % 16)+1;
			
			switch(countFact10_11){
			
			case 1:
				if( i <= 32 || (i > 64 && i <= 96) )
					factorsData.put(10, true);
				else
					factorsData.put(10, false);
				break;
				
			case 2:
				if( i <= 32 || (i > 64 && i <= 96) )
					factorsData.put(10, false);
				else
					factorsData.put(10, true);
				break;
				
			case 3:
				if( i <= 32 || (i > 64 && i <= 96) )
					factorsData.put(10, true);
				else
					factorsData.put(10, false);
				break;
				
			case 4:
				if( i <= 32 || (i > 64 && i <= 96) )
					factorsData.put(10, false);
				else
					factorsData.put(10, true);
				break;
				
			case 5:
				if( i <= 32 || (i > 64 && i <= 96) )
					factorsData.put(10, false);
				else
					factorsData.put(10, true);
				break;
				
			case 6:
				if( i <= 32 || (i > 64 && i <= 96) )
					factorsData.put(10, true);
				else
					factorsData.put(10, false);
				break;
				
			case 7:
				if( i <= 32 || (i > 64 && i <= 96) )
					factorsData.put(10, false);
				else
					factorsData.put(10, true);
				break;
				
			case 8:
				if( i <= 32 || (i > 64 && i <= 96) )
					factorsData.put(10, true);
				else
					factorsData.put(10, false);
				break;					
				
			case 9:
				if( i <= 32 || (i > 64 && i <= 96) )
					factorsData.put(10, false);
				else
					factorsData.put(10, true);
				break;
				
			case 10:
				if( i <= 32 || (i > 64 && i <= 96) )
					factorsData.put(10, true);
				else
					factorsData.put(10, false);
				break;
				
			case 11:
				if( i <= 32 || (i > 64 && i <= 96) )
					factorsData.put(10, false);
				else
					factorsData.put(10, true);
				break;
				
			case 12:
				if( i <= 32 || (i > 64 && i <= 96) )
					factorsData.put(10, true);
				else
					factorsData.put(10, false);
				break;
				
			case 13:
				if( i <= 32 || (i > 64 && i <= 96) )
					factorsData.put(10, true);
				else
					factorsData.put(10, false);
				break;
				
			case 14:
				if( i <= 32 || (i > 64 && i <= 96) )
					factorsData.put(10, false);
				else
					factorsData.put(10, true);
				break;
				
			case 15:
				if( i <= 32 || (i > 64 && i <= 96) )
					factorsData.put(10, true);
				else
					factorsData.put(10, false);
				break;
				
			case 16:
				if( i <= 32 || (i > 64 && i <= 96) )
					factorsData.put(10, false);
				else
					factorsData.put(10, true);
				break;
			}
			
			//Factor 11 settings

			int countFact11_11 = ((i-1) % 64)+1;
			
			switch(countFact11_11){
			
			case 1:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 2:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 3:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 4:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 5:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 6:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 7:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 8:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;					
				
			case 9:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 10:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 11:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 12:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 13:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 14:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 15:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 16:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 17:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 18:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 19:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 20:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 21:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 22:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 23:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 24:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 25:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 26:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 27:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 28:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;

			case 29:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 30:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 31:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 32:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 33:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 34:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 35:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 36:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 37:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 38:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 39:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 40:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;

			case 41:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 42:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 43:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 44:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;

			case 45:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 46:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 47:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 48:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 49:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 50:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 51:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 52:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 53:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 54:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 55:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 56:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 57:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 58:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 59:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 60:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
	
			case 61:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			case 62:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 63:
				if(i <= 64)
					factorsData.put(11, true);
				else
					factorsData.put(11, false);
				break;
				
			case 64:
				if(i <= 64)
					factorsData.put(11, false);
				else
					factorsData.put(11, true);
				break;
				
			}
				
			catalouge.put(i, factorsData);
		}
		
		break;
			
		default:
			System.out.println("Uncoded TrialsCatologe for Factors: "+factors);
			break;
		}
	}
	
}
