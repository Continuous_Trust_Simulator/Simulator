package doe;

public class Factor {
	
	private String factor;
	
	private int order;
	
	private double low;
	
	private double high;
	
	private double fixed;

	public String getFactor() {
		return factor;
	}

	public void setFactor(String factor) {
		this.factor = factor;
	}

	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public double getFixed() {
		return fixed;
	}

	public void setFixed(double fixed) {
		this.fixed = fixed;
	}

}
