package trust.simulator.rmgr.networkGenerator;

import peersim.config.FastConfig;
import peersim.core.CommonState;
import peersim.core.Linkable;
import peersim.core.Network;
import peersim.core.Node;
import peersim.graph.Graph;
import peersim.graph.GraphFactory;

public class RMGR_GraphFactory extends GraphFactory {

	private RMGR_GraphFactory() {super();}
	
	public static Graph wireRMGR(Graph g) {
		
		int networkSize = g.size();
	
    	if (CommonState.getTime() == 0)
    	{
    		for(int i = 3 ; i < networkSize; i++)
    		{
    			g.setEdge(1, i);
    		}
    		
    	}	
    	else
    	{
    		Node idp = Network.get(1);
    		//TODO: Find better way to know the linkable protocol ID!!
    		// It seems to be always the last protocol in the stack!!
    		Linkable linkable = (Linkable) idp.getProtocol( FastConfig.getLinkable(5) );
    		for(int i = linkable.degree(); i < networkSize; i++)
    		{
    			g.setEdge(1, i);
    		}
    	}
		return g;
	}
	
}
