package trust.simulator.rmgr.networkGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import doe.Factor;
import doe.TrialsCatalouge;

import peersim.config.Configuration;
import peersim.core.Control;
import peersim.core.Network;
import peersim.core.Node;
import trust.simulator.rmgr.protocol.auditor.Auditor_Node;
import trust.simulator.rmgr.protocol.idp.IDP_Node;
import trust.simulator.rmgr.protocol.me.ME_Node;
import trust.simulator.rmgr.protocol.sp.SP_Node;
import trust.simulator.rmgr.protocol.superNode.Super_Node;
import trust.simulator.rmgr.protocol.user.User_Node;

/**
 * <p>
 * This initialization class collects the simulation parameters from the config
 * file and generates uniformly random 2D-coordinates for each node. The
 * coordinates are distributed on a unit (1.0) square.
 * </p>
 * <p>
 * The first node in the {@link Network} is considered as the root node and its
 * coordinate is set to the center of the square.
 * </p>
 * 
 * 
 * @author Gian Paolo Jesi
 */
public class SimulationInitializer implements Control {
	// ------------------------------------------------------------------------
    // Parameters
    // ------------------------------------------------------------------------
	
	
	private static final String PAR_Experiment = "Experiment";
	private static final String PAR_Fixed = "Fixed";
	private static final String PAR_Factors = "Factors";
	
	private static final String PAR_RMGR_Node_PROT = "RMGR_Node";
	private static final String PAR_Super_Node_PROT = "Super_Node";
	private static final String PAR_IDP_Node_PROT = "IDP_Node";
    private static final String PAR_Auditor_Node_PROT = "Auditor_Node";
    private static final String PAR_User_Node_PROT = "User_Node";    
    private static final String PAR_SP_Node_PROT = "SP_Node";
    private static final String PAR_ME_Node_PROT = "ME_Node";
    
    private static final String PAR_SPRank = "SPRank";
    
    private static final String PAR_INI_Users = "INI_Users";
    private static final String PAR_UsersGR = "UsersGR";
    private static final String PAR_INI_PSPs = "INI_PSPs";
    private static final String PAR_PSPsGR = "PSPsGR";
    private static final String PAR_INI_SPs = "INI_SPs";
    private static final String PAR_SPsGR = "SPsGR";
    private static final String PAR_INI_MPSPs = "INI_MPSPs";
    private static final String PAR_MPSPsGR = "MPSPsGR";
    private static final String PAR_INI_MSPs = "INI_MSPs";
    private static final String PAR_MSPsGR = "MSPsGR";
    private static final String PAR_INI_ME = "INI_ME";
    private static final String PAR_MEGR = "MEGR";
    private static final String PAR_INI_ME_MPSPs = "INI_ME_MPSPs";
    private static final String PAR_INI_ME_MSPs = "INI_ME_MSPs";
    private static final String PAR_ME_MPSPsGR = "ME_MPSPsGR";
    private static final String PAR_ME_MSPsGR = "ME_MSPsGR";
    private static final String PAR_ME_MAX_MPSPs = "ME_MAX_MPSPs";
    private static final String PAR_ME_MAX_MSPs = "ME_MAX_MSPs";

    private static final String PAR_UsefulnessMIN = "usefulnessMIN";
    private static final String PAR_NewPartnershipProb = "newPartnershipProb";
    private static final String PAR_NewPartnershipDuration = "newPartnershipDuration";
    private static final String PAR_PartnerSharingProb = "partnerSharingProb";
    private static final String PAR_OfferDGUProb = "OfferDGUProb";
    private static final String PAR_OfferStrictDGUProb = "OfferStrictDGUProb";
    private static final String PAR_AcceptDGUProb = "AcceptDGUProb";
    private static final String PAR_AcceptStrictDGUProb = "AcceptStrictDGUProb";
    private static final String PAR_AcceptCompulsoryStrictDGUProb = "AcceptCompulsoryStrictDGUProb";
    private static final String PAR_AcceptCompulsoryDGUProb = "AcceptCompulsoryDGUProb";
    
    private static final String PAR_CredentialsArraySize = "CredentialsArraySize";    
	private static final String PAR_GenerateNewCredentialRate = "GenerateNewCredentialRate";
	private static final String PAR_GenerateNewServiceRequestRate = "GenerateNewServiceRequestRate";
	private static final String PAR_StrictCredentialProb = "StrictCredentialProb";
	private static final String PAR_IniCredentialType = "IniCredentialType";
	private static final String PAR_TrustInNetwork = "TrustInNetwork";
	private static final String PAR_IgnoranceRate = "IgnoranceRate";
	private static final String PAR_StopUsingCredentialAfterSpam = "StopUsingCredentialAfterSpam";
	private static final String PAR_TrustDropPostSpam = "TrustDropPostSpam";
	private static final String PAR_TrustIncreasePostSpamResolved = "TrustIncreasePostSpamResolved";
	private static final String PAR_TrustIncreaseRate = "TrustIncreaseRate";
	private static final String PAR_TrustDecreaseRate = "TrustDecreaseRate";
	private static final String PAR_TrustDecreasePeriodPostSpam = "TrustDecreasePeriodPostSpam";
	
	private static final String PAR_TGTWholeWeight = "TGTWholeWeight";
	private static final String PAR_TGTWeight = "TGTWeight";
	private static final String PAR_TGTColludingWeight = "TGTColludingWeight";
	private static final String PAR_TGTWeakColludingWeight = "TGTWeakColludingWeight";
	private static final String PAR_TLRavgWeight = "TLRavgWeight";
	private static final String PAR_TSTWeight = "TSTWeight";
	
	private static final String PAR_SuspiciousSPRank = "SuspiciousSPRank";
	private static final String PAR_SuspiciousSPRange = "SuspiciousSPRange";
	private static final String PAR_SuspiciousSPBanningRank = "SuspiciousSPBanningRank";
	
	private static final String PAR_PostDGUinstallationRank = "postDGUinstallationRank";
	private static final String PAR_PostStrictDGUEnableRank = "postStrictDGUEnableRank";
	private static final String PAR_DeployDGU = "DeployDGU";
	
	private static final String PAR_STagentStatusPostSpam = "STagentStatusPostSpam";
	private static final String PAR_STmaxLifeTime = "STmaxLifeTime";
	private static final String PAR_TopTLRavgPCT_ST = "TopTLRavgPCT_ST";
	
	private static final String PAR_BottomTLRavgPCT_ST = "BottomTLRavgPCT_ST";
	private static final String PAR_PSL_STselector = "PSL_STselector";
	private static final String PAR_SuspiciousNodes_STselector = "SuspiciousNodes_STselector";
	private static final String PAR_TopTLRavg_STselector = "TopTLRavg_STselector";
	private static final String PAR_BottomTLRavg_STselector = "BottomTLRavg_STselector";
	private static final String PAR_IIR_STselector = "IIR_STselector";
	
	private static final String PAR_PSL_PCT = "PSL_PCT";
	private static final String PAR_SufficientTLRus_PSP = "SufficientTLRus_PSP";
	private static final String PAR_SufficientTLRus_SP = "SufficientTLRus_SP";
	private static final String PAR_DeployST = "DeployST";
	
	private static final String PAR_DeployGT = "DeployGT";
	private static final String PAR_GT_avgTGT = "GT_avgTGT";
	private static final String PAR_GTagentStatusPostSpam = "GTagentStatusPostSpam";
	private static final String PAR_GTmaxLifeTime = "GTmaxLifeTime";
	private static final String PAR_PcrThreshold = "PcrThreshold";
	private static final String PAR_PcrWeakThreshold = "PcrWeakThreshold";
	private static final String PAR_IgnoreOldGranks = "IgnoreOldGranks";
	private static final String PAR_StopTestingIfMinReached = "StopTestingIfMinReached";
	private static final String PAR_GT_SelectOnlyPSP = "GT_SelectOnlyPSP";
	private static final String PAR_GT_MaxSPNum = "GT_MaxSPNum";
	private static final String PAR_GT_MaxSize = "GT_MaxSize";
	private static final String PAR_PIIL_PCT = "PIIL_PCT";
	private static final String PAR_PIIL_GTselector = "PIIL_GTselector";
	private static final String PAR_PSL_GTselector = "PSL_GTselector";
	private static final String PAR_SuspiciousNodes_GTselector = "SuspiciousNodes_GTselector";
	
	private static final String PAR_SPAM_Drop_StrictCredential_Prob = "SPAM_Drop_StrictCredential_Prob";
	
	private static final String PAR_SPAM_Delay_MSP_PCT = "SPAM_Delay_MSP_PCT";
	private static final String PAR_SPAM_Delay_MPSP_PCT = "SPAM_Delay_MPSP_PCT";
	private static final String PAR_SPAM_Delay_Period = "SPAM_Delay_Period";
	private static final String PAR_SPAM_Bombarding_MSP_PCT = "SPAM_Bombarding_MSP_PCT";
	private static final String PAR_SPAM_Bombarding_MPSP_PCT = "SPAM_Bombarding_MPSP_PCT";
	private static final String PAR_SPAM_Bombarding_Period = "SPAM_Bombarding_Period";
	private static final String PAR_SPAM_Drop_MSP_PCT = "SPAM_Drop_MSP_PCT";
	private static final String PAR_SPAM_Drop_MPSP_PCT = "SPAM_Drop_MPSP_PCT";
	private static final String PAR_SPAM_DropUser_Rate = "SPAM_DropUser_Rate";
	private static final String PAR_SPAM_DropCredential_Rate = "SPAM_DropCredential_Rate";
	private static final String PAR_SPAM_Delay_Bombarding_MSP_PCT = "SPAM_Delay_Bombarding_MSP_PCT";
	private static final String PAR_SPAM_Delay_Bombarding_MPSP_PCT = "SPAM_Delay_Bombarding_MPSP_PCT";
	private static final String PAR_SPAM_Delay_Drop_MSP_PCT = "SPAM_Delay_Drop_MSP_PCT";
	private static final String PAR_SPAM_Delay_Drop_MPSP_PCT = "SPAM_Delay_Drop_MPSP_PCT";
	private static final String PAR_SPAM_Drop_Bombarding_MSP_PCT = "SPAM_Drop_Bombarding_MSP_PCT";
	private static final String PAR_SPAM_Drop_Bombarding_MPSP_PCT = "SPAM_Drop_Bombarding_MPSP_PCT";
	private static final String PAR_SPAM_Delay_Drop_Bombarding_MSP_PCT = "SPAM_Delay_Drop_Bombarding_MSP_PCT";
	private static final String PAR_SPAM_Delay_Drop_Bombarding_MPSP_PCT = "SPAM_Delay_Drop_Bombarding_MPSP_PCT";

	private static final String PAR_SPAM_Delay_ME_MSP_PCT = "SPAM_Delay_ME_MSP_PCT";
	private static final String PAR_SPAM_Delay_ME_MPSP_PCT = "SPAM_Delay_ME_MPSP_PCT";
	private static final String PAR_SPAM_Delay_ME_Period = "SPAM_Delay_ME_Period";
	private static final String PAR_SPAM_Bombarding_ME_MSP_PCT = "SPAM_Bombarding_ME_MSP_PCT";
	private static final String PAR_SPAM_Bombarding_ME_MPSP_PCT = "SPAM_Bombarding_ME_MPSP_PCT";
	private static final String PAR_SPAM_Bombarding_ME_Period = "SPAM_Bombarding_ME_Period";
	private static final String PAR_SPAM_Drop_ME_MSP_PCT = "SPAM_Drop_ME_MSP_PCT";
	private static final String PAR_SPAM_Drop_ME_MPSP_PCT = "SPAM_Drop_ME_MPSP_PCT";
	private static final String PAR_SPAM_DropUser_ME_Rate = "SPAM_DropUser_ME_Rate";
	private static final String PAR_SPAM_DropCredential_ME_Rate = "SPAM_DropCredential_ME_Rate";
	private static final String PAR_SPAM_Delay_Bombarding_ME_MSP_PCT = "SPAM_Delay_Bombarding_ME_MSP_PCT";
	private static final String PAR_SPAM_Delay_Bombarding_ME_MPSP_PCT = "SPAM_Delay_Bombarding_ME_MPSP_PCT";
	private static final String PAR_SPAM_Delay_Drop_ME_MSP_PCT = "SPAM_Delay_Drop_ME_MSP_PCT";
	private static final String PAR_SPAM_Delay_Drop_ME_MPSP_PCT = "SPAM_Delay_Drop_ME_MPSP_PCT";
	private static final String PAR_SPAM_Drop_Bombarding_ME_MSP_PCT = "SPAM_Drop_Bombarding_ME_MSP_PCT";
	private static final String PAR_SPAM_Drop_Bombarding_ME_MPSP_PCT = "SPAM_Drop_Bombarding_ME_MPSP_PCT";
	private static final String PAR_SPAM_Delay_Drop_Bombarding_ME_MSP_PCT = "SPAM_Delay_Drop_Bombarding_ME_MSP_PCT";
	private static final String PAR_SPAM_Delay_Drop_Bombarding_ME_MPSP_PCT = "SPAM_Delay_Drop_Bombarding_ME_MPSP_PCT";
	private static final String PAR_ME_N_MIN_MPSPs_toSPAM_User = "ME_N_MIN_MPSPs_toSPAM_User";
	private static final String PAR_ME_N_MIN_MPSPs_toSPAM_Credential = "ME_N_MIN_MPSPs_toSPAM_Credential";
	private static final String PAR_ME_Colluding_Unpopular_PCT = "ME_Colluding_Unpopular_PCT";
	//private static final String PAR_ME_Colluding_Popular_PCT = "ME_Colluding_Popular_PCT";
	
	
    // ------------------------------------------------------------------------
    // Fields
    // ------------------------------------------------------------------------
    
	private int exprimentTrial_k;
	
	private int Experiment;
	private boolean Fixed;
	private int Factors;
	
	private static int RMGRPid;
	private static int SuperPid;
	private static int IDPPid;
    private static int AuditorPid;
    private static int UserPid;
    private static int SPPid;
    private static int MEPid;
    
    private static double SPRank;
    
    private static int INI_Users;
    private static double UsersGR;
    private static int INI_PSPs;
    private static double PSPsGR;
    private static int INI_SPs;
    private static double SPsGR;
    private static int INI_MPSPs;
    private static double MPSPsGR;
    private static int INI_MSPs;
    private static double MSPsGR;
    private static int INI_ME;
    private static double MEGR;
    private static int INI_ME_MPSPs;
    private static int INI_ME_MSPs;
    private static double ME_MPSPsGR;
    private static double ME_MSPsGR;
    private static int ME_MAX_MPSPs;
    private static int ME_MAX_MSPs;
    
    private static double usefulnessMIN;
    private static double newPartnershipProb;
    private static int newPartnershipDuration;
    private static double partnerSharingProb;
    private static double OfferDGUProb;
    private static double OfferStrictDGUProb;
    private static double AcceptDGUProb;
    private static double AcceptStrictDGUProb;
    private static boolean DeployDGU;
    private static double  AcceptCompulsoryStrictDGUProb;
    private static double  AcceptCompulsoryDGUProb;
    
	
    private static int CredentialsArraySize;
	private static double GenerateNewCredentialRate;
	private static double GenerateNewServiceRequestRate;
	private static double StrictCredentialProb;
	private static int IniCredentialType;
    private static double TrustInNetwork;
    private static double IgnoranceRate;
    private static boolean StopUsingCredentialAfterSpam;
    private static double TrustDropPostSpam;
    private static double TrustIncreasePostSpamResolved;
    private static double TrustIncreaseRate;
    private static double TrustDecreaseRate;
    private static double TrustDecreasePeriodPostSpam;
    
	private static double TGTWholeWeight;
	private static double TGTWeight;
	private static double TGTColludingWeight;
	private static double TGTWeakColludingWeight;
	private static double TLRavgWeight;
	private static double TSTWeight;
    
    private static double SuspiciousSPRank;
    private static double SuspiciousSPRange;
    private static double SuspiciousSPBanningRank;
    
    private static double postDGUinstallationRank;
    private static double postStrictDGUEnableRank;
    
    private static int STagentStatusPostSpam;
    private static int STmaxLifeTime;
    private static int TopTLRavgPCT_ST;
	private static int BottomTLRavgPCT_ST;
	private static boolean PSL_STselector;
	private static boolean SuspiciousNodes_STselector;
	private static boolean TopTLRavg_STselector;
	private boolean BottomTLRavg_STselector;
	private static boolean IIR_STselector;
    private static double PSL_PCT;
    private static int SufficientTLRus_PSP;
    private static int SufficientTLRus_SP;
    private static boolean DeployST;
    
    
	private static boolean DeployGT;
	private static boolean GT_avgTGT;
	private static int GTagentStatusPostSpam;
	private static int GTmaxLifeTime;
	private static double PcrThreshold;
	private static double PcrWeakThreshold;
	private static boolean IgnoreOldGranks;
	private static boolean StopTestingIfMinReached;
	private static boolean GT_SelectOnlyPSP;
	private static int GT_MaxSPNum;
	private static int GT_MaxSize;
    private static double PIIL_PCT;
    private static boolean PIIL_GTselector;
    private static boolean PSL_GTselector;
    private static boolean SuspiciousNodes_GTselector;
    
    private static double SPAM_Drop_StrictCredential_Prob;
    
    private static double SPAM_Delay_MSP_PCT;
    private static double SPAM_Delay_MPSP_PCT;
    private static int SPAM_Delay_Period;
    private static double SPAM_Bombarding_MSP_PCT;
    private static double SPAM_Bombarding_MPSP_PCT;
    private static int SPAM_Bombarding_Period;
    private static double SPAM_Drop_MSP_PCT;
    private static double SPAM_Drop_MPSP_PCT;
    private static double SPAM_DropUser_Rate;
    private static double SPAM_DropCredential_Rate;
    private static double SPAM_Delay_Bombarding_MSP_PCT;
    private static double SPAM_Delay_Bombarding_MPSP_PCT;
    private static double SPAM_Delay_Drop_MSP_PCT;
    private static double SPAM_Delay_Drop_MPSP_PCT;
    private static double SPAM_Drop_Bombarding_MSP_PCT;
    private static double SPAM_Drop_Bombarding_MPSP_PCT;
    private static double SPAM_Delay_Drop_Bombarding_MSP_PCT;
    private static double SPAM_Delay_Drop_Bombarding_MPSP_PCT;
    
    
    private static double SPAM_Delay_ME_MSP_PCT;
    private static double SPAM_Delay_ME_MPSP_PCT;
    private static int SPAM_Delay_ME_Period;
    private static double SPAM_Bombarding_ME_MSP_PCT;
    private static double SPAM_Bombarding_ME_MPSP_PCT;
    private static int SPAM_Bombarding_ME_Period;
    private static double SPAM_Drop_ME_MSP_PCT;
    private static double SPAM_Drop_ME_MPSP_PCT;
    private static double SPAM_DropUser_ME_Rate;
    private static double SPAM_DropCredential_ME_Rate;
    private static double SPAM_Delay_Bombarding_ME_MSP_PCT;
    private static double SPAM_Delay_Bombarding_ME_MPSP_PCT;
    private static double SPAM_Delay_Drop_ME_MSP_PCT;
    private static double SPAM_Delay_Drop_ME_MPSP_PCT;
    private static double SPAM_Drop_Bombarding_ME_MSP_PCT;
    private static double SPAM_Drop_Bombarding_ME_MPSP_PCT;
    private static double SPAM_Delay_Drop_Bombarding_ME_MSP_PCT;
    private static double SPAM_Delay_Drop_Bombarding_ME_MPSP_PCT;
	private static int ME_N_MIN_MPSPs_toSPAM_User;
	private static int ME_N_MIN_MPSPs_toSPAM_Credential;
	private static double ME_Colluding_Unpopular_PCT;

	private List<Factor> FactorsList;
	
    // ------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------

    /**
     * Standard constructor that reads the configuration parameters. Invoked by
     * the simulation engine.
     * 
     * @param prefix
     *            the configuration prefix for this class.
     */
    public SimulationInitializer(String prefix) {
     
    	Experiment = Configuration.getInt(PAR_Experiment);
    	
        if(Configuration.getInt(PAR_Fixed) == 0)
        	Fixed =  false;
    	else
    		Fixed = true;
    	
    	Factors = Configuration.getInt(PAR_Factors);
    	
    	FactorsList = new ArrayList<Factor>(Factors);
    	
    	for(int i = 1; i <= Factors; i++)
    	{
    		int nestedFactors = Configuration.getInt("Factor_"+i+"_nested");
    		
    		for(int j = 1; j <= nestedFactors; j++)
    		{
    			Factor factor = new Factor();
    			
        		String tempFactor =  Configuration.getString("Factor_"+i+"_"+j);
        		
        		double tempFactorLow =  Configuration.getDouble("Factor_"+i+"_"+j+"_low");
        		
        		double tempFactorHigh =  Configuration.getDouble("Factor_"+i+"_"+j+"_high");
        		
        		double tempFactorFixed =  Configuration.getDouble("Factor_"+i+"_"+j+"_fixed");
        		
        		factor.setFactor(tempFactor);
        		factor.setOrder(i);
        		factor.setLow(tempFactorLow);
        		factor.setHigh(tempFactorHigh);
        		factor.setFixed(tempFactorFixed);
        		
        		FactorsList.add(factor);
    		}
    	}
    	
    	RMGRPid = Configuration.getPid(prefix + "." + PAR_RMGR_Node_PROT);
    	SuperPid = Configuration.getPid(prefix + "." + PAR_Super_Node_PROT);
    	IDPPid = Configuration.getPid(prefix + "." + PAR_IDP_Node_PROT);
        AuditorPid = Configuration.getPid(prefix + "." + PAR_Auditor_Node_PROT);
        UserPid = Configuration.getPid(prefix + "." + PAR_User_Node_PROT);
        SPPid = Configuration.getPid(prefix + "." + PAR_SP_Node_PROT);
        MEPid = Configuration.getPid(prefix + "." + PAR_ME_Node_PROT);
        
        SPRank = Configuration.getDouble(prefix + "." + PAR_SPRank);
        
        INI_Users = Configuration.getInt(prefix + "." + PAR_INI_Users);
        UsersGR = Configuration.getDouble(prefix + "." + PAR_UsersGR);
        INI_PSPs = Configuration.getInt(prefix + "." + PAR_INI_PSPs);
        PSPsGR = Configuration.getDouble(prefix + "." + PAR_PSPsGR);
        INI_SPs = Configuration.getInt(prefix + "." + PAR_INI_SPs);
        SPsGR = Configuration.getDouble(prefix + "." + PAR_SPsGR);
        INI_MPSPs = Configuration.getInt(prefix + "." + PAR_INI_MPSPs);
        MPSPsGR = Configuration.getDouble(prefix + "." + PAR_MPSPsGR);
        INI_MSPs = Configuration.getInt(prefix + "." + PAR_INI_MSPs);
        MSPsGR = Configuration.getDouble(prefix + "." + PAR_MSPsGR);   
        INI_ME = Configuration.getInt(prefix + "." + PAR_INI_ME);
        MEGR = Configuration.getDouble(prefix + "." + PAR_MEGR);
        INI_ME_MPSPs = Configuration.getInt(prefix + "." + PAR_INI_ME_MPSPs);
        INI_ME_MSPs = Configuration.getInt(prefix + "." + PAR_INI_ME_MSPs);
        ME_MPSPsGR = Configuration.getDouble(prefix + "." + PAR_ME_MPSPsGR);
        ME_MSPsGR = Configuration.getDouble(prefix + "." + PAR_ME_MSPsGR);
        ME_MAX_MPSPs = Configuration.getInt(prefix + "." + PAR_ME_MAX_MPSPs);
        ME_MAX_MSPs = Configuration.getInt(prefix + "." + PAR_ME_MAX_MSPs);

        usefulnessMIN = Configuration.getDouble(prefix + "." + PAR_UsefulnessMIN);
        newPartnershipProb = Configuration.getDouble(prefix + "." + PAR_NewPartnershipProb);
        newPartnershipDuration = Configuration.getInt(prefix + "." + PAR_NewPartnershipDuration);
        partnerSharingProb = Configuration.getDouble(prefix + "." + PAR_PartnerSharingProb);
        OfferDGUProb = Configuration.getDouble(prefix + "." + PAR_OfferDGUProb);
        OfferStrictDGUProb = Configuration.getDouble(prefix + "." + PAR_OfferStrictDGUProb);
        AcceptDGUProb = Configuration.getDouble(prefix + "." + PAR_AcceptDGUProb);
        AcceptStrictDGUProb = Configuration.getDouble(prefix + "." + PAR_AcceptStrictDGUProb);
    
        if(Configuration.getInt(prefix + "." + PAR_DeployDGU) == 0)
        	DeployDGU =  false;
    	else
    		DeployDGU = true;
        
        AcceptCompulsoryStrictDGUProb = Configuration.getDouble(prefix + "." + PAR_AcceptCompulsoryStrictDGUProb);
        AcceptCompulsoryDGUProb = Configuration.getDouble(prefix + "." + PAR_AcceptCompulsoryDGUProb);
             
        GenerateNewCredentialRate = Configuration.getDouble(prefix + "." + PAR_GenerateNewCredentialRate);
		GenerateNewServiceRequestRate = Configuration.getDouble(prefix + "." + PAR_GenerateNewServiceRequestRate);
		CredentialsArraySize = Configuration.getInt(prefix + "." + PAR_CredentialsArraySize);
		StrictCredentialProb = Configuration.getDouble(prefix + "." + PAR_StrictCredentialProb);
		IniCredentialType = Configuration.getInt(prefix + "." + PAR_IniCredentialType);
        TrustInNetwork = Configuration.getDouble(prefix + "." + PAR_TrustInNetwork);
        IgnoranceRate = Configuration.getDouble(prefix + "." + PAR_IgnoranceRate);
        StopUsingCredentialAfterSpam = Configuration.getBoolean(prefix + "." + PAR_StopUsingCredentialAfterSpam);
        TrustDropPostSpam = Configuration.getDouble(prefix + "." + PAR_TrustDropPostSpam);
        TrustIncreasePostSpamResolved = Configuration.getDouble(prefix + "." + PAR_TrustIncreasePostSpamResolved);
        TrustIncreaseRate = Configuration.getDouble(prefix + "." + PAR_TrustIncreaseRate);
        TrustDecreaseRate = Configuration.getDouble(prefix + "." + PAR_TrustDecreaseRate);
        TrustDecreasePeriodPostSpam = Configuration.getDouble(prefix + "." + PAR_TrustDecreasePeriodPostSpam);
        
        TGTWholeWeight = Configuration.getDouble(prefix + "." + PAR_TGTWholeWeight);
        TGTWeight = Configuration.getDouble(prefix + "." + PAR_TGTWeight);
        TGTColludingWeight = Configuration.getDouble(prefix + "." + PAR_TGTColludingWeight);
        TGTWeakColludingWeight = Configuration.getDouble(prefix + "." + PAR_TGTWeakColludingWeight);
        TLRavgWeight = Configuration.getDouble(prefix + "." + PAR_TLRavgWeight);
        TSTWeight = Configuration.getDouble(prefix + "." + PAR_TSTWeight);

        SuspiciousSPRank = Configuration.getDouble(prefix + "." + PAR_SuspiciousSPRank);
        SuspiciousSPRange = Configuration.getDouble(prefix + "." + PAR_SuspiciousSPRange);
        SuspiciousSPBanningRank = Configuration.getDouble(prefix + "." + PAR_SuspiciousSPBanningRank);
        
        postDGUinstallationRank = Configuration.getDouble(prefix + "." + PAR_PostDGUinstallationRank);
        postStrictDGUEnableRank = Configuration.getDouble(prefix + "." + PAR_PostStrictDGUEnableRank);
        
        STagentStatusPostSpam = Configuration.getInt(prefix + "." + PAR_STagentStatusPostSpam);
        STmaxLifeTime = Configuration.getInt(prefix + "." + PAR_STmaxLifeTime);
        TopTLRavgPCT_ST = Configuration.getInt(prefix + "." + PAR_TopTLRavgPCT_ST);	
    	BottomTLRavgPCT_ST = Configuration.getInt(prefix + "." + PAR_BottomTLRavgPCT_ST);
    	
    	if(Configuration.getInt(prefix + "." + PAR_PSL_STselector) == 0)
    		PSL_STselector =  false;
    	else
    		PSL_STselector = true;
    	 
    	if(Configuration.getInt(prefix + "." + PAR_SuspiciousNodes_STselector) == 0) 
    		SuspiciousNodes_STselector = false;
    	else
    		SuspiciousNodes_STselector = true;
    	
    	if( Configuration.getInt(prefix + "." + PAR_TopTLRavg_STselector) == 0) 
    		TopTLRavg_STselector = false;
    	else
    		TopTLRavg_STselector = true;
    				
    	if(Configuration.getInt(prefix + "." + PAR_BottomTLRavg_STselector) == 0)
    		BottomTLRavg_STselector = false;
    	else
    		BottomTLRavg_STselector = true;
    	
        if(Configuration.getInt(prefix + "." + PAR_IIR_STselector) == 0)
    		IIR_STselector =  false;
    	else
    		IIR_STselector = true;
    	
        PSL_PCT = Configuration.getDouble(prefix + "." + PAR_PSL_PCT);
        SufficientTLRus_PSP = Configuration.getInt(prefix + "." + PAR_SufficientTLRus_PSP);
        SufficientTLRus_SP = Configuration.getInt(prefix + "." + PAR_SufficientTLRus_SP);
    	
        if( Configuration.getInt(prefix + "." + PAR_DeployST) == 0) 
        	DeployST = false;
    	else
    		DeployST = true;
        
        
        
        if( Configuration.getInt(prefix + "." + PAR_DeployGT) == 0) 
        	DeployGT = false;
    	else
    		DeployGT = true;
        
        if( Configuration.getInt(prefix + "." + PAR_GT_avgTGT) == 0) 
        	GT_avgTGT = false;
    	else
    		GT_avgTGT = true;
        
        GTagentStatusPostSpam = Configuration.getInt(prefix + "." + PAR_GTagentStatusPostSpam);
        GTmaxLifeTime = Configuration.getInt(prefix + "." + PAR_GTmaxLifeTime);
        PcrThreshold = Configuration.getDouble(prefix + "." + PAR_PcrThreshold);
        PcrWeakThreshold = Configuration.getDouble(prefix + "." + PAR_PcrWeakThreshold);
        
        if(Configuration.getInt(prefix + "." + PAR_IgnoreOldGranks) == 0)
        	IgnoreOldGranks =  false;
    	else
    		IgnoreOldGranks = true;
        
        if(Configuration.getInt(prefix + "." + PAR_StopTestingIfMinReached) == 0)
        	StopTestingIfMinReached =  false;
    	else
    		StopTestingIfMinReached = true;
        
        if(Configuration.getInt(prefix + "." + PAR_GT_SelectOnlyPSP) == 0)
        	GT_SelectOnlyPSP =  false;
    	else
    		GT_SelectOnlyPSP = true;

        GT_MaxSPNum = Configuration.getInt(prefix + "." + PAR_GT_MaxSPNum);
        GT_MaxSize = Configuration.getInt(prefix + "." + PAR_GT_MaxSize);

        PIIL_PCT = Configuration.getDouble(prefix + "." + PAR_PIIL_PCT);
    	
        if(Configuration.getInt(prefix + "." + PAR_PIIL_GTselector) == 0)
    		PIIL_GTselector =  false;
    	else
    		PIIL_GTselector = true;
        
    	if(Configuration.getInt(prefix + "." + PAR_PSL_GTselector) == 0)
    		PSL_GTselector =  false;
    	else
    		PSL_GTselector = true;
    	
    	if(Configuration.getInt(prefix + "." + PAR_SuspiciousNodes_GTselector) == 0) 
    		SuspiciousNodes_GTselector = false;
    	else
    		SuspiciousNodes_GTselector = true;
        
    	SPAM_Drop_StrictCredential_Prob = Configuration.getDouble(prefix + "." + PAR_SPAM_Drop_StrictCredential_Prob);

    	SPAM_Delay_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_MSP_PCT);
        SPAM_Delay_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_MPSP_PCT);
        SPAM_Delay_Period = Configuration.getInt(prefix + "." + PAR_SPAM_Delay_Period);
        SPAM_Bombarding_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Bombarding_MSP_PCT);
        SPAM_Bombarding_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Bombarding_MPSP_PCT);
        SPAM_Bombarding_Period = Configuration.getInt(prefix + "." + PAR_SPAM_Bombarding_Period);
        SPAM_Drop_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Drop_MSP_PCT);
        SPAM_Drop_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Drop_MPSP_PCT);
        SPAM_DropUser_Rate = Configuration.getDouble(prefix + "." + PAR_SPAM_DropUser_Rate);
        SPAM_DropCredential_Rate = Configuration.getDouble(prefix + "." + PAR_SPAM_DropCredential_Rate);
        SPAM_Delay_Bombarding_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Bombarding_MSP_PCT);
        SPAM_Delay_Bombarding_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Bombarding_MPSP_PCT);
        SPAM_Delay_Drop_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Drop_MSP_PCT);
        SPAM_Delay_Drop_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Drop_MPSP_PCT);
        SPAM_Drop_Bombarding_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Drop_Bombarding_MSP_PCT);
        SPAM_Drop_Bombarding_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Drop_Bombarding_MPSP_PCT);
        SPAM_Delay_Drop_Bombarding_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Drop_Bombarding_MSP_PCT);
        SPAM_Delay_Drop_Bombarding_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Drop_Bombarding_MPSP_PCT);

        
        SPAM_Delay_ME_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_ME_MSP_PCT);
        SPAM_Delay_ME_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_ME_MPSP_PCT);
        SPAM_Delay_ME_Period = Configuration.getInt(prefix + "." + PAR_SPAM_Delay_ME_Period);
        SPAM_Bombarding_ME_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Bombarding_ME_MSP_PCT);
        SPAM_Bombarding_ME_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Bombarding_ME_MPSP_PCT);
        SPAM_Bombarding_ME_Period = Configuration.getInt(prefix + "." + PAR_SPAM_Bombarding_ME_Period);
        SPAM_Drop_ME_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Drop_ME_MSP_PCT);
        SPAM_Drop_ME_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Drop_ME_MPSP_PCT);
        SPAM_DropUser_ME_Rate = Configuration.getDouble(prefix + "." + PAR_SPAM_DropUser_ME_Rate);
        SPAM_DropCredential_ME_Rate = Configuration.getDouble(prefix + "." + PAR_SPAM_DropCredential_ME_Rate);
        SPAM_Delay_Bombarding_ME_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Bombarding_ME_MSP_PCT);
        SPAM_Delay_Bombarding_ME_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Bombarding_ME_MPSP_PCT);
        SPAM_Delay_Drop_ME_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Drop_ME_MSP_PCT);
        SPAM_Delay_Drop_ME_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Drop_ME_MPSP_PCT);
        SPAM_Drop_Bombarding_ME_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Drop_Bombarding_ME_MSP_PCT);
        SPAM_Drop_Bombarding_ME_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Drop_Bombarding_ME_MPSP_PCT);
        SPAM_Delay_Drop_Bombarding_ME_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Drop_Bombarding_ME_MSP_PCT);
        SPAM_Delay_Drop_Bombarding_ME_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Drop_Bombarding_ME_MPSP_PCT);
    	ME_N_MIN_MPSPs_toSPAM_User = Configuration.getInt(prefix + "." + PAR_ME_N_MIN_MPSPs_toSPAM_User);
    	ME_N_MIN_MPSPs_toSPAM_Credential = Configuration.getInt(prefix + "." + PAR_ME_N_MIN_MPSPs_toSPAM_Credential);
    	ME_Colluding_Unpopular_PCT = Configuration.getDouble(prefix + "." + PAR_ME_Colluding_Unpopular_PCT);
    	//ME_Colluding_Popular_PCT = Configuration.getDouble(prefix + "." + PAR_ME_Colluding_Popular_PCT);

    }

    // ------------------------------------------------------------------------
    // Methods
    // ------------------------------------------------------------------------
    /**
     * Initialize the node coordinates. The first node in the {@link Network} is
     * the root node by default and it is located in the middle (the center of
     * the square) of the surface area.
     * 
     * 
     *
     * 
     */
    public boolean execute() {

    	//System.out.println("trial: "+this.exprimentTrial_k);
    	
    	//Map<String, Boolean> TGTweights = new HashMap<String, Boolean>(3);;
    	//Boolean TGTweightsTesting = false;
    	
    	TrialsCatalouge catalouge = new TrialsCatalouge(Factors);
    	
    	int trial = (exprimentTrial_k % catalouge.size())+1;
    	
    	for(Factor factor: FactorsList)
    	{
    		switch(factor.getFactor()){
    		
    		case "postDGUinstallationRank":
    			
    			if(Fixed)
    			{
    				postDGUinstallationRank = factor.getFixed();
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				postDGUinstallationRank = factor.getHigh();
    			else
    				postDGUinstallationRank = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+postDGUinstallationRank);
    			break;

    		case "TGTWholeWeight":
    			
    			if(Fixed)
    			{
    				TGTWholeWeight = factor.getFixed();
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				TGTWholeWeight = factor.getHigh();
    			else
    				TGTWholeWeight = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+TGTWholeWeight);
    			break;
    			
    		case "TGTWeakColludingWeight":
    			
    			if(Fixed)
    			{
    				TGTWeakColludingWeight = factor.getFixed();
    				break;
    			}
    			
    			//TGTweightsTesting = true;
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    			{
    				TGTWeakColludingWeight = factor.getHigh();
    				//TGTweights.put("TGTWeakColludingWeight", true);
    			}
    			else
    			{
    				TGTWeakColludingWeight = factor.getLow();
    				//TGTweights.put("TGTWeakColludingWeight", false);
    			}
   
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+TGTWeakColludingWeight);
    			break;
    			
    		case "DeployGT":
    			
    			if(Fixed)
    			{
    				if(factor.getFixed() == 1) DeployGT = true;
    				else DeployGT = false;
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    			{
    				if(factor.getHigh() == 1) DeployGT = true;
    				else DeployGT = false;
    			}
    			else
    			{
    				if(factor.getLow() == 1) DeployGT = true;
    				else DeployGT = false;
    			}
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+DeployGT);
    			break;
    			
    		case "TGTWeight":
    			
    			if(Fixed)
    			{
    				TGTWeight = factor.getFixed();
    				break;
    			}
    			
    			//TGTweightsTesting = true;
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    			{
    				TGTWeight = factor.getHigh();
    			//	TGTweights.put("TGTWeight", true);
    			}
    			else
    			{
    				TGTWeight = factor.getLow();
    				//TGTweights.put("TGTWeight", false);
    			}
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+TGTWeight);
    			break;
    			
    		case "TGTColludingWeight":
    			
    			if(Fixed)
    			{
    				TGTColludingWeight = factor.getFixed();
    				break;
    			}
    			
    			//TGTweightsTesting = true;
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    			{
    				TGTColludingWeight = factor.getHigh();
    				//TGTweights.put("TGTColludingWeight", true);
    			}
    			else
    			{
    				TGTColludingWeight = factor.getLow();
    				//TGTweights.put("TGTColludingWeight", false);
    			}
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+TGTColludingWeight);
    			break;
    			
    		case "TLRavgWeight":
    			
    			if(Fixed)
    			{
    				TLRavgWeight = factor.getFixed();
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				TLRavgWeight = factor.getHigh();
    			else
    				TLRavgWeight = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+TLRavgWeight);
    			break;
    			
    		case "DeployST":
    			
    			if(Fixed)
    			{
    				if(factor.getFixed() == 1) DeployST = true;
    				else DeployST = false;
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    			{
    				if(factor.getHigh() == 1) DeployST = true;
    				else DeployST = false;
    			}
    			else
    			{
    				if(factor.getLow() == 1) DeployST = true;
    				else DeployST = false;
    			}
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+DeployST);
    			break;
    			
    		case "TSTWeight":
    			
    			if(Fixed)
    			{
    				TSTWeight = factor.getFixed();
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				TSTWeight = factor.getHigh();
    			else
    				TSTWeight = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+TSTWeight);
    			break;
    			
    		case "SuspiciousSPRank":
    			
    			if(Fixed)
    			{
    				SuspiciousSPRank = factor.getFixed();
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SuspiciousSPRank = factor.getHigh();
    			else
    				SuspiciousSPRank = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SuspiciousSPRank);
    			break;
    			
    		case "SuspiciousSPBanningRank":
    			
    			if(Fixed)
    			{
    				SuspiciousSPBanningRank = factor.getFixed();
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SuspiciousSPBanningRank = factor.getHigh();
    			else
    				SuspiciousSPBanningRank = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SuspiciousSPBanningRank);
    			break;
    			
    		case "SufficientTLRus_PSP":
    			
    			if(Fixed)
    			{
    				SufficientTLRus_PSP = (int) factor.getFixed();
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SufficientTLRus_PSP = (int) factor.getHigh();
    			else
    				SufficientTLRus_PSP = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SufficientTLRus_PSP);
    			break;
    			
    		case "SufficientTLRus_SP":

    			if(Fixed)
    			{
    				SufficientTLRus_SP = (int) factor.getFixed();
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SufficientTLRus_SP = (int) factor.getHigh();
    			else
    				SufficientTLRus_SP = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SufficientTLRus_SP);
    			break;
    			
    		case "DeployDGU":
    			
    			if(Fixed)
    			{
    				if(factor.getFixed() == 1) DeployDGU = true;
    				else DeployDGU = false;
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    			{
    				if(factor.getHigh() == 1) DeployDGU = true;
    				else DeployDGU = false;
    			}
    			else
    			{
    				if(factor.getLow() == 1) DeployDGU = true;
    				else DeployDGU = false;
    			}
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+DeployDGU);
    			break;
    			
    		case "STmaxLifeTime":
    			
    			if(Fixed)
    			{
    				STmaxLifeTime = (int) factor.getFixed();
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				STmaxLifeTime = (int) factor.getHigh();
    			else
    				STmaxLifeTime = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+STmaxLifeTime);
    			break;
    			
    		case "STreportSpam":
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is ignored by init Initiliazer!");
    			break;
    			
    		case "PSL_STselector":
    			
    			if(Fixed)
    			{
    				if(factor.getFixed() == 1) PSL_STselector = true;
    				else PSL_STselector = false;
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    			{
    				if(factor.getHigh() == 1) PSL_STselector = true;
    				else PSL_STselector = false;
    			}
    			else
    			{
    				if(factor.getLow() == 1) PSL_STselector = true;
    				else PSL_STselector = false;
    			}
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+PSL_STselector);
    			break;
    			
    		case "SuspiciousNodes_STselector":
    			
    			if(Fixed)
    			{
    				if(factor.getFixed() == 1) SuspiciousNodes_STselector = true;
    				else SuspiciousNodes_STselector = false;
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    			{
    				if(factor.getHigh() == 1) SuspiciousNodes_STselector = true;
    				else SuspiciousNodes_STselector = false;
    			}
    			else
    			{
    				if(factor.getLow() == 1) SuspiciousNodes_STselector = true;
    				else SuspiciousNodes_STselector = false;
    			}
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SuspiciousNodes_STselector);
    			break;
    			
    		case "TopTLRavg_STselector":
    			
    			if(Fixed)
    			{
    				if(factor.getFixed() == 1) TopTLRavg_STselector = true;
    				else TopTLRavg_STselector = false;
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    			{
    				if(factor.getHigh() == 1) TopTLRavg_STselector = true;
    				else TopTLRavg_STselector = false;
    			}
    			else
    			{
    				if(factor.getLow() == 1) TopTLRavg_STselector = true;
    				else TopTLRavg_STselector = false;
    			}
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+TopTLRavg_STselector);
    			break;
    			
    		case "BottomTLRavg_STselector":
    			
    			if(Fixed)
    			{
    				if(factor.getFixed() == 1) BottomTLRavg_STselector = true;
    				else BottomTLRavg_STselector = false;
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    			{
    				if(factor.getHigh() == 1) BottomTLRavg_STselector = true;
    				else BottomTLRavg_STselector = false;
    			}
    			else
    			{
    				if(factor.getLow() == 1) BottomTLRavg_STselector = true;
    				else BottomTLRavg_STselector = false;
    			}
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+BottomTLRavg_STselector);
    			break;
    			
    		case "IIR_STselector":
    			
    			if(Fixed)
    			{
    				if(factor.getFixed() == 1) IIR_STselector = true;
    				else IIR_STselector = false;
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    			{
    				if(factor.getHigh() == 1) IIR_STselector = true;
    				else IIR_STselector = false;
    			}
    			else
    			{
    				if(factor.getLow() == 1) IIR_STselector = true;
    				else IIR_STselector = false;
    			}
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+IIR_STselector);
    			break;
    			
    		case "GTmaxLifeTime":
    			
    			if(Fixed)
    			{
    				GTmaxLifeTime = (int) factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				GTmaxLifeTime = (int) factor.getHigh();
    			else
    				GTmaxLifeTime = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+GTmaxLifeTime);
    			break;
    			
    		case "GTreportSpam":
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is ignored by init Initiliazer!");
    			break;
    			
    		case "PcrThreshold":
    			
    			if(Fixed)
    			{
    				PcrThreshold = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				PcrThreshold = factor.getHigh();
    			else
    				PcrThreshold = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+PcrThreshold);
    			break;
    			
    		case "PcrWeakThreshold":
    			
    			if(Fixed)
    			{
    				PcrWeakThreshold = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				PcrWeakThreshold = factor.getHigh();
    			else
    				PcrWeakThreshold = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+PcrWeakThreshold);
    			break;
    			
    		case "IgnoreOldGranks":
    			
    			if(Fixed)
    			{
    				if(factor.getFixed() == 1) IgnoreOldGranks = true;
    				else IgnoreOldGranks = false;
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    			{
    				if(factor.getHigh() == 1) IgnoreOldGranks = true;
    				else IgnoreOldGranks = false;
    			}
    			else
    			{
    				if(factor.getLow() == 1) IgnoreOldGranks = true;
    				else IgnoreOldGranks = false;
    			}
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+IgnoreOldGranks);
    			break;
    			
    		case "GT_MaxSPNum":
    			
    			if(Fixed)
    			{
    				GT_MaxSPNum = (int) factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				GT_MaxSPNum = (int) factor.getHigh();
    			else
    				GT_MaxSPNum = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+GT_MaxSPNum);
    			break;
    			
    		case "GT_MaxSize":
    			
     			if(Fixed)
    			{
     				GT_MaxSize = (int) factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				GT_MaxSize = (int) factor.getHigh();
    			else
    				GT_MaxSize = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+GT_MaxSize);
    			break;
    			
    		case "PIIL_GTselector":
    			
    			if(Fixed)
    			{
    				if(factor.getFixed() == 1) PIIL_GTselector = true;
    				else PIIL_GTselector = false;
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    			{
    				if(factor.getHigh() == 1) PIIL_GTselector = true;
    				else PIIL_GTselector = false;
    			}
    			else
    			{
    				if(factor.getLow() == 1) PIIL_GTselector = true;
    				else PIIL_GTselector = false;
    			}
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+PIIL_GTselector);
    			break;
    			
    		case "PSL_GTselector":
    			
    			if(Fixed)
    			{
    				if(factor.getFixed() == 1) PSL_GTselector = true;
    				else PSL_GTselector = false;
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    			{
    				if(factor.getHigh() == 1) PSL_GTselector = true;
    				else PSL_GTselector = false;
    			}
    			else
    			{
    				if(factor.getLow() == 1) PSL_GTselector = true;
    				else PSL_GTselector = false;
    			}
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+PSL_GTselector);
    			break;
    			
    		case "SuspiciousNodes_GTselector":
    			
    			if(Fixed)
    			{
    				if(factor.getFixed() == 1) SuspiciousNodes_GTselector = true;
    				else SuspiciousNodes_GTselector = false;
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SuspiciousNodes_GTselector = true;
    			else
    				SuspiciousNodes_GTselector = false;
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SuspiciousNodes_GTselector);
    			break;
    			
    		case "OfferDGUProb":
    			
    			if(Fixed)
    			{
    				OfferDGUProb = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				OfferDGUProb = factor.getHigh();
    			else
    				OfferDGUProb = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+OfferDGUProb);
    			break;
    			
    		case "OfferStrictDGUProb":
    			
    			if(Fixed)
    			{
    				OfferStrictDGUProb = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				OfferStrictDGUProb = factor.getHigh();
    			else
    				OfferStrictDGUProb = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+OfferStrictDGUProb);
    			break;
    			
    		case "IgnoranceRate":
    			
    			if(Fixed)
    			{
    				IgnoranceRate = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				IgnoranceRate = factor.getHigh();
    			else
    				IgnoranceRate = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+IgnoranceRate);
    			break;
    			
    		case "AcceptDGUProb":
    			
    			if(Fixed)
    			{
    				AcceptDGUProb = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				AcceptDGUProb = factor.getHigh();
    			else
    				AcceptDGUProb = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+AcceptDGUProb);
    			break;
    			
    		case "AcceptCompulsoryDGUProb":
    			
    			if(Fixed)
    			{
    				AcceptCompulsoryDGUProb = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				AcceptCompulsoryDGUProb = factor.getHigh();
    			else
    				AcceptCompulsoryDGUProb = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+AcceptCompulsoryDGUProb);
    			break;
    			
    		case "AcceptStrictDGUProb":
    			
      			if(Fixed)
    			{
      				AcceptStrictDGUProb = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				AcceptStrictDGUProb = factor.getHigh();
    			else
    				AcceptStrictDGUProb = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+AcceptStrictDGUProb);
    			break;
    			
    		case "AcceptCompulsoryStrictDGUProb":
    			
      			if(Fixed)
    			{
      				AcceptCompulsoryStrictDGUProb = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				AcceptCompulsoryStrictDGUProb = factor.getHigh();
    			else
    				AcceptCompulsoryStrictDGUProb = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+AcceptCompulsoryStrictDGUProb);
    			break;
    			
    		case "INI_Users":
    			
      			if(Fixed)
    			{
      				INI_Users = (int) factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				INI_Users = (int) factor.getHigh();
    			else
    				INI_Users = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+INI_Users);
    			break;
    			
    		case "INI_PSPs":
    			
      			if(Fixed)
    			{
      				INI_PSPs = (int) factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				INI_PSPs = (int) factor.getHigh();
    			else
    				INI_PSPs = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+INI_PSPs);
    			break;
    			
    		case "INI_SPs":
    			
      			if(Fixed)
    			{
      				INI_SPs = (int) factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				INI_SPs = (int) factor.getHigh();
    			else
    				INI_SPs = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+INI_SPs);
    			break;
    			
    		case "UsersGR":
    			
      			if(Fixed)
    			{
      				UsersGR = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				UsersGR = factor.getHigh();
    			else
    				UsersGR = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+UsersGR);
    			break;
    			
    		case "SPsGR":
    			
      			if(Fixed)
    			{
      				SPsGR = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SPsGR = factor.getHigh();
    			else
    				SPsGR = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SPsGR);
    			break;
    			
    		case "PSPsGR":
    			
      			if(Fixed)
    			{
      				PSPsGR = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				PSPsGR = factor.getHigh();
    			else
    				PSPsGR = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+PSPsGR);
    			break;
    			
    		case "INI_MPSPs":
    			
      			if(Fixed)
    			{
      				INI_MPSPs = (int) factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				INI_MPSPs = (int) factor.getHigh();
    			else
    				INI_MPSPs = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+INI_MPSPs);
    			break;
    			
    		case "INI_MSPs":
    			
    			
      			if(Fixed)
    			{
      				INI_MSPs = (int) factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				INI_MSPs = (int) factor.getHigh();
    			else
    				INI_MSPs = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+INI_MSPs);
    			break;
    			
    		case "INI_ME_MPSPs":
    			
    			
      			if(Fixed)
    			{
      				INI_ME_MPSPs = (int) factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				INI_ME_MPSPs = (int) factor.getHigh();
    			else
    				INI_ME_MPSPs = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+INI_ME_MPSPs);
    			break;
    			
    		case "INI_ME_MSPs":
    			
      			if(Fixed)
    			{
      				INI_ME_MSPs = (int) factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				INI_ME_MSPs = (int) factor.getHigh();
    			else
    				INI_ME_MSPs = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+INI_ME_MSPs);
    			break;
    			
    		case "MPSPsGR":
    			
      			if(Fixed)
    			{
      				MPSPsGR = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				MPSPsGR = factor.getHigh();
    			else
    				MPSPsGR = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+MPSPsGR);
    			break;
    			
    		case "ME_MPSPsGR":
    			
      			if(Fixed)
    			{
      				ME_MPSPsGR = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				ME_MPSPsGR = factor.getHigh();
    			else
    				ME_MPSPsGR = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+ME_MPSPsGR);
    			break;
    			
    		case "MSPsGR":
    			
      			if(Fixed)
    			{
      				MSPsGR = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				MSPsGR = factor.getHigh();
    			else
    				MSPsGR = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+MSPsGR);
    			break;
    			
    		case "ME_MSPsGR":
    			
      			if(Fixed)
    			{
      				ME_MSPsGR = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				ME_MSPsGR = factor.getHigh();
    			else
    				ME_MSPsGR = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+ME_MSPsGR);
    			break;
    		
    		case "SPAM_Delay_Period":
    			
      			if(Fixed)
    			{
      				SPAM_Delay_Period = (int) factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SPAM_Delay_Period = (int) factor.getHigh();
    			else
    				SPAM_Delay_Period = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SPAM_Delay_Period);
    			break;
    			
    		case "SPAM_Bombarding_Period":
    			
      			if(Fixed)
    			{
      				SPAM_Bombarding_Period = (int) factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SPAM_Bombarding_Period = (int) factor.getHigh();
    			else
    				SPAM_Bombarding_Period = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SPAM_Bombarding_Period);
    			break;
    			
    		case "SPAM_DropUser_Rate":
    			
      			if(Fixed)
    			{
      				SPAM_DropUser_Rate = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SPAM_DropUser_Rate = factor.getHigh();
    			else
    				SPAM_DropUser_Rate = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SPAM_DropUser_Rate);
    			break;
    			
    		case "SPAM_DropCredential_Rate":
    			
      			if(Fixed)
    			{
      				SPAM_DropCredential_Rate = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SPAM_DropCredential_Rate = factor.getHigh();
    			else
    				SPAM_DropCredential_Rate = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SPAM_DropCredential_Rate);
    			break;
 
    		case "SPAM_Delay_ME_Period":
    			
      			if(Fixed)
    			{
      				SPAM_Delay_ME_Period = (int) factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SPAM_Delay_ME_Period = (int) factor.getHigh();
    			else
    				SPAM_Delay_ME_Period = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SPAM_Delay_ME_Period);
    			break;
    			
    		case "SPAM_Bombarding_ME_Period":
    			
      			if(Fixed)
    			{
      				SPAM_Bombarding_ME_Period = (int) factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SPAM_Bombarding_ME_Period = (int) factor.getHigh();
    			else
    				SPAM_Bombarding_ME_Period = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SPAM_Bombarding_ME_Period);
    			break;
    			
    		case "SPAM_DropUser_ME_Rate":
    			
      			if(Fixed)
    			{
      				SPAM_DropUser_ME_Rate = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SPAM_DropUser_ME_Rate = factor.getHigh();
    			else
    				SPAM_DropUser_ME_Rate = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SPAM_DropUser_ME_Rate);
    			break;
    			
    		case "SPAM_DropCredential_ME_Rate":
    			
      			if(Fixed)
    			{
      				SPAM_DropCredential_ME_Rate = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SPAM_DropCredential_ME_Rate = factor.getHigh();
    			else
    				SPAM_DropCredential_ME_Rate = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SPAM_DropCredential_ME_Rate);
    			break;
    			
    		case "ME_N_MIN_MPSPs_toSPAM_User":
    			
      			if(Fixed)
    			{
      				ME_N_MIN_MPSPs_toSPAM_User = (int) factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				ME_N_MIN_MPSPs_toSPAM_User = (int) factor.getHigh();
    			else
    				ME_N_MIN_MPSPs_toSPAM_User = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+ME_N_MIN_MPSPs_toSPAM_User);
    			break;
    			
    		case "ME_N_MIN_MPSPs_toSPAM_Credential":
    			
      			if(Fixed)
    			{
      				ME_N_MIN_MPSPs_toSPAM_Credential = (int) factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				ME_N_MIN_MPSPs_toSPAM_Credential = (int) factor.getHigh();
    			else
    				ME_N_MIN_MPSPs_toSPAM_Credential = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+ME_N_MIN_MPSPs_toSPAM_Credential);
    			break;
    			
    		default:
    			System.out.println("Factor: "+factor.getFactor()+" is not found!");
    			break;
    		}
    	}
    	
    	Random rGenerator = new Random();
    	
    	int networkSize = Network.size();
    	
    	//TODO: monitor how these methods round numbers
    	//
    	//double sumUsers = (Users * (networkSize - 3) )/ 100;
    	double usersPointer = INI_Users+3;
    	
    	//double sumPSPs = (SPs * (usefulnessPCT/100) * (networkSize - 3) )/ 100;
    	//double sumMPSPs =  (MPSPS_PCT/100) * sumPSPs;
    	
    	//sumPSPs = sumPSPs - sumMPSPs;
    	double mpspsPointer = usersPointer+INI_MPSPs;
    	double pspsPointer = mpspsPointer+INI_PSPs;
    	//
    	
    	double sumMPSP_SPAM_Delay = (SPAM_Delay_MPSP_PCT/100) * INI_MPSPs;
    	double sumMPSP_SPAM_Bombarding = (SPAM_Bombarding_MPSP_PCT/100) * INI_MPSPs;
    	double sumMPSP_SPAM_Drop = (SPAM_Drop_MPSP_PCT/100) * INI_MPSPs;
    	double sumMPSP_SPAM_Delay_Bombarding = (SPAM_Delay_Bombarding_MPSP_PCT/100) * INI_MPSPs;
    	double sumMPSP_SPAM_Delay_Drop = (SPAM_Delay_Drop_MPSP_PCT/100) * INI_MPSPs;
    	double sumMPSP_SPAM_Drop_Bombarding = (SPAM_Drop_Bombarding_MPSP_PCT/100) * INI_MPSPs;
    	double sumMPSP_SPAM_Delay_Drop_Bombarding = (SPAM_Delay_Drop_Bombarding_MPSP_PCT/100) * INI_MPSPs;
    	
    	//double sumSPs = ((SPs * (networkSize - 3) )/ 100) - (sumPSPs+sumMPSPs);
    	//double sumMSPs =  (MSPS_PCT/100) * sumSPs;

    	double sumMSP_SPAM_Delay = (SPAM_Delay_MSP_PCT/100) * INI_MSPs;
    	double sumMSP_SPAM_Bombarding = (SPAM_Bombarding_MSP_PCT/100) * INI_MSPs;
    	double sumMSP_SPAM_Drop = (SPAM_Drop_MSP_PCT/100) * INI_MSPs;
    	double sumMSP_SPAM_Delay_Bombarding = (SPAM_Delay_Bombarding_MSP_PCT/100) * INI_MSPs;
    	double sumMSP_SPAM_Delay_Drop = (SPAM_Delay_Drop_MSP_PCT/100) * INI_MSPs;
    	double sumMSP_SPAM_Drop_Bombarding = (SPAM_Drop_Bombarding_MSP_PCT/100) * INI_MSPs;
    	double sumMSP_SPAM_Delay_Drop_Bombarding = (SPAM_Delay_Drop_Bombarding_MSP_PCT/100) * INI_MSPs;
    	
    	//
    	//sumSPs = sumSPs - sumMSPs;
    	double mspsPointer = pspsPointer+INI_MSPs;
    	double spsPointer = mspsPointer+INI_SPs;
    	
    	double sumME_ColludingUnpopular = (INI_ME * ME_Colluding_Unpopular_PCT/100);
    	double sumME_ColludingPopular = INI_ME - sumME_ColludingUnpopular;
    	
    	double ME_ColludingUnpopular_Pointer = spsPointer + sumME_ColludingUnpopular * (INI_ME_MPSPs + INI_ME_MSPs);
    	
    	/* 	
    	System.out.println("INI_Users = "+INI_Users+" usersPointer = "+usersPointer);
    	System.out.println("INI_MPSPs = "+INI_MPSPs+" mpspsPointer = "+mpspsPointer);
    	System.out.println("INI_PSPs = "+INI_PSPs+" pspsPointer = "+pspsPointer);
    	System.out.println("INI_MSPs = "+INI_MSPs+" mspsPointer = "+mspsPointer);
    	System.out.println("INI_SPs = "+INI_SPs+" spsPointer = "+spsPointer);
    	System.out.println("INI_ME = "+INI_ME+" sumME_ColludingUnpopular = "+sumME_ColludingUnpopular+" sumME_ColludingPopular = "+sumME_ColludingPopular+" ME_ColludingUnpopular_Pointer = "+ME_ColludingUnpopular_Pointer);
    	//
    	System.out.println("");
    	System.out.println("INI_MPSPs = "+INI_MPSPs);
    	System.out.println(":: sumMPSP_SPAM_Delay = "+sumMPSP_SPAM_Delay);
    	System.out.println(":: sumMPSP_SPAM_Bombarding = "+sumMPSP_SPAM_Bombarding);
    	System.out.println(":: sumMPSP_SPAM_Drop = "+sumMPSP_SPAM_Drop);
    	System.out.println(":: sumMPSP_SPAM_Delay_Bombarding = "+sumMPSP_SPAM_Delay_Bombarding);
    	System.out.println(":: sumMPSP_SPAM_Delay_Drop = "+sumMPSP_SPAM_Delay_Drop);
    	System.out.println(":: sumMPSP_SPAM_Drop_Bombarding = "+sumMPSP_SPAM_Drop_Bombarding);
    	System.out.println(":: sumMPSP_SPAM_Delay_Drop_Bombarding = "+sumMPSP_SPAM_Delay_Drop_Bombarding);
    	
    	System.out.println("");
    	System.out.println("INI_MSPs = "+INI_MSPs);
    	System.out.println(":: sumMSP_SPAM_Delay = "+sumMSP_SPAM_Delay);
    	System.out.println(":: sumMSP_SPAM_Bombarding = "+sumMSP_SPAM_Bombarding);
    	System.out.println(":: sumMSP_SPAM_Drop = "+sumMSP_SPAM_Drop);
    	System.out.println(":: sumMSP_SPAM_Delay_Bombarding = "+sumMSP_SPAM_Delay_Bombarding);
    	System.out.println(":: sumMSP_SPAM_Delay_Drop = "+sumMSP_SPAM_Delay_Drop);
    	System.out.println(":: sumMSP_SPAM_Drop_Bombarding = "+sumMSP_SPAM_Drop_Bombarding);
    	System.out.println(":: sumMSP_SPAM_Delay_Drop_Bombarding = "+sumMSP_SPAM_Delay_Drop_Bombarding);
   		*/
    	System.out.println("");
    	
    	double SPAM_Delay_Pointer = SPAM_Delay_MPSP_PCT;
    	double SPAM_Bombarding_Pointer = SPAM_Delay_Pointer + SPAM_Bombarding_MPSP_PCT;
    	double SPAM_Drop_Pointer = SPAM_Bombarding_Pointer + SPAM_Drop_MPSP_PCT;
    	double SPAM_Delay_Bombarding_Pointer = SPAM_Drop_Pointer + SPAM_Delay_Bombarding_MPSP_PCT;
    	double SPAM_Delay_Drop_Pointer = SPAM_Delay_Bombarding_Pointer + SPAM_Delay_Drop_MPSP_PCT;
    	double SPAM_Drop_Bombarding_Pointer = SPAM_Delay_Drop_Pointer + SPAM_Drop_Bombarding_MPSP_PCT;
    	double SPAM_Delay_Drop_Bombarding_Pointer = SPAM_Drop_Bombarding_Pointer + SPAM_Delay_Drop_Bombarding_MPSP_PCT;
    	
    	int countMPSP_SPAM_Delay = 0;
    	int countMPSP_SPAM_Bombarding = 0;
    	int countMPSP_SPAM_Drop = 0;
    	int countMPSP_SPAM_Delay_Bombarding = 0;
    	int countMPSP_SPAM_Delay_Drop = 0;
    	int countMPSP_SPAM_Drop_Bombarding = 0;
    	int countMPSP_SPAM_Delay_Drop_Bombarding = 0;
    	
    	int countMSP_SPAM_Delay = 0;
    	int countMSP_SPAM_Bombarding = 0;
    	int countMSP_SPAM_Drop = 0;
    	int countMSP_SPAM_Delay_Bombarding = 0;
    	int countMSP_SPAM_Delay_Drop = 0;
    	int countMSP_SPAM_Drop_Bombarding = 0;
    	int countMSP_SPAM_Delay_Drop_Bombarding = 0;
    	
    	IDP_Node idp = (IDP_Node) Network.get(1).getProtocol(IDPPid);
    	
        for (int i = 0 ; i < networkSize; i++) {

        	Node n = Network.get(i);
            
            if(i == 0)
            {
            	Super_Node node = (Super_Node) n.getProtocol(SuperPid);
            	node.setTrial(trial);
            	node.setFactorsList(FactorsList);
            	node.setNodePos(i);
            	node.setRMGRPid(RMGRPid);
            	node.setSuperPid(SuperPid);
            	node.setIDPPid(IDPPid);
            	node.setAuditorPid(AuditorPid);
            	node.setUserPid(UserPid);
            	node.setSPPid(SPPid);
            	node.setMEPid(MEPid);
            	node.setNodeType(0);
            	node.setrGen(rGenerator);
            	node.setUsefulnessMIN(usefulnessMIN);
            	node.setUsersGR(UsersGR);
            	node.setPSPsGR(PSPsGR);
            	node.setSPsGR(SPsGR);
            	node.setMSPsGR(MSPsGR);
            	node.setMPSPsGR(MPSPsGR);
            	node.setMEGR(MEGR);
            	node.setUsersCount(INI_Users);
            	node.setPSPsCount(INI_PSPs);
            	node.setSPsCount(INI_SPs);
            	node.setMPSPsCount(INI_MPSPs);
            	node.setMSPsCount(INI_MSPs);
            	node.setMECount(INI_ME);
            	node.setOfferDGUProb(OfferDGUProb);
            	node.setOfferStrictDGUProb(OfferStrictDGUProb);
            	
            	//System.out.println("SuperNodeId: "+node.getNodePos()+" nodeType: "+node.getNodeType()+" SuperPid: "+node.getSuperPid());
            }
            
            if(i == 1)
            {
            	IDP_Node node = (IDP_Node) n.getProtocol(IDPPid);
            	node.setNodePos(i);
            	node.setRMGRPid(RMGRPid);
            	node.setSuperPid(SuperPid);
            	node.setIDPPid(IDPPid);
            	node.setAuditorPid(AuditorPid);
            	node.setUserPid(UserPid);
            	node.setSPPid(SPPid);
            	node.setMEPid(MEPid);
            	node.setNodeType(1);
            	node.setrGen(rGenerator);
            	node.setUsefulnessMIN(usefulnessMIN);
            	node.setDeployDGU(DeployDGU);
            	
            	//System.out.println("IDPid: "+i);
            }
            else if (i == 2)
            {
            	Auditor_Node node = (Auditor_Node) n.getProtocol(AuditorPid);
            	node.setNodePos(i);
            	node.setNodeType(2);
            	node.setrGen(rGenerator);
            	node.setRMGRPid(RMGRPid);
            	node.setSuperPid(SuperPid);
            	node.setIDPPid(IDPPid);
            	node.setAuditorPid(AuditorPid);
            	node.setUserPid(UserPid);
            	node.setSPPid(SPPid);
            	node.setMEPid(MEPid);
            	node.setUsefulnessMIN(usefulnessMIN);
            	node.setTGTWholeWeight(TGTWholeWeight);
            	node.setTGTWeight(TGTWeight);
            	node.setTGTColludingWeight(TGTColludingWeight);
            	node.setTGTWeakColludingWeight(TGTWeakColludingWeight);
            	node.setTLRavgWeight(TLRavgWeight);
            	node.setTSTWeight(TSTWeight);
            	node.setSuspiciousSPRank(SuspiciousSPRank);
            	node.setSuspiciousSPRange(SuspiciousSPRange);
            	node.setSuspiciousSPBanningRank(SuspiciousSPBanningRank);
            	node.setPostDGUinstallationRank(postDGUinstallationRank);
            	node.setPostStrictDGUEnableRank(postStrictDGUEnableRank);
            	node.setDeployDGU(DeployDGU);
            	node.setSTagentStatusPostSpam(STagentStatusPostSpam);
            	node.setSTmaxLifeTime(STmaxLifeTime);
            	node.setTopTLRavgPCT_ST(TopTLRavgPCT_ST);
            	node.setBottomTLRavgPCT_ST(BottomTLRavgPCT_ST);
            	node.setPSL_STselector(PSL_STselector);
            	node.setSuspiciousNodes_STselector(SuspiciousNodes_STselector);
            	node.setTopTLRavg_STselector(TopTLRavg_STselector);
            	node.setBottomTLRavg_STselector(BottomTLRavg_STselector);
            	node.setPSL_PCT(PSL_PCT);
            	node.setSufficientTLRus_PSP(SufficientTLRus_PSP);
            	node.setSufficientTLRus_SP(SufficientTLRus_SP);
            	node.setDeployST(DeployST);
            	node.setPIIL_GTselector(PIIL_GTselector);
            	node.setPIIL_PCT(PIIL_PCT);
            	node.setIIR_STselector(IIR_STselector);
            	node.setDeployGT(DeployGT);
            	node.setGT_avgTGT(GT_avgTGT);
            	node.setGTagentStatusPostSpam(GTagentStatusPostSpam);
            	node.setGTmaxLifeTime(GTmaxLifeTime);
            	node.setPcrThreshold(PcrThreshold);
            	node.setPcrWeakThreshold(PcrWeakThreshold);
            	node.setIgnoreOldGranks(IgnoreOldGranks);
                node.setStopTestingIfMinReached(StopTestingIfMinReached);
                node.setGT_SelectOnlyPSP(GT_SelectOnlyPSP);
                node.setGT_MaxSPNum(GT_MaxSPNum);
                node.setGT_MaxSize(GT_MaxSize);
            	node.setPSL_GTselector(PSL_GTselector);
            	node.setSuspiciousNodes_GTselector(SuspiciousNodes_GTselector);
            	
            	//System.out.println("Auditor_id: "+i);
            }
            else if(i > 2 && i < usersPointer)
            {
            	User_Node node = (User_Node) n.getProtocol(UserPid);
            	node.setNodePos(i);
            	node.setRMGRPid(RMGRPid);
            	node.setSuperPid(SuperPid);
            	node.setIDPPid(IDPPid);
            	node.setAuditorPid(AuditorPid);
            	node.setUserPid(UserPid);
            	node.setSPPid(SPPid);
            	node.setMEPid(MEPid);
            	node.setNodeType(3);
            	node.setrGen(rGenerator);
            	node.setGenerateNewCredentialRate(GenerateNewCredentialRate);
            	node.setGenerateNewServiceRequestRate(GenerateNewServiceRequestRate);
            	node.setCredentialsArray(CredentialsArraySize);
            	node.setStrictCredentialProb(StrictCredentialProb);
            	//Every User should get at least one Credential initially
            	//First Credential is normally strict and important
            	node.createNewCredential(false, false, false, IniCredentialType);
            	node.setTrustInNetwork(TrustInNetwork);
            	node.setIgnoranceRate(IgnoranceRate);
            	node.setUsefulnessMIN(usefulnessMIN);
            	node.setStopUsingCredentialAfterSpam(StopUsingCredentialAfterSpam);
            	node.setTrustDropPostSpam(TrustDropPostSpam);
            	node.setTrustIncreasePostSpamResolved(TrustIncreasePostSpamResolved);
            	node.setTrustIncreaseRate(TrustIncreaseRate);
            	node.setTrustDecreaseRate(TrustDecreaseRate);
            	node.setTrustDecreasePeriodPostSpam(TrustDecreasePeriodPostSpam);
            	
            	idp.addNodeDir(i, 3, SPRank);
            	
            	//System.out.println("Userid: "+i);
            }
            else if(i >= usersPointer && i < mpspsPointer)
            {
            	SP_Node node = (SP_Node) n.getProtocol(SPPid);
            	node.setNodePos(i);
            	node.setRMGRPid(RMGRPid);
            	node.setSuperPid(SuperPid);
            	node.setIDPPid(IDPPid);
            	node.setAuditorPid(AuditorPid);
            	node.setUserPid(UserPid);
            	node.setSPPid(SPPid);
            	node.setMEPid(MEPid);
            	node.setNodeType(4);
            	node.setrGen(rGenerator);
            	node.setUsefulnessMIN(usefulnessMIN);
            	node.setUsefulness(node.randDouble(usefulnessMIN, 100));
            	node.setMalicious(true);
            	node.setSPAM_Drop_StrictCredential_Prob(SPAM_Drop_StrictCredential_Prob);
            	node.setNewPartnershipProb(newPartnershipProb);
            	node.setNewPartnershipDuration(newPartnershipDuration);
                node.setPartnerSharingProb(partnerSharingProb);
                node.setAcceptDGUProb(AcceptDGUProb);
                node.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
            	
            	idp.addNodeDir(i, 4, SPRank);
            	
            	double randAttack = node.randDouble(0, 100);

            	if(countMPSP_SPAM_Delay < sumMPSP_SPAM_Delay && ( (sumMPSP_SPAM_Delay - countMPSP_SPAM_Delay) > 1 || (randAttack >= 0 && randAttack < SPAM_Delay_Pointer) ) )
            	{

        			node.setSPAM_Delay(true);
        			node.setSPAM_Delay_Period(SPAM_Delay_Period);
        			
            		//System.out.println("");
            		//System.out.println("MPSP "+i+" is SPAM_Delay");
        			
            		countMPSP_SPAM_Delay++;
            	}
            	else if(countMPSP_SPAM_Bombarding < sumMPSP_SPAM_Bombarding && ( (sumMPSP_SPAM_Bombarding - countMPSP_SPAM_Bombarding) > 1 || (randAttack >= SPAM_Delay_Pointer && randAttack < SPAM_Bombarding_Pointer) ) )
            	{

        			node.setSPAM_Bombarding(true);
        			node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
        			
            		//System.out.println("");
            		//System.out.println("MPSP "+i+" is SPAM_Bombarding");
        	            		
            		countMPSP_SPAM_Bombarding++;
            	}
            	else if(countMPSP_SPAM_Drop < sumMPSP_SPAM_Drop && ( (sumMPSP_SPAM_Drop - countMPSP_SPAM_Drop) > 1 || (randAttack >= SPAM_Bombarding_Pointer && randAttack < SPAM_Drop_Pointer) ) )
            	{
            		node.setSPAM_Drop(true);
            		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
            		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
            		
            		//System.out.println("");
            		//System.out.println("MPSP "+i+" is SPAM_Drop");
        			
            		countMPSP_SPAM_Drop++;
            	}
            	else if(countMPSP_SPAM_Delay_Bombarding < sumMPSP_SPAM_Delay_Bombarding && ( (sumMPSP_SPAM_Delay_Bombarding - countMPSP_SPAM_Delay_Bombarding) > 1 || (randAttack >= SPAM_Drop_Pointer && randAttack < SPAM_Delay_Bombarding_Pointer) ) )
            	{
            		node.setSPAM_Delay(true);
            		node.setSPAM_Delay_Period(SPAM_Delay_Period);
            		
            		node.setSPAM_Bombarding(true);
            		node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
            				
            		//System.out.println("");
            		//System.out.println("MPSP "+i+" is SPAM_Delay_Bombarding");
        	
            		countMPSP_SPAM_Delay_Bombarding++;
            	}
            	else if(countMPSP_SPAM_Delay_Drop < sumMPSP_SPAM_Delay_Drop && ( (sumMPSP_SPAM_Delay_Drop - countMPSP_SPAM_Delay_Drop) > 1 || (randAttack >= SPAM_Delay_Bombarding_Pointer && randAttack < SPAM_Delay_Drop_Pointer) ) )
            	{
            		node.setSPAM_Delay(true);
            		node.setSPAM_Delay_Period(SPAM_Delay_Period);
            		
            		node.setSPAM_Drop(true);
            		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
            		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
	
            		//System.out.println("");
            		//System.out.println("MPSP "+i+" is SPAM_Delay_Drop");
            		
            		countMPSP_SPAM_Delay_Drop++;
            	}
            	else if(countMPSP_SPAM_Drop_Bombarding < sumMPSP_SPAM_Drop_Bombarding && ( (sumMPSP_SPAM_Drop_Bombarding - countMPSP_SPAM_Drop_Bombarding) > 1 || (randAttack >= SPAM_Delay_Drop_Pointer && randAttack < SPAM_Drop_Bombarding_Pointer) ) )
            	{
        			node.setSPAM_Drop(true);
            		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
            		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
            		
            		node.setSPAM_Bombarding(true);
            		node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
            				
            		//System.out.println("");
            		//System.out.println("MPSP "+i+" is SPAM_Drop_Bombarding");
        		
            		countMPSP_SPAM_Drop_Bombarding++;
            	}
            	else if(countMPSP_SPAM_Delay_Drop_Bombarding < sumMPSP_SPAM_Delay_Drop_Bombarding && ( (sumMPSP_SPAM_Delay_Drop_Bombarding - countMPSP_SPAM_Delay_Drop_Bombarding) > 1 || (randAttack >= SPAM_Drop_Bombarding_Pointer && randAttack <= SPAM_Delay_Drop_Bombarding_Pointer) ))
            	{
        			node.setSPAM_Delay(true);
            		node.setSPAM_Delay_Period(SPAM_Delay_Period);
            		
            		node.setSPAM_Drop(true);
            		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
            		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
            		
            		node.setSPAM_Bombarding(true);
            		node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
	
            		//System.out.println("");
            		//System.out.println("MPSP "+i+" is SPAM_Delay_Drop_Bombarding");
            		
            		countMPSP_SPAM_Delay_Drop_Bombarding++;
            	}
            	else
            	{
            		if(randAttack >= 0 && randAttack < SPAM_Delay_Pointer)
            		{
            			node.setSPAM_Delay(true);
            			node.setSPAM_Delay_Period(SPAM_Delay_Period);
            			
                		//System.out.println("");
                		//System.out.println("MPSP "+i+" is SPAM_Delay");
            			
                		countMPSP_SPAM_Delay++;
            		}
            		else if(randAttack >= SPAM_Delay_Pointer && randAttack < SPAM_Bombarding_Pointer)
            		{
            			node.setSPAM_Bombarding(true);
            			node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
            			
                		//System.out.println("");
                		//System.out.println("MPSP "+i+" is SPAM_Bombarding");
            	            		
                		countMPSP_SPAM_Bombarding++;
            		}
            		else if(randAttack >= SPAM_Bombarding_Pointer && randAttack < SPAM_Drop_Pointer)
            		{
                		node.setSPAM_Drop(true);
                		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
                		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
                		
                		//System.out.println("");
                		//System.out.println("MPSP "+i+" is SPAM_Drop");
            			
                		countMPSP_SPAM_Drop++;
            		}
            		else if(randAttack >= SPAM_Drop_Pointer && randAttack < SPAM_Delay_Bombarding_Pointer)
            		{
                		node.setSPAM_Delay(true);
                		node.setSPAM_Delay_Period(SPAM_Delay_Period);
                		
                		node.setSPAM_Bombarding(true);
                		node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
                				
                		//System.out.println("");
                		//System.out.println("MPSP "+i+" is SPAM_Delay_Bombarding");
            	
                		countMPSP_SPAM_Delay_Bombarding++;
            		}
            		else if(randAttack >= SPAM_Delay_Bombarding_Pointer && randAttack < SPAM_Delay_Drop_Pointer)
            		{
                   		node.setSPAM_Delay(true);
                		node.setSPAM_Delay_Period(SPAM_Delay_Period);
                		
                		node.setSPAM_Drop(true);
                		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
                		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
    	
                		//System.out.println("");
                		//System.out.println("MPSP "+i+" is SPAM_Delay_Drop");
                		
                		countMPSP_SPAM_Delay_Drop++;
            		}
            		else if(randAttack >= SPAM_Delay_Drop_Pointer && randAttack < SPAM_Drop_Bombarding_Pointer)
            		{
            			node.setSPAM_Drop(true);
                		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
                		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
                		
                		node.setSPAM_Bombarding(true);
                		node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
                				
                		//System.out.println("");
                		//System.out.println("MPSP "+i+" is SPAM_Drop_Bombarding");
            		
                		countMPSP_SPAM_Drop_Bombarding++;
            		}
            		else if(randAttack >= SPAM_Drop_Bombarding_Pointer && randAttack <= SPAM_Delay_Drop_Bombarding_Pointer)
            		{
            			node.setSPAM_Delay(true);
                		node.setSPAM_Delay_Period(SPAM_Delay_Period);
                		
                		node.setSPAM_Drop(true);
                		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
                		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
                		
                		node.setSPAM_Bombarding(true);
                		node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
    	
                		//System.out.println("");
                		//System.out.println("MPSP "+i+" is SPAM_Delay_Drop_Bombarding");
                		
                		countMPSP_SPAM_Delay_Drop_Bombarding++;
            		}
            	}

            	//System.out.println("MPSPid: "+i);	
            	//System.out.println("Node: "+node.getNodePos()+" has Usefulness = "+node.getUsefulness());
            }
            else if(i >= mpspsPointer && i < pspsPointer)
            {
            	SP_Node node = (SP_Node) n.getProtocol(SPPid);
            	node.setNodePos(i);
            	node.setRMGRPid(RMGRPid);
            	node.setSuperPid(SuperPid);
            	node.setIDPPid(IDPPid);
            	node.setAuditorPid(AuditorPid);
            	node.setUserPid(UserPid);
            	node.setSPPid(SPPid);
            	node.setMEPid(MEPid);
            	node.setNodeType(4);
            	node.setrGen(rGenerator);
            	node.setUsefulnessMIN(usefulnessMIN);
            	node.setUsefulness(node.randDouble(usefulnessMIN, 100));     
            	node.setNewPartnershipProb(newPartnershipProb);
            	node.setNewPartnershipDuration(newPartnershipDuration);
                node.setPartnerSharingProb(partnerSharingProb);
                node.setAcceptDGUProb(AcceptDGUProb);
                node.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
            	node.setAcceptCompulsoryDGUProb(AcceptCompulsoryDGUProb);
            	node.setAcceptCompulsoryStrictDGUProb(AcceptCompulsoryStrictDGUProb);
                                
            	idp.addNodeDir(i, 4, SPRank);
            	
            	//System.out.println("PSPid: "+i);
            }
            else if(i >= pspsPointer && i < mspsPointer)
            {
            	SP_Node node = (SP_Node) n.getProtocol(SPPid);
            	node.setNodePos(i);
            	node.setRMGRPid(RMGRPid);
            	node.setSuperPid(SuperPid);
            	node.setIDPPid(IDPPid);
            	node.setAuditorPid(AuditorPid);
            	node.setUserPid(UserPid);
            	node.setSPPid(SPPid);
            	node.setMEPid(MEPid);
            	node.setNodeType(4);
            	node.setrGen(rGenerator);
            	node.setUsefulnessMIN(usefulnessMIN);
            	node.setUsefulness(node.randDouble(0, usefulnessMIN));
            	node.setMalicious(true);
            	node.setSPAM_Drop_StrictCredential_Prob(SPAM_Drop_StrictCredential_Prob);  
            	node.setNewPartnershipProb(newPartnershipProb);
            	node.setNewPartnershipDuration(newPartnershipDuration);
                node.setPartnerSharingProb(partnerSharingProb);
                node.setAcceptDGUProb(AcceptDGUProb);
                node.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
            	
            	idp.addNodeDir(i, 4, SPRank);
            	
            	double randAttack = node.randDouble(0, 100);

            	if(countMSP_SPAM_Delay < sumMSP_SPAM_Delay && ( (sumMSP_SPAM_Delay - countMSP_SPAM_Delay) > 1 || (randAttack >= 0 && randAttack < SPAM_Delay_Pointer) ) )
            	{

        			node.setSPAM_Delay(true);
        			node.setSPAM_Delay_Period(SPAM_Delay_Period);
        			
            		//System.out.println("");
            		//System.out.println("MSP "+i+" is SPAM_Delay");
        			
            		countMSP_SPAM_Delay++;
            	}
            	else if(countMSP_SPAM_Bombarding < sumMSP_SPAM_Bombarding && ( (sumMSP_SPAM_Bombarding - countMSP_SPAM_Bombarding) > 1 || (randAttack >= SPAM_Delay_Pointer && randAttack < SPAM_Bombarding_Pointer) ) )
            	{

        			node.setSPAM_Bombarding(true);
        			node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
        			
            		//System.out.println("");
            		//System.out.println("MSP "+i+" is SPAM_Bombarding");
        	            		
            		countMSP_SPAM_Bombarding++;
            	}
            	else if(countMSP_SPAM_Drop < sumMSP_SPAM_Drop && ( (sumMSP_SPAM_Drop - countMSP_SPAM_Drop) > 1 || (randAttack >= SPAM_Bombarding_Pointer && randAttack < SPAM_Drop_Pointer) ) )
            	{
            		node.setSPAM_Drop(true);
            		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
            		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
            		
            		//System.out.println("");
            		//System.out.println("MSP "+i+" is SPAM_Drop");
        			
            		countMSP_SPAM_Drop++;
            	}
            	else if(countMSP_SPAM_Delay_Bombarding < sumMSP_SPAM_Delay_Bombarding && ( (sumMSP_SPAM_Delay_Bombarding - countMSP_SPAM_Delay_Bombarding) > 1 || (randAttack >= SPAM_Drop_Pointer && randAttack < SPAM_Delay_Bombarding_Pointer) ) )
            	{
            		node.setSPAM_Delay(true);
            		node.setSPAM_Delay_Period(SPAM_Delay_Period);
            		
            		node.setSPAM_Bombarding(true);
            		node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
            				
            		//System.out.println("");
            		//System.out.println("MSP "+i+" is SPAM_Delay_Bombarding");
        	
            		countMSP_SPAM_Delay_Bombarding++;
            	}
            	else if(countMSP_SPAM_Delay_Drop < sumMSP_SPAM_Delay_Drop && ( (sumMSP_SPAM_Delay_Drop - countMSP_SPAM_Delay_Drop) > 1 || (randAttack >= SPAM_Delay_Bombarding_Pointer && randAttack < SPAM_Delay_Drop_Pointer) ) )
            	{
            		node.setSPAM_Delay(true);
            		node.setSPAM_Delay_Period(SPAM_Delay_Period);
            		
            		node.setSPAM_Drop(true);
            		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
            		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
	
            		//System.out.println("");
            		//System.out.println("MSP "+i+" is SPAM_Delay_Drop");
            		
            		countMSP_SPAM_Delay_Drop++;
            	}
            	else if(countMSP_SPAM_Drop_Bombarding < sumMSP_SPAM_Drop_Bombarding && ( (sumMSP_SPAM_Drop_Bombarding - countMSP_SPAM_Drop_Bombarding) > 1 || (randAttack >= SPAM_Delay_Drop_Pointer && randAttack < SPAM_Drop_Bombarding_Pointer) ) )
            	{
        			node.setSPAM_Drop(true);
            		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
            		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
            		
            		node.setSPAM_Bombarding(true);
            		node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
            				
            		//System.out.println("");
            		//System.out.println("MSP "+i+" is SPAM_Drop_Bombarding");
        		
            		countMSP_SPAM_Drop_Bombarding++;
            	}
            	else if(countMSP_SPAM_Delay_Drop_Bombarding < sumMSP_SPAM_Delay_Drop_Bombarding && ( (sumMSP_SPAM_Delay_Drop_Bombarding - countMSP_SPAM_Delay_Drop_Bombarding) > 1 || (randAttack >= SPAM_Drop_Bombarding_Pointer && randAttack <= SPAM_Delay_Drop_Bombarding_Pointer) ))
            	{
        			node.setSPAM_Delay(true);
            		node.setSPAM_Delay_Period(SPAM_Delay_Period);
            		
            		node.setSPAM_Drop(true);
            		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
            		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
            		
            		node.setSPAM_Bombarding(true);
            		node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
	
            		//System.out.println("");
            		//System.out.println("MSP "+i+" is SPAM_Delay_Drop_Bombarding");
            		
            		countMSP_SPAM_Delay_Drop_Bombarding++;
            	}
            	else
            	{
            		if(randAttack >= 0 && randAttack < SPAM_Delay_Pointer)
            		{
            			node.setSPAM_Delay(true);
            			node.setSPAM_Delay_Period(SPAM_Delay_Period);
            			
                		//System.out.println("");
                		//System.out.println("MSP "+i+" is SPAM_Delay");
            			
                		countMSP_SPAM_Delay++;
            		}
            		else if(randAttack >= SPAM_Delay_Pointer && randAttack < SPAM_Bombarding_Pointer)
            		{
            			node.setSPAM_Bombarding(true);
            			node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
            			
                		//System.out.println("");
                		//System.out.println("MSP "+i+" is SPAM_Bombarding");
            	            		
                		countMSP_SPAM_Bombarding++;
            		}
            		else if(randAttack >= SPAM_Bombarding_Pointer && randAttack < SPAM_Drop_Pointer)
            		{
                		node.setSPAM_Drop(true);
                		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
                		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
                		
                		//System.out.println("");
                		//System.out.println("MSP "+i+" is SPAM_Drop");
            			
                		countMSP_SPAM_Drop++;
            		}
            		else if(randAttack >= SPAM_Drop_Pointer && randAttack < SPAM_Delay_Bombarding_Pointer)
            		{
                		node.setSPAM_Delay(true);
                		node.setSPAM_Delay_Period(SPAM_Delay_Period);
                		
                		node.setSPAM_Bombarding(true);
                		node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
                				
                		//System.out.println("");
                		//System.out.println("MSP "+i+" is SPAM_Delay_Bombarding");
            	
                		countMSP_SPAM_Delay_Bombarding++;
            		}
            		else if(randAttack >= SPAM_Delay_Bombarding_Pointer && randAttack < SPAM_Delay_Drop_Pointer)
            		{
                   		node.setSPAM_Delay(true);
                		node.setSPAM_Delay_Period(SPAM_Delay_Period);
                		
                		node.setSPAM_Drop(true);
                		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
                		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
    	
                		//System.out.println("");
                		//System.out.println("MSP "+i+" is SPAM_Delay_Drop");
                		
                		countMSP_SPAM_Delay_Drop++;
            		}
            		else if(randAttack >= SPAM_Delay_Drop_Pointer && randAttack < SPAM_Drop_Bombarding_Pointer)
            		{
            			node.setSPAM_Drop(true);
                		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
                		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
                		
                		node.setSPAM_Bombarding(true);
                		node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
                				
                		//System.out.println("");
                		//System.out.println("MSP "+i+" is SPAM_Drop_Bombarding");
            		
                		countMSP_SPAM_Drop_Bombarding++;
            		}
            		else if(randAttack >= SPAM_Drop_Bombarding_Pointer && randAttack <= SPAM_Delay_Drop_Bombarding_Pointer)
            		{
            			node.setSPAM_Delay(true);
                		node.setSPAM_Delay_Period(SPAM_Delay_Period);
                		
                		node.setSPAM_Drop(true);
                		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
                		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
                		
                		node.setSPAM_Bombarding(true);
                		node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
    	
                		//System.out.println("");
                		//System.out.println("MSP "+i+" is SPAM_Delay_Drop_Bombarding");
                		
                		countMSP_SPAM_Delay_Drop_Bombarding++;
            		}
            	}
            	
            	//System.out.println("MSPid: "+i);
            }
            else if(i >= mspsPointer && i < spsPointer)
            {
            	SP_Node node = (SP_Node) n.getProtocol(SPPid);
            	node.setNodePos(i);
            	node.setRMGRPid(RMGRPid);
            	node.setSuperPid(SuperPid);
            	node.setIDPPid(IDPPid);
            	node.setAuditorPid(AuditorPid);
            	node.setUserPid(UserPid);
            	node.setSPPid(SPPid);
            	node.setMEPid(MEPid);
            	node.setNodeType(4);
            	node.setrGen(rGenerator);
            	node.setUsefulnessMIN(usefulnessMIN);
            	node.setUsefulness(node.randDouble(0, usefulnessMIN));
            	node.setNewPartnershipProb(newPartnershipProb);
            	node.setNewPartnershipDuration(newPartnershipDuration);
                node.setPartnerSharingProb(partnerSharingProb);
                node.setAcceptDGUProb(AcceptDGUProb);
                node.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
            	node.setAcceptCompulsoryDGUProb(AcceptCompulsoryDGUProb);
            	node.setAcceptCompulsoryStrictDGUProb(AcceptCompulsoryStrictDGUProb);
            	
            	idp.addNodeDir(i, 4, SPRank);
            	
            	//System.out.println("SPid: "+i);
            }
            else if(i >= spsPointer && i < ME_ColludingUnpopular_Pointer)
            {
            	for(int j = 0; j < sumME_ColludingUnpopular; j++)
            	{
            		n = Network.get(i);
            		
            		ME_Node node = (ME_Node) n.getProtocol(MEPid);
            		node.setME_MAX_MPSPs(ME_MAX_MPSPs);
            		node.setME_MAX_MSPs(ME_MAX_MSPs);
            		node.setME_MPSPsGR(ME_MPSPsGR);
            		node.setME_MSPsGR(ME_MSPsGR);
            		node.setME_N_MIN_MPSPs_toSPAM_Credential(ME_N_MIN_MPSPs_toSPAM_Credential);
            		node.setME_N_MIN_MPSPs_toSPAM_User(ME_N_MIN_MPSPs_toSPAM_User);
            		node.setNodePos(i);
                	node.setRMGRPid(RMGRPid);
                	node.setSuperPid(SuperPid);
                	node.setIDPPid(IDPPid);
                	node.setAuditorPid(AuditorPid);
                	node.setUserPid(UserPid);
                	node.setSPPid(SPPid);
                	node.setMEPid(MEPid);
            		node.setNodeType(5);
            		node.setrGen(rGenerator);
            		node.setUnpopularSPsColluding(true);
            		node.setUsefulnessMIN(usefulnessMIN);
            		
            		int MEid = i;
            		
            		//System.out.println("MEid: "+i+" UnpopularSPsColluding");
            		
            		i++;
            		
            		i = this.initializeMEnodes(i, MEid, true);
            		i = this.initializeMEnodes(i, MEid, false);
            	}
            }
            else if( i >= ME_ColludingUnpopular_Pointer && i < Network.size())
            {
            	for(int j = 0; j < sumME_ColludingPopular; j++)
            	{
            		n = Network.get(i);
            		
            		ME_Node node = (ME_Node) n.getProtocol(MEPid);
            		node.setME_MAX_MPSPs(ME_MAX_MPSPs);
            		node.setME_MAX_MSPs(ME_MAX_MSPs);
            		node.setME_MPSPsGR(ME_MPSPsGR);
            		node.setME_MSPsGR(ME_MSPsGR);
            		node.setME_N_MIN_MPSPs_toSPAM_Credential(ME_N_MIN_MPSPs_toSPAM_Credential);
            		node.setME_N_MIN_MPSPs_toSPAM_User(ME_N_MIN_MPSPs_toSPAM_User);
            		node.setNodePos(i);
                	node.setRMGRPid(RMGRPid);
                	node.setSuperPid(SuperPid);
                	node.setIDPPid(IDPPid);
                	node.setAuditorPid(AuditorPid);
                	node.setUserPid(UserPid);
                	node.setSPPid(SPPid);
                	node.setMEPid(MEPid);
            		node.setNodeType(5);
            		node.setrGen(rGenerator);
            		node.setUnpopularSPsColluding(false);
            		node.setUsefulnessMIN(usefulnessMIN);
            		
            		int MEid = i;
            		//System.out.println("MEid: "+i+" PopularSPsColluding");
            		i++;
            		
            		i = this.initializeMEnodes(i, MEid, true);
            		i = this.initializeMEnodes(i, MEid, false);
            	}
            }
            
            
        }
        return false;
    }
    
    public int initializeMEnodes(int i, int MEid, boolean MPSP)
    {
    	double SPAM_Delay_Pointer = SPAM_Delay_ME_MPSP_PCT;
    	double SPAM_Bombarding_Pointer = SPAM_Delay_Pointer + SPAM_Bombarding_ME_MPSP_PCT;
    	double SPAM_Drop_Pointer = SPAM_Bombarding_Pointer + SPAM_Drop_ME_MPSP_PCT;
    	double SPAM_Delay_Bombarding_Pointer = SPAM_Drop_Pointer + SPAM_Delay_Bombarding_ME_MPSP_PCT;
    	double SPAM_Delay_Drop_Pointer = SPAM_Delay_Bombarding_Pointer + SPAM_Delay_Drop_ME_MPSP_PCT;
    	double SPAM_Drop_Bombarding_Pointer = SPAM_Delay_Drop_Pointer + SPAM_Drop_Bombarding_ME_MPSP_PCT;
    	double SPAM_Delay_Drop_Bombarding_Pointer = SPAM_Drop_Bombarding_Pointer + SPAM_Delay_Drop_Bombarding_ME_MPSP_PCT;
    	
		double sumME_SPAM_Delay = 0;
    	double sumME_SPAM_Bombarding = 0;
    	double sumME_SPAM_Drop = 0;
    	double sumME_SPAM_Delay_Bombarding = 0;
    	double sumME_SPAM_Delay_Drop = 0;
    	double sumME_SPAM_Drop_Bombarding = 0;
    	double sumME_SPAM_Delay_Drop_Bombarding = 0;
    	
    	int nodesNum = 0;
    	
    	if(MPSP)
    	{
    		sumME_SPAM_Delay = (SPAM_Delay_ME_MPSP_PCT/100) * INI_ME_MPSPs;
        	sumME_SPAM_Bombarding = (SPAM_Bombarding_ME_MPSP_PCT/100) * INI_ME_MPSPs;
        	sumME_SPAM_Drop = (SPAM_Drop_ME_MPSP_PCT/100) * INI_ME_MPSPs;
        	sumME_SPAM_Delay_Bombarding = (SPAM_Delay_Bombarding_ME_MPSP_PCT/100) * INI_ME_MPSPs;
        	sumME_SPAM_Delay_Drop = (SPAM_Delay_Drop_ME_MPSP_PCT/100) * INI_ME_MPSPs;
        	sumME_SPAM_Drop_Bombarding = (SPAM_Drop_Bombarding_ME_MPSP_PCT/100) * INI_ME_MPSPs;
        	sumME_SPAM_Delay_Drop_Bombarding = (SPAM_Delay_Drop_Bombarding_ME_MPSP_PCT/100) * INI_ME_MPSPs;
        	
        	nodesNum = INI_ME_MPSPs;
    	}
    	else
    	{
     		sumME_SPAM_Delay = (SPAM_Delay_ME_MSP_PCT/100) * INI_ME_MSPs;
        	sumME_SPAM_Bombarding = (SPAM_Bombarding_ME_MSP_PCT/100) * INI_ME_MSPs;
        	sumME_SPAM_Drop = (SPAM_Drop_ME_MSP_PCT/100) * INI_ME_MSPs;
        	sumME_SPAM_Delay_Bombarding = (SPAM_Delay_Bombarding_ME_MSP_PCT/100) * INI_ME_MSPs;
        	sumME_SPAM_Delay_Drop = (SPAM_Delay_Drop_ME_MSP_PCT/100) * INI_ME_MSPs;
        	sumME_SPAM_Drop_Bombarding = (SPAM_Drop_Bombarding_ME_MSP_PCT/100) * INI_ME_MSPs;
        	sumME_SPAM_Delay_Drop_Bombarding = (SPAM_Delay_Drop_Bombarding_ME_MSP_PCT/100) * INI_ME_MSPs;
        	
        	nodesNum = INI_ME_MSPs;
    	}
    	
    	Random rGenerator = new Random();
    	Node n;
    	int k = 0;	
    	
    	IDP_Node idp = (IDP_Node) Network.get(1).getProtocol(IDPPid);
    	
    	for(int l = 0; l < sumME_SPAM_Delay; l++)
    	{
    		if(sumME_SPAM_Delay - l < 1) break;
    		
    		n = Network.get(i);
			SP_Node node_sp = (SP_Node) n.getProtocol(SPPid);
			node_sp.setAssociatedWithME(true);
			node_sp.setMalicious(true);
			node_sp.setMEid(MEid);
			node_sp.setNodePos(i);
			node_sp.setRMGRPid(RMGRPid);
			node_sp.setSuperPid(SuperPid);
			node_sp.setIDPPid(IDPPid);
			node_sp.setAuditorPid(AuditorPid);
			node_sp.setUserPid(UserPid);
			node_sp.setSPPid(SPPid);
			node_sp.setMEPid(MEPid);
			node_sp.setNodeType(4);
			node_sp.setrGen(rGenerator);
			node_sp.setUsefulnessMIN(usefulnessMIN);
        	node_sp.setNewPartnershipProb(newPartnershipProb);
        	node_sp.setNewPartnershipDuration(newPartnershipDuration);
            node_sp.setPartnerSharingProb(partnerSharingProb);
            node_sp.setAcceptDGUProb(AcceptDGUProb);
            node_sp.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
			node_sp.setSPAM_Drop_StrictCredential_Prob(SPAM_Drop_StrictCredential_Prob);
			if(MPSP)
				node_sp.setUsefulness(node_sp.randDouble(usefulnessMIN, 100));
			else
				node_sp.setUsefulness(node_sp.randDouble(0, usefulnessMIN));
			
			node_sp.setSPAM_Delay(true);
    		node_sp.setSPAM_Delay_Period(SPAM_Delay_ME_Period);
    		
        	idp.addNodeDir(i, 4, SPRank);
        	
        	ME_Node meNode = (ME_Node) Network.get(MEid).getProtocol(MEPid);
        	meNode.addMSPNode(node_sp, MPSP);
    		
    	//	if(MPSP)
    	//		System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay");
    	//	else
    		//	System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay");

    		i++;
    		k++;
    	}
    	
    	for(int l = 0; l < sumME_SPAM_Bombarding; l++)
    	{
    		if(sumME_SPAM_Bombarding - l < 1) break;
    		
    		n = Network.get(i);
			SP_Node node_sp = (SP_Node) n.getProtocol(SPPid);
			node_sp.setAssociatedWithME(true);
			node_sp.setMalicious(true);
			node_sp.setMEid(MEid);
			node_sp.setNodePos(i);
			node_sp.setRMGRPid(RMGRPid);
			node_sp.setSuperPid(SuperPid);
			node_sp.setIDPPid(IDPPid);
			node_sp.setAuditorPid(AuditorPid);
			node_sp.setUserPid(UserPid);
			node_sp.setSPPid(SPPid);
			node_sp.setMEPid(MEPid);
			node_sp.setNodeType(4);
			node_sp.setrGen(rGenerator);
			node_sp.setUsefulnessMIN(usefulnessMIN);
			node_sp.setNewPartnershipProb(newPartnershipProb);
        	node_sp.setNewPartnershipDuration(newPartnershipDuration);
            node_sp.setPartnerSharingProb(partnerSharingProb);
            node_sp.setAcceptDGUProb(AcceptDGUProb);
            node_sp.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
			node_sp.setSPAM_Drop_StrictCredential_Prob(SPAM_Drop_StrictCredential_Prob);
			if(MPSP)
				node_sp.setUsefulness(node_sp.randDouble(usefulnessMIN, 100));
			else
				node_sp.setUsefulness(node_sp.randDouble(0, usefulnessMIN));
			
    		node_sp.setSPAM_Bombarding(true);
    		node_sp.setSPAM_Bombarding_Period(SPAM_Bombarding_ME_Period);
    		
        	idp.addNodeDir(i, 4, SPRank);
        	
        	ME_Node meNode = (ME_Node) Network.get(MEid).getProtocol(MEPid);
        	meNode.addMSPNode(node_sp, MPSP);
    		
    	//	if(MPSP)
    	//		System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Bombarding");
    		//else
    			//System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Bombarding");
    		
    		i++;
    		k++;
    	}
    	
    	for(int l = 0; l < sumME_SPAM_Drop; l++)
    	{
    		if(sumME_SPAM_Drop - l < 1) break;
    		
    		n = Network.get(i);
			SP_Node node_sp = (SP_Node) n.getProtocol(SPPid);
			node_sp.setAssociatedWithME(true);
			node_sp.setMalicious(true);
			node_sp.setMEid(MEid);
			node_sp.setNodePos(i);
			node_sp.setRMGRPid(RMGRPid);
			node_sp.setSuperPid(SuperPid);
			node_sp.setIDPPid(IDPPid);
			node_sp.setAuditorPid(AuditorPid);
			node_sp.setUserPid(UserPid);
			node_sp.setSPPid(SPPid);
			node_sp.setMEPid(MEPid);
			node_sp.setNodeType(4);
			node_sp.setrGen(rGenerator);
			node_sp.setUsefulnessMIN(usefulnessMIN);
			node_sp.setNewPartnershipProb(newPartnershipProb);
        	node_sp.setNewPartnershipDuration(newPartnershipDuration);
            node_sp.setPartnerSharingProb(partnerSharingProb);
            node_sp.setAcceptDGUProb(AcceptDGUProb);
            node_sp.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
			node_sp.setSPAM_Drop_StrictCredential_Prob(SPAM_Drop_StrictCredential_Prob);
			if(MPSP)
				node_sp.setUsefulness(node_sp.randDouble(usefulnessMIN, 100));
			else
				node_sp.setUsefulness(node_sp.randDouble(0, usefulnessMIN));
			
    		node_sp.setSPAM_Drop(true);
    		node_sp.setSPAM_DropCredential_Rate(SPAM_DropCredential_ME_Rate);
    		node_sp.setSPAM_DropUser_Rate(SPAM_DropUser_ME_Rate);
    		
    	//	if(MPSP)
    		//	System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Drop");
    	//	else
    		//	System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Drop");
    		
        	
        	idp.addNodeDir(i, 4, SPRank);
    		
        	ME_Node meNode = (ME_Node) Network.get(MEid).getProtocol(MEPid);
        	meNode.addMSPNode(node_sp, MPSP);
        	
    		i++;
    		k++;
    	}
    	
    	for(int l = 0; l < sumME_SPAM_Delay_Bombarding; l++)
    	{
    		if(sumME_SPAM_Delay_Bombarding - l < 1) break;
    		
    		n = Network.get(i);
			SP_Node node_sp = (SP_Node) n.getProtocol(SPPid);
			node_sp.setAssociatedWithME(true);
			node_sp.setMalicious(true);
			node_sp.setMEid(MEid);
			node_sp.setNodePos(i);
			node_sp.setRMGRPid(RMGRPid);
			node_sp.setSuperPid(SuperPid);
			node_sp.setIDPPid(IDPPid);
			node_sp.setAuditorPid(AuditorPid);
			node_sp.setUserPid(UserPid);
			node_sp.setSPPid(SPPid);
			node_sp.setMEPid(MEPid);
			node_sp.setNodeType(4);
			node_sp.setrGen(rGenerator);
			node_sp.setUsefulnessMIN(usefulnessMIN);
			node_sp.setNewPartnershipProb(newPartnershipProb);
        	node_sp.setNewPartnershipDuration(newPartnershipDuration);
            node_sp.setPartnerSharingProb(partnerSharingProb);
            node_sp.setAcceptDGUProb(AcceptDGUProb);
            node_sp.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
			node_sp.setSPAM_Drop_StrictCredential_Prob(SPAM_Drop_StrictCredential_Prob);
			if(MPSP)
				node_sp.setUsefulness(node_sp.randDouble(usefulnessMIN, 100));
			else
				node_sp.setUsefulness(node_sp.randDouble(0, usefulnessMIN));
			
			node_sp.setSPAM_Delay(true);
    		node_sp.setSPAM_Delay_Period(SPAM_Delay_ME_Period);
    		
    		node_sp.setSPAM_Bombarding(true);
    		node_sp.setSPAM_Bombarding_Period(SPAM_Bombarding_ME_Period);
    		
    		idp.addNodeDir(i, 4, SPRank);
    		
        	ME_Node meNode = (ME_Node) Network.get(MEid).getProtocol(MEPid);
        	meNode.addMSPNode(node_sp, MPSP);
    		
    	//	if(MPSP)
    		//	System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Bombarding");
    //		else
    	//		System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Bombarding");
    		
    		i++;
    		k++;
    	}
    	
    	for(int l = 0; l < sumME_SPAM_Delay_Drop; l++)
    	{
    		if(sumME_SPAM_Delay_Drop - l < 1) break;
    		
    		n = Network.get(i);
			SP_Node node_sp = (SP_Node) n.getProtocol(SPPid);
			node_sp.setAssociatedWithME(true);
			node_sp.setMalicious(true);
			node_sp.setMEid(MEid);
			node_sp.setNodePos(i);
			node_sp.setRMGRPid(RMGRPid);
			node_sp.setSuperPid(SuperPid);
			node_sp.setIDPPid(IDPPid);
			node_sp.setAuditorPid(AuditorPid);
			node_sp.setUserPid(UserPid);
			node_sp.setSPPid(SPPid);
			node_sp.setMEPid(MEPid);
			node_sp.setNodeType(4);
			node_sp.setrGen(rGenerator);
			node_sp.setUsefulnessMIN(usefulnessMIN);
			node_sp.setNewPartnershipProb(newPartnershipProb);
        	node_sp.setNewPartnershipDuration(newPartnershipDuration);
            node_sp.setPartnerSharingProb(partnerSharingProb);
            node_sp.setAcceptDGUProb(AcceptDGUProb);
            node_sp.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
			node_sp.setSPAM_Drop_StrictCredential_Prob(SPAM_Drop_StrictCredential_Prob);
			if(MPSP)
				node_sp.setUsefulness(node_sp.randDouble(usefulnessMIN, 100));
			else
				node_sp.setUsefulness(node_sp.randDouble(0, usefulnessMIN));
			
    		node_sp.setSPAM_Delay(true);
    		node_sp.setSPAM_Delay_Period(SPAM_Delay_Period);
    		
    		node_sp.setSPAM_Drop(true);
    		node_sp.setSPAM_DropCredential_Rate(SPAM_DropCredential_ME_Rate);
    		node_sp.setSPAM_DropUser_Rate(SPAM_DropUser_ME_Rate);
    		
    		idp.addNodeDir(i, 4, SPRank);
    		
        	ME_Node meNode = (ME_Node) Network.get(MEid).getProtocol(MEPid);
        	meNode.addMSPNode(node_sp, MPSP);
    		
    //		if(MPSP)
    	//		System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Drop");
    		//else
    			//System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Drop");
    		
    		i++;
    		k++;
    	}
    	
    	for(int l = 0; l < sumME_SPAM_Drop_Bombarding; l++)
    	{
    		if(sumME_SPAM_Drop_Bombarding - l < 1) break;
    		
    		n = Network.get(i);
			SP_Node node_sp = (SP_Node) n.getProtocol(SPPid);
			node_sp.setAssociatedWithME(true);
			node_sp.setMalicious(true);
			node_sp.setMEid(MEid);
			node_sp.setNodePos(i);
			node_sp.setRMGRPid(RMGRPid);
			node_sp.setSuperPid(SuperPid);
			node_sp.setIDPPid(IDPPid);
			node_sp.setAuditorPid(AuditorPid);
			node_sp.setUserPid(UserPid);
			node_sp.setSPPid(SPPid);
			node_sp.setMEPid(MEPid);
			node_sp.setNodeType(4);
			node_sp.setrGen(rGenerator);
			node_sp.setUsefulnessMIN(usefulnessMIN);
			node_sp.setNewPartnershipProb(newPartnershipProb);
        	node_sp.setNewPartnershipDuration(newPartnershipDuration);
            node_sp.setPartnerSharingProb(partnerSharingProb);
            node_sp.setAcceptDGUProb(AcceptDGUProb);
            node_sp.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
			node_sp.setSPAM_Drop_StrictCredential_Prob(SPAM_Drop_StrictCredential_Prob);
			if(MPSP)
				node_sp.setUsefulness(node_sp.randDouble(usefulnessMIN, 100));
			else
				node_sp.setUsefulness(node_sp.randDouble(0, usefulnessMIN));
			
    		node_sp.setSPAM_Drop(true);
    		node_sp.setSPAM_DropCredential_Rate(SPAM_DropCredential_ME_Rate);
    		node_sp.setSPAM_DropUser_Rate(SPAM_DropUser_ME_Rate);
    		
    		node_sp.setSPAM_Bombarding(true);
    		node_sp.setSPAM_Bombarding_Period(SPAM_Bombarding_ME_Period);
    		
    		idp.addNodeDir(i, 4, SPRank);
    		
        	ME_Node meNode = (ME_Node) Network.get(MEid).getProtocol(MEPid);
        	meNode.addMSPNode(node_sp, MPSP);
    		
    //		if(MPSP)
    	//		System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Drop_Bombarding");
    		//else
    		//	System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Drop_Bombarding");
    		
    		i++;
    		k++;
    	}
    	
    	for(int l = 0; l < sumME_SPAM_Delay_Drop_Bombarding; l++)
    	{
    		if(sumME_SPAM_Delay_Drop_Bombarding - l < 1) break;
    		
    		n = Network.get(i);
			SP_Node node_sp = (SP_Node) n.getProtocol(SPPid);
			node_sp.setAssociatedWithME(true);
			node_sp.setMalicious(true);
			node_sp.setMEid(MEid);
			node_sp.setNodePos(i);
			node_sp.setRMGRPid(RMGRPid);
			node_sp.setSuperPid(SuperPid);
			node_sp.setIDPPid(IDPPid);
			node_sp.setAuditorPid(AuditorPid);
			node_sp.setUserPid(UserPid);
			node_sp.setSPPid(SPPid);
			node_sp.setMEPid(MEPid);
			node_sp.setNodeType(4);
			node_sp.setrGen(rGenerator);
			node_sp.setUsefulnessMIN(usefulnessMIN);
			node_sp.setNewPartnershipProb(newPartnershipProb);
        	node_sp.setNewPartnershipDuration(newPartnershipDuration);
            node_sp.setPartnerSharingProb(partnerSharingProb);
            node_sp.setAcceptDGUProb(AcceptDGUProb);
            node_sp.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
			node_sp.setSPAM_Drop_StrictCredential_Prob(SPAM_Drop_StrictCredential_Prob);
			if(MPSP)
				node_sp.setUsefulness(node_sp.randDouble(usefulnessMIN, 100));
			else
				node_sp.setUsefulness(node_sp.randDouble(0, usefulnessMIN));
			
			node_sp.setSPAM_Delay(true);
    		node_sp.setSPAM_Delay_Period(SPAM_Delay_ME_Period);
    		
    		node_sp.setSPAM_Drop(true);
    		node_sp.setSPAM_DropCredential_Rate(SPAM_DropCredential_ME_Rate);
    		node_sp.setSPAM_DropUser_Rate(SPAM_DropUser_ME_Rate);
    		
    		node_sp.setSPAM_Bombarding(true);
    		node_sp.setSPAM_Bombarding_Period(SPAM_Bombarding_ME_Period);
    		
    		idp.addNodeDir(i, 4, SPRank);
    		
        	ME_Node meNode = (ME_Node) Network.get(MEid).getProtocol(MEPid);
        	meNode.addMSPNode(node_sp, MPSP);
    		
    	//	if(MPSP)
    		//	System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Drop_Bombarding");
    	//	else
    		//	System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Drop_Bombarding");
    		
    		i++;
    		k++;
    	}
    	
    	for(int m = k; m < nodesNum; m++)
    	{
    		n = Network.get(i);
			SP_Node node_sp = (SP_Node) n.getProtocol(SPPid);
			node_sp.setAssociatedWithME(true);
			node_sp.setMalicious(true);
			node_sp.setMEid(MEid);
			node_sp.setNodePos(i);
			node_sp.setRMGRPid(RMGRPid);
			node_sp.setSuperPid(SuperPid);
			node_sp.setIDPPid(IDPPid);
			node_sp.setAuditorPid(AuditorPid);
			node_sp.setUserPid(UserPid);
			node_sp.setSPPid(SPPid);
			node_sp.setMEPid(MEPid);
			node_sp.setNodeType(4);
			node_sp.setrGen(rGenerator);
			node_sp.setUsefulnessMIN(usefulnessMIN);
			node_sp.setNewPartnershipProb(newPartnershipProb);
        	node_sp.setNewPartnershipDuration(newPartnershipDuration);
            node_sp.setPartnerSharingProb(partnerSharingProb);
            node_sp.setAcceptDGUProb(AcceptDGUProb);
            node_sp.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
			node_sp.setSPAM_Drop_StrictCredential_Prob(SPAM_Drop_StrictCredential_Prob);
			if(MPSP)
				node_sp.setUsefulness(node_sp.randDouble(usefulnessMIN, 100));
			else
				node_sp.setUsefulness(node_sp.randDouble(0, usefulnessMIN));
			
			idp.addNodeDir(i, 4, SPRank);
			
        	ME_Node meNode = (ME_Node) Network.get(MEid).getProtocol(MEPid);
        	meNode.addMSPNode(node_sp, MPSP);
			
			double randAttack = node_sp.randDouble(0, 100);
    		
    		if(randAttack >= 0 && randAttack < SPAM_Delay_Pointer)
    		{
    			node_sp.setSPAM_Delay(true);
    			node_sp.setSPAM_Delay_Period(SPAM_Delay_ME_Period);

    	//		if(MPSP)
        	//		System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay && Rand Attack");
        		//else
        			//System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay && Rand Attack");
    			
    		}
    		else if(randAttack >= SPAM_Delay_Pointer && randAttack < SPAM_Bombarding_Pointer)
    		{
    			node_sp.setSPAM_Bombarding(true);
    			node_sp.setSPAM_Bombarding_Period(SPAM_Bombarding_ME_Period);
    			
    	//		if(MPSP)
        	//		System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Bombarding && Rand Attack");
        		//else
        			//System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Bombarding && Rand Attack");
 
    		}
    		else if(randAttack >= SPAM_Bombarding_Pointer && randAttack < SPAM_Drop_Pointer)
    		{
    			node_sp.setSPAM_Drop(true);
    			node_sp.setSPAM_DropCredential_Rate(SPAM_DropCredential_ME_Rate);
    			node_sp.setSPAM_DropUser_Rate(SPAM_DropUser_ME_Rate);
    			
    		//	if(MPSP)
        		//	System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Drop && Rand Attack");
        	//	else
        		//	System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Drop && Rand Attack");
    		
    		}
    		else if(randAttack >= SPAM_Drop_Pointer && randAttack < SPAM_Delay_Bombarding_Pointer)
    		{
    			node_sp.setSPAM_Delay(true);
    			node_sp.setSPAM_Delay_Period(SPAM_Delay_ME_Period);
        		
    			node_sp.setSPAM_Bombarding(true);
    			node_sp.setSPAM_Bombarding_Period(SPAM_Bombarding_ME_Period);
    			
    		//	if(MPSP)
        		//	System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Bombarding && Rand Attack");
        	//	else
        		//	System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Bombarding && Rand Attack");
    			
    		}
    		else if(randAttack >= SPAM_Delay_Bombarding_Pointer && randAttack < SPAM_Delay_Drop_Pointer)
    		{
    			node_sp.setSPAM_Delay(true);
    			node_sp.setSPAM_Delay_Period(SPAM_Delay_ME_Period);
        		
    			node_sp.setSPAM_Drop(true);
    			node_sp.setSPAM_DropCredential_Rate(SPAM_DropCredential_ME_Rate);
    			node_sp.setSPAM_DropUser_Rate(SPAM_DropUser_ME_Rate);
    			
    	//		if(MPSP)
        //			System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Drop && Rand Attack");
        //		else
        //			System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Drop && Rand Attack");
    		}
    		else if(randAttack >= SPAM_Delay_Drop_Pointer && randAttack < SPAM_Drop_Bombarding_Pointer)
    		{
    			node_sp.setSPAM_Drop(true);
    			node_sp.setSPAM_DropCredential_Rate(SPAM_DropCredential_ME_Rate);
    			node_sp.setSPAM_DropUser_Rate(SPAM_DropUser_ME_Rate);
        		
    			node_sp.setSPAM_Bombarding(true);
    			node_sp.setSPAM_Bombarding_Period(SPAM_Bombarding_ME_Period);
    			
    	//		if(MPSP)
        //			System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Drop_Bombarding && Rand Attack");
        //		else
        //			System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Drop_Bombarding && Rand Attack");
    			
    		}
    		else if(randAttack >= SPAM_Drop_Bombarding_Pointer && randAttack <= SPAM_Delay_Drop_Bombarding_Pointer)
    		{
    			node_sp.setSPAM_Delay(true);
    			node_sp.setSPAM_Delay_Period(SPAM_Delay_ME_Period);
        		
    			node_sp.setSPAM_Drop(true);
    			node_sp.setSPAM_DropCredential_Rate(SPAM_DropCredential_ME_Rate);
    			node_sp.setSPAM_DropUser_Rate(SPAM_DropUser_ME_Rate);
        		
    			node_sp.setSPAM_Bombarding(true);
    			node_sp.setSPAM_Bombarding_Period(SPAM_Bombarding_ME_Period);
    			
    	//		if(MPSP)
        //			System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Drop_Bombarding && Rand Attack");
        //		else
        //			System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Drop_Bombarding && Rand Attack");
    			
    		}
    		
    		i++;
    	}
    	
    	return i;
    }

	public int getExprimentTrial_k() {
		return exprimentTrial_k;
	}

	public void setExprimentTrial_k(int exprimentTrial_k) {
		this.exprimentTrial_k = exprimentTrial_k;
	}
        
}
