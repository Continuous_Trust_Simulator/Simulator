package trust.simulator.rmgr.networkGenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import doe.Factor;
import doe.TrialsCatalouge;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Network;
import peersim.core.Node;
import peersim.dynamics.NodeInitializer;
import trust.simulator.rmgr.protocol.idp.IDP_Node;
import trust.simulator.rmgr.protocol.me.ME_Node;
import trust.simulator.rmgr.protocol.sp.SP_Node;
import trust.simulator.rmgr.protocol.user.GT_TargetedSP;
import trust.simulator.rmgr.protocol.user.User_Node;

/**
 * <p>
 * This initialization class collects the simulation parameters from the config
 * file.
 * </p>
 * <p>
 * The first node in the {@link Network} is considered as the root node.
 * </p>
 */
public class DynamicInitializer implements NodeInitializer {
   
	private int nodeType;
	private int nodePosition;
	
	private double userIniTrustinNetwork;
	
	// 1: MPSP, 2: PSP, 3: MSP, 4: SP
	private int spNodeType; 
	
	// 0: Normal User 1: ST, 2: GT
	private int agentNodeType;
	private int STtargetedSPid;
	private int GTid;
	private List<GT_TargetedSP> GT_TargetedSPs;
	
	// ------------------------------------------------------------------------
    // Parameters
    // ------------------------------------------------------------------------
	
	private static final String PAR_Factors = "Factors";
	
	private static final String PAR_Fixed = "Fixed";

	private static final String PAR_RMGR_Node_PROT = "RMGR_Node";
	private static final String PAR_Super_Node_PROT = "Super_Node";
	private static final String PAR_IDP_Node_PROT = "IDP_Node";
	private static final String PAR_Auditor_Node_PROT = "Auditor_Node";   
    private static final String PAR_User_Node_PROT = "User_Node";    
    private static final String PAR_SP_Node_PROT = "SP_Node";
    private static final String PAR_ME_Node_PROT = "ME_Node";
    
    private static final String PAR_SPRank = "SPRank";
    
    //private static final String PAR_MEGR = "MEGR";
    private static final String PAR_INI_ME_MPSPs = "INI_ME_MPSPs";
    private static final String PAR_INI_ME_MSPs = "INI_ME_MSPs";
    private static final String PAR_ME_MPSPsGR = "ME_MPSPsGR";
    private static final String PAR_ME_MSPsGR = "ME_MSPsGR";
    private static final String PAR_ME_MAX_MPSPs = "ME_MAX_MPSPs";
    private static final String PAR_ME_MAX_MSPs = "ME_MAX_MSPs";
    
    private static final String PAR_UsefulnessMIN = "usefulnessMIN";
    private static final String PAR_NewPartnershipProb = "newPartnershipProb";
    private static final String PAR_NewPartnershipDuration = "newPartnershipDuration";
    private static final String PAR_PartnerSharingProb = "partnerSharingProb";
    private static final String PAR_AcceptDGUProb = "AcceptDGUProb";
    private static final String PAR_AcceptStrictDGUProb = "AcceptStrictDGUProb";
    private static final String PAR_AcceptCompulsoryDGUProb = "AcceptCompulsoryDGUProb";
    private static final String PAR_AcceptCompulsoryStrictDGUProb = "AcceptCompulsoryStrictDGUProb";
    
    private static final String PAR_CredentialsArraySize = "CredentialsArraySize";    
	private static final String PAR_GenerateNewCredentialRate = "GenerateNewCredentialRate";
	private static final String PAR_GenerateNewServiceRequestRate = "GenerateNewServiceRequestRate";
	private static final String PAR_StrictCredentialProb = "StrictCredentialProb";
	private static final String PAR_IniCredentialType = "IniCredentialType";
	private static final String PAR_TrustInNetwork = "TrustInNetwork";
	private static final String PAR_IgnoranceRate = "IgnoranceRate";
	private static final String PAR_StopUsingCredentialAfterSpam = "StopUsingCredentialAfterSpam";
	private static final String PAR_TrustDropPostSpam = "TrustDropPostSpam";
	private static final String PAR_TrustIncreasePostSpamResolved = "TrustIncreasePostSpamResolved";
	private static final String PAR_TrustIncreaseRate = "TrustIncreaseRate";
	private static final String PAR_TrustDecreaseRate = "TrustDecreaseRate";
	private static final String PAR_TrustDecreasePeriodPostSpam = "TrustDecreasePeriodPostSpam";
	
	private static final String PAR_STidleTime = "STidleTime";
	private static final String PAR_STmaxLifeTime = "STmaxLifeTime";
	private static final String PAR_STunimportantCredentialsPool = "STunimportantCredentialsPool";
	private static final String PAR_STreportSpam = "STreportSpam";
	
	private static final String PAR_GTidleTime = "GTidleTime";
	private static final String PAR_GTmaxLifeTime = "GTmaxLifeTime";
	private static final String PAR_GTunimportantCredentialsPool = "GTunimportantCredentialsPool";
	private static final String PAR_GTreportSpam = "GTreportSpam";
	
	private static final String PAR_SPAM_Drop_StrictCredential_Prob = "SPAM_Drop_StrictCredential_Prob";
	private static final String PAR_SPAM_Delay_MSP_PCT = "SPAM_Delay_MSP_PCT";
	private static final String PAR_SPAM_Delay_MPSP_PCT = "SPAM_Delay_MPSP_PCT";
	private static final String PAR_SPAM_Delay_Period = "SPAM_Delay_Period";
	private static final String PAR_SPAM_Bombarding_MSP_PCT = "SPAM_Bombarding_MSP_PCT";
	private static final String PAR_SPAM_Bombarding_MPSP_PCT = "SPAM_Bombarding_MPSP_PCT";
	private static final String PAR_SPAM_Bombarding_Period = "SPAM_Bombarding_Period";
	private static final String PAR_SPAM_Drop_MSP_PCT = "SPAM_Drop_MSP_PCT";
	private static final String PAR_SPAM_Drop_MPSP_PCT = "SPAM_Drop_MPSP_PCT";
	private static final String PAR_SPAM_DropUser_Rate = "SPAM_DropUser_Rate";
	private static final String PAR_SPAM_DropCredential_Rate = "SPAM_DropCredential_Rate";
	private static final String PAR_SPAM_Delay_Bombarding_MSP_PCT = "SPAM_Delay_Bombarding_MSP_PCT";
	private static final String PAR_SPAM_Delay_Bombarding_MPSP_PCT = "SPAM_Delay_Bombarding_MPSP_PCT";
	private static final String PAR_SPAM_Delay_Drop_MSP_PCT = "SPAM_Delay_Drop_MSP_PCT";
	private static final String PAR_SPAM_Delay_Drop_MPSP_PCT = "SPAM_Delay_Drop_MPSP_PCT";
	private static final String PAR_SPAM_Drop_Bombarding_MSP_PCT = "SPAM_Drop_Bombarding_MSP_PCT";
	private static final String PAR_SPAM_Drop_Bombarding_MPSP_PCT = "SPAM_Drop_Bombarding_MPSP_PCT";
	private static final String PAR_SPAM_Delay_Drop_Bombarding_MSP_PCT = "SPAM_Delay_Drop_Bombarding_MSP_PCT";
	private static final String PAR_SPAM_Delay_Drop_Bombarding_MPSP_PCT = "SPAM_Delay_Drop_Bombarding_MPSP_PCT";
    
	private static final String PAR_SPAM_Delay_ME_MSP_PCT = "SPAM_Delay_ME_MSP_PCT";
	private static final String PAR_SPAM_Delay_ME_MPSP_PCT = "SPAM_Delay_ME_MPSP_PCT";
	private static final String PAR_SPAM_Delay_ME_Period = "SPAM_Delay_ME_Period";
	private static final String PAR_SPAM_Bombarding_ME_MSP_PCT = "SPAM_Bombarding_ME_MSP_PCT";
	private static final String PAR_SPAM_Bombarding_ME_MPSP_PCT = "SPAM_Bombarding_ME_MPSP_PCT";
	private static final String PAR_SPAM_Bombarding_ME_Period = "SPAM_Bombarding_ME_Period";
	private static final String PAR_SPAM_Drop_ME_MSP_PCT = "SPAM_Drop_ME_MSP_PCT";
	private static final String PAR_SPAM_Drop_ME_MPSP_PCT = "SPAM_Drop_ME_MPSP_PCT";
	private static final String PAR_SPAM_DropUser_ME_Rate = "SPAM_DropUser_ME_Rate";
	private static final String PAR_SPAM_DropCredential_ME_Rate = "SPAM_DropCredential_ME_Rate";
	private static final String PAR_SPAM_Delay_Bombarding_ME_MSP_PCT = "SPAM_Delay_Bombarding_ME_MSP_PCT";
	private static final String PAR_SPAM_Delay_Bombarding_ME_MPSP_PCT = "SPAM_Delay_Bombarding_ME_MPSP_PCT";
	private static final String PAR_SPAM_Delay_Drop_ME_MSP_PCT = "SPAM_Delay_Drop_ME_MSP_PCT";
	private static final String PAR_SPAM_Delay_Drop_ME_MPSP_PCT = "SPAM_Delay_Drop_ME_MPSP_PCT";
	private static final String PAR_SPAM_Drop_Bombarding_ME_MSP_PCT = "SPAM_Drop_Bombarding_ME_MSP_PCT";
	private static final String PAR_SPAM_Drop_Bombarding_ME_MPSP_PCT = "SPAM_Drop_Bombarding_ME_MPSP_PCT";
	private static final String PAR_SPAM_Delay_Drop_Bombarding_ME_MSP_PCT = "SPAM_Delay_Drop_Bombarding_ME_MSP_PCT";
	private static final String PAR_SPAM_Delay_Drop_Bombarding_ME_MPSP_PCT = "SPAM_Delay_Drop_Bombarding_ME_MPSP_PCT";
	private static final String PAR_ME_N_MIN_MPSPs_toSPAM_User = "ME_N_MIN_MPSPs_toSPAM_User";
	private static final String PAR_ME_N_MIN_MPSPs_toSPAM_Credential = "ME_N_MIN_MPSPs_toSPAM_Credential";
	private static final String PAR_ME_Colluding_Unpopular_PCT = "ME_Colluding_Unpopular_PCT";
	
    // ------------------------------------------------------------------------
    // Fields
    // ------------------------------------------------------------------------
    
	private int exprimentTrial_k;
	private int Factors;
	private boolean Fixed;
	private List<Factor> FactorsList;
	
	private static int RMGRPid;
	private static int SuperPid;
	private static int IDPPid;
	private static int AuditorPid;
	private static int UserPid;
    private static int SPPid;
    private static int MEPid;
    
    private static double SPRank;
    
   // private static double MEGR;
    private static int INI_ME_MPSPs;
    private static int INI_ME_MSPs;
    private static double ME_MPSPsGR;
    private static double ME_MSPsGR;
    private static int ME_MAX_MPSPs;
    private static int ME_MAX_MSPs;
    
    private static double usefulnessMIN;
    private static double newPartnershipProb;
    private static int newPartnershipDuration;
    private static double partnerSharingProb;
    private static double AcceptDGUProb;
    private static double AcceptStrictDGUProb;
    private static double AcceptCompulsoryDGUProb;
    private static double AcceptCompulsoryStrictDGUProb;
    
	
    private static int CredentialsArraySize;
	// The probability of generating new Credential
	private static double GenerateNewCredentialRate;
	// The probability of generating new Service Request
	private static double GenerateNewServiceRequestRate;
	private static double StrictCredentialProb;
	private static int IniCredentialType;
	// The initial Users' Trust in the Network
    private static double TrustInNetwork;
    private static double IgnoranceRate;
    private static boolean StopUsingCredentialAfterSpam;
    private static double TrustDropPostSpam;
    private static double TrustIncreasePostSpamResolved;
    private static double TrustIncreaseRate;
    private static double TrustDecreaseRate;
    private static double TrustDecreasePeriodPostSpam;
    
    private static int STidleTime;
    private static int STmaxLifeTime;
    private static int STunimportantCredentialsPool;
    private static boolean STreportSpam;
    
    private static int GTidleTime;
    private static int GTmaxLifeTime;
    private static int GTunimportantCredentialsPool;
    private static boolean GTreportSpam;
    
    private static double SPAM_Drop_StrictCredential_Prob;
    private static double SPAM_Delay_MSP_PCT;
    private static double SPAM_Delay_MPSP_PCT;
    private static int SPAM_Delay_Period;
    private static double SPAM_Bombarding_MSP_PCT;
    private static double SPAM_Bombarding_MPSP_PCT;
    private static int SPAM_Bombarding_Period;
    private static double SPAM_Drop_MSP_PCT;
    private static double SPAM_Drop_MPSP_PCT;
    private static double SPAM_DropUser_Rate;
    private static double SPAM_DropCredential_Rate;
    private static double SPAM_Delay_Bombarding_MSP_PCT;
    private static double SPAM_Delay_Bombarding_MPSP_PCT;
    private static double SPAM_Delay_Drop_MSP_PCT;
    private static double SPAM_Delay_Drop_MPSP_PCT;
    private static double SPAM_Drop_Bombarding_MSP_PCT;
    private static double SPAM_Drop_Bombarding_MPSP_PCT;
    private static double SPAM_Delay_Drop_Bombarding_MSP_PCT;
    private static double SPAM_Delay_Drop_Bombarding_MPSP_PCT;
    
    private static double SPAM_Delay_ME_MSP_PCT;
    private static double SPAM_Delay_ME_MPSP_PCT;
    private static int SPAM_Delay_ME_Period;
    private static double SPAM_Bombarding_ME_MSP_PCT;
    private static double SPAM_Bombarding_ME_MPSP_PCT;
    private static int SPAM_Bombarding_ME_Period;
    private static double SPAM_Drop_ME_MSP_PCT;
    private static double SPAM_Drop_ME_MPSP_PCT;
    private static double SPAM_DropUser_ME_Rate;
    private static double SPAM_DropCredential_ME_Rate;
    private static double SPAM_Delay_Bombarding_ME_MSP_PCT;
    private static double SPAM_Delay_Bombarding_ME_MPSP_PCT;
    private static double SPAM_Delay_Drop_ME_MSP_PCT;
    private static double SPAM_Delay_Drop_ME_MPSP_PCT;
    private static double SPAM_Drop_Bombarding_ME_MSP_PCT;
    private static double SPAM_Drop_Bombarding_ME_MPSP_PCT;
    private static double SPAM_Delay_Drop_Bombarding_ME_MSP_PCT;
    private static double SPAM_Delay_Drop_Bombarding_ME_MPSP_PCT;
	private static int ME_N_MIN_MPSPs_toSPAM_User;
	private static int ME_N_MIN_MPSPs_toSPAM_Credential;
	private static double ME_Colluding_Unpopular_PCT;
    
    // ------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------

    /**
     * Standard constructor that reads the configuration parameters. Invoked by
     * the simulation engine.
     * 
     * @param prefix
     *            the configuration prefix for this class.
     */
    public DynamicInitializer(String prefix) {
    	
    	System.out.println("DynamicInitializer Constructor!");
     
    	Factors = Configuration.getInt(PAR_Factors);
    	
    	if(Configuration.getInt(PAR_Fixed) == 0)
    		Fixed =  false;
    	else
    		Fixed = true;
    	
    	FactorsList = new ArrayList<Factor>(Factors);
    	
    	for(int i = 1; i <= Factors; i++)
    	{
    		int nestedFactors = Configuration.getInt("Factor_"+i+"_nested");
    		
    		for(int j = 1; j <= nestedFactors; j++)
    		{
    			Factor factor = new Factor();
    			
        		String tempFactor =  Configuration.getString("Factor_"+i+"_"+j);
        		
        		double tempFactorLow =  Configuration.getDouble("Factor_"+i+"_"+j+"_low");
        		
        		double tempFactorHigh =  Configuration.getDouble("Factor_"+i+"_"+j+"_high");
        		
        		double tempFactorFixed =  Configuration.getDouble("Factor_"+i+"_"+j+"_fixed");
        		
        		factor.setFactor(tempFactor);
        		factor.setOrder(i);
        		factor.setLow(tempFactorLow);
        		factor.setHigh(tempFactorHigh);
        		factor.setFixed(tempFactorFixed);
        		
        		FactorsList.add(factor);
    		}
    	}
    	
    	//for(Factor factor: FactorsList)
    		//System.out.println("Factor: "+factor.getFactor()+" Order: "+factor.getOrder()+" Low: "+ factor.getLow()+" High: "+factor.getHigh()+" Fixed: "+factor.getFixed());
    	
    	
    	RMGRPid = Configuration.getPid(prefix + "." + PAR_RMGR_Node_PROT);
    	SuperPid = Configuration.getPid(prefix + "." + PAR_Super_Node_PROT);
    	IDPPid = Configuration.getPid(prefix + "." + PAR_IDP_Node_PROT);
    	AuditorPid = Configuration.getPid(prefix + "." + PAR_Auditor_Node_PROT);
    	UserPid = Configuration.getPid(prefix + "." + PAR_User_Node_PROT);
        SPPid = Configuration.getPid(prefix + "." + PAR_SP_Node_PROT);
        MEPid = Configuration.getPid(prefix + "." + PAR_ME_Node_PROT);
        
        SPRank = Configuration.getDouble(prefix + "." + PAR_SPRank);
        
        //MEGR = Configuration.getDouble(prefix + "." + PAR_MEGR);
        INI_ME_MPSPs = Configuration.getInt(prefix + "." + PAR_INI_ME_MPSPs);
        INI_ME_MSPs = Configuration.getInt(prefix + "." + PAR_INI_ME_MSPs);
        ME_MPSPsGR = Configuration.getDouble(prefix + "." + PAR_ME_MPSPsGR);
        ME_MSPsGR = Configuration.getDouble(prefix + "." + PAR_ME_MSPsGR);
        ME_MAX_MPSPs = Configuration.getInt(prefix + "." + PAR_ME_MAX_MPSPs);
        ME_MAX_MSPs = Configuration.getInt(prefix + "." + PAR_ME_MAX_MSPs);
     
        usefulnessMIN = Configuration.getDouble(prefix + "." + PAR_UsefulnessMIN);
        newPartnershipProb = Configuration.getDouble(prefix + "." + PAR_NewPartnershipProb);
        newPartnershipDuration = Configuration.getInt(prefix + "." + PAR_NewPartnershipDuration);
        partnerSharingProb = Configuration.getDouble(prefix + "." + PAR_PartnerSharingProb);
        AcceptDGUProb = Configuration.getDouble(prefix + "." + PAR_AcceptDGUProb);
        AcceptStrictDGUProb = Configuration.getDouble(prefix + "." + PAR_AcceptStrictDGUProb);
        AcceptCompulsoryDGUProb = Configuration.getDouble(prefix + "." + PAR_AcceptCompulsoryDGUProb);
        AcceptCompulsoryStrictDGUProb = Configuration.getDouble(prefix + "." + PAR_AcceptCompulsoryStrictDGUProb);
	
        GenerateNewCredentialRate = Configuration.getDouble(prefix + "." + PAR_GenerateNewCredentialRate);
		GenerateNewServiceRequestRate = Configuration.getDouble(prefix + "." + PAR_GenerateNewServiceRequestRate);
		CredentialsArraySize = Configuration.getInt(prefix + "." + PAR_CredentialsArraySize);
		StrictCredentialProb = Configuration.getDouble(prefix + "." + PAR_StrictCredentialProb);
		IniCredentialType = Configuration.getInt(prefix + "." + PAR_IniCredentialType);
		TrustInNetwork = Configuration.getDouble(prefix + "." + PAR_TrustInNetwork);
        IgnoranceRate = Configuration.getDouble(prefix + "." + PAR_IgnoranceRate);
        StopUsingCredentialAfterSpam = Configuration.getBoolean(prefix + "." + PAR_StopUsingCredentialAfterSpam);
        TrustDropPostSpam = Configuration.getDouble(prefix + "." + PAR_TrustDropPostSpam);
        TrustIncreasePostSpamResolved = Configuration.getDouble(prefix + "." + PAR_TrustIncreasePostSpamResolved);
        TrustIncreaseRate = Configuration.getDouble(prefix + "." + PAR_TrustIncreaseRate);
        TrustDecreaseRate = Configuration.getDouble(prefix + "." + PAR_TrustDecreaseRate);
        TrustDecreasePeriodPostSpam = Configuration.getDouble(prefix + "." + PAR_TrustDecreasePeriodPostSpam);
        
        STidleTime = Configuration.getInt(prefix + "." + PAR_STidleTime);
        STmaxLifeTime = Configuration.getInt(prefix + "." + PAR_STmaxLifeTime);
        STunimportantCredentialsPool = Configuration.getInt(prefix + "." + PAR_STunimportantCredentialsPool);
        
    	if(Configuration.getInt(prefix + "." + PAR_STreportSpam) == 0)
    		STreportSpam =  false;
    	else
    		STreportSpam = true;
        
        GTidleTime = Configuration.getInt(prefix + "." + PAR_GTidleTime);
        GTmaxLifeTime = Configuration.getInt(prefix + "." + PAR_GTmaxLifeTime);
        GTunimportantCredentialsPool = Configuration.getInt(prefix + "." + PAR_GTunimportantCredentialsPool);
        
    	if(Configuration.getInt(prefix + "." + PAR_GTreportSpam) == 0)
    		GTreportSpam =  false;
    	else
    		GTreportSpam = true;
        
    	SPAM_Drop_StrictCredential_Prob = Configuration.getDouble(prefix + "." + PAR_SPAM_Drop_StrictCredential_Prob);
        SPAM_Delay_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_MSP_PCT);
        SPAM_Delay_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_MPSP_PCT);
        SPAM_Delay_Period = Configuration.getInt(prefix + "." + PAR_SPAM_Delay_Period);
        SPAM_Bombarding_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Bombarding_MSP_PCT);
        SPAM_Bombarding_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Bombarding_MPSP_PCT);
        SPAM_Bombarding_Period = Configuration.getInt(prefix + "." + PAR_SPAM_Bombarding_Period);
        SPAM_Drop_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Drop_MSP_PCT);
        SPAM_Drop_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Drop_MPSP_PCT);
        SPAM_DropUser_Rate = Configuration.getDouble(prefix + "." + PAR_SPAM_DropUser_Rate);
        SPAM_DropCredential_Rate = Configuration.getDouble(prefix + "." + PAR_SPAM_DropCredential_Rate);
        SPAM_Delay_Bombarding_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Bombarding_MSP_PCT);
        SPAM_Delay_Bombarding_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Bombarding_MPSP_PCT);
        SPAM_Delay_Drop_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Drop_MSP_PCT);
        SPAM_Delay_Drop_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Drop_MPSP_PCT);
        SPAM_Drop_Bombarding_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Drop_Bombarding_MSP_PCT);
        SPAM_Drop_Bombarding_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Drop_Bombarding_MPSP_PCT);
        SPAM_Delay_Drop_Bombarding_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Drop_Bombarding_MSP_PCT);
        SPAM_Delay_Drop_Bombarding_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Drop_Bombarding_MPSP_PCT);
        
        SPAM_Delay_ME_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_ME_MSP_PCT);
        SPAM_Delay_ME_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_ME_MPSP_PCT);
        SPAM_Delay_ME_Period = Configuration.getInt(prefix + "." + PAR_SPAM_Delay_ME_Period);
        SPAM_Bombarding_ME_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Bombarding_ME_MSP_PCT);
        SPAM_Bombarding_ME_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Bombarding_ME_MPSP_PCT);
        SPAM_Bombarding_ME_Period = Configuration.getInt(prefix + "." + PAR_SPAM_Bombarding_ME_Period);
        SPAM_Drop_ME_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Drop_ME_MSP_PCT);
        SPAM_Drop_ME_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Drop_ME_MPSP_PCT);
        SPAM_DropUser_ME_Rate = Configuration.getDouble(prefix + "." + PAR_SPAM_DropUser_ME_Rate);
        SPAM_DropCredential_ME_Rate = Configuration.getDouble(prefix + "." + PAR_SPAM_DropCredential_ME_Rate);
        SPAM_Delay_Bombarding_ME_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Bombarding_ME_MSP_PCT);
        SPAM_Delay_Bombarding_ME_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Bombarding_ME_MPSP_PCT);
        SPAM_Delay_Drop_ME_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Drop_ME_MSP_PCT);
        SPAM_Delay_Drop_ME_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Drop_ME_MPSP_PCT);
        SPAM_Drop_Bombarding_ME_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Drop_Bombarding_ME_MSP_PCT);
        SPAM_Drop_Bombarding_ME_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Drop_Bombarding_ME_MPSP_PCT);
        SPAM_Delay_Drop_Bombarding_ME_MSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Drop_Bombarding_ME_MSP_PCT);
        SPAM_Delay_Drop_Bombarding_ME_MPSP_PCT = Configuration.getDouble(prefix + "." + PAR_SPAM_Delay_Drop_Bombarding_ME_MPSP_PCT);

    	ME_N_MIN_MPSPs_toSPAM_User = Configuration.getInt(prefix + "." + PAR_ME_N_MIN_MPSPs_toSPAM_User);
    	ME_N_MIN_MPSPs_toSPAM_Credential = Configuration.getInt(prefix + "." + PAR_ME_N_MIN_MPSPs_toSPAM_Credential);
    	ME_Colluding_Unpopular_PCT = Configuration.getDouble(prefix + "." + PAR_ME_Colluding_Unpopular_PCT);
    	
    	userIniTrustinNetwork = TrustInNetwork;   	
    }

    // ------------------------------------------------------------------------
    // Methods
    // ------------------------------------------------------------------------
    /**
     * Initialize the node coordinates. The first node in the {@link Network} is
     * the root node by default and it is located in the middle (the center of
     * the square) of the surface area.
     */
    public void initialize(Node n) {

    	Random rGenerator = new Random();
    	
    	//RMGR_Node genericNode = (RMGR_Node) n.getProtocol(RMGRPid);
    	
    	//System.out.println("Node Pos = "+nodePosition);
    	//System.out.println("Node Type = "+nodeType);
    	
    	//IDP_Node idp = (IDP_Node) Network.get(1).getProtocol(IDPPid);
    	//idp.addNodeDir(nodePosition, nodeType, SPRank);
    	
        if(nodeType == 3 && agentNodeType == 0)
        {
        	User_Node node = (User_Node) n.getProtocol(UserPid);
        	node.setCreationCycle(CommonState.getIntTime());
        	node.setNodePos(nodePosition);
        	node.setRMGRPid(RMGRPid);
        	node.setSuperPid(SuperPid);
        	node.setIDPPid(IDPPid);
        	node.setAuditorPid(AuditorPid);
        	node.setUserPid(UserPid);
        	node.setSPPid(SPPid);
        	node.setMEPid(MEPid);
        	node.setNodeType(3);
        	node.setrGen(rGenerator);
        	node.setGenerateNewCredentialRate(GenerateNewCredentialRate);
        	node.setGenerateNewServiceRequestRate(GenerateNewServiceRequestRate);
        	node.setCredentialsArray(CredentialsArraySize);
        	node.setStrictCredentialProb(StrictCredentialProb);
        	//Every User should get at least one Credential initially
        	//First one is normally important and strict
        	node.createNewCredential(false, false, false, IniCredentialType);
        	node.setTrustInNetwork(userIniTrustinNetwork);
        	node.setIgnoranceRate(IgnoranceRate);
        	node.setUsefulnessMIN(usefulnessMIN);
        	node.setStopUsingCredentialAfterSpam(StopUsingCredentialAfterSpam);
        	node.setTrustDropPostSpam(TrustDropPostSpam);
        	node.setTrustIncreasePostSpamResolved(TrustIncreasePostSpamResolved);
        	node.setTrustIncreaseRate(TrustIncreaseRate);
        	node.setTrustDecreaseRate(TrustDecreaseRate);
        	node.setTrustDecreasePeriodPostSpam(TrustDecreasePeriodPostSpam);
        }

        else if(nodeType == 3 && agentNodeType == 1)
        {
        	//TODO decide whether an ST agent should behave as a normal User or not
        	User_Node node = (User_Node) n.getProtocol(UserPid);
        	node.setCreationCycle(CommonState.getIntTime());
        	node.setNodePos(nodePosition);
        	node.setRMGRPid(RMGRPid);
        	node.setSuperPid(SuperPid);
        	node.setIDPPid(IDPPid);
        	node.setAuditorPid(AuditorPid);
        	node.setUserPid(UserPid);
        	node.setSPPid(SPPid);
        	node.setMEPid(MEPid);
        	node.setNodeType(3);
        	node.setrGen(rGenerator);
        	node.setGenerateNewCredentialRate(GenerateNewCredentialRate);
        	node.setGenerateNewServiceRequestRate(GenerateNewServiceRequestRate);
        	node.setCredentialsArray(CredentialsArraySize);
        	node.setStrictCredentialProb(StrictCredentialProb);
        	for(int i = 0; i < STunimportantCredentialsPool; i++)
        	{
        		if(i == 0)
        			node.createNewCredential(false, false, false, IniCredentialType);
        		else
        			node.createNewCredential(false, false, false, -1);
        	}
        	node.createNewCredential(false, true, false, 2);
        	
        	node.setSTreportSpam(STreportSpam);
        	node.setSTagent(true);
        	node.setSTstatus(1);
        	node.setSTidleTime(STidleTime);
        	node.setSTmaxLifeTime(STmaxLifeTime);
        	node.setSTtargetedSPid(STtargetedSPid);
        	node.setSTunimportantCredentialsPool(STunimportantCredentialsPool);
        	
        	node.setTrustInNetwork(userIniTrustinNetwork);
        	node.setIgnoranceRate(IgnoranceRate);
        	node.setUsefulnessMIN(usefulnessMIN);
        	node.setStopUsingCredentialAfterSpam(StopUsingCredentialAfterSpam);
        	node.setTrustDropPostSpam(TrustDropPostSpam);
        	node.setTrustIncreasePostSpamResolved(TrustIncreasePostSpamResolved);
        	node.setTrustIncreaseRate(TrustIncreaseRate);
        	node.setTrustDecreaseRate(TrustDecreaseRate);
        	node.setTrustDecreasePeriodPostSpam(TrustDecreasePeriodPostSpam);
        }
        
        else if(nodeType == 3 && agentNodeType == 2)
        {
        	User_Node node = (User_Node) n.getProtocol(UserPid);
        	node.setCreationCycle(CommonState.getIntTime());
        	node.setNodePos(nodePosition);
        	node.setRMGRPid(RMGRPid);
        	node.setSuperPid(SuperPid);
        	node.setIDPPid(IDPPid);
        	node.setAuditorPid(AuditorPid);
        	node.setUserPid(UserPid);
        	node.setSPPid(SPPid);
        	node.setMEPid(MEPid);
        	node.setNodeType(3);
        	node.setrGen(rGenerator);
        	node.setGenerateNewCredentialRate(GenerateNewCredentialRate);
        	node.setGenerateNewServiceRequestRate(GenerateNewServiceRequestRate);
        	node.setCredentialsArray(CredentialsArraySize);
        	node.setStrictCredentialProb(StrictCredentialProb);
        	for(int i = 0; i < GTunimportantCredentialsPool; i++)
        	{
        		if(i == 0)
        			node.createNewCredential(false, false, false, IniCredentialType);
        		else
        			node.createNewCredential(false, false, false, -1);
        	}
        	node.createNewCredential(false, false, true, 2);
        	
        	node.setGTreportSpam(GTreportSpam);
        	node.setGTagent(true);
        	node.setGTstatus(1);
        	node.setGTid(GTid);
        	node.setGTidleTime(GTidleTime);
        	node.setGTmaxLifeTime(GTmaxLifeTime);
        	node.setGTtargetedSPs(GT_TargetedSPs);
        	node.setGTunimportantCredentialsPool(GTunimportantCredentialsPool);
        	
        	node.setTrustInNetwork(userIniTrustinNetwork);
        	node.setIgnoranceRate(IgnoranceRate);
        	node.setUsefulnessMIN(usefulnessMIN);
        	node.setStopUsingCredentialAfterSpam(StopUsingCredentialAfterSpam);
        	node.setTrustDropPostSpam(TrustDropPostSpam);
        	node.setTrustIncreasePostSpamResolved(TrustIncreasePostSpamResolved);
        	node.setTrustIncreaseRate(TrustIncreaseRate);
        	node.setTrustDecreaseRate(TrustDecreaseRate);
        	node.setTrustDecreasePeriodPostSpam(TrustDecreasePeriodPostSpam);
        }

        
        else if(nodeType == 4 && spNodeType == 1)
        {
        	SP_Node node = (SP_Node) n.getProtocol(SPPid);
        	node.setCreationCycle(CommonState.getIntTime());
        	node.setNodePos(nodePosition);
        	node.setRMGRPid(RMGRPid);
        	node.setSuperPid(SuperPid);
        	node.setIDPPid(IDPPid);
        	node.setAuditorPid(AuditorPid);
        	node.setUserPid(UserPid);
        	node.setSPPid(SPPid);
        	node.setMEPid(MEPid);
        	node.setNodeType(4);
        	node.setrGen(rGenerator);
    		node.setUsefulnessMIN(usefulnessMIN);
        	node.setUsefulness(node.randDouble(usefulnessMIN, 100));
        	node.setMalicious(true);
        	node.setSPAM_Drop_StrictCredential_Prob(SPAM_Drop_StrictCredential_Prob);
        	node.setNewPartnershipProb(newPartnershipProb);
        	node.setNewPartnershipDuration(newPartnershipDuration);
            node.setPartnerSharingProb(partnerSharingProb);
            node.setAcceptDGUProb(AcceptDGUProb);
            node.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
        	
        	double randAttack = node.randDouble(0, 100);
        	//System.out.println("");
        	//System.out.println("randAttack = "+randAttack);
        	double SPAM_Delay_Pointer = SPAM_Delay_MPSP_PCT;
        	double SPAM_Bombarding_Pointer = SPAM_Delay_Pointer + SPAM_Bombarding_MPSP_PCT;
        	double SPAM_Drop_Pointer = SPAM_Bombarding_Pointer + SPAM_Drop_MPSP_PCT;
        	double SPAM_Delay_Bombarding_Pointer = SPAM_Drop_Pointer + SPAM_Delay_Bombarding_MPSP_PCT;
        	double SPAM_Delay_Drop_Pointer = SPAM_Delay_Bombarding_Pointer + SPAM_Delay_Drop_MPSP_PCT;
        	double SPAM_Drop_Bombarding_Pointer = SPAM_Delay_Drop_Pointer + SPAM_Drop_Bombarding_MPSP_PCT;
        	double SPAM_Delay_Drop_Bombarding_Pointer = SPAM_Drop_Bombarding_Pointer + SPAM_Delay_Drop_Bombarding_MPSP_PCT;

        	if(randAttack >= 0 && randAttack < SPAM_Delay_Pointer) 
        	{
    			node.setSPAM_Delay(true);
    			node.setSPAM_Delay_Period(SPAM_Delay_Period);
    			
        		//System.out.println("");
        		//System.out.println("MPSP "+i+" is SPAM_Delay");
    			
        	}
        	else if(randAttack >= SPAM_Delay_Pointer && randAttack < SPAM_Bombarding_Pointer)
        	{
    			node.setSPAM_Bombarding(true);
    			node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
    			
        		//System.out.println("");
        		//System.out.println("MPSP "+i+" is SPAM_Bombarding");
    	    
        	}
        	else if(randAttack >= SPAM_Bombarding_Pointer && randAttack < SPAM_Drop_Pointer)
        	{
        		node.setSPAM_Drop(true);
        		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
        		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
        		
        		//System.out.println("");
        		//System.out.println("MPSP "+i+" is SPAM_Drop");

        	}
        	else if(randAttack >= SPAM_Drop_Pointer && randAttack < SPAM_Delay_Bombarding_Pointer)
        	{
        		node.setSPAM_Delay(true);
        		node.setSPAM_Delay_Period(SPAM_Delay_Period);
        		
        		node.setSPAM_Bombarding(true);
        		node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
        				
        		//System.out.println("");
        		//System.out.println("MPSP "+i+" is SPAM_Delay_Bombarding");

        	}
        	else if(randAttack >= SPAM_Delay_Bombarding_Pointer && randAttack < SPAM_Delay_Drop_Pointer)
        	{
        		node.setSPAM_Delay(true);
        		node.setSPAM_Delay_Period(SPAM_Delay_Period);
        		
        		node.setSPAM_Drop(true);
        		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
        		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);

        		//System.out.println("");
        		//System.out.println("MPSP "+i+" is SPAM_Delay_Drop");
        		
        	}
        	else if(randAttack >= SPAM_Delay_Drop_Pointer && randAttack < SPAM_Drop_Bombarding_Pointer)
        	{
    			node.setSPAM_Drop(true);
        		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
        		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
        		
        		node.setSPAM_Bombarding(true);
        		node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
        				
        		//System.out.println("");
        		//System.out.println("MPSP "+i+" is SPAM_Drop_Bombarding");

        	}
        	else if(randAttack >= SPAM_Drop_Bombarding_Pointer && randAttack <= SPAM_Delay_Drop_Bombarding_Pointer)
        	{
    			node.setSPAM_Delay(true);
        		node.setSPAM_Delay_Period(SPAM_Delay_Period);
        		
        		node.setSPAM_Drop(true);
        		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
        		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
        		
        		node.setSPAM_Bombarding(true);
        		node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);

        		//System.out.println("");
        		//System.out.println("MPSP "+i+" is SPAM_Delay_Drop_Bombarding");

        	}        	

        	System.out.println("MPSPid: "+nodePosition);	
        	//System.out.println("Node: "+node.getNodePos()+" has Usefulness = "+node.getUsefulness());
        }
        else if(nodeType == 4 && spNodeType == 2)
        {
        	SP_Node node = (SP_Node) n.getProtocol(SPPid);
        	node.setCreationCycle(CommonState.getIntTime());
        	node.setNodePos(this.nodePosition);
        	node.setRMGRPid(RMGRPid);
        	node.setSuperPid(SuperPid);
        	node.setIDPPid(IDPPid);
        	node.setAuditorPid(AuditorPid);
        	node.setUserPid(UserPid);
        	node.setSPPid(SPPid);
        	node.setMEPid(MEPid);
        	node.setNodeType(4);
        	node.setrGen(rGenerator);
        	node.setUsefulnessMIN(usefulnessMIN);
        	node.setUsefulness(node.randDouble(usefulnessMIN, 100));
            node.setAcceptDGUProb(AcceptDGUProb);
            node.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
        	node.setAcceptCompulsoryDGUProb(AcceptCompulsoryDGUProb);
        	node.setAcceptCompulsoryStrictDGUProb(AcceptCompulsoryStrictDGUProb);
        	
        	System.out.println("PSPid: "+nodePosition);
        }
        else if(nodeType == 4 && spNodeType == 3)
        {
        	SP_Node node = (SP_Node) n.getProtocol(SPPid);
        	node.setCreationCycle(CommonState.getIntTime());
        	node.setNodePos(this.nodePosition);
        	node.setRMGRPid(RMGRPid);
        	node.setSuperPid(SuperPid);
        	node.setIDPPid(IDPPid);
        	node.setAuditorPid(AuditorPid);
        	node.setUserPid(UserPid);
        	node.setSPPid(SPPid);
        	node.setMEPid(MEPid);
        	node.setNodeType(4);
        	node.setrGen(rGenerator);
        	node.setUsefulnessMIN(usefulnessMIN);
        	node.setUsefulness(node.randDouble(0, usefulnessMIN));
        	node.setMalicious(true);
        	node.setSPAM_Drop_StrictCredential_Prob(SPAM_Drop_StrictCredential_Prob);
        	node.setNewPartnershipProb(newPartnershipProb);
        	node.setNewPartnershipDuration(newPartnershipDuration);
            node.setPartnerSharingProb(partnerSharingProb);
            node.setAcceptDGUProb(AcceptDGUProb);
            node.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
        	
        	double randAttack = node.randDouble(0, 100);
        	//System.out.println("");
        	//System.out.println("randAttack = "+randAttack);
        	double SPAM_Delay_Pointer = SPAM_Delay_MSP_PCT;
        	double SPAM_Bombarding_Pointer = SPAM_Delay_Pointer + SPAM_Bombarding_MSP_PCT;
        	double SPAM_Drop_Pointer = SPAM_Bombarding_Pointer + SPAM_Drop_MSP_PCT;
        	double SPAM_Delay_Bombarding_Pointer = SPAM_Drop_Pointer + SPAM_Delay_Bombarding_MSP_PCT;
        	double SPAM_Delay_Drop_Pointer = SPAM_Delay_Bombarding_Pointer + SPAM_Delay_Drop_MSP_PCT;
        	double SPAM_Drop_Bombarding_Pointer = SPAM_Delay_Drop_Pointer + SPAM_Drop_Bombarding_MSP_PCT;
        	double SPAM_Delay_Drop_Bombarding_Pointer = SPAM_Drop_Bombarding_Pointer + SPAM_Delay_Drop_Bombarding_MSP_PCT;

        	if(randAttack >= 0 && randAttack < SPAM_Delay_Pointer)
        	{

    			node.setSPAM_Delay(true);
    			node.setSPAM_Delay_Period(SPAM_Delay_Period);
    			
        		//System.out.println("");
        		//System.out.println("MSP "+i+" is SPAM_Delay");
    		
        	}
        	else if(randAttack >= SPAM_Delay_Pointer && randAttack < SPAM_Bombarding_Pointer)
        	{

    			node.setSPAM_Bombarding(true);
    			node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
    			
        		//System.out.println("");
        		//System.out.println("MSP "+i+" is SPAM_Bombarding");
        	}
        	else if(randAttack >= SPAM_Bombarding_Pointer && randAttack < SPAM_Drop_Pointer)
        	{
        		node.setSPAM_Drop(true);
        		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
        		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
        		
        		//System.out.println("");
        		//System.out.println("MSP "+i+" is SPAM_Drop");
        	}
        	else if(randAttack >= SPAM_Drop_Pointer && randAttack < SPAM_Delay_Bombarding_Pointer)
        	{
        		node.setSPAM_Delay(true);
        		node.setSPAM_Delay_Period(SPAM_Delay_Period);
        		
        		node.setSPAM_Bombarding(true);
        		node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
        				
        		//System.out.println("");
        		//System.out.println("MSP "+i+" is SPAM_Delay_Bombarding");

        	}
        	else if(randAttack >= SPAM_Delay_Bombarding_Pointer && randAttack < SPAM_Delay_Drop_Pointer)
        	{
        		node.setSPAM_Delay(true);
        		node.setSPAM_Delay_Period(SPAM_Delay_Period);
        		
        		node.setSPAM_Drop(true);
        		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
        		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);

        		//System.out.println("");
        		//System.out.println("MSP "+i+" is SPAM_Delay_Drop");

        	}
        	else if(randAttack >= SPAM_Delay_Drop_Pointer && randAttack < SPAM_Drop_Bombarding_Pointer)
        	{
    			node.setSPAM_Drop(true);
        		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
        		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
        		
        		node.setSPAM_Bombarding(true);
        		node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);
        				
        		//System.out.println("");
        		//System.out.println("MSP "+i+" is SPAM_Drop_Bombarding");

        	}
        	else if(randAttack >= SPAM_Drop_Bombarding_Pointer && randAttack <= SPAM_Delay_Drop_Bombarding_Pointer)
        	{
    			node.setSPAM_Delay(true);
        		node.setSPAM_Delay_Period(SPAM_Delay_Period);
        		
        		node.setSPAM_Drop(true);
        		node.setSPAM_DropCredential_Rate(SPAM_DropCredential_Rate);
        		node.setSPAM_DropUser_Rate(SPAM_DropUser_Rate);
        		
        		node.setSPAM_Bombarding(true);
        		node.setSPAM_Bombarding_Period(SPAM_Bombarding_Period);

        		//System.out.println("");
        		//System.out.println("MSP "+i+" is SPAM_Delay_Drop_Bombarding");

        	}
        	
        	System.out.println("MSPid: "+nodePosition);
        }
        else if(nodeType == 4 && spNodeType == 4)
        {
        	SP_Node node = (SP_Node) n.getProtocol(SPPid);
        	node.setCreationCycle(CommonState.getIntTime());
        	node.setNodePos(this.nodePosition);
        	node.setRMGRPid(RMGRPid);
        	node.setSuperPid(SuperPid);
        	node.setIDPPid(IDPPid);
        	node.setAuditorPid(AuditorPid);
        	node.setUserPid(UserPid);
        	node.setSPPid(SPPid);
        	node.setMEPid(MEPid);
        	node.setNodeType(4);
        	node.setrGen(rGenerator);
        	node.setUsefulnessMIN(usefulnessMIN);
        	node.setUsefulness(node.randDouble(0, usefulnessMIN));
            node.setAcceptDGUProb(AcceptDGUProb);
            node.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
        	node.setAcceptCompulsoryDGUProb(AcceptCompulsoryDGUProb);
        	node.setAcceptCompulsoryStrictDGUProb(AcceptCompulsoryStrictDGUProb);
        	
        	System.out.println("SPid: "+nodePosition);
        } 
    }
    
    public void initializeME(Node n)
    {
    	Random rGenerator = new Random();
    	
    	ME_Node node = (ME_Node) n.getProtocol(MEPid);
    	node.setCreationCycle(CommonState.getIntTime());
		node.setME_MAX_MPSPs(ME_MAX_MPSPs);
		node.setME_MAX_MSPs(ME_MAX_MSPs);
		node.setME_MPSPsGR(ME_MPSPsGR);
		node.setME_MSPsGR(ME_MSPsGR);
		node.setME_N_MIN_MPSPs_toSPAM_Credential(ME_N_MIN_MPSPs_toSPAM_Credential);
		node.setME_N_MIN_MPSPs_toSPAM_User(ME_N_MIN_MPSPs_toSPAM_User);
		node.setNodePos(this.nodePosition);
    	node.setRMGRPid(RMGRPid);
    	node.setSuperPid(SuperPid);
    	node.setIDPPid(IDPPid);
    	node.setAuditorPid(AuditorPid);
    	node.setUserPid(UserPid);
    	node.setSPPid(SPPid);
    	node.setMEPid(MEPid);
		node.setNodeType(5);
		node.setrGen(rGenerator);
		node.setUsefulnessMIN(usefulnessMIN);
		
		boolean unpopularColluding = node.randBoolean((double) (1-(ME_Colluding_Unpopular_PCT/100)));
		if(unpopularColluding)
			node.setUnpopularSPsColluding(true);
		else
			node.setUnpopularSPsColluding(false);
		
		int i = nodePosition;
		int MEid = i;
		System.out.println("MEid: "+nodePosition+" unpopularSPsColluding? "+unpopularColluding);
		i++;
		
		Network.add(n);
		
		i = initializeMEnodes(i, MEid, true, INI_ME_MPSPs, INI_ME_MSPs);

		i = initializeMEnodes(i, MEid, false, INI_ME_MPSPs, INI_ME_MSPs);	
    }
    
    public int initializeMEnodes(int i, int MEid, boolean MPSP,int INI_ME_MPSPs, int  INI_ME_MSPs)
    {	
    	double SPAM_Delay_Pointer = SPAM_Delay_ME_MPSP_PCT;
    	double SPAM_Bombarding_Pointer = SPAM_Delay_Pointer + SPAM_Bombarding_ME_MPSP_PCT;
    	double SPAM_Drop_Pointer = SPAM_Bombarding_Pointer + SPAM_Drop_ME_MPSP_PCT;
    	double SPAM_Delay_Bombarding_Pointer = SPAM_Drop_Pointer + SPAM_Delay_Bombarding_ME_MPSP_PCT;
    	double SPAM_Delay_Drop_Pointer = SPAM_Delay_Bombarding_Pointer + SPAM_Delay_Drop_ME_MPSP_PCT;
    	double SPAM_Drop_Bombarding_Pointer = SPAM_Delay_Drop_Pointer + SPAM_Drop_Bombarding_ME_MPSP_PCT;
    	double SPAM_Delay_Drop_Bombarding_Pointer = SPAM_Drop_Bombarding_Pointer + SPAM_Delay_Drop_Bombarding_ME_MPSP_PCT;
    	
		double sumME_SPAM_Delay = 0;
    	double sumME_SPAM_Bombarding = 0;
    	double sumME_SPAM_Drop = 0;
    	double sumME_SPAM_Delay_Bombarding = 0;
    	double sumME_SPAM_Delay_Drop = 0;
    	double sumME_SPAM_Drop_Bombarding = 0;
    	double sumME_SPAM_Delay_Drop_Bombarding = 0;
    	
    	int nodesNum = 0;
    	
    	if(MPSP)
    	{
    		sumME_SPAM_Delay = (SPAM_Delay_ME_MPSP_PCT/100) * INI_ME_MPSPs;
        	sumME_SPAM_Bombarding = (SPAM_Bombarding_ME_MPSP_PCT/100) * INI_ME_MPSPs;
        	sumME_SPAM_Drop = (SPAM_Drop_ME_MPSP_PCT/100) * INI_ME_MPSPs;
        	sumME_SPAM_Delay_Bombarding = (SPAM_Delay_Bombarding_ME_MPSP_PCT/100) * INI_ME_MPSPs;
        	sumME_SPAM_Delay_Drop = (SPAM_Delay_Drop_ME_MPSP_PCT/100) * INI_ME_MPSPs;
        	sumME_SPAM_Drop_Bombarding = (SPAM_Drop_Bombarding_ME_MPSP_PCT/100) * INI_ME_MPSPs;
        	sumME_SPAM_Delay_Drop_Bombarding = (SPAM_Delay_Drop_Bombarding_ME_MPSP_PCT/100) * INI_ME_MPSPs;
        	
        	nodesNum = INI_ME_MPSPs;
    	}
    	else
    	{
     		sumME_SPAM_Delay = (SPAM_Delay_ME_MSP_PCT/100) * INI_ME_MSPs;
        	sumME_SPAM_Bombarding = (SPAM_Bombarding_ME_MSP_PCT/100) * INI_ME_MSPs;
        	sumME_SPAM_Drop = (SPAM_Drop_ME_MSP_PCT/100) * INI_ME_MSPs;
        	sumME_SPAM_Delay_Bombarding = (SPAM_Delay_Bombarding_ME_MSP_PCT/100) * INI_ME_MSPs;
        	sumME_SPAM_Delay_Drop = (SPAM_Delay_Drop_ME_MSP_PCT/100) * INI_ME_MSPs;
        	sumME_SPAM_Drop_Bombarding = (SPAM_Drop_Bombarding_ME_MSP_PCT/100) * INI_ME_MSPs;
        	sumME_SPAM_Delay_Drop_Bombarding = (SPAM_Delay_Drop_Bombarding_ME_MSP_PCT/100) * INI_ME_MSPs;
        	
        	nodesNum = INI_ME_MSPs;
    	}
    	
    	Random rGenerator = new Random();
    	
    	Node n;

    	int k = 0;	
    	
    	IDP_Node idp = (IDP_Node) Network.get(1).getProtocol(IDPPid);
    	
    	for(int l = 0; l < sumME_SPAM_Delay; l++)
    	{
    		if(sumME_SPAM_Delay - l < 1) break;
    		
    		
    		n = (Node) Network.prototype.clone();
    		
			SP_Node node_sp = (SP_Node) n.getProtocol(SPPid);
			node_sp.setCreationCycle(CommonState.getIntTime());
			node_sp.setAssociatedWithME(true);
			node_sp.setMalicious(true);
			node_sp.setMEid(MEid);
			node_sp.setNodePos(i);
			node_sp.setRMGRPid(RMGRPid);
			node_sp.setSuperPid(SuperPid);
			node_sp.setIDPPid(IDPPid);
			node_sp.setAuditorPid(AuditorPid);
			node_sp.setUserPid(UserPid);
			node_sp.setSPPid(SPPid);
			node_sp.setMEPid(MEPid);
			node_sp.setNodeType(4);
			node_sp.setrGen(rGenerator);
			node_sp.setUsefulnessMIN(usefulnessMIN);
			node_sp.setSPAM_Drop_StrictCredential_Prob(SPAM_Drop_StrictCredential_Prob);
        	node_sp.setNewPartnershipProb(newPartnershipProb);
        	node_sp.setNewPartnershipDuration(newPartnershipDuration);
            node_sp.setPartnerSharingProb(partnerSharingProb);
            node_sp.setAcceptDGUProb(AcceptDGUProb);
            node_sp.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
			
			if(MPSP)
				node_sp.setUsefulness(node_sp.randDouble(usefulnessMIN, 100));
			else
				node_sp.setUsefulness(node_sp.randDouble(0, usefulnessMIN));
			
			node_sp.setSPAM_Delay(true);
    		node_sp.setSPAM_Delay_Period(SPAM_Delay_ME_Period);
    		
    		Network.add(n);
        	idp.addNodeDir(i, 4, SPRank);
    		
        	ME_Node meNode = (ME_Node) Network.get(MEid).getProtocol(MEPid);
        	meNode.addMSPNode(node_sp, MPSP);
        	
    		if(MPSP)
    			System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay");
    		else
    			System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay");

    		i++;
    		k++;
    		//result.add(node_sp);
    	}
    	
    	for(int l = 0; l < sumME_SPAM_Bombarding; l++)
    	{
    		if(sumME_SPAM_Bombarding - l < 1) break;
    		
    		n = (Node) Network.prototype.clone();
			SP_Node node_sp = (SP_Node) n.getProtocol(SPPid);
			node_sp.setCreationCycle(CommonState.getIntTime());
			node_sp.setAssociatedWithME(true);
			node_sp.setMalicious(true);
			node_sp.setMEid(MEid);
			node_sp.setNodePos(i);
			node_sp.setRMGRPid(RMGRPid);
			node_sp.setSuperPid(SuperPid);
			node_sp.setIDPPid(IDPPid);
			node_sp.setAuditorPid(AuditorPid);
			node_sp.setUserPid(UserPid);
			node_sp.setSPPid(SPPid);
			node_sp.setMEPid(MEPid);
			node_sp.setNodeType(4);
			node_sp.setrGen(rGenerator);
			node_sp.setUsefulnessMIN(usefulnessMIN);
			node_sp.setSPAM_Drop_StrictCredential_Prob(SPAM_Drop_StrictCredential_Prob);
			node_sp.setNewPartnershipProb(newPartnershipProb);
        	node_sp.setNewPartnershipDuration(newPartnershipDuration);
            node_sp.setPartnerSharingProb(partnerSharingProb);
            node_sp.setAcceptDGUProb(AcceptDGUProb);
            node_sp.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
			if(MPSP)
				node_sp.setUsefulness(node_sp.randDouble(usefulnessMIN, 100));
			else
				node_sp.setUsefulness(node_sp.randDouble(0, usefulnessMIN));
			
    		node_sp.setSPAM_Bombarding(true);
    		node_sp.setSPAM_Bombarding_Period(SPAM_Bombarding_ME_Period);
    		
    		Network.add(n);
        	idp.addNodeDir(i, 4, SPRank);
        	
        	ME_Node meNode = (ME_Node) Network.get(MEid).getProtocol(MEPid);
        	meNode.addMSPNode(node_sp, MPSP);
    		
    		if(MPSP)
    			System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Bombarding");
    		else
    			System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Bombarding");
    		
    		i++;
    		k++;
    		//result.add(node_sp);
    	}
    	
    	for(int l = 0; l < sumME_SPAM_Drop; l++)
    	{
    		if(sumME_SPAM_Drop - l < 1) break;
    		
    		n = (Node) Network.prototype.clone();
			SP_Node node_sp = (SP_Node) n.getProtocol(SPPid);
			node_sp.setCreationCycle(CommonState.getIntTime());
			node_sp.setAssociatedWithME(true);
			node_sp.setMalicious(true);
			node_sp.setMEid(MEid);
			node_sp.setNodePos(i);
			node_sp.setRMGRPid(RMGRPid);
			node_sp.setSuperPid(SuperPid);
			node_sp.setIDPPid(IDPPid);
			node_sp.setAuditorPid(AuditorPid);
			node_sp.setUserPid(UserPid);
			node_sp.setSPPid(SPPid);
			node_sp.setMEPid(MEPid);
			node_sp.setNodeType(4);
			node_sp.setrGen(rGenerator);
			node_sp.setUsefulnessMIN(usefulnessMIN);
			node_sp.setSPAM_Drop_StrictCredential_Prob(SPAM_Drop_StrictCredential_Prob);
			node_sp.setNewPartnershipProb(newPartnershipProb);
        	node_sp.setNewPartnershipDuration(newPartnershipDuration);
            node_sp.setPartnerSharingProb(partnerSharingProb);
            node_sp.setAcceptDGUProb(AcceptDGUProb);
            node_sp.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
			if(MPSP)
				node_sp.setUsefulness(node_sp.randDouble(usefulnessMIN, 100));
			else
				node_sp.setUsefulness(node_sp.randDouble(0, usefulnessMIN));
			
    		node_sp.setSPAM_Drop(true);
    		node_sp.setSPAM_DropCredential_Rate(SPAM_DropCredential_ME_Rate);
    		node_sp.setSPAM_DropUser_Rate(SPAM_DropUser_ME_Rate);
    		
    		if(MPSP)
    			System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Drop");
    		else
    			System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Drop");
    		
    		Network.add(n);
        	idp.addNodeDir(i, 4, SPRank);
        	
        	ME_Node meNode = (ME_Node) Network.get(MEid).getProtocol(MEPid);
        	meNode.addMSPNode(node_sp, MPSP);
    		
    		i++;
    		k++;
    		//result.add(node_sp);
    	}
    	
    	for(int l = 0; l < sumME_SPAM_Delay_Bombarding; l++)
    	{
    		if(sumME_SPAM_Delay_Bombarding - l < 1) break;
    		
    		n = (Node) Network.prototype.clone();
			SP_Node node_sp = (SP_Node) n.getProtocol(SPPid);
			node_sp.setCreationCycle(CommonState.getIntTime());
			node_sp.setAssociatedWithME(true);
			node_sp.setMalicious(true);
			node_sp.setMEid(MEid);
			node_sp.setNodePos(i);
			node_sp.setRMGRPid(RMGRPid);
			node_sp.setSuperPid(SuperPid);
			node_sp.setIDPPid(IDPPid);
			node_sp.setAuditorPid(AuditorPid);
			node_sp.setUserPid(UserPid);
			node_sp.setSPPid(SPPid);
			node_sp.setMEPid(MEPid);
			node_sp.setNodeType(4);
			node_sp.setrGen(rGenerator);
			node_sp.setUsefulnessMIN(usefulnessMIN);
			node_sp.setSPAM_Drop_StrictCredential_Prob(SPAM_Drop_StrictCredential_Prob);
			node_sp.setNewPartnershipProb(newPartnershipProb);
        	node_sp.setNewPartnershipDuration(newPartnershipDuration);
            node_sp.setPartnerSharingProb(partnerSharingProb);
            node_sp.setAcceptDGUProb(AcceptDGUProb);
            node_sp.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
			if(MPSP)
				node_sp.setUsefulness(node_sp.randDouble(usefulnessMIN, 100));
			else
				node_sp.setUsefulness(node_sp.randDouble(0, usefulnessMIN));
			
			node_sp.setSPAM_Delay(true);
    		node_sp.setSPAM_Delay_Period(SPAM_Delay_ME_Period);
    		
    		node_sp.setSPAM_Bombarding(true);
    		node_sp.setSPAM_Bombarding_Period(SPAM_Bombarding_ME_Period);
    		
    		Network.add(n);
    		idp.addNodeDir(i, 4, SPRank);
    		
        	ME_Node meNode = (ME_Node) Network.get(MEid).getProtocol(MEPid);
        	meNode.addMSPNode(node_sp, MPSP);
    		
    		if(MPSP)
    			System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Bombarding");
    		else
    			System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Bombarding");
    		
    		i++;
    		k++;
    		//result.add(node_sp);
    	}
    	
    	for(int l = 0; l < sumME_SPAM_Delay_Drop; l++)
    	{
    		if(sumME_SPAM_Delay_Drop - l < 1) break;
    		
    		n = (Node) Network.prototype.clone();
			SP_Node node_sp = (SP_Node) n.getProtocol(SPPid);
			node_sp.setCreationCycle(CommonState.getIntTime());
			node_sp.setAssociatedWithME(true);
			node_sp.setMalicious(true);
			node_sp.setMEid(MEid);
			node_sp.setNodePos(i);
			node_sp.setRMGRPid(RMGRPid);
			node_sp.setSuperPid(SuperPid);
			node_sp.setIDPPid(IDPPid);
			node_sp.setAuditorPid(AuditorPid);
			node_sp.setUserPid(UserPid);
			node_sp.setSPPid(SPPid);
			node_sp.setMEPid(MEPid);
			node_sp.setNodeType(4);
			node_sp.setrGen(rGenerator);
			node_sp.setUsefulnessMIN(usefulnessMIN);
			node_sp.setSPAM_Drop_StrictCredential_Prob(SPAM_Drop_StrictCredential_Prob);
			node_sp.setNewPartnershipProb(newPartnershipProb);
        	node_sp.setNewPartnershipDuration(newPartnershipDuration);
            node_sp.setPartnerSharingProb(partnerSharingProb);
            node_sp.setAcceptDGUProb(AcceptDGUProb);
            node_sp.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
			if(MPSP)
				node_sp.setUsefulness(node_sp.randDouble(usefulnessMIN, 100));
			else
				node_sp.setUsefulness(node_sp.randDouble(0, usefulnessMIN));
			
    		node_sp.setSPAM_Delay(true);
    		node_sp.setSPAM_Delay_Period(SPAM_Delay_ME_Period);
    		
    		node_sp.setSPAM_Drop(true);
    		node_sp.setSPAM_DropCredential_Rate(SPAM_DropCredential_ME_Rate);
    		node_sp.setSPAM_DropUser_Rate(SPAM_DropUser_ME_Rate);
    		
    		Network.add(n);
    		idp.addNodeDir(i, 4, SPRank);
    		
        	ME_Node meNode = (ME_Node) Network.get(MEid).getProtocol(MEPid);
        	meNode.addMSPNode(node_sp, MPSP);
    		
    		if(MPSP)
    			System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Drop");
    		else
    			System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Drop");
    		
    		i++;
    		k++;
    		//result.add(node_sp);
    	}
    	
    	for(int l = 0; l < sumME_SPAM_Drop_Bombarding; l++)
    	{
    		if(sumME_SPAM_Drop_Bombarding - l < 1) break;
    		
    		n = (Node) Network.prototype.clone();
			SP_Node node_sp = (SP_Node) n.getProtocol(SPPid);
			node_sp.setCreationCycle(CommonState.getIntTime());
			node_sp.setAssociatedWithME(true);
			node_sp.setMalicious(true);
			node_sp.setMEid(MEid);
			node_sp.setNodePos(i);
			node_sp.setRMGRPid(RMGRPid);
			node_sp.setSuperPid(SuperPid);
			node_sp.setIDPPid(IDPPid);
			node_sp.setAuditorPid(AuditorPid);
			node_sp.setUserPid(UserPid);
			node_sp.setSPPid(SPPid);
			node_sp.setMEPid(MEPid);
			node_sp.setNodeType(4);
			node_sp.setrGen(rGenerator);
			node_sp.setUsefulnessMIN(usefulnessMIN);
			node_sp.setSPAM_Drop_StrictCredential_Prob(SPAM_Drop_StrictCredential_Prob);
			node_sp.setNewPartnershipProb(newPartnershipProb);
        	node_sp.setNewPartnershipDuration(newPartnershipDuration);
            node_sp.setPartnerSharingProb(partnerSharingProb);
            node_sp.setAcceptDGUProb(AcceptDGUProb);
            node_sp.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
			if(MPSP)
				node_sp.setUsefulness(node_sp.randDouble(usefulnessMIN, 100));
			else
				node_sp.setUsefulness(node_sp.randDouble(0, usefulnessMIN));
			
    		node_sp.setSPAM_Drop(true);
    		node_sp.setSPAM_DropCredential_Rate(SPAM_DropCredential_ME_Rate);
    		node_sp.setSPAM_DropUser_Rate(SPAM_DropUser_ME_Rate);
    		
    		node_sp.setSPAM_Bombarding(true);
    		node_sp.setSPAM_Bombarding_Period(SPAM_Bombarding_ME_Period);
    		
    		Network.add(n);
    		idp.addNodeDir(i, 4, SPRank);
    		
        	ME_Node meNode = (ME_Node) Network.get(MEid).getProtocol(MEPid);
        	meNode.addMSPNode(node_sp, MPSP);
    		
    		if(MPSP)
    			System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Drop_Bombarding");
    		else
    			System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Drop_Bombarding");
    		
    		i++;
    		k++;
    		//result.add(node_sp);
    	}
    	
    	for(int l = 0; l < sumME_SPAM_Delay_Drop_Bombarding; l++)
    	{
    		if(sumME_SPAM_Delay_Drop_Bombarding - l < 1) break;
    		
    		n = (Node) Network.prototype.clone();
			SP_Node node_sp = (SP_Node) n.getProtocol(SPPid);
			node_sp.setCreationCycle(CommonState.getIntTime());
			node_sp.setAssociatedWithME(true);
			node_sp.setMalicious(true);
			node_sp.setMEid(MEid);
			node_sp.setNodePos(i);
			node_sp.setRMGRPid(RMGRPid);
			node_sp.setSuperPid(SuperPid);
			node_sp.setIDPPid(IDPPid);
			node_sp.setAuditorPid(AuditorPid);
			node_sp.setUserPid(UserPid);
			node_sp.setSPPid(SPPid);
			node_sp.setMEPid(MEPid);
			node_sp.setNodeType(4);
			node_sp.setrGen(rGenerator);
			node_sp.setUsefulnessMIN(usefulnessMIN);
			node_sp.setSPAM_Drop_StrictCredential_Prob(SPAM_Drop_StrictCredential_Prob);
			node_sp.setNewPartnershipProb(newPartnershipProb);
        	node_sp.setNewPartnershipDuration(newPartnershipDuration);
            node_sp.setPartnerSharingProb(partnerSharingProb);
            node_sp.setAcceptDGUProb(AcceptDGUProb);
            node_sp.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
			if(MPSP)
				node_sp.setUsefulness(node_sp.randDouble(usefulnessMIN, 100));
			else
				node_sp.setUsefulness(node_sp.randDouble(0, usefulnessMIN));
			
			node_sp.setSPAM_Delay(true);
    		node_sp.setSPAM_Delay_Period(SPAM_Delay_ME_Period);
    		
    		node_sp.setSPAM_Drop(true);
    		node_sp.setSPAM_DropCredential_Rate(SPAM_DropCredential_ME_Rate);
    		node_sp.setSPAM_DropUser_Rate(SPAM_DropUser_ME_Rate);
    		
    		node_sp.setSPAM_Bombarding(true);
    		node_sp.setSPAM_Bombarding_Period(SPAM_Bombarding_ME_Period);
    		
    		Network.add(n);
    		idp.addNodeDir(i, 4, SPRank);
    		
        	ME_Node meNode = (ME_Node) Network.get(MEid).getProtocol(MEPid);
        	meNode.addMSPNode(node_sp, MPSP);
    		
    		if(MPSP)
    			System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Drop_Bombarding");
    		else
    			System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Drop_Bombarding");
    		
    		i++;
    		k++;
    		//result.add(node_sp);
    	}
    	
    	for(int m = k; m < nodesNum; m++)
    	{
    		n = (Node) Network.prototype.clone();
			SP_Node node_sp = (SP_Node) n.getProtocol(SPPid);
			node_sp.setCreationCycle(CommonState.getIntTime());
			node_sp.setAssociatedWithME(true);
			node_sp.setMalicious(true);
			node_sp.setMEid(MEid);
			node_sp.setNodePos(i);
			node_sp.setRMGRPid(RMGRPid);
			node_sp.setSuperPid(SuperPid);
			node_sp.setIDPPid(IDPPid);
			node_sp.setAuditorPid(AuditorPid);
			node_sp.setUserPid(UserPid);
			node_sp.setSPPid(SPPid);
			node_sp.setMEPid(MEPid);
			node_sp.setNodeType(4);
			node_sp.setrGen(rGenerator);
			node_sp.setUsefulnessMIN(usefulnessMIN);
			node_sp.setSPAM_Drop_StrictCredential_Prob(SPAM_Drop_StrictCredential_Prob);
			node_sp.setNewPartnershipProb(newPartnershipProb);
        	node_sp.setNewPartnershipDuration(newPartnershipDuration);
            node_sp.setPartnerSharingProb(partnerSharingProb);
            node_sp.setAcceptDGUProb(AcceptDGUProb);
            node_sp.setAcceptSTrictDGUProb(AcceptStrictDGUProb);
			if(MPSP)
				node_sp.setUsefulness(node_sp.randDouble(usefulnessMIN, 100));
			else
				node_sp.setUsefulness(node_sp.randDouble(0, usefulnessMIN));
			
			Network.add(n);
			idp.addNodeDir(i, 4, SPRank);
			
        	ME_Node meNode = (ME_Node) Network.get(MEid).getProtocol(MEPid);
        	meNode.addMSPNode(node_sp, MPSP);
			
			double randAttack = node_sp.randDouble(0, 100);
    		
    		if(randAttack >= 0 && randAttack < SPAM_Delay_Pointer)
    		{
    			node_sp.setSPAM_Delay(true);
    			node_sp.setSPAM_Delay_Period(SPAM_Delay_ME_Period);

    			if(MPSP)
        			System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay && Rand Attack");
        		else
        			System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay && Rand Attack");
    			
    		}
    		else if(randAttack >= SPAM_Delay_Pointer && randAttack < SPAM_Bombarding_Pointer)
    		{
    			node_sp.setSPAM_Bombarding(true);
    			node_sp.setSPAM_Bombarding_Period(SPAM_Bombarding_ME_Period);
    			
    			if(MPSP)
        			System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Bombarding && Rand Attack");
        		else
        			System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Bombarding && Rand Attack");
 
    		}
    		else if(randAttack >= SPAM_Bombarding_Pointer && randAttack < SPAM_Drop_Pointer)
    		{
    			node_sp.setSPAM_Drop(true);
    			node_sp.setSPAM_DropCredential_Rate(SPAM_DropCredential_ME_Rate);
    			node_sp.setSPAM_DropUser_Rate(SPAM_DropUser_ME_Rate);
    			
    			if(MPSP)
        			System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Drop && Rand Attack");
        		else
        			System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Drop && Rand Attack");
    		
    		}
    		else if(randAttack >= SPAM_Drop_Pointer && randAttack < SPAM_Delay_Bombarding_Pointer)
    		{
    			node_sp.setSPAM_Delay(true);
    			node_sp.setSPAM_Delay_Period(SPAM_Delay_ME_Period);
        		
    			node_sp.setSPAM_Bombarding(true);
    			node_sp.setSPAM_Bombarding_Period(SPAM_Bombarding_ME_Period);
    			
    			if(MPSP)
        			System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Bombarding && Rand Attack");
        		else
        			System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Bombarding && Rand Attack");
    			
    		}
    		else if(randAttack >= SPAM_Delay_Bombarding_Pointer && randAttack < SPAM_Delay_Drop_Pointer)
    		{
    			node_sp.setSPAM_Delay(true);
    			node_sp.setSPAM_Delay_Period(SPAM_Delay_ME_Period);
        		
    			node_sp.setSPAM_Drop(true);
    			node_sp.setSPAM_DropCredential_Rate(SPAM_DropCredential_ME_Rate);
    			node_sp.setSPAM_DropUser_Rate(SPAM_DropUser_ME_Rate);
    			
    			if(MPSP)
        			System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Drop && Rand Attack");
        		else
        			System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Drop && Rand Attack");
    		}
    		else if(randAttack >= SPAM_Delay_Drop_Pointer && randAttack < SPAM_Drop_Bombarding_Pointer)
    		{
    			node_sp.setSPAM_Drop(true);
    			node_sp.setSPAM_DropCredential_Rate(SPAM_DropCredential_ME_Rate);
    			node_sp.setSPAM_DropUser_Rate(SPAM_DropUser_ME_Rate);
        		
    			node_sp.setSPAM_Bombarding(true);
    			node_sp.setSPAM_Bombarding_Period(SPAM_Bombarding_ME_Period);
    			
    			if(MPSP)
        			System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Drop_Bombarding && Rand Attack");
        		else
        			System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Drop_Bombarding && Rand Attack");
    			
    		}
    		else if(randAttack >= SPAM_Drop_Bombarding_Pointer && randAttack <= SPAM_Delay_Drop_Bombarding_Pointer)
    		{
    			node_sp.setSPAM_Delay(true);
    			node_sp.setSPAM_Delay_Period(SPAM_Delay_ME_Period);
        		
    			node_sp.setSPAM_Drop(true);
    			node_sp.setSPAM_DropCredential_Rate(SPAM_DropCredential_ME_Rate);
    			node_sp.setSPAM_DropUser_Rate(SPAM_DropUser_ME_Rate);
        		
    			node_sp.setSPAM_Bombarding(true);
    			node_sp.setSPAM_Bombarding_Period(SPAM_Bombarding_ME_Period);
    			
    			if(MPSP)
        			System.out.println("MPSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Drop_Bombarding && Rand Attack");
        		else
        			System.out.println("MSP: "+i+" belonging to MEid: "+MEid+" SPAM_Delay_Drop_Bombarding && Rand Attack");
    			
    		}
    		
    		i++;
    		//result.add(node_sp);
    	}
    	
    	return i;
    }


	public int getNodeType() {
		return nodeType;
	}

	public void setNodeType(int nodeType) {
		this.nodeType = nodeType;
	}

	public int getNodePosition() {
		return nodePosition;
	}

	public void setNodePosition(int nodePosition) {
		this.nodePosition = nodePosition;
		//System.out.println("Setter - nodePosition: "+nodePosition);
	}

	public int getSpNodeType() {
		return spNodeType;
	}

	public void setSpNodeType(int spNodeType) {
		this.spNodeType = spNodeType;
	}

	public int getAgentNodeType() {
		return agentNodeType;
	}

	public void setAgentNodeType(int agentNodeType) {
		this.agentNodeType = agentNodeType;
	}

	public int getSTtargetedSPid() {
		return STtargetedSPid;
	}

	public void setSTtargetedSPid(int sTtargetedSPid) {
		STtargetedSPid = sTtargetedSPid;
	}

	public List<GT_TargetedSP> getGT_TargetedSPs() {
		return GT_TargetedSPs;
	}

	public void setGT_TargetedSPs(List<GT_TargetedSP> gT_TargetedSPs) {
		GT_TargetedSPs = gT_TargetedSPs;
	}

	public int getGTid() {
		return GTid;
	}

	public void setGTid(int gTid) {
		GTid = gTid;
	}

	public double getUserIniTrustinNetwork() {
		return userIniTrustinNetwork;
	}

	public void setUserIniTrustinNetwork(double userIniTrustinNetwork) {
		if(userIniTrustinNetwork >= 0)
			this.userIniTrustinNetwork = userIniTrustinNetwork;
		else
			this.userIniTrustinNetwork = TrustInNetwork;
	}
	
	public int getExprimentTrial_k() {
		return exprimentTrial_k;
	}

	public void setExprimentTrial_k(int exprimentTrial_k) {
		this.exprimentTrial_k = exprimentTrial_k;
		System.out.println("Dynamic set trial = "+exprimentTrial_k);
		
		TrialsCatalouge catalouge = new TrialsCatalouge(Factors);
    	
    	int trial = (exprimentTrial_k % catalouge.size())+1;
    	
    	System.out.println("trial: "+trial);
    	
    	for(Factor factor: FactorsList)
    	{
    		switch(factor.getFactor()){
    		    			
    		case "STmaxLifeTime":
    			
    			if(Fixed)
    			{
    				STmaxLifeTime = (int) factor.getFixed();
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				STmaxLifeTime = (int) factor.getHigh();
    			else
    				STmaxLifeTime = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+STmaxLifeTime);
    			break;
    			
    		case "STreportSpam":
    			
    			if(Fixed)
    			{
    				if(factor.getFixed() == 1) STreportSpam = true;
    				else STreportSpam = false;
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    			{
    				if(factor.getHigh() == 1) STreportSpam = true;
    				else STreportSpam = false;
    			}
    			else
    			{
    				if(factor.getLow() == 1) STreportSpam = true;
    				else STreportSpam = false;
    			}
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+STreportSpam);
    			break;
    						
    		case "GTmaxLifeTime":
    			
    			if(Fixed)
    			{
    				GTmaxLifeTime = (int) factor.getFixed();
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				GTmaxLifeTime = (int) factor.getHigh();
    			else
    				GTmaxLifeTime = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+GTmaxLifeTime);
    			break;
    			
    		case "GTreportSpam":
    			
    			if(Fixed)
    			{
    				if(factor.getFixed() == 1) GTreportSpam = true;
    				else GTreportSpam = false;
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    			{
      				if(factor.getHigh() == 1) GTreportSpam = true;
    				else GTreportSpam = false;
    			}
    			else
    			{
      				if(factor.getLow() == 1) GTreportSpam = true;
    				else GTreportSpam = false;
    			}
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+GTreportSpam);
    			break;
    			    			
    		case "IgnoranceRate":
    			
    			if(Fixed)
    			{
    				IgnoranceRate = factor.getFixed();
    				break;
    			}    			
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				IgnoranceRate = factor.getHigh();
    			else
    				IgnoranceRate = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+IgnoranceRate);
    			break;
    			
    		case "AcceptDGUProb":
    			
    			if(Fixed)
    			{
    				AcceptDGUProb = factor.getFixed();
    				break;
    			}   
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				AcceptDGUProb = factor.getHigh();
    			else
    				AcceptDGUProb = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+AcceptDGUProb);
    			break;
    			
    		case "AcceptCompulsoryDGUProb":
    			
    			if(Fixed)
    			{
    				AcceptCompulsoryDGUProb = factor.getFixed();
    				break;
    			}   
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				AcceptCompulsoryDGUProb = factor.getHigh();
    			else
    				AcceptCompulsoryDGUProb = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+AcceptCompulsoryDGUProb);
    			break;
    			
    		case "AcceptStrictDGUProb":
    			
    			if(Fixed)
    			{
    				AcceptStrictDGUProb = factor.getFixed();
    				break;
    			}   
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				AcceptStrictDGUProb = factor.getHigh();
    			else
    				AcceptStrictDGUProb = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+AcceptStrictDGUProb);
    			break;
    			
    		case "AcceptCompulsoryStrictDGUProb":
    			
    			if(Fixed)
    			{
    				AcceptCompulsoryStrictDGUProb = factor.getFixed();
    				break;
    			}   
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				AcceptCompulsoryStrictDGUProb = factor.getHigh();
    			else
    				AcceptCompulsoryStrictDGUProb = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+AcceptCompulsoryStrictDGUProb);
    			break;
    			    			
    		case "INI_ME_MPSPs":
    			
    			if(Fixed)
    			{
    				INI_ME_MPSPs = (int) factor.getFixed();
    				break;
    			} 
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				INI_ME_MPSPs = (int) factor.getHigh();
    			else
    				INI_ME_MPSPs = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+INI_ME_MPSPs);
    			break;
    			
    		case "INI_ME_MSPs":
    			
    			if(Fixed)
    			{
    				INI_ME_MSPs = (int) factor.getFixed();
    				break;
    			} 
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				INI_ME_MSPs = (int) factor.getHigh();
    			else
    				INI_ME_MSPs = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+INI_ME_MSPs);
    			break;
    			    			
    		case "ME_MPSPsGR":
    			
    			if(Fixed)
    			{
    				ME_MPSPsGR = factor.getFixed();
    				break;
    			} 
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				ME_MPSPsGR = factor.getHigh();
    			else
    				ME_MPSPsGR = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+ME_MPSPsGR);
    			break;
    			    			
    		case "ME_MSPsGR":
    			
    			if(Fixed)
    			{
    				ME_MSPsGR = factor.getFixed();
    				break;
    			} 
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				ME_MSPsGR = factor.getHigh();
    			else
    				ME_MSPsGR = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+ME_MSPsGR);
    			break;
    		
    		case "SPAM_Delay_Period":
    			
    			if(Fixed)
    			{
    				SPAM_Delay_Period = (int)  factor.getFixed();
    				break;
    			} 
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SPAM_Delay_Period = (int) factor.getHigh();
    			else
    				SPAM_Delay_Period = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SPAM_Delay_Period);
    			break;
    			
    		case "SPAM_Bombarding_Period":
    			
    			if(Fixed)
    			{
    				SPAM_Bombarding_Period = (int)  factor.getFixed();
    				break;
    			} 
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SPAM_Bombarding_Period = (int) factor.getHigh();
    			else
    				SPAM_Bombarding_Period = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SPAM_Bombarding_Period);
    			break;
    			
    		case "SPAM_DropUser_Rate":
    			
    			if(Fixed)
    			{
    				SPAM_DropUser_Rate = factor.getFixed();
    				break;
    			} 
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SPAM_DropUser_Rate = factor.getHigh();
    			else
    				SPAM_DropUser_Rate = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SPAM_DropUser_Rate);
    			break;
    			
    		case "SPAM_DropCredential_Rate":
    			
    			if(Fixed)
    			{
    				SPAM_DropCredential_Rate = factor.getFixed();
    				break;
    			} 
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SPAM_DropCredential_Rate = factor.getHigh();
    			else
    				SPAM_DropCredential_Rate = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SPAM_DropCredential_Rate);
    			break;
    		case "SPAM_Delay_ME_Period":
    			
      			if(Fixed)
    			{
      				SPAM_Delay_ME_Period = (int) factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SPAM_Delay_ME_Period = (int) factor.getHigh();
    			else
    				SPAM_Delay_ME_Period = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SPAM_Delay_ME_Period);
    			break;
    			
    		case "SPAM_Bombarding_ME_Period":
    			
      			if(Fixed)
    			{
      				SPAM_Bombarding_ME_Period = (int) factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SPAM_Bombarding_ME_Period = (int) factor.getHigh();
    			else
    				SPAM_Bombarding_ME_Period = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SPAM_Bombarding_ME_Period);
    			break;
    			
    		case "SPAM_DropUser_ME_Rate":
    			
      			if(Fixed)
    			{
      				SPAM_DropUser_ME_Rate = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SPAM_DropUser_ME_Rate = factor.getHigh();
    			else
    				SPAM_DropUser_ME_Rate = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SPAM_DropUser_ME_Rate);
    			break;
    			
    		case "SPAM_DropCredential_ME_Rate":
    			
      			if(Fixed)
    			{
      				SPAM_DropCredential_ME_Rate = factor.getFixed();
    				
    				break;
    			}
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				SPAM_DropCredential_ME_Rate = factor.getHigh();
    			else
    				SPAM_DropCredential_ME_Rate = factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+SPAM_DropCredential_ME_Rate);
    			break;
     		
    		case "ME_N_MIN_MPSPs_toSPAM_User":
    			
    			if(Fixed)
    			{
    				ME_N_MIN_MPSPs_toSPAM_User = (int) factor.getFixed();
    				break;
    			} 
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				ME_N_MIN_MPSPs_toSPAM_User = (int) factor.getHigh();
    			else
    				ME_N_MIN_MPSPs_toSPAM_User = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+ME_N_MIN_MPSPs_toSPAM_User);
    			break;
    			
    		case "ME_N_MIN_MPSPs_toSPAM_Credential":
    			
    			if(Fixed)
    			{
    				ME_N_MIN_MPSPs_toSPAM_Credential = (int) factor.getFixed();
    				break;
    			} 
    			
    			if(catalouge.getFactorStatus(trial, factor.getOrder()))
    				ME_N_MIN_MPSPs_toSPAM_Credential = (int) factor.getHigh();
    			else
    				ME_N_MIN_MPSPs_toSPAM_Credential = (int) factor.getLow();
    			
    			System.out.println("Factor: "+factor.getFactor()+" Ordered: "+factor.getOrder()+" is set to "+ME_N_MIN_MPSPs_toSPAM_Credential);
    			break;
    			
    		default:
    			System.out.println("Factor: "+factor.getFactor()+" is ignored by Dynamic Initiliazer!");
    			break;
    		}
    	}
		
	}
        
}
