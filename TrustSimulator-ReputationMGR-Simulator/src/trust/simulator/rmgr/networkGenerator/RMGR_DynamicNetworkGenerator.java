/*
 * Copyright (c) 2003-2005 The BISON Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 */

package trust.simulator.rmgr.networkGenerator;

import java.util.List;

import peersim.config.Configuration;
import peersim.core.*;
import peersim.dynamics.DynamicNetwork;
import trust.simulator.rmgr.protocol.auditor.Auditor_GTNodesDir;
import trust.simulator.rmgr.protocol.auditor.Auditor_Node;
import trust.simulator.rmgr.protocol.auditor.Auditor_STNodesDir;
import trust.simulator.rmgr.protocol.idp.IDP_Node;
import trust.simulator.rmgr.protocol.me.ME_NodesToAdd;
import trust.simulator.rmgr.protocol.superNode.Super_Node;

/**
 * This {@link Control} can change the size of networks by adding and removing
 * nodes. Can be used to model churn. This class supports only permanent removal
 * of nodes and the addition of brand new nodes. That is, temporary downtime
 * is not supported by this class.
 */
public class RMGR_DynamicNetworkGenerator extends DynamicNetwork
{

// --------------------------------------------------------------------------
// Parameters
// --------------------------------------------------------------------------


/**
 * The protocol to operate on.
 * 
 * @config
 */
	private static final String PAR_Super_Node_PROT = "Super_Node";
	private static final String PAR_Auditor_Node_PROT = "Auditor_Node";
	private static final String PAR_IDP_Node_PROT = "IDP_Node";
	//private static final String PAR_ME_Node_PROT = "ME_Node";
    
    private static final String PAR_SPRank = "SPRank";
    private static final String PAR_TrustInNetworkDynamic = "TrustInNetworkDynamic";
    
	


// --------------------------------------------------------------------------
// Fields
// --------------------------------------------------------------------------

    private static int exprimentTrial_k;
    
/** Protocol identifier; obtained from config property {@link #PAR_PROT}. */
	private static int SuperNodePid;
	private static int AuditorPid;
	private static int IDPPid;
	//private static int MEPid;
	
	private static double SPRank;
	private static boolean TrustInNetworkDynamic;


// --------------------------------------------------------------------------
// Protected methods
// --------------------------------------------------------------------------

/**
 * Adds n nodes to the network. Extending classes can implement any algorithm to
 * do that. The default algorithm adds the given number of nodes after calling
 * all the configured initializers on them.
 * 
 * @param n
 *          the number of nodes to add, must be non-negative.
 */
protected void addUsers(int n, double UserIniTrustinNetwork)
{
	IDP_Node idp = (IDP_Node) Network.get(1).getProtocol(IDPPid);
	
	for (int i = 0; i < n; ++i) {	
		
		Node newnode = (Node) Network.prototype.clone();
		
		DynamicInitializer init = (DynamicInitializer) inits[0];
		
		int nodePos = Network.size();
		
		init.setNodePosition(nodePos);
		init.setNodeType(3);
		init.setAgentNodeType(0);
		if(TrustInNetworkDynamic)
			init.setUserIniTrustinNetwork(UserIniTrustinNetwork);
		
		init.initialize(newnode);
	
		Network.add(newnode);
		
		idp.addNodeDir(nodePos, 3, SPRank);
	
	}
}

protected void addPSPs(int n)
{
	IDP_Node idp = (IDP_Node) Network.get(1).getProtocol(IDPPid);
		
	for (int i = 0; i < n; ++i) {
		
		Node newnode = (Node) Network.prototype.clone();
		
		DynamicInitializer init = (DynamicInitializer) inits[0];
	
		int nodePos = Network.size();
		
		init.setNodePosition(nodePos);
		init.setNodeType(4);
		init.setSpNodeType(2);
		init.initialize(newnode);
		
		Network.add(newnode);
		
		idp.addNodeDir(nodePos, 4, SPRank);
	}
}

protected void addSPs(int n)
{
	IDP_Node idp = (IDP_Node) Network.get(1).getProtocol(IDPPid);
	
	for (int i = 0; i < n; ++i) {
		
		Node newnode = (Node) Network.prototype.clone();
	
		DynamicInitializer init = (DynamicInitializer) inits[0];
	
		int nodePos = Network.size();
		
		init.setNodePosition(nodePos);
		init.setNodeType(4);
		init.setSpNodeType(4);
		init.initialize(newnode);
		
		Network.add(newnode);
		
		idp.addNodeDir(nodePos, 4, SPRank);
	}
}

protected void addMPSPs(int n)
{
	IDP_Node idp = (IDP_Node) Network.get(1).getProtocol(IDPPid);
	
	for (int i = 0; i < n; ++i) {
		
		Node newnode = (Node) Network.prototype.clone();

		DynamicInitializer init = (DynamicInitializer) inits[0];
	
		int nodePos = Network.size();
		
		init.setNodePosition(nodePos);
		init.setNodeType(4);
		init.setSpNodeType(1);
		init.initialize(newnode);
		
		Network.add(newnode);
		
		idp.addNodeDir(nodePos, 4, SPRank);
	}
}

protected void addMSPs(int n)
{
	IDP_Node idp = (IDP_Node) Network.get(1).getProtocol(IDPPid);
	
	for (int i = 0; i < n; ++i) {
		
		Node newnode = (Node) Network.prototype.clone();

		DynamicInitializer init = (DynamicInitializer) inits[0];
		
		int nodePos = Network.size();
	
		init.setNodePosition(Network.size());
		init.setNodeType(4);
		init.setSpNodeType(3);
		init.initialize(newnode);
	
		Network.add(newnode);
		
		idp.addNodeDir(nodePos, 4, SPRank);
	}
}

protected void addMEs(int n)
{
	for (int i = 0; i < n; ++i) {
		
		Node newnode = (Node) Network.prototype.clone();

		DynamicInitializer init = (DynamicInitializer) inits[0];
		
		init.setNodePosition(Network.size());
		init.initializeME(newnode);
	
		//Network.add(newnode);	
	}
}

protected List<Auditor_STNodesDir> addSTs(List<Auditor_STNodesDir> STtoAdd, double UserIniTrustinNetwork)
{
	Auditor_STNodesDir currentST;
	
	IDP_Node idp = (IDP_Node) Network.get(1).getProtocol(IDPPid);
	
	for (int i = 0; i < STtoAdd.size(); ++i) {
	
		currentST = STtoAdd.get(i);
		Node newnode = (Node) Network.prototype.clone();
		
		DynamicInitializer init = (DynamicInitializer) inits[0];
	
		int nodePos = Network.size();
		
		init.setNodePosition(nodePos);
		init.setNodeType(3);
		init.setAgentNodeType(1);
		if(TrustInNetworkDynamic)
			init.setUserIniTrustinNetwork(UserIniTrustinNetwork);
		init.setSTtargetedSPid(currentST.getSPID());
		init.initialize(newnode);
		
		
		currentST.setNodePos(Network.size());
		currentST.setCreatedOn((int) CommonState.getTime());
		
		Network.add(newnode);
		
		idp.addNodeDir(nodePos, 3, SPRank);
		
		//System.out.println("New ST added! nodePos: "+currentST.getNodePos()+" SPid: "+currentST.getSPID());
	}
	
	return STtoAdd;
}

protected List<Auditor_GTNodesDir> addGTs(List<Auditor_GTNodesDir> GTtoAdd, double UserIniTrustinNetwork)
{
	Auditor_GTNodesDir currentGT;
	
	IDP_Node idp = (IDP_Node) Network.get(1).getProtocol(IDPPid);
	
	for (int i = 0; i < GTtoAdd.size(); ++i) {
	
		currentGT = GTtoAdd.get(i);
		Node newnode = (Node) Network.prototype.clone();
		
		DynamicInitializer init = (DynamicInitializer) inits[0];
	
		int nodePos = Network.size();
		
		init.setNodePosition(nodePos);
		init.setNodeType(3);
		init.setAgentNodeType(2);
		if(TrustInNetworkDynamic)
			init.setUserIniTrustinNetwork(UserIniTrustinNetwork);
		init.setGT_TargetedSPs(currentGT.getTargetedSPs());
		init.setGTid(currentGT.getGTid());
		init.initialize(newnode);
		
		
		currentGT.setNodePos(Network.size());
		currentGT.setCreatedOn((int) CommonState.getTime());
		
		Network.add(newnode);
		
		idp.addNodeDir(nodePos, 3, SPRank);
		
		//System.out.println("New GT added! GTid: "+currentGT.getGTid()+" nodePos: "+currentGT.getNodePos()+" SPs: ");
		//for(GT_TargetedSP sp: currentGT.getTargetedSPs())
			//System.out.println("--sp: "+sp.getSPid());
		
	}
	
	return GTtoAdd;
}

protected void addMEnodes(List<ME_NodesToAdd> MEnodesToAdd)
{
	ME_NodesToAdd currentNode;
	
	for (int i = 0; i < MEnodesToAdd.size(); i++) {
	
		currentNode = MEnodesToAdd.get(i);
		
		DynamicInitializer init = (DynamicInitializer) inits[0];
		
		//System.out.println("currentNode.getMPSPs_NUM() == "+currentNode.getMPSPs_NUM()+" currentNode.getMSPs_NUM() == "+currentNode.getMSPs_NUM());
	
		if(currentNode.getMPSPs_NUM() > 0)
			init.initializeMEnodes(Network.size(), currentNode.getMEid(), true, currentNode.getMPSPs_NUM(), 0);
		
		if(currentNode.getMSPs_NUM() > 0)
			init.initializeMEnodes(Network.size(), currentNode.getMEid(), false, 0, currentNode.getMSPs_NUM());
		
	}
	
}


// --------------------------------------------------------------------------
// Initialization
// --------------------------------------------------------------------------

/**
 * Standard constructor that reads the configuration parameters.
 * Invoked by the simulation engine.
 * @param prefix the configuration prefix for this class
 */
public RMGR_DynamicNetworkGenerator(String prefix)
{
	super(prefix);
	SuperNodePid = Configuration.getPid(prefix + "." + PAR_Super_Node_PROT);
	AuditorPid = Configuration.getPid(prefix + "." + PAR_Auditor_Node_PROT);
	IDPPid = Configuration.getPid(prefix + "." + PAR_IDP_Node_PROT);
	//MEPid = Configuration.getPid(prefix + "." + PAR_ME_Node_PROT);
	
	SPRank = Configuration.getDouble(prefix + "." + PAR_SPRank);
	
	if(Configuration.getInt(prefix + "." + PAR_TrustInNetworkDynamic) == 0)
		TrustInNetworkDynamic =  false;
	else
		TrustInNetworkDynamic = true;
	
	
}

// --------------------------------------------------------------------------
// Public methods
// --------------------------------------------------------------------------

/**
 * Calls {@link #add(int)} or {@link #remove} with the parameters defined by the
 * configuration.
 * @return always false 
 */
public final boolean execute()
{
	
	Super_Node superNode = (Super_Node) Network.get(0).getProtocol(SuperNodePid);
	
	double usersGR = superNode.getUsersGR();
	double pspsGR = superNode.getPSPsGR();
	double spsGR = superNode.getSPsGR();
	double mpspsGR = superNode.getMPSPsGR();
	double mspsGR = superNode.getMSPsGR();
	double meGR = superNode.getMEGR();
	
	double usersCount = superNode.getUsersCount();
	double pspsCount = superNode.getPSPsCount();
	double spsCount = superNode.getSPsCount();
	double mpspsCount = superNode.getMPSPsCount();
	double mspsCount = superNode.getMSPsCount();
	double meCount = superNode.getMECount();
	
	double UnAddedUsersCount = superNode.getUnAddedUsersCount();
	double UnAddedPSPsCount = superNode.getUnAddedPSPsCount();
	double UnAddedSPsCount = superNode.getUnAddedSPsCount();
	double UnAddedMPSPsCount = superNode.getUnAddedMPSPsCount();
	double UnAddedMSPsCount = superNode.getUnAddedMSPsCount();
	double UnAddedMECount = superNode.getUnAddedMECount();
	
	
	//Adding Normal Users
	if(usersCount == 0) usersCount = 1;
	
	double addUsersTotal = ((usersGR/100) * usersCount) + UnAddedUsersCount;
	int addUsers = (int) addUsersTotal;
	
	superNode.setUnAddedUsersCount(addUsersTotal - addUsers);
	
	this.addUsers((int) addUsers, superNode.getAvgUser().getCurrentTrust());
	superNode.setUsersCount(superNode.getUsersCount()+(int)addUsers);
	
	//Adding PSPs
	if(pspsCount == 0) pspsCount = 1;
	
	double addPSPsTotal = ((pspsGR/100) * pspsCount) + UnAddedPSPsCount;
	int addPSPs = (int) addPSPsTotal;
	
	superNode.setUnAddedPSPsCount(addPSPsTotal - addPSPs);
	
	this.addPSPs((int) addPSPs);
	superNode.setPSPsCount(superNode.getPSPsCount()+(int)addPSPs);
	
	//Adding SPs
	if(spsCount == 0) spsCount = 1;

	double addSPsTotal = ((spsGR/100) * spsCount) + UnAddedSPsCount;
	int addSPs = (int) addSPsTotal;
	
	superNode.setUnAddedSPsCount(addSPsTotal - addSPs);

	this.addSPs((int) addSPs);
	superNode.setSPsCount(superNode.getSPsCount()+(int)addSPs);
	
	//Adding MPSPs
	if(mpspsCount == 0) mpspsCount = 1;

	double addMPSPsTotal = ((mpspsGR/100) * mpspsCount) + UnAddedMPSPsCount;
	int addMPSPs = (int) addMPSPsTotal;
	
	superNode.setUnAddedMPSPsCount(addMPSPsTotal - addMPSPs);
	
	this.addMPSPs((int) addMPSPs);
	superNode.setMPSPsCount(superNode.getMPSPsCount()+(int)addMPSPs);
		
	//Adding MSPs
	if(mspsCount == 0) mspsCount = 1;

	double addMSPsTotal = ((mspsGR/100) * mspsCount) + UnAddedMSPsCount;
	int addMSPs = (int) addMSPsTotal;
	
	superNode.setUnAddedMSPsCount(addMSPsTotal - addMSPs);	

	this.addMSPs((int) addMSPs);
	superNode.setMSPsCount(superNode.getMSPsCount()+(int)addMSPs);
	
	//Adding MEs
	if(meCount == 0) meCount = 1;

	double addMEsTotal = ((meGR/100) * meCount) + UnAddedMECount;
	int addMEs = (int) addMEsTotal;
	
	superNode.setUnAddedMECount(addMEsTotal - addMEs);	

	this.addMEs((int) addMEs);
	superNode.setMECount(superNode.getMECount()+(int)addMEs);
	
	//Adding ME Nodes
	if(superNode.getMENodesToAdd().size() > 0)
	{
		this.addMEnodes(superNode.getMENodesToAdd());
		superNode.resetMENodestoAdd();
	}
	
	//Adding STs
	if(superNode.getSTtoAdd().size() > 0)
	{
		Auditor_Node auditor = (Auditor_Node) Network.get(2).getProtocol(AuditorPid);
		auditor.recieveSTcreationUpdate(this.addSTs(superNode.getSTtoAdd(), superNode.getAvgAgentUser().getCurrentTrust()));
		superNode.resetSTtoAdd();
	}
	
	//Adding GTs
	if(superNode.getGTtoAdd().size() > 0)
	{
		Auditor_Node auditor = (Auditor_Node) Network.get(2).getProtocol(AuditorPid);
		auditor.recieveGTcreationUpdate(this.addGTs(superNode.getGTtoAdd(), superNode.getAvgAgentUser().getCurrentTrust()));
		superNode.resetGTtoAdd();
	}
	
	
	return false;
}

public int getExprimentTrial_k() {
	return exprimentTrial_k;
}

public void setExprimentTrial_k(int exprimentTrial_k) {
	RMGR_DynamicNetworkGenerator.exprimentTrial_k = exprimentTrial_k;
	
	DynamicInitializer init = (DynamicInitializer) inits[0];
	
	init.setExprimentTrial_k(exprimentTrial_k);
	
}

}
