package trust.simulator.rmgr.protocol.auditor;

import java.util.ArrayList;
import java.util.List;

import peersim.core.Network;

import trust.simulator.rmgr.protocol.idp.Log;
import trust.simulator.rmgr.protocol.sp.SP_Node;
import trust.simulator.rmgr.protocol.user.Credential;

public class GPD_Evaluator {
	
	// This class implements the GPD function. Pc(SPi)= i / {sum(i)from i =1 to 1 =n} 
	public List<GPD_GuiltRate> generateGuiltRates(CaseLogs caseLogs, int SPPid)
	{
		List<GPD_GuiltRate> finalRatesList = new ArrayList<GPD_GuiltRate>();
		
		double caseSize = caseLogs.getLogs().size();

		double den = 0;
		for(int i = 1 ; i <= caseSize; i++)
			den = den + i;

		boolean itemExist;
		double guiltProb;
		
		for(int i = 0; i < caseSize;i++)
		{
			itemExist = false;
					
			Log log = caseLogs.getLogs().get(i);
			
			GPD_GuiltRate guiltRate = new GPD_GuiltRate();
			
			guiltProb = ((i+1)/den)*100;
			
			guiltRate.setSPid(log.getSpPosition());
			guiltRate.setSpUsefulness(log.getSpUsefulness());
			guiltRate.setGuiltRate(guiltProb);
			
			for(GPD_GuiltRate oldGuiltRate: finalRatesList)
				if(guiltRate.getSPid() == (oldGuiltRate.getSPid()))
				{
					oldGuiltRate.setGuiltRate(guiltProb+oldGuiltRate.getGuiltRate());
					itemExist = true;
					break;
				}
			
			if(!itemExist)
			{
				SP_Node sp = (SP_Node) Network.get(guiltRate.getSPid()).getProtocol(SPPid);
				
				if(sp.isEnabledStrictDGU()) continue;
				
				else if(!sp.isInstalledDGU() || (sp.isInstalledDGU() && sp.getInstalledDGUCycle() < log.getCycle()))
					finalRatesList.add(guiltRate);
			}
		}
		
		return finalRatesList;
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		CaseLogs caseLogs = new CaseLogs(new Credential(1, 3, 2));
		
		List<Log> caseList = caseLogs.getLogs();
		caseList.add(new Log(1, 1, 1, 1, 80, false));
		caseList.add(new Log(1, 2, 1, 4, 70, false));
		caseList.add(new Log(1, 3, 1, 3, 20, false));
		caseList.add(new Log(1, 4, 1, 1, 35, false));
		
		GPD_Evaluator ranker = new GPD_Evaluator();
		
		SP_Node node = new SP_Node(null);
		
		List<GPD_GuiltRate> rankedList = ranker.generateGuiltRates(caseLogs, node.getSPPid());
		
		for(int i = 0; i < rankedList.size(); i++)
		{
			System.out.println(rankedList.get(i).getSPid()+" Rank: "+ (100 - rankedList.get(i).getGuiltRate()) );
		}


	}


}
