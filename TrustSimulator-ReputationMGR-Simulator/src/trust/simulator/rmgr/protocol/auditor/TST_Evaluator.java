package trust.simulator.rmgr.protocol.auditor;

import java.util.ArrayList;
import java.util.List;

import peersim.core.Network;

import trust.simulator.rmgr.protocol.idp.Log;
import trust.simulator.rmgr.protocol.sp.SP_Node;

public class TST_Evaluator {
	
	// {False} This class implements the TST evaluation function. TST(SPi)= 1 - ( (all i appearances) / { sum(i) from i =1 to 1 = n }) 
	// {True} This class implements the TST evaluation function. TST(SPi)= 1 - (1 / { sum(i) from i =1 to 1 =n}) 
	public List<Aux_TSTrateRecord> generateTSTRanks(CaseLogs caseLogs, int SPPid)
	{
		double caseSize = caseLogs.getLogs().size();
	
		List<Aux_TSTrateRecord> ratesList = new ArrayList<Aux_TSTrateRecord>((int)caseSize);
		
		for(Log log: caseLogs.getLogs())
		{
			Aux_TSTrateRecord record = new Aux_TSTrateRecord();
			record.setSPid(log.getSpPosition());
			record.setSpUsefulness(log.getSpUsefulness());
			
			SP_Node sp = (SP_Node) Network.get(record.getSPid()).getProtocol(SPPid);
			
			boolean recordFound = false;
			
			for(Aux_TSTrateRecord tempRecord: ratesList)
			{
				if(tempRecord.getSPid() == record.getSPid())
				{
					recordFound = true;
					tempRecord.setLatestApperanceCycle((int) log.getCycle());
					//tempRecord.setCountApperances(tempRecord.getCountApperances() + 1);
					break;
				}
			}
			
			if(!recordFound)
			{	
				record.setInstalledDGU(sp.isInstalledDGU());
				record.setInstalledDGUCycle(sp.getInstalledDGUCycle());
				record.setEnabledStrictDGU(sp.isEnabledStrictDGU());
				record.setEnabledStrictDGUCycle(sp.getEnabledStrictDGUCycle());
				
				record.setCountApperances(record.getCountApperances() + 1);
				ratesList.add(record);
			}
		}
					
		for(Aux_TSTrateRecord record: ratesList)
		{
			if(!record.isInstalledDGU() || (record.isInstalledDGU() && record.getInstalledDGUCycle() < record.getLatestApperanceCycle()) )
				record.setRank((1 - record.getCountApperances()/caseSize)*100);
			else
				record.setRank(100);
		}
		
		return ratesList;
	}


}
