package trust.simulator.rmgr.protocol.auditor;

import java.util.ArrayList;
import java.util.List;

public class SPRrecord{

	private int SPid = -1;
	private int Counter = 0;
	private boolean PSL = false;
	private boolean STtested = false;
	private boolean installedDGU = false;
	private int installedDGYCycle = -1;
	private boolean enabledStrictDGU = false;
	private int enabledStrictDGUCycle = -1;
		
	private boolean Banned = false;
	
	private double spUsefulness = -1;
	
	private List<Integer> pslGTagents = new ArrayList<Integer>();

	public SPRrecord copy()
	{
		SPRrecord copy = new SPRrecord();
		
		copy.setSPid(SPid);
		copy.setCounter(Counter);
		copy.setPSL(PSL);
		copy.setSTtested(STtested);
		copy.setBanned(Banned);
		copy.setSpUsefulness(spUsefulness);
		copy.setInstalledDGU(installedDGU);
		copy.setInstalledDGYCycle(installedDGYCycle);
		
		return copy;
	}

	public int getSPid() {
		return SPid;
	}
	public void setSPid(int SPid) {
		this.SPid = SPid;
	}

	public int getCounter() {
		return Counter;
	}

	public void setCounter(int counter) {
		Counter = counter;
	}

	public boolean isPSL() {
		return PSL;
	}

	public void setPSL(boolean pSL) {
		PSL = pSL;
	}

	public boolean isSTtested() {
		return STtested;
	}

	public void setSTtested(boolean sTtested) {
		STtested = sTtested;
	}

	public double getSpUsefulness() {
		return spUsefulness;
	}

	public void setSpUsefulness(double spUsefulness) {
		this.spUsefulness = spUsefulness;
	}

	public boolean isBanned() {
		return Banned;
	}

	public void setBanned(boolean banned) {
		Banned = banned;
	}

	public List<Integer> getPslGTagents() {
		return pslGTagents;
	}

	public void setPslGTagents(List<Integer> pslGTagents) {
		this.pslGTagents = pslGTagents;
	}

	public boolean isInstalledDGU() {
		return installedDGU;
	}

	public void setInstalledDGU(boolean installedDGU) {
		this.installedDGU = installedDGU;
	}

	public int getInstalledDGYCycle() {
		return installedDGYCycle;
	}

	public void setInstalledDGYCycle(int installedDGYCycle) {
		this.installedDGYCycle = installedDGYCycle;
	}

	public boolean isEnabledStrictDGU() {
		return enabledStrictDGU;
	}

	public void setEnabledStrictDGU(boolean enabledStrictDGU) {
		this.enabledStrictDGU = enabledStrictDGU;
	}

	public int getEnabledStrictDGUCycle() {
		return enabledStrictDGUCycle;
	}

	public void setEnabledStrictDGUCycle(int enabledStrictDGUCycle) {
		this.enabledStrictDGUCycle = enabledStrictDGUCycle;
	}
	
}