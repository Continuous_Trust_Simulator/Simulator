package trust.simulator.rmgr.protocol.auditor;

import java.util.ArrayList;
import java.util.List;


public class Auditor_UsersNodesDir {
	
	private int position;
	
	//TODO optimize the size of this array
	private List<CaseLogs> CaseLogs = new ArrayList<CaseLogs>(250);
	
	//TODO optimize the size of this array
	private List<TLRuRank> TLRu = new ArrayList<TLRuRank>(500);
	
//	private int countGPD_Evaluated = 0;

	public Auditor_UsersNodesDir() {
		super();
		
		position = -1;

	}


	public int getPosition() {
		return position;
	}


	public void setPosition(int position) {
		this.position = position;
	}


	public List<CaseLogs> getCaseLogs() {
		return CaseLogs;
	}


	public void setCaseLogs(List<CaseLogs> caseLogs) {
		CaseLogs = caseLogs;
	}


	public List<TLRuRank> getTLRu() {
		return TLRu;
	}


	public void setTLRu(List<TLRuRank> tLRu) {
		TLRu = tLRu;
	}


/*	public int getCountGPD_Evaluated() {
		return countGPD_Evaluated;
	}


	public void setCountGPD_Evaluated(int countGPD_Evaluated) {
		this.countGPD_Evaluated = countGPD_Evaluated;
	}
*/
	
}
