package trust.simulator.rmgr.protocol.auditor;

import java.util.ArrayList;
import java.util.List;

import trust.simulator.rmgr.protocol.user.GT_TargetedSP;

public class Auditor_GTNodesDir {
	
	private int GTid = -1;
	private List<GT_TargetedSP> targetedSPs = new ArrayList<GT_TargetedSP>();
	private int nodePos;
	private int createdOn;
	// 0 = not assigned, 1 = waiting assignment, 2 = assigned, 3 = killed
	private int GTstatus = 0;
	
	// 1 = suspiciousNodes_GT, 2 = PSL_GT, 3 = PIIL_GT
	private int GTtype = -1;

/*	public boolean equalTargetedSPs(List<GT_TargetedSP> otherTargetedSPs)
	{
		if(targetedSPs.size() != otherTargetedSPs.size())
			return false;
		
		for(int i = 0; i < targetedSPs.size(); i++)
		{
			if(targetedSPs.get(i) != otherTargetedSPs.get(i))
				return false;
		}	
		
		return true;
	}
*/	
	public int getNodePos() {
		return nodePos;
	}

	public void setNodePos(int nodePos) {
		this.nodePos = nodePos;
	}

	public int getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(int createdOn) {
		this.createdOn = createdOn;
	}

	public int getGTstatus() {
		return GTstatus;
	}

	public void setGTstatus(int GTstatus) {
		this.GTstatus = GTstatus;
	}

	public List<GT_TargetedSP> getTargetedSPs() {
		return targetedSPs;
	}

	public void setTargetedSPs(List<GT_TargetedSP> targetedSPs) {
		this.targetedSPs = targetedSPs;
	}

	public int getGTtype() {
		return GTtype;
	}

	public void setGTtype(int gTtype) {
		GTtype = gTtype;
	}

	public int getGTid() {
		return GTid;
	}

	public void setGTid(int gTid) {
		GTid = gTid;
	}

}
