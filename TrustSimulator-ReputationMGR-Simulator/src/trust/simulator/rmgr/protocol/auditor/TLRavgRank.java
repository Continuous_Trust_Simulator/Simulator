package trust.simulator.rmgr.protocol.auditor;

public class TLRavgRank{

	private int SPid;
	private double Rank;
	
	//Aux holder of sum of this SP TLRu values before averaging
	private double TLRuSum;
	
	//Aux holder of count of cases where this SP appeared
	private double TLRUCount;
	
	private double spUsefulness = -1;
	
	private boolean installedDGU = false;
	private int installedDGYCycle = -1;
	
	private boolean enabledStrictDGU = false;
	private int enabledStrictDGUCycle = -1;
	
	
	public TLRavgRank copy()
	{
		TLRavgRank copy = new TLRavgRank();
		
		copy.setRank(Rank);
		copy.setSPid(SPid);
		copy.setTLRUCount(TLRUCount);
		copy.setTLRuSum(TLRuSum);
		copy.setSpUsefulness(spUsefulness);
		copy.setInstalledDGU(installedDGU);
		copy.setInstalledDGYCycle(installedDGYCycle);
		copy.setEnabledStrictDGU(enabledStrictDGU);
		copy.setEnabledStrictDGUCycle(enabledStrictDGUCycle);
		
		return copy;
	}

	public int getSPid() {
		return SPid;
	}
	public void setSPid(int SPid) {
		this.SPid = SPid;
	}

	public double getRank() {
		return Rank;
	}
	public void setRank(double Rank) {
		this.Rank = Rank;
	}
	public double getTLRuSum() {
		return TLRuSum;
	}
	public void setTLRuSum(double tLRuSum) {
		TLRuSum = tLRuSum;
	}
	public double getTLRUCount() {
		return TLRUCount;
	}
	public void setTLRUCount(double tLRUCount) {
		TLRUCount = tLRUCount;
	}

	public double getSpUsefulness() {
		return spUsefulness;
	}

	public void setSpUsefulness(double spUsefulness) {
		this.spUsefulness = spUsefulness;
	}

	public boolean isEnabledStrictDGU() {
		return enabledStrictDGU;
	}

	public void setEnabledStrictDGU(boolean enabledStrictDGU) {
		this.enabledStrictDGU = enabledStrictDGU;
	}

	public boolean isInstalledDGU() {
		return installedDGU;
	}

	public void setInstalledDGU(boolean installedDGU) {
		this.installedDGU = installedDGU;
	}

	public int getInstalledDGYCycle() {
		return installedDGYCycle;
	}

	public void setInstalledDGYCycle(int installedDGYCycle) {
		this.installedDGYCycle = installedDGYCycle;
	}

	public int getEnabledStrictDGUCycle() {
		return enabledStrictDGUCycle;
	}

	public void setEnabledStrictDGUCycle(int enabledStrictDGUCycle) {
		this.enabledStrictDGUCycle = enabledStrictDGUCycle;
	}

}