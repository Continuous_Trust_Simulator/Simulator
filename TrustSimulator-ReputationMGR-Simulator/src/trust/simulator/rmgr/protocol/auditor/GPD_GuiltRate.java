package trust.simulator.rmgr.protocol.auditor;

public class GPD_GuiltRate{

	private int SPid;
	private double GuiltRate;
	private double spUsefulness;

	public int getSPid() {
		return SPid;
	}
	public void setSPid(int SPid) {
		this.SPid = SPid;
	}

	public double getGuiltRate() {
		return GuiltRate;
	}
	public void setGuiltRate(double GuiltRate) {
		this.GuiltRate = GuiltRate;
	}
	public double getSpUsefulness() {
		return spUsefulness;
	}
	public void setSpUsefulness(double spUsefulness) {
		this.spUsefulness = spUsefulness;
	}

}