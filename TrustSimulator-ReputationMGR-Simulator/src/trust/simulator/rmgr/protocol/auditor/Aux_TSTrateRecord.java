package trust.simulator.rmgr.protocol.auditor;

public class Aux_TSTrateRecord
{
	private int SPid = -1;
	private double countApperances = 0;
	private double rank = -1;
	private double spUsefulness;
	
	private boolean installedDGU = false;
	private int installedDGUCycle = -1;
	private boolean enabledStrictDGU = false;
	private int enabledStrictDGUCycle = -1;
	
	private int latestApperanceCycle = -1;

	public int getSPid() {
		return SPid;
	}
	public void setSPid(int sPid) {
		SPid = sPid;
	}
	public double getCountApperances() {
		return countApperances;
	}
	public void setCountApperances(double countApperances) {
		this.countApperances = countApperances;
	}
	public double getRank() {
		return rank;
	}
	public void setRank(double rank) {
		this.rank = rank;
	}
	public double getSpUsefulness() {
		return spUsefulness;
	}
	public void setSpUsefulness(double spUsefulness) {
		this.spUsefulness = spUsefulness;
	}
	public boolean isInstalledDGU() {
		return installedDGU;
	}
	public void setInstalledDGU(boolean installedDGU) {
		this.installedDGU = installedDGU;
	}
	public int getInstalledDGUCycle() {
		return installedDGUCycle;
	}
	public void setInstalledDGUCycle(int installedDGUCycle) {
		this.installedDGUCycle = installedDGUCycle;
	}
	public boolean isEnabledStrictDGU() {
		return enabledStrictDGU;
	}
	public void setEnabledStrictDGU(boolean enabledStrictDGU) {
		this.enabledStrictDGU = enabledStrictDGU;
	}
	public int getEnabledStrictDGUCycle() {
		return enabledStrictDGUCycle;
	}
	public void setEnabledStrictDGUCycle(int enabledStrictDGUCycle) {
		this.enabledStrictDGUCycle = enabledStrictDGUCycle;
	}
	public int getLatestApperanceCycle() {
		return latestApperanceCycle;
	}
	public void setLatestApperanceCycle(int latestApperanceCycle) {
		this.latestApperanceCycle = latestApperanceCycle;
	}
}