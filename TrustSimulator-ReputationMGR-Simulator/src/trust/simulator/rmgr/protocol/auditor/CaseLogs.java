package trust.simulator.rmgr.protocol.auditor;

import java.util.ArrayList;
import java.util.List;

import trust.simulator.rmgr.protocol.idp.Log;
import trust.simulator.rmgr.protocol.user.Credential;

//import peersim.core.CommonState;

public class CaseLogs {
	
	private int userPos;
	
	private Credential credential;
	
	private List<Log> logs;

	private boolean GPD_Evaluated = false;
	
	private boolean closed = false;
	
	private long createdOn;
	
	private long periodToCloseCase;
	
	private List<Auditor_SPsNodesDir> caseSps;
	private List<Auditor_SPsNodesDir> caseGuiltySps;
	
	//TODO optimize the size of the arrays
	public CaseLogs()
	{
		this.setCredential(new Credential());
		this.setLogs(new ArrayList<Log> (20));
		this.setCaseSps(new ArrayList<Auditor_SPsNodesDir>(20));
		this.setCaseGuiltySps(new ArrayList<Auditor_SPsNodesDir>(20));
	}
	
	public CaseLogs(Credential credential)
	{
		this.setCredential(credential);
		this.userPos = credential.getNodePosition();
		this.setLogs(new ArrayList<Log> (20));
		this.setCaseSps(new ArrayList<Auditor_SPsNodesDir>(20));
		this.setCaseGuiltySps(new ArrayList<Auditor_SPsNodesDir>(20));
	}
	
	public boolean equalCaseGuiltyLists(List<Auditor_SPsNodesDir> list1, List<Auditor_SPsNodesDir> list2)
	{
		if(list1.size() != list2.size()) return false;
		
		int sp1;
		int sp2;
		boolean spFound;
		for(int i = 0; i < list1.size(); i++)
		{
			sp1 = list1.get(i).getPosition();
			spFound = false;
			for(int j = 0; j < list2.size(); j++)
			{
				sp2 = list2.get(j).getPosition();
				if(sp1 == sp2)
				{
					spFound = true;
					break;
				}
			}
			if(!spFound) return false;
		}
		
		return true;
	}

	public Credential getCredential() {
		return credential;
	}

	public void setCredential(Credential credential) {
		this.credential = credential;
	}
	
/*	public void addLog(int spPosition)
	{
		logs.add(new Log(credential.getNodePosition(), CommonState.getTime(), credential.getId(), spPosition));
	}
*/
	public int getUserPos() {
		return userPos;
	}

	public void setUserPos(int userPos) {
		this.userPos = userPos;
	}

	public List<Log> getLogs() {
		return logs;
	}

	public void setLogs(List<Log> logs) {
		this.logs = logs;
	}

	public boolean isGPD_Evaluated() {
		return GPD_Evaluated;
	}

	public void setGPD_Evaluated(boolean gPD_Evaluated) {
		GPD_Evaluated = gPD_Evaluated;
	}

	public boolean isClosed() {
		return closed;
	}

	public void setClosed(boolean closed) {
		this.closed = closed;
	}

	public List<Auditor_SPsNodesDir> getCaseSps() {
		return caseSps;
	}

	public void setCaseSps(List<Auditor_SPsNodesDir> caseSps) {
		this.caseSps = caseSps;
	}

	public List<Auditor_SPsNodesDir> getCaseGuiltySps() {
		return caseGuiltySps;
	}
	
	public List<Auditor_SPsNodesDir> cloneCaseGuiltySps() {
		
		List<Auditor_SPsNodesDir> clonedList = new ArrayList<Auditor_SPsNodesDir>(caseGuiltySps.size());
		
		for(Auditor_SPsNodesDir item : caseGuiltySps) clonedList.add(item);
		
		return clonedList;
	}

	public void setCaseGuiltySps(List<Auditor_SPsNodesDir> caseGuiltySps) {
		this.caseGuiltySps = caseGuiltySps;
	}

	public long getPeriodToCloseCase() {
		return periodToCloseCase;
	}

	public void setPeriodToCloseCase(long periodToCloseCase) {
		this.periodToCloseCase = periodToCloseCase;
	}

	public long getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(long createdOn) {
		this.createdOn = createdOn;
	}
		
}
