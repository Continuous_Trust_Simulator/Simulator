package trust.simulator.rmgr.protocol.auditor;

import java.util.ArrayList;
import java.util.List;

public class IILrecord{

	private int UserID = -1;
	private int CredentialID = -1;
	private int createdON = -1;
	private boolean PIILevaluated = false;
	private boolean stTested = false;
	private boolean ignore = false;
	
	//TODO optimize the size of this array
	private List<Integer> SPs = new ArrayList<Integer>();

	public IILrecord copy()
	{
		IILrecord copy = new IILrecord();
		
		copy.setUserID(UserID);
		copy.setCredentialID(CredentialID);
		copy.setPIILevaluated(PIILevaluated);
		copy.setSPs(SPs);
		copy.setIgnore(ignore);
		
		return copy;
	}

	public int getUserID() {
		return UserID;
	}
	public void setUserID(int UserID) {
		this.UserID = UserID;
	}

	public boolean isPIILevaluated() {
		return PIILevaluated;
	}

	public void setPIILevaluated(boolean PIILevaluated) {
		this.PIILevaluated = PIILevaluated;
	}

	public List<Integer> getSPs() {
		return SPs;
	}

	public void setSPs(List<Integer> sPs) {
		SPs = sPs;
	}

	public int getCredentialID() {
		return CredentialID;
	}

	public void setCredentialID(int credentialID) {
		CredentialID = credentialID;
	}

	public boolean isStTested() {
		return stTested;
	}

	public void setStTested(boolean stTested) {
		this.stTested = stTested;
	}

	public int getCreatedON() {
		return createdON;
	}

	public void setCreatedON(int createdON) {
		this.createdON = createdON;
	}

	public boolean isIgnore() {
		return ignore;
	}

	public void setIgnore(boolean ignore) {
		this.ignore = ignore;
	}


	
}