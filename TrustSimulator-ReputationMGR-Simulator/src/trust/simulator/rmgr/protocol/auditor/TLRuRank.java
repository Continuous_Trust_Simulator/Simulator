package trust.simulator.rmgr.protocol.auditor;

public class TLRuRank{

	private int SPid;
	private double Rank;
	
	//Aux holder of sum of this SP GuiltRates before averaging
	private double guiltRateSum;
	
	//Aux holder of count of cases where this SP appeared
	private double casesCount;
	
	private double spUsefulness = -1;
	

	public int getSPid() {
		return SPid;
	}
	public void setSPid(int SPid) {
		this.SPid = SPid;
	}

	public double getRank() {
		return Rank;
	}
	public void setRank(double Rank) {
		this.Rank = Rank;
	}
	public double getGuiltRateSum() {
		return guiltRateSum;
	}
	public void setGuiltRateSum(double guiltRateSum) {
		this.guiltRateSum = guiltRateSum;
	}
	public double getCasesCount() {
		return casesCount;
	}
	public void setCasesCount(double casesCount) {
		this.casesCount = casesCount;
	}
	public double getSpUsefulness() {
		return spUsefulness;
	}
	public void setSpUsefulness(double spUsefulness) {
		this.spUsefulness = spUsefulness;
	}

}