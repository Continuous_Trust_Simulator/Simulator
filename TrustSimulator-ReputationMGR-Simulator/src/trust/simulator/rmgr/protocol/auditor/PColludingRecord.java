package trust.simulator.rmgr.protocol.auditor;

import java.util.ArrayList;
import java.util.List;

public class PColludingRecord{

	private int Counter = 0;
	
	//TODO optimize the size of this array
	private List<Integer> SPs = new ArrayList<Integer>();
	
	private boolean ignore = false;

	public PColludingRecord copy()
	{
		PColludingRecord copy = new PColludingRecord();
		
		copy.setCounter(Counter);
		copy.setSPs(SPs);
		copy.setIgnore(ignore);
		
		return copy;
	}

	public boolean containSP(int SPid)
	{
		for(Integer sp: SPs)
			if(sp == SPid) return true;
		
		return false;
	}
	
	public void incrementCounter()
	{
		Counter++;
	}
	
	public int getCounter() {
		return Counter;
	}
	public void setCounter(int Counter) {
		this.Counter = Counter;
	}

	public List<Integer> getSPs() {
		return SPs;
	}

	public void setSPs(List<Integer> sPs) {
		SPs = sPs;
	}

	public boolean isIgnore() {
		return ignore;
	}

	public void setIgnore(boolean ignore) {
		this.ignore = ignore;
	}
	
}