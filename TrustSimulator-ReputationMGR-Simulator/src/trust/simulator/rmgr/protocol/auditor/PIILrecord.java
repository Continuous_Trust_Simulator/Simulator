package trust.simulator.rmgr.protocol.auditor;

import java.util.ArrayList;
import java.util.List;

public class PIILrecord{

	private int Counter = 0;
	
	//TODO optimize the size of this array
	private List<Integer> SPs = new ArrayList<Integer>();
	private boolean PIILs = false;
	private List<Integer> piilGTagents = new ArrayList<Integer>();
	//private boolean containDGUSP = false;

	public PIILrecord copy()
	{
		PIILrecord copy = new PIILrecord();
		
		copy.setCounter(Counter);
		copy.setSPs(SPs);
		copy.setPiilGTagents(piilGTagents);
		copy.setPIILs(PIILs);
		//copy.setContainDGUSP(containDGUSP);
		
		return copy;
	}

	public void incrementCounter()
	{
		Counter++;
	}
	
	public int getCounter() {
		return Counter;
	}
	public void setCounter(int Counter) {
		this.Counter = Counter;
	}

	public List<Integer> getSPs() {
		return SPs;
	}

	public void setSPs(List<Integer> sPs) {
		SPs = sPs;
	}

	public boolean isPIILs() {
		return PIILs;
	}

	public void setPIILs(boolean pIILs) {
		PIILs = pIILs;
	}

	public List<Integer> getPiilGTagents() {
		return piilGTagents;
	}

	public void setPiilGTagents(List<Integer> piilGTagents) {
		this.piilGTagents = piilGTagents;
	}
/*
	public boolean isContainDGUSP() {
		return containDGUSP;
	}

	public void setContainDGUSP(boolean containDGUSP) {
		this.containDGUSP = containDGUSP;
	}

*/	
}