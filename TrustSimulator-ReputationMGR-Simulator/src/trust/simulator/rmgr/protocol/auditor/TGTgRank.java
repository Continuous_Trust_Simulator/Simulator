package trust.simulator.rmgr.protocol.auditor;

import java.util.ArrayList;
import java.util.List;

public class TGTgRank{

	private List<Integer> SPs = new ArrayList<Integer>();
	
	private double TGTgRank;
	
	private boolean ignore = false;
	
	private boolean minRankReached = false;
	
	private int Counter = -100000;

	
	public TGTgRank copy()
	{
		TGTgRank copy = new TGTgRank();

		copy.setSPs(SPs);
		copy.setTGTgRank(TGTgRank);
		copy.setIgnore(ignore);
		copy.setMinRankReached(minRankReached);
		copy.setCounter(Counter);
		
		return copy;
	}

	public void incrementCounter()
	{
		if(Counter == -100000) Counter = 0;
		Counter++;
		if(Counter > 0)
		{
			minRankReached = true;
			TGTgRank = (1-(2/(double)SPs.size()) ) * 100;
			//System.out.println("TGTgRank: "+TGTgRank);
		}
	}
	
	public void decrementCounter()
	{
		if(Counter == -100000) Counter = 0;
		Counter--;
		if(Counter <= 0)
		{
			minRankReached = false;
			TGTgRank = 100;
		}
	}
	
	public boolean containSP(int SPid)
	{
		for(Integer sp: SPs)
			if(sp == SPid) return true;
		
		return false;
	}
	
	public void removeSP(int SPid)
	{
		for(int i = 0; i < SPs.size(); i++) 
		{
			int sp = SPs.get(i);
			
			if(sp == SPid)
			{
				SPs.remove(i);
				return;
			}
		}		
	}

	public List<Integer> getSPs() {
		return SPs;
	}

	public void setSPs(List<Integer> sPs) {
		SPs = sPs;
	}


	public double getTGTgRank() {
		return TGTgRank;
	}


	public void setTGTgRank(double tGTgRank) {
		TGTgRank = tGTgRank;
	}

	public boolean isIgnore() {
		return ignore;
	}

	public void setIgnore(boolean ignore) {
		this.ignore = ignore;
	}

	public boolean isMinRankReached() {
		return minRankReached;
	}

	public void setMinRankReached(boolean minRankReached) {
		this.minRankReached = minRankReached;
	}

	public int getCounter() {
		if(Counter == -100000) Counter = 0;
		return Counter;
	}

	public void setCounter(int counter) {
		this.Counter = counter;
		if(Counter == 0) setTGTgRank(-1);
	}

}