package trust.simulator.rmgr.protocol.auditor;

import java.util.ArrayList;
import java.util.List;

public class Auditor_SPsNodesDir {
	
	private int position;
	
	private double rank;
	
	private boolean malicious = false;
	
	private boolean banned = false;
	
	private int installDGUdetectorType = -1;
	
	private int enableStrictDGUdetectorType = -1;
	
	private int BanDetectorType = -1;
	
	private boolean installedDGU = false;
	private int installedDGUCycle = -1;
	
	private boolean enabledStrictDGU = false;
	private int enabledStrictDGUCycle = -1;
	
	private boolean stTested = false;
	
	private List<Integer> suspiciousGTagents = new ArrayList<Integer>();
	
	private double spUsefulness;
	
	private double TSTrank = -1;
	private double TGTrank = -1;
	private double TGTColludingRank = -1;
	private double TGTWeakColludingRank = -1;

	public Auditor_SPsNodesDir() {

	}


	public int getPosition() {
		return position;
	}


	public void setPosition(int position) {
		this.position = position;
	}


	public double getRank() {
		return rank;
	}


	public void setRank(double rank) {
		this.rank = rank;
	}

	public boolean isMalicious() {
		return malicious;
	}


	public void setMalicious(boolean malicious) {
		this.malicious = malicious;
	}


	public boolean isBanned() {
		return banned;
	}


	public void setBanned(boolean banned) {
		this.banned = banned;
	}


	public boolean isStTested() {
		return stTested;
	}


	public void setStTested(boolean stTested) {
		this.stTested = stTested;
	}

	public double getSpUsefulness() {
		return spUsefulness;
	}


	public void setSpUsefulness(double spUsefulness) {
		this.spUsefulness = spUsefulness;
	}


	public double getTSTrank() {
		return TSTrank;
	}


	public void setTSTrank(double TSTrank) {
		this.TSTrank = TSTrank;
	}


	public double getTGTrank() {
		return TGTrank;
	}


	public void setTGTrank(double TGTrank) {
		this.TGTrank = TGTrank;
	}

	public List<Integer> getSuspiciousGTagents() {
		return suspiciousGTagents;
	}


	public void setSuspiciousGTagents(List<Integer> suspiciousGTagents) {
		this.suspiciousGTagents = suspiciousGTagents;
	}


	public double getTGTColludingRank() {
		return TGTColludingRank;
	}


	public void setTGTColludingRank(double tGTColludingRank) {
		TGTColludingRank = tGTColludingRank;
	}


	public double getTGTWeakColludingRank() {
		return TGTWeakColludingRank;
	}


	public void setTGTWeakColludingRank(double tGTWeakColludingRank) {
		TGTWeakColludingRank = tGTWeakColludingRank;
	}


	public boolean isInstalledDGU() {
		return installedDGU;
	}


	public void setInstalledDGU(boolean installedDGU) {
		this.installedDGU = installedDGU;
	}


	public int getInstalledDGUCycle() {
		return installedDGUCycle;
	}


	public void setInstalledDGUCycle(int installedDGUCycle) {
		this.installedDGUCycle = installedDGUCycle;
	}


	public boolean isEnabledStrictDGU() {
		return enabledStrictDGU;
	}


	public void setEnabledStrictDGU(boolean enabledStrictDGU) {
		this.enabledStrictDGU = enabledStrictDGU;
	}


	public int getEnabledStrictDGUCycle() {
		return enabledStrictDGUCycle;
	}


	public void setEnabledStrictDGUCycle(int enabledStrictDGUCycle) {
		this.enabledStrictDGUCycle = enabledStrictDGUCycle;
	}

	public int getInstallDGUdetectorType() {
		return installDGUdetectorType;
	}


	public void setInstallDGUdetectorType(int installDGUdetectorType) {
		this.installDGUdetectorType = installDGUdetectorType;
	}


	public int getEnableStrictDGUdetectorType() {
		return enableStrictDGUdetectorType;
	}


	public void setEnableStrictDGUdetectorType(int enableStrictDGUdetectorType) {
		this.enableStrictDGUdetectorType = enableStrictDGUdetectorType;
	}


	public int getBanDetectorType() {
		return BanDetectorType;
	}


	public void setBanDetectorType(int banDetectorType) {
		BanDetectorType = banDetectorType;
	}

}
