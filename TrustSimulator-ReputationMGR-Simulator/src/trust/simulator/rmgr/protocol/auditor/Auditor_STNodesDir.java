package trust.simulator.rmgr.protocol.auditor;

public class Auditor_STNodesDir {
	
	private int spID;
	private int nodePos;
	private int createdOn;
	// 0 = not assigned, 1 = waiting assignment, 2 = assigned, 3 = killed
	private int STstatus = 0;
	
	private boolean DGUSPtargeted = false;
	

	public int getSPID() {
		return spID;
	}

	public void setSPID(int spID) {
		this.spID = spID;
	}

	public int getNodePos() {
		return nodePos;
	}

	public void setNodePos(int nodePos) {
		this.nodePos = nodePos;
	}

	public int getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(int createdOn) {
		this.createdOn = createdOn;
	}

	public int getSTstatus() {
		return STstatus;
	}

	public void setSTstatus(int sTstatus) {
		STstatus = sTstatus;
	}

	public boolean isDGUSPtargeted() {
		return DGUSPtargeted;
	}

	public void setDGUSPtargeted(boolean dGUSPtargeted) {
		DGUSPtargeted = dGUSPtargeted;
	}

}
