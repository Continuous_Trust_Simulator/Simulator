package trust.simulator.rmgr.protocol.auditor;

public class CaseSPs {
	
	private int SPid;
	
	private int cycle;

	public CaseSPs(int SPid, int cycle)
	{
		this.SPid = SPid;
		this.cycle = cycle;
	}
	
	public int getSPid() {
		return SPid;
	}

	public void setSPid(int sPid) {
		SPid = sPid;
	}

	public int getCycle() {
		return cycle;
	}

	public void setCycle(int cycle) {
		this.cycle = cycle;
	}

}
