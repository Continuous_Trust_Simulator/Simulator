package trust.simulator.rmgr.protocol.auditor;

public class TGRRank{

	private int SPid;
	private double Rank;
	
	private boolean installedDGU = false;
	private int installedDGYCycle = -1;
	
	private boolean enabledStrictDGU = false;
	private int enabledStrictDGUCycle = -1;
	
	private int installDGUdetectorType = -1;
	
	private int enableStrictDGUdetectorType = -1;
	
	private int BanDetectorType = -1;
	
	private double TSTRank = -1;
	private double TGTRank = -1;
	private double TGTColludingRank = -1;
	//Based on TGTg_allCredential Records
	private double TGTWeakColludingRank = -1;
	private double TLRavgRank = -1;

	
	private boolean Banned = false;
	
	private double spUsefulness = -1;

	// 0 = not assigned, 1 = waiting assignment, 2 = assigned.
	//private int STstatus = 0;
	//ST position in the STDir
	private int STLocator = -1;
	
	public TGRRank copy()
	{
		TGRRank copy = new TGRRank();

		copy.setSPid(SPid);
		copy.setRank(Rank);
		copy.setTSTRank(TSTRank);
		copy.setTGTRank(TGTRank);
		copy.setTLRavgRank(TLRavgRank);
		copy.setBanned(Banned);
		copy.setSpUsefulness(spUsefulness);
		copy.setSTLocator(STLocator);
		copy.setTGTWeakColludingRank(TGTWeakColludingRank);
		copy.setTGTColludingRank(TGTColludingRank);
		copy.setInstalledDGU(installedDGU);
		copy.setEnabledStrictDGU(enabledStrictDGU);
		copy.setEnabledStrictDGUCycle(enabledStrictDGUCycle);
		copy.setInstalledDGYCycle(installedDGYCycle);

		
		return copy;
	}

	public int getSPid() {
		return SPid;
	}
	public void setSPid(int SPid) {
		this.SPid = SPid;
	}

	public double getRank() {
		return Rank;
	}
	public void setRank(double Rank) {
		this.Rank = Rank;
	}

	public double getTSTRank() {
		return TSTRank;
	}
	public void setTSTRank(double tSTRank) {
		TSTRank = tSTRank;
	}
	public double getTGTRank() {
		return TGTRank;
	}
	public void setTGTRank(double tGTRank) {
		TGTRank = tGTRank;
	}
	public double getTLRavgRank() {
		return TLRavgRank;
	}
	public void setTLRavgRank(double tLRavgRank) {
		TLRavgRank = tLRavgRank;
	}

	public int getSTLocator() {
		return STLocator;
	}

	public void setSTLocator(int STLocator) {
		this.STLocator = STLocator;
	}

	public double getSpUsefulness() {
		return spUsefulness;
	}

	public void setSpUsefulness(double spUsefulness) {
		this.spUsefulness = spUsefulness;
	}

	public boolean isBanned() {
		return Banned;
	}

	public void setBanned(boolean banned) {
		Banned = banned;
	}

	public double getTGTColludingRank() {
		return TGTColludingRank;
	}

	public void setTGTColludingRank(double tGTColludingRank) {
		TGTColludingRank = tGTColludingRank;
	}
	
	public double getTGTWeakColludingRank() {
		return TGTWeakColludingRank;
	}

	public void setTGTWeakColludingRank(double tGTWeakColludingRank) {
		TGTWeakColludingRank = tGTWeakColludingRank;
	}

	public boolean isInstalledDGU() {
		return installedDGU;
	}

	public void setInstalledDGU(boolean installedDGU) {
		this.installedDGU = installedDGU;
	}

	public int getInstalledDGYCycle() {
		return installedDGYCycle;
	}

	public void setInstalledDGYCycle(int installedDGYCycle) {
		this.installedDGYCycle = installedDGYCycle;
	}

	public boolean isEnabledStrictDGU() {
		return enabledStrictDGU;
	}

	public void setEnabledStrictDGU(boolean enabledStrictDGU) {
		this.enabledStrictDGU = enabledStrictDGU;
	}

	public int getEnabledStrictDGUCycle() {
		return enabledStrictDGUCycle;
	}

	public void setEnabledStrictDGUCycle(int enabledStrictDGUCycle) {
		this.enabledStrictDGUCycle = enabledStrictDGUCycle;
	}

	public int getInstallDGUdetectorType() {
		return installDGUdetectorType;
	}

	public void setInstallDGUdetectorType(int installDGUdetectorType) {
		this.installDGUdetectorType = installDGUdetectorType;
	}

	public int getEnableStrictDGUdetectorType() {
		return enableStrictDGUdetectorType;
	}

	public void setEnableStrictDGUdetectorType(int enableStrictDGUdetectorType) {
		this.enableStrictDGUdetectorType = enableStrictDGUdetectorType;
	}

	public int getBanDetectorType() {
		return BanDetectorType;
	}

	public void setBanDetectorType(int banDetectorType) {
		BanDetectorType = banDetectorType;
	}

/*	public int getSTstatus() {
		return STstatus;
	}

	public void setSTstatus(int sTstatus) {
		STstatus = sTstatus;
	}
	*/
}