package trust.simulator.rmgr.protocol.auditor;

import java.util.ArrayList;
import java.util.List;

public class TSTRank{

	private int SPid;
	private double Rank;
	
	//Holder of all ST ranks this SP got 
	private List<Double> tstRanks = new ArrayList<Double>();
	
	private double spUsefulness = -1;
	
	public double updateRank()
	{
		double sumRanks = 0;
		double countRecords = 0;
		
		for(double rank: tstRanks)
		{
			sumRanks = sumRanks + rank;
			countRecords++;
		}
		
		Rank = sumRanks/countRecords;

		return Rank;
	}
	
	public void addTstRank(double rank)
	{
		tstRanks.add(rank);
		updateRank();
	}

	public int getSPid() {
		return SPid;
	}
	public void setSPid(int SPid) {
		this.SPid = SPid;
	}

	public double getRank() {
		return Rank;
	}
	public void setRank(double Rank) {
		this.Rank = Rank;
	}
	public double getSpUsefulness() {
		return spUsefulness;
	}
	public void setSpUsefulness(double spUsefulness) {
		this.spUsefulness = spUsefulness;
	}
	public List<Double> getTstRanks() {
		return tstRanks;
	}
	public void setTstRanks(List<Double> tstRanks) {
		this.tstRanks = tstRanks;
	}

}