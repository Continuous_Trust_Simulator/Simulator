package trust.simulator.rmgr.protocol.auditor;

import java.util.ArrayList;
import java.util.List;

import peersim.core.CommonState;
import peersim.core.Network;
import trust.simulator.rmgr.protocol.RMGR_Node;
import trust.simulator.rmgr.protocol.idp.IDP_Node;
import trust.simulator.rmgr.protocol.idp.Log;
import trust.simulator.rmgr.protocol.idp.SpamReport;
import trust.simulator.rmgr.protocol.me.ME_Node;
import trust.simulator.rmgr.protocol.sp.SP_Node;
import trust.simulator.rmgr.protocol.superNode.Super_Node;
import trust.simulator.rmgr.protocol.user.Credential;
import trust.simulator.rmgr.protocol.user.GT_TargetedSP;
import trust.simulator.rmgr.protocol.user.User_Node;

public class Auditor_Node extends RMGR_Node{
	
	private String thePrefix;
	
	private List<Auditor_UsersNodesDir> uNodes; 
	
	private List<Auditor_STNodesDir> stNodes; 
	
	private List<Auditor_SPsNodesDir> suspiciousSPNodes; 
	
	private List<Auditor_SPsNodesDir> bannedSPNodes; 
	
	private List<Auditor_SPsNodesDir> DGUSPNodes; 
	
	private double TGTWholeWeight;
	
	private double TLRavgWeight;
	
	private double TSTWeight;
	
	private double suspiciousSPRank;
	
	private double suspiciousSPRange;
	
	private double suspiciousSPBanningRank;
	
	private int STmaxLifeTime;
	
	private int STagentStatusPostSpam;
	
	private int TopTLRavgPCT_ST;

	private int BottomTLRavgPCT_ST;
	
	private boolean PSL_STselector;
	
	private boolean SuspiciousNodes_STselector;
	
	private boolean TopTLRavg_STselector;
	
	private boolean BottomTLRavg_STselector;
	
	private boolean IIR_STselector;

	private int SufficientTLRus_SP;
	
	private int SufficientTLRus_PSP;
	
	private double PSL_PCT;
	
	private List<Auditor_GTNodesDir> gtNodes;
	
	private double PIIL_PCT;
	
	private boolean PIIL_GTselector;
	
	private boolean PSL_GTselector;
	
	private boolean SuspiciousNodes_GTselector;
	
	private int GTmaxLifeTime;
	
	private int GTagentStatusPostSpam;
	
	private boolean DeployST;
	
	private boolean DeployGT;
	
	private boolean GT_avgTGT;
	
	private double TGTWeight;
	
	private double TGTColludingWeight;
	
	private double TGTWeakColludingWeight;
	
	private double pcrThreshold;
	
	private double pcrWeakThreshold;
	
	private boolean ignoreOldGranks;
	
	private boolean stopTestingIfMinReached;
	
	private boolean GT_SelectOnlyPSP;
	
	private int GT_MaxSPNum;
	
	private int GT_MaxSize;
	
	private double postDGUinstallationRank;
	
	private double postStrictDGUEnableRank;
	
	private boolean DeployDGU;
	
	//TODO optimize the size of this array
	private List<TLRavgRank> TLRavg = new ArrayList<TLRavgRank>(1000);
	
	//TODO optimize the size of this array
	private List<TGTgRank> TGTg = new ArrayList<TGTgRank>(1000);
	
	//TODO optimize the size of this array
	private List<TGTgRank> TGTg_allCredential = new ArrayList<TGTgRank>(1000);
	
	//TODO optimize the size of this array
	private List<TGRRank> TGR = new ArrayList<TGRRank>(1000);
	
	//TODO optimize the size of this array
	private List<TSTRank> TST = new ArrayList<TSTRank>(1000);
	
	//TODO optimize the size of this array
	private List<SPRrecord> SPR = new ArrayList<SPRrecord>(1000);
	
	//TODO optimize the size of this array
	private List<IILrecord> IIR = new ArrayList<IILrecord>(500);
	
	//TODO optimize the size of this array
	private List<PIILrecord> PIILs = new ArrayList<PIILrecord>(1000);
	
	//TODO optimize the size of this array
	private List<PColludingRecord> PColludingRecords = new ArrayList<PColludingRecord>(300);
	
	//TODO optimize the size of this array
	private List<PColludingRecord> PWeakColludingRecords = new ArrayList<PColludingRecord>(300);
	
	
    private double AvgTimeToResolveCase_at_125;
    private double AvgTimeToResolveCase_at_250;
    private double AvgTimeToResolveCase_at_375;
    private double AvgTimeToResolveCase_at_500;
    
    private double PCT_Open_Cases_at_125;
    private double PCT_Open_Cases_at_250;
    private double PCT_Open_Cases_at_375;
    private double PCT_Open_Cases_at_500;
    
    private double PCT_PSPs_detected_by_TLRavg_Rank_at_125;
    private double PCT_PSPs_detected_by_TLRavg_Rank_at_250;
    private double PCT_PSPs_detected_by_TLRavg_Rank_at_375;
    private double PCT_PSPs_detected_by_TLRavg_Rank_at_500;
    
    private double PCT_PSPs_detected_by_TST_Rank_at_125;
    private double PCT_PSPs_detected_by_TST_Rank_at_250;
    private double PCT_PSPs_detected_by_TST_Rank_at_375;
    private double PCT_PSPs_detected_by_TST_Rank_at_500;
    
    private double PCT_PSPs_detected_by_TGTg_Rank_at_125;
    private double PCT_PSPs_detected_by_TGTg_Rank_at_250;
    private double PCT_PSPs_detected_by_TGTg_Rank_at_375;
    private double PCT_PSPs_detected_by_TGTg_Rank_at_500;
    
    private double PCT_PSPs_detected_by_Colluding_at_125;
    private double PCT_PSPs_detected_by_Colluding_at_250;
    private double PCT_PSPs_detected_by_Colluding_at_375;
    private double PCT_PSPs_detected_by_Colluding_at_500;
    
    private double PCT_PSPs_detected_by_weakColluding_at_125;
    private double PCT_PSPs_detected_by_weakColluding_at_250;
    private double PCT_PSPs_detected_by_weakColluding_at_375;
    private double PCT_PSPs_detected_by_weakColluding_at_500;
    
    private double PCT_PSPs_detected_by_Agent_at_125;
    private double PCT_PSPs_detected_by_Agent_at_250;
    private double PCT_PSPs_detected_by_Agent_at_375;
    private double PCT_PSPs_detected_by_Agent_at_500;

    private double PCT_PSPs_detected_by_Total_Rank_at_125;
    private double PCT_PSPs_detected_by_Total_Rank_at_250;
    private double PCT_PSPs_detected_by_Total_Rank_at_375;
    private double PCT_PSPs_detected_by_Total_Rank_at_500;
    
    private double PCT_SPs_detected_by_TLRavg_Rank_at_125;
    private double PCT_SPs_detected_by_TLRavg_Rank_at_250;
    private double PCT_SPs_detected_by_TLRavg_Rank_at_375;
    private double PCT_SPs_detected_by_TLRavg_Rank_at_500;
    
    private double PCT_SPs_detected_by_TST_Rank_at_125;
    private double PCT_SPs_detected_by_TST_Rank_at_250;
    private double PCT_SPs_detected_by_TST_Rank_at_375;
    private double PCT_SPs_detected_by_TST_Rank_at_500;
    
    private double PCT_SPs_detected_by_TGTg_Rank_at_125;
    private double PCT_SPs_detected_by_TGTg_Rank_at_250;
    private double PCT_SPs_detected_by_TGTg_Rank_at_375;
    private double PCT_SPs_detected_by_TGTg_Rank_at_500;
    
    private double PCT_SPs_detected_by_Colluding_at_125;
    private double PCT_SPs_detected_by_Colluding_at_250;
    private double PCT_SPs_detected_by_Colluding_at_375;
    private double PCT_SPs_detected_by_Colluding_at_500;
    
    private double PCT_SPs_detected_by_weakColluding_at_125;
    private double PCT_SPs_detected_by_weakColluding_at_250;
    private double PCT_SPs_detected_by_weakColluding_at_375;
    private double PCT_SPs_detected_by_weakColluding_at_500;
    
    private double PCT_SPs_detected_by_Agent_at_125;
    private double PCT_SPs_detected_by_Agent_at_250;
    private double PCT_SPs_detected_by_Agent_at_375;
    private double PCT_SPs_detected_by_Agent_at_500;
    
    private double PCT_SPs_detected_by_Total_Rank_at_125;
    private double PCT_SPs_detected_by_Total_Rank_at_250;
    private double PCT_SPs_detected_by_Total_Rank_at_375;
    private double PCT_SPs_detected_by_Total_Rank_at_500;
    
    private double PCT_MPSPs_detected_by_TLRavg_Rank_at_125;
    private double PCT_MPSPs_detected_by_TLRavg_Rank_at_250;
    private double PCT_MPSPs_detected_by_TLRavg_Rank_at_375;
    private double PCT_MPSPs_detected_by_TLRavg_Rank_at_500;
    
    private double PCT_MPSPs_detected_by_TST_Rank_at_125;
    private double PCT_MPSPs_detected_by_TST_Rank_at_250;
    private double PCT_MPSPs_detected_by_TST_Rank_at_375;
    private double PCT_MPSPs_detected_by_TST_Rank_at_500;
    
    private double PCT_MPSPs_detected_by_TGTg_Rank_at_125;
    private double PCT_MPSPs_detected_by_TGTg_Rank_at_250;
    private double PCT_MPSPs_detected_by_TGTg_Rank_at_375;
    private double PCT_MPSPs_detected_by_TGTg_Rank_at_500;
    
    private double PCT_MPSPs_detected_by_Colluding_at_125;
    private double PCT_MPSPs_detected_by_Colluding_at_250;
    private double PCT_MPSPs_detected_by_Colluding_at_375;
    private double PCT_MPSPs_detected_by_Colluding_at_500;
    
    private double PCT_MPSPs_detected_by_weakColluding_at_125;
    private double PCT_MPSPs_detected_by_weakColluding_at_250;
    private double PCT_MPSPs_detected_by_weakColluding_at_375;
    private double PCT_MPSPs_detected_by_weakColluding_at_500;
    
    private double PCT_MPSPs_detected_by_Agent_at_125;
    private double PCT_MPSPs_detected_by_Agent_at_250;
    private double PCT_MPSPs_detected_by_Agent_at_375;
    private double PCT_MPSPs_detected_by_Agent_at_500;

    private double PCT_MPSPs_detected_by_Total_Rank_at_125;
    private double PCT_MPSPs_detected_by_Total_Rank_at_250;
    private double PCT_MPSPs_detected_by_Total_Rank_at_375;
    private double PCT_MPSPs_detected_by_Total_Rank_at_500;

    private double PCT_MSPs_detected_by_TLRavg_Rank_at_125;
    private double PCT_MSPs_detected_by_TLRavg_Rank_at_250;
    private double PCT_MSPs_detected_by_TLRavg_Rank_at_375;
    private double PCT_MSPs_detected_by_TLRavg_Rank_at_500;

    private double PCT_MSPs_detected_by_TST_Rank_at_125;
    private double PCT_MSPs_detected_by_TST_Rank_at_250;
    private double PCT_MSPs_detected_by_TST_Rank_at_375;
    private double PCT_MSPs_detected_by_TST_Rank_at_500;
    
    private double PCT_MSPs_detected_by_TGTg_Rank_at_125;
    private double PCT_MSPs_detected_by_TGTg_Rank_at_250;
    private double PCT_MSPs_detected_by_TGTg_Rank_at_375;
    private double PCT_MSPs_detected_by_TGTg_Rank_at_500;
    
    private double PCT_MSPs_detected_by_Colluding_at_125;
    private double PCT_MSPs_detected_by_Colluding_at_250;
    private double PCT_MSPs_detected_by_Colluding_at_375;
    private double PCT_MSPs_detected_by_Colluding_at_500;
    
    private double PCT_MSPs_detected_by_weakColluding_at_125;
    private double PCT_MSPs_detected_by_weakColluding_at_250;
    private double PCT_MSPs_detected_by_weakColluding_at_375;
    private double PCT_MSPs_detected_by_weakColluding_at_500;
    
    private double PCT_MSPs_detected_by_Agent_at_125;
    private double PCT_MSPs_detected_by_Agent_at_250;
    private double PCT_MSPs_detected_by_Agent_at_375;
    private double PCT_MSPs_detected_by_Agent_at_500;
    
    private double PCT_MSPs_detected_by_Total_Rank_at_125;
    private double PCT_MSPs_detected_by_Total_Rank_at_250;
    private double PCT_MSPs_detected_by_Total_Rank_at_375;
    private double PCT_MSPs_detected_by_Total_Rank_at_500;
    
    private double PCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_125;
    private double PCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_250;
    private double PCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_375;
    private double PCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_500;

    private double PCT_Popular_MPSPs_detected_by_TST_Rank_at_125;
    private double PCT_Popular_MPSPs_detected_by_TST_Rank_at_250;
    private double PCT_Popular_MPSPs_detected_by_TST_Rank_at_375;
    private double PCT_Popular_MPSPs_detected_by_TST_Rank_at_500;
    
    private double PCT_Popular_MPSPs_detected_by_TGTg_Rank_at_125;
    private double PCT_Popular_MPSPs_detected_by_TGTg_Rank_at_250;
    private double PCT_Popular_MPSPs_detected_by_TGTg_Rank_at_375;
    private double PCT_Popular_MPSPs_detected_by_TGTg_Rank_at_500;
    
    private double PCT_Popular_MPSPs_detected_by_Colluding_at_125;
    private double PCT_Popular_MPSPs_detected_by_Colluding_at_250;
    private double PCT_Popular_MPSPs_detected_by_Colluding_at_375;
    private double PCT_Popular_MPSPs_detected_by_Colluding_at_500;
    
    private double PCT_Popular_MPSPs_detected_by_weakColluding_at_125;
    private double PCT_Popular_MPSPs_detected_by_weakColluding_at_250;
    private double PCT_Popular_MPSPs_detected_by_weakColluding_at_375;
    private double PCT_Popular_MPSPs_detected_by_weakColluding_at_500;
    
    private double PCT_Popular_MPSPs_detected_by_Agent_at_125;
    private double PCT_Popular_MPSPs_detected_by_Agent_at_250;
    private double PCT_Popular_MPSPs_detected_by_Agent_at_375;
    private double PCT_Popular_MPSPs_detected_by_Agent_at_500;

    private double PCT_Popular_MPSPs_detected_by_Total_Rank_at_125;
    private double PCT_Popular_MPSPs_detected_by_Total_Rank_at_250;
    private double PCT_Popular_MPSPs_detected_by_Total_Rank_at_375;
    private double PCT_Popular_MPSPs_detected_by_Total_Rank_at_500;

    private double PCT_Popular_MSPs_detected_by_TLRavg_Rank_at_125;
    private double PCT_Popular_MSPs_detected_by_TLRavg_Rank_at_250;
    private double PCT_Popular_MSPs_detected_by_TLRavg_Rank_at_375;
    private double PCT_Popular_MSPs_detected_by_TLRavg_Rank_at_500;
    
    private double PCT_Popular_MSPs_detected_by_TST_Rank_at_125;
    private double PCT_Popular_MSPs_detected_by_TST_Rank_at_250;
    private double PCT_Popular_MSPs_detected_by_TST_Rank_at_375;
    private double PCT_Popular_MSPs_detected_by_TST_Rank_at_500;
    
    private double PCT_Popular_MSPs_detected_by_TGTg_Rank_at_125;
    private double PCT_Popular_MSPs_detected_by_TGTg_Rank_at_250;
    private double PCT_Popular_MSPs_detected_by_TGTg_Rank_at_375;
    private double PCT_Popular_MSPs_detected_by_TGTg_Rank_at_500;
    
    private double PCT_Popular_MSPs_detected_by_Colluding_at_125;
    private double PCT_Popular_MSPs_detected_by_Colluding_at_250;
    private double PCT_Popular_MSPs_detected_by_Colluding_at_375;
    private double PCT_Popular_MSPs_detected_by_Colluding_at_500;
    
    private double PCT_Popular_MSPs_detected_by_weakColluding_at_125;
    private double PCT_Popular_MSPs_detected_by_weakColluding_at_250;
    private double PCT_Popular_MSPs_detected_by_weakColluding_at_375;
    private double PCT_Popular_MSPs_detected_by_weakColluding_at_500;
    
    private double PCT_Popular_MSPs_detected_by_Agent_at_125;
    private double PCT_Popular_MSPs_detected_by_Agent_at_250;
    private double PCT_Popular_MSPs_detected_by_Agent_at_375;
    private double PCT_Popular_MSPs_detected_by_Agent_at_500;

    private double PCT_Popular_MSPs_detected_by_Total_Rank_at_125;
    private double PCT_Popular_MSPs_detected_by_Total_Rank_at_250;
    private double PCT_Popular_MSPs_detected_by_Total_Rank_at_375;
    private double PCT_Popular_MSPs_detected_by_Total_Rank_at_500;
    
    private double PCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_125;
    private double PCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_250;
    private double PCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_375;
    private double PCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_500;

    private double PCT_Unpopular_MPSPs_detected_by_TST_Rank_at_125;
    private double PCT_Unpopular_MPSPs_detected_by_TST_Rank_at_250;
    private double PCT_Unpopular_MPSPs_detected_by_TST_Rank_at_375;
    private double PCT_Unpopular_MPSPs_detected_by_TST_Rank_at_500;
    
    private double PCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_125;
    private double PCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_250;
    private double PCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_375;
    private double PCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_500;
    
    private double PCT_Unpopular_MPSPs_detected_by_Colluding_at_125;
    private double PCT_Unpopular_MPSPs_detected_by_Colluding_at_250;
    private double PCT_Unpopular_MPSPs_detected_by_Colluding_at_375;
    private double PCT_Unpopular_MPSPs_detected_by_Colluding_at_500;
    
    private double PCT_Unpopular_MPSPs_detected_by_weakColluding_at_125;
    private double PCT_Unpopular_MPSPs_detected_by_weakColluding_at_250;
    private double PCT_Unpopular_MPSPs_detected_by_weakColluding_at_375;
    private double PCT_Unpopular_MPSPs_detected_by_weakColluding_at_500;
    
    private double PCT_Unpopular_MPSPs_detected_by_Agent_at_125;
    private double PCT_Unpopular_MPSPs_detected_by_Agent_at_250;
    private double PCT_Unpopular_MPSPs_detected_by_Agent_at_375;
    private double PCT_Unpopular_MPSPs_detected_by_Agent_at_500;

    private double PCT_Unpopular_MPSPs_detected_by_Total_Rank_at_125;
    private double PCT_Unpopular_MPSPs_detected_by_Total_Rank_at_250;
    private double PCT_Unpopular_MPSPs_detected_by_Total_Rank_at_375;
    private double PCT_Unpopular_MPSPs_detected_by_Total_Rank_at_500;

    private double PCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_125;
    private double PCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_250;
    private double PCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_375;
    private double PCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_500;
    
    private double PCT_Unpopular_MSPs_detected_by_TST_Rank_at_125;
    private double PCT_Unpopular_MSPs_detected_by_TST_Rank_at_250;
    private double PCT_Unpopular_MSPs_detected_by_TST_Rank_at_375;
    private double PCT_Unpopular_MSPs_detected_by_TST_Rank_at_500;
    
    private double PCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_125;
    private double PCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_250;
    private double PCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_375;
    private double PCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_500;
    
    private double PCT_Unpopular_MSPs_detected_by_Colluding_at_125;
    private double PCT_Unpopular_MSPs_detected_by_Colluding_at_250;
    private double PCT_Unpopular_MSPs_detected_by_Colluding_at_375;
    private double PCT_Unpopular_MSPs_detected_by_Colluding_at_500;
    
    private double PCT_Unpopular_MSPs_detected_by_weakColluding_at_125;
    private double PCT_Unpopular_MSPs_detected_by_weakColluding_at_250;
    private double PCT_Unpopular_MSPs_detected_by_weakColluding_at_375;
    private double PCT_Unpopular_MSPs_detected_by_weakColluding_at_500;

    private double PCT_Unpopular_MSPs_detected_by_Agent_at_125;
    private double PCT_Unpopular_MSPs_detected_by_Agent_at_250;
    private double PCT_Unpopular_MSPs_detected_by_Agent_at_375;
    private double PCT_Unpopular_MSPs_detected_by_Agent_at_500;

    private double PCT_Unpopular_MSPs_detected_by_Total_Rank_at_125;
    private double PCT_Unpopular_MSPs_detected_by_Total_Rank_at_250;
    private double PCT_Unpopular_MSPs_detected_by_Total_Rank_at_375;
    private double PCT_Unpopular_MSPs_detected_by_Total_Rank_at_500;
    
	public Auditor_Node(String prefix) {
		super(prefix);
		thePrefix = prefix;
		
		//TODO optimize the size of this array
	    setuNodes(new ArrayList<Auditor_UsersNodesDir> (1000));
	    //TODO optimize the size of this array
	    setSuspiciousSPNodes(new ArrayList<Auditor_SPsNodesDir> (100));
	    //TODO optimize the size of this array
	    setBannedSPNodes(new ArrayList<Auditor_SPsNodesDir> (100));
	    //TODO optimize the size of this array
	    setDGUSPNodes(new ArrayList<Auditor_SPsNodesDir> (100));
	    
	    //TODO optimize the size of this array
	    setStNodes(new ArrayList<Auditor_STNodesDir> (400));
	    
	    //TODO optimize the size of this array
	    setGtNodes(new ArrayList<Auditor_GTNodesDir> (400));
	}

	public Object clone() {

        Auditor_Node inp = new Auditor_Node(thePrefix);
        return inp;
    }
	
	public void updateDetectorList(int cycle)
	{
		double countBannedPSPs = 0;
		double countBannedPSPs_detectedByTLRavg = 0;
		double countBannedPSPs_detectedByTST = 0;
		double countBannedPSPs_detectedByTGTg = 0;
		double countBannedPSPs_detectedByColluding = 0;
		double countBannedPSPs_detectedByWeakColluding = 0;
		double countBannedPSPs_detectedByAgent = 0;
		double countBannedPSPs_detectedByTotalRank = 0;
		
		double countBannedSPs = 0;
		double countBannedSPs_detectedByTLRavg = 0;
		double countBannedSPs_detectedByTST = 0;
		double countBannedSPs_detectedByTGTg = 0;
		double countBannedSPs_detectedByColluding = 0;
		double countBannedSPs_detectedByWeakColluding = 0;
		double countBannedSPs_detectedByAgent = 0;
		double countBannedSPs_detectedByTotalRank = 0;
		
		double countBannedMPSPs = 0;
		double countBannedMPSPs_detectedByTLRavg = 0;
		double countBannedMPSPs_detectedByTST = 0;
		double countBannedMPSPs_detectedByTGTg = 0;
		double countBannedMPSPs_detectedByColluding = 0;
		double countBannedMPSPs_detectedByWeakColluding = 0;
		double countBannedMPSPs_detectedByAgent = 0;
		double countBannedMPSPs_detectedByTotalRank = 0;
		
		double countBannedMSPs = 0;
		double countBannedMSPs_detectedByTLRavg = 0;
		double countBannedMSPs_detectedByTST = 0;
		double countBannedMSPs_detectedByTGTg = 0;
		double countBannedMSPs_detectedByColluding = 0;
		double countBannedMSPs_detectedByWeakColluding = 0;
		double countBannedMSPs_detectedByAgent = 0;
		double countBannedMSPs_detectedByTotalRank = 0;
		
		double countBannedPopularMPSPs = 0;
		double countBannedPopularMPSPs_detectedByTLRavg = 0;
		double countBannedPopularMPSPs_detectedByTST = 0;
		double countBannedPopularMPSPs_detectedByTGTg = 0;
		double countBannedPopularMPSPs_detectedByColluding = 0;
		double countBannedPopularMPSPs_detectedByWeakColluding = 0;
		double countBannedPopularMPSPs_detectedByAgent = 0;
		double countBannedPopularMPSPs_detectedByTotalRank = 0;
		
		double countBannedPopularMSPs = 0;
		double countBannedPopularMSPs_detectedByTLRavg = 0;
		double countBannedPopularMSPs_detectedByTST = 0;
		double countBannedPopularMSPs_detectedByTGTg = 0;
		double countBannedPopularMSPs_detectedByColluding = 0;
		double countBannedPopularMSPs_detectedByWeakColluding = 0;
		double countBannedPopularMSPs_detectedByAgent = 0;
		double countBannedPopularMSPs_detectedByTotalRank = 0;

		double countBannedUnpopularMPSPs = 0;
		double countBannedUnpopularMPSPs_detectedByTLRavg = 0;
		double countBannedUnpopularMPSPs_detectedByTST = 0;
		double countBannedUnpopularMPSPs_detectedByTGTg = 0;
		double countBannedUnpopularMPSPs_detectedByColluding = 0;
		double countBannedUnpopularMPSPs_detectedByWeakColluding = 0;
		double countBannedUnpopularMPSPs_detectedByAgent = 0;
		double countBannedUnpopularMPSPs_detectedByTotalRank = 0;
		
		double countBannedUnpopularMSPs = 0;
		double countBannedUnpopularMSPs_detectedByTLRavg = 0;
		double countBannedUnpopularMSPs_detectedByTST = 0;
		double countBannedUnpopularMSPs_detectedByTGTg = 0;
		double countBannedUnpopularMSPs_detectedByColluding = 0;
		double countBannedUnpopularMSPs_detectedByWeakColluding = 0;
		double countBannedUnpopularMSPs_detectedByAgent = 0;
		double countBannedUnpopularMSPs_detectedByTotalRank = 0;
		
		// detectorType: 1: TLRavg_Rank; 2: TST_Rank; 3: TGTg_Rank; 4: colluding; 5: weakColluding; 6: ST/GT_Agent; 7: totalRank
		
		for(Auditor_SPsNodesDir node: bannedSPNodes)
		{
			boolean popular = node.getSpUsefulness() >= usefulnessMIN;
			
			SP_Node sp = (SP_Node) Network.get(node.getPosition()).getProtocol(SPPid);
			
			if(!sp.isMalicious())
			{
				if(popular)
				{
					countBannedPSPs++;
					
					switch(sp.getBanDetectorType()){
					
					case 1:
						countBannedPSPs_detectedByTLRavg++;
						break;
						
					case 2: 
						countBannedPSPs_detectedByTST++;
						break;
						
					case 3: 
						countBannedPSPs_detectedByTGTg++;
						break;
						
					case 4: 
						countBannedPSPs_detectedByColluding++;
						break;
						
					case 5: 
						countBannedPSPs_detectedByWeakColluding++;
						break;
					
					case 6:
						countBannedPSPs_detectedByAgent++;
						break;
					case 7:
						countBannedPSPs_detectedByTotalRank++;
						break;
					}
					
				}
				else
				{
					countBannedSPs++;
					
					switch(sp.getBanDetectorType()){
					
					case 1:
						countBannedSPs_detectedByTLRavg++;
						break;
						
					case 2: 
						countBannedSPs_detectedByTST++;
						break;
						
					case 3: 
						countBannedSPs_detectedByTGTg++;
						break;
						
					case 4: 
						countBannedSPs_detectedByColluding++;
						break;
						
					case 5: 
						countBannedSPs_detectedByWeakColluding++;
						break;
					
					case 6:
						countBannedSPs_detectedByAgent++;
						break;
					case 7:
						countBannedSPs_detectedByTotalRank++;
						break;
					}
				}
				
				continue;
			}
			
			if(sp.isAssociatedWithME())
			{
				//System.out.println("Hello Colluding!");
				
				ME_Node me = (ME_Node) Network.get(sp.getMEid()).getProtocol(MEPid);
				
				if(!me.isUnpopularSPsColluding())
				{
					if(popular)
					{
						countBannedPopularMPSPs++;
						
						switch(sp.getBanDetectorType()){
						
						case 1:
							countBannedPopularMPSPs_detectedByTLRavg++;
							break;
							
						case 2: 
							countBannedPopularMPSPs_detectedByTST++;
							break;
						
						case 3: 
							countBannedPopularMPSPs_detectedByTGTg++;
							break;
							
						case 4: 
							countBannedPopularMPSPs_detectedByColluding++;
							break;
							
						case 5: 
							countBannedPopularMPSPs_detectedByWeakColluding++;
							break;
							
						case 6:
							countBannedPopularMPSPs_detectedByAgent++;
							break;
						case 7:
							countBannedPopularMPSPs_detectedByTotalRank++;
							break;
							
						default:
							System.out.println("P_MPSP Unknown type: "+node.getBanDetectorType());
							break;
						}
					}
					else
					{
						countBannedPopularMSPs++;
						
						switch(sp.getBanDetectorType()){
						
						case 1:
							countBannedPopularMSPs_detectedByTLRavg++;
							break;
							
						case 2: 
							countBannedPopularMSPs_detectedByTST++;
							break;
						
						case 3: 
							countBannedPopularMSPs_detectedByTGTg++;
							break;
							
						case 4: 
							countBannedPopularMSPs_detectedByColluding++;
							break;
							
						case 5: 
							countBannedPopularMSPs_detectedByWeakColluding++;
							break;
							
						case 6:
							countBannedPopularMSPs_detectedByAgent++;
							break;
						case 7:
							countBannedPopularMSPs_detectedByTotalRank++;
							break;
							
						default:
							System.out.println("P_MSP Unknown type: "+node.getBanDetectorType());
							break;
						}
					}
				}
				else
				{
					//System.out.println("Hello Malicious!");
					
					if(popular)
					{
						countBannedUnpopularMPSPs++;
						
						switch(sp.getBanDetectorType()){
						
						case 1:
							countBannedUnpopularMPSPs_detectedByTLRavg++;
							break;
							
						case 2: 
							countBannedUnpopularMPSPs_detectedByTST++;
							break;
						
						case 3: 
							countBannedUnpopularMPSPs_detectedByTGTg++;
							break;
							
						case 4: 
							countBannedUnpopularMPSPs_detectedByColluding++;
							break;
							
						case 5: 
							countBannedUnpopularMPSPs_detectedByWeakColluding++;
							break;
							
						case 6:
							countBannedUnpopularMPSPs_detectedByAgent++;
							break;
						case 7:
							countBannedUnpopularMPSPs_detectedByTotalRank++;
							break;
							
						default:
							System.out.println("U_MPSP Unknown type: "+node.getBanDetectorType());
							break;
						}
					}
					else
					{
						countBannedUnpopularMSPs++;
						
						switch(sp.getBanDetectorType()){
						
						case 1:
							countBannedUnpopularMSPs_detectedByTLRavg++;
							break;
							
						case 2: 
							countBannedUnpopularMSPs_detectedByTST++;
							break;
							
						case 3: 
							countBannedUnpopularMSPs_detectedByTGTg++;
							break;
							
						case 4: 
							countBannedUnpopularMSPs_detectedByColluding++;
							break;
							
						case 5: 
							countBannedUnpopularMSPs_detectedByWeakColluding++;
							break;
						
						case 6:
							countBannedUnpopularMSPs_detectedByAgent++;
							break;
						case 7:
							countBannedUnpopularMSPs_detectedByTotalRank++;
							break;
							
						default:
							System.out.println("U_MPSP Unknown type: "+node.getBanDetectorType());
							break;
						}
					}
				}
				
				continue;
			}
			
			if(popular)
			{
				countBannedMPSPs++;
				
				switch(sp.getBanDetectorType()){
				
				case 1:
					countBannedMPSPs_detectedByTLRavg++;
					break;
					
				case 2: 
					countBannedMPSPs_detectedByTST++;
					break;
					
				case 3: 
					countBannedMPSPs_detectedByTGTg++;
					break;
					
				case 4: 
					countBannedMPSPs_detectedByColluding++;
					break;
					
				case 5: 
					countBannedMPSPs_detectedByWeakColluding++;
					break;
				
				case 6:
					countBannedMPSPs_detectedByAgent++;
					break;
				case 7:
					countBannedMPSPs_detectedByTotalRank++;
					break;
					
				default:
					System.out.println("MPSP Unknown type: "+node.getBanDetectorType());
					break;
				}
			}
			else
			{
				countBannedMSPs++;
				
				switch(sp.getBanDetectorType()){
				
				case 1:
					countBannedMSPs_detectedByTLRavg++;
					break;
					
				case 2: 
					countBannedMSPs_detectedByTST++;
					break;
				
				case 3: 
					countBannedMSPs_detectedByTGTg++;
					break;
					
				case 4: 
					countBannedMSPs_detectedByColluding++;
					break;
					
				case 5: 
					countBannedMSPs_detectedByWeakColluding++;
					break;
					
				case 6:
					countBannedMSPs_detectedByAgent++;
					break;
				case 7:
					countBannedMSPs_detectedByTotalRank++;
					break;
					
				default:
					System.out.println("MSP Unknown type: "+node.getBanDetectorType());
					break;
				}
			}
			
		}
		
		if(DeployDGU)
		for(Auditor_SPsNodesDir node: DGUSPNodes)
		{
			boolean popular = node.getSpUsefulness() >= usefulnessMIN;
			
			SP_Node sp = (SP_Node) Network.get(node.getPosition()).getProtocol(SPPid);
			
			if(sp.isBanned())
				continue;
			
			if(!sp.isMalicious())
				continue;
			
			if(sp.isAssociatedWithME())
			{			
				if(!sp.isEnabledStrictDGU())
					continue;
				
				ME_Node me = (ME_Node) Network.get(sp.getMEid()).getProtocol(MEPid);
				
				if(!me.isUnpopularSPsColluding())
				{
					if(popular)
					{
						countBannedPopularMPSPs++;
						
						switch(sp.getEnableStrictDGUdetectorType()){
						
						case 1:
							countBannedPopularMPSPs_detectedByTLRavg++;
							break;
							
						case 2: 
							countBannedPopularMPSPs_detectedByTST++;
							break;
						
						case 3: 
							countBannedPopularMPSPs_detectedByTGTg++;
							break;
							
						case 4: 
							countBannedPopularMPSPs_detectedByColluding++;
							break;
							
						case 5: 
							countBannedPopularMPSPs_detectedByWeakColluding++;
							break;
							
						case 6:
							countBannedPopularMPSPs_detectedByAgent++;
							break;
						case 7:
							countBannedPopularMPSPs_detectedByTotalRank++;
							break;
							
						default:
							System.out.println("P_MPSP Unknown type: "+node.getBanDetectorType());
							break;
						}
					}
					else
					{
						countBannedPopularMSPs++;
						
						switch(sp.getEnableStrictDGUdetectorType()){
						
						case 1:
							countBannedPopularMSPs_detectedByTLRavg++;
							break;
							
						case 2: 
							countBannedPopularMSPs_detectedByTST++;
							break;
						
						case 3: 
							countBannedPopularMSPs_detectedByTGTg++;
							break;
							
						case 4: 
							countBannedPopularMSPs_detectedByColluding++;
							break;
							
						case 5: 
							countBannedPopularMSPs_detectedByWeakColluding++;
							break;
							
						case 6:
							countBannedPopularMSPs_detectedByAgent++;
							break;
						case 7:
							countBannedPopularMSPs_detectedByTotalRank++;
							break;
							
						default:
							System.out.println("P_MSP Unknown type: "+node.getBanDetectorType());
							break;
						}
					}
				}
				else
				{	
					if(popular)
					{
						countBannedUnpopularMPSPs++;
						
						switch(sp.getEnableStrictDGUdetectorType()){
						
						case 1:
							countBannedUnpopularMPSPs_detectedByTLRavg++;
							break;
							
						case 2: 
							countBannedUnpopularMPSPs_detectedByTST++;
							break;
						
						case 3: 
							countBannedUnpopularMPSPs_detectedByTGTg++;
							break;
							
						case 4: 
							countBannedUnpopularMPSPs_detectedByColluding++;
							break;
							
						case 5: 
							countBannedUnpopularMPSPs_detectedByWeakColluding++;
							break;
							
						case 6:
							countBannedUnpopularMPSPs_detectedByAgent++;
							break;
						case 7:
							countBannedUnpopularMPSPs_detectedByTotalRank++;
							break;
							
						default:
							System.out.println("U_MPSP Unknown type: "+node.getBanDetectorType());
							break;
						}
					}
					else
					{
						countBannedUnpopularMSPs++;
						
						switch(sp.getEnableStrictDGUdetectorType()){
						
						case 1:
							countBannedUnpopularMSPs_detectedByTLRavg++;
							break;
							
						case 2: 
							countBannedUnpopularMSPs_detectedByTST++;
							break;
							
						case 3: 
							countBannedUnpopularMSPs_detectedByTGTg++;
							break;
							
						case 4: 
							countBannedUnpopularMSPs_detectedByColluding++;
							break;
							
						case 5: 
							countBannedUnpopularMSPs_detectedByWeakColluding++;
							break;
						
						case 6:
							countBannedUnpopularMSPs_detectedByAgent++;
							break;
						case 7:
							countBannedUnpopularMSPs_detectedByTotalRank++;
							break;
							
						default:
							System.out.println("U_MPSP Unknown type: "+node.getBanDetectorType());
							break;
						}
					}
				}
				
				continue;
			}
			
			if(popular)
			{
				countBannedMPSPs++;
				
				switch(sp.getInstallDGUdetectorType()){
				
				case 1:
					countBannedMPSPs_detectedByTLRavg++;
					break;
					
				case 2: 
					countBannedMPSPs_detectedByTST++;
					break;
					
				case 3: 
					countBannedMPSPs_detectedByTGTg++;
					break;
					
				case 4: 
					countBannedMPSPs_detectedByColluding++;
					break;
					
				case 5: 
					countBannedMPSPs_detectedByWeakColluding++;
					break;
				
				case 6:
					countBannedMPSPs_detectedByAgent++;
					break;
				case 7:
					countBannedMPSPs_detectedByTotalRank++;
					break;
					
				default:
					System.out.println("MPSP Unknown type: "+node.getBanDetectorType());
					break;
				}
			}
			else
			{
				countBannedMSPs++;
				
				switch(sp.getInstallDGUdetectorType()){
				
				case 1:
					countBannedMSPs_detectedByTLRavg++;
					break;
					
				case 2: 
					countBannedMSPs_detectedByTST++;
					break;
				
				case 3: 
					countBannedMSPs_detectedByTGTg++;
					break;
					
				case 4: 
					countBannedMSPs_detectedByColluding++;
					break;
					
				case 5: 
					countBannedMSPs_detectedByWeakColluding++;
					break;
					
				case 6:
					countBannedMSPs_detectedByAgent++;
					break;
				case 7:
					countBannedMSPs_detectedByTotalRank++;
					break;
					
				default:
					System.out.println("MSP Unknown type: "+node.getBanDetectorType());
					break;
				}
			}
			
		}
		
		switch(cycle){
		
		case 125:
			
			PCT_PSPs_detected_by_TLRavg_Rank_at_125 = countBannedPSPs_detectedByTLRavg*100/countBannedPSPs;
			PCT_SPs_detected_by_TLRavg_Rank_at_125 = countBannedSPs_detectedByTLRavg*100/countBannedSPs;
			PCT_MPSPs_detected_by_TLRavg_Rank_at_125 = countBannedMPSPs_detectedByTLRavg*100/countBannedMPSPs;
			PCT_MSPs_detected_by_TLRavg_Rank_at_125 = countBannedMSPs_detectedByTLRavg*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_125 = countBannedPopularMPSPs_detectedByTLRavg*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_TLRavg_Rank_at_125 = countBannedPopularMSPs_detectedByTLRavg*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_125 = countBannedUnpopularMPSPs_detectedByTLRavg*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_125 = countBannedUnpopularMSPs_detectedByTLRavg*100/countBannedUnpopularMSPs;
			
			PCT_PSPs_detected_by_TST_Rank_at_125 = countBannedPSPs_detectedByTST*100/countBannedPSPs;
			PCT_SPs_detected_by_TST_Rank_at_125 = countBannedSPs_detectedByTST*100/countBannedSPs;
			PCT_MPSPs_detected_by_TST_Rank_at_125 = countBannedMPSPs_detectedByTST*100/countBannedMPSPs;
			PCT_MSPs_detected_by_TST_Rank_at_125 = countBannedMSPs_detectedByTST*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_TST_Rank_at_125 = countBannedPopularMPSPs_detectedByTST*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_TST_Rank_at_125 = countBannedPopularMSPs_detectedByTST*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_TST_Rank_at_125 = countBannedUnpopularMPSPs_detectedByTST*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_TST_Rank_at_125 = countBannedUnpopularMSPs_detectedByTST*100/countBannedUnpopularMSPs;
			
			PCT_PSPs_detected_by_TGTg_Rank_at_125 = countBannedPSPs_detectedByTGTg*100/countBannedPSPs;
			PCT_SPs_detected_by_TGTg_Rank_at_125 = countBannedSPs_detectedByTGTg*100/countBannedSPs;
			PCT_MPSPs_detected_by_TGTg_Rank_at_125 = countBannedMPSPs_detectedByTGTg*100/countBannedMPSPs;
			PCT_MSPs_detected_by_TGTg_Rank_at_125 = countBannedMSPs_detectedByTGTg*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_TGTg_Rank_at_125 = countBannedPopularMPSPs_detectedByTGTg*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_TGTg_Rank_at_125 = countBannedPopularMSPs_detectedByTGTg*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_125 = countBannedUnpopularMPSPs_detectedByTGTg*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_125 = countBannedUnpopularMSPs_detectedByTGTg*100/countBannedUnpopularMSPs;
			
			PCT_PSPs_detected_by_Colluding_at_125 = countBannedPSPs_detectedByColluding*100/countBannedPSPs;
			PCT_SPs_detected_by_Colluding_at_125 = countBannedSPs_detectedByColluding*100/countBannedSPs;
			PCT_MPSPs_detected_by_Colluding_at_125 = countBannedMPSPs_detectedByColluding*100/countBannedMPSPs;
			PCT_MSPs_detected_by_Colluding_at_125 = countBannedMSPs_detectedByColluding*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_Colluding_at_125 = countBannedPopularMPSPs_detectedByColluding*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_Colluding_at_125 = countBannedPopularMSPs_detectedByColluding*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_Colluding_at_125 = countBannedUnpopularMPSPs_detectedByColluding*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_Colluding_at_125 = countBannedUnpopularMSPs_detectedByColluding*100/countBannedUnpopularMSPs;
			
			PCT_PSPs_detected_by_weakColluding_at_125 = countBannedPSPs_detectedByWeakColluding*100/countBannedPSPs;
			PCT_SPs_detected_by_weakColluding_at_125 = countBannedSPs_detectedByWeakColluding*100/countBannedSPs;
			PCT_MPSPs_detected_by_weakColluding_at_125 = countBannedMPSPs_detectedByWeakColluding*100/countBannedMPSPs;
			PCT_MSPs_detected_by_weakColluding_at_125 = countBannedMSPs_detectedByWeakColluding*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_weakColluding_at_125 = countBannedPopularMPSPs_detectedByWeakColluding*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_weakColluding_at_125 = countBannedPopularMSPs_detectedByWeakColluding*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_weakColluding_at_125 = countBannedUnpopularMPSPs_detectedByWeakColluding*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_weakColluding_at_125 = countBannedUnpopularMSPs_detectedByWeakColluding*100/countBannedUnpopularMSPs;
						
			PCT_PSPs_detected_by_Agent_at_125 = countBannedPSPs_detectedByAgent*100/countBannedPSPs;
			PCT_SPs_detected_by_Agent_at_125 = countBannedSPs_detectedByAgent*100/countBannedSPs;
			PCT_MPSPs_detected_by_Agent_at_125 = countBannedMPSPs_detectedByAgent*100/countBannedMPSPs;
			PCT_MSPs_detected_by_Agent_at_125 = countBannedMSPs_detectedByAgent*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_Agent_at_125 = countBannedPopularMPSPs_detectedByAgent*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_Agent_at_125 = countBannedPopularMSPs_detectedByAgent*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_Agent_at_125 = countBannedUnpopularMPSPs_detectedByAgent*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_Agent_at_125 = countBannedUnpopularMSPs_detectedByAgent*100/countBannedUnpopularMSPs;
			
			PCT_PSPs_detected_by_Total_Rank_at_125 = countBannedPSPs_detectedByTotalRank*100/countBannedPSPs;
			PCT_SPs_detected_by_Total_Rank_at_125 = countBannedSPs_detectedByTotalRank*100/countBannedSPs;
			PCT_MPSPs_detected_by_Total_Rank_at_125 = countBannedMPSPs_detectedByTotalRank*100/countBannedMPSPs;
			PCT_MSPs_detected_by_Total_Rank_at_125 = countBannedMSPs_detectedByTotalRank*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_Total_Rank_at_125 = countBannedPopularMPSPs_detectedByTotalRank*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_Total_Rank_at_125 = countBannedPopularMSPs_detectedByTotalRank*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_Total_Rank_at_125 = countBannedUnpopularMPSPs_detectedByTotalRank*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_Total_Rank_at_125 = countBannedUnpopularMSPs_detectedByTotalRank*100/countBannedUnpopularMSPs;
			
			break;
		
		case 250:
			
			PCT_PSPs_detected_by_TLRavg_Rank_at_250 = countBannedPSPs_detectedByTLRavg*100/countBannedPSPs;
			PCT_SPs_detected_by_TLRavg_Rank_at_250 = countBannedSPs_detectedByTLRavg*100/countBannedSPs;
			PCT_MPSPs_detected_by_TLRavg_Rank_at_250 = countBannedMPSPs_detectedByTLRavg*100/countBannedMPSPs;
			PCT_MSPs_detected_by_TLRavg_Rank_at_250 = countBannedMSPs_detectedByTLRavg*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_250 = countBannedPopularMPSPs_detectedByTLRavg*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_TLRavg_Rank_at_250 = countBannedPopularMSPs_detectedByTLRavg*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_250 = countBannedUnpopularMPSPs_detectedByTLRavg*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_250 = countBannedUnpopularMSPs_detectedByTLRavg*100/countBannedUnpopularMSPs;

			PCT_PSPs_detected_by_TST_Rank_at_250 = countBannedPSPs_detectedByTST*100/countBannedPSPs;
			PCT_SPs_detected_by_TST_Rank_at_250 = countBannedSPs_detectedByTST*100/countBannedSPs;
			PCT_MPSPs_detected_by_TST_Rank_at_250 = countBannedMPSPs_detectedByTST*100/countBannedMPSPs;
			PCT_MSPs_detected_by_TST_Rank_at_250 = countBannedMSPs_detectedByTST*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_TST_Rank_at_250 = countBannedPopularMPSPs_detectedByTST*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_TST_Rank_at_250 = countBannedPopularMSPs_detectedByTST*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_TST_Rank_at_250 = countBannedUnpopularMPSPs_detectedByTST*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_TST_Rank_at_250 = countBannedUnpopularMSPs_detectedByTST*100/countBannedUnpopularMSPs;
			
			PCT_PSPs_detected_by_TGTg_Rank_at_250 = countBannedPSPs_detectedByTGTg*100/countBannedPSPs;
			PCT_SPs_detected_by_TGTg_Rank_at_250 = countBannedSPs_detectedByTGTg*100/countBannedSPs;
			PCT_MPSPs_detected_by_TGTg_Rank_at_250 = countBannedMPSPs_detectedByTGTg*100/countBannedMPSPs;
			PCT_MSPs_detected_by_TGTg_Rank_at_250 = countBannedMSPs_detectedByTGTg*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_TGTg_Rank_at_250 = countBannedPopularMPSPs_detectedByTGTg*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_TGTg_Rank_at_250 = countBannedPopularMSPs_detectedByTGTg*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_250 = countBannedUnpopularMPSPs_detectedByTGTg*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_250 = countBannedUnpopularMSPs_detectedByTGTg*100/countBannedUnpopularMSPs;
			
			PCT_PSPs_detected_by_Colluding_at_250 = countBannedPSPs_detectedByColluding*100/countBannedPSPs;
			PCT_SPs_detected_by_Colluding_at_250 = countBannedSPs_detectedByColluding*100/countBannedSPs;
			PCT_MPSPs_detected_by_Colluding_at_250 = countBannedMPSPs_detectedByColluding*100/countBannedMPSPs;
			PCT_MSPs_detected_by_Colluding_at_250 = countBannedMSPs_detectedByColluding*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_Colluding_at_250 = countBannedPopularMPSPs_detectedByColluding*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_Colluding_at_250 = countBannedPopularMSPs_detectedByColluding*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_Colluding_at_250 = countBannedUnpopularMPSPs_detectedByColluding*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_Colluding_at_250 = countBannedUnpopularMSPs_detectedByColluding*100/countBannedUnpopularMSPs;
			
			PCT_PSPs_detected_by_weakColluding_at_250 = countBannedPSPs_detectedByWeakColluding*100/countBannedPSPs;
			PCT_SPs_detected_by_weakColluding_at_250 = countBannedSPs_detectedByWeakColluding*100/countBannedSPs;
			PCT_MPSPs_detected_by_weakColluding_at_250 = countBannedMPSPs_detectedByWeakColluding*100/countBannedMPSPs;
			PCT_MSPs_detected_by_weakColluding_at_250 = countBannedMSPs_detectedByWeakColluding*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_weakColluding_at_250 = countBannedPopularMPSPs_detectedByWeakColluding*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_weakColluding_at_250 = countBannedPopularMSPs_detectedByWeakColluding*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_weakColluding_at_250 = countBannedUnpopularMPSPs_detectedByWeakColluding*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_weakColluding_at_250 = countBannedUnpopularMSPs_detectedByWeakColluding*100/countBannedUnpopularMSPs;
			
			PCT_PSPs_detected_by_Agent_at_250 = countBannedPSPs_detectedByAgent*100/countBannedPSPs;
			PCT_SPs_detected_by_Agent_at_250 = countBannedSPs_detectedByAgent*100/countBannedSPs;
			PCT_MPSPs_detected_by_Agent_at_250 = countBannedMPSPs_detectedByAgent*100/countBannedMPSPs;
			PCT_MSPs_detected_by_Agent_at_250 = countBannedMSPs_detectedByAgent*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_Agent_at_250 = countBannedPopularMPSPs_detectedByAgent*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_Agent_at_250 = countBannedPopularMSPs_detectedByAgent*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_Agent_at_250 = countBannedUnpopularMPSPs_detectedByAgent*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_Agent_at_250 = countBannedUnpopularMSPs_detectedByAgent*100/countBannedUnpopularMSPs;
			
			PCT_PSPs_detected_by_Total_Rank_at_250 = countBannedPSPs_detectedByTotalRank*100/countBannedPSPs;
			PCT_SPs_detected_by_Total_Rank_at_250 = countBannedSPs_detectedByTotalRank*100/countBannedSPs;
			PCT_MPSPs_detected_by_Total_Rank_at_250 = countBannedMPSPs_detectedByTotalRank*100/countBannedMPSPs;
			PCT_MSPs_detected_by_Total_Rank_at_250 = countBannedMSPs_detectedByTotalRank*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_Total_Rank_at_250 = countBannedPopularMPSPs_detectedByTotalRank*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_Total_Rank_at_250 = countBannedPopularMSPs_detectedByTotalRank*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_Total_Rank_at_250 = countBannedUnpopularMPSPs_detectedByTotalRank*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_Total_Rank_at_250 = countBannedUnpopularMSPs_detectedByTotalRank*100/countBannedUnpopularMSPs;

			
			break;
		
		case 375:
			
			PCT_PSPs_detected_by_TLRavg_Rank_at_375 = countBannedPSPs_detectedByTLRavg*100/countBannedPSPs;
			PCT_SPs_detected_by_TLRavg_Rank_at_375 = countBannedSPs_detectedByTLRavg*100/countBannedSPs;
			PCT_MPSPs_detected_by_TLRavg_Rank_at_375 = countBannedMPSPs_detectedByTLRavg*100/countBannedMPSPs;
			PCT_MSPs_detected_by_TLRavg_Rank_at_375 = countBannedMSPs_detectedByTLRavg*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_375 = countBannedPopularMPSPs_detectedByTLRavg*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_TLRavg_Rank_at_375 = countBannedPopularMSPs_detectedByTLRavg*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_375 = countBannedUnpopularMPSPs_detectedByTLRavg*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_375 = countBannedUnpopularMSPs_detectedByTLRavg*100/countBannedUnpopularMSPs;
			
			PCT_PSPs_detected_by_TST_Rank_at_375 = countBannedPSPs_detectedByTST*100/countBannedPSPs;
			PCT_SPs_detected_by_TST_Rank_at_375 = countBannedSPs_detectedByTST*100/countBannedSPs;
			PCT_MPSPs_detected_by_TST_Rank_at_375 = countBannedMPSPs_detectedByTST*100/countBannedMPSPs;
			PCT_MSPs_detected_by_TST_Rank_at_375 = countBannedMSPs_detectedByTST*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_TST_Rank_at_375 = countBannedPopularMPSPs_detectedByTST*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_TST_Rank_at_375 = countBannedPopularMSPs_detectedByTST*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_TST_Rank_at_375 = countBannedUnpopularMPSPs_detectedByTST*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_TST_Rank_at_375 = countBannedUnpopularMSPs_detectedByTST*100/countBannedUnpopularMSPs;
						
			PCT_PSPs_detected_by_TGTg_Rank_at_375 = countBannedPSPs_detectedByTGTg*100/countBannedPSPs;
			PCT_SPs_detected_by_TGTg_Rank_at_375 = countBannedSPs_detectedByTGTg*100/countBannedSPs;
			PCT_MPSPs_detected_by_TGTg_Rank_at_375 = countBannedMPSPs_detectedByTGTg*100/countBannedMPSPs;
			PCT_MSPs_detected_by_TGTg_Rank_at_375 = countBannedMSPs_detectedByTGTg*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_TGTg_Rank_at_375 = countBannedPopularMPSPs_detectedByTGTg*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_TGTg_Rank_at_375 = countBannedPopularMSPs_detectedByTGTg*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_375 = countBannedUnpopularMPSPs_detectedByTGTg*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_375 = countBannedUnpopularMSPs_detectedByTGTg*100/countBannedUnpopularMSPs;
			
			PCT_PSPs_detected_by_Colluding_at_375 = countBannedPSPs_detectedByColluding*100/countBannedPSPs;
			PCT_SPs_detected_by_Colluding_at_375 = countBannedSPs_detectedByColluding*100/countBannedSPs;
			PCT_MPSPs_detected_by_Colluding_at_375 = countBannedMPSPs_detectedByColluding*100/countBannedMPSPs;
			PCT_MSPs_detected_by_Colluding_at_375 = countBannedMSPs_detectedByColluding*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_Colluding_at_375 = countBannedPopularMPSPs_detectedByColluding*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_Colluding_at_375 = countBannedPopularMSPs_detectedByColluding*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_Colluding_at_375 = countBannedUnpopularMPSPs_detectedByColluding*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_Colluding_at_375 = countBannedUnpopularMSPs_detectedByColluding*100/countBannedUnpopularMSPs;
			
			PCT_PSPs_detected_by_weakColluding_at_375 = countBannedPSPs_detectedByWeakColluding*100/countBannedPSPs;
			PCT_SPs_detected_by_weakColluding_at_375 = countBannedSPs_detectedByWeakColluding*100/countBannedSPs;
			PCT_MPSPs_detected_by_weakColluding_at_375 = countBannedMPSPs_detectedByWeakColluding*100/countBannedMPSPs;
			PCT_MSPs_detected_by_weakColluding_at_375 = countBannedMSPs_detectedByWeakColluding*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_weakColluding_at_375 = countBannedPopularMPSPs_detectedByWeakColluding*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_weakColluding_at_375 = countBannedPopularMSPs_detectedByWeakColluding*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_weakColluding_at_375 = countBannedUnpopularMPSPs_detectedByWeakColluding*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_weakColluding_at_375 = countBannedUnpopularMSPs_detectedByWeakColluding*100/countBannedUnpopularMSPs;
			
			PCT_PSPs_detected_by_Agent_at_375 = countBannedPSPs_detectedByAgent*100/countBannedPSPs;
			PCT_SPs_detected_by_Agent_at_375 = countBannedSPs_detectedByAgent*100/countBannedSPs;
			PCT_MPSPs_detected_by_Agent_at_375 = countBannedMPSPs_detectedByAgent*100/countBannedMPSPs;
			PCT_MSPs_detected_by_Agent_at_375 = countBannedMSPs_detectedByAgent*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_Agent_at_375 = countBannedPopularMPSPs_detectedByAgent*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_Agent_at_375 = countBannedPopularMSPs_detectedByAgent*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_Agent_at_375 = countBannedUnpopularMPSPs_detectedByAgent*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_Agent_at_375 = countBannedUnpopularMSPs_detectedByAgent*100/countBannedUnpopularMSPs;
			
			PCT_PSPs_detected_by_Total_Rank_at_375 = countBannedPSPs_detectedByTotalRank*100/countBannedPSPs;
			PCT_SPs_detected_by_Total_Rank_at_375 = countBannedSPs_detectedByTotalRank*100/countBannedSPs;
			PCT_MPSPs_detected_by_Total_Rank_at_375 = countBannedMPSPs_detectedByTotalRank*100/countBannedMPSPs;
			PCT_MSPs_detected_by_Total_Rank_at_375 = countBannedMSPs_detectedByTotalRank*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_Total_Rank_at_375 = countBannedPopularMPSPs_detectedByTotalRank*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_Total_Rank_at_375 = countBannedPopularMSPs_detectedByTotalRank*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_Total_Rank_at_375 = countBannedUnpopularMPSPs_detectedByTotalRank*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_Total_Rank_at_375 = countBannedUnpopularMSPs_detectedByTotalRank*100/countBannedUnpopularMSPs;

			
			break;
		
		case 500:
			
			PCT_PSPs_detected_by_TLRavg_Rank_at_500 = countBannedPSPs_detectedByTLRavg*100/countBannedPSPs;
			PCT_SPs_detected_by_TLRavg_Rank_at_500 = countBannedSPs_detectedByTLRavg*100/countBannedSPs;
			PCT_MPSPs_detected_by_TLRavg_Rank_at_500 = countBannedMPSPs_detectedByTLRavg*100/countBannedMPSPs;
			PCT_MSPs_detected_by_TLRavg_Rank_at_500 = countBannedMSPs_detectedByTLRavg*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_500 = countBannedPopularMPSPs_detectedByTLRavg*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_TLRavg_Rank_at_500 = countBannedPopularMSPs_detectedByTLRavg*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_500 = countBannedUnpopularMPSPs_detectedByTLRavg*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_500 = countBannedUnpopularMSPs_detectedByTLRavg*100/countBannedUnpopularMSPs;
			
			PCT_PSPs_detected_by_TST_Rank_at_500 = countBannedPSPs_detectedByTST*100/countBannedPSPs;
			PCT_SPs_detected_by_TST_Rank_at_500 = countBannedSPs_detectedByTST*100/countBannedSPs;
			PCT_MPSPs_detected_by_TST_Rank_at_500 = countBannedMPSPs_detectedByTST*100/countBannedMPSPs;
			PCT_MSPs_detected_by_TST_Rank_at_500 = countBannedMSPs_detectedByTST*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_TST_Rank_at_500 = countBannedPopularMPSPs_detectedByTST*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_TST_Rank_at_500 = countBannedPopularMSPs_detectedByTST*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_TST_Rank_at_500 = countBannedUnpopularMPSPs_detectedByTST*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_TST_Rank_at_500 = countBannedUnpopularMSPs_detectedByTST*100/countBannedUnpopularMSPs;
						
			PCT_PSPs_detected_by_TGTg_Rank_at_500 = countBannedPSPs_detectedByTGTg*100/countBannedPSPs;
			PCT_SPs_detected_by_TGTg_Rank_at_500 = countBannedSPs_detectedByTGTg*100/countBannedSPs;
			PCT_MPSPs_detected_by_TGTg_Rank_at_500 = countBannedMPSPs_detectedByTGTg*100/countBannedMPSPs;
			PCT_MSPs_detected_by_TGTg_Rank_at_500 = countBannedMSPs_detectedByTGTg*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_TGTg_Rank_at_500 = countBannedPopularMPSPs_detectedByTGTg*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_TGTg_Rank_at_500 = countBannedPopularMSPs_detectedByTGTg*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_500 = countBannedUnpopularMPSPs_detectedByTGTg*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_500 = countBannedUnpopularMSPs_detectedByTGTg*100/countBannedUnpopularMSPs;
			
			PCT_PSPs_detected_by_Colluding_at_500 = countBannedPSPs_detectedByColluding*100/countBannedPSPs;
			PCT_SPs_detected_by_Colluding_at_500 = countBannedSPs_detectedByColluding*100/countBannedSPs;
			PCT_MPSPs_detected_by_Colluding_at_500 = countBannedMPSPs_detectedByColluding*100/countBannedMPSPs;
			PCT_MSPs_detected_by_Colluding_at_500 = countBannedMSPs_detectedByColluding*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_Colluding_at_500 = countBannedPopularMPSPs_detectedByColluding*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_Colluding_at_500 = countBannedPopularMSPs_detectedByColluding*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_Colluding_at_500 = countBannedUnpopularMPSPs_detectedByColluding*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_Colluding_at_500 = countBannedUnpopularMSPs_detectedByColluding*100/countBannedUnpopularMSPs;
			
			PCT_PSPs_detected_by_weakColluding_at_500 = countBannedPSPs_detectedByWeakColluding*100/countBannedPSPs;
			PCT_SPs_detected_by_weakColluding_at_500 = countBannedSPs_detectedByWeakColluding*100/countBannedSPs;
			PCT_MPSPs_detected_by_weakColluding_at_500 = countBannedMPSPs_detectedByWeakColluding*100/countBannedMPSPs;
			PCT_MSPs_detected_by_weakColluding_at_500 = countBannedMSPs_detectedByWeakColluding*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_weakColluding_at_500 = countBannedPopularMPSPs_detectedByWeakColluding*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_weakColluding_at_500 = countBannedPopularMSPs_detectedByWeakColluding*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_weakColluding_at_500 = countBannedUnpopularMPSPs_detectedByWeakColluding*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_weakColluding_at_500 = countBannedUnpopularMSPs_detectedByWeakColluding*100/countBannedUnpopularMSPs;
			
			PCT_PSPs_detected_by_Agent_at_500 = countBannedPSPs_detectedByAgent*100/countBannedPSPs;
			PCT_SPs_detected_by_Agent_at_500 = countBannedSPs_detectedByAgent*100/countBannedSPs;
			PCT_MPSPs_detected_by_Agent_at_500 = countBannedMPSPs_detectedByAgent*100/countBannedMPSPs;
			PCT_MSPs_detected_by_Agent_at_500 = countBannedMSPs_detectedByAgent*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_Agent_at_500 = countBannedPopularMPSPs_detectedByAgent*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_Agent_at_500 = countBannedPopularMSPs_detectedByAgent*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_Agent_at_500 = countBannedUnpopularMPSPs_detectedByAgent*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_Agent_at_500 = countBannedUnpopularMSPs_detectedByAgent*100/countBannedUnpopularMSPs;
			
			PCT_PSPs_detected_by_Total_Rank_at_500 = countBannedPSPs_detectedByTotalRank*100/countBannedPSPs;
			PCT_SPs_detected_by_Total_Rank_at_500 = countBannedSPs_detectedByTotalRank*100/countBannedSPs;
			PCT_MPSPs_detected_by_Total_Rank_at_500 = countBannedMPSPs_detectedByTotalRank*100/countBannedMPSPs;
			PCT_MSPs_detected_by_Total_Rank_at_500 = countBannedMSPs_detectedByTotalRank*100/countBannedMSPs;
			PCT_Popular_MPSPs_detected_by_Total_Rank_at_500 = countBannedPopularMPSPs_detectedByTotalRank*100/countBannedPopularMPSPs;
			PCT_Popular_MSPs_detected_by_Total_Rank_at_500 = countBannedPopularMSPs_detectedByTotalRank*100/countBannedPopularMSPs;
			PCT_Unpopular_MPSPs_detected_by_Total_Rank_at_500 = countBannedUnpopularMPSPs_detectedByTotalRank*100/countBannedUnpopularMPSPs;
			PCT_Unpopular_MSPs_detected_by_Total_Rank_at_500 = countBannedUnpopularMSPs_detectedByTotalRank*100/countBannedUnpopularMSPs;

			
			break;
			
		default:
		
			System.out.println("updateDetectroList unkown cycle: "+cycle);
			break;
	}
	}
	
	public void updateAvgTimeToResolveCase(int cycle, boolean countAgent)
	{
		double sumClosedCases = 0;
		double sumCases = 0;
		double sumPeriodToClose = 0;
		
		for(Auditor_UsersNodesDir user: uNodes)
		{
			User_Node userNode = (User_Node) Network.get(user.getPosition()).getProtocol(UserPid);
			
			if(!countAgent && (userNode.isSTagent() || userNode.isGTagent())) continue;
			
			List<CaseLogs> caseLogs = user.getCaseLogs();
			for(CaseLogs logs: caseLogs)
			{
				sumCases++;
				
				if(logs.isClosed())
				{
					sumClosedCases++;
					sumPeriodToClose = sumPeriodToClose + logs.getPeriodToCloseCase();
				}
			}
		}
		
		switch(cycle){
			case 125:
				AvgTimeToResolveCase_at_125 = sumPeriodToClose/sumClosedCases;
				PCT_Open_Cases_at_125 = (sumCases-sumClosedCases)*100/sumCases;
				break;
			case 250:
				AvgTimeToResolveCase_at_250 = sumPeriodToClose/sumClosedCases;
				PCT_Open_Cases_at_250 = (sumCases-sumClosedCases)*100/sumCases;
				break;
			case 375:
				AvgTimeToResolveCase_at_375 = sumPeriodToClose/sumClosedCases;
				PCT_Open_Cases_at_375 = (sumCases-sumClosedCases)*100/sumCases;
				break;
			case 500:
				AvgTimeToResolveCase_at_500 = sumPeriodToClose/sumClosedCases;
				PCT_Open_Cases_at_500 = (sumCases-sumClosedCases)*100/sumCases;
				break;
			default:
					System.out.println("updateAvgTimeToResolve case unkown cycle: "+cycle);
				break;
		}
	}
	
	public void updateBannedSPsList()
	{
		for(Auditor_SPsNodesDir suspiciousNode: suspiciousSPNodes)
		{	
			if(suspiciousNode.isBanned()) continue;
			
			boolean colluding = false;
			
			if(suspiciousNode.getTGTColludingRank() <= suspiciousSPBanningRank && suspiciousNode.getTGTColludingRank() >= 0)
				colluding = true;
			
			if( (suspiciousNode.getRank() <= suspiciousSPBanningRank) || colluding )
			{
				double tlrusFound = 0;
				
				if(!colluding)
				{
					boolean sufficientTLRusFound = false;
					
					for(TLRavgRank tlravgRank: TLRavg)
					{	
						
						tlrusFound = tlravgRank.getTLRUCount();
						
						if(tlravgRank.getSPid() == suspiciousNode.getPosition())
						{
							if(suspiciousNode.getSpUsefulness() < this.usefulnessMIN && tlravgRank.getTLRUCount() >= SufficientTLRus_SP)
								sufficientTLRusFound = true;
							
							else if(suspiciousNode.getSpUsefulness() >= this.usefulnessMIN && tlravgRank.getTLRUCount() >= SufficientTLRus_PSP)
								sufficientTLRusFound = true;
							
							break;
						}
					}
					
					if(!sufficientTLRusFound)
					{
						//System.out.println("SPid: "+suspiciousNode.getPosition()+" is to be banned now But !sufficientTLRusFound; TLRusFound == "+tlrusFound);
					
						continue;
					}
					/*
					else
					{
						System.out.println("sufficientTLRusFound!!");
						System.out.println("SPid: "+suspiciousNode.getPosition()+" is to be banned now unless if it is not STtested yet!!");
						//System.out.println("SPid: "+suspiciousNode.getPosition()+" isSTTested? "+suspiciousNode.isStTested()+" Rank: "+suspiciousNode.getRank());
						//System.out.println("SPid: "+suspiciousNode.getPosition()+" tlruCount: "+tlruCount);
					}
					*/
				}
				
				for(TGRRank rank: TGR)
				{
					if(rank.getSPid() == suspiciousNode.getPosition())
					{	
					//	System.out.println("Banning SPid: "+rank.getSPid()+" isBanned? "+rank.isBanned()+" TGTColludingRank = "+rank.getTGTColludingRank()+" TGTWeakColludingRank = "+rank.getTGTWeakColludingRank()+" rank: "+rank.getRank()+" TGTrank: "+rank.getTGTRank()+" TSTrank: "+rank.getTSTRank()+" colluding? "+colluding+" suspiciousNode.getTGTColludingRank() >= 0 : "+(suspiciousNode.getTGTColludingRank() >= 0));
					//	System.out.println("--Banning SPid: "+suspiciousNode.getPosition()+" isBanned? "+suspiciousNode.isBanned()+" TGTColludingRank = "+suspiciousNode.getTGTColludingRank()+" rank: "+suspiciousNode.getRank()+" TGTrank: "+suspiciousNode.getTGTrank()+" TSTrank: "+suspiciousNode.getTSTrank()+" colluding? "+colluding+" suspiciousNode.getTGTColludingRank() >= 0 : "+(suspiciousNode.getTGTColludingRank() >= 0));
				
						//boolean stAllowsBanning = !DeployST || suspiciousNode.isStTested();
						
						if(suspiciousNode.getTSTrank() == 0 || suspiciousNode.getRank() <= suspiciousSPBanningRank || suspiciousNode.getTGTColludingRank() <= suspiciousSPBanningRank)
						{
							if(suspiciousNode.getTSTrank() == 0)
								preBanProcess(rank.getSPid(), 2);
							
							else if(suspiciousNode.getTGTrank() <= suspiciousSPBanningRank && suspiciousNode.getTGTrank() >= 0)
								preBanProcess(rank.getSPid(), 3);
							
							else if(suspiciousNode.getTGTWeakColludingRank() <= suspiciousSPBanningRank && suspiciousNode.getTGTWeakColludingRank() >= 0)
								preBanProcess(rank.getSPid(), 5);
							
							else if(rank.getTLRavgRank() <= suspiciousSPBanningRank && rank.getTLRavgRank() >= 0)
								preBanProcess(rank.getSPid(), 2);
							
							else if(colluding)
								preBanProcess(rank.getSPid(), 4);
							
							else
								preBanProcess(rank.getSPid(), 7);
						}	
						
						break;
					}
				}	
				
			}
		}
	}
	
	// detectorType: 0: Voluntarily; 1: TLRavg_Rank; 2: TST_Rank; 3: TGTg_Rank; 4: colluding; 5: weakColluding; 6: ST/GT_Agent; 7: totalRank
	public void preBanProcess(int SPid, int detectorType)
	{
		//System.out.println("PrebanProcess for SPid: "+SPid+" detectorType: "+detectorType);
		
		//outputBanDetector(SPid, detectorType);

		SP_Node toBeBannedSP = (SP_Node) Network.get(SPid).getProtocol(this.SPPid);
		
		TGRRank rank = new TGRRank();
		
		for(TGRRank tempRank: TGR)
			if(tempRank.getSPid() == SPid)
			{
				rank = tempRank;
				break;
			}
		
		
		boolean ban = true;
		
		if(!toBeBannedSP.isInstalledDGU() && DeployDGU)							
		{
			boolean acceptedDGUoffer = toBeBannedSP.acceptDGUoffer(false);
			ban = !acceptedDGUoffer;
			
			if(acceptedDGUoffer)
			{
				updateSPnodeStatus(SPid, 1, detectorType);
				//toBeBannedSP.setInstallDGUdetectorType(detectorType);
				rank.setInstallDGUdetectorType(detectorType);
				//System.out.println("SPid: "+SPid+" accepted DGU Offer!");
			}
			//else
				//System.out.println("SPid: "+SPid+" rejected DGU Offer!");
		}
		else if(!toBeBannedSP.isEnabledStrictDGU() && DeployDGU)
		{
			boolean acceptedStrictDGUoffer = toBeBannedSP.acceptStrictDGUoffer(false);
			ban = !acceptedStrictDGUoffer;
			
			if(acceptedStrictDGUoffer)
			{
				updateSPnodeStatus(SPid,2, detectorType);
				//toBeBannedSP.setEnableStrictDGUdetectorType(detectorType);
				rank.setEnableStrictDGUdetectorType(detectorType);
			//	System.out.println("SPid: "+SPid+" accepted Strict DGU Offer!");
			}
		//	else
			//	System.out.println("SPid: "+SPid+" rejected Strict DGU Offer!");
			
		}
		
		if(ban)
			banSP(SPid, detectorType);
	}
	
	// detectorType: 1: TLRavg_Rank; 2: TST_Rank; 3: TGTg_Rank; 4: colluding; 5: weakColluding; 6: ST/GT_Agent; 7: totalRank
	public void banSP(int SPid, int detectorType)
	{
		//outputBanDetector(SPid, detectorType);
		
		TGRRank rank = new TGRRank();
		for(TGRRank tempRank: TGR)
			if(tempRank.getSPid() == SPid)
			{
				rank = tempRank;
				break;
			}
		
		//Kill ST agents containing the Suspicious SP
		if(rank.getSTLocator() >= 0)
		{
			Auditor_STNodesDir stNode = stNodes.get(rank.getSTLocator());
			
			if(stNode.getSTstatus() == 2)
				killSTagent(rank.getSPid(), false);
		}
		
		//Kill GT agents containing the Suspicious SP
		for(Auditor_GTNodesDir gtNode: gtNodes)
			if(gtNode.getGTstatus() == 2)
			for(GT_TargetedSP targetedSP: gtNode.getTargetedSPs())
			{
				if(targetedSP.getSPid() == rank.getSPid())
				{
					killGTagent(gtNode.getGTid());
					break;
				}
			}
						
		for(SPRrecord sprRecord: SPR)
		{
			if(sprRecord.getSPid() == SPid)
			{
				SPR.remove(sprRecord);
				break;
			}
		}
		
		for(int i = 0; i < PIILs.size(); i++)
		{
			PIILrecord piil = PIILs.get(i);
			
			boolean suspiciousFound = false;
			
			for(int sp: piil.getSPs())
			{
				if(sp == SPid)
				{
					suspiciousFound = true;
					break;
				}
			}
			
			if(suspiciousFound) 
				PIILs.remove(piil);
		}
		
		for(IILrecord iil: IIR)
			for(int sp: iil.getSPs())
				if(sp == SPid)
				{
					iil.setIgnore(true);
					break;
				}
		
		//Ignore TGTg's containing the Suspicious SP
		if(ignoreOldGranks)
		for(TGTgRank tgtRank: TGTg)
		{
			if(tgtRank.containSP(rank.getSPid()))
			{
				tgtRank.setIgnore(true);
			}
		}
		
		//Ignore TGTg's containing the Suspicious SP
		if(ignoreOldGranks)
		for(TGTgRank tgtRank: TGTg_allCredential)
		{
			if(tgtRank.containSP(rank.getSPid()))
			{
				tgtRank.setIgnore(true);
			}
		}

		boolean suspiciousSPFound = false;
		
		for(Auditor_SPsNodesDir suspiciousNode: suspiciousSPNodes)
		{
			if(suspiciousNode.getPosition() == SPid)
			{
				suspiciousNode.setBanned(true);
				suspiciousNode.setBanDetectorType(detectorType);
				bannedSPNodes.add(suspiciousNode);
				suspiciousSPFound = true;
				break;
			}
		}
		
		if(!suspiciousSPFound)
		{
			Auditor_SPsNodesDir suspiciousNode = new Auditor_SPsNodesDir();
			suspiciousNode.setBanned(true);
			suspiciousNode.setBanDetectorType(detectorType);
			suspiciousNode.setInstallDGUdetectorType(rank.getInstallDGUdetectorType());
			suspiciousNode.setEnableStrictDGUdetectorType(rank.getEnableStrictDGUdetectorType());
			suspiciousNode.setEnabledStrictDGU(rank.isEnabledStrictDGU());
			suspiciousNode.setEnabledStrictDGUCycle(rank.getEnabledStrictDGUCycle());
			suspiciousNode.setInstalledDGU(rank.isInstalledDGU());
			suspiciousNode.setInstalledDGUCycle(rank.getInstalledDGYCycle());
			suspiciousNode.setPosition(SPid);
			suspiciousNode.setRank(rank.getRank());
			suspiciousNode.setSpUsefulness(rank.getSpUsefulness());
			suspiciousNode.setTGTColludingRank(rank.getTGTColludingRank());
			suspiciousNode.setTGTrank(rank.getTGTRank());
			suspiciousNode.setTGTWeakColludingRank(rank.getTGTWeakColludingRank());
			suspiciousNode.setTSTrank(rank.getTSTRank());

			suspiciousSPNodes.add(suspiciousNode);
			bannedSPNodes.add(suspiciousNode);
		}
		
		
		rank.setBanned(true);	
		rank.setBanDetectorType(detectorType);
		IDP_Node idp = (IDP_Node) Network.get(1).getProtocol(IDPPid);
		idp.removeMaliciousSP(SPid, detectorType);

	}
	
	private void outputBanDetector(int SPid, int detectorType)
	{
		switch(detectorType){
		case 1:
			System.out.println("PreBan: TLRavg_Rank detector; SPid: "+SPid);
			break;
			
		case 2:
			System.out.println("PreBan: TST_Rank detector; SPid: "+SPid);
			break;
		
		case 3:
			System.out.println("PreBan: TGTg_Rank detector; SPid: "+SPid);
			break;
			
		case 4:
			System.out.println("PreBan: Colluding detector; SPid: "+SPid);
			break;
			
		case 5:
			System.out.println("PreBan: weakColluding detector; SPid: "+SPid);
			break;
			
		case 6:
			System.out.println("PreBan: ST/GT_Agent detector; SPid: "+SPid);
			break;
			
		case 7:
			System.out.println("PreBan: totalRank detector; SPid: "+SPid);
			break;
	}
	}
	
	//Action: 1 = installDGU, 2 = enable StrictDGU, 3 = installDGU && enable StrictDGU
	public void updateSPnodeStatus(int SPid, int action, int detectorType)
	{		
		TGRRank rank = new TGRRank();
		boolean rankFound = false;
		Auditor_SPsNodesDir suspiciousNode = new Auditor_SPsNodesDir();
		boolean suspiciousFound = false;
		SPRrecord spr = new SPRrecord();
		boolean sprFound = false;
		TLRavgRank tlravgRank = new TLRavgRank();
		boolean tlravgRankFound = false;
		
		SP_Node spNode = (SP_Node) Network.get(SPid).getProtocol(this.SPPid);
		
		for(TGRRank tgrRank: TGR)
		{
			if(tgrRank.getSPid() == SPid)
			{				
				rankFound = true;
				
				rank = tgrRank;
			
				break;
			}
		}
		
		if(!rankFound)
		{
			rank.setSPid(SPid);
			rank.setSpUsefulness(spNode.getUsefulness());
			TGR.add(rank);
		}
		
		rank.setTGTColludingRank(-1);
		rank.setTGTRank(-1);
		rank.setTGTWeakColludingRank(-1);
		rank.setTLRavgRank(-1);
		rank.setTSTRank(-1);
		
		if(rank.getSTLocator() >= 0)
		{
			
			Auditor_STNodesDir stNode = stNodes.get(rank.getSTLocator());
			
			if(stNode.getSTstatus() == 2)
				killSTagent(rank.getSPid(), true);
		}
		
		//Kill GT agents containing the Suspicious SP
		for(Auditor_GTNodesDir gtNode: gtNodes)
			if(gtNode.getGTstatus() == 2)
				for(GT_TargetedSP targetedSP: gtNode.getTargetedSPs())
				{
					if(targetedSP.getSPid() == rank.getSPid())
					{
						killGTagent(gtNode.getGTid());
						break;
					}
				}
		
		for(TLRavgRank tlravg: TLRavg)
			if(tlravg.getSPid() == SPid)
			{
				tlravgRank = tlravg;
				tlravgRankFound = true;
				break;
			}
		
		for(TSTRank tst: TST)
			if(tst.getSPid() == SPid)
			{
				tst.getTstRanks().clear();
				break;
			}
		
		for(int i = 0; i < TGTg.size(); i++)
		{
			TGTgRank tgtgRank = TGTg.get(i);
			
			tgtgRank.removeSP(SPid);
			
			if(tgtgRank.getSPs().size() == 0)
				TGTg.remove(tgtgRank);

		}	
		
		for(int i = 0; i < TGTg_allCredential.size(); i++)
		{
			TGTgRank tgtgRank = TGTg_allCredential.get(i);
			
			tgtgRank.removeSP(SPid);
			
			if(tgtgRank.getSPs().size() == 0)
				TGTg.remove(tgtgRank);

		}	
		
		for(Auditor_SPsNodesDir node: suspiciousSPNodes)
			if(node.getPosition() == SPid)
			{
				suspiciousNode = node;
				
				suspiciousNode.setRank(rank.getRank());
				suspiciousNode.setTGTColludingRank(-1);
				suspiciousNode.setTGTrank(-1);
				suspiciousNode.setTGTWeakColludingRank(-1);
				suspiciousNode.setTSTrank(-1);
				
				suspiciousFound = true;
				break;
			}
		
		if(!suspiciousFound)
		{
			suspiciousNode.setPosition(SPid);
			suspiciousNode.setSpUsefulness(rank.getSpUsefulness());
			suspiciousNode.setRank(rank.getRank());
			suspiciousNode.setTGTColludingRank(-1);
			suspiciousNode.setTGTrank(-1);
			suspiciousNode.setTGTWeakColludingRank(-1);
			suspiciousNode.setTSTrank(-1);
			suspiciousSPNodes.add(suspiciousNode);
		}
		
		for(SPRrecord sprrecord: SPR)
			if(sprrecord.getSPid() == SPid)
			{
				spr = sprrecord;
				sprFound = true;
				break;
			}
		
		if(action == 1)
		{
			spNode.setInstallDGUdetectorType(detectorType);
			
			rank.setInstalledDGU(true);
			rank.setInstalledDGYCycle(CommonState.getIntTime());
			rank.setRank(postDGUinstallationRank);
			rank.setInstallDGUdetectorType(detectorType);
			
			if(sprFound)
			{
				spr.setInstalledDGU(true);
				spr.setInstalledDGYCycle(CommonState.getIntTime());
			}
			
			if(tlravgRankFound)
			{
				tlravgRank.setInstalledDGU(true);
				tlravgRank.setInstalledDGYCycle(CommonState.getIntTime());
			}

			suspiciousNode.setInstalledDGU(true);
			suspiciousNode.setInstallDGUdetectorType(detectorType);
			suspiciousNode.setInstalledDGUCycle(CommonState.getIntTime());
			DGUSPNodes.add(suspiciousNode);
		}
	
		else if(action == 2)
		{
			spNode.setEnableStrictDGUdetectorType(detectorType);
			
			rank.setEnabledStrictDGU(true);
			rank.setEnabledStrictDGUCycle(CommonState.getIntTime());
			rank.setRank(postStrictDGUEnableRank);
			rank.setEnableStrictDGUdetectorType(detectorType);
			
			if(sprFound)
			{
				spr.setEnabledStrictDGU(true);
				spr.setEnabledStrictDGUCycle(CommonState.getIntTime());
			}
			
			if(tlravgRankFound)
			{
				tlravgRank.setEnabledStrictDGU(true);
				tlravgRank.setEnabledStrictDGUCycle(CommonState.getIntTime());
			}
			
			suspiciousNode.setEnabledStrictDGU(true);
			suspiciousNode.setEnabledStrictDGUCycle(CommonState.getIntTime());
			suspiciousNode.setEnableStrictDGUdetectorType(detectorType);

			for(Auditor_SPsNodesDir DGUnode: DGUSPNodes)
			{
				if(DGUnode.getPosition() == rank.getSPid())
				{
					DGUnode.setEnabledStrictDGU(true);
					DGUnode.setEnabledStrictDGUCycle(CommonState.getIntTime());
					DGUnode.setEnableStrictDGUdetectorType(detectorType);
					break;
				}
			}
			
			for(int i = 0; i < PIILs.size(); i++)
			{
				PIILrecord piil = PIILs.get(i);	
					
				boolean spFound = false;
				
				for(int sp: piil.getSPs())
				{
					if(sp == SPid)
					{
						spFound = true;
						break;
					}
				}
				
				if(spFound)
					PIILs.remove(i);		
			}
			
			for(TGTgRank tgtgRank: TGTg)
				if(tgtgRank.containSP(SPid))
					tgtgRank.setIgnore(true);
			
		}
		
		else if(action == 3)
		{
			spNode.setInstallDGUdetectorType(detectorType);
			spNode.setEnableStrictDGUdetectorType(detectorType);
			
			rank.setInstalledDGU(true);
			rank.setInstalledDGYCycle(CommonState.getIntTime());
			rank.setInstallDGUdetectorType(detectorType);
			rank.setEnabledStrictDGU(true);
			rank.setEnabledStrictDGUCycle(CommonState.getIntTime());
			rank.setEnableStrictDGUdetectorType(detectorType);
			rank.setRank(postStrictDGUEnableRank);
			
			if(sprFound)
			{
				spr.setInstalledDGU(true);
				spr.setInstalledDGYCycle(CommonState.getIntTime());
				spr.setEnabledStrictDGU(true);
				spr.setEnabledStrictDGUCycle(CommonState.getIntTime());
			}
			
			if(tlravgRankFound)
			{
				tlravgRank.setInstalledDGU(true);
				tlravgRank.setInstalledDGYCycle(CommonState.getIntTime());
				tlravgRank.setEnabledStrictDGU(true);
				tlravgRank.setEnabledStrictDGUCycle(CommonState.getIntTime());
			}
			
			for(PIILrecord piil: PIILs)
				for(int sp: piil.getSPs())
					if(sp == SPid)
					{
						PIILs.remove(piil);
						break;
					}
			
			for(TGTgRank tgtgRank: TGTg)
				if(tgtgRank.containSP(SPid))
					tgtgRank.setIgnore(true);

			suspiciousNode.setInstalledDGU(true);
			suspiciousNode.setInstalledDGUCycle(CommonState.getIntTime());
			suspiciousNode.setInstallDGUdetectorType(detectorType);
			suspiciousNode.setEnabledStrictDGU(true);
			suspiciousNode.setEnabledStrictDGUCycle(CommonState.getIntTime());
			suspiciousNode.setEnableStrictDGUdetectorType(detectorType);
			DGUSPNodes.add(suspiciousNode);
		}
	
	}
	
	public void reportSpam(CaseLogs caseLogs, List<CaseSPs> PSPsInteractedWith)
	{	
		for(Log log: caseLogs.getLogs())
		{
			boolean sprFound = false;
			
			for(SPRrecord sprRecord: SPR)
			{
				if(sprRecord.getSPid() == log.getSpPosition())
				{
					sprRecord.setCounter(sprRecord.getCounter()+1);
					sprFound = true;
					break;
				}
			}
			
			if(!sprFound)
			{
				SPRrecord sprRecord = new SPRrecord();
				sprRecord.setSPid(log.getSpPosition());
				sprRecord.setSpUsefulness(log.getSpUsefulness());
				sprRecord.setCounter(1);
				
				for(Auditor_SPsNodesDir dguNode: DGUSPNodes)
					if(dguNode.getPosition() == sprRecord.getSPid())
					{
						sprRecord.setInstalledDGU(dguNode.isInstalledDGU());
						sprRecord.setInstalledDGYCycle(dguNode.getInstalledDGUCycle());
						sprRecord.setEnabledStrictDGU(dguNode.isEnabledStrictDGU());
						sprRecord.setEnabledStrictDGUCycle(dguNode.getInstalledDGUCycle());
						break;
					}
				
				SPR.add(sprRecord);
			}
		}
		
		boolean userFound = false;

		for(Auditor_UsersNodesDir uNode: uNodes)
			if(uNode.getPosition() == caseLogs.getUserPos())
			{
				uNode.getCaseLogs().add(caseLogs);
				userFound = true;
				break;
			}
		
		if(!userFound)
		{
			Auditor_UsersNodesDir uNode = new Auditor_UsersNodesDir();
			uNode.setPosition(caseLogs.getUserPos());
			uNode.getCaseLogs().add(caseLogs);
			uNodes.add(uNode);
			
			boolean gAllCredentialFound = false;
			
			TGTgRank newGrankAllCredential = new TGTgRank();

			if(PSPsInteractedWith.size() > 0)
			{
				boolean ignoreFound = false;
				
				for(CaseSPs targetedSP: PSPsInteractedWith)
					for(Auditor_SPsNodesDir dguNode: DGUSPNodes)
						if(dguNode.getPosition() == targetedSP.getSPid())					
							if(dguNode.isEnabledStrictDGU() || (dguNode.isInstalledDGU() && dguNode.getInstalledDGUCycle() >= targetedSP.getCycle()))
							{
								ignoreFound = true;
								break;
							}
						
				if(!ignoreFound)
				{
					for(TGTgRank gRank: TGTg_allCredential)
						if(equalTargetedSPs_int_CaseSPs(gRank.getSPs(), PSPsInteractedWith))
						{
							gRank.incrementCounter();		
											
							gAllCredentialFound = true;
							break;
						}
					
					if(!gAllCredentialFound && PSPsInteractedWith.size() > 0)
					{	
						for(CaseSPs targetedSP: PSPsInteractedWith)
							newGrankAllCredential.getSPs().add(targetedSP.getSPid());
				
						newGrankAllCredential.incrementCounter();
						
						TGTg_allCredential.add(newGrankAllCredential);
					}
				}
			}
			
		}
		
		boolean iilFound = false;
		
		if(userFound)
		for(IILrecord iil: IIR)
		{
			if(iil.getUserID() == caseLogs.getUserPos() && iil.getCredentialID() == caseLogs.getCredential().getId())
			{
				iilFound = true;
				break;
			}
		}
		
		if(!iilFound)
		{		
			IILrecord iilRecord = new IILrecord();
			
			List<Integer> sps = new ArrayList<Integer>(caseLogs.getCaseSps().size());	
			List<Auditor_SPsNodesDir> caseSPs = caseLogs.getCaseSps();
			
			Integer tempSP, currentSP, prevSP;
			for(int i = 0; i < caseSPs.size(); i++)
			{	
				sps.add(caseSPs.get(i).getPosition());
				
				if(i > 0)
				{
					for(int j = i - 1; j >= 0; j--)
					{
						currentSP = sps.get(j+1);
						prevSP = sps.get(j);
						if(currentSP < prevSP)
						{
							tempSP = currentSP;
							sps.set(j+1, prevSP);
							sps.set(j, tempSP);
						}
						else if(currentSP == prevSP)
						{
							sps.remove(j+1);
							break;
						}
						else
							break;
					}
				}
			}
			
			iilRecord.setSPs(sps);
			iilRecord.setUserID(caseLogs.getUserPos());
			iilRecord.setCredentialID(caseLogs.getCredential().getId());
			iilRecord.setCreatedON(CommonState.getIntTime());
			
			IIR.add(iilRecord);
			
			/**
			 * Update TST Record 
			 */
			
			TST_Evaluator tstEvaluater = new TST_Evaluator();
			
			List<Aux_TSTrateRecord> tstRates = tstEvaluater.generateTSTRanks(caseLogs, this.SPPid);
			
			for(Aux_TSTrateRecord newRate: tstRates)
			{	
				boolean spFound = false;
				for(TSTRank oldRank: TST)
				{
					if(newRate.getSPid() == oldRank.getSPid())
					{
						oldRank.addTstRank(newRate.getRank());
						spFound = true;
						break;
					}
				}
				if(!spFound)
				{
					TSTRank newRank = new TSTRank();
					newRank.setSPid(newRate.getSPid());
					newRank.setSpUsefulness(newRate.getSpUsefulness());
					newRank.addTstRank(newRate.getRank());
					TST.add(newRank);
				}
			}

		}
		
	}
	
	public void runGPD_Evaluater()
	{
		GPD_Evaluator gpdEvaluater = new GPD_Evaluator();
		
		for(Auditor_UsersNodesDir uNode: uNodes)
		{
			for(CaseLogs caseLogs: uNode.getCaseLogs())
			{
				if(!caseLogs.isGPD_Evaluated())
				{
					List<GPD_GuiltRate> gpdRates = gpdEvaluater.generateGuiltRates(caseLogs, SPPid);
					caseLogs.setGPD_Evaluated(true);
					
					for(GPD_GuiltRate newRate: gpdRates)
					{	
						boolean spFound = false;
						for(TLRuRank oldRank: uNode.getTLRu())
						{
							if(newRate.getSPid() == oldRank.getSPid())
							{
								oldRank.setCasesCount(oldRank.getCasesCount()+1);
								oldRank.setGuiltRateSum(oldRank.getGuiltRateSum()+newRate.getGuiltRate());
								
								spFound = true;
								break;
							}
						}
						if(!spFound)
						{
							TLRuRank newRank = new TLRuRank();
							newRank.setCasesCount(1);
							newRank.setGuiltRateSum(newRate.getGuiltRate());
							newRank.setSPid(newRate.getSPid());
							newRank.setSpUsefulness(newRate.getSpUsefulness());
							uNode.getTLRu().add(newRank);
						}
					}
				}			
			}
		}
	}
	
	
	public void updateTLRu()
	{
		for(Auditor_UsersNodesDir uNode : uNodes)
		{
			for(TLRuRank newRank : uNode.getTLRu())
			{
				if(newRank.getCasesCount() == 0)
					newRank.setRank(100);
				else
					newRank.setRank( 100 - (newRank.getGuiltRateSum()/newRank.getCasesCount()) );	
			}
		}
	}
	
	public void updateTLRavg()
	{
		this.updateTLRu();
		
		Auditor_UsersNodesDir uNode;
		TLRuRank tlruRank;
		int spID;
		TLRavgRank tlravgRank;
		boolean spFound;
		
		//Reset TLRavg aux holders
		for(TLRavgRank rank: TLRavg)
		{
			rank.setTLRUCount(0);
			rank.setTLRuSum(0);
		}
		
		for(int i =0 ; i < uNodes.size(); i++)
		{
			uNode = uNodes.get(i);
			for(int j = 0; j < uNode.getTLRu().size(); j++)
			{
				tlruRank = uNode.getTLRu().get(j);
				spID = tlruRank.getSPid();
				spFound = false;
				for(int k = 0; k < TLRavg.size(); k++)
				{
					tlravgRank = TLRavg.get(k);
					if(spID == tlravgRank.getSPid())
					{
						tlravgRank.setTLRuSum(tlravgRank.getTLRuSum()+tlruRank.getRank());
						tlravgRank.setTLRUCount(tlravgRank.getTLRUCount()+1);
						spFound = true;
						break;
					}
				}
				if(!spFound)
				{
					//System.out.println("Adding new SP to TLRavg, SPid:  "+spID);
					tlravgRank = new TLRavgRank();
					tlravgRank.setSPid(spID);
					tlravgRank.setTLRuSum(tlruRank.getRank());
					tlravgRank.setTLRUCount(1);
					tlravgRank.setSpUsefulness(tlruRank.getSpUsefulness());
					TLRavg.add(tlravgRank);
				}
			}
		}
		
		TLRavgRank tempRank, currentRank, prevRank;
		for(int i = 0; i < TLRavg.size(); i++)
		{
			tlravgRank = TLRavg.get(i);
			
			if(tlravgRank.isEnabledStrictDGU())
				tlravgRank.setRank(postStrictDGUEnableRank);
			else if(tlravgRank.getTLRUCount() <= 0)
				tlravgRank.setRank(-1);
			else
				tlravgRank.setRank(tlravgRank.getTLRuSum()/tlravgRank.getTLRUCount());
			
			if(i > 0)
			{
				for(int j = i - 1; j >= 0; j--)
				{
					currentRank = TLRavg.get(j+1);
					prevRank = TLRavg.get(j);
					if(currentRank.getRank() > prevRank.getRank())
					{
						tempRank = currentRank.copy();
						TLRavg.set(j+1, prevRank.copy());
						TLRavg.set(j, tempRank.copy());
					}
					else
						break;
				}
			}
		}
	}

	public void updateTST()
	{
		if(!DeployST) return;
		
		//Manage currently Deployed STs
		for(Auditor_STNodesDir stNode: stNodes)
		{
			if(stNode.getSTstatus() == 2 && (CommonState.getTime() - stNode.getCreatedOn()) >= STmaxLifeTime)
			{
				//System.out.println("STagent: "+stNode.getNodePos()+" is retiring!");
				
				//User_Node agent = (User_Node) Network.get(stNode.getNodePos()).getProtocol(UserPid);
				
				recieveSTagentReport(stNode.getSPID(), stNode.getNodePos(), false);
				killSTagent(stNode.getSPID(), false);
			}
			
			for(Auditor_SPsNodesDir spNode: bannedSPNodes)
			{
				if(spNode.getPosition() == stNode.getSPID())
				{
					killSTagent(stNode.getSPID(), false);
					break;
				}
			}
		}
		
		//List of STs to be dynamically added in the next cycle
		List<Auditor_STNodesDir> sttoAdd = new ArrayList<Auditor_STNodesDir>(20);
		
		/**
		 * Select Suspicious Unbanned SPs for ST testing
		 */
		if(SuspiciousNodes_STselector)
		for(Auditor_SPsNodesDir suspiciousSP: suspiciousSPNodes)
		{
			if(!suspiciousSP.isBanned() && !suspiciousSP.isStTested() && !suspiciousSP.isEnabledStrictDGU() )
			{
				for(TGRRank tgrRank:TGR)
				{
					if(tgrRank.getSPid() == suspiciousSP.getPosition())
					{
						Auditor_STNodesDir newST = new Auditor_STNodesDir();
						
						//Add an ST
						if(tgrRank.getSTLocator() == -1 && !tgrRank.isBanned())
						{
							//System.out.println("New ST to be added for Suspicious SPid: "+tgrRank.getSPid()+" with tgrRank: "+tgrRank.getRank());
							
							newST.setSPID(tgrRank.getSPid());
							newST.setSTstatus(1);
							if(suspiciousSP.isInstalledDGU())
								newST.setDGUSPtargeted(true);
							
							sttoAdd.add(newST);
							
							tgrRank.setSTLocator(stNodes.size());
							suspiciousSP.setStTested(true);
						
							stNodes.add(newST);
						}
						
						break;
					}
				}
			}
		}
		
		/**
		 * Select top and/or bottom TLRavg for ST testing
		 */
		int bannedSPs = bannedSPNodes.size();
		
		TLRavgRank tlravgRank;
		TLRavgRank prevTlravgRank = new TLRavgRank();
		TGRRank tgrRank = new TGRRank();
		
		if(TopTLRavg_STselector)
		{
			double TopPoolSize = (TLRavg.size() - bannedSPs) * TopTLRavgPCT_ST/100; 
			if((TLRavg.size() - bannedSPs) >= 1 && TopPoolSize < 1) TopPoolSize = 1;
			
			int poolCount = 0;
			
			for(int i = 0; i < TLRavg.size(); i++)
			{
				tlravgRank = TLRavg.get(i);
				for(int j = 0; j < TGR.size() ; j++)
				{
					tgrRank = TGR.get(j);
					if(tgrRank.getSPid() == tlravgRank.getSPid())
						break;
				}
				
				if(poolCount >= TopPoolSize && tlravgRank.getRank() < prevTlravgRank.getRank())
				{
					break;
				}
				
				Auditor_STNodesDir newST = new Auditor_STNodesDir();
				
				//Add an ST
				if(tgrRank.getSTLocator() == -1 && !tgrRank.isBanned() && !tgrRank.isEnabledStrictDGU())
				{
				//	System.out.println("New ST to be added for: "+tgrRank.getSPid()+" with top tlravgRank: "+tlravgRank.getRank());
					
					newST.setSPID(tgrRank.getSPid());
					newST.setSTstatus(1);
					if(tgrRank.isInstalledDGU())
						newST.setDGUSPtargeted(true);
					
					sttoAdd.add(newST);
					
					tgrRank.setSTLocator(stNodes.size());
				
					stNodes.add(newST);
					
					poolCount++;
				}
				
				prevTlravgRank = tlravgRank;
			}
		}
		

		if(BottomTLRavg_STselector)
		{
			double BottomPoolSize = (TLRavg.size() - bannedSPs) * BottomTLRavgPCT_ST/100; 	
			if((TLRavg.size() - bannedSPs) >= 1 && BottomPoolSize < 1) BottomPoolSize = 1;
			BottomPoolSize = BottomPoolSize + bannedSPs;
			
			int poolCount = 0;
			
			for(int i = TLRavg.size()-1; i >= 0; i--)
			{
				tlravgRank = TLRavg.get(i);
				for(int j = TGR.size()-1; j >= 0 ; j--)
				{
					tgrRank = TGR.get(j);
					if(tgrRank.getSPid() == tlravgRank.getSPid())
						break;
				}
				
				if( poolCount >= BottomPoolSize && tlravgRank.getRank() > prevTlravgRank.getRank())
				{
					break;
				}
				
				Auditor_STNodesDir newST = new Auditor_STNodesDir();
				
				//Add an ST
				if(tgrRank.getSTLocator() == -1 && !tgrRank.isBanned() && !tgrRank.isEnabledStrictDGU())
				{
					//System.out.println("New ST to be added for: "+tgrRank.getSPid()+" with bottom tlravgRank: "+tlravgRank.getRank());
				
					newST.setSPID(tgrRank.getSPid());
					newST.setSTstatus(1);
					if(tgrRank.isInstalledDGU())
						newST.setDGUSPtargeted(true);
					
					sttoAdd.add(newST);
					
					tgrRank.setSTLocator(stNodes.size());
				
					stNodes.add(newST);
					poolCount++;
				}
				
				prevTlravgRank = tlravgRank;
			}
		}
		
		/**
		 * Select PSL for ST testing
		 */
		if(PSL_STselector)
		for(SPRrecord sprRecord: SPR)
		{
			if(sprRecord.isPSL() && !sprRecord.isSTtested() && !sprRecord.isEnabledStrictDGU())
			{
				TGRRank tgrRank2 = new TGRRank();
				for(int i = 0; i < TGR.size(); i++)
				{
					tgrRank2 = TGR.get(i);
					if(tgrRank2.getSPid() == sprRecord.getSPid())
					{
						if(tgrRank2.getSTLocator() != -1)
							sprRecord.setSTtested(true);
						
						break;
					}
				}
				
				if(!sprRecord.isSTtested())
				{
					//System.out.println("Creating an ST for PSL SPid: "+sprRecord.getSPid());
					Auditor_STNodesDir newST = new Auditor_STNodesDir();
					
					newST.setSPID(sprRecord.getSPid());
					newST.setSTstatus(1);
					if(sprRecord.isInstalledDGU())
						newST.setDGUSPtargeted(true);
					
					sttoAdd.add(newST);
					
					tgrRank2.setSTLocator(stNodes.size());
				
					stNodes.add(newST);
					
					sprRecord.setSTtested(true);
				}
				
				break;
			}
			
			if(!sprRecord.isPSL()) break;
		}
		
		/**
		 * Select Singular IIR records for ST testing
		 */
		if(IIR_STselector)
		for(IILrecord iil: IIR)
		{
			
			if(iil.isIgnore())
				continue;
			
			int spID = iil.getSPs().get(0);
			
			if(!iil.isStTested() && iil.getSPs().size() == 1)
			{
				TGRRank tgrRank2 = new TGRRank();
				for(int i = 0; i < TGR.size(); i++)
				{
					tgrRank2 = TGR.get(i);
					if(tgrRank2.getSPid() == spID)
					{
						if(tgrRank2.getSTLocator() != -1 || tgrRank2.getTSTRank() == 0)
							iil.setStTested(true);
						
						break;
					}
				}
				
				if(!iil.isStTested() && !tgrRank2.isEnabledStrictDGU())
				{
					//System.out.println("Creating an ST for IIR with only SPid: "+spID);
					
					Auditor_STNodesDir newST = new Auditor_STNodesDir();
					
					newST.setSPID(spID);
					newST.setSTstatus(1);
					if(tgrRank2.isInstalledDGU())
						newST.setDGUSPtargeted(true);
					
					sttoAdd.add(newST);
					
					tgrRank2.setSTLocator(stNodes.size());
				
					stNodes.add(newST);
					
					iil.setStTested(true);
				}
			}
		}
				
		Super_Node superNode = (Super_Node) Network.get(0).getProtocol(SuperPid);
		superNode.createSTs(sttoAdd);
		
	}
	
	public void updateTGT()
	{
		
		if(!DeployGT) return;
		
		//Update TGR.TGTsp values based on the TGTg records
		for(TGRRank rank: TGR)
		{
			if(rank.isBanned() || rank.isEnabledStrictDGU()) continue;
			
			rank.setTGTRank(-1);
			
			if(GT_avgTGT)
			{	
				double tgtgSum = 0;
				double tgtgRecords = 0;
				
				for(TGTgRank gRank: TGTg)
				{	
					if( (ignoreOldGranks && gRank.isIgnore()) || !gRank.containSP(rank.getSPid())) continue;
					
					for(int i = 0; i < Math.abs(gRank.getCounter()); i++)
					{
						tgtgSum = tgtgSum + gRank.getTGTgRank();
						tgtgRecords++;
					}
				}
				
				if(tgtgRecords == 0) 
					rank.setTGTRank(-1);
				else
					rank.setTGTRank(tgtgSum/tgtgRecords);				
			}
			
			else
			{
				for(TGTgRank gRank: TGTg)
				{
					if(rank.getTGTRank() == 0) break;
					
					if(!gRank.containSP(rank.getSPid())) continue;
					
					if(!(ignoreOldGranks && gRank.isIgnore()) && (rank.getTGTRank() < 0 || rank.getTGTRank() > gRank.getTGTgRank()))
						rank.setTGTRank(gRank.getTGTgRank());
				}
				
			}
			
			double tgtColluding = 0;	
			double pcrNum = 0;
			
			for(PColludingRecord pcolludingRecord: PColludingRecords)
				if(!pcolludingRecord.isIgnore() && pcolludingRecord.containSP(rank.getSPid()))
				{
					pcrNum = pcrNum + ( (2/(double)pcolludingRecord.getSPs().size() * (double) pcolludingRecord.getCounter() ));
				}
			
				if(pcrNum == 0) 
					tgtColluding = -1;
				else
				{
					tgtColluding = ((pcrThreshold - pcrNum)/pcrThreshold) * 100;
				
					//System.out.println("For SPid: "+rank.getSPid()+" pcrNum = "+pcrNum+" and tgtColluding: "+tgtColluding);
					
					if(tgtColluding < 0) tgtColluding = 0;
				}
				
			
				rank.setTGTColludingRank(tgtColluding);
				
				tgtColluding = 0;	
				pcrNum = 0;
				
				for(PColludingRecord pcolludingRecord: PWeakColludingRecords)
					if(!pcolludingRecord.isIgnore() && pcolludingRecord.containSP(rank.getSPid()))
					{
						pcrNum = pcrNum + ( (2/(double)pcolludingRecord.getSPs().size() * (double) pcolludingRecord.getCounter() ));
					}
				
					if(pcrNum == 0) 
						tgtColluding = -1;
					else
					{
						tgtColluding = ((pcrWeakThreshold - pcrNum)/pcrWeakThreshold) * 100;
					
						//System.out.println("For SPid: "+rank.getSPid()+" pcrNum = "+pcrNum+" and tgtColluding: "+tgtColluding);
						
						if(tgtColluding < 0) tgtColluding = 0;
					}
					
				
					rank.setTGTWeakColludingRank(tgtColluding);
		}
		
		//Manage currently Deployed GTs
		for(Auditor_GTNodesDir gtNode: gtNodes)
		{
			if(gtNode.getGTstatus() == 2 && (CommonState.getTime() - gtNode.getCreatedOn()) >= GTmaxLifeTime)
			{
				//System.out.println("GTagent: "+gtNode.getNodePos()+" is retiring!");
				
				User_Node agent = (User_Node) Network.get(gtNode.getNodePos()).getProtocol(UserPid);
				
				recieveGTagentReport(gtNode.getGTid(), gtNode.getTargetedSPs(), gtNode.getNodePos(), false, agent.getPSPsInteractedWith());
				killGTagent(gtNode.getGTid());
			}
		}
		
		//List of GTs to be dynamically added in the next cycle
		List<Auditor_GTNodesDir> gttoAdd = new ArrayList<Auditor_GTNodesDir>(20);
		
		/**
		 * Select Suspicious Unbanned SPs for GT testing
		 */
		if(SuspiciousNodes_GTselector && suspiciousSPNodes.size() > 1)
		for(Auditor_SPsNodesDir suspiciousSP: suspiciousSPNodes)
		{
			int SPsCount = 0;
			
			if(!suspiciousSP.isBanned() && !suspiciousSP.isEnabledStrictDGU() && suspiciousSP.getSuspiciousGTagents().size() == 0 && (!GT_SelectOnlyPSP || suspiciousSP.getSpUsefulness() >= this.usefulnessMIN) )
			{			
				Auditor_GTNodesDir newGT = new Auditor_GTNodesDir();
				newGT.setGTid(gtNodes.size());
				newGT.setGTtype(1);
				newGT.setGTstatus(1);
				
				for(Auditor_SPsNodesDir suspiciousSP2: suspiciousSPNodes)
				{
					if(suspiciousSP2.isBanned() || suspiciousSP2.isEnabledStrictDGU())
						continue;
					
					if(GT_SelectOnlyPSP && suspiciousSP2.getSpUsefulness() < this.usefulnessMIN )
						continue;
					
					else if(suspiciousSP2.getSpUsefulness() >= this.usefulnessMIN)
					{
						GT_TargetedSP targetedSP = new GT_TargetedSP();
						targetedSP.setSPid(suspiciousSP2.getPosition());
						newGT.getTargetedSPs().add(targetedSP);
						suspiciousSP2.getSuspiciousGTagents().add(newGT.getGTid());
					}
					else if(GT_MaxSPNum != -1 && SPsCount < GT_MaxSPNum)
					{
						GT_TargetedSP targetedSP = new GT_TargetedSP();
						targetedSP.setSPid(suspiciousSP2.getPosition());
						newGT.getTargetedSPs().add(targetedSP);
						suspiciousSP2.getSuspiciousGTagents().add(newGT.getGTid());
						SPsCount++;
					}
				}
				
				boolean Gfound = false;
				
				for(Auditor_GTNodesDir gtNode: gtNodes)
				{
					if( gtNode.getGTstatus() == 2  && equalTargetedSPs(newGT.getTargetedSPs(), gtNode.getTargetedSPs()))
					{
						Gfound = true;
						break;
					}
				}
				
				int removedSPsCount = 0;
				
				if(Gfound || newGT.getTargetedSPs().size() > GT_MaxSize)
					for(Auditor_SPsNodesDir suspiciousSP2: suspiciousSPNodes)
					{
						if(suspiciousSP2.isBanned() || suspiciousSP2.isEnabledStrictDGU())
							continue;
						
						if(suspiciousSP2.getSpUsefulness() >= this.usefulnessMIN)
							suspiciousSP2.getSuspiciousGTagents().remove(suspiciousSP2.getSuspiciousGTagents().size()-1);
					
						else if(!GT_SelectOnlyPSP && (GT_MaxSPNum == -1 || removedSPsCount < SPsCount  ))
						{
							if(suspiciousSP2.getSuspiciousGTagents().get(suspiciousSP2.getSuspiciousGTagents().size()-1) == newGT.getGTid())
							{
								suspiciousSP2.getSuspiciousGTagents().remove(suspiciousSP2.getSuspiciousGTagents().size()-1);
								removedSPsCount++;
							}
						}
					}
			
				else
				{
					boolean minRankReached = false;
							
					if(stopTestingIfMinReached)
					{
						for(TGTgRank tgtgRank: TGTg)
						{
							if(equalTargetedSPs_int_GT(tgtgRank.getSPs(), newGT.getTargetedSPs()))
							{	
								if(tgtgRank.isMinRankReached())
									minRankReached = true;
								
								break;
							}
						}
						
						if(minRankReached)
							for(Auditor_SPsNodesDir suspiciousSP2: suspiciousSPNodes)
							{
								if(suspiciousSP2.isBanned() || suspiciousSP2.isEnabledStrictDGU())
									continue;
								
								if(suspiciousSP2.getSpUsefulness() >= this.usefulnessMIN)
									suspiciousSP2.getSuspiciousGTagents().remove(suspiciousSP2.getSuspiciousGTagents().size()-1);
								
								else if(!GT_SelectOnlyPSP && (GT_MaxSPNum == -1 || removedSPsCount < SPsCount  ))
								{
									if(suspiciousSP2.getSuspiciousGTagents().get(suspiciousSP2.getSuspiciousGTagents().size()-1) == newGT.getGTid())
									{
										suspiciousSP2.getSuspiciousGTagents().remove(suspiciousSP2.getSuspiciousGTagents().size()-1);
										removedSPsCount++;
									}
								}
								
							}
					}
					
					if(!minRankReached)
					{
						boolean allSPsReachedMinRank = true;
						
						if(stopTestingIfMinReached)
						{
							double minPossibleRank = (1 - 2 / (double)newGT.getTargetedSPs().size()) * 100;
							for(GT_TargetedSP sp: newGT.getTargetedSPs())
							{
								for(TGRRank tgrRank: TGR)
								{
									if(tgrRank.getSPid() == sp.getSPid())
									{
										if(tgrRank.getTGTRank() > minPossibleRank || tgrRank.getTGTRank() < 0)
											allSPsReachedMinRank = false;
											
										break;
									}
								}
								
								if(!allSPsReachedMinRank)
									break;
							}
							
							if(allSPsReachedMinRank)
								for(Auditor_SPsNodesDir suspiciousSP2: suspiciousSPNodes)
								{
									if(suspiciousSP2.isBanned() || suspiciousSP2.isEnabledStrictDGU())
										continue;
									
									if(suspiciousSP2.getSpUsefulness() >= this.usefulnessMIN)
										suspiciousSP2.getSuspiciousGTagents().remove(suspiciousSP2.getSuspiciousGTagents().size()-1);
									
									else if(!GT_SelectOnlyPSP && (GT_MaxSPNum == -1 || removedSPsCount < SPsCount  ))
									{
										if(suspiciousSP2.getSuspiciousGTagents().get(suspiciousSP2.getSuspiciousGTagents().size()-1) == newGT.getGTid())
										{
											suspiciousSP2.getSuspiciousGTagents().remove(suspiciousSP2.getSuspiciousGTagents().size()-1);
											removedSPsCount++;
										}
									}
								}
						}
						
						if(!(stopTestingIfMinReached && allSPsReachedMinRank))
						{
							//Add a GT
							//System.out.println("New GT to be added for Suspicious SPs: ");
							//for(GT_TargetedSP sp: newGT.getTargetedSPs())
								//System.out.println("--sp: "+sp.getSPid());
							
							
							gttoAdd.add(newGT);
						
							gtNodes.add(newGT);
						}
					}
				}
				
				break;
			}
		}
				
		/**
		 * Select PSL for GT testing
		 */
		if(PSL_GTselector && SPR.size() > 1)
		for(int i = 0; i < SPR.size(); i++)
		{
			SPRrecord sprRecord = SPR.get(i);
			
			if(sprRecord.isPSL() && !sprRecord.isEnabledStrictDGU() && sprRecord.getPslGTagents().size() == 0)
			{
				Auditor_GTNodesDir newGT = new Auditor_GTNodesDir();
				
				newGT.setGTid(gtNodes.size());
				newGT.setGTtype(2);
				newGT.setGTstatus(1);
				
				for(SPRrecord sprRecord2: SPR)
				{
					if(sprRecord2.isEnabledStrictDGU()) continue;
					
					if(!sprRecord2.isPSL()) break;
					
					GT_TargetedSP targetedSP = new GT_TargetedSP();
					targetedSP.setSPid(sprRecord2.getSPid());
					newGT.getTargetedSPs().add(targetedSP);
					sprRecord2.getPslGTagents().add(newGT.getGTid());
					
				}		
				
				boolean Gfound = false;
				
				if(newGT.getTargetedSPs().size() > 1)
				for(Auditor_GTNodesDir gtNode: gtNodes)
				{
					if( (gtNode.getGTstatus() == 2 ) && equalTargetedSPs(newGT.getTargetedSPs(), gtNode.getTargetedSPs()))
					{
						Gfound = true;
						break;
					}
				}
				
				if(Gfound || newGT.getTargetedSPs().size() <= 1 || newGT.getTargetedSPs().size() > 5)
					for(SPRrecord sprRecord2: SPR)
					{
						if(sprRecord2.isEnabledStrictDGU()) continue;
						
						if(!sprRecord2.isPSL()) break;
						sprRecord2.getPslGTagents().remove(sprRecord2.getPslGTagents().size()-1);	
					}
				
				else
				{
					boolean minRankReached = false;
					if(stopTestingIfMinReached)
					{
						for(TGTgRank tgtgRank: TGTg)
						{
							if(equalTargetedSPs_int_GT(tgtgRank.getSPs(), newGT.getTargetedSPs()))
							{	
								if(tgtgRank.isMinRankReached())
									minRankReached = true;
								
								break;
							}
						}
						
						if(minRankReached)
							for(SPRrecord sprRecord2: SPR)
							{
								if(sprRecord2.isEnabledStrictDGU()) continue;
								
								if(!sprRecord2.isPSL()) break;
								sprRecord2.getPslGTagents().remove(sprRecord2.getPslGTagents().size()-1);	
							}
					}
					if(!minRankReached)
					{
						boolean allSPsReachedMinRank = true;
						
						if(stopTestingIfMinReached)
						{
							double minPossibleRank = (1 - 2 / (double) newGT.getTargetedSPs().size()) * 100;
							for(GT_TargetedSP sp: newGT.getTargetedSPs())
							{
								for(TGRRank tgrRank: TGR)
								{
									if(tgrRank.getSPid() == sp.getSPid())
									{
										if(tgrRank.getTGTRank() > minPossibleRank || tgrRank.getTGTRank() < 0)
											allSPsReachedMinRank = false;
											
										break;
									}
								}
								
								if(!allSPsReachedMinRank)
									break;
							}
							
							if(allSPsReachedMinRank)
								for(SPRrecord sprRecord2: SPR)
								{
									if(sprRecord2.isEnabledStrictDGU()) continue;
									
									if(!sprRecord2.isPSL()) break;
									sprRecord2.getPslGTagents().remove(sprRecord2.getPslGTagents().size()-1);	
								}
						}
				
						if(!(stopTestingIfMinReached && allSPsReachedMinRank) &&  newGT.getTargetedSPs().size() <= 5)
						{
						
							//Add a GT
							//System.out.println("Creating a GT for PSL G: ");
							//for(GT_TargetedSP sp: newGT.getTargetedSPs())
								//System.out.println("--sp: "+sp.getSPid());
							
									
							gttoAdd.add(newGT);
										
							gtNodes.add(newGT);
						}
					}
				}
				
				break;
			}
			
			if(!sprRecord.isPSL()) break;
		}
		
		/**
		 * Select Top PIIL entries for GT testing
		 */
		if(PIIL_GTselector)
		for(int i = 0; i < PIILs.size(); i++)
		{	
			PIILrecord piil = PIILs.get(i);
			
			int SPsCount = 0;
			
			if(piil.isPIILs() && piil.getPiilGTagents().size() == 0)
			{
				Auditor_GTNodesDir newGT = new Auditor_GTNodesDir();
				
				newGT.setGTid(gtNodes.size());
				newGT.setGTtype(3);
				newGT.setGTstatus(1);
				
				for(int j = piil.getSPs().size()-1; j >= 0; j--)
				{
					int sp = piil.getSPs().get(j);	
							
					if(GT_SelectOnlyPSP || GT_MaxSPNum != -1)
					{
						for(TGRRank tgrRank: TGR)
						{
							if(tgrRank.getSPid() == sp)
							{
								//if(tgrRank.isEnabledStrictDGU()) continue;
								
								if(tgrRank.getSpUsefulness() >= this.usefulnessMIN)
								{
									GT_TargetedSP targetedSP = new GT_TargetedSP();
									targetedSP.setSPid(sp);
									newGT.getTargetedSPs().add(targetedSP);
								}
								else if(!GT_SelectOnlyPSP && SPsCount < GT_MaxSPNum)
								{
									GT_TargetedSP targetedSP = new GT_TargetedSP();
									targetedSP.setSPid(sp);
									newGT.getTargetedSPs().add(targetedSP);
									SPsCount++;
								}
								break;
							}
						}
					}
					else
					{
						//for(TGRRank tgrRank: TGR)
						{
							//if(tgrRank.getSPid() == sp)
							{
								//if(tgrRank.isEnabledStrictDGU()) continue;
								
								GT_TargetedSP targetedSP = new GT_TargetedSP();
								targetedSP.setSPid(sp);
								newGT.getTargetedSPs().add(targetedSP);
								
								break;
							}
						}
						
					}
				}
				
				boolean Gfound = false;
				
				for(Auditor_GTNodesDir gtNode: gtNodes)
				{
					if( (gtNode.getGTstatus() == 2 ) && equalTargetedSPs(newGT.getTargetedSPs(), gtNode.getTargetedSPs()))
					{
						Gfound = true;
						break;
					}
				}
				
				if(!Gfound)
				{
					boolean minRankReached = false;

					if(stopTestingIfMinReached)
					{
						for(TGTgRank tgtgRank: TGTg)
						{
							if(equalTargetedSPs_int_GT(tgtgRank.getSPs(), newGT.getTargetedSPs()))
							{	
								if(tgtgRank.isMinRankReached())
									minRankReached = true;
								
								break;
							}
						}
					}
					
					if(stopTestingIfMinReached || !minRankReached)
					{
						boolean allSPsReachedMinRank = true;
						
						if(stopTestingIfMinReached)
						{
							double minPossibleRank = (1 - 2 / (double) newGT.getTargetedSPs().size()) * 100;
							for(GT_TargetedSP sp: newGT.getTargetedSPs())
							{
								for(TGRRank tgrRank: TGR)
								{
									if(tgrRank.getSPid() == sp.getSPid())
									{
										if(tgrRank.getTGTRank() > minPossibleRank || tgrRank.getTGTRank() < 0)
											allSPsReachedMinRank = false;
											
										break;
									}
								}
								
								if(!allSPsReachedMinRank)
									break;
							}
						}
						
						if(!(stopTestingIfMinReached && allSPsReachedMinRank))
						{	
							//Add a GT
							//System.out.println("Creating an GT for PIIL with SPs: ");
							//for(Integer sp: piil.getSPs())
								//System.out.println("--sp: "+sp);
							
						
							piil.getPiilGTagents().add(newGT.getGTid());
							
							gttoAdd.add(newGT);
							
							gtNodes.add(newGT);
						}
					}
				}
				
				break;
			}
			
			if(!piil.isPIILs())
				break;
			
		}
			
		Super_Node superNode = (Super_Node) Network.get(0).getProtocol(SuperPid);
		superNode.createGTs(gttoAdd);
	}
	
	public void updatePColludingRecords()
	{
		
		if(!DeployGT) return;
		
		PColludingRecords.clear();
		
		for(TGTgRank tgtgRank: TGTg)
		{
			if( (!ignoreOldGranks || !tgtgRank.isIgnore()) && tgtgRank.getTGTgRank() != 100 && tgtgRank.getTGTgRank() != -1)
			{
				//System.out.println("TGTgRank size: "+tgtgRank.getSPs().size());
				
				List<Integer> psps = new ArrayList<Integer>(tgtgRank.getSPs().size());
			
				for(Integer sp: tgtgRank.getSPs())
				{
					for(TGRRank tgrRank: TGR)
					{
						if(tgrRank.getSPid() == sp)
						{	
							//System.out.println("tgrRank.getSpUsefulness() = "+tgrRank.getSpUsefulness());
							
							if(tgrRank.getSpUsefulness() >= usefulnessMIN)
							{
								psps.add(sp);
							}
							
							break;
						}
					}
				}
				
				if(psps.size() == 0) continue;
				
				//System.out.println("PSPS size: "+psps.size());
				
				boolean pspsFound = false;
				
				for(PColludingRecord pcolludingRecord: PColludingRecords)
				{
					if(!pspsFound && this.equalTargetedSPs_int_int(pcolludingRecord.getSPs(), psps))
					{
						pspsFound = true;
						
						for(int i = 0; i < tgtgRank.getCounter(); i++)
							pcolludingRecord.incrementCounter();
					}
					else if(this.containTargetedSPs_int_int(psps, pcolludingRecord.getSPs()))
						for(int i = 0; i < tgtgRank.getCounter(); i++)
							pcolludingRecord.incrementCounter();
					
				}
				
				if(!pspsFound)
				{
					PColludingRecord newPColludingRecord = new PColludingRecord();
					newPColludingRecord.setSPs(psps);
					newPColludingRecord.setCounter(tgtgRank.getCounter());
					PColludingRecords.add(newPColludingRecord);
				}
			}
		}
		
		//Sort Records
		PColludingRecord tempRecord, currentRecord, prevRecord;
		for(int i = 0; i < PColludingRecords.size(); i++)
		{	
			if(i > 0)
			{
				currentRecord = PColludingRecords.get(i);
				
				for(int j = i - 1; j >= 0; j--)
				{
					currentRecord = PColludingRecords.get(j+1);
					prevRecord = PColludingRecords.get(j);
					if(currentRecord.getCounter() > prevRecord.getCounter())
					{
						tempRecord = currentRecord.copy();
						PColludingRecords.set(j+1, prevRecord.copy());
						PColludingRecords.set(j, tempRecord.copy());
					}
					else
						break;
				}
			}
		}
	}

	public void updatePWeakColludingRecords()
	{
		if(!DeployGT) return;
		
		PWeakColludingRecords.clear();
		
		for(TGTgRank tgtgRank: TGTg_allCredential)
		{
			if( (!ignoreOldGranks || !tgtgRank.isIgnore()) && tgtgRank.getTGTgRank() != 100 && tgtgRank.getTGTgRank() != -1)
			{
				List<Integer> psps = tgtgRank.getSPs();
				
				if(psps.size() == 0) continue;
								
				boolean pspsFound = false;
				
				for(PColludingRecord pcolludingRecord: PWeakColludingRecords)
				{
					if(!pspsFound && this.equalTargetedSPs_int_int(pcolludingRecord.getSPs(), psps))
					{
						pspsFound = true;
						
						for(int i = 0; i < tgtgRank.getCounter(); i++)
							pcolludingRecord.incrementCounter();
					}
					else if(this.containTargetedSPs_int_int(psps, pcolludingRecord.getSPs()))
						for(int i = 0; i < tgtgRank.getCounter(); i++)
							pcolludingRecord.incrementCounter();
					
				}
				
				if(!pspsFound)
				{
					PColludingRecord newPColludingRecord = new PColludingRecord();
					newPColludingRecord.setSPs(psps);
					newPColludingRecord.setCounter(tgtgRank.getCounter());
					PWeakColludingRecords.add(newPColludingRecord);
				}
			}
		}
		
		//Sort Records
		PColludingRecord tempRecord, currentRecord, prevRecord;
		for(int i = 0; i < PWeakColludingRecords.size(); i++)
		{	
			if(i > 0)
			{
				currentRecord = PWeakColludingRecords.get(i);
				
				for(int j = i - 1; j >= 0; j--)
				{
					currentRecord = PWeakColludingRecords.get(j+1);
					prevRecord = PWeakColludingRecords.get(j);
					if(currentRecord.getCounter() > prevRecord.getCounter())
					{
						tempRecord = currentRecord.copy();
						PWeakColludingRecords.set(j+1, prevRecord.copy());
						PWeakColludingRecords.set(j, tempRecord.copy());
					}
					else
						break;
				}
			}
		}

	}

	
	public void updateTGR()
	{
		this.updateTLRavg();
		
		int spID;
		boolean spFound;
		
		for(TLRavgRank tlravgRank: TLRavg)
		{
			if(tlravgRank.isEnabledStrictDGU())
				continue;
			
			spID = tlravgRank.getSPid();
			spFound = false;
			
			for(TGRRank tgrRank: TGR)
				if(spID == tgrRank.getSPid())
				{
					tgrRank.setTLRavgRank(tlravgRank.getRank());
					spFound = true;					
					break;
				}
			
			if(!spFound)
			{
				//System.out.println("Adding new SP to TGR, SPid:  "+spID);
				TGRRank tgrRank = new TGRRank();
				tgrRank.setSPid(spID);
				tgrRank.setTLRavgRank(tlravgRank.getRank());
				tgrRank.setSpUsefulness(tlravgRank.getSpUsefulness());
				TGR.add(tgrRank);
			}
		}
		
		for(TSTRank tstRank: TST)
			for(TGRRank tgrRank: TGR)
			{
				if(tstRank.getSPid() == tgrRank.getSPid())
				{
					tgrRank.setTSTRank(tstRank.getRank());
					break;
				}
			}		

		double rank, tstRank, tgtRank, tgtcolludingRank, tgtweakcolludingRank, tlravgRank;
		
		double TSTWeight,TLRavgWeight, TGTWholeWeight, TGTWeight, 
		TGTColludingWeight, TGTWeakColludingWeight, TGTFinalWeight, 
		TGTFinalColludingWeight, TGTFinalWeakColludingWeight;
		
		for(TGRRank tgrRank: TGR)
		{		
			if(tgrRank.isEnabledStrictDGU())
				continue;
			
			rank = 0;
			tstRank = tgrRank.getTSTRank();
			tlravgRank = tgrRank.getTLRavgRank();
			
			tgtRank = tgrRank.getTGTRank();
			tgtcolludingRank = tgrRank.getTGTColludingRank();
			tgtweakcolludingRank = tgrRank.getTGTWeakColludingRank();
			
			TSTWeight = this.TSTWeight/100;
			TLRavgWeight = this.TLRavgWeight/100;
			
			TGTWholeWeight = this.TGTWholeWeight/100;
			
			TGTWeight = this.TGTWeight;
			TGTColludingWeight = this.TGTColludingWeight;
			TGTWeakColludingWeight =this.TGTWeakColludingWeight;
			
			TGTFinalWeight = TGTWholeWeight * TGTWeight/100;
			TGTFinalColludingWeight = TGTWholeWeight * TGTColludingWeight/100 ;
			TGTFinalWeakColludingWeight = TGTWholeWeight * TGTWeakColludingWeight/100;
			
			if(tlravgRank >= 0 && tstRank < 0 && (tgtRank >= 0 || tgtweakcolludingRank >= 0))
			{ 
				TLRavgWeight = TLRavgWeight + TSTWeight/2;
				
				TGTWholeWeight = TGTWholeWeight + TSTWeight/2;
				
				TSTWeight = 0;
				
				TGTFinalWeight = TGTWholeWeight * TGTWeight/100;
				TGTFinalColludingWeight = TGTWholeWeight * TGTColludingWeight/100 ;
				TGTFinalWeakColludingWeight = TGTWholeWeight * TGTWeakColludingWeight/100;
			}
			else if(tlravgRank >= 0 && tstRank >= 0 && (tgtRank < 0 && tgtweakcolludingRank < 0))
			{	
				TLRavgWeight = TLRavgWeight + TGTWholeWeight/2;
				
				TSTWeight = TSTWeight + TGTWholeWeight/2;
				
				TGTFinalWeight = TGTFinalColludingWeight = TGTFinalWeakColludingWeight = 0;
			}
			else if(tlravgRank >= 0 && tstRank < 0 && (tgtRank < 0 && tgtweakcolludingRank < 0))
			{
				TLRavgWeight = 1;
				TSTWeight = TGTFinalWeight = TGTFinalColludingWeight = TGTFinalWeakColludingWeight = 0;
			}
			else if(tlravgRank < 0 && tstRank < 0 && (tgtRank >= 0 || tgtweakcolludingRank >= 0))
			{	
				TGTWholeWeight = 1;
				
				TSTWeight = TLRavgWeight = 0;
				
				TGTFinalWeight = TGTWholeWeight * TGTWeight/100;
				TGTFinalColludingWeight = TGTWholeWeight * TGTColludingWeight/100 ;
				TGTFinalWeakColludingWeight = TGTWholeWeight * TGTWeakColludingWeight/100;
			}
			else if(tlravgRank < 0 && tstRank >= 0 && (tgtRank < 0 && tgtweakcolludingRank < 0))
			{	
				TSTWeight = 1;
				
				TGTFinalWeight = TGTFinalColludingWeight = TGTFinalWeakColludingWeight
						= TGTWholeWeight = TLRavgWeight = 0;
			}
			else if(tlravgRank < 0 && tstRank >= 0 && (tgtRank >= 0 || tgtweakcolludingRank >= 0))
			{	
				TGTWholeWeight = TGTWholeWeight + TLRavgWeight/2;
				
				TSTWeight = TSTWeight + TLRavgWeight/2;
				
				TGTFinalWeight = TGTWholeWeight * TGTWeight/100;
				TGTFinalColludingWeight = TGTWholeWeight * TGTColludingWeight/100 ;
				TGTFinalWeakColludingWeight = TGTWholeWeight * TGTWeakColludingWeight/100;
			}
			
			
			if(tgtRank >= 0 && tgtcolludingRank == -1 && tgtweakcolludingRank == -1) 
			{
				TGTFinalWeight = TGTWholeWeight;
				TGTFinalColludingWeight = TGTFinalWeakColludingWeight = 0;
			}
			else if(tgtRank >= 0 && tgtcolludingRank == -1 && tgtweakcolludingRank >= 0 ) 
			{
				TGTFinalWeight = TGTFinalWeight + TGTFinalColludingWeight/2;
				TGTFinalWeakColludingWeight = TGTFinalWeakColludingWeight + TGTFinalColludingWeight/2;
				TGTFinalColludingWeight = 0;
			}
			else if(tgtRank >= 0 && tgtcolludingRank >= 0 && tgtweakcolludingRank == -1) 
			{
				TGTFinalWeight = TGTFinalWeight + TGTFinalWeakColludingWeight/2;
				TGTFinalColludingWeight = TGTFinalColludingWeight + TGTFinalWeakColludingWeight/2;
				TGTFinalWeakColludingWeight = 0;
			}
			else if(tgtRank == -1 && tgtcolludingRank == -1 && tgtweakcolludingRank >= 0) 
			{
				TGTFinalWeakColludingWeight = TGTWholeWeight;
				TGTFinalColludingWeight = TGTFinalWeight = 0;
			}
			
			rank = (TSTWeight * tstRank) + (TGTFinalWeight * tgtRank) + (TGTFinalColludingWeight * tgtcolludingRank) + (TGTFinalWeakColludingWeight * tgtweakcolludingRank) + (TLRavgWeight * tlravgRank);
			tgrRank.setRank(rank);
		
			if(tlravgRank == -1 && tgtRank == -1 && tgtweakcolludingRank == -1 && tstRank == -1)
			{
				if(tgrRank.isEnabledStrictDGU()) 
					tgrRank.setRank(postStrictDGUEnableRank);
				
				else if(tgrRank.isInstalledDGU()) 
					tgrRank.setRank(postDGUinstallationRank);
			}
			
			//System.out.println("tgrRank.getRank() = "+tgrRank.getRank());
			if(tgrRank.getRank() < suspiciousSPRank || (tgrRank.getTGTColludingRank() < suspiciousSPRank && tgrRank.getTGTColludingRank() >= 0) || (tgrRank.getTGTWeakColludingRank() < suspiciousSPRank && tgrRank.getTGTWeakColludingRank() >= 0))
			{
				spID = tgrRank.getSPid();
				spFound = false;
				for(Auditor_SPsNodesDir currentSuspiciousSP: suspiciousSPNodes)
				{
					if(spID == currentSuspiciousSP.getPosition())
					{	
						spFound = true;
						break;
					}
				}
				if(!spFound)
				{
					/*
					if(tgrRank.getTGTColludingRank() < suspiciousSPRank && tgrRank.getTGTColludingRank() > 0)
						System.out.println("SPid: "+tgrRank.getSPid()+" is suspicious with rank = "+tgrRank.getRank()+" and TGTcolluding: "+tgrRank.getTGTColludingRank());
					if(tgrRank.getTGTWeakColludingRank() < suspiciousSPRank && tgrRank.getTGTWeakColludingRank() > 0)
						System.out.println("SPid: "+tgrRank.getSPid()+" is suspicious with rank = "+tgrRank.getRank()+" and TGTWeakColluding: "+tgrRank.getTGTWeakColludingRank());
					*/
					//System.out.println("SPid: "+tgrRank.getSPid()+" is added to the suspicious list");

					Auditor_SPsNodesDir newSuspiciousSP = new Auditor_SPsNodesDir();
					newSuspiciousSP.setPosition(spID);
					newSuspiciousSP.setRank(tgrRank.getRank());
					newSuspiciousSP.setTSTrank(tgrRank.getTSTRank());
					newSuspiciousSP.setTGTColludingRank(tgrRank.getTGTColludingRank());
					newSuspiciousSP.setTGTWeakColludingRank(tgrRank.getTGTWeakColludingRank());
					newSuspiciousSP.setTGTrank(tgrRank.getTGTRank());
					newSuspiciousSP.setSpUsefulness(tgrRank.getSpUsefulness());
					if(tgrRank.getSTLocator() > -1)
					{
						newSuspiciousSP.setStTested(true);
						newSuspiciousSP.setTSTrank(tgrRank.getTSTRank());
					}
					
					suspiciousSPNodes.add(newSuspiciousSP);
				}
			}
		}
		
		//Sort Rankings
		TGRRank tempRank, currentRank, prevRank;
		for(int i = 0; i < TGR.size(); i++)	
			if(i > 0)
			{
				for(int j = i - 1; j >= 0; j--)
				{
					currentRank = TGR.get(j+1);
					prevRank = TGR.get(j);
					if(currentRank.getRank() > prevRank.getRank())
					{
						tempRank = currentRank.copy();
						TGR.set(j+1, prevRank.copy());
						TGR.set(j, tempRank.copy());
					}
					else
						break;
				}
			}
		
		//Update Suspicious List Rankings
		for(Auditor_SPsNodesDir currentSuspiciousSP: suspiciousSPNodes)
		{
			TGRRank tgrRank;
			for(int j = TGR.size()-1; j >= 0; j--)
			{
				tgrRank = TGR.get(j);
				if(tgrRank.getSPid() == currentSuspiciousSP.getPosition())
				{
					currentSuspiciousSP.setRank(tgrRank.getRank());
				
					if(tgrRank.getSTLocator() > -1)
						currentSuspiciousSP.setStTested(true);
					else
						currentSuspiciousSP.setStTested(false);
					
					currentSuspiciousSP.setTGTrank(tgrRank.getTGTRank());
					currentSuspiciousSP.setTGTColludingRank(tgrRank.getTGTColludingRank());
					currentSuspiciousSP.setTGTWeakColludingRank(tgrRank.getTGTWeakColludingRank());
					currentSuspiciousSP.setTSTrank(tgrRank.getTSTRank());
					
					break;
				}
			}
		}
		
	}
	
	public void updateSPR()
	{
		double PSLrange = (PSL_PCT/100) * SPR.size();
		if(PSLrange < 1) PSLrange = 1;
		
		//Sort Records
		SPRrecord tempRank, currentRank, prevRank;
		for(int i = 0; i < SPR.size(); i++)
		{	
			if(i > 0)
			{
				currentRank = SPR.get(i);
				currentRank.setPSL(false);
				
				for(int j = i - 1; j >= 0; j--)
				{
					currentRank = SPR.get(j+1);
					prevRank = SPR.get(j);
					if(currentRank.getCounter() > prevRank.getCounter())
					{
						tempRank = currentRank.copy();
						SPR.set(j+1, prevRank.copy());
						SPR.set(j, tempRank.copy());
					}
					else
						break;
				}
			}
		}
		
		//Set the PSL records
		if(SPR.size() > 0)
		{
			int prevCount = 0;
			for(int i = 0; i < SPR.size(); i++)
			{
				SPRrecord sprRecord = SPR.get(i);
				
				if(i < (int) PSLrange || sprRecord.getCounter() == prevCount)
				{
					sprRecord.setPSL(true);
					prevCount = sprRecord.getCounter();
				}
				else
					break;
			}
		}
	}
	
	public void updatePIILs()
	{
		for(int i = IIR.size()-1; i >= 0; i--)
		{
			IILrecord iil = IIR.get(i);
			
			if(iil.isPIILevaluated()) break;
			
			else if(iil.getSPs().size() < 2)
			{
				iil.setPIILevaluated(true);
				continue;
			}
			
			boolean bannedFound = false;
			
			for(int iilSP: iil.getSPs())
			{
				for(Auditor_SPsNodesDir bannedSPNode: bannedSPNodes)
				{
					int bannedSP = bannedSPNode.getPosition();
					
					if(bannedSP == iilSP)
					{
						bannedFound = true;
						break;
					}
				}
			}
			
			
			if(bannedFound)
			{
				iil.setPIILevaluated(true);
				continue;
			}
			
			boolean iilFound = false;
			
			for(int j = 0; j < PIILs.size(); j++)
			{
				PIILrecord piil = PIILs.get(j);
				
				if(piil.getSPs().size() > iil.getSPs().size())
					continue;
				
				else if(piil.getSPs().size() == iil.getSPs().size() && !iilFound)
				{
					boolean allSPsEqual = true;
					for(int k = 0; k < piil.getSPs().size(); k++)
					{
						if(piil.getSPs().get(k) != iil.getSPs().get(k))
						{
							allSPsEqual = false;
							break;
						}
					}
					
					if(allSPsEqual)
					{
						iilFound = true;
						piil.incrementCounter();
					}
				}
				
				else if(piil.getSPs().size() < iil.getSPs().size())
				{
					int foundSPs = 0;
					
					int k = 0;
					int l = 0;
					
					while(true)
					{
						if(k == piil.getSPs().size() || l == iil.getSPs().size())
							break;
						
						int piilSP = piil.getSPs().get(k);
						int iilSP = iil.getSPs().get(l);
						
						if(piilSP == iilSP)
						{
							k++;
							l++;
							foundSPs++;
						}
						
						else if(piilSP < iilSP)
							break;
						
						else if(piilSP > iilSP)
							l++;	
					}
					
					if(foundSPs == piil.getSPs().size())
					{
						piil.incrementCounter();
					}
				}
			}
			
			if(!iilFound)
			{
				PIILrecord piil = new PIILrecord();
				piil.setCounter(1);
				piil.setSPs(iil.getSPs());
				PIILs.add(piil);
			}
			
			iil.setPIILevaluated(true);
		}
		
		//Sort Records
		PIILrecord tempPIIL, currentPIIL, prevPIIL;
		for(int i = 0; i < PIILs.size(); i++)
		{	
			if(i > 0)
			{
				currentPIIL = PIILs.get(i);
				currentPIIL.setPIILs(false);
				
				for(int j = i - 1; j >= 0; j--)
				{
					currentPIIL = PIILs.get(j+1);
					prevPIIL = PIILs.get(j);
					if(currentPIIL.getCounter() > prevPIIL.getCounter())
					{
						tempPIIL = currentPIIL.copy();
						PIILs.set(j+1, prevPIIL.copy());
						PIILs.set(j, tempPIIL.copy());
					}
					else
						break;
				}
			}
		}
		
		//Set the PIILs records
		
		double PIILrange = (PIIL_PCT/100) * PIILs.size();
		if(PIILrange < 1) PIILrange = 1;
		
		if(PIILs.size() > 0)
		{
			int prevCount = 0;
			for(int i = 0; i < PIILs.size(); i++)
			{
				PIILrecord piilRecord = PIILs.get(i);
				
				if(i < (int) PIILrange || piilRecord.getCounter() == prevCount)
				{
					piilRecord.setPIILs(true);
					prevCount = piilRecord.getCounter();
				}
				else
					break;
			}
		}
		
	}
	
	
	public void processCases()
	{
		CaseLogs caseLogs;
		Auditor_SPsNodesDir suspiciousSP;
		Auditor_SPsNodesDir caseSP;
		Auditor_SPsNodesDir caseGuiltySP;
		boolean guiltySPFound;
		List<Auditor_SPsNodesDir> iniCaseGuiltySPs;
		
		for(Auditor_UsersNodesDir uNode: uNodes)
		{
			for(int j =0; j < uNode.getCaseLogs().size(); j++)
			{	
				caseLogs = uNode.getCaseLogs().get(j);
				
				if(!caseLogs.isGPD_Evaluated())
					continue;
				
				if(caseLogs.getLogs().size() == 0 && !caseLogs.isClosed())
				{
					int credentialID = caseLogs.getCredential().getId();
					CaseLogs prevCaseLogs;
					
					for(int k = j-1; k >= 0; k--)
					{
						prevCaseLogs = uNode.getCaseLogs().get(k);
						if(credentialID == prevCaseLogs.getCredential().getId() && prevCaseLogs.isClosed())
						{
							iniCaseGuiltySPs = prevCaseLogs.cloneCaseGuiltySps();
							boolean allBanned = true;
							for(Auditor_SPsNodesDir sp: iniCaseGuiltySPs)
							{
								boolean spFound = false;
								for(Auditor_SPsNodesDir bannedSP: bannedSPNodes)
								{
									if(bannedSP.getPosition() == sp.getPosition())
									{
										spFound = true;
										break;
									}
								}
								if(!spFound)
								{
									allBanned = false;
									break;
								}
							}
							
							if(allBanned)
							{
								caseLogs.setCaseGuiltySps(iniCaseGuiltySPs);
								caseLogs.setClosed(true);
								this.closeCase(caseLogs);
								break;
							}
						}
					}
					
					continue;
				}
				
				iniCaseGuiltySPs = caseLogs.cloneCaseGuiltySps();
				for(int k = 0; k < suspiciousSPNodes.size(); k++)
				{
					suspiciousSP = suspiciousSPNodes.get(k);
					for(int l = 0; l < caseLogs.getCaseSps().size(); l++)
					{
						caseSP = caseLogs.getCaseSps().get(l);
						if(caseSP.getPosition() == suspiciousSP.getPosition())
						{
							guiltySPFound = false;
							for(int m = 0; m < caseLogs.getCaseGuiltySps().size(); m++)
							{
								caseGuiltySP = caseLogs.getCaseGuiltySps().get(m);
								if(caseGuiltySP.getPosition() == suspiciousSP.getPosition())
								{
									caseGuiltySP.setRank(suspiciousSP.getRank());
									guiltySPFound = true;
									break;
								}
							}
							if(!guiltySPFound)
							{
								caseGuiltySP = new Auditor_SPsNodesDir();
								caseGuiltySP.setMalicious(true);
								caseGuiltySP.setPosition(suspiciousSP.getPosition());
								caseGuiltySP.setRank(suspiciousSP.getRank());
								
								caseLogs.getCaseGuiltySps().add(caseGuiltySP);
							}
							break;
						}
					}
				}
				
				double minRank = 100;
				for(int k = 0; k < caseLogs.getCaseGuiltySps().size(); k++)
				{
					caseGuiltySP = caseLogs.getCaseGuiltySps().get(k);
					if(caseGuiltySP.getRank() > suspiciousSPRank)
					{
						caseLogs.getCaseGuiltySps().remove(k);
					}
					else if(caseGuiltySP.getRank() < minRank)
						minRank = caseGuiltySP.getRank();
				}
				
				double minRankUpperLimit = minRank + suspiciousSPRange;
				double minRankLowerLimit = minRank - suspiciousSPRange;
				
				for(int k = 0; k < caseLogs.getCaseGuiltySps().size(); k++)
				{
					caseGuiltySP = caseLogs.getCaseGuiltySps().get(k);
					if( !(caseGuiltySP.getRank() <= minRankUpperLimit && caseGuiltySP.getRank() >= minRankLowerLimit))
					{
						caseLogs.getCaseGuiltySps().remove(k);
					}
				}
				
				if( caseLogs.getCaseGuiltySps().size() == 0 && caseLogs.isClosed())
					this.openClosedCase(caseLogs,iniCaseGuiltySPs );
				
				else if( caseLogs.getCaseGuiltySps().size() > 0 && !caseLogs.isClosed() )
					this.closeCase(caseLogs);
				
				else if(!caseLogs.equalCaseGuiltyLists(caseLogs.getCaseGuiltySps(), iniCaseGuiltySPs))
					this.reCloseCase(caseLogs,iniCaseGuiltySPs );
				
			
			}
		}
	}
	
	public void closeCase(CaseLogs caseLogs)
	{
		GPD_Evaluator evaluater = new GPD_Evaluator();
		List<GPD_GuiltRate> gpdRates;
		
		gpdRates = evaluater.generateGuiltRates(caseLogs, SPPid);
		caseLogs.setGPD_Evaluated(true);
		
		Auditor_UsersNodesDir uNode = new Auditor_UsersNodesDir();
		for(Auditor_UsersNodesDir uNodeItem: uNodes)
		{
			if(uNodeItem.getPosition() == caseLogs.getUserPos())
			{
				uNode = uNodeItem;
				break;
			}
		}
		
		for(GPD_GuiltRate rate: gpdRates)
		{
			for(TLRuRank rank: uNode.getTLRu())
			{
				if(rank.getSPid() == rate.getSPid())
				{
					rank.setGuiltRateSum(rank.getGuiltRateSum()-rate.getGuiltRate());
					rank.setCasesCount(rank.getCasesCount()-1);
					
					//System.out.println("Closing Case! TLRu Rank for SPid"+rank.getSPid()+" is: "+(double) (100 - (rank.getGuiltRateSum()/rank.getCasesCount())));
					
					for(Auditor_SPsNodesDir spNode: caseLogs.getCaseGuiltySps())
					{
						if(spNode.getPosition() == rank.getSPid())
						{
							rank.setGuiltRateSum(rank.getGuiltRateSum()+100);
							rank.setCasesCount(rank.getCasesCount()+1);
							break;
						}
					}
					
					break;
				}
			}
		}
		
		caseLogs.setClosed(true);
		caseLogs.setPeriodToCloseCase(CommonState.getTime() - caseLogs.getCreatedOn());
		
		this.sednUserCaseUpdate(caseLogs.getUserPos(), true, caseLogs.getCredential(), caseLogs.getPeriodToCloseCase());
	}
	
	public void reCloseCase(CaseLogs caseLogs, List<Auditor_SPsNodesDir> iniCaseGuiltySPs)
	{
		//GPD evaluater = new GPD();
		//List<GPD_GuiltRate> gpdRates;
		
		//gpdRates = evaluater.generateGuiltRates(caseLogs);
		//caseLogs.setGPD_Evaluated(true);
		
		Auditor_UsersNodesDir uNode = new Auditor_UsersNodesDir();
		for(Auditor_UsersNodesDir uNodeItem: uNodes)
		{
			if(uNodeItem.getPosition() == caseLogs.getUserPos())
			{
				uNode = uNodeItem;
				break;
			}
		}
		
		//boolean iniSPFound = false;
		
		//for(GPD_GuiltRate rate: gpdRates)
		{
			for(TLRuRank rank: uNode.getTLRu())
			{
				//if(rank.getSPid() == rate.getSPid())
				{
					//rank.setGuiltRateSum(rank.getGuiltRateSum()-rate.getGuiltRate());
					//rank.setCasesCount(rank.getCasesCount()-1);
				//	System.out.println("");
				//	System.out.println("Re Closing Case! New list");
					for(Auditor_SPsNodesDir spNode: caseLogs.getCaseGuiltySps())
					{
						if(spNode.getPosition() == rank.getSPid())
						{
							rank.setGuiltRateSum(rank.getGuiltRateSum()+100);
							rank.setCasesCount(rank.getCasesCount()+1);
							//System.out.println("--TLRu Rank for SPid"+rank.getSPid()+" is: "+(double) (100 - (rank.getGuiltRateSum()/rank.getCasesCount())));
							break;
						}
					}
					
				//	System.out.println("");
					//System.out.println("Re Closing Case! Old List");
					for(Auditor_SPsNodesDir spNode: iniCaseGuiltySPs)
					{					
						if(spNode.getPosition() == rank.getSPid())
						{
							rank.setGuiltRateSum(rank.getGuiltRateSum()-100);
							rank.setCasesCount(rank.getCasesCount()-1);
							
							//System.out.println("--TLRu Rank for SPid"+rank.getSPid()+" is: "+(double) (100 - (rank.getGuiltRateSum()/rank.getCasesCount())));
						}
					}
				//	System.out.println("");
					break;
				}
			}
		}
		
		caseLogs.setPeriodToCloseCase(CommonState.getTime() - caseLogs.getCreatedOn());
	}
	
	public void openClosedCase(CaseLogs caseLogs, List<Auditor_SPsNodesDir> iniCaseGuiltySPs)
	{
		//System.out.println("Open Case! ");
		
		GPD_Evaluator evaluater = new GPD_Evaluator();
		List<GPD_GuiltRate> gpdRates;
		
		gpdRates = evaluater.generateGuiltRates(caseLogs, SPPid);
		caseLogs.setGPD_Evaluated(true);
		
		Auditor_UsersNodesDir uNode = new Auditor_UsersNodesDir();
		for(Auditor_UsersNodesDir uNodeItem: uNodes)
		{
			if(uNodeItem.getPosition() == caseLogs.getUserPos())
			{
				uNode = uNodeItem;
				break;
			}
		}
		
		for(Auditor_SPsNodesDir iniGuilty: iniCaseGuiltySPs)
		{
		//	System.out.println("");
			//System.out.println("Open Closed Case!");
			for(TLRuRank rank: uNode.getTLRu())
			{
				if(rank.getSPid() == iniGuilty.getPosition())
				{
					rank.setGuiltRateSum(rank.getGuiltRateSum()-100);	
					rank.setCasesCount(rank.getCasesCount()-1);
				//	System.out.println("--old Guilty: TLRu Rank for SPid"+rank.getSPid()+" is: "+(double) (100 - (rank.getGuiltRateSum()/rank.getCasesCount())));
					break;
				}
			}
		//	System.out.println("");
		}
		
		for(GPD_GuiltRate rate: gpdRates)
		{
		//	System.out.println("");
		//	System.out.println("Open Closed Case!");
			for(TLRuRank rank: uNode.getTLRu())
			{
				if(rank.getSPid() == rate.getSPid())
				{
					rank.setGuiltRateSum(rank.getGuiltRateSum()+rate.getGuiltRate());	
					rank.setCasesCount(rank.getCasesCount()+1);
				//	System.out.println("--TLRu Rank for SPid"+rank.getSPid()+" is: "+(double) (100 - (rank.getGuiltRateSum()/rank.getCasesCount())));
					break;
				}
			}
			//System.out.println("");
		}
		
		//this.updateTLRu();
		
		this.sednUserCaseUpdate(caseLogs.getUserPos(), false, caseLogs.getCredential(), 0);
		
		caseLogs.setClosed(false);
	}
	
	public void sednUserCaseUpdate(int userID, boolean closure, Credential credential, long periodToClose)
	{
		User_Node user = (User_Node) Network.get(userID).getProtocol(UserPid);
		
		if(closure)
			user.recieveCaseClosure(periodToClose);
	
		else
			user.recieveSpam(credential);	
	}
	
	public void recieveSTcreationUpdate(List<Auditor_STNodesDir> newSTs)
	{
		for(Auditor_STNodesDir newST: newSTs)
		{
			for(Auditor_STNodesDir stRecord: stNodes)
			{
				if(stRecord.getSPID() == newST.getSPID())
				{
					stRecord.setCreatedOn(newST.getCreatedOn());
					stRecord.setNodePos(newST.getNodePos());
					stRecord.setSTstatus(2);
					break;
				}
			}
		}
	}
	
	
	public void recieveSTagentReport(int SPid, int nodePos, boolean spamRecieved)
	{		
		User_Node user = (User_Node) Network.get(nodePos).getProtocol(UserPid);
	
		SP_Node suspiciousSP = (SP_Node) Network.get(SPid).getProtocol(SPPid);
		
		IDP_Node idp = (IDP_Node) Network.get(1).getProtocol(IDPPid);
		
		SpamReport spamReport = idp.reportSpam(user.getSTimportantCredential(), false);
		
		List<CaseSPs> PSPsInteractedWith = spamReport.getPSPsInteractedWith();
		CaseLogs caseLogs = spamReport.getCaseLogs();
		
		for(TGRRank rank: TGR)
		{
			if(rank.getSPid() == SPid)
			{
				if(spamRecieved) 
				{
					//System.out.println("Auditor recieved SPAM report by STagent: "+nodePos+" from SPid: "+SPid);
					
					if(!DeployDGU)
					{
						rank.setTSTRank(0);	
						banSP(SPid, 6);
					}
					else if(!rank.isInstalledDGU() && DeployDGU)							
					{
						boolean acceptedDGUoffer = suspiciousSP.acceptDGUoffer(false);
						
						if(acceptedDGUoffer)
							updateSPnodeStatus(SPid, 1, 6);
						else
						{
							rank.setTSTRank(0);	
							banSP(SPid, 6);
						}
					}
					else
					{
						TST_Evaluator tstEvaluater = new TST_Evaluator();
						List<Aux_TSTrateRecord> tstRates = tstEvaluater.generateTSTRanks(caseLogs, this.SPPid);
						
						for(Aux_TSTrateRecord tstRate: tstRates)
						{
							boolean spFound = false;
							
							for(TSTRank tstRank: TST)
							{
								if(tstRate.getSPid() == tstRank.getSPid())
								{
									tstRank.addTstRank(tstRate.getRank());
									spFound = true;
									break;
								}
							}
							
							if(!spFound)
							{
								TSTRank tstRank = new TSTRank();
								tstRank.setSPid(tstRate.getSPid());
								tstRank.addTstRank(tstRate.getRank());
								
								SP_Node spNode = (SP_Node) Network.get(tstRate.getSPid()).getProtocol(SPPid);
								tstRank.setSpUsefulness(spNode.getUsefulness());

								TST.add(tstRank);
							}
						}
					}
				}
				else
				{
					//System.out.println("Auditor recieved a NO SPAM report by STagent: "+nodePos+" from SPid: "+SPid);

					boolean spFound = false;
					
					for(TSTRank tstRank: TST)
					{
						if(SPid == tstRank.getSPid())
						{
							tstRank.addTstRank(100);
							spFound = true;
							break;
						}
					}
					
					if(!spFound)
					{
						TSTRank tstRank = new TSTRank();
						tstRank.setSPid(SPid);
						tstRank.addTstRank(100);
						
						SP_Node spNode = (SP_Node) Network.get(SPid).getProtocol(SPPid);
						tstRank.setSpUsefulness(spNode.getUsefulness());

						TST.add(tstRank);
					}
				}
				break;
			}
		}
		
		
		
		boolean gAllCredentialFound = false;
		
		TGTgRank newGrankAllCredential = new TGTgRank();

		if(PSPsInteractedWith.size() > 0)
		for(TGTgRank gRank: TGTg_allCredential)
			if(equalTargetedSPs_int_CaseSPs(gRank.getSPs(), PSPsInteractedWith))
			{
				if(spamRecieved)
					gRank.incrementCounter();		
				else
					gRank.decrementCounter();
								
				gAllCredentialFound = true;
				break;
			}
		
		if(!gAllCredentialFound && PSPsInteractedWith.size() > 0)
		{	
			for(CaseSPs targetedSP: PSPsInteractedWith)
				newGrankAllCredential.getSPs().add(targetedSP.getSPid());
	
			if(spamRecieved)
				newGrankAllCredential.incrementCounter();
			else
				newGrankAllCredential.decrementCounter();
			
			TGTg_allCredential.add(newGrankAllCredential);
		}

	}
	
	public void killSTagent(int SPid, boolean unsetSPR_STtested)
	{
		Auditor_STNodesDir stNode = new Auditor_STNodesDir();
		
		User_Node user;// = new User_Node(thePrefix);
		
		TGRRank rank = new TGRRank();
		for(int i = 0; i < TGR.size(); i++)
		{
			rank = TGR.get(i);
			if(rank.getSPid() == SPid)
			{
				if(rank.getSTLocator() < 0)
					break;
				
				//if(rank.getSTLocator() <= 0)
					//System.out.println("rank for SPid: "+rank.getSPid()+" has STLocator: "+rank.getSTLocator());
				
				stNode = stNodes.get(rank.getSTLocator());
				
				user = (User_Node) Network.get(stNode.getNodePos()).getProtocol(UserPid);
				
				//System.out.println("Auditor is killing STagent: "+user.getNodePos()+" corresponding to SPid: "+SPid);
				
				rank.setSTLocator(-1);
				
				stNode.setSPID(-1);
				stNode.setSTstatus(3);
				
				user.setSTstatus(STagentStatusPostSpam);
				
				break;
			}
		}
		
		if(unsetSPR_STtested)
			for(SPRrecord sprRecord: SPR)
				if(sprRecord.getSPid() == SPid)
				{
					sprRecord.setSTtested(false);
					break;
				}
			
		
	}
	
	public void recieveGTcreationUpdate(List<Auditor_GTNodesDir> newGTs)
	{
		for(Auditor_GTNodesDir newGT: newGTs)
		{
			for(Auditor_GTNodesDir gtRecord: gtNodes)
			{
				if(gtRecord.getGTid() == newGT.getGTid())
				{
					gtRecord.setCreatedOn(newGT.getCreatedOn());
					gtRecord.setNodePos(newGT.getNodePos());
					gtRecord.setGTstatus(2);
					break;
				}
			}
		}
	}

	public void recieveGTagentReport(int GTid, List<GT_TargetedSP> targetedSPs, int nodePos, boolean spamRecieved, List<CaseSPs> PSPsInteractedWith)
	{		
		
		List<GT_TargetedSP> interactedWithSPs = new ArrayList<GT_TargetedSP>(targetedSPs.size());
		
		for(GT_TargetedSP targetedSP: targetedSPs)
			if(targetedSP.isInteractedWith()) interactedWithSPs.add(targetedSP);
		
		//System.out.println("GT Agent Report: spam? "+spamRecieved+" GTid: "+GTid+" nodePos: "+nodePos+" TargetedSPs size: "+targetedSPs.size()+" SPs:");
		//for(GT_TargetedSP sp: targetedSPs)
			//System.out.println("SP: "+sp.getSPid()+" isInteractedWith? "+sp.isInteractedWith());
		
		
		boolean gFound = false;
		
		TGTgRank newGrank = new TGTgRank();
		

		if(interactedWithSPs.size() == 1 && PSPsInteractedWith.size() == 1)
		{
			for(TGRRank tgrRank: TGR)
			{
				if(tgrRank.getSPid() == interactedWithSPs.get(0).getSPid())
				{
					if(spamRecieved && !tgrRank.isInstalledDGU() && DeployDGU)							
					{
						SP_Node suspiciousSP = (SP_Node) Network.get(tgrRank.getSPid()).getProtocol(SPPid);
						
						boolean acceptedDGUoffer = suspiciousSP.acceptDGUoffer(false);
						
						if(acceptedDGUoffer)
							updateSPnodeStatus(tgrRank.getSPid(), 1, 6);
						else
						{
							tgrRank.setTSTRank(0);	
							banSP(tgrRank.getSPid(), 6);
						}
					}
					else if(spamRecieved && !DeployDGU)							
					{
						tgrRank.setTSTRank(0);	
						banSP(tgrRank.getSPid(), 6);
					}
					else if(!spamRecieved)
					{
						boolean spFound = false;
						
						for(TSTRank tstRank: TST)
						{
							if(tgrRank.getSPid() == tstRank.getSPid())
							{
								tstRank.addTstRank(100);
								spFound = true;
								break;
							}
						}
						
						if(!spFound)
						{
							TSTRank tstRank = new TSTRank();
							tstRank.setSPid(tgrRank.getSPid());
							tstRank.addTstRank(100);
							
							SP_Node spNode = (SP_Node) Network.get(tgrRank.getSPid()).getProtocol(SPPid);
							tstRank.setSpUsefulness(spNode.getUsefulness());

							TST.add(tstRank);
						}
					}
					break;
				}
			}
		}			

		if(interactedWithSPs.size() > 1)
		for(TGTgRank gRank: TGTg)
			if(equalTargetedSPs_int_GT(gRank.getSPs(), interactedWithSPs))
			{
				if(spamRecieved)
					gRank.incrementCounter();		
				else
					gRank.decrementCounter();
								
				gFound = true;
				break;
			}
		
		if(!gFound && interactedWithSPs.size() > 1)
		{	
			for(GT_TargetedSP targetedSP: interactedWithSPs)
				newGrank.getSPs().add(targetedSP.getSPid());
	
			if(spamRecieved)
				newGrank.incrementCounter();
			else
				newGrank.decrementCounter();
			
			TGTg.add(newGrank);
		}
		
				
		
		boolean gAllCredentialFound = false;
		
		TGTgRank newGrankAllCredential = new TGTgRank();

		if(PSPsInteractedWith.size() > 0)
		for(TGTgRank gRank: TGTg_allCredential)
			if(equalTargetedSPs_int_CaseSPs(gRank.getSPs(), PSPsInteractedWith))
			{
				if(spamRecieved)
					gRank.incrementCounter();		
				else
					gRank.decrementCounter();
								
				gAllCredentialFound = true;
				break;
			}
		
		if(!gAllCredentialFound && PSPsInteractedWith.size() > 0)
		{	
			for(CaseSPs targetedSP: PSPsInteractedWith)
				newGrankAllCredential.getSPs().add(targetedSP.getSPid());
	
			if(spamRecieved)
				newGrankAllCredential.incrementCounter();
			else
				newGrankAllCredential.decrementCounter();
			
			TGTg_allCredential.add(newGrankAllCredential);
		}

		
		if(spamRecieved) 
		{
			/*
			System.out.println("Auditor recieved SPAM report by GTagent: "+nodePos+" from SPs: ");
			for(GT_TargetedSP sp: interactedWithSPs)
				System.out.println("--SP: "+sp.getSPid());
			
			System.out.println("newGrank.isMinRankReached():"+newGrank.isMinRankReached() );
			*/
			killGTagent(GTid);
			
		}
		
		/*
		else
		{
			System.out.println("Auditor recieved a NO SPAM report by GTagent: "+nodePos+" from SPs: ");
			for(GT_TargetedSP sp: interactedWithSPs)
				System.out.println("--SP: "+sp.getSPid());
		}
		*/
	}
	
	public void killGTagent(int GTid)
	{
		Auditor_GTNodesDir gtNode = new Auditor_GTNodesDir();
		
		User_Node user = new User_Node(thePrefix);
		
		for(int i = 0; i < gtNodes.size(); i++)
		{
			gtNode = gtNodes.get(i);
			
			if(gtNode.getGTid() == GTid)
			{
				user = (User_Node) Network.get(gtNode.getNodePos()).getProtocol(UserPid);
				break;
			}
		}
		
		/*			
		System.out.println("Auditor is killing GTagent: "+user.getNodePos()+" corresponding to SPs: ");
		for(GT_TargetedSP sp: gtNode.getTargetedSPs())
			System.out.println("--SP: "+sp.getSPid());
		*/
		
		if(gtNode.getGTtype() == 1)
		{
			for(Auditor_SPsNodesDir spNode: suspiciousSPNodes)
				for(int i = 0; i < spNode.getSuspiciousGTagents().size(); i++)
				{
					Integer spGT = spNode.getSuspiciousGTagents().get(i);
			
					if(spGT == GTid)
					{
						spNode.getSuspiciousGTagents().remove(i);
						break;
					}
				}
		}
		else if(gtNode.getGTtype() == 2)
		{
			for(SPRrecord spRecord: SPR)
				for(int i = 0; i < spRecord.getPslGTagents().size(); i++)
				{
					Integer pslGT = spRecord.getPslGTagents().get(i);
			
					if(pslGT == GTid)
					{
						spRecord.getPslGTagents().remove(i);
						break;
					}
				}
		}
		else if(gtNode.getGTtype() == 3)
		{
			for(PIILrecord piilRecord: PIILs)
				for(int i = 0; i < piilRecord.getPiilGTagents().size(); i++)
				{
					Integer piilGT = piilRecord.getPiilGTagents().get(i);
			
					if(piilGT == GTid)
					{
						piilRecord.getPiilGTagents().remove(i);
						break;
					}
				}
		}
		
		gtNode.setGTtype(-1);
		gtNode.setTargetedSPs(null);
		gtNode.setGTstatus(3);
					
		user.setGTstatus(GTagentStatusPostSpam);
		
	}


	public boolean equalTargetedSPs(List<GT_TargetedSP> targetedSPs, List<GT_TargetedSP> otherTargetedSPs)
	{	
		if(targetedSPs.size() != otherTargetedSPs.size())
			return false;
		
		for(GT_TargetedSP sp: targetedSPs)
		{
			boolean spFound = false;
			
			for(GT_TargetedSP other_sp: otherTargetedSPs)
			{
				if(sp.getSPid() == other_sp.getSPid())
				{
					spFound = true;
					break;
				}
			}
			
			if(!spFound)
				return false;
		}
		
		
		return true;
	}
	
	public boolean equalTargetedSPs_int_GT(List<Integer> targetedSPs, List<GT_TargetedSP> otherTargetedSPs)
	{
		if(targetedSPs.size() != otherTargetedSPs.size())
			return false;
		
		for(Integer sp: targetedSPs)
		{
			boolean spFound = false;
			
			for(GT_TargetedSP other_sp: otherTargetedSPs)
			{
				if(sp == other_sp.getSPid())
				{
					spFound = true;
					break;
				}
			}
			
			if(!spFound)
				return false;
		}
		
		return true;
	}
	
	public boolean equalTargetedSPs_int_int(List<Integer> targetedSPs, List<Integer> otherTargetedSPs)
	{
		if(targetedSPs.size() != otherTargetedSPs.size())
			return false;
		
		for(Integer sp: targetedSPs)
		{
			boolean spFound = false;
			
			for(Integer other_sp: otherTargetedSPs)
			{
				if(sp == other_sp)
				{
					spFound = true;
					break;
				}
			}
			
			if(!spFound)
				return false;
		}
		
		return true;
	}
	
	public boolean equalTargetedSPs_int_CaseSPs(List<Integer> targetedSPs, List<CaseSPs> otherTargetedSPs)
	{
		if(targetedSPs.size() != otherTargetedSPs.size())
			return false;
		
		for(Integer sp: targetedSPs)
		{
			boolean spFound = false;
			
			for(CaseSPs other_sp: otherTargetedSPs)
			{
				if(sp == other_sp.getSPid())
				{
					spFound = true;
					break;
				}
			}
			
			if(!spFound)
				return false;
		}
		
		return true;
	}
	
	public boolean containTargetedSPs_int_int(List<Integer> mainSPs, List<Integer> testedSPs)
	{
		if(mainSPs.size() < testedSPs.size())
			return false;
		
		for(Integer sp: testedSPs)
		{
			boolean spFound = false;
			
			for(Integer other_sp: mainSPs)
			{
				if(sp == other_sp)
				{
					spFound = true;
					break;
				}
			}
			
			if(!spFound)
				return false;
		}
		
		return true;
	}
	
	public List<Auditor_UsersNodesDir> getuNodes() {
		return uNodes;
	}

	public void setuNodes(List<Auditor_UsersNodesDir> uNodes) {
		this.uNodes = uNodes;
	}

	public List<TLRavgRank> getTLRavg() {
		return TLRavg;
	}

	public void setTLRavg(List<TLRavgRank> tLRavg) {
		TLRavg = tLRavg;
	}

/*	public List<Auditor_SPsNodesDir> getSPNodes() {
		return SPNodes;
	}

	public void setSPNodes(List<Auditor_SPsNodesDir> sPNodes) {
		SPNodes = sPNodes;
	}
*/
	public List<Auditor_SPsNodesDir> getSuspiciousSPNodes() {
		return suspiciousSPNodes;
	}

	public void setSuspiciousSPNodes(List<Auditor_SPsNodesDir> suspiciousSPNodes) {
		this.suspiciousSPNodes = suspiciousSPNodes;
	}

	public double getSuspiciousSPRank() {
		return suspiciousSPRank;
	}

	public void setSuspiciousSPRank(double suspiciousSPRank) {
		this.suspiciousSPRank = suspiciousSPRank;
	}

	public double getSuspiciousSPRange() {
		return suspiciousSPRange;
	}

	public void setSuspiciousSPRange(double suspiciousSPRange) {
		this.suspiciousSPRange = suspiciousSPRange;
	}

	public double getSuspiciousSPBanningRank() {
		return suspiciousSPBanningRank;
	}

	public void setSuspiciousSPBanningRank(double suspiciousSPBanningRank) {
		this.suspiciousSPBanningRank = suspiciousSPBanningRank;
	}


	public List<Auditor_SPsNodesDir> getBannedSPNodes() {
		return bannedSPNodes;
	}

	public void setBannedSPNodes(List<Auditor_SPsNodesDir> bannedSPNodes) {
		this.bannedSPNodes = bannedSPNodes;
	}

	public List<TGRRank> getTGR() {
		return TGR;
	}

	public void setTGR(List<TGRRank> tGR) {
		TGR = tGR;
	}

	public List<Auditor_STNodesDir> getStNodes() {
		return stNodes;
	}

	public void setStNodes(List<Auditor_STNodesDir> stNodes) {
		this.stNodes = stNodes;
	}

	public int getTopTLRavgPCT_ST() {
		return TopTLRavgPCT_ST;
	}

	public void setTopTLRavgPCT_ST(int TopTLRavgPCT_ST) {
		this.TopTLRavgPCT_ST = TopTLRavgPCT_ST;
	}

	public int getSufficientTLRus_SP() {
		return SufficientTLRus_SP;
	}

	public void setSufficientTLRus_SP(int sufficientTLRus_SP) {
		SufficientTLRus_SP = sufficientTLRus_SP;
	}

	public int getSufficientTLRus_PSP() {
		return SufficientTLRus_PSP;
	}

	public void setSufficientTLRus_PSP(int sufficientTLRus_PSP) {
		SufficientTLRus_PSP = sufficientTLRus_PSP;
	}

	public List<SPRrecord> getSPR() {
		return SPR;
	}

	public void setSPR(List<SPRrecord> sPR) {
		SPR = sPR;
	}

	public double getPSL_PCT() {
		return PSL_PCT;
	}

	public void setPSL_PCT(double pSL_PCT) {
		PSL_PCT = pSL_PCT;
	}

	public int getSTmaxLifeTime() {
		return STmaxLifeTime;
	}

	public void setSTmaxLifeTime(int sTmaxLifeTime) {
		STmaxLifeTime = sTmaxLifeTime;
	}

	public int getSTagentStatusPostSpam() {
		return STagentStatusPostSpam;
	}

	public void setSTagentStatusPostSpam(int sTagentStatusPostSpam) {
		STagentStatusPostSpam = sTagentStatusPostSpam;
	}

	public int getBottomTLRavgPCT_ST() {
		return BottomTLRavgPCT_ST;
	}

	public void setBottomTLRavgPCT_ST(int bottomTLRavgPCT_ST) {
		BottomTLRavgPCT_ST = bottomTLRavgPCT_ST;
	}

	public boolean isPSL_STselector() {
		return PSL_STselector;
	}

	public void setPSL_STselector(boolean pSL_STselector) {
		PSL_STselector = pSL_STselector;
	}

	public boolean isSuspiciousNodes_STselector() {
		return SuspiciousNodes_STselector;
	}

	public void setSuspiciousNodes_STselector(boolean suspiciousNodes_STselector) {
		SuspiciousNodes_STselector = suspiciousNodes_STselector;
	}

	public boolean isTopTLRavg_STselector() {
		return TopTLRavg_STselector;
	}

	public void setTopTLRavg_STselector(boolean topTLRavg_STselector) {
		TopTLRavg_STselector = topTLRavg_STselector;
	}

	public boolean isBottomTLRavg_STselector() {
		return BottomTLRavg_STselector;
	}

	public void setBottomTLRavg_STselector(boolean bottomTLRavg_STselector) {
		BottomTLRavg_STselector = bottomTLRavg_STselector;
	}

	public double getTGTWeight() {
		return TGTWeight;
	}

	public void setTGTWeight(double tGTWeight) {
		TGTWeight = tGTWeight;
	}

	public double getTLRavgWeight() {
		return TLRavgWeight;
	}

	public void setTLRavgWeight(double tLRavgWeight) {
		TLRavgWeight = tLRavgWeight;
	}

	public double getTSTWeight() {
		return TSTWeight;
	}

	public void setTSTWeight(double tSTWeight) {
		TSTWeight = tSTWeight;
	}

	public boolean isDeployST() {
		return DeployST;
	}

	public void setDeployST(boolean deployST) {
		DeployST = deployST;
	}

	public List<IILrecord> getIIR() {
		return IIR;
	}

	public void setIIR(List<IILrecord> iIR) {
		IIR = iIR;
	}

	public List<PIILrecord> getPIILs() {
		return PIILs;
	}

	public void setPIILs(List<PIILrecord> pIILs) {
		PIILs = pIILs;
	}

	public double getPIIL_PCT() {
		return PIIL_PCT;
	}

	public void setPIIL_PCT(double pIIL_PCT) {
		PIIL_PCT = pIIL_PCT;
	}

	public boolean isPIIL_GTselector() {
		return PIIL_GTselector;
	}

	public void setPIIL_GTselector(boolean pIIL_GTselector) {
		PIIL_GTselector = pIIL_GTselector;
	}

	public boolean isIIR_STselector() {
		return IIR_STselector;
	}

	public void setIIR_STselector(boolean IIR_STselector) {
		this.IIR_STselector = IIR_STselector;
	}

	public boolean isPSL_GTselector() {
		return PSL_GTselector;
	}

	public void setPSL_GTselector(boolean pSL_GTselector) {
		PSL_GTselector = pSL_GTselector;
	}

	public boolean isDeployGT() {
		return DeployGT;
	}

	public void setDeployGT(boolean deployGT) {
		DeployGT = deployGT;
	}

	public int getGTmaxLifeTime() {
		return GTmaxLifeTime;
	}

	public void setGTmaxLifeTime(int gTmaxLifeTime) {
		GTmaxLifeTime = gTmaxLifeTime;
	}

	public int getGTagentStatusPostSpam() {
		return GTagentStatusPostSpam;
	}

	public void setGTagentStatusPostSpam(int gTagentStatusPostSpam) {
		GTagentStatusPostSpam = gTagentStatusPostSpam;
	}

	public boolean isSuspiciousNodes_GTselector() {
		return SuspiciousNodes_GTselector;
	}

	public void setSuspiciousNodes_GTselector(boolean suspiciousNodes_GTselector) {
		SuspiciousNodes_GTselector = suspiciousNodes_GTselector;
	}

	public List<TGTgRank> getTGTg() {
		return TGTg;
	}

	public void setTGTg(List<TGTgRank> tGTg) {
		TGTg = tGTg;
	}
	
	public void setGtNodes(List<Auditor_GTNodesDir> gtNodes) {
		this.gtNodes = gtNodes;
	}

	public List<Auditor_GTNodesDir> getGtNodes() {
		return gtNodes;
	}

	public boolean isGT_avgTGT() {
		return GT_avgTGT;
	}

	public void setGT_avgTGT(boolean gT_avgTGT) {
		GT_avgTGT = gT_avgTGT;
	}

	public List<PColludingRecord> getPColludingRecords() {
		return PColludingRecords;
	}

	public void setPColludingRecords(List<PColludingRecord> pColludingRecords) {
		PColludingRecords = pColludingRecords;
	}

	public double getPcrThreshold() {
		return pcrThreshold;
	}

	public void setPcrThreshold(double pcrThreshold) {
		this.pcrThreshold = pcrThreshold;
	}

	public boolean isIgnoreOldGranks() {
		return ignoreOldGranks;
	}

	public void setIgnoreOldGranks(boolean ignoreOldGranks) {
		this.ignoreOldGranks = ignoreOldGranks;
	}

	public boolean isStopTestingIfMinReached() {
		return stopTestingIfMinReached;
	}

	public void setStopTestingIfMinReached(boolean stopTestingIfMinReached) {
		this.stopTestingIfMinReached = stopTestingIfMinReached;
	}

	public boolean isGT_SelectOnlyPSP() {
		return GT_SelectOnlyPSP;
	}

	public void setGT_SelectOnlyPSP(boolean gT_SelectOnlyPSP) {
		GT_SelectOnlyPSP = gT_SelectOnlyPSP;
	}

	public int getGT_MaxSPNum() {
		return GT_MaxSPNum;
	}

	public void setGT_MaxSPNum(int gT_MaxSPNum) {
		GT_MaxSPNum = gT_MaxSPNum;
	}

	public List<TGTgRank> getTGTg_allCredential() {
		return TGTg_allCredential;
	}

	public void setTGTg_allCredential(List<TGTgRank> tGTg_allCredential) {
		TGTg_allCredential = tGTg_allCredential;
	}

	public List<PColludingRecord> getPWeakColludingRecords() {
		return PWeakColludingRecords;
	}

	public void setPWeakColludingRecords(List<PColludingRecord> pWeakColludingRecords) {
		PWeakColludingRecords = pWeakColludingRecords;
	}

	public double getPcrWeakThreshold() {
		return pcrWeakThreshold;
	}

	public void setPcrWeakThreshold(double pcrWeakThreshold) {
		this.pcrWeakThreshold = pcrWeakThreshold;
	}

	public double getTGTColludingWeight() {
		return TGTColludingWeight;
	}

	public void setTGTColludingWeight(double tGTColludingWeight) {
		TGTColludingWeight = tGTColludingWeight;
	}

	public double getTGTWeakColludingWeight() {
		return TGTWeakColludingWeight;
	}

	public void setTGTWeakColludingWeight(double tGTWeakColludingWeight) {
		TGTWeakColludingWeight = tGTWeakColludingWeight;
	}

	public double getTGTWholeWeight() {
		return TGTWholeWeight;
	}

	public void setTGTWholeWeight(double tGTWholeWeight) {
		TGTWholeWeight = tGTWholeWeight;
	}

	public List<Auditor_SPsNodesDir> getDGUSPNodes() {
		return DGUSPNodes;
	}

	public void setDGUSPNodes(List<Auditor_SPsNodesDir> dGUSPNodes) {
		DGUSPNodes = dGUSPNodes;
	}

	public double getPostDGUinstallationRank() {
		return postDGUinstallationRank;
	}

	public void setPostDGUinstallationRank(double postDGUinstallationRank) {
		this.postDGUinstallationRank = postDGUinstallationRank;
	}

	public double getPostStrictDGUEnableRank() {
		return postStrictDGUEnableRank;
	}

	public void setPostStrictDGUEnableRank(double postStrictDGUEnableRank) {
		this.postStrictDGUEnableRank = postStrictDGUEnableRank;
	}

	public List<TSTRank> getTST() {
		return TST;
	}

	public void setTST(List<TSTRank> tST) {
		TST = tST;
	}

	public int getGT_MaxSize() {
		return GT_MaxSize;
	}

	public void setGT_MaxSize(int gT_MaxSize) {
		GT_MaxSize = gT_MaxSize;
	}

	public boolean getDeployDGU() {
		return DeployDGU;
	}

	public void setDeployDGU(boolean deployDGU) {
		DeployDGU = deployDGU;
	}

	public double getAvgTimeToResolveCase_at_125() {
		return AvgTimeToResolveCase_at_125;
	}

	public void setAvgTimeToResolveCase_at_125(double avgTimeToResolveCase_at_125) {
		AvgTimeToResolveCase_at_125 = avgTimeToResolveCase_at_125;
	}

	public double getAvgTimeToResolveCase_at_250() {
		return AvgTimeToResolveCase_at_250;
	}

	public void setAvgTimeToResolveCase_at_250(double avgTimeToResolveCase_at_250) {
		AvgTimeToResolveCase_at_250 = avgTimeToResolveCase_at_250;
	}

	public double getAvgTimeToResolveCase_at_375() {
		return AvgTimeToResolveCase_at_375;
	}

	public void setAvgTimeToResolveCase_at_375(double avgTimeToResolveCase_at_375) {
		AvgTimeToResolveCase_at_375 = avgTimeToResolveCase_at_375;
	}

	public double getAvgTimeToResolveCase_at_500() {
		return AvgTimeToResolveCase_at_500;
	}

	public void setAvgTimeToResolveCase_at_500(double avgTimeToResolveCase_at_500) {
		AvgTimeToResolveCase_at_500 = avgTimeToResolveCase_at_500;
	}

	public double getPCT_Open_Cases_at_125() {
		return PCT_Open_Cases_at_125;
	}

	public void setPCT_Open_Cases_at_125(double pCT_Open_Cases_at_125) {
		PCT_Open_Cases_at_125 = pCT_Open_Cases_at_125;
	}

	public double getPCT_Open_Cases_at_250() {
		return PCT_Open_Cases_at_250;
	}

	public void setPCT_Open_Cases_at_250(double pCT_Open_Cases_at_250) {
		PCT_Open_Cases_at_250 = pCT_Open_Cases_at_250;
	}

	public double getPCT_Open_Cases_at_375() {
		return PCT_Open_Cases_at_375;
	}

	public void setPCT_Open_Cases_at_375(double pCT_Open_Cases_at_375) {
		PCT_Open_Cases_at_375 = pCT_Open_Cases_at_375;
	}

	public double getPCT_Open_Cases_at_500() {
		return PCT_Open_Cases_at_500;
	}

	public void setPCT_Open_Cases_at_500(double pCT_Open_Cases_at_500) {
		PCT_Open_Cases_at_500 = pCT_Open_Cases_at_500;
	}

	public double getPCT_PSPs_detected_by_TLRavg_Rank_at_125() {
		return PCT_PSPs_detected_by_TLRavg_Rank_at_125;
	}

	public void setPCT_PSPs_detected_by_TLRavg_Rank_at_125(
			double pCT_PSPs_detected_by_TLRavg_Rank_at_125) {
		PCT_PSPs_detected_by_TLRavg_Rank_at_125 = pCT_PSPs_detected_by_TLRavg_Rank_at_125;
	}

	public double getPCT_PSPs_detected_by_TLRavg_Rank_at_250() {
		return PCT_PSPs_detected_by_TLRavg_Rank_at_250;
	}

	public void setPCT_PSPs_detected_by_TLRavg_Rank_at_250(
			double pCT_PSPs_detected_by_TLRavg_Rank_at_250) {
		PCT_PSPs_detected_by_TLRavg_Rank_at_250 = pCT_PSPs_detected_by_TLRavg_Rank_at_250;
	}

	public double getPCT_PSPs_detected_by_TLRavg_Rank_at_375() {
		return PCT_PSPs_detected_by_TLRavg_Rank_at_375;
	}

	public void setPCT_PSPs_detected_by_TLRavg_Rank_at_375(
			double pCT_PSPs_detected_by_TLRavg_Rank_at_375) {
		PCT_PSPs_detected_by_TLRavg_Rank_at_375 = pCT_PSPs_detected_by_TLRavg_Rank_at_375;
	}

	public double getPCT_PSPs_detected_by_TLRavg_Rank_at_500() {
		return PCT_PSPs_detected_by_TLRavg_Rank_at_500;
	}

	public void setPCT_PSPs_detected_by_TLRavg_Rank_at_500(
			double pCT_PSPs_detected_by_TLRavg_Rank_at_500) {
		PCT_PSPs_detected_by_TLRavg_Rank_at_500 = pCT_PSPs_detected_by_TLRavg_Rank_at_500;
	}

	public double getPCT_PSPs_detected_by_TST_Rank_at_125() {
		return PCT_PSPs_detected_by_TST_Rank_at_125;
	}

	public void setPCT_PSPs_detected_by_TST_Rank_at_125(
			double pCT_PSPs_detected_by_TST_Rank_at_125) {
		PCT_PSPs_detected_by_TST_Rank_at_125 = pCT_PSPs_detected_by_TST_Rank_at_125;
	}

	public double getPCT_PSPs_detected_by_TST_Rank_at_250() {
		return PCT_PSPs_detected_by_TST_Rank_at_250;
	}

	public void setPCT_PSPs_detected_by_TST_Rank_at_250(
			double pCT_PSPs_detected_by_TST_Rank_at_250) {
		PCT_PSPs_detected_by_TST_Rank_at_250 = pCT_PSPs_detected_by_TST_Rank_at_250;
	}

	public double getPCT_PSPs_detected_by_TST_Rank_at_375() {
		return PCT_PSPs_detected_by_TST_Rank_at_375;
	}

	public void setPCT_PSPs_detected_by_TST_Rank_at_375(
			double pCT_PSPs_detected_by_TST_Rank_at_375) {
		PCT_PSPs_detected_by_TST_Rank_at_375 = pCT_PSPs_detected_by_TST_Rank_at_375;
	}

	public double getPCT_PSPs_detected_by_TST_Rank_at_500() {
		return PCT_PSPs_detected_by_TST_Rank_at_500;
	}

	public void setPCT_PSPs_detected_by_TST_Rank_at_500(
			double pCT_PSPs_detected_by_TST_Rank_at_500) {
		PCT_PSPs_detected_by_TST_Rank_at_500 = pCT_PSPs_detected_by_TST_Rank_at_500;
	}

	public double getPCT_PSPs_detected_by_Agent_at_125() {
		return PCT_PSPs_detected_by_Agent_at_125;
	}

	public void setPCT_PSPs_detected_by_Agent_at_125(
			double pCT_PSPs_detected_by_Agent_at_125) {
		PCT_PSPs_detected_by_Agent_at_125 = pCT_PSPs_detected_by_Agent_at_125;
	}

	public double getPCT_PSPs_detected_by_Agent_at_250() {
		return PCT_PSPs_detected_by_Agent_at_250;
	}

	public void setPCT_PSPs_detected_by_Agent_at_250(
			double pCT_PSPs_detected_by_Agent_at_250) {
		PCT_PSPs_detected_by_Agent_at_250 = pCT_PSPs_detected_by_Agent_at_250;
	}

	public double getPCT_PSPs_detected_by_Agent_at_375() {
		return PCT_PSPs_detected_by_Agent_at_375;
	}

	public void setPCT_PSPs_detected_by_Agent_at_375(
			double pCT_PSPs_detected_by_Agent_at_375) {
		PCT_PSPs_detected_by_Agent_at_375 = pCT_PSPs_detected_by_Agent_at_375;
	}

	public double getPCT_PSPs_detected_by_Agent_at_500() {
		return PCT_PSPs_detected_by_Agent_at_500;
	}

	public void setPCT_PSPs_detected_by_Agent_at_500(
			double pCT_PSPs_detected_by_Agent_at_500) {
		PCT_PSPs_detected_by_Agent_at_500 = pCT_PSPs_detected_by_Agent_at_500;
	}

	public double getPCT_PSPs_detected_by_Total_Rank_at_125() {
		return PCT_PSPs_detected_by_Total_Rank_at_125;
	}

	public void setPCT_PSPs_detected_by_Total_Rank_at_125(
			double pCT_PSPs_detected_by_Total_Rank_at_125) {
		PCT_PSPs_detected_by_Total_Rank_at_125 = pCT_PSPs_detected_by_Total_Rank_at_125;
	}

	public double getPCT_PSPs_detected_by_Total_Rank_at_250() {
		return PCT_PSPs_detected_by_Total_Rank_at_250;
	}

	public void setPCT_PSPs_detected_by_Total_Rank_at_250(
			double pCT_PSPs_detected_by_Total_Rank_at_250) {
		PCT_PSPs_detected_by_Total_Rank_at_250 = pCT_PSPs_detected_by_Total_Rank_at_250;
	}

	public double getPCT_PSPs_detected_by_Total_Rank_at_375() {
		return PCT_PSPs_detected_by_Total_Rank_at_375;
	}

	public void setPCT_PSPs_detected_by_Total_Rank_at_375(
			double pCT_PSPs_detected_by_Total_Rank_at_375) {
		PCT_PSPs_detected_by_Total_Rank_at_375 = pCT_PSPs_detected_by_Total_Rank_at_375;
	}

	public double getPCT_PSPs_detected_by_Total_Rank_at_500() {
		return PCT_PSPs_detected_by_Total_Rank_at_500;
	}

	public void setPCT_PSPs_detected_by_Total_Rank_at_500(
			double pCT_PSPs_detected_by_Total_Rank_at_500) {
		PCT_PSPs_detected_by_Total_Rank_at_500 = pCT_PSPs_detected_by_Total_Rank_at_500;
	}

	public double getPCT_SPs_detected_by_TLRavg_Rank_at_125() {
		return PCT_SPs_detected_by_TLRavg_Rank_at_125;
	}

	public void setPCT_SPs_detected_by_TLRavg_Rank_at_125(
			double pCT_SPs_detected_by_TLRavg_Rank_at_125) {
		PCT_SPs_detected_by_TLRavg_Rank_at_125 = pCT_SPs_detected_by_TLRavg_Rank_at_125;
	}

	public double getPCT_SPs_detected_by_TLRavg_Rank_at_250() {
		return PCT_SPs_detected_by_TLRavg_Rank_at_250;
	}

	public void setPCT_SPs_detected_by_TLRavg_Rank_at_250(
			double pCT_SPs_detected_by_TLRavg_Rank_at_250) {
		PCT_SPs_detected_by_TLRavg_Rank_at_250 = pCT_SPs_detected_by_TLRavg_Rank_at_250;
	}

	public double getPCT_SPs_detected_by_TLRavg_Rank_at_375() {
		return PCT_SPs_detected_by_TLRavg_Rank_at_375;
	}

	public void setPCT_SPs_detected_by_TLRavg_Rank_at_375(
			double pCT_SPs_detected_by_TLRavg_Rank_at_375) {
		PCT_SPs_detected_by_TLRavg_Rank_at_375 = pCT_SPs_detected_by_TLRavg_Rank_at_375;
	}

	public double getPCT_SPs_detected_by_TLRavg_Rank_at_500() {
		return PCT_SPs_detected_by_TLRavg_Rank_at_500;
	}

	public void setPCT_SPs_detected_by_TLRavg_Rank_at_500(
			double pCT_SPs_detected_by_TLRavg_Rank_at_500) {
		PCT_SPs_detected_by_TLRavg_Rank_at_500 = pCT_SPs_detected_by_TLRavg_Rank_at_500;
	}

	public double getPCT_SPs_detected_by_TST_Rank_at_125() {
		return PCT_SPs_detected_by_TST_Rank_at_125;
	}

	public void setPCT_SPs_detected_by_TST_Rank_at_125(
			double pCT_SPs_detected_by_TST_Rank_at_125) {
		PCT_SPs_detected_by_TST_Rank_at_125 = pCT_SPs_detected_by_TST_Rank_at_125;
	}

	public double getPCT_SPs_detected_by_TST_Rank_at_250() {
		return PCT_SPs_detected_by_TST_Rank_at_250;
	}

	public void setPCT_SPs_detected_by_TST_Rank_at_250(
			double pCT_SPs_detected_by_TST_Rank_at_250) {
		PCT_SPs_detected_by_TST_Rank_at_250 = pCT_SPs_detected_by_TST_Rank_at_250;
	}

	public double getPCT_SPs_detected_by_TST_Rank_at_375() {
		return PCT_SPs_detected_by_TST_Rank_at_375;
	}

	public void setPCT_SPs_detected_by_TST_Rank_at_375(
			double pCT_SPs_detected_by_TST_Rank_at_375) {
		PCT_SPs_detected_by_TST_Rank_at_375 = pCT_SPs_detected_by_TST_Rank_at_375;
	}

	public double getPCT_SPs_detected_by_TST_Rank_at_500() {
		return PCT_SPs_detected_by_TST_Rank_at_500;
	}

	public void setPCT_SPs_detected_by_TST_Rank_at_500(
			double pCT_SPs_detected_by_TST_Rank_at_500) {
		PCT_SPs_detected_by_TST_Rank_at_500 = pCT_SPs_detected_by_TST_Rank_at_500;
	}

	public double getPCT_SPs_detected_by_Agent_at_125() {
		return PCT_SPs_detected_by_Agent_at_125;
	}

	public void setPCT_SPs_detected_by_Agent_at_125(
			double pCT_SPs_detected_by_Agent_at_125) {
		PCT_SPs_detected_by_Agent_at_125 = pCT_SPs_detected_by_Agent_at_125;
	}

	public double getPCT_SPs_detected_by_Agent_at_250() {
		return PCT_SPs_detected_by_Agent_at_250;
	}

	public void setPCT_SPs_detected_by_Agent_at_250(
			double pCT_SPs_detected_by_Agent_at_250) {
		PCT_SPs_detected_by_Agent_at_250 = pCT_SPs_detected_by_Agent_at_250;
	}

	public double getPCT_SPs_detected_by_Agent_at_375() {
		return PCT_SPs_detected_by_Agent_at_375;
	}

	public void setPCT_SPs_detected_by_Agent_at_375(
			double pCT_SPs_detected_by_Agent_at_375) {
		PCT_SPs_detected_by_Agent_at_375 = pCT_SPs_detected_by_Agent_at_375;
	}

	public double getPCT_SPs_detected_by_Agent_at_500() {
		return PCT_SPs_detected_by_Agent_at_500;
	}

	public void setPCT_SPs_detected_by_Agent_at_500(
			double pCT_SPs_detected_by_Agent_at_500) {
		PCT_SPs_detected_by_Agent_at_500 = pCT_SPs_detected_by_Agent_at_500;
	}

	public double getPCT_SPs_detected_by_Total_Rank_at_125() {
		return PCT_SPs_detected_by_Total_Rank_at_125;
	}

	public void setPCT_SPs_detected_by_Total_Rank_at_125(
			double pCT_SPs_detected_by_Total_Rank_at_125) {
		PCT_SPs_detected_by_Total_Rank_at_125 = pCT_SPs_detected_by_Total_Rank_at_125;
	}

	public double getPCT_SPs_detected_by_Total_Rank_at_250() {
		return PCT_SPs_detected_by_Total_Rank_at_250;
	}

	public void setPCT_SPs_detected_by_Total_Rank_at_250(
			double pCT_SPs_detected_by_Total_Rank_at_250) {
		PCT_SPs_detected_by_Total_Rank_at_250 = pCT_SPs_detected_by_Total_Rank_at_250;
	}

	public double getPCT_SPs_detected_by_Total_Rank_at_375() {
		return PCT_SPs_detected_by_Total_Rank_at_375;
	}

	public void setPCT_SPs_detected_by_Total_Rank_at_375(
			double pCT_SPs_detected_by_Total_Rank_at_375) {
		PCT_SPs_detected_by_Total_Rank_at_375 = pCT_SPs_detected_by_Total_Rank_at_375;
	}

	public double getPCT_SPs_detected_by_Total_Rank_at_500() {
		return PCT_SPs_detected_by_Total_Rank_at_500;
	}

	public void setPCT_SPs_detected_by_Total_Rank_at_500(
			double pCT_SPs_detected_by_Total_Rank_at_500) {
		PCT_SPs_detected_by_Total_Rank_at_500 = pCT_SPs_detected_by_Total_Rank_at_500;
	}

	public double getPCT_MPSPs_detected_by_TLRavg_Rank_at_125() {
		return PCT_MPSPs_detected_by_TLRavg_Rank_at_125;
	}

	public void setPCT_MPSPs_detected_by_TLRavg_Rank_at_125(
			double pCT_MPSPs_detected_by_TLRavg_Rank_at_125) {
		PCT_MPSPs_detected_by_TLRavg_Rank_at_125 = pCT_MPSPs_detected_by_TLRavg_Rank_at_125;
	}

	public double getPCT_MPSPs_detected_by_TLRavg_Rank_at_250() {
		return PCT_MPSPs_detected_by_TLRavg_Rank_at_250;
	}

	public void setPCT_MPSPs_detected_by_TLRavg_Rank_at_250(
			double pCT_MPSPs_detected_by_TLRavg_Rank_at_250) {
		PCT_MPSPs_detected_by_TLRavg_Rank_at_250 = pCT_MPSPs_detected_by_TLRavg_Rank_at_250;
	}

	public double getPCT_MPSPs_detected_by_TLRavg_Rank_at_375() {
		return PCT_MPSPs_detected_by_TLRavg_Rank_at_375;
	}

	public void setPCT_MPSPs_detected_by_TLRavg_Rank_at_375(
			double pCT_MPSPs_detected_by_TLRavg_Rank_at_375) {
		PCT_MPSPs_detected_by_TLRavg_Rank_at_375 = pCT_MPSPs_detected_by_TLRavg_Rank_at_375;
	}

	public double getPCT_MPSPs_detected_by_TLRavg_Rank_at_500() {
		return PCT_MPSPs_detected_by_TLRavg_Rank_at_500;
	}

	public void setPCT_MPSPs_detected_by_TLRavg_Rank_at_500(
			double pCT_MPSPs_detected_by_TLRavg_Rank_at_500) {
		PCT_MPSPs_detected_by_TLRavg_Rank_at_500 = pCT_MPSPs_detected_by_TLRavg_Rank_at_500;
	}

	public double getPCT_MPSPs_detected_by_TST_Rank_at_125() {
		return PCT_MPSPs_detected_by_TST_Rank_at_125;
	}

	public void setPCT_MPSPs_detected_by_TST_Rank_at_125(
			double pCT_MPSPs_detected_by_TST_Rank_at_125) {
		PCT_MPSPs_detected_by_TST_Rank_at_125 = pCT_MPSPs_detected_by_TST_Rank_at_125;
	}

	public double getPCT_MPSPs_detected_by_TST_Rank_at_250() {
		return PCT_MPSPs_detected_by_TST_Rank_at_250;
	}

	public void setPCT_MPSPs_detected_by_TST_Rank_at_250(
			double pCT_MPSPs_detected_by_TST_Rank_at_250) {
		PCT_MPSPs_detected_by_TST_Rank_at_250 = pCT_MPSPs_detected_by_TST_Rank_at_250;
	}

	public double getPCT_MPSPs_detected_by_TST_Rank_at_375() {
		return PCT_MPSPs_detected_by_TST_Rank_at_375;
	}

	public void setPCT_MPSPs_detected_by_TST_Rank_at_375(
			double pCT_MPSPs_detected_by_TST_Rank_at_375) {
		PCT_MPSPs_detected_by_TST_Rank_at_375 = pCT_MPSPs_detected_by_TST_Rank_at_375;
	}

	public double getPCT_MPSPs_detected_by_TST_Rank_at_500() {
		return PCT_MPSPs_detected_by_TST_Rank_at_500;
	}

	public void setPCT_MPSPs_detected_by_TST_Rank_at_500(
			double pCT_MPSPs_detected_by_TST_Rank_at_500) {
		PCT_MPSPs_detected_by_TST_Rank_at_500 = pCT_MPSPs_detected_by_TST_Rank_at_500;
	}

	public double getPCT_MPSPs_detected_by_Agent_at_125() {
		return PCT_MPSPs_detected_by_Agent_at_125;
	}

	public void setPCT_MPSPs_detected_by_Agent_at_125(
			double pCT_MPSPs_detected_by_Agent_at_125) {
		PCT_MPSPs_detected_by_Agent_at_125 = pCT_MPSPs_detected_by_Agent_at_125;
	}

	public double getPCT_MPSPs_detected_by_Agent_at_250() {
		return PCT_MPSPs_detected_by_Agent_at_250;
	}

	public void setPCT_MPSPs_detected_by_Agent_at_250(
			double pCT_MPSPs_detected_by_Agent_at_250) {
		PCT_MPSPs_detected_by_Agent_at_250 = pCT_MPSPs_detected_by_Agent_at_250;
	}

	public double getPCT_MPSPs_detected_by_Agent_at_375() {
		return PCT_MPSPs_detected_by_Agent_at_375;
	}

	public void setPCT_MPSPs_detected_by_Agent_at_375(
			double pCT_MPSPs_detected_by_Agent_at_375) {
		PCT_MPSPs_detected_by_Agent_at_375 = pCT_MPSPs_detected_by_Agent_at_375;
	}

	public double getPCT_MPSPs_detected_by_Agent_at_500() {
		return PCT_MPSPs_detected_by_Agent_at_500;
	}

	public void setPCT_MPSPs_detected_by_Agent_at_500(
			double pCT_MPSPs_detected_by_Agent_at_500) {
		PCT_MPSPs_detected_by_Agent_at_500 = pCT_MPSPs_detected_by_Agent_at_500;
	}

	public double getPCT_MPSPs_detected_by_Total_Rank_at_125() {
		return PCT_MPSPs_detected_by_Total_Rank_at_125;
	}

	public void setPCT_MPSPs_detected_by_Total_Rank_at_125(
			double pCT_MPSPs_detected_by_Total_Rank_at_125) {
		PCT_MPSPs_detected_by_Total_Rank_at_125 = pCT_MPSPs_detected_by_Total_Rank_at_125;
	}

	public double getPCT_MPSPs_detected_by_Total_Rank_at_250() {
		return PCT_MPSPs_detected_by_Total_Rank_at_250;
	}

	public void setPCT_MPSPs_detected_by_Total_Rank_at_250(
			double pCT_MPSPs_detected_by_Total_Rank_at_250) {
		PCT_MPSPs_detected_by_Total_Rank_at_250 = pCT_MPSPs_detected_by_Total_Rank_at_250;
	}

	public double getPCT_MPSPs_detected_by_Total_Rank_at_375() {
		return PCT_MPSPs_detected_by_Total_Rank_at_375;
	}

	public void setPCT_MPSPs_detected_by_Total_Rank_at_375(
			double pCT_MPSPs_detected_by_Total_Rank_at_375) {
		PCT_MPSPs_detected_by_Total_Rank_at_375 = pCT_MPSPs_detected_by_Total_Rank_at_375;
	}

	public double getPCT_MPSPs_detected_by_Total_Rank_at_500() {
		return PCT_MPSPs_detected_by_Total_Rank_at_500;
	}

	public void setPCT_MPSPs_detected_by_Total_Rank_at_500(
			double pCT_MPSPs_detected_by_Total_Rank_at_500) {
		PCT_MPSPs_detected_by_Total_Rank_at_500 = pCT_MPSPs_detected_by_Total_Rank_at_500;
	}

	public double getPCT_MSPs_detected_by_TLRavg_Rank_at_125() {
		return PCT_MSPs_detected_by_TLRavg_Rank_at_125;
	}

	public void setPCT_MSPs_detected_by_TLRavg_Rank_at_125(
			double pCT_MSPs_detected_by_TLRavg_Rank_at_125) {
		PCT_MSPs_detected_by_TLRavg_Rank_at_125 = pCT_MSPs_detected_by_TLRavg_Rank_at_125;
	}

	public double getPCT_MSPs_detected_by_TLRavg_Rank_at_250() {
		return PCT_MSPs_detected_by_TLRavg_Rank_at_250;
	}

	public void setPCT_MSPs_detected_by_TLRavg_Rank_at_250(
			double pCT_MSPs_detected_by_TLRavg_Rank_at_250) {
		PCT_MSPs_detected_by_TLRavg_Rank_at_250 = pCT_MSPs_detected_by_TLRavg_Rank_at_250;
	}

	public double getPCT_MSPs_detected_by_TLRavg_Rank_at_375() {
		return PCT_MSPs_detected_by_TLRavg_Rank_at_375;
	}

	public void setPCT_MSPs_detected_by_TLRavg_Rank_at_375(
			double pCT_MSPs_detected_by_TLRavg_Rank_at_375) {
		PCT_MSPs_detected_by_TLRavg_Rank_at_375 = pCT_MSPs_detected_by_TLRavg_Rank_at_375;
	}

	public double getPCT_MSPs_detected_by_TLRavg_Rank_at_500() {
		return PCT_MSPs_detected_by_TLRavg_Rank_at_500;
	}

	public void setPCT_MSPs_detected_by_TLRavg_Rank_at_500(
			double pCT_MSPs_detected_by_TLRavg_Rank_at_500) {
		PCT_MSPs_detected_by_TLRavg_Rank_at_500 = pCT_MSPs_detected_by_TLRavg_Rank_at_500;
	}

	public double getPCT_MSPs_detected_by_TST_Rank_at_125() {
		return PCT_MSPs_detected_by_TST_Rank_at_125;
	}

	public void setPCT_MSPs_detected_by_TST_Rank_at_125(
			double pCT_MSPs_detected_by_TST_Rank_at_125) {
		PCT_MSPs_detected_by_TST_Rank_at_125 = pCT_MSPs_detected_by_TST_Rank_at_125;
	}

	public double getPCT_MSPs_detected_by_TST_Rank_at_250() {
		return PCT_MSPs_detected_by_TST_Rank_at_250;
	}

	public void setPCT_MSPs_detected_by_TST_Rank_at_250(
			double pCT_MSPs_detected_by_TST_Rank_at_250) {
		PCT_MSPs_detected_by_TST_Rank_at_250 = pCT_MSPs_detected_by_TST_Rank_at_250;
	}

	public double getPCT_MSPs_detected_by_TST_Rank_at_375() {
		return PCT_MSPs_detected_by_TST_Rank_at_375;
	}

	public void setPCT_MSPs_detected_by_TST_Rank_at_375(
			double pCT_MSPs_detected_by_TST_Rank_at_375) {
		PCT_MSPs_detected_by_TST_Rank_at_375 = pCT_MSPs_detected_by_TST_Rank_at_375;
	}

	public double getPCT_MSPs_detected_by_TST_Rank_at_500() {
		return PCT_MSPs_detected_by_TST_Rank_at_500;
	}

	public void setPCT_MSPs_detected_by_TST_Rank_at_500(
			double pCT_MSPs_detected_by_TST_Rank_at_500) {
		PCT_MSPs_detected_by_TST_Rank_at_500 = pCT_MSPs_detected_by_TST_Rank_at_500;
	}

	public double getPCT_MSPs_detected_by_Agent_at_125() {
		return PCT_MSPs_detected_by_Agent_at_125;
	}

	public void setPCT_MSPs_detected_by_Agent_at_125(
			double pCT_MSPs_detected_by_Agent_at_125) {
		PCT_MSPs_detected_by_Agent_at_125 = pCT_MSPs_detected_by_Agent_at_125;
	}

	public double getPCT_MSPs_detected_by_Agent_at_250() {
		return PCT_MSPs_detected_by_Agent_at_250;
	}

	public void setPCT_MSPs_detected_by_Agent_at_250(
			double pCT_MSPs_detected_by_Agent_at_250) {
		PCT_MSPs_detected_by_Agent_at_250 = pCT_MSPs_detected_by_Agent_at_250;
	}

	public double getPCT_MSPs_detected_by_Agent_at_375() {
		return PCT_MSPs_detected_by_Agent_at_375;
	}

	public void setPCT_MSPs_detected_by_Agent_at_375(
			double pCT_MSPs_detected_by_Agent_at_375) {
		PCT_MSPs_detected_by_Agent_at_375 = pCT_MSPs_detected_by_Agent_at_375;
	}

	public double getPCT_MSPs_detected_by_Agent_at_500() {
		return PCT_MSPs_detected_by_Agent_at_500;
	}

	public void setPCT_MSPs_detected_by_Agent_at_500(
			double pCT_MSPs_detected_by_Agent_at_500) {
		PCT_MSPs_detected_by_Agent_at_500 = pCT_MSPs_detected_by_Agent_at_500;
	}

	public double getPCT_MSPs_detected_by_Total_Rank_at_125() {
		return PCT_MSPs_detected_by_Total_Rank_at_125;
	}

	public void setPCT_MSPs_detected_by_Total_Rank_at_125(
			double pCT_MSPs_detected_by_Total_Rank_at_125) {
		PCT_MSPs_detected_by_Total_Rank_at_125 = pCT_MSPs_detected_by_Total_Rank_at_125;
	}

	public double getPCT_MSPs_detected_by_Total_Rank_at_250() {
		return PCT_MSPs_detected_by_Total_Rank_at_250;
	}

	public void setPCT_MSPs_detected_by_Total_Rank_at_250(
			double pCT_MSPs_detected_by_Total_Rank_at_250) {
		PCT_MSPs_detected_by_Total_Rank_at_250 = pCT_MSPs_detected_by_Total_Rank_at_250;
	}

	public double getPCT_MSPs_detected_by_Total_Rank_at_375() {
		return PCT_MSPs_detected_by_Total_Rank_at_375;
	}

	public void setPCT_MSPs_detected_by_Total_Rank_at_375(
			double pCT_MSPs_detected_by_Total_Rank_at_375) {
		PCT_MSPs_detected_by_Total_Rank_at_375 = pCT_MSPs_detected_by_Total_Rank_at_375;
	}

	public double getPCT_MSPs_detected_by_Total_Rank_at_500() {
		return PCT_MSPs_detected_by_Total_Rank_at_500;
	}

	public void setPCT_MSPs_detected_by_Total_Rank_at_500(
			double pCT_MSPs_detected_by_Total_Rank_at_500) {
		PCT_MSPs_detected_by_Total_Rank_at_500 = pCT_MSPs_detected_by_Total_Rank_at_500;
	}

	public double getPCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_125() {
		return PCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_125;
	}

	public void setPCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_125(
			double pCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_125) {
		PCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_125 = pCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_125;
	}

	public double getPCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_250() {
		return PCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_250;
	}

	public void setPCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_250(
			double pCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_250) {
		PCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_250 = pCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_250;
	}

	public double getPCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_375() {
		return PCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_375;
	}

	public void setPCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_375(
			double pCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_375) {
		PCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_375 = pCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_375;
	}

	public double getPCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_500() {
		return PCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_500;
	}

	public void setPCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_500(
			double pCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_500) {
		PCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_500 = pCT_Popular_MPSPs_detected_by_TLRavg_Rank_at_500;
	}

	public double getPCT_Popular_MPSPs_detected_by_TST_Rank_at_125() {
		return PCT_Popular_MPSPs_detected_by_TST_Rank_at_125;
	}

	public void setPCT_Popular_MPSPs_detected_by_TST_Rank_at_125(
			double pCT_Popular_MPSPs_detected_by_TST_Rank_at_125) {
		PCT_Popular_MPSPs_detected_by_TST_Rank_at_125 = pCT_Popular_MPSPs_detected_by_TST_Rank_at_125;
	}

	public double getPCT_Popular_MPSPs_detected_by_TST_Rank_at_250() {
		return PCT_Popular_MPSPs_detected_by_TST_Rank_at_250;
	}

	public void setPCT_Popular_MPSPs_detected_by_TST_Rank_at_250(
			double pCT_Popular_MPSPs_detected_by_TST_Rank_at_250) {
		PCT_Popular_MPSPs_detected_by_TST_Rank_at_250 = pCT_Popular_MPSPs_detected_by_TST_Rank_at_250;
	}

	public double getPCT_Popular_MPSPs_detected_by_TST_Rank_at_375() {
		return PCT_Popular_MPSPs_detected_by_TST_Rank_at_375;
	}

	public void setPCT_Popular_MPSPs_detected_by_TST_Rank_at_375(
			double pCT_Popular_MPSPs_detected_by_TST_Rank_at_375) {
		PCT_Popular_MPSPs_detected_by_TST_Rank_at_375 = pCT_Popular_MPSPs_detected_by_TST_Rank_at_375;
	}

	public double getPCT_Popular_MPSPs_detected_by_TST_Rank_at_500() {
		return PCT_Popular_MPSPs_detected_by_TST_Rank_at_500;
	}

	public void setPCT_Popular_MPSPs_detected_by_TST_Rank_at_500(
			double pCT_Popular_MPSPs_detected_by_TST_Rank_at_500) {
		PCT_Popular_MPSPs_detected_by_TST_Rank_at_500 = pCT_Popular_MPSPs_detected_by_TST_Rank_at_500;
	}

	public double getPCT_Popular_MPSPs_detected_by_Agent_at_125() {
		return PCT_Popular_MPSPs_detected_by_Agent_at_125;
	}

	public void setPCT_Popular_MPSPs_detected_by_Agent_at_125(
			double pCT_Popular_MPSPs_detected_by_Agent_at_125) {
		PCT_Popular_MPSPs_detected_by_Agent_at_125 = pCT_Popular_MPSPs_detected_by_Agent_at_125;
	}

	public double getPCT_Popular_MPSPs_detected_by_Agent_at_250() {
		return PCT_Popular_MPSPs_detected_by_Agent_at_250;
	}

	public void setPCT_Popular_MPSPs_detected_by_Agent_at_250(
			double pCT_Popular_MPSPs_detected_by_Agent_at_250) {
		PCT_Popular_MPSPs_detected_by_Agent_at_250 = pCT_Popular_MPSPs_detected_by_Agent_at_250;
	}

	public double getPCT_Popular_MPSPs_detected_by_Agent_at_375() {
		return PCT_Popular_MPSPs_detected_by_Agent_at_375;
	}

	public void setPCT_Popular_MPSPs_detected_by_Agent_at_375(
			double pCT_Popular_MPSPs_detected_by_Agent_at_375) {
		PCT_Popular_MPSPs_detected_by_Agent_at_375 = pCT_Popular_MPSPs_detected_by_Agent_at_375;
	}

	public double getPCT_Popular_MPSPs_detected_by_Agent_at_500() {
		return PCT_Popular_MPSPs_detected_by_Agent_at_500;
	}

	public void setPCT_Popular_MPSPs_detected_by_Agent_at_500(
			double pCT_Popular_MPSPs_detected_by_Agent_at_500) {
		PCT_Popular_MPSPs_detected_by_Agent_at_500 = pCT_Popular_MPSPs_detected_by_Agent_at_500;
	}

	public double getPCT_Popular_MPSPs_detected_by_Total_Rank_at_125() {
		return PCT_Popular_MPSPs_detected_by_Total_Rank_at_125;
	}

	public void setPCT_Popular_MPSPs_detected_by_Total_Rank_at_125(
			double pCT_Popular_MPSPs_detected_by_Total_Rank_at_125) {
		PCT_Popular_MPSPs_detected_by_Total_Rank_at_125 = pCT_Popular_MPSPs_detected_by_Total_Rank_at_125;
	}

	public double getPCT_Popular_MPSPs_detected_by_Total_Rank_at_250() {
		return PCT_Popular_MPSPs_detected_by_Total_Rank_at_250;
	}

	public void setPCT_Popular_MPSPs_detected_by_Total_Rank_at_250(
			double pCT_Popular_MPSPs_detected_by_Total_Rank_at_250) {
		PCT_Popular_MPSPs_detected_by_Total_Rank_at_250 = pCT_Popular_MPSPs_detected_by_Total_Rank_at_250;
	}

	public double getPCT_Popular_MPSPs_detected_by_Total_Rank_at_375() {
		return PCT_Popular_MPSPs_detected_by_Total_Rank_at_375;
	}

	public void setPCT_Popular_MPSPs_detected_by_Total_Rank_at_375(
			double pCT_Popular_MPSPs_detected_by_Total_Rank_at_375) {
		PCT_Popular_MPSPs_detected_by_Total_Rank_at_375 = pCT_Popular_MPSPs_detected_by_Total_Rank_at_375;
	}

	public double getPCT_Popular_MPSPs_detected_by_Total_Rank_at_500() {
		return PCT_Popular_MPSPs_detected_by_Total_Rank_at_500;
	}

	public void setPCT_Popular_MPSPs_detected_by_Total_Rank_at_500(
			double pCT_Popular_MPSPs_detected_by_Total_Rank_at_500) {
		PCT_Popular_MPSPs_detected_by_Total_Rank_at_500 = pCT_Popular_MPSPs_detected_by_Total_Rank_at_500;
	}

	public double getPCT_Popular_MSPs_detected_by_TLRavg_Rank_at_125() {
		return PCT_Popular_MSPs_detected_by_TLRavg_Rank_at_125;
	}

	public void setPCT_Popular_MSPs_detected_by_TLRavg_Rank_at_125(
			double pCT_Popular_MSPs_detected_by_TLRavg_Rank_at_125) {
		PCT_Popular_MSPs_detected_by_TLRavg_Rank_at_125 = pCT_Popular_MSPs_detected_by_TLRavg_Rank_at_125;
	}

	public double getPCT_Popular_MSPs_detected_by_TLRavg_Rank_at_250() {
		return PCT_Popular_MSPs_detected_by_TLRavg_Rank_at_250;
	}

	public void setPCT_Popular_MSPs_detected_by_TLRavg_Rank_at_250(
			double pCT_Popular_MSPs_detected_by_TLRavg_Rank_at_250) {
		PCT_Popular_MSPs_detected_by_TLRavg_Rank_at_250 = pCT_Popular_MSPs_detected_by_TLRavg_Rank_at_250;
	}

	public double getPCT_Popular_MSPs_detected_by_TLRavg_Rank_at_375() {
		return PCT_Popular_MSPs_detected_by_TLRavg_Rank_at_375;
	}

	public void setPCT_Popular_MSPs_detected_by_TLRavg_Rank_at_375(
			double pCT_Popular_MSPs_detected_by_TLRavg_Rank_at_375) {
		PCT_Popular_MSPs_detected_by_TLRavg_Rank_at_375 = pCT_Popular_MSPs_detected_by_TLRavg_Rank_at_375;
	}

	public double getPCT_Popular_MSPs_detected_by_TLRavg_Rank_at_500() {
		return PCT_Popular_MSPs_detected_by_TLRavg_Rank_at_500;
	}

	public void setPCT_Popular_MSPs_detected_by_TLRavg_Rank_at_500(
			double pCT_Popular_MSPs_detected_by_TLRavg_Rank_at_500) {
		PCT_Popular_MSPs_detected_by_TLRavg_Rank_at_500 = pCT_Popular_MSPs_detected_by_TLRavg_Rank_at_500;
	}

	public double getPCT_Popular_MSPs_detected_by_TST_Rank_at_125() {
		return PCT_Popular_MSPs_detected_by_TST_Rank_at_125;
	}

	public void setPCT_Popular_MSPs_detected_by_TST_Rank_at_125(
			double pCT_Popular_MSPs_detected_by_TST_Rank_at_125) {
		PCT_Popular_MSPs_detected_by_TST_Rank_at_125 = pCT_Popular_MSPs_detected_by_TST_Rank_at_125;
	}

	public double getPCT_Popular_MSPs_detected_by_TST_Rank_at_250() {
		return PCT_Popular_MSPs_detected_by_TST_Rank_at_250;
	}

	public void setPCT_Popular_MSPs_detected_by_TST_Rank_at_250(
			double pCT_Popular_MSPs_detected_by_TST_Rank_at_250) {
		PCT_Popular_MSPs_detected_by_TST_Rank_at_250 = pCT_Popular_MSPs_detected_by_TST_Rank_at_250;
	}

	public double getPCT_Popular_MSPs_detected_by_TST_Rank_at_375() {
		return PCT_Popular_MSPs_detected_by_TST_Rank_at_375;
	}

	public void setPCT_Popular_MSPs_detected_by_TST_Rank_at_375(
			double pCT_Popular_MSPs_detected_by_TST_Rank_at_375) {
		PCT_Popular_MSPs_detected_by_TST_Rank_at_375 = pCT_Popular_MSPs_detected_by_TST_Rank_at_375;
	}

	public double getPCT_Popular_MSPs_detected_by_TST_Rank_at_500() {
		return PCT_Popular_MSPs_detected_by_TST_Rank_at_500;
	}

	public void setPCT_Popular_MSPs_detected_by_TST_Rank_at_500(
			double pCT_Popular_MSPs_detected_by_TST_Rank_at_500) {
		PCT_Popular_MSPs_detected_by_TST_Rank_at_500 = pCT_Popular_MSPs_detected_by_TST_Rank_at_500;
	}

	public double getPCT_Popular_MSPs_detected_by_Agent_at_125() {
		return PCT_Popular_MSPs_detected_by_Agent_at_125;
	}

	public void setPCT_Popular_MSPs_detected_by_Agent_at_125(
			double pCT_Popular_MSPs_detected_by_Agent_at_125) {
		PCT_Popular_MSPs_detected_by_Agent_at_125 = pCT_Popular_MSPs_detected_by_Agent_at_125;
	}

	public double getPCT_Popular_MSPs_detected_by_Agent_at_250() {
		return PCT_Popular_MSPs_detected_by_Agent_at_250;
	}

	public void setPCT_Popular_MSPs_detected_by_Agent_at_250(
			double pCT_Popular_MSPs_detected_by_Agent_at_250) {
		PCT_Popular_MSPs_detected_by_Agent_at_250 = pCT_Popular_MSPs_detected_by_Agent_at_250;
	}

	public double getPCT_Popular_MSPs_detected_by_Agent_at_375() {
		return PCT_Popular_MSPs_detected_by_Agent_at_375;
	}

	public void setPCT_Popular_MSPs_detected_by_Agent_at_375(
			double pCT_Popular_MSPs_detected_by_Agent_at_375) {
		PCT_Popular_MSPs_detected_by_Agent_at_375 = pCT_Popular_MSPs_detected_by_Agent_at_375;
	}

	public double getPCT_Popular_MSPs_detected_by_Agent_at_500() {
		return PCT_Popular_MSPs_detected_by_Agent_at_500;
	}

	public void setPCT_Popular_MSPs_detected_by_Agent_at_500(
			double pCT_Popular_MSPs_detected_by_Agent_at_500) {
		PCT_Popular_MSPs_detected_by_Agent_at_500 = pCT_Popular_MSPs_detected_by_Agent_at_500;
	}

	public double getPCT_Popular_MSPs_detected_by_Total_Rank_at_125() {
		return PCT_Popular_MSPs_detected_by_Total_Rank_at_125;
	}

	public void setPCT_Popular_MSPs_detected_by_Total_Rank_at_125(
			double pCT_Popular_MSPs_detected_by_Total_Rank_at_125) {
		PCT_Popular_MSPs_detected_by_Total_Rank_at_125 = pCT_Popular_MSPs_detected_by_Total_Rank_at_125;
	}

	public double getPCT_Popular_MSPs_detected_by_Total_Rank_at_250() {
		return PCT_Popular_MSPs_detected_by_Total_Rank_at_250;
	}

	public void setPCT_Popular_MSPs_detected_by_Total_Rank_at_250(
			double pCT_Popular_MSPs_detected_by_Total_Rank_at_250) {
		PCT_Popular_MSPs_detected_by_Total_Rank_at_250 = pCT_Popular_MSPs_detected_by_Total_Rank_at_250;
	}

	public double getPCT_Popular_MSPs_detected_by_Total_Rank_at_375() {
		return PCT_Popular_MSPs_detected_by_Total_Rank_at_375;
	}

	public void setPCT_Popular_MSPs_detected_by_Total_Rank_at_375(
			double pCT_Popular_MSPs_detected_by_Total_Rank_at_375) {
		PCT_Popular_MSPs_detected_by_Total_Rank_at_375 = pCT_Popular_MSPs_detected_by_Total_Rank_at_375;
	}

	public double getPCT_Popular_MSPs_detected_by_Total_Rank_at_500() {
		return PCT_Popular_MSPs_detected_by_Total_Rank_at_500;
	}

	public void setPCT_Popular_MSPs_detected_by_Total_Rank_at_500(
			double pCT_Popular_MSPs_detected_by_Total_Rank_at_500) {
		PCT_Popular_MSPs_detected_by_Total_Rank_at_500 = pCT_Popular_MSPs_detected_by_Total_Rank_at_500;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_125() {
		return PCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_125;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_125(
			double pCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_125) {
		PCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_125 = pCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_125;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_250() {
		return PCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_250;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_250(
			double pCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_250) {
		PCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_250 = pCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_250;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_375() {
		return PCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_375;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_375(
			double pCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_375) {
		PCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_375 = pCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_375;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_500() {
		return PCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_500;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_500(
			double pCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_500) {
		PCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_500 = pCT_Unpopular_MPSPs_detected_by_TLRavg_Rank_at_500;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_TST_Rank_at_125() {
		return PCT_Unpopular_MPSPs_detected_by_TST_Rank_at_125;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_TST_Rank_at_125(
			double pCT_Unpopular_MPSPs_detected_by_TST_Rank_at_125) {
		PCT_Unpopular_MPSPs_detected_by_TST_Rank_at_125 = pCT_Unpopular_MPSPs_detected_by_TST_Rank_at_125;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_TST_Rank_at_250() {
		return PCT_Unpopular_MPSPs_detected_by_TST_Rank_at_250;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_TST_Rank_at_250(
			double pCT_Unpopular_MPSPs_detected_by_TST_Rank_at_250) {
		PCT_Unpopular_MPSPs_detected_by_TST_Rank_at_250 = pCT_Unpopular_MPSPs_detected_by_TST_Rank_at_250;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_TST_Rank_at_375() {
		return PCT_Unpopular_MPSPs_detected_by_TST_Rank_at_375;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_TST_Rank_at_375(
			double pCT_Unpopular_MPSPs_detected_by_TST_Rank_at_375) {
		PCT_Unpopular_MPSPs_detected_by_TST_Rank_at_375 = pCT_Unpopular_MPSPs_detected_by_TST_Rank_at_375;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_TST_Rank_at_500() {
		return PCT_Unpopular_MPSPs_detected_by_TST_Rank_at_500;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_TST_Rank_at_500(
			double pCT_Unpopular_MPSPs_detected_by_TST_Rank_at_500) {
		PCT_Unpopular_MPSPs_detected_by_TST_Rank_at_500 = pCT_Unpopular_MPSPs_detected_by_TST_Rank_at_500;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_Agent_at_125() {
		return PCT_Unpopular_MPSPs_detected_by_Agent_at_125;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_Agent_at_125(
			double pCT_Unpopular_MPSPs_detected_by_Agent_at_125) {
		PCT_Unpopular_MPSPs_detected_by_Agent_at_125 = pCT_Unpopular_MPSPs_detected_by_Agent_at_125;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_Agent_at_250() {
		return PCT_Unpopular_MPSPs_detected_by_Agent_at_250;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_Agent_at_250(
			double pCT_Unpopular_MPSPs_detected_by_Agent_at_250) {
		PCT_Unpopular_MPSPs_detected_by_Agent_at_250 = pCT_Unpopular_MPSPs_detected_by_Agent_at_250;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_Agent_at_375() {
		return PCT_Unpopular_MPSPs_detected_by_Agent_at_375;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_Agent_at_375(
			double pCT_Unpopular_MPSPs_detected_by_Agent_at_375) {
		PCT_Unpopular_MPSPs_detected_by_Agent_at_375 = pCT_Unpopular_MPSPs_detected_by_Agent_at_375;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_Agent_at_500() {
		return PCT_Unpopular_MPSPs_detected_by_Agent_at_500;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_Agent_at_500(
			double pCT_Unpopular_MPSPs_detected_by_Agent_at_500) {
		PCT_Unpopular_MPSPs_detected_by_Agent_at_500 = pCT_Unpopular_MPSPs_detected_by_Agent_at_500;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_Total_Rank_at_125() {
		return PCT_Unpopular_MPSPs_detected_by_Total_Rank_at_125;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_Total_Rank_at_125(
			double pCT_Unpopular_MPSPs_detected_by_Total_Rank_at_125) {
		PCT_Unpopular_MPSPs_detected_by_Total_Rank_at_125 = pCT_Unpopular_MPSPs_detected_by_Total_Rank_at_125;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_Total_Rank_at_250() {
		return PCT_Unpopular_MPSPs_detected_by_Total_Rank_at_250;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_Total_Rank_at_250(
			double pCT_Unpopular_MPSPs_detected_by_Total_Rank_at_250) {
		PCT_Unpopular_MPSPs_detected_by_Total_Rank_at_250 = pCT_Unpopular_MPSPs_detected_by_Total_Rank_at_250;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_Total_Rank_at_375() {
		return PCT_Unpopular_MPSPs_detected_by_Total_Rank_at_375;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_Total_Rank_at_375(
			double pCT_Unpopular_MPSPs_detected_by_Total_Rank_at_375) {
		PCT_Unpopular_MPSPs_detected_by_Total_Rank_at_375 = pCT_Unpopular_MPSPs_detected_by_Total_Rank_at_375;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_Total_Rank_at_500() {
		return PCT_Unpopular_MPSPs_detected_by_Total_Rank_at_500;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_Total_Rank_at_500(
			double pCT_Unpopular_MPSPs_detected_by_Total_Rank_at_500) {
		PCT_Unpopular_MPSPs_detected_by_Total_Rank_at_500 = pCT_Unpopular_MPSPs_detected_by_Total_Rank_at_500;
	}

	public double getPCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_125() {
		return PCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_125;
	}

	public void setPCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_125(
			double pCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_125) {
		PCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_125 = pCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_125;
	}

	public double getPCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_250() {
		return PCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_250;
	}

	public void setPCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_250(
			double pCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_250) {
		PCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_250 = pCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_250;
	}

	public double getPCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_375() {
		return PCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_375;
	}

	public void setPCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_375(
			double pCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_375) {
		PCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_375 = pCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_375;
	}

	public double getPCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_500() {
		return PCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_500;
	}

	public void setPCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_500(
			double pCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_500) {
		PCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_500 = pCT_Unpopular_MSPs_detected_by_TLRavg_Rank_at_500;
	}

	public double getPCT_Unpopular_MSPs_detected_by_TST_Rank_at_125() {
		return PCT_Unpopular_MSPs_detected_by_TST_Rank_at_125;
	}

	public void setPCT_Unpopular_MSPs_detected_by_TST_Rank_at_125(
			double pCT_Unpopular_MSPs_detected_by_TST_Rank_at_125) {
		PCT_Unpopular_MSPs_detected_by_TST_Rank_at_125 = pCT_Unpopular_MSPs_detected_by_TST_Rank_at_125;
	}

	public double getPCT_Unpopular_MSPs_detected_by_TST_Rank_at_250() {
		return PCT_Unpopular_MSPs_detected_by_TST_Rank_at_250;
	}

	public void setPCT_Unpopular_MSPs_detected_by_TST_Rank_at_250(
			double pCT_Unpopular_MSPs_detected_by_TST_Rank_at_250) {
		PCT_Unpopular_MSPs_detected_by_TST_Rank_at_250 = pCT_Unpopular_MSPs_detected_by_TST_Rank_at_250;
	}

	public double getPCT_Unpopular_MSPs_detected_by_TST_Rank_at_375() {
		return PCT_Unpopular_MSPs_detected_by_TST_Rank_at_375;
	}

	public void setPCT_Unpopular_MSPs_detected_by_TST_Rank_at_375(
			double pCT_Unpopular_MSPs_detected_by_TST_Rank_at_375) {
		PCT_Unpopular_MSPs_detected_by_TST_Rank_at_375 = pCT_Unpopular_MSPs_detected_by_TST_Rank_at_375;
	}

	public double getPCT_Unpopular_MSPs_detected_by_TST_Rank_at_500() {
		return PCT_Unpopular_MSPs_detected_by_TST_Rank_at_500;
	}

	public void setPCT_Unpopular_MSPs_detected_by_TST_Rank_at_500(
			double pCT_Unpopular_MSPs_detected_by_TST_Rank_at_500) {
		PCT_Unpopular_MSPs_detected_by_TST_Rank_at_500 = pCT_Unpopular_MSPs_detected_by_TST_Rank_at_500;
	}

	public double getPCT_Unpopular_MSPs_detected_by_Agent_at_125() {
		return PCT_Unpopular_MSPs_detected_by_Agent_at_125;
	}

	public void setPCT_Unpopular_MSPs_detected_by_Agent_at_125(
			double pCT_Unpopular_MSPs_detected_by_Agent_at_125) {
		PCT_Unpopular_MSPs_detected_by_Agent_at_125 = pCT_Unpopular_MSPs_detected_by_Agent_at_125;
	}

	public double getPCT_Unpopular_MSPs_detected_by_Agent_at_250() {
		return PCT_Unpopular_MSPs_detected_by_Agent_at_250;
	}

	public void setPCT_Unpopular_MSPs_detected_by_Agent_at_250(
			double pCT_Unpopular_MSPs_detected_by_Agent_at_250) {
		PCT_Unpopular_MSPs_detected_by_Agent_at_250 = pCT_Unpopular_MSPs_detected_by_Agent_at_250;
	}

	public double getPCT_Unpopular_MSPs_detected_by_Agent_at_375() {
		return PCT_Unpopular_MSPs_detected_by_Agent_at_375;
	}

	public void setPCT_Unpopular_MSPs_detected_by_Agent_at_375(
			double pCT_Unpopular_MSPs_detected_by_Agent_at_375) {
		PCT_Unpopular_MSPs_detected_by_Agent_at_375 = pCT_Unpopular_MSPs_detected_by_Agent_at_375;
	}

	public double getPCT_Unpopular_MSPs_detected_by_Agent_at_500() {
		return PCT_Unpopular_MSPs_detected_by_Agent_at_500;
	}

	public void setPCT_Unpopular_MSPs_detected_by_Agent_at_500(
			double pCT_Unpopular_MSPs_detected_by_Agent_at_500) {
		PCT_Unpopular_MSPs_detected_by_Agent_at_500 = pCT_Unpopular_MSPs_detected_by_Agent_at_500;
	}

	public double getPCT_Unpopular_MSPs_detected_by_Total_Rank_at_125() {
		return PCT_Unpopular_MSPs_detected_by_Total_Rank_at_125;
	}

	public void setPCT_Unpopular_MSPs_detected_by_Total_Rank_at_125(
			double pCT_Unpopular_MSPs_detected_by_Total_Rank_at_125) {
		PCT_Unpopular_MSPs_detected_by_Total_Rank_at_125 = pCT_Unpopular_MSPs_detected_by_Total_Rank_at_125;
	}

	public double getPCT_Unpopular_MSPs_detected_by_Total_Rank_at_250() {
		return PCT_Unpopular_MSPs_detected_by_Total_Rank_at_250;
	}

	public void setPCT_Unpopular_MSPs_detected_by_Total_Rank_at_250(
			double pCT_Unpopular_MSPs_detected_by_Total_Rank_at_250) {
		PCT_Unpopular_MSPs_detected_by_Total_Rank_at_250 = pCT_Unpopular_MSPs_detected_by_Total_Rank_at_250;
	}

	public double getPCT_Unpopular_MSPs_detected_by_Total_Rank_at_375() {
		return PCT_Unpopular_MSPs_detected_by_Total_Rank_at_375;
	}

	public void setPCT_Unpopular_MSPs_detected_by_Total_Rank_at_375(
			double pCT_Unpopular_MSPs_detected_by_Total_Rank_at_375) {
		PCT_Unpopular_MSPs_detected_by_Total_Rank_at_375 = pCT_Unpopular_MSPs_detected_by_Total_Rank_at_375;
	}

	public double getPCT_Unpopular_MSPs_detected_by_Total_Rank_at_500() {
		return PCT_Unpopular_MSPs_detected_by_Total_Rank_at_500;
	}

	public void setPCT_Unpopular_MSPs_detected_by_Total_Rank_at_500(
			double pCT_Unpopular_MSPs_detected_by_Total_Rank_at_500) {
		PCT_Unpopular_MSPs_detected_by_Total_Rank_at_500 = pCT_Unpopular_MSPs_detected_by_Total_Rank_at_500;
	}

	public double getPCT_PSPs_detected_by_TGTg_Rank_at_125() {
		return PCT_PSPs_detected_by_TGTg_Rank_at_125;
	}

	public void setPCT_PSPs_detected_by_TGTg_Rank_at_125(
			double pCT_PSPs_detected_by_TGTg_Rank_at_125) {
		PCT_PSPs_detected_by_TGTg_Rank_at_125 = pCT_PSPs_detected_by_TGTg_Rank_at_125;
	}

	public double getPCT_PSPs_detected_by_TGTg_Rank_at_250() {
		return PCT_PSPs_detected_by_TGTg_Rank_at_250;
	}

	public void setPCT_PSPs_detected_by_TGTg_Rank_at_250(
			double pCT_PSPs_detected_by_TGTg_Rank_at_250) {
		PCT_PSPs_detected_by_TGTg_Rank_at_250 = pCT_PSPs_detected_by_TGTg_Rank_at_250;
	}

	public double getPCT_PSPs_detected_by_TGTg_Rank_at_375() {
		return PCT_PSPs_detected_by_TGTg_Rank_at_375;
	}

	public void setPCT_PSPs_detected_by_TGTg_Rank_at_375(
			double pCT_PSPs_detected_by_TGTg_Rank_at_375) {
		PCT_PSPs_detected_by_TGTg_Rank_at_375 = pCT_PSPs_detected_by_TGTg_Rank_at_375;
	}

	public double getPCT_PSPs_detected_by_TGTg_Rank_at_500() {
		return PCT_PSPs_detected_by_TGTg_Rank_at_500;
	}

	public void setPCT_PSPs_detected_by_TGTg_Rank_at_500(
			double pCT_PSPs_detected_by_TGTg_Rank_at_500) {
		PCT_PSPs_detected_by_TGTg_Rank_at_500 = pCT_PSPs_detected_by_TGTg_Rank_at_500;
	}

	public double getPCT_PSPs_detected_by_Colluding_at_125() {
		return PCT_PSPs_detected_by_Colluding_at_125;
	}

	public void setPCT_PSPs_detected_by_Colluding_at_125(
			double pCT_PSPs_detected_by_Colluding_at_125) {
		PCT_PSPs_detected_by_Colluding_at_125 = pCT_PSPs_detected_by_Colluding_at_125;
	}

	public double getPCT_PSPs_detected_by_Colluding_at_250() {
		return PCT_PSPs_detected_by_Colluding_at_250;
	}

	public void setPCT_PSPs_detected_by_Colluding_at_250(
			double pCT_PSPs_detected_by_Colluding_at_250) {
		PCT_PSPs_detected_by_Colluding_at_250 = pCT_PSPs_detected_by_Colluding_at_250;
	}

	public double getPCT_PSPs_detected_by_Colluding_at_375() {
		return PCT_PSPs_detected_by_Colluding_at_375;
	}

	public void setPCT_PSPs_detected_by_Colluding_at_375(
			double pCT_PSPs_detected_by_Colluding_at_375) {
		PCT_PSPs_detected_by_Colluding_at_375 = pCT_PSPs_detected_by_Colluding_at_375;
	}

	public double getPCT_PSPs_detected_by_Colluding_at_500() {
		return PCT_PSPs_detected_by_Colluding_at_500;
	}

	public void setPCT_PSPs_detected_by_Colluding_at_500(
			double pCT_PSPs_detected_by_Colluding_at_500) {
		PCT_PSPs_detected_by_Colluding_at_500 = pCT_PSPs_detected_by_Colluding_at_500;
	}

	public double getPCT_PSPs_detected_by_weakColluding_at_125() {
		return PCT_PSPs_detected_by_weakColluding_at_125;
	}

	public void setPCT_PSPs_detected_by_weakColluding_at_125(
			double pCT_PSPs_detected_by_weakColluding_at_125) {
		PCT_PSPs_detected_by_weakColluding_at_125 = pCT_PSPs_detected_by_weakColluding_at_125;
	}

	public double getPCT_PSPs_detected_by_weakColluding_at_250() {
		return PCT_PSPs_detected_by_weakColluding_at_250;
	}

	public void setPCT_PSPs_detected_by_weakColluding_at_250(
			double pCT_PSPs_detected_by_weakColluding_at_250) {
		PCT_PSPs_detected_by_weakColluding_at_250 = pCT_PSPs_detected_by_weakColluding_at_250;
	}

	public double getPCT_PSPs_detected_by_weakColluding_at_375() {
		return PCT_PSPs_detected_by_weakColluding_at_375;
	}

	public void setPCT_PSPs_detected_by_weakColluding_at_375(
			double pCT_PSPs_detected_by_weakColluding_at_375) {
		PCT_PSPs_detected_by_weakColluding_at_375 = pCT_PSPs_detected_by_weakColluding_at_375;
	}

	public double getPCT_PSPs_detected_by_weakColluding_at_500() {
		return PCT_PSPs_detected_by_weakColluding_at_500;
	}

	public void setPCT_PSPs_detected_by_weakColluding_at_500(
			double pCT_PSPs_detected_by_weakColluding_at_500) {
		PCT_PSPs_detected_by_weakColluding_at_500 = pCT_PSPs_detected_by_weakColluding_at_500;
	}

	public double getPCT_SPs_detected_by_TGTg_Rank_at_125() {
		return PCT_SPs_detected_by_TGTg_Rank_at_125;
	}

	public void setPCT_SPs_detected_by_TGTg_Rank_at_125(
			double pCT_SPs_detected_by_TGTg_Rank_at_125) {
		PCT_SPs_detected_by_TGTg_Rank_at_125 = pCT_SPs_detected_by_TGTg_Rank_at_125;
	}

	public double getPCT_SPs_detected_by_TGTg_Rank_at_250() {
		return PCT_SPs_detected_by_TGTg_Rank_at_250;
	}

	public void setPCT_SPs_detected_by_TGTg_Rank_at_250(
			double pCT_SPs_detected_by_TGTg_Rank_at_250) {
		PCT_SPs_detected_by_TGTg_Rank_at_250 = pCT_SPs_detected_by_TGTg_Rank_at_250;
	}

	public double getPCT_SPs_detected_by_TGTg_Rank_at_375() {
		return PCT_SPs_detected_by_TGTg_Rank_at_375;
	}

	public void setPCT_SPs_detected_by_TGTg_Rank_at_375(
			double pCT_SPs_detected_by_TGTg_Rank_at_375) {
		PCT_SPs_detected_by_TGTg_Rank_at_375 = pCT_SPs_detected_by_TGTg_Rank_at_375;
	}

	public double getPCT_SPs_detected_by_TGTg_Rank_at_500() {
		return PCT_SPs_detected_by_TGTg_Rank_at_500;
	}

	public void setPCT_SPs_detected_by_TGTg_Rank_at_500(
			double pCT_SPs_detected_by_TGTg_Rank_at_500) {
		PCT_SPs_detected_by_TGTg_Rank_at_500 = pCT_SPs_detected_by_TGTg_Rank_at_500;
	}

	public double getPCT_SPs_detected_by_Colluding_at_125() {
		return PCT_SPs_detected_by_Colluding_at_125;
	}

	public void setPCT_SPs_detected_by_Colluding_at_125(
			double pCT_SPs_detected_by_Colluding_at_125) {
		PCT_SPs_detected_by_Colluding_at_125 = pCT_SPs_detected_by_Colluding_at_125;
	}

	public double getPCT_SPs_detected_by_Colluding_at_250() {
		return PCT_SPs_detected_by_Colluding_at_250;
	}

	public void setPCT_SPs_detected_by_Colluding_at_250(
			double pCT_SPs_detected_by_Colluding_at_250) {
		PCT_SPs_detected_by_Colluding_at_250 = pCT_SPs_detected_by_Colluding_at_250;
	}

	public double getPCT_SPs_detected_by_Colluding_at_375() {
		return PCT_SPs_detected_by_Colluding_at_375;
	}

	public void setPCT_SPs_detected_by_Colluding_at_375(
			double pCT_SPs_detected_by_Colluding_at_375) {
		PCT_SPs_detected_by_Colluding_at_375 = pCT_SPs_detected_by_Colluding_at_375;
	}

	public double getPCT_SPs_detected_by_Colluding_at_500() {
		return PCT_SPs_detected_by_Colluding_at_500;
	}

	public void setPCT_SPs_detected_by_Colluding_at_500(
			double pCT_SPs_detected_by_Colluding_at_500) {
		PCT_SPs_detected_by_Colluding_at_500 = pCT_SPs_detected_by_Colluding_at_500;
	}

	public double getPCT_SPs_detected_by_weakColluding_at_125() {
		return PCT_SPs_detected_by_weakColluding_at_125;
	}

	public void setPCT_SPs_detected_by_weakColluding_at_125(
			double pCT_SPs_detected_by_weakColluding_at_125) {
		PCT_SPs_detected_by_weakColluding_at_125 = pCT_SPs_detected_by_weakColluding_at_125;
	}

	public double getPCT_SPs_detected_by_weakColluding_at_250() {
		return PCT_SPs_detected_by_weakColluding_at_250;
	}

	public void setPCT_SPs_detected_by_weakColluding_at_250(
			double pCT_SPs_detected_by_weakColluding_at_250) {
		PCT_SPs_detected_by_weakColluding_at_250 = pCT_SPs_detected_by_weakColluding_at_250;
	}

	public double getPCT_SPs_detected_by_weakColluding_at_375() {
		return PCT_SPs_detected_by_weakColluding_at_375;
	}

	public void setPCT_SPs_detected_by_weakColluding_at_375(
			double pCT_SPs_detected_by_weakColluding_at_375) {
		PCT_SPs_detected_by_weakColluding_at_375 = pCT_SPs_detected_by_weakColluding_at_375;
	}

	public double getPCT_SPs_detected_by_weakColluding_at_500() {
		return PCT_SPs_detected_by_weakColluding_at_500;
	}

	public void setPCT_SPs_detected_by_weakColluding_at_500(
			double pCT_SPs_detected_by_weakColluding_at_500) {
		PCT_SPs_detected_by_weakColluding_at_500 = pCT_SPs_detected_by_weakColluding_at_500;
	}

	public double getPCT_MPSPs_detected_by_TGTg_Rank_at_125() {
		return PCT_MPSPs_detected_by_TGTg_Rank_at_125;
	}

	public void setPCT_MPSPs_detected_by_TGTg_Rank_at_125(
			double pCT_MPSPs_detected_by_TGTg_Rank_at_125) {
		PCT_MPSPs_detected_by_TGTg_Rank_at_125 = pCT_MPSPs_detected_by_TGTg_Rank_at_125;
	}

	public double getPCT_MPSPs_detected_by_TGTg_Rank_at_250() {
		return PCT_MPSPs_detected_by_TGTg_Rank_at_250;
	}

	public void setPCT_MPSPs_detected_by_TGTg_Rank_at_250(
			double pCT_MPSPs_detected_by_TGTg_Rank_at_250) {
		PCT_MPSPs_detected_by_TGTg_Rank_at_250 = pCT_MPSPs_detected_by_TGTg_Rank_at_250;
	}

	public double getPCT_MPSPs_detected_by_TGTg_Rank_at_375() {
		return PCT_MPSPs_detected_by_TGTg_Rank_at_375;
	}

	public void setPCT_MPSPs_detected_by_TGTg_Rank_at_375(
			double pCT_MPSPs_detected_by_TGTg_Rank_at_375) {
		PCT_MPSPs_detected_by_TGTg_Rank_at_375 = pCT_MPSPs_detected_by_TGTg_Rank_at_375;
	}

	public double getPCT_MPSPs_detected_by_TGTg_Rank_at_500() {
		return PCT_MPSPs_detected_by_TGTg_Rank_at_500;
	}

	public void setPCT_MPSPs_detected_by_TGTg_Rank_at_500(
			double pCT_MPSPs_detected_by_TGTg_Rank_at_500) {
		PCT_MPSPs_detected_by_TGTg_Rank_at_500 = pCT_MPSPs_detected_by_TGTg_Rank_at_500;
	}

	public double getPCT_MPSPs_detected_by_Colluding_at_125() {
		return PCT_MPSPs_detected_by_Colluding_at_125;
	}

	public void setPCT_MPSPs_detected_by_Colluding_at_125(
			double pCT_MPSPs_detected_by_Colluding_at_125) {
		PCT_MPSPs_detected_by_Colluding_at_125 = pCT_MPSPs_detected_by_Colluding_at_125;
	}

	public double getPCT_MPSPs_detected_by_Colluding_at_250() {
		return PCT_MPSPs_detected_by_Colluding_at_250;
	}

	public void setPCT_MPSPs_detected_by_Colluding_at_250(
			double pCT_MPSPs_detected_by_Colluding_at_250) {
		PCT_MPSPs_detected_by_Colluding_at_250 = pCT_MPSPs_detected_by_Colluding_at_250;
	}

	public double getPCT_MPSPs_detected_by_Colluding_at_375() {
		return PCT_MPSPs_detected_by_Colluding_at_375;
	}

	public void setPCT_MPSPs_detected_by_Colluding_at_375(
			double pCT_MPSPs_detected_by_Colluding_at_375) {
		PCT_MPSPs_detected_by_Colluding_at_375 = pCT_MPSPs_detected_by_Colluding_at_375;
	}

	public double getPCT_MPSPs_detected_by_Colluding_at_500() {
		return PCT_MPSPs_detected_by_Colluding_at_500;
	}

	public void setPCT_MPSPs_detected_by_Colluding_at_500(
			double pCT_MPSPs_detected_by_Colluding_at_500) {
		PCT_MPSPs_detected_by_Colluding_at_500 = pCT_MPSPs_detected_by_Colluding_at_500;
	}

	public double getPCT_MPSPs_detected_by_weakColluding_at_125() {
		return PCT_MPSPs_detected_by_weakColluding_at_125;
	}

	public void setPCT_MPSPs_detected_by_weakColluding_at_125(
			double pCT_MPSPs_detected_by_weakColluding_at_125) {
		PCT_MPSPs_detected_by_weakColluding_at_125 = pCT_MPSPs_detected_by_weakColluding_at_125;
	}

	public double getPCT_MPSPs_detected_by_weakColluding_at_250() {
		return PCT_MPSPs_detected_by_weakColluding_at_250;
	}

	public void setPCT_MPSPs_detected_by_weakColluding_at_250(
			double pCT_MPSPs_detected_by_weakColluding_at_250) {
		PCT_MPSPs_detected_by_weakColluding_at_250 = pCT_MPSPs_detected_by_weakColluding_at_250;
	}

	public double getPCT_MPSPs_detected_by_weakColluding_at_375() {
		return PCT_MPSPs_detected_by_weakColluding_at_375;
	}

	public void setPCT_MPSPs_detected_by_weakColluding_at_375(
			double pCT_MPSPs_detected_by_weakColluding_at_375) {
		PCT_MPSPs_detected_by_weakColluding_at_375 = pCT_MPSPs_detected_by_weakColluding_at_375;
	}

	public double getPCT_MPSPs_detected_by_weakColluding_at_500() {
		return PCT_MPSPs_detected_by_weakColluding_at_500;
	}

	public void setPCT_MPSPs_detected_by_weakColluding_at_500(
			double pCT_MPSPs_detected_by_weakColluding_at_500) {
		PCT_MPSPs_detected_by_weakColluding_at_500 = pCT_MPSPs_detected_by_weakColluding_at_500;
	}

	public double getPCT_MSPs_detected_by_TGTg_Rank_at_125() {
		return PCT_MSPs_detected_by_TGTg_Rank_at_125;
	}

	public void setPCT_MSPs_detected_by_TGTg_Rank_at_125(
			double pCT_MSPs_detected_by_TGTg_Rank_at_125) {
		PCT_MSPs_detected_by_TGTg_Rank_at_125 = pCT_MSPs_detected_by_TGTg_Rank_at_125;
	}

	public double getPCT_MSPs_detected_by_TGTg_Rank_at_250() {
		return PCT_MSPs_detected_by_TGTg_Rank_at_250;
	}

	public void setPCT_MSPs_detected_by_TGTg_Rank_at_250(
			double pCT_MSPs_detected_by_TGTg_Rank_at_250) {
		PCT_MSPs_detected_by_TGTg_Rank_at_250 = pCT_MSPs_detected_by_TGTg_Rank_at_250;
	}

	public double getPCT_MSPs_detected_by_TGTg_Rank_at_375() {
		return PCT_MSPs_detected_by_TGTg_Rank_at_375;
	}

	public void setPCT_MSPs_detected_by_TGTg_Rank_at_375(
			double pCT_MSPs_detected_by_TGTg_Rank_at_375) {
		PCT_MSPs_detected_by_TGTg_Rank_at_375 = pCT_MSPs_detected_by_TGTg_Rank_at_375;
	}

	public double getPCT_MSPs_detected_by_TGTg_Rank_at_500() {
		return PCT_MSPs_detected_by_TGTg_Rank_at_500;
	}

	public void setPCT_MSPs_detected_by_TGTg_Rank_at_500(
			double pCT_MSPs_detected_by_TGTg_Rank_at_500) {
		PCT_MSPs_detected_by_TGTg_Rank_at_500 = pCT_MSPs_detected_by_TGTg_Rank_at_500;
	}

	public double getPCT_MSPs_detected_by_Colluding_at_125() {
		return PCT_MSPs_detected_by_Colluding_at_125;
	}

	public void setPCT_MSPs_detected_by_Colluding_at_125(
			double pCT_MSPs_detected_by_Colluding_at_125) {
		PCT_MSPs_detected_by_Colluding_at_125 = pCT_MSPs_detected_by_Colluding_at_125;
	}

	public double getPCT_MSPs_detected_by_Colluding_at_250() {
		return PCT_MSPs_detected_by_Colluding_at_250;
	}

	public void setPCT_MSPs_detected_by_Colluding_at_250(
			double pCT_MSPs_detected_by_Colluding_at_250) {
		PCT_MSPs_detected_by_Colluding_at_250 = pCT_MSPs_detected_by_Colluding_at_250;
	}

	public double getPCT_MSPs_detected_by_Colluding_at_375() {
		return PCT_MSPs_detected_by_Colluding_at_375;
	}

	public void setPCT_MSPs_detected_by_Colluding_at_375(
			double pCT_MSPs_detected_by_Colluding_at_375) {
		PCT_MSPs_detected_by_Colluding_at_375 = pCT_MSPs_detected_by_Colluding_at_375;
	}

	public double getPCT_MSPs_detected_by_Colluding_at_500() {
		return PCT_MSPs_detected_by_Colluding_at_500;
	}

	public void setPCT_MSPs_detected_by_Colluding_at_500(
			double pCT_MSPs_detected_by_Colluding_at_500) {
		PCT_MSPs_detected_by_Colluding_at_500 = pCT_MSPs_detected_by_Colluding_at_500;
	}

	public double getPCT_MSPs_detected_by_weakColluding_at_125() {
		return PCT_MSPs_detected_by_weakColluding_at_125;
	}

	public void setPCT_MSPs_detected_by_weakColluding_at_125(
			double pCT_MSPs_detected_by_weakColluding_at_125) {
		PCT_MSPs_detected_by_weakColluding_at_125 = pCT_MSPs_detected_by_weakColluding_at_125;
	}

	public double getPCT_MSPs_detected_by_weakColluding_at_250() {
		return PCT_MSPs_detected_by_weakColluding_at_250;
	}

	public void setPCT_MSPs_detected_by_weakColluding_at_250(
			double pCT_MSPs_detected_by_weakColluding_at_250) {
		PCT_MSPs_detected_by_weakColluding_at_250 = pCT_MSPs_detected_by_weakColluding_at_250;
	}

	public double getPCT_MSPs_detected_by_weakColluding_at_375() {
		return PCT_MSPs_detected_by_weakColluding_at_375;
	}

	public void setPCT_MSPs_detected_by_weakColluding_at_375(
			double pCT_MSPs_detected_by_weakColluding_at_375) {
		PCT_MSPs_detected_by_weakColluding_at_375 = pCT_MSPs_detected_by_weakColluding_at_375;
	}

	public double getPCT_MSPs_detected_by_weakColluding_at_500() {
		return PCT_MSPs_detected_by_weakColluding_at_500;
	}

	public void setPCT_MSPs_detected_by_weakColluding_at_500(
			double pCT_MSPs_detected_by_weakColluding_at_500) {
		PCT_MSPs_detected_by_weakColluding_at_500 = pCT_MSPs_detected_by_weakColluding_at_500;
	}

	public double getPCT_Popular_MPSPs_detected_by_TGTg_Rank_at_125() {
		return PCT_Popular_MPSPs_detected_by_TGTg_Rank_at_125;
	}

	public void setPCT_Popular_MPSPs_detected_by_TGTg_Rank_at_125(
			double pCT_Popular_MPSPs_detected_by_TGTg_Rank_at_125) {
		PCT_Popular_MPSPs_detected_by_TGTg_Rank_at_125 = pCT_Popular_MPSPs_detected_by_TGTg_Rank_at_125;
	}

	public double getPCT_Popular_MPSPs_detected_by_TGTg_Rank_at_250() {
		return PCT_Popular_MPSPs_detected_by_TGTg_Rank_at_250;
	}

	public void setPCT_Popular_MPSPs_detected_by_TGTg_Rank_at_250(
			double pCT_Popular_MPSPs_detected_by_TGTg_Rank_at_250) {
		PCT_Popular_MPSPs_detected_by_TGTg_Rank_at_250 = pCT_Popular_MPSPs_detected_by_TGTg_Rank_at_250;
	}

	public double getPCT_Popular_MPSPs_detected_by_TGTg_Rank_at_375() {
		return PCT_Popular_MPSPs_detected_by_TGTg_Rank_at_375;
	}

	public void setPCT_Popular_MPSPs_detected_by_TGTg_Rank_at_375(
			double pCT_Popular_MPSPs_detected_by_TGTg_Rank_at_375) {
		PCT_Popular_MPSPs_detected_by_TGTg_Rank_at_375 = pCT_Popular_MPSPs_detected_by_TGTg_Rank_at_375;
	}

	public double getPCT_Popular_MPSPs_detected_by_TGTg_Rank_at_500() {
		return PCT_Popular_MPSPs_detected_by_TGTg_Rank_at_500;
	}

	public void setPCT_Popular_MPSPs_detected_by_TGTg_Rank_at_500(
			double pCT_Popular_MPSPs_detected_by_TGTg_Rank_at_500) {
		PCT_Popular_MPSPs_detected_by_TGTg_Rank_at_500 = pCT_Popular_MPSPs_detected_by_TGTg_Rank_at_500;
	}

	public double getPCT_Popular_MPSPs_detected_by_Colluding_at_125() {
		return PCT_Popular_MPSPs_detected_by_Colluding_at_125;
	}

	public void setPCT_Popular_MPSPs_detected_by_Colluding_at_125(
			double pCT_Popular_MPSPs_detected_by_Colluding_at_125) {
		PCT_Popular_MPSPs_detected_by_Colluding_at_125 = pCT_Popular_MPSPs_detected_by_Colluding_at_125;
	}

	public double getPCT_Popular_MPSPs_detected_by_Colluding_at_250() {
		return PCT_Popular_MPSPs_detected_by_Colluding_at_250;
	}

	public void setPCT_Popular_MPSPs_detected_by_Colluding_at_250(
			double pCT_Popular_MPSPs_detected_by_Colluding_at_250) {
		PCT_Popular_MPSPs_detected_by_Colluding_at_250 = pCT_Popular_MPSPs_detected_by_Colluding_at_250;
	}

	public double getPCT_Popular_MPSPs_detected_by_Colluding_at_375() {
		return PCT_Popular_MPSPs_detected_by_Colluding_at_375;
	}

	public void setPCT_Popular_MPSPs_detected_by_Colluding_at_375(
			double pCT_Popular_MPSPs_detected_by_Colluding_at_375) {
		PCT_Popular_MPSPs_detected_by_Colluding_at_375 = pCT_Popular_MPSPs_detected_by_Colluding_at_375;
	}

	public double getPCT_Popular_MPSPs_detected_by_Colluding_at_500() {
		return PCT_Popular_MPSPs_detected_by_Colluding_at_500;
	}

	public void setPCT_Popular_MPSPs_detected_by_Colluding_at_500(
			double pCT_Popular_MPSPs_detected_by_Colluding_at_500) {
		PCT_Popular_MPSPs_detected_by_Colluding_at_500 = pCT_Popular_MPSPs_detected_by_Colluding_at_500;
	}

	public double getPCT_Popular_MPSPs_detected_by_weakColluding_at_125() {
		return PCT_Popular_MPSPs_detected_by_weakColluding_at_125;
	}

	public void setPCT_Popular_MPSPs_detected_by_weakColluding_at_125(
			double pCT_Popular_MPSPs_detected_by_weakColluding_at_125) {
		PCT_Popular_MPSPs_detected_by_weakColluding_at_125 = pCT_Popular_MPSPs_detected_by_weakColluding_at_125;
	}

	public double getPCT_Popular_MPSPs_detected_by_weakColluding_at_250() {
		return PCT_Popular_MPSPs_detected_by_weakColluding_at_250;
	}

	public void setPCT_Popular_MPSPs_detected_by_weakColluding_at_250(
			double pCT_Popular_MPSPs_detected_by_weakColluding_at_250) {
		PCT_Popular_MPSPs_detected_by_weakColluding_at_250 = pCT_Popular_MPSPs_detected_by_weakColluding_at_250;
	}

	public double getPCT_Popular_MPSPs_detected_by_weakColluding_at_375() {
		return PCT_Popular_MPSPs_detected_by_weakColluding_at_375;
	}

	public void setPCT_Popular_MPSPs_detected_by_weakColluding_at_375(
			double pCT_Popular_MPSPs_detected_by_weakColluding_at_375) {
		PCT_Popular_MPSPs_detected_by_weakColluding_at_375 = pCT_Popular_MPSPs_detected_by_weakColluding_at_375;
	}

	public double getPCT_Popular_MPSPs_detected_by_weakColluding_at_500() {
		return PCT_Popular_MPSPs_detected_by_weakColluding_at_500;
	}

	public void setPCT_Popular_MPSPs_detected_by_weakColluding_at_500(
			double pCT_Popular_MPSPs_detected_by_weakColluding_at_500) {
		PCT_Popular_MPSPs_detected_by_weakColluding_at_500 = pCT_Popular_MPSPs_detected_by_weakColluding_at_500;
	}

	public double getPCT_Popular_MSPs_detected_by_TGTg_Rank_at_125() {
		return PCT_Popular_MSPs_detected_by_TGTg_Rank_at_125;
	}

	public void setPCT_Popular_MSPs_detected_by_TGTg_Rank_at_125(
			double pCT_Popular_MSPs_detected_by_TGTg_Rank_at_125) {
		PCT_Popular_MSPs_detected_by_TGTg_Rank_at_125 = pCT_Popular_MSPs_detected_by_TGTg_Rank_at_125;
	}

	public double getPCT_Popular_MSPs_detected_by_TGTg_Rank_at_250() {
		return PCT_Popular_MSPs_detected_by_TGTg_Rank_at_250;
	}

	public void setPCT_Popular_MSPs_detected_by_TGTg_Rank_at_250(
			double pCT_Popular_MSPs_detected_by_TGTg_Rank_at_250) {
		PCT_Popular_MSPs_detected_by_TGTg_Rank_at_250 = pCT_Popular_MSPs_detected_by_TGTg_Rank_at_250;
	}

	public double getPCT_Popular_MSPs_detected_by_TGTg_Rank_at_375() {
		return PCT_Popular_MSPs_detected_by_TGTg_Rank_at_375;
	}

	public void setPCT_Popular_MSPs_detected_by_TGTg_Rank_at_375(
			double pCT_Popular_MSPs_detected_by_TGTg_Rank_at_375) {
		PCT_Popular_MSPs_detected_by_TGTg_Rank_at_375 = pCT_Popular_MSPs_detected_by_TGTg_Rank_at_375;
	}

	public double getPCT_Popular_MSPs_detected_by_TGTg_Rank_at_500() {
		return PCT_Popular_MSPs_detected_by_TGTg_Rank_at_500;
	}

	public void setPCT_Popular_MSPs_detected_by_TGTg_Rank_at_500(
			double pCT_Popular_MSPs_detected_by_TGTg_Rank_at_500) {
		PCT_Popular_MSPs_detected_by_TGTg_Rank_at_500 = pCT_Popular_MSPs_detected_by_TGTg_Rank_at_500;
	}

	public double getPCT_Popular_MSPs_detected_by_Colluding_at_125() {
		return PCT_Popular_MSPs_detected_by_Colluding_at_125;
	}

	public void setPCT_Popular_MSPs_detected_by_Colluding_at_125(
			double pCT_Popular_MSPs_detected_by_Colluding_at_125) {
		PCT_Popular_MSPs_detected_by_Colluding_at_125 = pCT_Popular_MSPs_detected_by_Colluding_at_125;
	}

	public double getPCT_Popular_MSPs_detected_by_Colluding_at_250() {
		return PCT_Popular_MSPs_detected_by_Colluding_at_250;
	}

	public void setPCT_Popular_MSPs_detected_by_Colluding_at_250(
			double pCT_Popular_MSPs_detected_by_Colluding_at_250) {
		PCT_Popular_MSPs_detected_by_Colluding_at_250 = pCT_Popular_MSPs_detected_by_Colluding_at_250;
	}

	public double getPCT_Popular_MSPs_detected_by_Colluding_at_375() {
		return PCT_Popular_MSPs_detected_by_Colluding_at_375;
	}

	public void setPCT_Popular_MSPs_detected_by_Colluding_at_375(
			double pCT_Popular_MSPs_detected_by_Colluding_at_375) {
		PCT_Popular_MSPs_detected_by_Colluding_at_375 = pCT_Popular_MSPs_detected_by_Colluding_at_375;
	}

	public double getPCT_Popular_MSPs_detected_by_Colluding_at_500() {
		return PCT_Popular_MSPs_detected_by_Colluding_at_500;
	}

	public void setPCT_Popular_MSPs_detected_by_Colluding_at_500(
			double pCT_Popular_MSPs_detected_by_Colluding_at_500) {
		PCT_Popular_MSPs_detected_by_Colluding_at_500 = pCT_Popular_MSPs_detected_by_Colluding_at_500;
	}

	public double getPCT_Popular_MSPs_detected_by_weakColluding_at_125() {
		return PCT_Popular_MSPs_detected_by_weakColluding_at_125;
	}

	public void setPCT_Popular_MSPs_detected_by_weakColluding_at_125(
			double pCT_Popular_MSPs_detected_by_weakColluding_at_125) {
		PCT_Popular_MSPs_detected_by_weakColluding_at_125 = pCT_Popular_MSPs_detected_by_weakColluding_at_125;
	}

	public double getPCT_Popular_MSPs_detected_by_weakColluding_at_250() {
		return PCT_Popular_MSPs_detected_by_weakColluding_at_250;
	}

	public void setPCT_Popular_MSPs_detected_by_weakColluding_at_250(
			double pCT_Popular_MSPs_detected_by_weakColluding_at_250) {
		PCT_Popular_MSPs_detected_by_weakColluding_at_250 = pCT_Popular_MSPs_detected_by_weakColluding_at_250;
	}

	public double getPCT_Popular_MSPs_detected_by_weakColluding_at_375() {
		return PCT_Popular_MSPs_detected_by_weakColluding_at_375;
	}

	public void setPCT_Popular_MSPs_detected_by_weakColluding_at_375(
			double pCT_Popular_MSPs_detected_by_weakColluding_at_375) {
		PCT_Popular_MSPs_detected_by_weakColluding_at_375 = pCT_Popular_MSPs_detected_by_weakColluding_at_375;
	}

	public double getPCT_Popular_MSPs_detected_by_weakColluding_at_500() {
		return PCT_Popular_MSPs_detected_by_weakColluding_at_500;
	}

	public void setPCT_Popular_MSPs_detected_by_weakColluding_at_500(
			double pCT_Popular_MSPs_detected_by_weakColluding_at_500) {
		PCT_Popular_MSPs_detected_by_weakColluding_at_500 = pCT_Popular_MSPs_detected_by_weakColluding_at_500;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_125() {
		return PCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_125;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_125(
			double pCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_125) {
		PCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_125 = pCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_125;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_250() {
		return PCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_250;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_250(
			double pCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_250) {
		PCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_250 = pCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_250;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_375() {
		return PCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_375;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_375(
			double pCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_375) {
		PCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_375 = pCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_375;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_500() {
		return PCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_500;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_500(
			double pCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_500) {
		PCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_500 = pCT_Unpopular_MPSPs_detected_by_TGTg_Rank_at_500;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_Colluding_at_125() {
		return PCT_Unpopular_MPSPs_detected_by_Colluding_at_125;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_Colluding_at_125(
			double pCT_Unpopular_MPSPs_detected_by_Colluding_at_125) {
		PCT_Unpopular_MPSPs_detected_by_Colluding_at_125 = pCT_Unpopular_MPSPs_detected_by_Colluding_at_125;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_Colluding_at_250() {
		return PCT_Unpopular_MPSPs_detected_by_Colluding_at_250;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_Colluding_at_250(
			double pCT_Unpopular_MPSPs_detected_by_Colluding_at_250) {
		PCT_Unpopular_MPSPs_detected_by_Colluding_at_250 = pCT_Unpopular_MPSPs_detected_by_Colluding_at_250;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_Colluding_at_375() {
		return PCT_Unpopular_MPSPs_detected_by_Colluding_at_375;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_Colluding_at_375(
			double pCT_Unpopular_MPSPs_detected_by_Colluding_at_375) {
		PCT_Unpopular_MPSPs_detected_by_Colluding_at_375 = pCT_Unpopular_MPSPs_detected_by_Colluding_at_375;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_Colluding_at_500() {
		return PCT_Unpopular_MPSPs_detected_by_Colluding_at_500;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_Colluding_at_500(
			double pCT_Unpopular_MPSPs_detected_by_Colluding_at_500) {
		PCT_Unpopular_MPSPs_detected_by_Colluding_at_500 = pCT_Unpopular_MPSPs_detected_by_Colluding_at_500;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_weakColluding_at_125() {
		return PCT_Unpopular_MPSPs_detected_by_weakColluding_at_125;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_weakColluding_at_125(
			double pCT_Unpopular_MPSPs_detected_by_weakColluding_at_125) {
		PCT_Unpopular_MPSPs_detected_by_weakColluding_at_125 = pCT_Unpopular_MPSPs_detected_by_weakColluding_at_125;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_weakColluding_at_250() {
		return PCT_Unpopular_MPSPs_detected_by_weakColluding_at_250;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_weakColluding_at_250(
			double pCT_Unpopular_MPSPs_detected_by_weakColluding_at_250) {
		PCT_Unpopular_MPSPs_detected_by_weakColluding_at_250 = pCT_Unpopular_MPSPs_detected_by_weakColluding_at_250;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_weakColluding_at_375() {
		return PCT_Unpopular_MPSPs_detected_by_weakColluding_at_375;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_weakColluding_at_375(
			double pCT_Unpopular_MPSPs_detected_by_weakColluding_at_375) {
		PCT_Unpopular_MPSPs_detected_by_weakColluding_at_375 = pCT_Unpopular_MPSPs_detected_by_weakColluding_at_375;
	}

	public double getPCT_Unpopular_MPSPs_detected_by_weakColluding_at_500() {
		return PCT_Unpopular_MPSPs_detected_by_weakColluding_at_500;
	}

	public void setPCT_Unpopular_MPSPs_detected_by_weakColluding_at_500(
			double pCT_Unpopular_MPSPs_detected_by_weakColluding_at_500) {
		PCT_Unpopular_MPSPs_detected_by_weakColluding_at_500 = pCT_Unpopular_MPSPs_detected_by_weakColluding_at_500;
	}

	public double getPCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_125() {
		return PCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_125;
	}

	public void setPCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_125(
			double pCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_125) {
		PCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_125 = pCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_125;
	}

	public double getPCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_250() {
		return PCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_250;
	}

	public void setPCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_250(
			double pCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_250) {
		PCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_250 = pCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_250;
	}

	public double getPCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_375() {
		return PCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_375;
	}

	public void setPCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_375(
			double pCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_375) {
		PCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_375 = pCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_375;
	}

	public double getPCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_500() {
		return PCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_500;
	}

	public void setPCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_500(
			double pCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_500) {
		PCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_500 = pCT_Unpopular_MSPs_detected_by_TGTg_Rank_at_500;
	}

	public double getPCT_Unpopular_MSPs_detected_by_Colluding_at_125() {
		return PCT_Unpopular_MSPs_detected_by_Colluding_at_125;
	}

	public void setPCT_Unpopular_MSPs_detected_by_Colluding_at_125(
			double pCT_Unpopular_MSPs_detected_by_Colluding_at_125) {
		PCT_Unpopular_MSPs_detected_by_Colluding_at_125 = pCT_Unpopular_MSPs_detected_by_Colluding_at_125;
	}

	public double getPCT_Unpopular_MSPs_detected_by_Colluding_at_250() {
		return PCT_Unpopular_MSPs_detected_by_Colluding_at_250;
	}

	public void setPCT_Unpopular_MSPs_detected_by_Colluding_at_250(
			double pCT_Unpopular_MSPs_detected_by_Colluding_at_250) {
		PCT_Unpopular_MSPs_detected_by_Colluding_at_250 = pCT_Unpopular_MSPs_detected_by_Colluding_at_250;
	}

	public double getPCT_Unpopular_MSPs_detected_by_Colluding_at_375() {
		return PCT_Unpopular_MSPs_detected_by_Colluding_at_375;
	}

	public void setPCT_Unpopular_MSPs_detected_by_Colluding_at_375(
			double pCT_Unpopular_MSPs_detected_by_Colluding_at_375) {
		PCT_Unpopular_MSPs_detected_by_Colluding_at_375 = pCT_Unpopular_MSPs_detected_by_Colluding_at_375;
	}

	public double getPCT_Unpopular_MSPs_detected_by_Colluding_at_500() {
		return PCT_Unpopular_MSPs_detected_by_Colluding_at_500;
	}

	public void setPCT_Unpopular_MSPs_detected_by_Colluding_at_500(
			double pCT_Unpopular_MSPs_detected_by_Colluding_at_500) {
		PCT_Unpopular_MSPs_detected_by_Colluding_at_500 = pCT_Unpopular_MSPs_detected_by_Colluding_at_500;
	}

	public double getPCT_Unpopular_MSPs_detected_by_weakColluding_at_125() {
		return PCT_Unpopular_MSPs_detected_by_weakColluding_at_125;
	}

	public void setPCT_Unpopular_MSPs_detected_by_weakColluding_at_125(
			double pCT_Unpopular_MSPs_detected_by_weakColluding_at_125) {
		PCT_Unpopular_MSPs_detected_by_weakColluding_at_125 = pCT_Unpopular_MSPs_detected_by_weakColluding_at_125;
	}

	public double getPCT_Unpopular_MSPs_detected_by_weakColluding_at_250() {
		return PCT_Unpopular_MSPs_detected_by_weakColluding_at_250;
	}

	public void setPCT_Unpopular_MSPs_detected_by_weakColluding_at_250(
			double pCT_Unpopular_MSPs_detected_by_weakColluding_at_250) {
		PCT_Unpopular_MSPs_detected_by_weakColluding_at_250 = pCT_Unpopular_MSPs_detected_by_weakColluding_at_250;
	}

	public double getPCT_Unpopular_MSPs_detected_by_weakColluding_at_375() {
		return PCT_Unpopular_MSPs_detected_by_weakColluding_at_375;
	}

	public void setPCT_Unpopular_MSPs_detected_by_weakColluding_at_375(
			double pCT_Unpopular_MSPs_detected_by_weakColluding_at_375) {
		PCT_Unpopular_MSPs_detected_by_weakColluding_at_375 = pCT_Unpopular_MSPs_detected_by_weakColluding_at_375;
	}

	public double getPCT_Unpopular_MSPs_detected_by_weakColluding_at_500() {
		return PCT_Unpopular_MSPs_detected_by_weakColluding_at_500;
	}

	public void setPCT_Unpopular_MSPs_detected_by_weakColluding_at_500(
			double pCT_Unpopular_MSPs_detected_by_weakColluding_at_500) {
		PCT_Unpopular_MSPs_detected_by_weakColluding_at_500 = pCT_Unpopular_MSPs_detected_by_weakColluding_at_500;
	}

	
	
}
