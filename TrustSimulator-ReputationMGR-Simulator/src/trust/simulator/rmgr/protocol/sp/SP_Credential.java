package trust.simulator.rmgr.protocol.sp;

import trust.simulator.rmgr.protocol.user.Credential;

public class SP_Credential extends Credential{
	
	private int spamedCount;
	private long obtainedInCycle;
	private boolean droped;
	private boolean MEallowedSPAM;
	private long sharedBySPid;
	private boolean directInteract;
	
	public SP_Credential()
	{
		super();
		spamedCount = 0;
		setDroped(false);
		MEallowedSPAM = true;
		sharedBySPid = -1;
		directInteract = false;
	}
	
	public SP_Credential(int id, int nodePos, long obtainedInCycle, int sharingPermission, long sharedBySPid)
	{
		super(id, nodePos, sharingPermission);
		spamedCount = 0;
		this.obtainedInCycle = obtainedInCycle;
		setDroped(false);
		MEallowedSPAM = true;
		this.sharedBySPid = sharedBySPid;
		this.directInteract = (sharedBySPid == 1);

	}
		
	public int getSpamedCount() {
		return spamedCount;
	}

	public void increaseSpamedCount() {
		spamedCount++;
	}

	public long getObtainedInCycle() {
		return obtainedInCycle;
	}

	public void setObtainedInCycle(long obtainedInCycle) {
		this.obtainedInCycle = obtainedInCycle;
	}

	public boolean isDroped() {
		return droped;
	}

	public void setDroped(boolean droped) {
		this.droped = droped;
	}

	public boolean isMEallowedSPAM() {
		return MEallowedSPAM;
	}

	public void setMEallowedSPAM(boolean mEallowedSPAM) {
		MEallowedSPAM = mEallowedSPAM;
	}

	public long getSharedBySPid() {
		return sharedBySPid;
	}

	public void setSharedBySPid(long sharedBySPid) {
		this.sharedBySPid = sharedBySPid;
	}

	public boolean isDirectInteract() {
		return directInteract;
	}

	public void setDirectInteract(boolean directInteract) {
		this.directInteract = directInteract;
	}

	
}
