package trust.simulator.rmgr.protocol.sp;

public class Partner {
	
	private int partnerID = -1;
	
	private int partnershipCreationCylce = -1;
	
	private int partnershipDuration = -1;
	
	private boolean colluding = false;
	
	private boolean malicious = false;
	
	private double sharingProb = -1;
	
	private boolean active = false;
	
	private boolean psp = false;
	
	private int sharedCount = 0;
	
	public Partner()
	{
		
	}
	
	public Partner(int parnerID, int partnershipCreationCylce,
			int partnershipDuration, boolean colluding, boolean malicious,
			double sharingProb, boolean active, boolean psp) {
		super();
		this.partnerID = parnerID;
		this.partnershipCreationCylce = partnershipCreationCylce;
		this.partnershipDuration = partnershipDuration;
		this.colluding = colluding;
		this.malicious = malicious;
		this.sharingProb = sharingProb;
		this.active = active;
		this.psp = psp;
	}

	public void incrementSharedCount()
	{
		sharedCount++;
	}
	
	public int getPartnerID() {
		return partnerID;
	}

	public void setPartnerID(int partnerID) {
		this.partnerID = partnerID;
	}

	public int getPartnershipCreationCylce() {
		return partnershipCreationCylce;
	}

	public void setPartnershipCreationCylce(int partnershipCreationCylce) {
		this.partnershipCreationCylce = partnershipCreationCylce;
	}

	public int getPartnershipDuration() {
		return partnershipDuration;
	}

	public void setPartnershipDuration(int partnershipDuration) {
		this.partnershipDuration = partnershipDuration;
	}

	public boolean isColluding() {
		return colluding;
	}

	public void setColluding(boolean colluding) {
		this.colluding = colluding;
	}

	public boolean isMalicious() {
		return malicious;
	}

	public void setMalicious(boolean malicious) {
		this.malicious = malicious;
	}

	public double getSharingProb() {
		return sharingProb;
	}

	public void setSharingProb(double sharingProb) {
		this.sharingProb = sharingProb;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isPsp() {
		return psp;
	}

	public void setPsp(boolean psp) {
		this.psp = psp;
	}

	public int getSharedCount() {
		return sharedCount;
	}

	public void setSharedCount(int sharedCount) {
		this.sharedCount = sharedCount;
	}


}
