package trust.simulator.rmgr.protocol.sp;

import java.util.ArrayList;
import java.util.List;


public class SP_UsersNodesDir {
	
	private int position;
	
	//TODO optimize the size of this array
	List<SP_Credential> Credentials = new ArrayList<SP_Credential>(100);
	
	private boolean droped;
	
	private boolean MEallowedSPAM;
	
	public SP_UsersNodesDir() {
		super();
		
		position = -1;
		setDroped(false);
		MEallowedSPAM = true;
		
	}


	public int getPosition() {
		return position;
	}


	public void setPosition(int position) {
		this.position = position;
	}


	public boolean isDroped() {
		return droped;
	}


	public void setDroped(boolean droped) {
		this.droped = droped;
	}


	public boolean isMEallowedSPAM() {
		return MEallowedSPAM;
	}


	public void setMEallowedSPAM(boolean mEallowedSPAM) {
		MEallowedSPAM = mEallowedSPAM;
	}

}
