package trust.simulator.rmgr.protocol.sp;

import java.util.ArrayList;
import java.util.List;

import peersim.config.Configuration;
import peersim.core.CommonState;
import peersim.core.Network;
import trust.simulator.rmgr.protocol.RMGR_Node;
import trust.simulator.rmgr.protocol.auditor.Auditor_Node;
import trust.simulator.rmgr.protocol.idp.IDP_Node;
import trust.simulator.rmgr.protocol.idp.SPsNodesDir;
import trust.simulator.rmgr.protocol.me.ME_Node;
import trust.simulator.rmgr.protocol.user.Credential;

public class SP_Node extends RMGR_Node{
	
	private String thePrefix;
	
	private double usefulness;
	
	private boolean malicious;
	
	private boolean associatedWithME;
	
	private int MEid;
	
	private List<SP_UsersNodesDir> uNodes;

	//Simple Attacks Strategies Settings
	
	private double SPAM_Drop_StrictCredential_Prob;
	
	private boolean SPAM_Delay;
	private int SPAM_Delay_Period;
	
	private boolean SPAM_Bombarding;
	private int SPAM_Bombarding_Period;
	
	private boolean SPAM_Drop;
	private double SPAM_DropUser_Rate;
	private double SPAM_DropCredential_Rate;
	
	private int spamsCount = 0;
	
	private boolean MEallowed = true;
	
	private boolean banned = false;
	
	private int installDGUdetectorType = -1;
	
	private int enableStrictDGUdetectorType = -1;
	
	private int BanDetectorType = -1;
	
	private int bannedCycle = -1;
	
	private List<Partner> Partners = new ArrayList<Partner>(20);
	
	private double newPartnershipProb;
	
	private int newPartnershipDuration;
	
	private double partnerSharingProb;

	private double AcceptDGUProb;
	
	private double AcceptStrictDGUProb;
	
	private double AcceptCompulsoryStrictDGUProb;
	private double AcceptCompulsoryDGUProb;
	
	private boolean installedDGU = false;
	private int installedDGUCycle = -1;

	private boolean enabledStrictDGU = false;
	private int enabledStrictDGUCycle = -1;

	public SP_Node(String prefix) {
		super(prefix);
		thePrefix = prefix;
		
		usefulness = -1;
		
		uNodes = new ArrayList<SP_UsersNodesDir> (100);
		
		malicious = false;
		associatedWithME = false;
		MEid = -1;
		
		SPAM_Drop_StrictCredential_Prob = 0;
		
		SPAM_Delay = false;
		SPAM_Delay_Period = -1;
		
		SPAM_Bombarding = false;
		SPAM_Bombarding_Period = -1;
		
		SPAM_Drop = false;
		SPAM_DropUser_Rate = -1;
		SPAM_DropCredential_Rate = -1;
	}
	
	public Object clone() {

        SP_Node inp = new SP_Node(thePrefix);
        return inp;
    }
	
	public void updatePartnerships()
	{
		// Manage current partnerships
		for(Partner partner: Partners)
		{
			if(partner.isActive() && CommonState.getIntTime()-partner.getPartnershipCreationCylce() >= partner.getPartnershipDuration())
			{
				partner.setActive(false);
			}
		}
		
		// Create new partnerships
		boolean createNewPartnership = this.randBoolean(1-(this.newPartnershipProb/100));
		
		if(createNewPartnership)
		{
			IDP_Node idp = (IDP_Node) Network.get(1).getProtocol(IDPPid);
			
			//PSP?
			boolean pspPartner = this.randBoolean(1 - (this.usefulnessMIN/100));
			
			SPsNodesDir newPartner = idp.getRandSP(pspPartner);
			
			if(newPartner != null)
			{
				boolean newPartnerFound = false;
				for(Partner oldPartner: Partners)
				{
					if(oldPartner.isActive() && oldPartner.getPartnerID() == newPartner.getPosition())
					{
						newPartnerFound = true;
						break;
					}
				}
				if(!newPartnerFound)
				{
					boolean isPSP = this.usefulness >= this.usefulnessMIN;
					
					Partner newPartnership = new Partner(newPartner.getPosition(), CommonState.getIntTime(),
							newPartnershipDuration, false, newPartner.isMalicious(),
							partnerSharingProb, true, pspPartner);
					Partners.add(newPartnership);
					SP_Node newPartnerSP = (SP_Node) Network.get(newPartner.getPosition()).getProtocol(SPPid);
					newPartnerSP.confirmPartnership(nodePos, CommonState.getIntTime(), newPartnershipDuration, malicious, false, isPSP);
				}
			}
		}
	}
	
	public void confirmPartnership(int partnerID, int creationCylce, int duration, boolean maliciousPartner, boolean colluding, boolean psp )
	{
		double partnerSharingProb = this.partnerSharingProb;
		
		if(colluding) partnerSharingProb = 0;
		
		Partner newPartnership = new Partner(partnerID, CommonState.getIntTime(),
				duration, colluding, maliciousPartner,
				partnerSharingProb, true, psp);
		Partners.add(newPartnership);
	}
	
	public void terminateAllPartnerships()
	{
		for(Partner partner: Partners)
		{
			if(partner.isActive())
			{
				partner.setActive(false);
				SP_Node partnerSP = (SP_Node) Network.get(partner.getPartnerID()).getProtocol(SPPid);
				partnerSP.terminatePartnership(partner.getPartnerID());
			}
		}
	}
	
	public void terminatePartnership(int partnerID)
	{
		for(Partner partner: Partners)
		{
			if(partner.getPartnerID() == partnerID)
			{
				partner.setActive(false);
				break;
			}
		}
	}
	
	public List<Credential> runAttacks()
	{
		List<Credential> spamCredentials = new ArrayList<Credential>();
		//TODO Add a condition to return null if there is no attack configured
		
		if(!MEallowed || installedDGU)
			return spamCredentials;
		
		long currentCycle = CommonState.getTime();
		long waitingPeriod;

		SP_UsersNodesDir user;
		List<SP_Credential> credentials;
		SP_Credential credential;
		
		int spamCount;
		if(SPAM_Bombarding) spamCount = SPAM_Bombarding_Period;
		else spamCount = 1;
		
		for(int i =0; i < uNodes.size(); i++)
		{
			user = uNodes.get(i);
			credentials = user.Credentials;
			
			if(!user.isDroped())
			{
				if(associatedWithME && !user.isMEallowedSPAM())
					continue;
				
				for(int j = 0; j < credentials.size(); j++)
				{	
					credential = credentials.get(j);
					if(!credential.isDroped())
					{
						if(associatedWithME && !credential.isMEallowedSPAM())
							continue;
						
						waitingPeriod = currentCycle - credential.getObtainedInCycle();

						if(waitingPeriod >= SPAM_Delay_Period && credential.getSpamedCount() < spamCount)
						{
							spamCredentials.add(credential);
							
							if(credential.getSpamedCount() == 0)
								setSpamsCount(getSpamsCount() + 1);
						
							credential.increaseSpamedCount();
							uNodes.get(i).Credentials.set(j, credential);
						}
					}
				}
			}
		}
		
		return spamCredentials;
	}
	
	//Returns SPids of the unsafePartner/s a DGU SP shared the credential with
	public List<Partner> addCredential(Credential credential, List<Long> SharedBySPs, boolean sharedByColluding)
	{
		//boolean sharedWithUnsafePartners = false;
		
		List<Partner> sharedWithUnsafePartners = new ArrayList<Partner>();
		
		int userPos = credential.getNodePosition();
		boolean userFound = false;
		boolean credentialFound = false;
		boolean updateCredential = false;
		SP_UsersNodesDir uNode = new SP_UsersNodesDir();
		List<SP_Credential> credentials; 
		SP_Credential tempCredential;
		SP_Credential newCredential;
		
		long SharedBySPid = SharedBySPs.get(SharedBySPs.size()-1);
		
		for(int i = 0; i < uNodes.size(); i++)
		{
			uNode = uNodes.get(i);
			credentials = uNode.Credentials;
			if(uNode.getPosition() == userPos)
			{
				userFound = true;
				
				if(!uNode.isDroped())
				{
					for(int j = 0; j < credentials.size(); j++)
					{
						tempCredential = credentials.get(j);
						
						if(tempCredential.getId() == credential.getId())
						{
							credentialFound = true;
							
							if(associatedWithME && installedDGU && !enabledStrictDGU)
							{
								if(!tempCredential.isDirectInteract() && SharedBySPid == 1)
								{
									tempCredential.setDirectInteract(true);
									tempCredential.setSharedBySPid(SharedBySPid);
									updateCredential = true;
								}
								else if(tempCredential.getSharedBySPid() != SharedBySPid && SharedBySPid > 2)
								{
									tempCredential.setSharedBySPid(SharedBySPid);
									updateCredential = true;
								}
							}
							
							break;
						}
					}
				}
				
				break;
			}
		}
		
		if(!userFound)
		{
			uNode = new SP_UsersNodesDir();
			uNode.setPosition(credential.getNodePosition());
			
			if(SPAM_Drop)
			{
				boolean droped = this.randBoolean(1 - SPAM_DropUser_Rate/100);
				uNode.setDroped(droped);
			}

			uNodes.add(uNode);
			
			userFound = true;
		}
		
		if(userFound && (!credentialFound || updateCredential) )
		{
			newCredential = new SP_Credential(credential.getId(), credential.getNodePosition(), CommonState.getTime(), credential.getSharingPermission(), SharedBySPid);
		
			if(!credentialFound)
			{
				boolean droped = false;
				
				if(credential.getSharingPermission() == 0)
					droped = this.randBoolean(1 - SPAM_Drop_StrictCredential_Prob/100);
				
				if(!droped && SPAM_Drop)
					droped = this.randBoolean(1 - SPAM_DropCredential_Rate/100);
				
				newCredential.setDroped(droped);
				
				uNode.Credentials.add(newCredential);
						
				if(SharedBySPid != 1)
					for(Partner partner: Partners)
					{
						if(partner.getPartnerID() == SharedBySPid)
							partner.incrementSharedCount();
					}
			}
			
			if(!enabledStrictDGU && associatedWithME)
			{
				ME_Node ME = (ME_Node) Network.get(MEid).getProtocol(MEPid);

				if(!installedDGU && !credentialFound)
				{
					List<Boolean> result = ME.addCredential(usefulness, nodePos, newCredential);
					uNode.setMEallowedSPAM(result.get(0));
					newCredential.setMEallowedSPAM(result.get(1));
				}
				else if(installedDGU && credential.getSharingPermission() != 0)
				{
					int safeNieghbour = -1;
					boolean pspPartner = false;
					
					if(credential.getSharingPermission() == 1)
						pspPartner = true;
					
					else if(credential.getSharingPermission() == 2)
						pspPartner = false;
					
						safeNieghbour = ME.getSafeNieghbour(nodePos, pspPartner);
					
					if(safeNieghbour > -1)
					{
						boolean partnerFound = false;
						
						Partner finalPartner = new Partner();
						
						for(Partner partner: Partners)
						{
							if(partner.getPartnerID() == safeNieghbour)
							{
								partnerFound = true;
								partner.incrementSharedCount();
								
								if(!partner.isColluding())
								{
									partner.setActive(true);
									partner.setColluding(true);
									partner.setMalicious(true);
									partner.setPartnershipDuration(20000000);
								}
								
								finalPartner = partner;
								
								break;
							}
						}
						
						if(!partnerFound)
						{
							boolean isPSP = this.usefulness >= this.usefulnessMIN;
							
							Partner newPartnership = new Partner(safeNieghbour, CommonState.getIntTime(),
									20000000, true, true, 0, true, pspPartner);
							Partners.add(newPartnership);
							SP_Node newPartnerSP = (SP_Node) Network.get(safeNieghbour).getProtocol(SPPid);
							newPartnerSP.confirmPartnership(nodePos, CommonState.getIntTime(), 20000000, malicious, true, isPSP);
							finalPartner = newPartnership;
						}
						
						SP_Node safeNieghbourSP = (SP_Node) Network.get(safeNieghbour).getProtocol(SPPid);
						SharedBySPs.add((long) nodePos);
						safeNieghbourSP.addCredential(credential, SharedBySPs, true);
						sharedWithUnsafePartners.add(finalPartner);
					}
				}
			}
			
			if(!sharedByColluding && credential.getSharingPermission() != 0)
			{
				List<Partner> toShareWithPartners = new ArrayList<Partner>(Partners.size());
				
				for(Partner partner: Partners)
				{
					if(!partner.isActive() || partner.isColluding() || partner.getPartnerID() == SharedBySPid)
						continue;
					
					boolean partnerInChain = false;
					for(long i: SharedBySPs)
						if(i == partner.getPartnerID())
						{
							partnerInChain = true;
							break;
						}
					
					if(partnerInChain)
						continue;
							
					
					boolean share = false;
					
					if(credential.getSharingPermission() == 2 || partner.isPsp())
					{
						share = this.randBoolean(1-partner.getSharingProb()/100);
						
						if(share)
							toShareWithPartners.add(partner);
					}
				}
					
				for(Partner partner: toShareWithPartners)
				{
					SP_Node partnerSP = (SP_Node) Network.get(partner.getPartnerID()).getProtocol(SPPid);
					
					if( !(isEnabledStrictDGU() && !partnerSP.isInstalledDGU()) )
					{
						SharedBySPs.add((long) nodePos);
					}
				}
				
				for(Partner partner: toShareWithPartners)
				{					
					SP_Node partnerSP = (SP_Node) Network.get(partner.getPartnerID()).getProtocol(SPPid);
					
					if( !(isEnabledStrictDGU() && !partnerSP.isInstalledDGU()) )
					{	
						List<Partner> partnerSharedWithPartners = partnerSP.addCredential(credential, SharedBySPs, false);
						partner.incrementSharedCount();
						
						if(this.isInstalledDGU() && !partnerSP.isEnabledStrictDGU())
							sharedWithUnsafePartners.add(partner);
						
						if(this.isInstalledDGU())
							for(Partner tempPartner: partnerSharedWithPartners)
								sharedWithUnsafePartners.add(tempPartner);
					}
				}
				
			}
		}
		
		return sharedWithUnsafePartners;
	}
	
	public List<Partner> addCredential(Credential credential)
	{
		List<Long> sharedBySPs = new ArrayList<Long>();
		
		sharedBySPs.add((long) 1);
		
		return addCredential(credential, sharedBySPs, false);
	}
	
	public void ME_AllowUserSPAM(int userID, boolean allow)
	{
		for(SP_UsersNodesDir user: uNodes)
		{
			if(user.getPosition() == userID)
			{
				user.setMEallowedSPAM(allow);
				break;
			}
		}
	}
	
	public void ME_AllowCredentialSPAM(int userID, int credentialID, boolean allow)
	{
		for(SP_UsersNodesDir user: uNodes)
		{
			if(user.getPosition() == userID)
			{
				if(allow)
					user.setMEallowedSPAM(true);
				
				for(SP_Credential credential: user.Credentials)
				{
					if(credential.getId() == credentialID)
					{
						credential.setMEallowedSPAM(allow);
						break;
					}
				}
				break;
			}
		}
	}

	public void ME_AllowAllUserCredentialsSPAM(int userID, boolean allow)
	{
		for(SP_UsersNodesDir user: uNodes)
		{
			if(user.getPosition() == userID)
			{
				user.setMEallowedSPAM(allow);
				
				for(SP_Credential credential: user.Credentials)
				{
					credential.setMEallowedSPAM(allow);
				}
				break;
			}
		}
	}
	
	public boolean acceptDGUoffer(boolean optional)
	{		
		boolean result = false;
		
		if(malicious && optional)
			result = false;
		else if(!malicious && optional)
			result = this.randBoolean(1-(AcceptDGUProb/100));
		else if(malicious && !optional)
			result = true;
		else
			result = this.randBoolean(1-(AcceptCompulsoryDGUProb/100));
		
		//System.out.println("SPid: "+this.nodePos+" acceptDGUoffer! result= "+result);
		
		if(result)
		{
			this.setInstalledDGU(true);
		}
		
		return (result);
	}
	
	public boolean acceptStrictDGUoffer(boolean optional)
	{
		boolean result = false;
		
		if(malicious && optional)
			result = false;
		else if(!malicious && optional)
			result = this.randBoolean(1-(AcceptStrictDGUProb/100));
		else if(malicious && !optional)
			result = true;
		else
			result = this.randBoolean(1-(AcceptCompulsoryStrictDGUProb/100));
		
		if(result)
		{
			this.setEnabledStrictDGU(true);
		}
		
		return (result);
	}

	
	public double getUsefulness() {
		return usefulness;
	}

	public void setUsefulness(double usefulness) {
		this.usefulness = usefulness;
	}


	public boolean isMalicious() {
		return malicious;
	}

	public void setMalicious(boolean malicious) {
		this.malicious = malicious;
	}
	
	public boolean isSPAM_Delay() {
		return SPAM_Delay;
	}
	public void setSPAM_Delay(boolean SPAM_Delay) {
		this.SPAM_Delay = SPAM_Delay;
	}
	public int getSPAM_Delay_Period() {
		return SPAM_Delay_Period;
	}
	public void setSPAM_Delay_Period(int SPAM_Delay_Period) {
		this.SPAM_Delay_Period = SPAM_Delay_Period;
	}
	public boolean isSPAM_Bombarding() {
		return SPAM_Bombarding;
	}
	public void setSPAM_Bombarding(boolean SPAM_Bombarding) {
		this.SPAM_Bombarding = SPAM_Bombarding;
	}
	public int getSPAM_Bombarding_Period() {
		return SPAM_Bombarding_Period;
	}
	public void setSPAM_Bombarding_Period(int SPAM_Bombarding_Period) {
		this.SPAM_Bombarding_Period = SPAM_Bombarding_Period;
	}
	public boolean isSPAM_Drop() {
		return SPAM_Drop;
	}
	public void setSPAM_Drop(boolean SPAM_Drop) {
		this.SPAM_Drop = SPAM_Drop;
	}
	public double getSPAM_DropUser_Rate() {
		return SPAM_DropUser_Rate;
	}
	public void setSPAM_DropUser_Rate(double SPAM_DropUser_Rate) {
		this.SPAM_DropUser_Rate = SPAM_DropUser_Rate;
	}
	public double getSPAM_DropCredential_Rate() {
		return SPAM_DropCredential_Rate;
	}
	public void setSPAM_DropCredential_Rate(double SPAM_DropCredential_Rate) {
		this.SPAM_DropCredential_Rate = SPAM_DropCredential_Rate;
	}
	public boolean isAssociatedWithME() {
		return associatedWithME;
	}
	public void setAssociatedWithME(boolean associatedWithME) {
		this.associatedWithME = associatedWithME;
	}
	public int getMEid() {
		return MEid;
	}
	public void setMEid(int mEid) {
		MEid = mEid;
	}

	public List<SP_UsersNodesDir> getuNodes() {
		return uNodes;
	}

	public void setuNodes(List<SP_UsersNodesDir> uNodes) {
		this.uNodes = uNodes;
	}

	public int getSpamsCount() {
		return spamsCount;
	}

	public void setSpamsCount(int spamsCount) {
		this.spamsCount = spamsCount;
	}

	public boolean isMEallowed() {
		return MEallowed;
	}

	public void setMEallowed(boolean mEallowed) {
		MEallowed = mEallowed;
	}

	public boolean isBanned() {
		return banned;
	}

	public void setBanned(boolean banned) {

		this.banned = banned;
		this.bannedCycle = CommonState.getIntTime();
		
		if(this.associatedWithME)
		{
			ME_Node me = (ME_Node) Network.get(MEid).getProtocol(MEPid);
			
			if(usefulness >= usefulnessMIN)
				me.recieveSPUpdate(nodePos, true, 1,bannedCycle );
			else
				me.recieveSPUpdate(nodePos, false, 1, bannedCycle);
		}

	}

	public int getBannedCycle() {
		return bannedCycle;
	}

	public void setBannedCycle(int bannedCycle) {
		this.bannedCycle = bannedCycle;
	}

	public double getNewPartnershipProb() {
		return newPartnershipProb;
	}

	public void setNewPartnershipProb(double newPartnershipProb) {
		this.newPartnershipProb = newPartnershipProb;
	}

	public int getNewPartnershipDuration() {
		return newPartnershipDuration;
	}

	public void setNewPartnershipDuration(int newPartnershipDuration) {
		this.newPartnershipDuration = newPartnershipDuration;
	}

	public double getPartnerSharingProb() {
		return partnerSharingProb;
	}

	public void setPartnerSharingProb(double partnerSharingProb) {
		this.partnerSharingProb = partnerSharingProb;
	}

	public List<Partner> getPartners() {
		return Partners;
	}

	public void setPartners(List<Partner> partners) {
		Partners = partners;
	}

	public boolean isInstalledDGU() {
		return installedDGU;
	}

	public void setInstalledDGU(boolean installedDGU) {

		this.installedDGU = installedDGU;
		
		if(installedDGU)
		{
		
			this.installedDGUCycle = CommonState.getIntTime();
			
			//Auditor_Node auditor = (Auditor_Node) Network.get(2).getProtocol(AuditorPid);

			//auditor.updateSPnodeStatus(nodePos, 1, -1);
			
			if(associatedWithME)
			{
				ME_Node me = (ME_Node) Network.get(MEid).getProtocol(MEPid);
				
				if(usefulness >= usefulnessMIN)
					me.recieveSPUpdate(nodePos, true, 2,installedDGUCycle );
				else
					me.recieveSPUpdate(nodePos, false, 2, installedDGUCycle);
			}
			
		}
	}

	public double getAcceptDGUProb() {
		return AcceptDGUProb;
	}

	public void setAcceptDGUProb(double acceptDGUProb) {
		AcceptDGUProb = acceptDGUProb;
	}

	public int getInstalledDGUCycle() {
		return installedDGUCycle;
	}

	public void setInstalledDGUCycle(int installedDGUCycle) {
		this.installedDGUCycle = installedDGUCycle;
	}

	public double getAcceptSTrictDGUProb() {
		return AcceptStrictDGUProb;
	}

	public void setAcceptSTrictDGUProb(double acceptStrictDGUProb) {
		AcceptStrictDGUProb = acceptStrictDGUProb;
	}

	public boolean isEnabledStrictDGU() {
		return enabledStrictDGU;
	}

	public void setEnabledStrictDGU(boolean enabledStrictDGU) {
	
		this.enabledStrictDGU = enabledStrictDGU;
		
		if(enabledStrictDGU)
		{
			enabledStrictDGUCycle = CommonState.getIntTime();
			
			//Auditor_Node auditor = (Auditor_Node) Network.get(2).getProtocol(AuditorPid);
	
			//auditor.updateSPnodeStatus(nodePos, 2, -1);
			
			if(associatedWithME)
			{
				ME_Node me = (ME_Node) Network.get(MEid).getProtocol(MEPid);
				
				if(usefulness >= usefulnessMIN)
					me.recieveSPUpdate(nodePos, true, 3, enabledStrictDGUCycle );
				else
					me.recieveSPUpdate(nodePos, false, 3, enabledStrictDGUCycle);
			}
		}
		
	}

	public int getEnabledStrictDGUCycle() {
		return enabledStrictDGUCycle;
	}

	public void setEnabledStrictDGUCycle(int enabledStrictDGUCycle) {
		this.enabledStrictDGUCycle = enabledStrictDGUCycle;
	}

	public double getSPAM_Drop_StrictCredential_Prob() {
		return SPAM_Drop_StrictCredential_Prob;
	}

	public void setSPAM_Drop_StrictCredential_Prob(
			double sPAM_Drop_StrictCredential_Prob) {
		SPAM_Drop_StrictCredential_Prob = sPAM_Drop_StrictCredential_Prob;
	}

	public int getInstallDGUdetectorType() {
		return installDGUdetectorType;
	}

	public void setInstallDGUdetectorType(int installDGUdetectorType) {
		this.installDGUdetectorType = installDGUdetectorType;
	}

	public int getEnableStrictDGUdetectorType() {
		return enableStrictDGUdetectorType;
	}

	public void setEnableStrictDGUdetectorType(int enableStrictDGUdetectorType) {
		this.enableStrictDGUdetectorType = enableStrictDGUdetectorType;
	}

	public int getBanDetectorType() {
		return BanDetectorType;
	}

	public void setBanDetectorType(int banDetectorType) {
		BanDetectorType = banDetectorType;
	}

	public double getAcceptCompulsoryStrictDGUProb() {
		return AcceptCompulsoryStrictDGUProb;
	}

	public void setAcceptCompulsoryStrictDGUProb(
			double acceptCompulsoryStrictDGUProb) {
		AcceptCompulsoryStrictDGUProb = acceptCompulsoryStrictDGUProb;
	}

	public double getAcceptCompulsoryDGUProb() {
		return AcceptCompulsoryDGUProb;
	}

	public void setAcceptCompulsoryDGUProb(double acceptCompulsoryDGUProb) {
		AcceptCompulsoryDGUProb = acceptCompulsoryDGUProb;
	}
	
}
