package trust.simulator.rmgr.protocol.sp;

import java.util.ArrayList;
import java.util.List;

public class SPCycleRecord {
	
	private int spPos;
	private int spType;
	private int cycle;	
	private boolean banned;
	private boolean malicious;
	
	private boolean associatedWithME;
	
	private int MEid;
	
	private List<SP_UsersNodesDir> uNodes;

	//Simple Attacks Strategies Settings
	private boolean SPAM_Delay;
	private int SPAM_Delay_Period;
	
	private boolean SPAM_Bombarding;
	private int SPAM_Bombarding_Period;
	
	private boolean SPAM_Drop;
	private double SPAM_DropUser_Rate;
	private double SPAM_DropCredential_Rate;
	
	private int spamsCount = 0;
	
	private boolean MEallowed = true;
	
	private int bannedCycle = -1;
	
	private List<Partner> Partners = new ArrayList<Partner>(20);
	
	private double newPartnershipProb = 5;
	
	private int newPartnershipDuration = 50;
	
	private double partnerSharingProb = 30;
	
	
	public SPCycleRecord()
	{
		spPos = -1;
		spType = -1;
		cycle = -1;	
		banned = false;
		malicious = false;
				
	}
	
	public SPCycleRecord(int spPos, int spType, int cycle, boolean banned, boolean malicious) {
	
		this.spPos = spPos;
		this.spType = spType;
		this.cycle = cycle;
		this.banned = banned;
		this.malicious = malicious;
		
	}
	

	public int getCycle() {
		return cycle;
	}

	public void setCycle(int cycle) {
		this.cycle = cycle;
	}

	public int getSpPos() {
		return spPos;
	}

	public void setSpPos(int spPos) {
		this.spPos = spPos;
	}

	public int getSpType() {
		return spType;
	}

	public void setSpType(int spType) {
		this.spType = spType;
	}

	public boolean isBanned() {
		return banned;
	}

	public void setBanned(boolean banned) {
		this.banned = banned;
	}

	public boolean isMalicious() {
		return malicious;
	}

	public void setMalicious(boolean malicious) {
		this.malicious = malicious;
	}

	public boolean isAssociatedWithME() {
		return associatedWithME;
	}

	public void setAssociatedWithME(boolean associatedWithME) {
		this.associatedWithME = associatedWithME;
	}

	public int getMEid() {
		return MEid;
	}

	public void setMEid(int mEid) {
		MEid = mEid;
	}

	public List<SP_UsersNodesDir> getuNodes() {
		return uNodes;
	}

	public void setuNodes(List<SP_UsersNodesDir> uNodes) {
		this.uNodes = uNodes;
	}

	public boolean isSPAM_Delay() {
		return SPAM_Delay;
	}

	public void setSPAM_Delay(boolean sPAM_Delay) {
		SPAM_Delay = sPAM_Delay;
	}

	public int getSPAM_Delay_Period() {
		return SPAM_Delay_Period;
	}

	public void setSPAM_Delay_Period(int sPAM_Delay_Period) {
		SPAM_Delay_Period = sPAM_Delay_Period;
	}

	public boolean isSPAM_Bombarding() {
		return SPAM_Bombarding;
	}

	public void setSPAM_Bombarding(boolean sPAM_Bombarding) {
		SPAM_Bombarding = sPAM_Bombarding;
	}

	public int getSPAM_Bombarding_Period() {
		return SPAM_Bombarding_Period;
	}

	public void setSPAM_Bombarding_Period(int sPAM_Bombarding_Period) {
		SPAM_Bombarding_Period = sPAM_Bombarding_Period;
	}

	public boolean isSPAM_Drop() {
		return SPAM_Drop;
	}

	public void setSPAM_Drop(boolean sPAM_Drop) {
		SPAM_Drop = sPAM_Drop;
	}

	public double getSPAM_DropUser_Rate() {
		return SPAM_DropUser_Rate;
	}

	public void setSPAM_DropUser_Rate(double sPAM_DropUser_Rate) {
		SPAM_DropUser_Rate = sPAM_DropUser_Rate;
	}

	public double getSPAM_DropCredential_Rate() {
		return SPAM_DropCredential_Rate;
	}

	public void setSPAM_DropCredential_Rate(double sPAM_DropCredential_Rate) {
		SPAM_DropCredential_Rate = sPAM_DropCredential_Rate;
	}

	public int getSpamsCount() {
		return spamsCount;
	}

	public void setSpamsCount(int spamsCount) {
		this.spamsCount = spamsCount;
	}

	public boolean isMEallowed() {
		return MEallowed;
	}

	public void setMEallowed(boolean mEallowed) {
		MEallowed = mEallowed;
	}

	public int getBannedCycle() {
		return bannedCycle;
	}

	public void setBannedCycle(int bannedCycle) {
		this.bannedCycle = bannedCycle;
	}

	public List<Partner> getPartners() {
		return Partners;
	}

	public void setPartners(List<Partner> partners) {
		Partners = partners;
	}

	public double getNewPartnershipProb() {
		return newPartnershipProb;
	}

	public void setNewPartnershipProb(double newPartnershipProb) {
		this.newPartnershipProb = newPartnershipProb;
	}

	public int getNewPartnershipDuration() {
		return newPartnershipDuration;
	}

	public void setNewPartnershipDuration(int newPartnershipDuration) {
		this.newPartnershipDuration = newPartnershipDuration;
	}

	public double getPartnerSharingProb() {
		return partnerSharingProb;
	}

	public void setPartnerSharingProb(double partnerSharingProb) {
		this.partnerSharingProb = partnerSharingProb;
	}

}
