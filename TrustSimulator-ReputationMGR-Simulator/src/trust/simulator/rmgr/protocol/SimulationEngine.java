package trust.simulator.rmgr.protocol;

import java.util.List;

import peersim.cdsim.CDProtocol;
import peersim.config.Configuration;
import peersim.config.FastConfig;
import peersim.core.CommonState;
import peersim.core.Linkable;
import peersim.core.Network;
import peersim.core.Node;
import trust.simulator.rmgr.protocol.auditor.Auditor_Node;
import trust.simulator.rmgr.protocol.auditor.Auditor_SPsNodesDir;
import trust.simulator.rmgr.protocol.auditor.Auditor_STNodesDir;
import trust.simulator.rmgr.protocol.auditor.Auditor_UsersNodesDir;
import trust.simulator.rmgr.protocol.auditor.CaseLogs;
import trust.simulator.rmgr.protocol.auditor.IILrecord;
import trust.simulator.rmgr.protocol.auditor.PColludingRecord;
import trust.simulator.rmgr.protocol.auditor.PIILrecord;
import trust.simulator.rmgr.protocol.auditor.SPRrecord;
import trust.simulator.rmgr.protocol.auditor.TGRRank;
import trust.simulator.rmgr.protocol.auditor.TGTgRank;
import trust.simulator.rmgr.protocol.auditor.TLRavgRank;
import trust.simulator.rmgr.protocol.idp.IDP_Node;
import trust.simulator.rmgr.protocol.idp.Log;
import trust.simulator.rmgr.protocol.idp.SPsNodesDir;
import trust.simulator.rmgr.protocol.idp.UsersNodesDir;
import trust.simulator.rmgr.protocol.sp.Partner;
import trust.simulator.rmgr.protocol.sp.SP_Node;
import trust.simulator.rmgr.protocol.superNode.Super_Node;
import trust.simulator.rmgr.protocol.me.ME_Node;
import trust.simulator.rmgr.protocol.me.ME_OwningSP;
import trust.simulator.rmgr.protocol.me.ME_SPsNodesDir;
import trust.simulator.rmgr.protocol.me.ME_UsersNodesDir;
import trust.simulator.rmgr.protocol.user.Credential;
import trust.simulator.rmgr.protocol.user.ServiceRequest;
import trust.simulator.rmgr.protocol.user.UserCycleRecord;
import trust.simulator.rmgr.protocol.user.User_Node;


public class SimulationEngine implements CDProtocol {

	// ------------------------------------------------------------------------
    // Parameters
    // ------------------------------------------------------------------------
	
	private static final String PAR_Experiment = "Experiment";
	
	private static final String PAR_RMGR_Node_PROT = "RMGR_Node";
    
	private static final String PAR_Super_Node_PROT = "Super_Node";
    
	private static final String PAR_IDP_Node_PROT = "IDP_Node";
    
    private static final String PAR_Auditor_Node_PROT = "Auditor_Node";
    
    private static final String PAR_User_Node_PROT = "User_Node";
    
    private static final String PAR_SP_Node_PROT = "SP_Node";
    
    private static final String PAR_ME_Node_PROT = "ME_Node";
    
    private static final String PAR_SPRank = "SPRank";
    
    private static final String PAR_UpdateIDPRankingsFreq = "UpdateIDPRankingsFreq";
    
    private static final String PAR_DeployST = "DeployST";
    
    private static final String PAR_DeployGT = "DeployGT";
    
    //private static final String PAR_OfferDGUProb = "OfferDGUProb"; 
    //private static final String PAR_OfferStrictDGUProb = "OfferStrictDGUProb";
    
    // ------------------------------------------------------------------------
    // Fields
    // ------------------------------------------------------------------------
    
    private static int Experiment;
    private static int RMGRPid;
    private static int SuperPid;
    private static int IDPPid;
    private static int AuditorPid;
    private static int UserPid;
    private static int SPPid;
    private static int MEPid;
    private static double SPRank;
    private static int UpdateIDPRankingsFreq;
    private static int DeployST;
    private static int DeployGT;
    private static double OfferDGUProb;
    private static double OfferStrictDGUProb;
    // --------------------------------------------------------------------------
    // Initialization
    // --------------------------------------------------------------------------

    /**
     * Standard constructor that reads the configuration parameters. Normally
     * invoked by the simulation engine.
     * 
     * @param prefix
     *            the configuration prefix for this class
     */
    public SimulationEngine(String prefix) {
        super();
        
        Experiment = Configuration.getInt(PAR_Experiment);
        RMGRPid = Configuration.getPid(prefix + "." + PAR_RMGR_Node_PROT);
        SuperPid = Configuration.getPid(prefix + "." + PAR_Super_Node_PROT);
        IDPPid = Configuration.getPid(prefix + "." + PAR_IDP_Node_PROT);
        AuditorPid = Configuration.getPid(prefix + "." + PAR_Auditor_Node_PROT);
        UserPid = Configuration.getPid(prefix + "." + PAR_User_Node_PROT);
        SPPid = Configuration.getPid(prefix + "." + PAR_SP_Node_PROT);
        MEPid = Configuration.getPid(prefix + "." + PAR_ME_Node_PROT);
        SPRank = Configuration.getDouble(prefix + "." + PAR_SPRank);
        UpdateIDPRankingsFreq = Configuration.getInt(prefix + "." + PAR_UpdateIDPRankingsFreq);
        DeployST = Configuration.getInt(prefix + "." + PAR_DeployST);
        DeployGT = Configuration.getInt(prefix + "." + PAR_DeployGT);
    }
    
    public Object clone() {
    	SimulationEngine inp = null;
        try {
            inp = (SimulationEngine) super.clone();
        } catch (CloneNotSupportedException e) {
        } // never happens
        return inp;
    }
    

    /**
     * 
     */
    public void nextCycle(Node node, int protocolID) {
    	
    	//System.out.println("ProtocolID = "+protocolID);
    	
    	//Linkable linkable = (Linkable) node.getProtocol( FastConfig.getLinkable(protocolID) );
    	
    	//System.out.println("Node: "+node.getID()+" has "+linkable.degree()+" Nighbours.");
    
    	//if (linkable.degree() > 0)
    	{
        	RMGR_Node currentNode;
        	
        	if( ((Super_Node) node.getProtocol(SuperPid)).getNodeType() == 0 )
        		currentNode = (Super_Node) node.getProtocol(SuperPid);
        	else if( ((IDP_Node) node.getProtocol(IDPPid)).getNodeType() == 1 )
        		currentNode = (IDP_Node) node.getProtocol(IDPPid);
        	else if( ((Auditor_Node) node.getProtocol(AuditorPid)).getNodeType() == 2 )
        		currentNode = (Auditor_Node) node.getProtocol(AuditorPid);
        	else if( ((User_Node) node.getProtocol(UserPid)).getNodeType() == 3 )
        		currentNode = (User_Node) node.getProtocol(UserPid);
        	else if( ((SP_Node) node.getProtocol(SPPid)).getNodeType() == 4 )
        		currentNode = (SP_Node) node.getProtocol(SPPid);
        	else if( ((ME_Node) node.getProtocol(MEPid)).getNodeType() == 5 )
        		currentNode = (ME_Node) node.getProtocol(MEPid);
        	else
        		currentNode = (RMGR_Node) node.getProtocol(RMGRPid);
        	
    		if (CommonState.getTime() == 0)
        	{	
    			//Any initialization goes here...
    			
    			if(currentNode.getNodeType() == 0)
    			{
    		        Super_Node superNode = (Super_Node) currentNode;
    		        
    		        OfferDGUProb = superNode.getOfferDGUProb();
    		        OfferStrictDGUProb = superNode.getOfferStrictDGUProb();
    			}
    			
    			if (currentNode.getNodeType() == 1) 
	        	{
        			//IDP_Node idpNode = (IDP_Node) currentNode;
        			//idpNode.setNodesDir(protocolID, UserPid, SPPid, SPRank);
	        	}
         	}
    		

    		else if(CommonState.getTime() > 0 && CommonState.getTime() < CommonState.getEndTime()-1)
    		{
    			
    			//System.out.println("currentNode.getNodeType() == "+currentNode.getNodeType());
				
    			if (currentNode.getNodeType() == 0)
    			{
    				Super_Node superNode = (Super_Node) currentNode;
    				
    				superNode.updateAvgUser();
    				
    				if(CommonState.getIntTime() == 125 || CommonState.getIntTime() == 250 || CommonState.getIntTime() == 375 || CommonState.getIntTime() == 500)
    				{
    					if(Experiment > 2)
    					{
        					superNode.updatePCT_Agents_to_Users(CommonState.getIntTime());
    					}
    				}
    			}
    			
    			else if (currentNode.getNodeType() == 1)
    			{
    				IDP_Node idp = (IDP_Node) currentNode;
    			
    				/*
    				if(CommonState.getIntTime() == 1)
    				{
	    				for(SPsNodesDir spNode: idp.getPspNodes())
	    					System.out.println("pspNode: "+spNode.getPosition());
	    				
	    				for(SPsNodesDir spNode: idp.getSpNodes())
	    					System.out.println("spNode: "+spNode.getPosition());
    				}
    				*/
    				if(CommonState.getIntTime() == 125 || CommonState.getIntTime() == 250 || CommonState.getIntTime() == 375 || CommonState.getIntTime() == 500)
    				{
    					idp.updateMSPsSPAMsCounters(CommonState.getIntTime());
    					if(Experiment > 1)
    					{
    						idp.updateUndetectedMSPs(CommonState.getIntTime());
    						idp.updateAvgToBan(CommonState.getIntTime());
    						idp.updateInnocentBanned(CommonState.getIntTime());
    					}
    					if(Experiment > 9)
    					{
    						idp.updateDGUNodes(CommonState.getIntTime());
    					}
    				}
    			}
    			else if (currentNode.getNodeType() == 2) 
	        	{
    			
    				Auditor_Node auditor = (Auditor_Node) currentNode;
    				auditor.runGPD_Evaluater();
    				auditor.processCases();
    				auditor.updateTGR();
    				auditor.updateBannedSPsList();
    				auditor.updateSPR();
    				auditor.updatePIILs();
    				
    				if(DeployST == 1)
    					auditor.updateTST();
    				
    				if(DeployGT == 1)
    				{
    					auditor.updatePColludingRecords();
    					auditor.updatePWeakColludingRecords();
    					auditor.updateTGT();
    				}
    				
    				if( CommonState.getTime() % UpdateIDPRankingsFreq == 0)
    				{
    					//System.out.println("Hello");
    					IDP_Node idpNode = (IDP_Node) Network.get(1).getProtocol(IDPPid);
    					idpNode.updateRankings(auditor.getTGR());
    				}
    				
    				if(CommonState.getIntTime() == 125 || CommonState.getIntTime() == 250 || CommonState.getIntTime() == 375 || CommonState.getIntTime() == 500)
    				{
    					if(Experiment > 1)
    					{
    						if(Experiment < 13)
    							auditor.updateAvgTimeToResolveCase(CommonState.getIntTime(), false);
    						else
    							auditor.updateAvgTimeToResolveCase(CommonState.getIntTime(), true);
    					}
    					if(Experiment > 2)
    					{
    						auditor.updateDetectorList(CommonState.getIntTime());
    					}
    				}
    				/*
    				//:: Debug starts
    				System.out.println("");
    				System.out.println("----Auditor Current TLRu Contents----");
    				Auditor_UsersNodesDir uNode;
    				//System.out.println("Auditor NodePos: "+auditor.nodePos);
    				//System.out.println("Auditor uNodes.size: "+auditor.uNodes.size());
    				for(int i = 0; i < auditor.getuNodes().size(); i++)
    				{
    					uNode = auditor.getuNodes().get(i);
    					System.out.println("");
    					System.out.println("User "+uNode.getPosition()+" got the following TLRu Values: ");
    					TLRuRank rank;
    					for(int j = 0; j < uNode.getTLRu().size(); j++)
    					{
    						rank = uNode.getTLRu().get(j);
    						System.out.println("");
    						System.out.println("--SP ID: "+rank.getSPid()+" with Rank: "+rank.getRank());
    						System.out.println("----Cases Count: "+rank.getCasesCount()+" Guilt Rate Sum: "+rank.getGuiltRateSum());
    					}
    				}
    				*/
    				/*
    				//Debug Finish ::
    				
    				//:: Debug starts
    				System.out.println("");
    				System.out.println("----Auditor Current TLRavg Contents----");
					System.out.println("");
    				TLRavgRank tlravgRank;
    				for(int i = 0; i < auditor.getTLRavg().size(); i++)
    				{
    					tlravgRank = auditor.getTLRavg().get(i);
    					System.out.println("SP "+tlravgRank.getSPid()+" got TLRavg Rank of: "+tlravgRank.getRank());
    				}
    				//Debug Finish ::
    				*/
    				
	        	}
    			else if (currentNode.getNodeType() == 3) 
	        	{
    				User_Node userNode = (User_Node) currentNode;
    				
    				if(userNode.getSTstatus() != 3 && userNode.getGTstatus() != 3)
    				{
						userNode.updateTrustInNetwork();
						
						if(CommonState.getTime() < CommonState.getEndTime()-2)
						{
		    				userNode.createNewCredential(true,false, false, -1);    				
		    				ServiceRequest serviceRequest = userNode.generateNewService();
		    				if(serviceRequest.isGetService())
		    				{
		    					IDP_Node idpNode = (IDP_Node) Network.get(1).getProtocol(IDPPid);
		    					//System.out.println("2 idpNode.getSpNodesDirIndex() = "+idpNode.getSpNodesDirIndex());
		    					//System.out.println("serviceRequest: rank = "+serviceRequest.getMinRank()+" usefulness = "+serviceRequest.getMinUsefulness()+" credential: ID = "+serviceRequest.getCredential().getId()+" nodePos: "+serviceRequest.getCredential().getNodePosition());
		    					idpNode.getService(serviceRequest);
		    					//System.out.println("requestDone: "+requestDone);
		    				}
						}
    				}
    				
	        	}
    			else if (currentNode.getNodeType() == 4) 
	        	{
    				SP_Node spNode = (SP_Node) currentNode;
    				
    				if(!spNode.isBanned())
    				{
						if(!spNode.isMalicious())
						{
							Auditor_Node auditor = (Auditor_Node) Network.get(2).getProtocol(AuditorPid);
    					
	    					if(!spNode.isInstalledDGU())
	    						if(spNode.randBoolean(1-OfferDGUProb/100))
	    							if(spNode.acceptDGUoffer(true))
	    								auditor.updateSPnodeStatus(spNode.getNodePos(), 1, 0);
	    					
							if(!spNode.isEnabledStrictDGU())
	    						if(spNode.randBoolean(1-OfferStrictDGUProb/100))
	    							if(spNode.acceptStrictDGUoffer(true))
	    								auditor.updateSPnodeStatus(spNode.getNodePos(), 2, 0);
						}
    					spNode.updatePartnerships();
    				}
    				if(spNode.isMalicious() && CommonState.getTime() < CommonState.getEndTime()-2)
    				{
    					//System.out.println("");
    					//System.out.println("This is MSP: "+spNode.getNodePos());
    					//System.out.println("The list of Credentials to SPAM is:");
    					
    					List<Credential> credentialsToSPAM = spNode.runAttacks();
    					IDP_Node idpNode = (IDP_Node) Network.get(1).getProtocol(IDPPid);
    					
    					if(!credentialsToSPAM.isEmpty())
    					for(Credential credential: credentialsToSPAM)
    					{
    					//	System.out.println("--User Pos: "+credentialsToSPAM.get(i).getNodePosition()+" CredentialID: "+credentialsToSPAM.get(i).getId());
    						idpNode.sendSpam(credential);
    					}

    				}
	        	}
    			else if (currentNode.getNodeType() == 5) 
	        	{
    				ME_Node meNode = (ME_Node) currentNode;
    				
    				meNode.grow();
    				meNode.processCredentials();
	        	}
    		}
    		
    		if(CommonState.getTime() == CommonState.getEndTime()-1)
    		{
    			if (currentNode.getNodeType() == 0)
    			{
    				Super_Node superNode = (Super_Node) currentNode;
    				/*
    				System.out.println("");
    				System.out.println("Final Users Nodes: "+superNode.getUsersCount());
    				System.out.println("Final PSPs Nodes: "+superNode.getPSPsCount());
    				System.out.println("Final SPs Nodes: "+superNode.getSPsCount());
    				System.out.println("Final MPSPs Nodes: "+superNode.getMPSPsCount());
    				System.out.println("Final MSPs Nodes: "+superNode.getMSPsCount());
    				System.out.println("Final MEs Nodes: "+superNode.getMECount());
    				
    				System.out.println("");
    				System.out.println("----avgUser Trust Contents----");
    				System.out.println("");
    				System.out.println("avgUser final Trust: "+superNode.getAvgUser().getCurrentTrust());
    				for(UserCycleRecord UCR: superNode.getAvgUser().getUCRs())
					{
						System.out.println("-- Cycle: "+UCR.getCycle()+" trust: "+UCR.getCurrentTrust());
					}
    				
    				System.out.println("");
    				System.out.println("----avgUser Credntials Contents----");
    				System.out.println("");
    				System.out.println("avgUser final Credentials Stat: ");
					System.out.println("  CredentialsTotal: "+superNode.getCredentialsTotal()+" CompromisedCredentialsTotal: "+superNode.getCompromisedCredentialsTotal()+" StrictCredentialsTotal: "+superNode.getStrictCredentialsTotal()+" CompromisedStrictCredentialsTotal: "+superNode.getCompromisedStrictCredentialsTotal()+" ShareWithTopCredentialsTotal: "+superNode.getShareWithTopCredentialsTotal()+" CompromisedShareWithTopCredentialsTotal: "+superNode.getCompromisedShareWithTopCredentialsTotal()+" ShareWithAnyCredentialsTotal: "+superNode.getShareWithAnyCredentialsTotal()+" CompromisedShareWithAnyCredentialsTotal: "+superNode.getCompromisedShareWithAnyCredentialsTotal());
					System.out.println("    StrictCredentialsTotal% = "+superNode.getStrictCredentialsTotal()*100/superNode.getCredentialsTotal()+" ShareWithTopCredentialsTotal% = "+superNode.getShareWithTopCredentialsTotal()*100/superNode.getCredentialsTotal()+" ShareWithAnyCredentialsTotal% = "+superNode.getShareWithAnyCredentialsTotal()*100/superNode.getCredentialsTotal());
					System.out.println("    CompromisedCredentialsTotal% = "+superNode.getCompromisedCredentialsTotal()*100/superNode.getCredentialsTotal()+" CompromisedStrictCredentialsTotal% = "+superNode.getCompromisedStrictCredentialsTotal()*100/superNode.getStrictCredentialsTotal()+" CompromisedShareWithTopCredentialsTotal% = "+superNode.getCompromisedShareWithTopCredentialsTotal()*100/superNode.getShareWithTopCredentialsTotal()+" ShareWithAnyCredentialsTotal% = "+superNode.getCompromisedShareWithAnyCredentialsTotal()*100/superNode.getShareWithAnyCredentialsTotal());
					System.out.println("    IgnoredCompromisedCredentialsTotal: "+superNode.getIgnoredCompromisedCredentialsTotal()+" IgnoredCompromisedStrictCredentialsTotal: "+superNode.getIgnoredCompromisedStrictCredentialsTotal()+" IgnoredCompromisedShareWithTopCredentialsTotal: "+superNode.getIgnoredCompromisedShareWithTopCredentialsTotal()+" IgnoredCompromisedShareWithAnyCredentialsTotal: "+superNode.getIgnoredCompromisedShareWithAnyCredentialsTotal());
					System.out.println("    ignoredCompromisedCredentialsTotal% = "+superNode.getIgnoredCompromisedCredentialsTotal()*100/superNode.getCredentialsTotal()+" ignoredCompromisedStrictCredentialsTotal% = "+superNode.getIgnoredCompromisedStrictCredentialsTotal()*100/superNode.getStrictCredentialsTotal()+" ignoredCompromisedShareWithTopCredentialsTotal% = "+superNode.getIgnoredCompromisedShareWithTopCredentialsTotal()*100/superNode.getShareWithTopCredentialsTotal()+" ignoredShareWithAnyCredentialsTotal% = "+superNode.getIgnoredCompromisedShareWithAnyCredentialsTotal()*100/superNode.getShareWithAnyCredentialsTotal());
					System.out.println("    realCompromisedCredentialsTotal% = "+(superNode.getIgnoredCompromisedCredentialsTotal() + superNode.getCompromisedCredentialsTotal())*100/superNode.getCredentialsTotal()+" realCompromisedStrictCredentialsTotal% = "+(superNode.getIgnoredCompromisedStrictCredentialsTotal() + superNode.getCompromisedStrictCredentialsTotal())*100/superNode.getStrictCredentialsTotal()+" realCompromisedShareWithTopCredentialsTotal% = "+(superNode.getIgnoredCompromisedShareWithTopCredentialsTotal() + superNode.getCompromisedShareWithTopCredentialsTotal())*100/superNode.getShareWithTopCredentialsTotal()+" realShareWithAnyCredentialsTotal% = "+(superNode.getIgnoredCompromisedShareWithAnyCredentialsTotal() + superNode.getCompromisedShareWithAnyCredentialsTotal())*100/superNode.getShareWithAnyCredentialsTotal());
					System.out.println("");
					
    				System.out.println("");
    				System.out.println("----avgAgentUser Trust Contents----");
    				System.out.println("");
    				System.out.println("avgAgentUser final Trust: "+superNode.getAvgAgentUser().getCurrentTrust());
    				for(UserCycleRecord UCR: superNode.getAvgAgentUser().getUCRs())
					{
						System.out.println("-- Cycle: "+UCR.getCycle()+" trust: "+UCR.getCurrentTrust());
					}
    				
    				System.out.println("");
    				System.out.println("----avgAgentUser Credntials Contents----");
    				System.out.println("");
    				System.out.println("avgAgentUser final Credentials Stat: ");
					System.out.println("  CredentialsTotal: "+superNode.getAgentCredentialsTotal()+" CompromisedCredentialsTotal: "+superNode.getAgentCompromisedCredentialsTotal()+" StrictCredentialsTotal: "+superNode.getAgentStrictCredentialsTotal()+" CompromisedStrictCredentialsTotal: "+superNode.getAgentCompromisedStrictCredentialsTotal()+" ShareWithTopCredentialsTotal: "+superNode.getAgentShareWithTopCredentialsTotal()+" CompromisedShareWithTopCredentialsTotal: "+superNode.getAgentCompromisedShareWithTopCredentialsTotal()+" ShareWithAnyCredentialsTotal: "+superNode.getAgentShareWithAnyCredentialsTotal()+" CompromisedShareWithAnyCredentialsTotal: "+superNode.getAgentCompromisedShareWithAnyCredentialsTotal());
					System.out.println("    StrictCredentialsTotal% = "+superNode.getAgentStrictCredentialsTotal()*100/superNode.getAgentCredentialsTotal()+" ShareWithTopCredentialsTotal% = "+superNode.getAgentShareWithTopCredentialsTotal()*100/superNode.getAgentCredentialsTotal()+" ShareWithAnyCredentialsTotal% = "+superNode.getAgentShareWithAnyCredentialsTotal()*100/superNode.getAgentCredentialsTotal());
					System.out.println("    CompromisedCredentialsTotal% = "+superNode.getAgentCompromisedCredentialsTotal()*100/superNode.getAgentCredentialsTotal()+" CompromisedStrictCredentialsTotal% = "+superNode.getAgentCompromisedStrictCredentialsTotal()*100/superNode.getAgentStrictCredentialsTotal()+" CompromisedShareWithTopCredentialsTotal% = "+superNode.getAgentCompromisedShareWithTopCredentialsTotal()*100/superNode.getAgentShareWithTopCredentialsTotal()+" ShareWithAnyCredentialsTotal% = "+superNode.getAgentCompromisedShareWithAnyCredentialsTotal()*100/superNode.getAgentShareWithAnyCredentialsTotal());
					System.out.println("    StrictCredentialsTotal% = "+superNode.getAgentStrictCredentialsTotal()*100/superNode.getAgentCredentialsTotal()+" ShareWithTopCredentialsTotal% = "+superNode.getAgentShareWithTopCredentialsTotal()*100/superNode.getAgentCredentialsTotal()+" ShareWithAnyCredentialsTotal% = "+superNode.getAgentShareWithAnyCredentialsTotal()*100/superNode.getAgentCredentialsTotal());
					System.out.println("");
					
					*/
    				
					/*
					for(UserCycleRecord UCR: superNode.getAvgUser().getUCRs())
					{
						System.out.println("-- Cycle: "+UCR.getCycle()+" CredentialsTotal: "+UCR.getCredentialsTotal()+" CompromisedCredentialsTotal: "+UCR.getCompromisedCredentialsTotal()+" StrictCredentialsTotal: "+UCR.getStrictCredentialsTotal()+" CompromisedStrictCredentialsTotal: "+UCR.getCompromisedStrictCredentialsTotal()+" ShareWithTopCredentialsTotal: "+UCR.getShareWithTopCredentialsTotal()+" CompromisedShareWithTopCredentialsTotal: "+UCR.getCompromisedShareWithTopCredentialsTotal()+" ShareWithAnyCredentialsTotal: "+UCR.getShareWithAnyCredentialsTotal()+" CompromisedShareWithAnyCredentialsTotal: "+UCR.getCompromisedShareWithAnyCredentialsTotal());
					}
					*/
    				
    				/*
    				System.out.println("");
    				System.out.println("----Super uNodes Trust Content----");
    				for(Super_UsersNodesDir uNode: superNode.getuNodes())
    				{
    					System.out.println("");
    					System.out.println("uNode: "+uNode.getPosition()+" type: "+uNode.getType()+" FinalTrust: "+uNode.getCurrentTrust()+" History: ");
    					for(UserTrustRecord UCR: uNode.getUCRs())
    					{
    						System.out.println("-- Cycle: "+UCR.getCycle()+" trust: "+UCR.getCurrentTrust());
    					}
    				}
    				System.out.println("");
    				*/
    			}
    			
    			if (currentNode.getNodeType() == 1)
    			{
    				IDP_Node idpNode = (IDP_Node) currentNode;
    				
    				System.out.println("innocent PSP banned: "+idpNode.getBannedInnocentPSPCount()+" innocent SP banned: "+idpNode.getBannedInnocentSPCount());
    				
    				
    				/*List<SPsNodesDir> spNodes = idpNode.getSpNodes(); 
    				int spNodesDirIndex = spNodes.size();

    				System.out.println("");
    				System.out.println("----spNodes Content----");
    				System.out.println("");
    				
    				for(int i = 0; i < spNodesDirIndex; i++)
    				{
    					SPsNodesDir spNode = spNodes.get(i);
    					System.out.println("");
    					System.out.println("spNode.get("+i+"): "+spNode.getPosition()+" has Rank = "+spNode.getRank()+" and Usefulness = "+spNode.getUsefulness());
    					if(spNode.isMalicious())
    					{
    						System.out.println("This is an MSP who obtained the following credentials:");
    						
    						SP_Node msp = (SP_Node) Network.get(spNode.getPosition()).getProtocol(SPPid);
    						for(int j = 0; j < msp.uNodes.size(); j++)
    						{
    							System.out.println("");
    							System.out.println("--User ID: "+msp.uNodes.get(j).getPosition());
    							for(int z = 0; z < msp.uNodes.get(j).Credentials.size(); z++)
    							{
    								System.out.println("--CredentialID: "+msp.uNodes.get(j).Credentials.get(z).getId()+" Compromised? "+msp.uNodes.get(j).Credentials.get(z).isCompromised());
    							}
    						}
    					}
    				}
    				
    				List<SPsNodesDir> pspNodes = idpNode.getPspNodes(); 
    				int pspNodesDirIndex = pspNodes.size();

    				System.out.println("");
    				System.out.println("----pspNodes Content----");
    				System.out.println("");
    				
    				for(int i = 0; i < pspNodesDirIndex; i++)
    				{
    					SPsNodesDir pspNode = pspNodes.get(i);
    					
    					System.out.println("");
    					System.out.println("pspNode.get("+i+"): "+pspNode.getPosition()+" has Rank = "+pspNode.getRank()+" and Usefulness = "+pspNode.getUsefulness());
    					
    					if(pspNode.isMalicious())
    					{
    						System.out.println("This is an MSP who obtained the following credentials:");
    						
    						SP_Node msp = (SP_Node) Network.get(pspNode.getPosition()).getProtocol(SPPid);
    						for(int j = 0; j < msp.uNodes.size(); j++)
    						{
    							System.out.println("");
    							System.out.println("--User ID: "+msp.uNodes.get(j).getPosition());
    							for(int z = 0; z < msp.uNodes.get(j).Credentials.size(); z++)
    							{
    								System.out.println("--CredentialID: "+msp.uNodes.get(j).Credentials.get(z).getId()+" Compromised? "+msp.uNodes.get(j).Credentials.get(z).isCompromised());
    							}
    						}
    					}
    				}*/

    				/*
    				List<UsersNodesDir> uNodes = idpNode.getUNodesDir(); 
    				int uNodesDirIndex = uNodes.size();

    				System.out.println("");
    				System.out.println("----uNodes Content----");
    				
    				for(int i = 0; i < uNodesDirIndex; i++)
    				{
    					UsersNodesDir uNode = uNodes.get(i);
    					
    					if(uNode.getType() == 1)
    					{
    					
							User_Node userNode = (User_Node) Network.get(uNode.getPosition()).getProtocol(UserPid);
							int credentialsSum = uNode.getCredentialLogs().size();
							System.out.println("");
							System.out.println("uNodes.get("+i+"): "+uNode.getPosition()+" has a total of "+userNode.getReportedCases()+" reported cases, TrustInNetwork = "+userNode.getTrustInNetwork()+" and the sum of "+credentialsSum+" credentials as following:");
							for(int j = 0; j < credentialsSum; j++)
							{
								CredentialLogs credentialLogs = uNode.CredentialLogs.get(j);
								Credential credential = credentialLogs.getCredential();
								System.out.println("");
								System.out.println("--Credential ID: "+credential.getId()+" with the following Log: ");
								
								List<Log> logs = credentialLogs.getLogs();
								for(int z = 0; z < credentialLogs.logs.size(); z++)
								{
									Log log = logs.get(z);
									System.out.println("----Log: "+z+" spPosition: "+log.getSpPosition()+" Cycle:"+log.getCycle());
								}
							}

    					}
    				}
    				*/
    		
    			}
    			if (currentNode.getNodeType() == 2) 
	        	{
    				Auditor_Node auditor = (Auditor_Node) currentNode;
    				/*
    				//:: Debug starts
    				System.out.println("");
    				System.out.println("----Auditor Current TGR Contents----");
					System.out.println("");
    				TGRRank tgrRank;
    				for(int i = 0; i < auditor.getTGR().size(); i++)
    				{
    					tgrRank = auditor.getTGR().get(i);
    					
    					boolean psp = false;
    					
    					if(tgrRank.getSpUsefulness() >= auditor.usefulnessMIN)
    						psp = true;
    					if(psp)
    						System.out.println("PSP "+tgrRank.getSPid()+" got TGR Rank of: "+tgrRank.getRank()+" TGTsp: "+tgrRank.getTGTRank()+" TGTcolluding: "+tgrRank.getTGTColludingRank()+" TGTWeakColluding: "+tgrRank.getTGTWeakColludingRank()+" TST: "+tgrRank.getTSTRank()+" TLRavg: "+tgrRank.getTLRavgRank());
    					else
    						System.out.println("SP "+tgrRank.getSPid()+" got TGR Rank of: "+tgrRank.getRank()+" TGTsp: "+tgrRank.getTGTRank()+" TGTcolluding: "+tgrRank.getTGTColludingRank()+" TGTWeakColluding: "+tgrRank.getTGTWeakColludingRank()+" TST: "+tgrRank.getTSTRank()+" TLRavg: "+tgrRank.getTLRavgRank());
    				}
    				//Debug Finish ::
    				*/
    				/*
    				//:: Debug starts
    				System.out.println("");
    				System.out.println("----Auditor Current TGTsp Contents----");
					System.out.println("");
    				TGRRank tgrRank2;
    				for(int i = 0; i < auditor.getTGR().size(); i++)
    				{
    					tgrRank2 = auditor.getTGR().get(i);
    					System.out.println("SP "+tgrRank2.getSPid()+" got TGTsp Rank of: "+tgrRank2.getTGTRank()+" and TGTcolluding: "+tgrRank2.getTGTColludingRank());
    				}
    				//Debug Finish ::
    				
    				//:: Debug starts
    				System.out.println("");
    				System.out.println("----Auditor Current TLRavg Contents----");
					System.out.println("");
    				TLRavgRank tlravgRank;
    				for(int i = 0; i < auditor.getTLRavg().size(); i++)
    				{
    					tlravgRank = auditor.getTLRavg().get(i);
    					System.out.println("SP "+tlravgRank.getSPid()+" got TLRavg Rank of: "+tlravgRank.getRank());
    				}
    				//Debug Finish ::
    				*/ 
    				/*
    				//:: Debug starts
    				System.out.println("");
    				System.out.println("----Auditor Current TGTg Contents----");
					System.out.println("");
    				TGTgRank tgtgRank;
    				for(int i = 0; i < auditor.getTGTg().size(); i++)
    				{
    					tgtgRank = auditor.getTGTg().get(i);
    					System.out.println("A rank of: "+tgtgRank.getTGTgRank()+" isIgnore? "+tgtgRank.isIgnore()+" Counter: "+tgtgRank.getCounter()+" is for the G of SPs: "+tgtgRank.getSPs().toString());
    				}
    				//Debug Finish ::
    				
    				//:: Debug starts
    				System.out.println("");
    				System.out.println("----Auditor Current suspiciousSPNodes Contents----");
					System.out.println("");
    				Auditor_SPsNodesDir suspiciousSP;
    				for(int i = 0; i < auditor.getSuspiciousSPNodes().size(); i++)
    				{
    					suspiciousSP = auditor.getSuspiciousSPNodes().get(i);
    					System.out.println("SP "+suspiciousSP.getPosition()+" got TGR Rank of: "+suspiciousSP.getRank());
    				}
    				//Debug Finish ::
    				
    				//:: Debug starts
    				System.out.println("");
    				System.out.println("----Auditor Current BannedSPNodes Contents----");
					System.out.println("");
    				Auditor_SPsNodesDir bannedSP;
    				for(int i = 0; i < auditor.getBannedSPNodes().size(); i++)
    				{
    					bannedSP = auditor.getBannedSPNodes().get(i);
    					System.out.println("SP "+bannedSP.getPosition()+" got TGR Rank of: "+bannedSP.getRank()+" ban Detector: "+bannedSP.getBanDetectorType()+" installedDGU? "+bannedSP.isInstalledDGU()+" DGU detector: "+bannedSP.getInstallDGUdetectorType()+" enabledStrictDGU? "+bannedSP.isEnabledStrictDGU()+" enabledStrictDGU detector: "+bannedSP.getEnableStrictDGUdetectorType());
    				}
    				System.out.println("");
    				//Debug Finish ::
    				
    				//:: Debug starts
    				System.out.println("");
    				System.out.println("----Auditor Current DGUSPNodes Contents----");
					System.out.println("");
					IDP_Node idp = (IDP_Node) Network.get(1).getProtocol(IDPPid);
					double dguspSize = auditor.getDGUSPNodes().size();
					double allSps = idp.getSpNodes().size()+idp.getPspNodes().size();
					double dguspsPCT = (dguspSize/allSps) * 100;
					System.out.println("DGUSPsNodes size: "+dguspSize+" All SPs Count = "+allSps+" DGUSPs% = "+dguspsPCT);
					
    				for(Auditor_SPsNodesDir DGUSP: auditor.getDGUSPNodes())
    					System.out.println("SP "+DGUSP.getPosition()+" got TGR Rank of: "+DGUSP.getRank()+" Banned? "+DGUSP.isBanned()+" Ban Detector: "+DGUSP.getBanDetectorType()+" installedDGU? "+DGUSP.isInstalledDGU()+" installedDGU Detector: "+DGUSP.getInstallDGUdetectorType()+" installedDGU Cycle: "+DGUSP.getInstalledDGUCycle()+" enabledStrictDGU? "+DGUSP.isEnabledStrictDGU()+" enabledStrictDGU detector: "+DGUSP.getEnableStrictDGUdetectorType()+" enabledSTrictDGU cycle: "+DGUSP.getEnabledStrictDGUCycle());
    				
    				System.out.println("");
    				//Debug Finish ::
    				*/
    				/*
    				//:: Debug starts
    				System.out.println("");
    				System.out.println("----Auditor Current PColludingRecords Contents----");
					System.out.println("");
    				for(PColludingRecord pcolludingRecord: auditor.getPColludingRecords())
    				{
    					System.out.println("A counter of: "+pcolludingRecord.getCounter()+" Ignore? "+pcolludingRecord.isIgnore()+" For G: ");
    					for(Integer sp: pcolludingRecord.getSPs()) System.out.println("--SP: "+sp);
    				}
    				System.out.println("");
    				System.out.println("----Auditor Current PWeakColludingRecords Contents----");
					System.out.println("");
    				for(PColludingRecord pcolludingRecord: auditor.getPWeakColludingRecords())
    				{
    					System.out.println("A counter of: "+pcolludingRecord.getCounter()+" Ignore? "+pcolludingRecord.isIgnore()+" For G: ");
    					for(Integer sp: pcolludingRecord.getSPs()) System.out.println("--SP: "+sp);
    				}
    				System.out.println("");
    				
    				System.out.println("The total Number of STs is: "+auditor.getStNodes().size());
    				
    				System.out.println("");
    				
    				System.out.println("The total Number of GTs is: "+auditor.getGtNodes().size());

       				System.out.println("");
       				
    				//Debug Finish ::
    				*/
    				
    				/*
    				//:: Debug starts
    				System.out.println("");
    				System.out.println("----Auditor Current SPR Contents----");
					System.out.println("");
    				for(SPRrecord sprRecord: auditor.getSPR())
    				{
    					System.out.println("-- SPid "+sprRecord.getSPid()+" Count: "+sprRecord.getCounter()+" PSL? "+sprRecord.isPSL()+" stTested? "+sprRecord.isSTtested());
    				}
    				//Debug Finish ::
    				*/
    				
    				//Debug Starts
    				/*
    				System.out.println("");
    				System.out.println("----Auditor Reported Cases Content----");
    				Auditor_UsersNodesDir uNode;
    				//System.out.println("Auditor NodePos: "+auditor.nodePos);
    				//System.out.println("Auditor uNodes.size: "+auditor.uNodes.size());
    				for(int i = 0; i < auditor.getuNodes().size(); i++)
    				{
    					uNode = auditor.getuNodes().get(i);
    					System.out.println("");
    					System.out.println("User "+uNode.getPosition()+" has a total of: "+uNode.getCaseLogs().size()+" reported Cases as following: ");
    					CaseLogs caseLogs;
    					for(int j = 0; j < uNode.getCaseLogs().size(); j++)
    					{
    						caseLogs = uNode.getCaseLogs().get(j);
    						//if(!caseLogs.isClosed())
    						{
    							//System.out.println("printing prior history!");
    							
    							//for(int k = 0; k <= j; k++)
    							{
    								//caseLogs = uNode.getCaseLogs().get(k);
    								System.out.println("");
    								System.out.println("");
    								System.out.println("::Case Closed?::"+caseLogs.isClosed());
		    						System.out.println("::Case isGPD_Evaluated?::"+caseLogs.isGPD_Evaluated());
		    						System.out.println("--Credential ID: "+caseLogs.getCredential().getId());
		    						System.out.println("----caseLogs.getLogs().size() = "+caseLogs.getLogs().size());
		    						Log log;
		    						for(int z = 0; z < caseLogs.getLogs().size(); z++)
		    						{
		    							log = caseLogs.getLogs().get(z);
		    							System.out.println("----Log: "+z+" spPosition: "+log.getSpPosition()+" Cycle: "+log.getCycle());
		    						}
    							}
    							
    							//break;
    						}
    					}
    				}
    				//Debug Finish
    				*/
    				
    				/*
    				System.out.println("");
    				System.out.println("--Contents of Auditor stNodes--");
    				System.out.println("");
    				for(Auditor_STNodesDir stNode: auditor.getStNodes())
    				{
    					System.out.println("---Node Pos: "+stNode.getNodePos()+" SPid: "+stNode.getSPID()+" Status: "+stNode.getSTstatus()+" CreatedOn: "+stNode.getCreatedOn());
    				}
    				System.out.println("");
    				*/
    			
    				/*
    				System.out.println("");
    				System.out.println("--Contents of Auditor IIR--");
    				System.out.println("");
    				for(IILrecord iil: auditor.getIIR())
    				{
    					System.out.println("---UserID: "+iil.getUserID()+" Credential ID: "+iil.getCredentialID()+" CreatedOn: "+iil.getCreatedON()+" PIIL Evaluated? "+iil.isPIILevaluated()+" stTested? "+iil.isStTested()+" SPs: ");
    					for(Integer sp: iil.getSPs()) System.out.println(sp);
    				}
    				System.out.println("");
    				*/
    				
    				/*
    				System.out.println("");
    				System.out.println("--Contents of Auditor PIILs--");
    				System.out.println("");
    				for(PIILrecord piil: auditor.getPIILs())
    				{
    					System.out.println("---piilCounter: "+piil.getCounter()+" PIILS? "+piil.isPIILs()+" SPs: ");
    					for(Integer sp: piil.getSPs()) System.out.println(sp);
    				}
    				System.out.println("");
    				*/
	        	}
    			else if (currentNode.getNodeType() == 3) 
	        	{
    				
	        	}
    			else if (currentNode.getNodeType() == 4) 
	        	{
    				SP_Node sp = (SP_Node) currentNode;
    				
    				boolean PSP = false;
    				if(sp.getUsefulness() >= sp.getUsefulnessMIN())
    					PSP = true;
    				/*
    				if(sp.isMalicious() && !sp.isAssociatedWithME())
    				{
    					System.out.println("The spamsCount for SPid: "+sp.getNodePos()+" is: "+sp.getSpamsCount()+" PSP? "+PSP+" Banned? "+sp.isBanned()+" Ban Detector: "+sp.getBanDetectorType()+" CreationCycle: "+sp.getCreationCycle()+" BannedCycle: "+sp.getBannedCycle()+" BanningTime: "+(sp.getBannedCycle()-sp.getCreationCycle()));
    					System.out.println("installedDGU? "+sp.isInstalledDGU()+" installedDGU detector: "+sp.getInstallDGUdetectorType()+" installedDGU cycle: "+sp.getInstalledDGUCycle()+" enabledStrictDGU? "+sp.isEnabledStrictDGU()+" enabledStrictDGU detector: "+sp.getEnableStrictDGUdetectorType()+" enabledSTrictDGU Cycle: "+sp.getEnabledStrictDGUCycle());
    					System.out.println("");
    				}
    				else
    				*/ 
    				if(!sp.isMalicious() && sp.isBanned())
    				{
    					System.out.println("Innocent SPid: "+sp.getNodePos()+" is Banned. PSP? "+PSP+" CreationCycle: "+sp.getCreationCycle()+" BannedCycle: "+sp.getBannedCycle()+" Ban Detector: "+sp.getBanDetectorType()+" BanningTime: "+(sp.getBannedCycle()-sp.getCreationCycle()));
    					System.out.println("installedDGU? "+sp.isInstalledDGU()+" installedDGU detector: "+sp.getInstallDGUdetectorType()+" installedDGU cycle: "+sp.getInstalledDGUCycle()+" enabledStrictDGU? "+sp.isEnabledStrictDGU()+" enabledStrictDGU detector: "+sp.getEnableStrictDGUdetectorType()+" enabledSTrictDGU Cycle: "+sp.getEnabledStrictDGUCycle());
    					System.out.println("");
    				}
    				//*/
    				/*
    				if(PSP)
    				{
    					System.out.println("");
    					System.out.println("The partnerships list of PSPid: "+sp.getNodePos()+" is as following: ");
	    				int i = 1;
    					for(Partner partner: sp.getPartners())
	    				{
	    					System.out.println("--Partnership: "+i+" partnerID: "+partner.getPartnerID()+" creationCycle: "+partner.getPartnershipCreationCylce()+" duration: "+partner.getPartnershipDuration()+" sharingProb "+partner.getSharingProb()+" active? "+partner.isActive()+" colluding? "+partner.isColluding()+" malicious? "+partner.isMalicious()+" PSP? "+partner.isPsp()+ " SharedCount: "+partner.getSharedCount());
	    					i++;
	    				}
    				}
    				*/
	        	}
    			else if (currentNode.getNodeType() == 5) 
	        	{

    				ME_Node me = (ME_Node) currentNode;
    				/*
    				System.out.println("");
    				System.out.println("MEid: "+me.getNodePos()+" isUnpopularSPsColluding? "+me.isUnpopularSPsColluding()+" and has The following: ");
    				int meTotalSpam = 0;
    				for(ME_SPsNodesDir mpsp: me.getMpspNodes())
    				{
    					SP_Node sp = (SP_Node) Network.get(mpsp.getPosition()).getProtocol(SPPid);
    					System.out.println("");
    					System.out.println("--MPSP: "+mpsp.getPosition()+" banned? "+mpsp.isBanned()+" Ban Detector: "+sp.getBanDetectorType()+" CreationCycle: "+mpsp.getCreationCycle()+" BannedCycle: "+mpsp.getBannedCycle()+" spamsCount: "+sp.getSpamsCount()+" BanningTime: "+(mpsp.getBannedCycle()-mpsp.getCreationCycle()));
    					System.out.println("installedDGU? "+sp.isInstalledDGU()+" installedDGU detector: "+sp.getInstallDGUdetectorType()+" installedDGU cycle: "+sp.getInstalledDGUCycle()+" enabledStrictDGU? "+sp.isEnabledStrictDGU()+" enabledStrictDGU detector: "+sp.getEnableStrictDGUdetectorType()+" enabledSTrictDGU Cycle: "+sp.getEnabledStrictDGUCycle());
    					
    					System.out.println("");
    					System.out.println("The partnerships list of MPSPid: "+sp.getNodePos()+" is as following: ");
	    				int i = 1;
    					for(Partner partner: sp.getPartners())
	    				{
	    					System.out.println("--Partnership: "+i+" partnerID: "+partner.getPartnerID()+" creationCycle: "+partner.getPartnershipCreationCylce()+" duration: "+partner.getPartnershipDuration()+" sharingProb "+partner.getSharingProb()+" active? "+partner.isActive()+" colluding? "+partner.isColluding()+" malicious? "+partner.isMalicious()+" PSP? "+partner.isPsp()+ " SharedCount: "+partner.getSharedCount());
	    					i++;
	    				}
    					
    					meTotalSpam = meTotalSpam + sp.getSpamsCount();
    				}
    				System.out.println("");
    				for(ME_SPsNodesDir msp: me.getMspNodes())
    				{
    					SP_Node sp = (SP_Node) Network.get(msp.getPosition()).getProtocol(SPPid);
    					System.out.println("--MSP: "+msp.getPosition()+" banned? "+msp.isBanned()+" Ban Detector: "+sp.getBanDetectorType()+" CreationCycle: "+msp.getCreationCycle()+" BannedCycle: "+msp.getBannedCycle()+" spamsCount: "+sp.getSpamsCount()+" BanningTime: "+(msp.getBannedCycle()-msp.getCreationCycle()));
    					System.out.println("installedDGU? "+sp.isInstalledDGU()+" installedDGU detector: "+sp.getInstallDGUdetectorType()+" installedDGU cycle: "+sp.getInstalledDGUCycle()+" enabledStrictDGU? "+sp.isEnabledStrictDGU()+" enabledStrictDGU detector: "+sp.getEnableStrictDGUdetectorType()+" enabledSTrictDGU Cycle: "+sp.getEnabledStrictDGUCycle());
    					meTotalSpam = meTotalSpam + sp.getSpamsCount();
    				}
    				System.out.println("");
    				System.out.println("ME Total SPAM = "+meTotalSpam);
    				System.out.println("");
    				*/
    				/*for(ME_UsersNodesDir uNode: me.getuNodes())
    				{
    					System.out.println("User: "+uNode.getPosition()+" has MPSPsCount: "+uNode.getMPSPsCount()+" and MSPsCount: "+uNode.getMSPsCount());
    					System.out.println("");
    					
    					System.out.println("---Owning MPSPs are as following: ");
    					for(ME_OwningSP owningSP: uNode.getOwningMPSPs())
    					{
    						System.out.println("----MPSP: "+owningSP.getSpID());
    					}
    					
    					System.out.println("");
    					System.out.println("---Owning MSPs are as following: ");
    					for(ME_OwningSP owningSP: uNode.getOwningMSPs())
    					{
    						System.out.println("----MSP: "+owningSP.getSpID());
    					}
    				}
    				*/	
    				
    				/*
    				System.out.println("");
    				System.out.println("--uNodes Contents of ME "+me.getNodePos());
    				System.out.println("");
    				
    				for(ME_UsersNodesDir uNode: me.getuNodes())
    				{
    					System.out.println("User: "+uNode.getPosition()+" has MPSPsCount: "+uNode.getMPSPsCount()+" and MSPsCount: "+uNode.getMSPsCount());
    					System.out.println("");
    					
    					System.out.println("---Owning MPSPs are as following: ");
    					for(ME_OwningSP owningSP: uNode.getOwningMPSPs())
    					{
    						System.out.println("----MPSP: "+owningSP.getSpID());
    					}
    					
    					System.out.println("");
    					System.out.println("---Owning MSPs are as following: ");
    					for(ME_OwningSP owningSP: uNode.getOwningMSPs())
    					{
    						System.out.println("----MSP: "+owningSP.getSpID());
    					}
    				}
    			*/	
	        	}
	        	
    		}
  
    	}
    	
    }


}
