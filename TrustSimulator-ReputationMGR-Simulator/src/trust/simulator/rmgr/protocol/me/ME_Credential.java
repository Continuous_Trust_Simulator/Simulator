package trust.simulator.rmgr.protocol.me;

import java.util.ArrayList;
import java.util.List;

import trust.simulator.rmgr.protocol.sp.SP_Credential;
import trust.simulator.rmgr.protocol.user.Credential;

public class ME_Credential extends Credential{
	
	private long obtainedInCycle;
	private boolean MEallowedSPAM;
	
	private int MPSPsCount;
	
	private int MSPsCount;
	
	private List<ME_OwningSP> OwningMSPs = new ArrayList<ME_OwningSP>(20);
	
	private List<ME_OwningSP> OwningMPSPs = new ArrayList<ME_OwningSP>(20);
	
	public ME_Credential()
	{
		super();
	
		MEallowedSPAM = true;
		
		MPSPsCount = 0;
		
		MSPsCount = 0;
	}
	
	public ME_Credential(SP_Credential credential, long SPid, boolean MPSP)
	{
		super(credential.getId(), credential.getNodePosition(), credential.getSharingPermission());
		this.obtainedInCycle = credential.getObtainedInCycle();
		MEallowedSPAM = false;
		MPSPsCount = 0;
		MSPsCount = 0;
		
		ME_OwningSP owningSP = new ME_OwningSP(SPid, false);
		
		if(MPSP)
		{
			MPSPsCount = 1;
			OwningMPSPs.add(owningSP);
		}
		else
		{
			MSPsCount = 1;
			OwningMSPs.add(owningSP);
		}
	}
		
	public long getObtainedInCycle() {
		return obtainedInCycle;
	}

	public void setObtainedInCycle(long obtainedInCycle) {
		this.obtainedInCycle = obtainedInCycle;
	}

	public boolean isMEallowedSPAM() {
		return MEallowedSPAM;
	}

	public void setMEallowedSPAM(boolean mEallowedSPAM) {
		MEallowedSPAM = mEallowedSPAM;
	}

	public int getMPSPsCount() {
		return MPSPsCount;
	}

	public void setMPSPsCount(int mPSPsCount) {
		MPSPsCount = mPSPsCount;
	}

	public int getMSPsCount() {
		return MSPsCount;
	}

	public void setMSPsCount(int mSPsCount) {
		MSPsCount = mSPsCount;
	}

	public List<ME_OwningSP> getOwningMSPs() {
		return OwningMSPs;
	}

	public void setOwningMSPs(List<ME_OwningSP> owningMSPs) {
		OwningMSPs = owningMSPs;
	}

	public List<ME_OwningSP> getOwningMPSPs() {
		return OwningMPSPs;
	}

	public void setOwningMPSPs(List<ME_OwningSP> owningMPSPs) {
		OwningMPSPs = owningMPSPs;
	}
	
}
