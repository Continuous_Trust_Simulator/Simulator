package trust.simulator.rmgr.protocol.me;

public class ME_SPsNodesDir {
	
	private int position = -1;
	
	private double rank = -1;
	
	private double usefulness = -1;
	
	private int creationCycle = -1;
	
	private boolean banned = false;
	private int bannedCycle = -1;
	
	private boolean installedDGU = false;
	private int installedDGUCycle = -1;
	
	private boolean enabledStrictDGU = false;
	private int enabledStrictDGUCycle = -1;
	
	private boolean SPAM_Delay = false;
	private int SPAM_Delay_Period = -1;
	
	private boolean SPAM_Bombarding = false;
	private int SPAM_Bombarding_Period = -1;
	
	private boolean SPAM_Drop = false;
	private double SPAM_DropUser_Rate = -1;
	private double SPAM_DropCredential_Rate = -1;


	public ME_SPsNodesDir() {
		super();

	}


	public int getPosition() {
		return position;
	}


	public void setPosition(int position) {
		this.position = position;
	}


	public double getRank() {
		return rank;
	}


	public void setRank(double rank) {
		this.rank = rank;
	}


	public double getUsefulness() {
		return usefulness;
	}


	public void setUsefulness(double usefulness) {
		this.usefulness = usefulness;
	}


	public boolean isSPAM_Delay() {
		return SPAM_Delay;
	}


	public void setSPAM_Delay(boolean sPAM_Delay) {
		SPAM_Delay = sPAM_Delay;
	}


	public int getSPAM_Delay_Period() {
		return SPAM_Delay_Period;
	}


	public void setSPAM_Delay_Period(int sPAM_Delay_Period) {
		SPAM_Delay_Period = sPAM_Delay_Period;
	}


	public boolean isSPAM_Bombarding() {
		return SPAM_Bombarding;
	}


	public void setSPAM_Bombarding(boolean sPAM_Bombarding) {
		SPAM_Bombarding = sPAM_Bombarding;
	}


	public int getSPAM_Bombarding_Period() {
		return SPAM_Bombarding_Period;
	}


	public void setSPAM_Bombarding_Period(int sPAM_Bombarding_Period) {
		SPAM_Bombarding_Period = sPAM_Bombarding_Period;
	}


	public boolean isSPAM_Drop() {
		return SPAM_Drop;
	}


	public void setSPAM_Drop(boolean sPAM_Drop) {
		SPAM_Drop = sPAM_Drop;
	}


	public double getSPAM_DropUser_Rate() {
		return SPAM_DropUser_Rate;
	}


	public void setSPAM_DropUser_Rate(double sPAM_DropUser_Rate) {
		SPAM_DropUser_Rate = sPAM_DropUser_Rate;
	}


	public double getSPAM_DropCredential_Rate() {
		return SPAM_DropCredential_Rate;
	}


	public void setSPAM_DropCredential_Rate(double sPAM_DropCredential_Rate) {
		SPAM_DropCredential_Rate = sPAM_DropCredential_Rate;
	}


	public boolean isBanned() {
		return banned;
	}


	public void setBanned(boolean banned) {
		this.banned = banned;
	}


	public int getBannedCycle() {
		return bannedCycle;
	}


	public void setBannedCycle(int bannedCycle) {
		this.bannedCycle = bannedCycle;
	}


	public int getCreationCycle() {
		return creationCycle;
	}


	public void setCreationCycle(int creationCycle) {
		this.creationCycle = creationCycle;
	}


	public boolean isInstalledDGU() {
		return installedDGU;
	}


	public void setInstalledDGU(boolean installedDGU) {
		this.installedDGU = installedDGU;
	}


	public int getInstalledDGUCycle() {
		return installedDGUCycle;
	}


	public void setInstalledDGUCycle(int installedDGUCycle) {
		this.installedDGUCycle = installedDGUCycle;
	}


	public boolean isEnabledStrictDGU() {
		return enabledStrictDGU;
	}


	public void setEnabledStrictDGU(boolean enabledStrictDGU) {
		this.enabledStrictDGU = enabledStrictDGU;
	}


	public int getEnabledStrictDGUCycle() {
		return enabledStrictDGUCycle;
	}


	public void setEnabledStrictDGUCycle(int enabledStrictDGUCycle) {
		this.enabledStrictDGUCycle = enabledStrictDGUCycle;
	}


}
