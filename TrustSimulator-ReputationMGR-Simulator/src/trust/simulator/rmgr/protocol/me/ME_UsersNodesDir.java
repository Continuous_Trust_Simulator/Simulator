package trust.simulator.rmgr.protocol.me;

import java.util.ArrayList;
import java.util.List;

public class ME_UsersNodesDir {
	
	private int position;
	
	private List<ME_OwningSP> OwningMSPs = new ArrayList<ME_OwningSP>(20);
	
	private List<ME_OwningSP> OwningMPSPs = new ArrayList<ME_OwningSP>(20);
	
	//TODO optimize the size of this array
	List<ME_Credential> Credentials = new ArrayList<ME_Credential>(100);
	
	private boolean MEallowedSPAM;
	
	private int MPSPsCount;
	
	private int MSPsCount;

	public ME_UsersNodesDir() {
		super();
		
		position = -1;
		MEallowedSPAM = true;
		
		MPSPsCount = 0;
		MSPsCount = 0;
	}


	public int getPosition() {
		return position;
	}


	public void setPosition(int position) {
		this.position = position;
	}

	public boolean isMEallowedSPAM() {
		return MEallowedSPAM;
	}


	public void setMEallowedSPAM(boolean mEallowedSPAM) {
		MEallowedSPAM = mEallowedSPAM;
	}


	public int getMPSPsCount() {
		return MPSPsCount;
	}


	public void setMPSPsCount(int mPSPsCount) {
		MPSPsCount = mPSPsCount;
	}


	public int getMSPsCount() {
		return MSPsCount;
	}


	public void setMSPsCount(int mSPsCount) {
		MSPsCount = mSPsCount;
	}


	public List<ME_OwningSP> getOwningMSPs() {
		return OwningMSPs;
	}


	public void setOwningMSPs(List<ME_OwningSP> owningMSPs) {
		OwningMSPs = owningMSPs;
	}


	public List<ME_OwningSP> getOwningMPSPs() {
		return OwningMPSPs;
	}


	public void setOwningMPSPs(List<ME_OwningSP> owningMPSPs) {
		OwningMPSPs = owningMPSPs;
	}

}
