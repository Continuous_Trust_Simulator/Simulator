package trust.simulator.rmgr.protocol.me;

import java.util.ArrayList;
import java.util.List;

import peersim.core.Network;

import trust.simulator.rmgr.protocol.RMGR_Node;
import trust.simulator.rmgr.protocol.sp.SP_Credential;
import trust.simulator.rmgr.protocol.sp.SP_Node;
import trust.simulator.rmgr.protocol.superNode.Super_Node;

public class ME_Node extends RMGR_Node{
	
	private String thePrefix;
	
	//List of associated malicious Users
	//TODO Optimize Array Size
	private List<ME_UsersNodesDir> uNodes = new ArrayList<ME_UsersNodesDir> (100);
	
	//TODO Optimize Array Size
	private List<ME_SPsNodesDir> mpspNodes = new ArrayList<ME_SPsNodesDir> (20);
	
	//TODO Optimize Array Size
	private List<ME_SPsNodesDir> mspNodes = new ArrayList<ME_SPsNodesDir> (50);

	// -1 means do not SPAM User, only its credentials if they satisfy the rules
	private int ME_N_MIN_MPSPs_toSPAM_User = -1;

	// -1 means do not check counter for specific credentials, just the overall counter for the user
	private int ME_N_MIN_MPSPs_toSPAM_Credential = -1;
	
	private boolean UnpopularSPsColluding = false; 
	
	private double ME_MPSPsGR = 0;
	
	private double ME_MSPsGR = 0;
	
	private int ME_MAX_MPSPs = 0;
	
	private int ME_MAX_MSPs = 0;
	
	private double UnAddedMPSPsCount;
	
	private double UnAddedMSPsCount;
	
	private int newMPSPcounter = 0;
	
	public ME_Node(String prefix) {
		super(prefix);
		thePrefix = prefix;
				
	}
	public Object clone() {

        ME_Node inp = new ME_Node(thePrefix);
        return inp;
    }
	
	public List<Boolean> addCredential(double usefulness, long spID, SP_Credential credential)
	{
		//System.out.println("Adding a new credential to ME, userID: "+credential.getNodePosition());
		
		List<Boolean> result = new ArrayList<Boolean>(2);
		
		long sharedBySPid = credential.getSharedBySPid();
		
		boolean MPSP = false;
		boolean sharedSPFound = false;
		
		if(sharedBySPid > 2)
		{
			for(ME_SPsNodesDir mpsp: mpspNodes)
				if(mpsp.getPosition() == sharedBySPid)
				{
					sharedSPFound = true;
					spID = sharedBySPid;
					MPSP = true;
					break;
				}
				
			if(!sharedSPFound)
				for(ME_SPsNodesDir msp: mspNodes)
					if(msp.getPosition() == sharedBySPid)
					{
						sharedSPFound = true;
						spID = sharedBySPid;
						MPSP = false;
						break;
					}
		}
			
	
		if(!sharedSPFound && usefulness >= usefulnessMIN)
			MPSP = true;
		
		boolean userFound = false;
		for(ME_UsersNodesDir user: uNodes)
		{
			if(user.getPosition() == credential.getNodePosition())
			{
				userFound = true;
						
				ME_OwningSP newOwningSP = new ME_OwningSP(spID, false);
				
				if(MPSP)
				{	
					boolean idFound = false;
					long id;
					for(ME_OwningSP owningSP: user.getOwningMPSPs())
					{
						id = owningSP.getSpID();
						
						if(id == spID)
						{
							idFound = true;
							newOwningSP = owningSP;
							break;
						}
					}
					if(!idFound)
					{
						user.getOwningMPSPs().add(newOwningSP);
						user.setMPSPsCount(user.getMPSPsCount()+1);
					}
				}
				else
				{			
					boolean idFound = false;
					long id;
					for(ME_OwningSP owningSP: user.getOwningMSPs())
					{
						id = owningSP.getSpID();
						
						if(id == spID)
						{
							idFound = true;
							newOwningSP = owningSP;
							break;
						}
					}
					if(!idFound)
					{
						user.getOwningMSPs().add(newOwningSP);
						user.setMSPsCount(user.getMSPsCount()+1);
					}
				}
			
				if(user.getMPSPsCount() >= this.ME_N_MIN_MPSPs_toSPAM_User && !(MPSP && UnpopularSPsColluding))
				{
					result.add(true);
					newOwningSP.setME_AllowedSPAM(true);	
				}
				else
				{
					result.add(false);
				}
					
				boolean credentialFound = false;
				for(ME_Credential meCredential: user.Credentials)
				{
					newOwningSP = new ME_OwningSP(spID, false);
					
					if(meCredential.getId() == credential.getId())
					{
						credentialFound = true;
						
						if(MPSP)
						{				
							boolean idFound = false;
							long id;
							for(ME_OwningSP owningSP: meCredential.getOwningMPSPs())
							{
								id = owningSP.getSpID();
								
								if(id == spID)
								{
									newOwningSP = owningSP;
									idFound = true;
									break;
								}
							}
							if(!idFound)
							{
								meCredential.getOwningMPSPs().add(newOwningSP);
								meCredential.setMPSPsCount(meCredential.getMPSPsCount()+1);
							}
						}
						else
						{
							boolean idFound = false;
							long id;
							for(ME_OwningSP owningSP: meCredential.getOwningMSPs())
							{
								id = owningSP.getSpID();
								
								if(id == spID)
								{
									newOwningSP = owningSP;
									
									idFound = true;
									break;
								}
							}
							if(!idFound)
							{
								meCredential.getOwningMSPs().add(newOwningSP);
								meCredential.setMSPsCount(meCredential.getMSPsCount()+1);
							}
						}
						
						if(meCredential.getMPSPsCount() >= this.ME_N_MIN_MPSPs_toSPAM_Credential && !(MPSP && UnpopularSPsColluding))
						{
							result.add(true);
							newOwningSP.setME_AllowedSPAM(true);
						}
						else
						{
							result.add(false);
						}
						
						break;
					}
				}
				
				if(!credentialFound)
				{
					ME_Credential newCredential = new ME_Credential(credential, spID, MPSP);
					user.Credentials.add(newCredential);
					
					if(newCredential.getMPSPsCount() >= this.ME_N_MIN_MPSPs_toSPAM_Credential && !(MPSP && UnpopularSPsColluding))
					{
						result.add(true);
						newCredential.setMEallowedSPAM(true);
						if(MPSP)
							newCredential.getOwningMPSPs().get(0).setME_AllowedSPAM(true);
						else
							newCredential.getOwningMSPs().get(0).setME_AllowedSPAM(true);
					}
					else
					{
						result.add(false);
						newCredential.setMEallowedSPAM(false);
					}
					
				}
				
				break;
			}
		}
		if(!userFound)
		{
			ME_UsersNodesDir user = new ME_UsersNodesDir();
			user.setPosition(credential.getNodePosition());
			
			ME_OwningSP newOwningSP = new ME_OwningSP(spID, false);
			
			if(MPSP)
			{
				user.setMPSPsCount(user.getMPSPsCount()+1);
				user.getOwningMPSPs().add(newOwningSP);
			}
			else
			{
				user.setMSPsCount(user.getMSPsCount()+1);
				user.getOwningMSPs().add(newOwningSP);
			}
			
			ME_Credential newCredential = new ME_Credential(credential, spID, MPSP);
			user.Credentials.add(newCredential);
			
			uNodes.add(user);
			
			if(user.getMPSPsCount() >= this.ME_N_MIN_MPSPs_toSPAM_User && !(MPSP && UnpopularSPsColluding))
			{
				result.add(true);
				user.setMEallowedSPAM(true);
				newOwningSP.setME_AllowedSPAM(true);
			}
			else
			{
				result.add(false);
				user.setMEallowedSPAM(false);
			}
			
			if(newCredential.getMPSPsCount() >= this.ME_N_MIN_MPSPs_toSPAM_Credential && !(MPSP && UnpopularSPsColluding))
			{
				result.add(true);
				newCredential.setMEallowedSPAM(true);
				if(MPSP)
					newCredential.getOwningMPSPs().get(0).setME_AllowedSPAM(true);
				else
					newCredential.getOwningMSPs().get(0).setME_AllowedSPAM(true);
			}
			else
			{
				result.add(false);
				newCredential.setMEallowedSPAM(false);
			}
		}
		
		return result;
	}
	
	public void processCredentials()
	{
		for(ME_UsersNodesDir user: uNodes)
		{
			//if(ME_N_MIN_MPSPs_toSPAM_User == -1)
				//user.setMEallowedSPAM(true);
			
			if(user.getMPSPsCount() < ME_N_MIN_MPSPs_toSPAM_User)
				continue;

			if(ME_N_MIN_MPSPs_toSPAM_User != -1 && !user.isMEallowedSPAM())
			{
				user.setMEallowedSPAM(true);
				
				if(!UnpopularSPsColluding)
				{
					for(ME_OwningSP owningSP: user.getOwningMPSPs())
					{
						long id = owningSP.getSpID();
						SP_Node mpsp = (SP_Node) Network.get((int) id).getProtocol(SPPid);
						
						if(ME_N_MIN_MPSPs_toSPAM_Credential == -1)
							mpsp.ME_AllowAllUserCredentialsSPAM(user.getPosition(), true);
						else
						{
							for(ME_Credential credential: user.Credentials)
							{
								for(ME_OwningSP owningSP2: credential.getOwningMPSPs())
								{
									long spID = owningSP2.getSpID();
									
									if(spID == id)
									{
										if(credential.getMPSPsCount() >= ME_N_MIN_MPSPs_toSPAM_Credential && !owningSP2.isME_AllowedSPAM())
										{
											credential.setMEallowedSPAM(true);
											owningSP2.setME_AllowedSPAM(true);
											mpsp.ME_AllowCredentialSPAM(user.getPosition(), credential.getId(), true);
										}
									}
								}
							}
						}
					}
				}
				
				for(ME_OwningSP owningSP: user.getOwningMSPs())
				{
					long id = owningSP.getSpID();
					SP_Node msp = (SP_Node) Network.get((int) id).getProtocol(SPPid);
					
					if(ME_N_MIN_MPSPs_toSPAM_Credential == -1)
						msp.ME_AllowAllUserCredentialsSPAM(user.getPosition(), true);
					
					else
					{
						for(ME_Credential credential: user.Credentials)
						{
							for(ME_OwningSP owningSP2: credential.getOwningMSPs())
							{
								long spID = owningSP2.getSpID();
								
								if(spID == id)
								{
									if(credential.getMPSPsCount() >= ME_N_MIN_MPSPs_toSPAM_Credential && !owningSP2.isME_AllowedSPAM())
									{
										credential.setMEallowedSPAM(true);
										owningSP2.setME_AllowedSPAM(true);
										msp.ME_AllowCredentialSPAM(user.getPosition(), credential.getId(), true);
									}
								}
							}
						}
					}
				}
			}
			
			else if ( (user.isMEallowedSPAM() && ME_N_MIN_MPSPs_toSPAM_Credential != -1) || ME_N_MIN_MPSPs_toSPAM_User == -1 )
			{
				if(!UnpopularSPsColluding)
				{
					for(ME_OwningSP owningSP: user.getOwningMPSPs())
					{
						long id = owningSP.getSpID();
						SP_Node mpsp = (SP_Node) Network.get((int) id).getProtocol(SPPid);
						
						for(ME_Credential credential: user.Credentials)
						{
							for(ME_OwningSP owningSP2: credential.getOwningMPSPs())
							{
								long spID = owningSP2.getSpID();
								
								if(spID == id)
								{
									if(credential.getMPSPsCount() >= ME_N_MIN_MPSPs_toSPAM_Credential && !owningSP2.isME_AllowedSPAM())
									{
										credential.setMEallowedSPAM(true);
										owningSP2.setME_AllowedSPAM(true);
										mpsp.ME_AllowCredentialSPAM(user.getPosition(), credential.getId(), true);
									}
								}
							}
						}
					}
				}
					
				for(ME_OwningSP owningSP: user.getOwningMSPs())
				{
					long id = owningSP.getSpID();
					SP_Node msp = (SP_Node) Network.get((int) id).getProtocol(SPPid);
					
					for(ME_Credential credential: user.Credentials)
					{
						for(ME_OwningSP owningSP2: credential.getOwningMSPs())
						{
							long spID = owningSP2.getSpID();
							
							if(spID == id)
							{
								if(credential.getMPSPsCount() >= ME_N_MIN_MPSPs_toSPAM_Credential && !owningSP2.isME_AllowedSPAM())
								{
									credential.setMEallowedSPAM(true);
									owningSP2.setME_AllowedSPAM(true);
									msp.ME_AllowCredentialSPAM(user.getPosition(), credential.getId(), true);
								}
							}
						}
					}
				}
			}
			
			
		}
	}
	
	public void addMSPNode(SP_Node spNode, boolean MPSP)
	{
		List<ME_SPsNodesDir> nodesDir;
		
		if(MPSP)
		{
			nodesDir = mpspNodes;
			
			/*
			if(CommonState.getIntTime() > 1)
			{
			
				if(!UnpopularSPsColluding && nodesDir.size() > 0)
				{
					//ME_SPsNodesDir oldNode = nodesDir.get(newMPSPcounter);
					
					for(ME_SPsNodesDir oldNode: nodesDir)
					{
						SP_Node oldMpsp = (SP_Node) Network.get(oldNode.getPosition()).getProtocol(SPPid);
						
						if(!oldMpsp.isBanned() && !oldMpsp.isMEallowed())
						{
							oldMpsp.setMEallowed(false);
							break;
						}
					}
					

				}
				
			
				newMPSPcounter++;
			}
			*/
		}
		else
			nodesDir = mspNodes;
		
		ME_SPsNodesDir newNode = new ME_SPsNodesDir();
		newNode.setCreationCycle(spNode.getCreationCycle());
		newNode.setPosition(spNode.getNodePos());
		newNode.setSPAM_Bombarding(spNode.isSPAM_Bombarding());
		newNode.setSPAM_Bombarding_Period(spNode.getSPAM_Bombarding_Period());
		newNode.setSPAM_Delay(spNode.isSPAM_Delay());
		newNode.setSPAM_Delay_Period(spNode.getSPAM_Delay_Period());
		newNode.setSPAM_Drop(spNode.isSPAM_Drop());
		newNode.setSPAM_DropCredential_Rate(spNode.getSPAM_DropCredential_Rate());
		newNode.setSPAM_DropUser_Rate(spNode.getSPAM_DropUser_Rate());
		newNode.setUsefulness(spNode.getUsefulness());
	
		nodesDir.add(newNode);
	}
	
	public void grow()
	{
		
		int mpspsCount = mpspNodes.size();
		
		double addMPSPsTotal = 0;
		int addMPSPs = 0;
		
		if(mpspsCount < ME_MAX_MPSPs)
		{
			if(mpspsCount == 0) mpspsCount = 1;
	
			addMPSPsTotal = ((ME_MPSPsGR/100) * mpspsCount) + UnAddedMPSPsCount;
			addMPSPs = (int) addMPSPsTotal;
			
			UnAddedMPSPsCount = addMPSPsTotal - addMPSPs;
		}
		
		
		int mspsCount = mspNodes.size();
		
		double addMSPsTotal = 0;
		int addMSPs = 0;
		
		if(mspsCount < ME_MAX_MSPs)
		{
		
			if(mspsCount == 0) mspsCount = 1;
	
			addMSPsTotal = ((ME_MSPsGR/100) * mspsCount) + UnAddedMSPsCount;
			addMSPs = (int) addMSPsTotal;
			
			UnAddedMSPsCount = addMSPsTotal - addMSPs;
		}
		
		if(addMPSPs > 0 || addMSPs > 0)
		{
			ME_NodesToAdd nodesToAdd = new ME_NodesToAdd();
			nodesToAdd.setMEid(this.nodePos);
			if(addMPSPs > 0)
				nodesToAdd.setMPSPs_NUM(addMPSPs);
			if(addMSPs > 0)
				nodesToAdd.setMSPs_NUM(addMSPs);
			
			Super_Node superNode = (Super_Node) Network.get(0).getProtocol(SuperPid);
			superNode.getMENodesToAdd().add(nodesToAdd);
		}

	}
	
	public int getSafeNieghbour(int SPid, boolean PSP)
	{
		int result = -1;
		
		//TODO optimize the number of trials
		if(!PSP)
			for(int i = 0; i < 5; i++)
			{
				ME_SPsNodesDir sp = mspNodes.get(this.randInt(0, mspNodes.size()-1));
				
				if(!sp.isInstalledDGU() && sp.getPosition() != SPid)
				{
					result = sp.getPosition();
					break;
				}
			}
		
		if(result == -1)
			//TODO optimize the number of trials
			for(int i = 0; i < 5; i++)
			{
				if(mpspNodes.size() > 100)
					System.out.println("mpspNodes.size == "+mpspNodes.size());
				
				ME_SPsNodesDir sp = mpspNodes.get(this.randInt(0, mpspNodes.size()-1));
				
				if(!sp.isInstalledDGU() && sp.getPosition() != SPid)
				{
					result = sp.getPosition();
					break;
				}
			}
		
		return result;
	}
	
	//action: 1 = banned; 2 = installedDGU; 3 = enabledSTrictDGU
	public void recieveSPUpdate(int spID, boolean MPSP, int action, int cycle)
	{
		if(MPSP)
		{
			for(ME_SPsNodesDir sp: mpspNodes)
			{
				if(sp.getPosition() == spID)
				{
					if(action == 1)
					{
						sp.setBanned(true);
						sp.setBannedCycle(cycle);
					}
					else if(action == 2)
					{
						sp.setInstalledDGU(true);
						sp.setInstalledDGUCycle(cycle);
					}
					else if(action == 3)
					{
						sp.setEnabledStrictDGU(true);
						sp.setEnabledStrictDGUCycle(cycle);
					}
					
					return;
				}
			}
		}
		else
		{
			for(ME_SPsNodesDir sp: mspNodes)
			{
				if(sp.getPosition() == spID)
				{
					if(action == 1)
					{
						sp.setBanned(true);
						sp.setBannedCycle(cycle);
					}
					else if(action == 2)
					{
						sp.setInstalledDGU(true);
						sp.setInstalledDGUCycle(cycle);
					}
					else if(action == 3)
					{
						sp.setEnabledStrictDGU(true);
						sp.setEnabledStrictDGUCycle(cycle);
					}
					
					return;
				}
			}
		}
	}
	
	public int getME_N_MIN_MPSPs_toSPAM_User() {
		return ME_N_MIN_MPSPs_toSPAM_User;
	}
	public void setME_N_MIN_MPSPs_toSPAM_User(int ME_N_MIN_MPSPs_toSPAM_User) {
		this.ME_N_MIN_MPSPs_toSPAM_User = ME_N_MIN_MPSPs_toSPAM_User;
	}
	public int getME_N_MIN_MPSPs_toSPAM_Credential() {
		return ME_N_MIN_MPSPs_toSPAM_Credential;
	}
	public void setME_N_MIN_MPSPs_toSPAM_Credential(
			int ME_N_MIN_MPSPs_toSPAM_Credential) {
		this.ME_N_MIN_MPSPs_toSPAM_Credential = ME_N_MIN_MPSPs_toSPAM_Credential;
	}
	public double getME_MPSPsGR() {
		return ME_MPSPsGR;
	}
	public void setME_MPSPsGR(double ME_MPSPsGR) {
		this.ME_MPSPsGR = ME_MPSPsGR;
	}
	public double getME_MSPsGR() {
		return ME_MSPsGR;
	}
	public void setME_MSPsGR(double ME_MSPsGR) {
		this.ME_MSPsGR = ME_MSPsGR;
	}
	public int getME_MAX_MPSPs() {
		return ME_MAX_MPSPs;
	}
	public void setME_MAX_MPSPs(int ME_MAX_MPSPs) {
		this.ME_MAX_MPSPs = ME_MAX_MPSPs;
	}
	public int getME_MAX_MSPs() {
		return ME_MAX_MSPs;
	}
	public void setME_MAX_MSPs(int ME_MAX_MSPs) {
		this.ME_MAX_MSPs = ME_MAX_MSPs;
	}
	public List<ME_UsersNodesDir> getuNodes() {
		return uNodes;
	}
	public void setuNodes(List<ME_UsersNodesDir> uNodes) {
		this.uNodes = uNodes;
	}
	public List<ME_SPsNodesDir> getMpspNodes() {
		return mpspNodes;
	}
	public void setMpspNodes(List<ME_SPsNodesDir> mpspNodes) {
		this.mpspNodes = mpspNodes;
	}
	public List<ME_SPsNodesDir> getMspNodes() {
		return mspNodes;
	}
	public void setMspNodes(List<ME_SPsNodesDir> mspNodes) {
		this.mspNodes = mspNodes;
	}
	public boolean isUnpopularSPsColluding() {
		return UnpopularSPsColluding;
	}
	public void setUnpopularSPsColluding(boolean UnpopularSPsColluding) {
		this.UnpopularSPsColluding = UnpopularSPsColluding;
	}
	public double getUnAddedMPSPsCount() {
		return UnAddedMPSPsCount;
	}
	public void setUnAddedMPSPsCount(double unAddedMPSPsCount) {
		UnAddedMPSPsCount = unAddedMPSPsCount;
	}
	public double getUnAddedMSPsCount() {
		return UnAddedMSPsCount;
	}
	public void setUnAddedMSPsCount(double unAddedMSPsCount) {
		UnAddedMSPsCount = unAddedMSPsCount;
	}
	public int getNewMPSPcounter() {
		return newMPSPcounter;
	}
	public void setNewMPSPcounter(int newMPSPcounter) {
		this.newMPSPcounter = newMPSPcounter;
	}
	

}
