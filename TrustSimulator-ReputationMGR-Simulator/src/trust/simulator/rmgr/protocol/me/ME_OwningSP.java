package trust.simulator.rmgr.protocol.me;

public class ME_OwningSP {

	private long spID = -1;
	
	private boolean ME_AllowedSPAM = false;

	public ME_OwningSP(long spID2, boolean ME_AllowedSPAM) {
		
		this.spID = spID2;
		this.ME_AllowedSPAM = ME_AllowedSPAM;
	}
	
	public ME_OwningSP() {

	}

	public long getSpID() {
		return spID;
	}

	public void setSpID(long spID) {
		this.spID = spID;
	}

	public boolean isME_AllowedSPAM() {
		return ME_AllowedSPAM;
	}

	public void setME_AllowedSPAM(boolean mE_AllowedSPAM) {
		ME_AllowedSPAM = mE_AllowedSPAM;
	}

}
