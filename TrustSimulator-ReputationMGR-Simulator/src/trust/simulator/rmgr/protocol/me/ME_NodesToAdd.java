package trust.simulator.rmgr.protocol.me;

public class ME_NodesToAdd {
	
	private int MEid = -1;
	private int MPSPs_NUM = 0;
	private int MSPs_NUM = 0;
	public int getMEid() {
		return MEid;
	}
	public void setMEid(int mEid) {
		MEid = mEid;
	}
	public int getMPSPs_NUM() {
		return MPSPs_NUM;
	}
	public void setMPSPs_NUM(int mPSPs_NUM) {
		MPSPs_NUM = mPSPs_NUM;
	}
	public int getMSPs_NUM() {
		return MSPs_NUM;
	}
	public void setMSPs_NUM(int mSPs_NUM) {
		MSPs_NUM = mSPs_NUM;
	}

}
