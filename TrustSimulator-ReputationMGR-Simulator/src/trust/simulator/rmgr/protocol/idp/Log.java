package trust.simulator.rmgr.protocol.idp;

public class Log {
	
	private int nodePosition;
	private long cycle;
	private int credentialID;
	private int spPosition;
	private double spUsefulness = -1;
	private boolean SharedByDGUSP = false;
	
	public Log()
	{
		//nodePosition = -1;
		//cycle = -1;
		//credentialID = -1;
	}
	
	public Log(int nodePos, long cycle, int credentialID, int spPosition, double spUsefulness, boolean SharedByDGUSP)
	{
		this.nodePosition = nodePos;
		this.cycle = cycle;
		this.credentialID = credentialID;
		this.spPosition = spPosition;
		this.spUsefulness = spUsefulness;
		this.SharedByDGUSP = SharedByDGUSP;
	}
	
	public int getNodePosition() {
		return nodePosition;
	}
	public void setNodePosition(int nodePosition) {
		this.nodePosition = nodePosition;
	}
	public long getCycle() {
		return cycle;
	}
	public void setCycle(long cycle) {
		this.cycle = cycle;
	}
	public int getCredentialID() {
		return credentialID;
	}
	public void setCredentialID(int credentialID) {
		this.credentialID = credentialID;
	}

	public int getSpPosition() {
		return spPosition;
	}

	public void setSpPosition(int spPosition) {
		this.spPosition = spPosition;
	}

	public double getSpUsefulness() {
		return spUsefulness;
	}

	public void setSpUsefulness(double spUsefulness) {
		this.spUsefulness = spUsefulness;
	}

	public boolean isSharedByDGUSP() {
		return SharedByDGUSP;
	}

	public void setSharedByDGUSP(boolean sharedByDGUSP) {
		SharedByDGUSP = sharedByDGUSP;
	}
}
