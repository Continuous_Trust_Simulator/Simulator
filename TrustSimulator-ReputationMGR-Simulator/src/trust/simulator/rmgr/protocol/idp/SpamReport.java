package trust.simulator.rmgr.protocol.idp;

import java.util.List;

import trust.simulator.rmgr.protocol.auditor.CaseLogs;
import trust.simulator.rmgr.protocol.auditor.CaseSPs;

public class SpamReport {

	private CaseLogs caseLogs;
	
	private List<CaseSPs> PSPsInteractedWith;

	public CaseLogs getCaseLogs() {
		return caseLogs;
	}

	public void setCaseLogs(CaseLogs caseLogs) {
		this.caseLogs = caseLogs;
	}

	public List<CaseSPs> getPSPsInteractedWith() {
		return PSPsInteractedWith;
	}

	public void setPSPsInteractedWith(List<CaseSPs> PSPsInteractedWith) {
		this.PSPsInteractedWith = PSPsInteractedWith;
	}
}
