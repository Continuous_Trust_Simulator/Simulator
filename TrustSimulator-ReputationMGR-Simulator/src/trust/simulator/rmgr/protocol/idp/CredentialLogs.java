package trust.simulator.rmgr.protocol.idp;

import java.util.ArrayList;
import java.util.List;

import peersim.core.CommonState;
import trust.simulator.rmgr.protocol.user.Credential;

public class CredentialLogs {
	
	private Credential credential;
	
	List<Log> logs;
	//private int logsCounter;

	//TODO optimize the size of the logs array
	
	public CredentialLogs()
	{
		this.setCredential(new Credential());
		this.logs = new ArrayList<Log> (250);
		//this.setLogsCounter(0);
	}
	
	public CredentialLogs(Credential credential)
	{
		this.setCredential(credential);
		this.logs = new ArrayList<Log> (250);
		//this.setLogsCounter(0);
	}

	public Credential getCredential() {
		return credential;
	}

	public void setCredential(Credential credential) {
		this.credential = credential;
	}
	
	/*public int getLogsCounter() {
		return logsCounter;
	}

	public void setLogsCounter(int logsCounter) {
		this.logsCounter = logsCounter;
	}
	*/

	public void addLog(int spPosition, double spUsefulness, boolean sharedByDGUSP)
	{
		logs.add(new Log(credential.getNodePosition(), CommonState.getTime(), credential.getId(), spPosition, spUsefulness, sharedByDGUSP));
		//setLogsCounter(getLogsCounter() + 1);
	}

	public List<Log> getLogs() {
		return logs;
	}
		
}
