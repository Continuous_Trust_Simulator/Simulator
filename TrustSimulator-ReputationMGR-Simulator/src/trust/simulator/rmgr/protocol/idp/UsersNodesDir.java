package trust.simulator.rmgr.protocol.idp;

import java.util.ArrayList;
import java.util.List;


//TODO Add holders for the local trust rankings and logs

public class UsersNodesDir {
	
	private int position;
	
	// 1: normal, 2: ST, 3: GT
	private int type;
	
	//TODO optimize the size of this array
	private List<CredentialLogs> CredentialLogs = new ArrayList<CredentialLogs>(100);
	

	public UsersNodesDir() {
		super();
		
		position = -1;

	}


	public int getPosition() {
		return position;
	}


	public void setPosition(int position) {
		this.position = position;
	}


	public List<CredentialLogs> getCredentialLogs() {
		return CredentialLogs;
	}


	public void setCredentialLogs(List<CredentialLogs> credentialLogs) {
		CredentialLogs = credentialLogs;
	}


	public int getType() {
		return type;
	}


	public void setType(int type) {
		this.type = type;
	}

}
