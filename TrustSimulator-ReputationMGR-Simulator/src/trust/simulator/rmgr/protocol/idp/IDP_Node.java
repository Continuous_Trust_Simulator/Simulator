package trust.simulator.rmgr.protocol.idp;

import java.util.ArrayList;
import java.util.List;

import peersim.core.CommonState;
import peersim.core.Network;
import trust.simulator.rmgr.protocol.RMGR_Node;
import trust.simulator.rmgr.protocol.auditor.Auditor_Node;
import trust.simulator.rmgr.protocol.auditor.Auditor_SPsNodesDir;
import trust.simulator.rmgr.protocol.auditor.CaseLogs;
import trust.simulator.rmgr.protocol.auditor.CaseSPs;
import trust.simulator.rmgr.protocol.auditor.TGRRank;
import trust.simulator.rmgr.protocol.me.ME_Node;
import trust.simulator.rmgr.protocol.sp.Partner;
import trust.simulator.rmgr.protocol.sp.SP_Node;
import trust.simulator.rmgr.protocol.user.Credential;
import trust.simulator.rmgr.protocol.user.ServiceRequest;
import trust.simulator.rmgr.protocol.user.User_Node;

public class IDP_Node extends RMGR_Node{
	
	List<UsersNodesDir> uNodes = new ArrayList<UsersNodesDir> (1000);
	
	List<SPsNodesDir> spNodes = new ArrayList<SPsNodesDir> (1000);
	
	List<SPsNodesDir> pspNodes = new ArrayList<SPsNodesDir> (100);
	
	List<SPsNodesDir> banned_spNodes = new ArrayList<SPsNodesDir> (1000);
	
	List<SPsNodesDir> banned_pspNodes = new ArrayList<SPsNodesDir> (100);
	
	private String thePrefix;
	
	private boolean DeployDGU = false;

	private double SPAM125Count = 0;
	private double MPSP125SPAMcount = 0;
	private double MSP125SPAMcount = 0;
	private double Popular125SPAMcount = 0;
	private double Unpopular125SPAMcount = 0;
	private double SPAM250Count = 0;
	private double MPSP250SPAMcount = 0;
	private double MSP250SPAMcount = 0;
	private double Popular250SPAMcount = 0;
	private double Unpopular250SPAMcount = 0;
	private double SPAM375Count = 0;
	private double MPSP375SPAMcount = 0;
	private double MSP375SPAMcount = 0;
	private double Popular375SPAMcount = 0;
	private double Unpopular375SPAMcount = 0;
	private double SPAM500Count = 0;
	private double MPSP500SPAMcount = 0;
	private double MSP500SPAMcount = 0;
	private double Popular500SPAMcount = 0;
	private double Unpopular500SPAMcount = 0;
	
	private double PCT_Undetected_MPSPs_at_125;
	private double PCT_Undetected_MPSPs_at_250;
	private double PCT_Undetected_MPSPs_at_375;
	private double PCT_Undetected_MPSPs_at_500;
    
	private double PCT_Undetected_MSPs_at_125;
	private double PCT_Undetected_MSPs_at_250;
	private double PCT_Undetected_MSPs_at_375;
	private double PCT_Undetected_MSPs_at_500;
    
	private double PCT_Undetected_Popular_MPSPs_at_125;
	private double PCT_Undetected_Popular_MPSPs_at_250;
	private double PCT_Undetected_Popular_MPSPs_at_375;
	private double PCT_Undetected_Popular_MPSPs_at_500;
    
	private double PCT_Undetected_Popular_MSPs_at_125;
	private double PCT_Undetected_Popular_MSPs_at_250;
	private double PCT_Undetected_Popular_MSPs_at_375;
	private double PCT_Undetected_Popular_MSPs_at_500;
    
	private double PCT_Undetected_Unpopular_MPSPs_at_125;
	private double PCT_Undetected_Unpopular_MPSPs_at_250;
	private double PCT_Undetected_Unpopular_MPSPs_at_375;
	private double PCT_Undetected_Unpopular_MPSPs_at_500;
    
	private double PCT_Undetected_Unpopular_MSPs_at_125;
	private double PCT_Undetected_Unpopular_MSPs_at_250;
	private double PCT_Undetected_Unpopular_MSPs_at_375;
	private double PCT_Undetected_Unpopular_MSPs_at_500;
	
	private double AvgTimeToBan_MPSP_at_125; 
	private double AvgTimeToBan_MPSP_at_250; 
	private double AvgTimeToBan_MPSP_at_375; 
	private double AvgTimeToBan_MPSP_at_500;
    
	private double AvgTimeToBan_MSP_at_125; 
	private double AvgTimeToBan_MSP_at_250; 
	private double AvgTimeToBan_MSP_at_375; 
	private double AvgTimeToBan_MSP_at_500;
    
	private double AvgTimeToBan_Popular_MPSP_at_125; 
	private double AvgTimeToBan_Popular_MPSP_at_250; 
	private double AvgTimeToBan_Popular_MPSP_at_375; 
	private double AvgTimeToBan_Popular_MPSP_at_500;
    
	private double AvgTimeToBan_Popular_MSP_at_125; 
	private double AvgTimeToBan_Popular_MSP_at_250; 
	private double AvgTimeToBan_Popular_MSP_at_375; 
	private double AvgTimeToBan_Popular_MSP_at_500;
    
	private double AvgTimeToBan_Unpopular_MPSP_at_125; 
	private double AvgTimeToBan_Unpopular_MPSP_at_250; 
	private double AvgTimeToBan_Unpopular_MPSP_at_375; 
	private double AvgTimeToBan_Unpopular_MPSP_at_500;
    
	private double AvgTimeToBan_Unpopular_MSP_at_125; 
	private double AvgTimeToBan_Unpopular_MSP_at_250; 
	private double AvgTimeToBan_Unpopular_MSP_at_375; 
	private double AvgTimeToBan_Unpopular_MSP_at_500;
    
	private double AvgSPAMToBan_MPSP_at_125; 
	private double AvgSPAMToBan_MPSP_at_250; 
	private double AvgSPAMToBan_MPSP_at_375; 
	private double AvgSPAMToBan_MPSP_at_500;
    
	private double AvgSPAMToBan_MSP_at_125; 
	private double AvgSPAMToBan_MSP_at_250; 
	private double AvgSPAMToBan_MSP_at_375; 
	private double AvgSPAMToBan_MSP_at_500;
    
	private double AvgSPAMToBan_Popular_MPSP_at_125; 
	private double AvgSPAMToBan_Popular_MPSP_at_250; 
	private double AvgSPAMToBan_Popular_MPSP_at_375; 
	private double AvgSPAMToBan_Popular_MPSP_at_500;
    
	private double AvgSPAMToBan_Popular_MSP_at_125; 
	private double AvgSPAMToBan_Popular_MSP_at_250; 
	private double AvgSPAMToBan_Popular_MSP_at_375; 
	private double AvgSPAMToBan_Popular_MSP_at_500;
    
	private double AvgSPAMToBan_Unpopular_MPSP_at_125; 
	private double AvgSPAMToBan_Unpopular_MPSP_at_250; 
	private double AvgSPAMToBan_Unpopular_MPSP_at_375; 
	private double AvgSPAMToBan_Unpopular_MPSP_at_500;
    
	private double AvgSPAMToBan_Unpopular_MSP_at_125; 
	private double AvgSPAMToBan_Unpopular_MSP_at_250; 
	private double AvgSPAMToBan_Unpopular_MSP_at_375; 
	private double AvgSPAMToBan_Unpopular_MSP_at_500;
    
	private double PCT_innocent_PSPs_banned_guilty_at_125;
	private double PCT_innocent_PSPs_banned_guilty_at_250;
	private double PCT_innocent_PSPs_banned_guilty_at_375;
	private double PCT_innocent_PSPs_banned_guilty_at_500;

	private double PCT_innocent_SPs_banned_guilty_at_125;
	private double PCT_innocent_SPs_banned_guilty_at_250;
	private double PCT_innocent_SPs_banned_guilty_at_375;
	private double PCT_innocent_SPs_banned_guilty_at_500;
	
	private double PCT_innocent_PSPs_banned_refusedInstallDGU_at_125;
	private double PCT_innocent_PSPs_banned_refusedInstallDGU_at_250;
	private double PCT_innocent_PSPs_banned_refusedInstallDGU_at_375;
	private double PCT_innocent_PSPs_banned_refusedInstallDGU_at_500;

	private double PCT_innocent_SPs_banned_refusedInstallDGU_at_125;
	private double PCT_innocent_SPs_banned_refusedInstallDGU_at_250;
	private double PCT_innocent_SPs_banned_refusedInstallDGU_at_375;
	private double PCT_innocent_SPs_banned_refusedInstallDGU_at_500;
	
	private double PCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_125;
	private double PCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_250;
	private double PCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_375;
	private double PCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_500;

	private double PCT_innocent_SPs_banned_refusedEnableStrictDGU_at_125;
	private double PCT_innocent_SPs_banned_refusedEnableStrictDGU_at_250;
	private double PCT_innocent_SPs_banned_refusedEnableStrictDGU_at_375;
	private double PCT_innocent_SPs_banned_refusedEnableStrictDGU_at_500;
	
	private double bannedInnocentSPCount = 0;
	private double bannedInnocentPSPCount = 0;
	
	double bannedInnocentSPCount_refusedInstallDGU = 0;
	double bannedInnocentPSPCount_refusedInstallDGU = 0;
	
	double bannedInnocentSPCount_refusedEnableStrictDGU = 0;
	double bannedInnocentPSPCount_refusedEnableStrictDGU = 0;
	
	double bannedMSPsCount = 0;
	double bannedMPSPsCount = 0;
	double bannedPopularMSPsCount = 0;
	double bannedPopularMPSPsCount = 0;
	double bannedUnpopularMSPsCount = 0;
	double bannedUnpopularMPSPsCount = 0;
	
    //% of Different groups of SPs installing DGU and % of those who enabled strict DGU
    private double PCT_PSPs_installed_DGU_at_125;
    private double PCT_PSPs_installed_DGU_at_250;
    private double PCT_PSPs_installed_DGU_at_375;
    private double PCT_PSPs_installed_DGU_at_500;

    private double PCT_PSPs_enabled_strict_DGU_at_125;
    private double PCT_PSPs_enabled_strict_DGU_at_250;
    private double PCT_PSPs_enabled_strict_DGU_at_375;
    private double PCT_PSPs_enabled_strict_DGU_at_500;
    
    private double PCT_SPs_installed_DGU_at_125;
    private double PCT_SPs_installed_DGU_at_250;
    private double PCT_SPs_installed_DGU_at_375;
    private double PCT_SPs_installed_DGU_at_500;

    private double PCT_SPs_enabled_strict_DGU_at_125;
    private double PCT_SPs_enabled_strict_DGU_at_250;
    private double PCT_SPs_enabled_strict_DGU_at_375;
    private double PCT_SPs_enabled_strict_DGU_at_500;
    
    private double PCT_MPSPs_installed_DGU_at_125;
    private double PCT_MPSPs_installed_DGU_at_250;
    private double PCT_MPSPs_installed_DGU_at_375;
    private double PCT_MPSPs_installed_DGU_at_500;

    private double PCT_MPSPs_enabled_strict_DGU_at_125;
    private double PCT_MPSPs_enabled_strict_DGU_at_250;
    private double PCT_MPSPs_enabled_strict_DGU_at_375;
    private double PCT_MPSPs_enabled_strict_DGU_at_500;
    
    private double PCT_MSPs_installed_DGU_at_125;
    private double PCT_MSPs_installed_DGU_at_250;
    private double PCT_MSPs_installed_DGU_at_375;
    private double PCT_MSPs_installed_DGU_at_500;

    private double PCT_MSPs_enabled_strict_DGU_at_125;
    private double PCT_MSPs_enabled_strict_DGU_at_250;
    private double PCT_MSPs_enabled_strict_DGU_at_375;
    private double PCT_MSPs_enabled_strict_DGU_at_500;
    
    private double PCT_Popular_MPSPs_installed_DGU_at_125;
    private double PCT_Popular_MPSPs_installed_DGU_at_250;
    private double PCT_Popular_MPSPs_installed_DGU_at_375;
    private double PCT_Popular_MPSPs_installed_DGU_at_500;

    private double PCT_Popular_MPSPs_enabled_strict_DGU_at_125;
    private double PCT_Popular_MPSPs_enabled_strict_DGU_at_250;
    private double PCT_Popular_MPSPs_enabled_strict_DGU_at_375;
    private double PCT_Popular_MPSPs_enabled_strict_DGU_at_500;
    
    private double PCT_Popular_MSPs_installed_DGU_at_125;
    private double PCT_Popular_MSPs_installed_DGU_at_250;
    private double PCT_Popular_MSPs_installed_DGU_at_375;
    private double PCT_Popular_MSPs_installed_DGU_at_500;

    private double PCT_Popular_MSPs_enabled_strict_DGU_at_125;
    private double PCT_Popular_MSPs_enabled_strict_DGU_at_250;
    private double PCT_Popular_MSPs_enabled_strict_DGU_at_375;
    private double PCT_Popular_MSPs_enabled_strict_DGU_at_500;
    
    private double PCT_Unpopular_MPSPs_installed_DGU_at_125;
    private double PCT_Unpopular_MPSPs_installed_DGU_at_250;
    private double PCT_Unpopular_MPSPs_installed_DGU_at_375;
    private double PCT_Unpopular_MPSPs_installed_DGU_at_500;

    private double PCT_Unpopular_MPSPs_enabled_strict_DGU_at_125;
    private double PCT_Unpopular_MPSPs_enabled_strict_DGU_at_250;
    private double PCT_Unpopular_MPSPs_enabled_strict_DGU_at_375;
    private double PCT_Unpopular_MPSPs_enabled_strict_DGU_at_500;
    
    private double PCT_Unpopular_MSPs_installed_DGU_at_125;
    private double PCT_Unpopular_MSPs_installed_DGU_at_250;
    private double PCT_Unpopular_MSPs_installed_DGU_at_375;
    private double PCT_Unpopular_MSPs_installed_DGU_at_500;

    private double PCT_Unpopular_MSPs_enabled_strict_DGU_at_125;
    private double PCT_Unpopular_MSPs_enabled_strict_DGU_at_250;
    private double PCT_Unpopular_MSPs_enabled_strict_DGU_at_375;
    private double PCT_Unpopular_MSPs_enabled_strict_DGU_at_500;	
	
	
	public IDP_Node(String prefix) {
		super(prefix);
		
		thePrefix = prefix;
	}
	
	public Object clone() {
        IDP_Node inp = new IDP_Node(thePrefix);
        return inp;
    }
	
	public void updateDGUNodes(int cycle)
	{
		double countPSPs = 0;
		double countInstalledDGUPSPs = 0;
		double countEnabledStrictDGUPSPs = 0;

		double countSPs = 0;
		double countInstalledDGUSPs = 0;
		double countEnabledStrictDGUSPs = 0;

		double countMPSPs = 0;
		double countInstalledDGUMPSPs = 0;
		double countEnabledStrictDGUMPSPs = 0;
		
		double countMSPs = 0;
		double countInstalledDGUMSPs = 0;
		double countEnabledStrictDGUMSPs = 0;
		
		double countPopularMPSPs = 0;
		double countPopularInstalledDGUMPSPs = 0;
		double countPopularEnabledStrictDGUMPSPs = 0;
		
		double countPopularMSPs = 0;
		double countPopularInstalledDGUMSPs = 0;
		double countPopularEnabledStrictDGUMSPs = 0;
		
		double countUnpopularMPSPs = 0;
		double countUnpopularInstalledDGUMPSPs = 0;
		double countUnpopularEnabledStrictDGUMPSPs = 0;
		
		double countUnpopularMSPs = 0;
		double countUnpopularInstalledDGUMSPs = 0;
		double countUnpopularEnabledStrictDGUMSPs = 0;
		
		for(SPsNodesDir spNode: banned_pspNodes)
		{
			SP_Node sp = (SP_Node) Network.get(spNode.getPosition()).getProtocol(SPPid);
			
			if(!sp.isMalicious())
			{
				countPSPs++;
				if(sp.isInstalledDGU())
					countInstalledDGUPSPs++;
				if(sp.isEnabledStrictDGU())
					countEnabledStrictDGUPSPs++;
				continue;
			}
			
			if(sp.isAssociatedWithME())
			{
				ME_Node me = (ME_Node) Network.get(sp.getMEid()).getProtocol(MEPid);
				
				if(me.isUnpopularSPsColluding())
				{
					countUnpopularMPSPs++;
					if(sp.isInstalledDGU())
						countUnpopularInstalledDGUMPSPs++;
					if(sp.isEnabledStrictDGU())
						countUnpopularEnabledStrictDGUMPSPs++;
				}
				else
				{
					countPopularMPSPs++;
					if(sp.isInstalledDGU())
						countPopularInstalledDGUMPSPs++;
					if(sp.isEnabledStrictDGU())
						countPopularEnabledStrictDGUMPSPs++;
				}
				
				continue;
			}
			
			countMPSPs++;
			if(sp.isInstalledDGU())
				countInstalledDGUMPSPs++;
			if(sp.isEnabledStrictDGU())
				countEnabledStrictDGUMPSPs++;
		}
		
		for(SPsNodesDir spNode: banned_spNodes)
		{
			SP_Node sp = (SP_Node) Network.get(spNode.getPosition()).getProtocol(SPPid);
			
			if(!sp.isMalicious())
			{
				countSPs++;
				if(sp.isInstalledDGU())
					countInstalledDGUSPs++;
				if(sp.isEnabledStrictDGU())
					countEnabledStrictDGUSPs++;
				continue;
			}
			
			if(sp.isAssociatedWithME())
			{
				ME_Node me = (ME_Node) Network.get(sp.getMEid()).getProtocol(MEPid);
				
				if(me.isUnpopularSPsColluding())
				{
					countUnpopularMSPs++;
					if(sp.isInstalledDGU())
						countUnpopularInstalledDGUMSPs++;
					if(sp.isEnabledStrictDGU())
						countUnpopularEnabledStrictDGUMSPs++;
				}
				else
				{
					countPopularMSPs++;
					if(sp.isInstalledDGU())
						countPopularInstalledDGUMSPs++;
					if(sp.isEnabledStrictDGU())
						countPopularEnabledStrictDGUMSPs++;
				}
				
				continue;
			}
			
			countMSPs++;
			if(sp.isInstalledDGU())
				countInstalledDGUMSPs++;
			if(sp.isEnabledStrictDGU())
				countEnabledStrictDGUMSPs++;
		}
		
		for(SPsNodesDir spNode: pspNodes)
		{
			SP_Node sp = (SP_Node) Network.get(spNode.getPosition()).getProtocol(SPPid);
			
			if(!sp.isMalicious())
			{
				countPSPs++;
				if(sp.isInstalledDGU())
					countInstalledDGUPSPs++;
				if(sp.isEnabledStrictDGU())
					countEnabledStrictDGUPSPs++;
				continue;
			}
			
			if(sp.isAssociatedWithME())
			{
				ME_Node me = (ME_Node) Network.get(sp.getMEid()).getProtocol(MEPid);
				
				if(me.isUnpopularSPsColluding())
				{
					countUnpopularMPSPs++;
					if(sp.isInstalledDGU())
						countUnpopularInstalledDGUMPSPs++;
					if(sp.isEnabledStrictDGU())
						countUnpopularEnabledStrictDGUMPSPs++;
				}
				else
				{
					countPopularMPSPs++;
					if(sp.isInstalledDGU())
						countPopularInstalledDGUMPSPs++;
					if(sp.isEnabledStrictDGU())
						countPopularEnabledStrictDGUMPSPs++;
				}
				
				continue;
			}
			
			countMPSPs++;
			if(sp.isInstalledDGU())
				countInstalledDGUMPSPs++;
			if(sp.isEnabledStrictDGU())
				countEnabledStrictDGUMPSPs++;
		}
		
		for(SPsNodesDir spNode: spNodes)
		{
			SP_Node sp = (SP_Node) Network.get(spNode.getPosition()).getProtocol(SPPid);
			
			if(!sp.isMalicious())
			{
				countSPs++;
				if(sp.isInstalledDGU())
					countInstalledDGUSPs++;
				if(sp.isEnabledStrictDGU())
					countEnabledStrictDGUSPs++;
				continue;
			}
			
			if(sp.isAssociatedWithME())
			{
				ME_Node me = (ME_Node) Network.get(sp.getMEid()).getProtocol(MEPid);
				
				if(me.isUnpopularSPsColluding())
				{
					countUnpopularMSPs++;
					if(sp.isInstalledDGU())
						countUnpopularInstalledDGUMSPs++;
					if(sp.isEnabledStrictDGU())
						countUnpopularEnabledStrictDGUMSPs++;
				}
				else
				{
					countPopularMSPs++;
					if(sp.isInstalledDGU())
						countPopularInstalledDGUMSPs++;
					if(sp.isEnabledStrictDGU())
						countPopularEnabledStrictDGUMSPs++;
				}
				
				continue;
			}
			
			countMSPs++;
			if(sp.isInstalledDGU())
				countInstalledDGUMSPs++;
			if(sp.isEnabledStrictDGU())
				countEnabledStrictDGUMSPs++;
		}
		
	switch(cycle){
		
		case 125:
			
			PCT_PSPs_installed_DGU_at_125 = countInstalledDGUPSPs*100/countPSPs;
			PCT_SPs_installed_DGU_at_125 = countInstalledDGUSPs*100/countSPs;
			PCT_MPSPs_installed_DGU_at_125 = countInstalledDGUMPSPs*100/countMPSPs;
			PCT_MSPs_installed_DGU_at_125 = countInstalledDGUMSPs*100/countMSPs;
			PCT_Popular_MPSPs_installed_DGU_at_125 = countPopularInstalledDGUMPSPs*100/countPopularMPSPs;
			PCT_Popular_MSPs_installed_DGU_at_125 = countPopularInstalledDGUMSPs*100/countPopularMSPs;
			PCT_Unpopular_MPSPs_installed_DGU_at_125 = countUnpopularInstalledDGUMPSPs*100/countUnpopularMPSPs;
			PCT_Popular_MSPs_installed_DGU_at_125 = countUnpopularInstalledDGUMSPs*100/countUnpopularMSPs;
			
			PCT_PSPs_enabled_strict_DGU_at_125 = countEnabledStrictDGUPSPs*100/countPSPs;
			PCT_SPs_enabled_strict_DGU_at_125 = countEnabledStrictDGUSPs*100/countSPs;
			PCT_MPSPs_enabled_strict_DGU_at_125 = countEnabledStrictDGUMPSPs*100/countMPSPs;
			PCT_MSPs_enabled_strict_DGU_at_125 = countEnabledStrictDGUMSPs*100/countMSPs;
			PCT_Popular_MPSPs_enabled_strict_DGU_at_125 = countPopularEnabledStrictDGUMPSPs*100/countPopularMPSPs;
			PCT_Popular_MSPs_enabled_strict_DGU_at_125 = countPopularEnabledStrictDGUMSPs*100/countPopularMSPs;
			PCT_Unpopular_MPSPs_enabled_strict_DGU_at_125 = countUnpopularEnabledStrictDGUMPSPs*100/countUnpopularMPSPs;
			PCT_Popular_MSPs_enabled_strict_DGU_at_125 = countUnpopularEnabledStrictDGUMSPs*100/countUnpopularMSPs;
			
			break;
			
		case 250:

			PCT_PSPs_installed_DGU_at_250 = countInstalledDGUPSPs*100/countPSPs;
			PCT_SPs_installed_DGU_at_250 = countInstalledDGUSPs*100/countSPs;
			PCT_MPSPs_installed_DGU_at_250 = countInstalledDGUMPSPs*100/countMPSPs;
			PCT_MSPs_installed_DGU_at_250 = countInstalledDGUMSPs*100/countMSPs;
			PCT_Popular_MPSPs_installed_DGU_at_250 = countPopularInstalledDGUMPSPs*100/countPopularMPSPs;
			PCT_Popular_MSPs_installed_DGU_at_250 = countPopularInstalledDGUMSPs*100/countPopularMSPs;
			PCT_Unpopular_MPSPs_installed_DGU_at_250 = countUnpopularInstalledDGUMPSPs*100/countUnpopularMPSPs;
			PCT_Popular_MSPs_installed_DGU_at_250 = countUnpopularInstalledDGUMSPs*100/countUnpopularMSPs;
			
			PCT_PSPs_enabled_strict_DGU_at_250 = countEnabledStrictDGUPSPs*100/countPSPs;
			PCT_SPs_enabled_strict_DGU_at_250 = countEnabledStrictDGUSPs*100/countSPs;
			PCT_MPSPs_enabled_strict_DGU_at_250 = countEnabledStrictDGUMPSPs*100/countMPSPs;
			PCT_MSPs_enabled_strict_DGU_at_250 = countEnabledStrictDGUMSPs*100/countMSPs;
			PCT_Popular_MPSPs_enabled_strict_DGU_at_250 = countPopularEnabledStrictDGUMPSPs*100/countPopularMPSPs;
			PCT_Popular_MSPs_enabled_strict_DGU_at_250 = countPopularEnabledStrictDGUMSPs*100/countPopularMSPs;
			PCT_Unpopular_MPSPs_enabled_strict_DGU_at_250 = countUnpopularEnabledStrictDGUMPSPs*100/countUnpopularMPSPs;
			PCT_Popular_MSPs_enabled_strict_DGU_at_250 = countUnpopularEnabledStrictDGUMSPs*100/countUnpopularMSPs;
			
			break;
			
		case 375:

			PCT_PSPs_installed_DGU_at_375 = countInstalledDGUPSPs*100/countPSPs;
			PCT_SPs_installed_DGU_at_375 = countInstalledDGUSPs*100/countSPs;
			PCT_MPSPs_installed_DGU_at_375 = countInstalledDGUMPSPs*100/countMPSPs;
			PCT_MSPs_installed_DGU_at_375 = countInstalledDGUMSPs*100/countMSPs;
			PCT_Popular_MPSPs_installed_DGU_at_375 = countPopularInstalledDGUMPSPs*100/countPopularMPSPs;
			PCT_Popular_MSPs_installed_DGU_at_375 = countPopularInstalledDGUMSPs*100/countPopularMSPs;
			PCT_Unpopular_MPSPs_installed_DGU_at_375 = countUnpopularInstalledDGUMPSPs*100/countUnpopularMPSPs;
			PCT_Popular_MSPs_installed_DGU_at_375 = countUnpopularInstalledDGUMSPs*100/countUnpopularMSPs;
			
			PCT_PSPs_enabled_strict_DGU_at_375 = countEnabledStrictDGUPSPs*100/countPSPs;
			PCT_SPs_enabled_strict_DGU_at_375 = countEnabledStrictDGUSPs*100/countSPs;
			PCT_MPSPs_enabled_strict_DGU_at_375 = countEnabledStrictDGUMPSPs*100/countMPSPs;
			PCT_MSPs_enabled_strict_DGU_at_375 = countEnabledStrictDGUMSPs*100/countMSPs;
			PCT_Popular_MPSPs_enabled_strict_DGU_at_375 = countPopularEnabledStrictDGUMPSPs*100/countPopularMPSPs;
			PCT_Popular_MSPs_enabled_strict_DGU_at_375 = countPopularEnabledStrictDGUMSPs*100/countPopularMSPs;
			PCT_Unpopular_MPSPs_enabled_strict_DGU_at_375 = countUnpopularEnabledStrictDGUMPSPs*100/countUnpopularMPSPs;
			PCT_Popular_MSPs_enabled_strict_DGU_at_375 = countUnpopularEnabledStrictDGUMSPs*100/countUnpopularMSPs;
			
			break;
			
		case 500:

			PCT_PSPs_installed_DGU_at_500 = countInstalledDGUPSPs*100/countPSPs;
			PCT_SPs_installed_DGU_at_500 = countInstalledDGUSPs*100/countSPs;
			PCT_MPSPs_installed_DGU_at_500 = countInstalledDGUMPSPs*100/countMPSPs;
			PCT_MSPs_installed_DGU_at_500 = countInstalledDGUMSPs*100/countMSPs;
			PCT_Popular_MPSPs_installed_DGU_at_500 = countPopularInstalledDGUMPSPs*100/countPopularMPSPs;
			PCT_Popular_MSPs_installed_DGU_at_500 = countPopularInstalledDGUMSPs*100/countPopularMSPs;
			PCT_Unpopular_MPSPs_installed_DGU_at_500 = countUnpopularInstalledDGUMPSPs*100/countUnpopularMPSPs;
			PCT_Popular_MSPs_installed_DGU_at_500 = countUnpopularInstalledDGUMSPs*100/countUnpopularMSPs;
				
			PCT_PSPs_enabled_strict_DGU_at_500 = countEnabledStrictDGUPSPs*100/countPSPs;
			PCT_SPs_enabled_strict_DGU_at_500 = countEnabledStrictDGUSPs*100/countSPs;
			PCT_MPSPs_enabled_strict_DGU_at_500 = countEnabledStrictDGUMPSPs*100/countMPSPs;
			PCT_MSPs_enabled_strict_DGU_at_500 = countEnabledStrictDGUMSPs*100/countMSPs;
			PCT_Popular_MPSPs_enabled_strict_DGU_at_500 = countPopularEnabledStrictDGUMPSPs*100/countPopularMPSPs;
			PCT_Popular_MSPs_enabled_strict_DGU_at_500 = countPopularEnabledStrictDGUMSPs*100/countPopularMSPs;
			PCT_Unpopular_MPSPs_enabled_strict_DGU_at_500 = countUnpopularEnabledStrictDGUMPSPs*100/countUnpopularMPSPs;
			PCT_Popular_MSPs_enabled_strict_DGU_at_500 = countUnpopularEnabledStrictDGUMSPs*100/countUnpopularMSPs;
			
			break;
		}
			
	}
	
	public void updateInnocentBanned(int cycle)
	{
		double PSP_Count = 0;
		
		double SP_Count = 0;
		
		for(SPsNodesDir spNode: spNodes)
			if(!spNode.isMalicious())
			{
				SP_Count++;
			}
		for(SPsNodesDir spNode: pspNodes)
			if(!spNode.isMalicious())
				PSP_Count++;
		
		switch(cycle){
		
		case 125:
		
			if(!DeployDGU)
			{
				PCT_innocent_PSPs_banned_guilty_at_125 = bannedInnocentPSPCount*100/(PSP_Count+bannedInnocentPSPCount);
				PCT_innocent_SPs_banned_guilty_at_125 = bannedInnocentSPCount*100/(SP_Count+bannedInnocentSPCount);
			}
			else
			{
				PCT_innocent_PSPs_banned_refusedInstallDGU_at_125 = bannedInnocentPSPCount_refusedInstallDGU*100/(PSP_Count+bannedInnocentPSPCount);
				PCT_innocent_SPs_banned_refusedInstallDGU_at_125 = bannedInnocentSPCount_refusedInstallDGU*100/(SP_Count+bannedInnocentSPCount);					
				
				PCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_125 = bannedInnocentPSPCount_refusedEnableStrictDGU*100/(PSP_Count+bannedInnocentPSPCount);
				PCT_innocent_SPs_banned_refusedEnableStrictDGU_at_125 = bannedInnocentSPCount_refusedEnableStrictDGU*100/(SP_Count+bannedInnocentSPCount);					
			}
			
			break;
			
		case 250:

			if(!DeployDGU)
			{
				PCT_innocent_PSPs_banned_guilty_at_250 = bannedInnocentPSPCount*100/(PSP_Count+bannedInnocentPSPCount);
				PCT_innocent_SPs_banned_guilty_at_250 = bannedInnocentSPCount*100/(SP_Count+bannedInnocentSPCount);
			}
			else
			{
				PCT_innocent_PSPs_banned_refusedInstallDGU_at_250 = bannedInnocentPSPCount_refusedInstallDGU*100/(PSP_Count+bannedInnocentPSPCount);
				PCT_innocent_SPs_banned_refusedInstallDGU_at_250 = bannedInnocentSPCount_refusedInstallDGU*100/(SP_Count+bannedInnocentSPCount);					
				
				PCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_250 = bannedInnocentPSPCount_refusedEnableStrictDGU*100/(PSP_Count+bannedInnocentPSPCount);
				PCT_innocent_SPs_banned_refusedEnableStrictDGU_at_250 = bannedInnocentSPCount_refusedEnableStrictDGU*100/(SP_Count+bannedInnocentSPCount);					
			}
			break;
			
		case 375:

			if(!DeployDGU)
			{
				PCT_innocent_PSPs_banned_guilty_at_375 = bannedInnocentPSPCount*100/(PSP_Count+bannedInnocentPSPCount);
				PCT_innocent_SPs_banned_guilty_at_375 = bannedInnocentSPCount*100/(SP_Count+bannedInnocentSPCount);						
			}
			else
			{
				PCT_innocent_PSPs_banned_refusedInstallDGU_at_375 = bannedInnocentPSPCount_refusedInstallDGU*100/(PSP_Count+bannedInnocentPSPCount);
				PCT_innocent_SPs_banned_refusedInstallDGU_at_375 = bannedInnocentSPCount_refusedInstallDGU*100/(SP_Count+bannedInnocentSPCount);					
				
				PCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_375 = bannedInnocentPSPCount_refusedEnableStrictDGU*100/(PSP_Count+bannedInnocentPSPCount);
				PCT_innocent_SPs_banned_refusedEnableStrictDGU_at_375 = bannedInnocentSPCount_refusedEnableStrictDGU*100/(SP_Count+bannedInnocentSPCount);					
			}
			
			
			break;
			
		case 500:

			
			if(!DeployDGU)
			{
				PCT_innocent_PSPs_banned_guilty_at_500 = bannedInnocentPSPCount*100/(PSP_Count+bannedInnocentPSPCount);
				PCT_innocent_SPs_banned_guilty_at_500 = bannedInnocentSPCount*100/(SP_Count+bannedInnocentSPCount);						
			}
			else
			{
				PCT_innocent_PSPs_banned_refusedInstallDGU_at_500 = bannedInnocentPSPCount_refusedInstallDGU*100/(PSP_Count+bannedInnocentPSPCount);
				PCT_innocent_SPs_banned_refusedInstallDGU_at_500 = bannedInnocentSPCount_refusedInstallDGU*100/(SP_Count+bannedInnocentSPCount);					
				
				PCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_500 = bannedInnocentPSPCount_refusedEnableStrictDGU*100/(PSP_Count+bannedInnocentPSPCount);
				PCT_innocent_SPs_banned_refusedEnableStrictDGU_at_500 = bannedInnocentSPCount_refusedEnableStrictDGU*100/(SP_Count+bannedInnocentSPCount);					
			}
			
			break;
		}
		
	}

	
	public void updateAvgToBan(int cycle)
	{
		SP_Node sp;
		
		double Banned_MPSP_Count = 0;
		double Time_toBan_MPSP_Count = 0;
		double SPAM_toBan_MPSP_Count = 0;
		
		double Banned_MSP_Count = 0;
		double Time_toBan_MSP_Count = 0;
		double SPAM_toBan_MSP_Count = 0;
		
		double Banned_Popular_MPSP_Count = 0;
		double Time_toBan_Popular_MPSP_Count = 0;
		double SPAM_toBan_Popular_MPSP_Count = 0;
		
		double Banned_Popular_MSP_Count = 0;
		double Time_toBan_Popular_MSP_Count = 0;
		double SPAM_toBan_Popular_MSP_Count = 0;
		
		double Banned_Unpopular_MPSP_Count = 0;
		double Time_toBan_Unpopular_MPSP_Count = 0;
		double SPAM_toBan_Unpopular_MPSP_Count = 0;
		
		double Banned_Unpopular_MSP_Count = 0;
		double Time_toBan_Unpopular_MSP_Count = 0;
		double SPAM_toBan_Unpopular_MSP_Count = 0;	
		
		for(SPsNodesDir spNode: banned_spNodes)
			if(spNode.isMalicious())
			{
				sp = (SP_Node) Network.get(spNode.getPosition()).getProtocol(SPPid);
					
				if(sp.isAssociatedWithME())
				{
					ME_Node me = (ME_Node) Network.get(sp.getMEid()).getProtocol(MEPid);

					if(me.isUnpopularSPsColluding())
					{
						Banned_Unpopular_MSP_Count++;
						Time_toBan_Unpopular_MSP_Count = Time_toBan_Unpopular_MSP_Count + sp.getBannedCycle() - sp.getCreationCycle();
						SPAM_toBan_Unpopular_MSP_Count = SPAM_toBan_Unpopular_MSP_Count + sp.getSpamsCount();
					}
					else
					{
						Banned_Popular_MSP_Count++;
						Time_toBan_Popular_MSP_Count = Time_toBan_Popular_MSP_Count + sp.getBannedCycle() - sp.getCreationCycle();
						SPAM_toBan_Popular_MSP_Count = SPAM_toBan_Popular_MSP_Count + sp.getSpamsCount();	
					}
				}
				else
				{
					Banned_MSP_Count++;
					Time_toBan_MSP_Count = Time_toBan_MSP_Count + sp.getBannedCycle() - sp.getCreationCycle();
					SPAM_toBan_MSP_Count = SPAM_toBan_MSP_Count + sp.getSpamsCount();				
				}
			}
		
		for(SPsNodesDir spNode: banned_pspNodes)
			if(spNode.isMalicious())
			{
				sp = (SP_Node) Network.get(spNode.getPosition()).getProtocol(SPPid);
					
				if(sp.isAssociatedWithME())
				{
					ME_Node me = (ME_Node) Network.get(sp.getMEid()).getProtocol(MEPid);

					if(me.isUnpopularSPsColluding())
					{
						Banned_Unpopular_MPSP_Count++;
						Time_toBan_Unpopular_MPSP_Count = Time_toBan_Unpopular_MPSP_Count + sp.getBannedCycle() - sp.getCreationCycle();
						SPAM_toBan_Unpopular_MPSP_Count = SPAM_toBan_Unpopular_MPSP_Count + sp.getSpamsCount();
					}
					else
					{
						Banned_Popular_MPSP_Count++;
						Time_toBan_Popular_MPSP_Count = Time_toBan_Popular_MPSP_Count + sp.getBannedCycle() - sp.getCreationCycle();
						SPAM_toBan_Popular_MPSP_Count = SPAM_toBan_Popular_MPSP_Count + sp.getSpamsCount();
					}
				}
				else
				{
					Banned_MPSP_Count++;
					Time_toBan_MPSP_Count = Time_toBan_MPSP_Count + sp.getBannedCycle() - sp.getCreationCycle();
					SPAM_toBan_MPSP_Count = SPAM_toBan_MPSP_Count + sp.getSpamsCount();				
				}
			}
		
		
		if(DeployDGU)
		{
			for(SPsNodesDir spNode: spNodes)
				if(spNode.isMalicious())
				{
					sp = (SP_Node) Network.get(spNode.getPosition()).getProtocol(SPPid);
						
					if(sp.isAssociatedWithME() && sp.isEnabledStrictDGU())
					{
						ME_Node me = (ME_Node) Network.get(sp.getMEid()).getProtocol(MEPid);
	
						if(me.isUnpopularSPsColluding())
						{
							Banned_Unpopular_MSP_Count++;
							Time_toBan_Unpopular_MSP_Count = Time_toBan_Unpopular_MSP_Count + sp.getEnabledStrictDGUCycle() - sp.getCreationCycle();
							SPAM_toBan_Unpopular_MSP_Count = SPAM_toBan_Unpopular_MSP_Count + sp.getSpamsCount();
						}
						else
						{
							Banned_Popular_MSP_Count++;
							Time_toBan_Popular_MSP_Count = Time_toBan_Popular_MSP_Count + sp.getEnabledStrictDGUCycle() - sp.getCreationCycle();
							SPAM_toBan_Popular_MSP_Count = SPAM_toBan_Popular_MSP_Count + sp.getSpamsCount();						
						}
					}
					else if(sp.isInstalledDGU())
					{
						Banned_MSP_Count++;
						Time_toBan_MSP_Count = Time_toBan_MSP_Count + sp.getInstalledDGUCycle() - sp.getCreationCycle();
						SPAM_toBan_MSP_Count = SPAM_toBan_MSP_Count + sp.getSpamsCount();				
					}
				}
			
			for(SPsNodesDir spNode: pspNodes)
				if(spNode.isMalicious())
				{
					sp = (SP_Node) Network.get(spNode.getPosition()).getProtocol(SPPid);
						
					if(sp.isAssociatedWithME() && sp.isEnabledStrictDGU())
					{
						ME_Node me = (ME_Node) Network.get(sp.getMEid()).getProtocol(MEPid);
	
						if(me.isUnpopularSPsColluding())
						{
							Banned_Unpopular_MPSP_Count++;
							Time_toBan_Unpopular_MPSP_Count = Time_toBan_Unpopular_MPSP_Count + sp.getEnabledStrictDGUCycle() - sp.getCreationCycle();
							SPAM_toBan_Unpopular_MPSP_Count = SPAM_toBan_Unpopular_MPSP_Count + sp.getSpamsCount();
						}
						else
						{
							Banned_Popular_MPSP_Count++;
							Time_toBan_Popular_MPSP_Count = Time_toBan_Popular_MPSP_Count + sp.getEnabledStrictDGUCycle() - sp.getCreationCycle();
							SPAM_toBan_Popular_MPSP_Count = SPAM_toBan_Popular_MPSP_Count + sp.getSpamsCount();				
						}
					}
					else if(sp.isInstalledDGU())
					{
						Banned_MPSP_Count++;
						Time_toBan_MPSP_Count = Time_toBan_MPSP_Count + sp.getInstalledDGUCycle() - sp.getCreationCycle();
						SPAM_toBan_MPSP_Count = SPAM_toBan_MPSP_Count + sp.getSpamsCount();				
					}
				}
		}
		
		switch(cycle){
		
		case 125:
			
			AvgTimeToBan_MPSP_at_125 = Time_toBan_MPSP_Count/Banned_MPSP_Count;
			AvgTimeToBan_MSP_at_125 = Time_toBan_MSP_Count/Banned_MSP_Count;
			AvgTimeToBan_Popular_MPSP_at_125 = Time_toBan_Popular_MPSP_Count/Banned_Popular_MPSP_Count;
			AvgTimeToBan_Popular_MSP_at_125 = Time_toBan_Popular_MSP_Count/Banned_Popular_MSP_Count;
			AvgTimeToBan_Unpopular_MPSP_at_125 = Time_toBan_Unpopular_MPSP_Count/Banned_Unpopular_MPSP_Count;
			AvgTimeToBan_Unpopular_MSP_at_125 = Time_toBan_Unpopular_MSP_Count/Banned_Unpopular_MSP_Count;
			
			AvgSPAMToBan_MPSP_at_125 = SPAM_toBan_MPSP_Count/Banned_MPSP_Count;
			AvgSPAMToBan_MSP_at_125 = SPAM_toBan_MSP_Count/Banned_MSP_Count;
			AvgSPAMToBan_Popular_MPSP_at_125 = SPAM_toBan_Popular_MPSP_Count/Banned_Popular_MPSP_Count;
			AvgSPAMToBan_Popular_MSP_at_125 = SPAM_toBan_Popular_MSP_Count/Banned_Popular_MSP_Count;
			AvgSPAMToBan_Unpopular_MPSP_at_125 = SPAM_toBan_Unpopular_MPSP_Count/Banned_Unpopular_MPSP_Count;
			AvgSPAMToBan_Unpopular_MSP_at_125 = SPAM_toBan_Unpopular_MSP_Count/Banned_Unpopular_MSP_Count;
			
			break;
			
		case 250:

			AvgTimeToBan_MPSP_at_250 = Time_toBan_MPSP_Count/Banned_MPSP_Count;
			AvgTimeToBan_MSP_at_250 = Time_toBan_MSP_Count/Banned_MSP_Count;
			AvgTimeToBan_Popular_MPSP_at_250 = Time_toBan_Popular_MPSP_Count/Banned_Popular_MPSP_Count;
			AvgTimeToBan_Popular_MSP_at_250 = Time_toBan_Popular_MSP_Count/Banned_Popular_MSP_Count;
			AvgTimeToBan_Unpopular_MPSP_at_250 = Time_toBan_Unpopular_MPSP_Count/Banned_Unpopular_MPSP_Count;
			AvgTimeToBan_Unpopular_MSP_at_250 = Time_toBan_Unpopular_MSP_Count/Banned_Unpopular_MSP_Count;
			
			AvgSPAMToBan_MPSP_at_250 = SPAM_toBan_MPSP_Count/Banned_MPSP_Count;
			AvgSPAMToBan_MSP_at_250 = SPAM_toBan_MSP_Count/Banned_MSP_Count;
			AvgSPAMToBan_Popular_MPSP_at_250 = SPAM_toBan_Popular_MPSP_Count/Banned_Popular_MPSP_Count;
			AvgSPAMToBan_Popular_MSP_at_250 = SPAM_toBan_Popular_MSP_Count/Banned_Popular_MSP_Count;
			AvgSPAMToBan_Unpopular_MPSP_at_250 = SPAM_toBan_Unpopular_MPSP_Count/Banned_Unpopular_MPSP_Count;
			AvgSPAMToBan_Unpopular_MSP_at_250 = SPAM_toBan_Unpopular_MSP_Count/Banned_Unpopular_MSP_Count;
			
			break;
			
		case 375:

			AvgTimeToBan_MPSP_at_375 = Time_toBan_MPSP_Count/Banned_MPSP_Count;
			AvgTimeToBan_MSP_at_375 = Time_toBan_MSP_Count/Banned_MSP_Count;
			AvgTimeToBan_Popular_MPSP_at_375 = Time_toBan_Popular_MPSP_Count/Banned_Popular_MPSP_Count;
			AvgTimeToBan_Popular_MSP_at_375 = Time_toBan_Popular_MSP_Count/Banned_Popular_MSP_Count;
			AvgTimeToBan_Unpopular_MPSP_at_375 = Time_toBan_Unpopular_MPSP_Count/Banned_Unpopular_MPSP_Count;
			AvgTimeToBan_Unpopular_MSP_at_375 = Time_toBan_Unpopular_MSP_Count/Banned_Unpopular_MSP_Count;
			
			AvgSPAMToBan_MPSP_at_375 = SPAM_toBan_MPSP_Count/Banned_MPSP_Count;
			AvgSPAMToBan_MSP_at_375 = SPAM_toBan_MSP_Count/Banned_MSP_Count;
			AvgSPAMToBan_Popular_MPSP_at_375 = SPAM_toBan_Popular_MPSP_Count/Banned_Popular_MPSP_Count;
			AvgSPAMToBan_Popular_MSP_at_375 = SPAM_toBan_Popular_MSP_Count/Banned_Popular_MSP_Count;
			AvgSPAMToBan_Unpopular_MPSP_at_375 = SPAM_toBan_Unpopular_MPSP_Count/Banned_Unpopular_MPSP_Count;
			AvgSPAMToBan_Unpopular_MSP_at_375 = SPAM_toBan_Unpopular_MSP_Count/Banned_Unpopular_MSP_Count;
			
			break;
			
		case 500:

			AvgTimeToBan_MPSP_at_500 = Time_toBan_MPSP_Count/Banned_MPSP_Count;
			AvgTimeToBan_MSP_at_500 = Time_toBan_MSP_Count/Banned_MSP_Count;
			AvgTimeToBan_Popular_MPSP_at_500 = Time_toBan_Popular_MPSP_Count/Banned_Popular_MPSP_Count;
			AvgTimeToBan_Popular_MSP_at_500 = Time_toBan_Popular_MSP_Count/Banned_Popular_MSP_Count;
			AvgTimeToBan_Unpopular_MPSP_at_500 = Time_toBan_Unpopular_MPSP_Count/Banned_Unpopular_MPSP_Count;
			AvgTimeToBan_Unpopular_MSP_at_500 = Time_toBan_Unpopular_MSP_Count/Banned_Unpopular_MSP_Count;
			
			AvgSPAMToBan_MPSP_at_500 = SPAM_toBan_MPSP_Count/Banned_MPSP_Count;
			AvgSPAMToBan_MSP_at_500 = SPAM_toBan_MSP_Count/Banned_MSP_Count;
			AvgSPAMToBan_Popular_MPSP_at_500 = SPAM_toBan_Popular_MPSP_Count/Banned_Popular_MPSP_Count;
			AvgSPAMToBan_Popular_MSP_at_500 = SPAM_toBan_Popular_MSP_Count/Banned_Popular_MSP_Count;
			AvgSPAMToBan_Unpopular_MPSP_at_500 = SPAM_toBan_Unpopular_MPSP_Count/Banned_Unpopular_MPSP_Count;
			AvgSPAMToBan_Unpopular_MSP_at_500 = SPAM_toBan_Unpopular_MSP_Count/Banned_Unpopular_MSP_Count;
			
			break;
		}
		
	}

	public void updateUndetectedMSPs(int cycle)
	{
		SP_Node sp;
		
		double UndetectedMPSP_Count = 0;
		
		double UndetectedMSP_Count = 0;
		
		double Popular_UndetectedMPSP_Count = 0;
		
		double Popular_UndetectedMSP_Count = 0;
		
		double Unpopular_UndetectedMPSP_Count = 0;
		
		double Unpopular_UndetectedMSP_Count = 0;
		
		
		double DetectedMPSP_Count = 0;
		
		double DetectedMSP_Count = 0;
		
		double Popular_DetectedMPSP_Count = 0;
		
		double Popular_DetectedMSP_Count = 0;
		
		double Unpopular_DetectedMPSP_Count = 0;
		
		double Unpopular_DetectedMSP_Count = 0;	
		
		for(SPsNodesDir spNode: spNodes)
			if(spNode.isMalicious())
			{
				sp = (SP_Node) Network.get(spNode.getPosition()).getProtocol(SPPid);
					
				if(sp.isAssociatedWithME() && !sp.isEnabledStrictDGU())
				{
					ME_Node me = (ME_Node) Network.get(sp.getMEid()).getProtocol(MEPid);

					if(me.isUnpopularSPsColluding())
					{
						Unpopular_UndetectedMSP_Count++;
					}
					else
					{
						Popular_UndetectedMSP_Count++;
					}
				}
				
				else if(sp.isAssociatedWithME() && sp.isEnabledStrictDGU())
				{
					ME_Node me = (ME_Node) Network.get(sp.getMEid()).getProtocol(MEPid);

					if(me.isUnpopularSPsColluding())
					{
						Unpopular_DetectedMSP_Count++;
					}
					else
					{
						Popular_DetectedMSP_Count++;
					}
				}
				
				else if(!sp.isAssociatedWithME() && !sp.isInstalledDGU())
				{
					UndetectedMSP_Count++;
				}
				
				else if(!sp.isAssociatedWithME() && sp.isInstalledDGU())
				{
					DetectedMSP_Count++;
				}
				
			}
		
		for(SPsNodesDir spNode: pspNodes)
			if(spNode.isMalicious())
			{
				sp = (SP_Node) Network.get(spNode.getPosition()).getProtocol(SPPid);
				
				if(sp.isAssociatedWithME() && !sp.isEnabledStrictDGU())
				{
					ME_Node me = (ME_Node) Network.get(sp.getMEid()).getProtocol(MEPid);

					if(me.isUnpopularSPsColluding())
					{
						Unpopular_UndetectedMPSP_Count++;
					}
					else
					{
						Popular_UndetectedMPSP_Count++;
					}
				}
				
				else if(sp.isAssociatedWithME() && sp.isEnabledStrictDGU())
				{
					ME_Node me = (ME_Node) Network.get(sp.getMEid()).getProtocol(MEPid);

					if(me.isUnpopularSPsColluding())
					{
						Unpopular_DetectedMPSP_Count++;
					}
					else
					{
						Popular_DetectedMPSP_Count++;
					}
				}
				
				else if(!sp.isAssociatedWithME() && !sp.isInstalledDGU())
				{
					UndetectedMPSP_Count++;
				}
				
				else if(!sp.isAssociatedWithME() && sp.isInstalledDGU())
				{
					DetectedMPSP_Count++;
				}
				
			}

		
		switch(cycle){
		
		case 125:
			PCT_Undetected_MSPs_at_125 = UndetectedMSP_Count*100/(UndetectedMSP_Count+bannedMSPsCount+DetectedMSP_Count);
			PCT_Undetected_MPSPs_at_125 = UndetectedMPSP_Count*100/(UndetectedMPSP_Count+bannedMPSPsCount+DetectedMPSP_Count);
			PCT_Undetected_Popular_MSPs_at_125 = Popular_UndetectedMSP_Count*100/(Popular_UndetectedMSP_Count+bannedPopularMSPsCount+Popular_DetectedMSP_Count);
			PCT_Undetected_Popular_MPSPs_at_125 = Popular_UndetectedMPSP_Count*100/(Popular_UndetectedMPSP_Count+bannedPopularMPSPsCount +Popular_DetectedMPSP_Count);
			PCT_Undetected_Unpopular_MSPs_at_125 = Unpopular_UndetectedMSP_Count*100/(Unpopular_UndetectedMSP_Count+bannedUnpopularMSPsCount+Unpopular_DetectedMSP_Count);
			PCT_Undetected_Unpopular_MPSPs_at_125 = Unpopular_UndetectedMPSP_Count*100/(Unpopular_UndetectedMPSP_Count+bannedUnpopularMPSPsCount+Unpopular_DetectedMPSP_Count);
			break;
			
		case 250:
			PCT_Undetected_MSPs_at_250 = UndetectedMSP_Count*100/(UndetectedMSP_Count+bannedMSPsCount+DetectedMSP_Count);
			PCT_Undetected_MPSPs_at_250 = UndetectedMPSP_Count*100/(UndetectedMPSP_Count+bannedMPSPsCount+DetectedMPSP_Count);
			PCT_Undetected_Popular_MSPs_at_250 = Popular_UndetectedMSP_Count*100/(Popular_UndetectedMSP_Count+bannedPopularMSPsCount+Popular_DetectedMSP_Count);
			PCT_Undetected_Popular_MPSPs_at_250 = Popular_UndetectedMPSP_Count*100/(Popular_UndetectedMPSP_Count+bannedPopularMPSPsCount +Popular_DetectedMPSP_Count);
			PCT_Undetected_Unpopular_MSPs_at_250 = Unpopular_UndetectedMSP_Count*100/(Unpopular_UndetectedMSP_Count+bannedUnpopularMSPsCount+Unpopular_DetectedMSP_Count);
			PCT_Undetected_Unpopular_MPSPs_at_250 = Unpopular_UndetectedMPSP_Count*100/(Unpopular_UndetectedMPSP_Count+bannedUnpopularMPSPsCount+Unpopular_DetectedMPSP_Count);
			break;
			
		case 375:
			PCT_Undetected_MSPs_at_375 = UndetectedMSP_Count*100/(UndetectedMSP_Count+bannedMSPsCount+DetectedMSP_Count);
			PCT_Undetected_MPSPs_at_375 = UndetectedMPSP_Count*100/(UndetectedMPSP_Count+bannedMPSPsCount+DetectedMPSP_Count);
			PCT_Undetected_Popular_MSPs_at_375 = Popular_UndetectedMSP_Count*100/(Popular_UndetectedMSP_Count+bannedPopularMSPsCount+Popular_DetectedMSP_Count);
			PCT_Undetected_Popular_MPSPs_at_375 = Popular_UndetectedMPSP_Count*100/(Popular_UndetectedMPSP_Count+bannedPopularMPSPsCount +Popular_DetectedMPSP_Count);
			PCT_Undetected_Unpopular_MSPs_at_375 = Unpopular_UndetectedMSP_Count*100/(Unpopular_UndetectedMSP_Count+bannedUnpopularMSPsCount+Unpopular_DetectedMSP_Count);
			PCT_Undetected_Unpopular_MPSPs_at_375 = Unpopular_UndetectedMPSP_Count*100/(Unpopular_UndetectedMPSP_Count+bannedUnpopularMPSPsCount+Unpopular_DetectedMPSP_Count);
			break;
			
		case 500:
			PCT_Undetected_MSPs_at_500 = UndetectedMSP_Count*100/(UndetectedMSP_Count+bannedMSPsCount+DetectedMSP_Count);
			PCT_Undetected_MPSPs_at_500 = UndetectedMPSP_Count*100/(UndetectedMPSP_Count+bannedMPSPsCount+DetectedMPSP_Count);
			PCT_Undetected_Popular_MSPs_at_500 = Popular_UndetectedMSP_Count*100/(Popular_UndetectedMSP_Count+bannedPopularMSPsCount+Popular_DetectedMSP_Count);
			PCT_Undetected_Popular_MPSPs_at_500 = Popular_UndetectedMPSP_Count*100/(Popular_UndetectedMPSP_Count+bannedPopularMPSPsCount +Popular_DetectedMPSP_Count);
			PCT_Undetected_Unpopular_MSPs_at_500 = Unpopular_UndetectedMSP_Count*100/(Unpopular_UndetectedMSP_Count+bannedUnpopularMSPsCount+Unpopular_DetectedMSP_Count);
			PCT_Undetected_Unpopular_MPSPs_at_500 = Unpopular_UndetectedMPSP_Count*100/(Unpopular_UndetectedMPSP_Count+bannedUnpopularMPSPsCount+Unpopular_DetectedMPSP_Count);
			break;
		}
		
	}
	
	public void updateMSPsSPAMsCounters(int cycle)
	{
		SP_Node sp;
		
		double SPAM_Count = 0;
		double MPSP_SPAM_Count = 0;
		double MSP_SPAM_Count = 0;
		double Popular_SPAM_Count = 0;
		double Unpopular_SPAM_Count = 0;
		
		for(SPsNodesDir spNode: spNodes)
			if(spNode.isMalicious())
			{
				sp = (SP_Node) Network.get(spNode.getPosition()).getProtocol(SPPid);
				SPAM_Count = SPAM_Count + sp.getSpamsCount();
				
				if(sp.isAssociatedWithME())
				{
					ME_Node me = (ME_Node) Network.get(sp.getMEid()).getProtocol(MEPid);

					if(me.isUnpopularSPsColluding())
						Unpopular_SPAM_Count = Unpopular_SPAM_Count + sp.getSpamsCount();
					
					else
						Popular_SPAM_Count = Popular_SPAM_Count + sp.getSpamsCount();
				}
				else
					MSP_SPAM_Count = MSP_SPAM_Count + sp.getSpamsCount();
				
			}
		
		
		for(SPsNodesDir spNode: pspNodes)
			if(spNode.isMalicious())
			{
				sp = (SP_Node) Network.get(spNode.getPosition()).getProtocol(SPPid);
				SPAM_Count = SPAM_Count + sp.getSpamsCount();

				if(sp.isAssociatedWithME())
				{
					ME_Node me = (ME_Node) Network.get(sp.getMEid()).getProtocol(MEPid);

					if(me.isUnpopularSPsColluding())
						Unpopular_SPAM_Count = Unpopular_SPAM_Count + sp.getSpamsCount();
					
					else
						Popular_SPAM_Count = Popular_SPAM_Count + sp.getSpamsCount();
				}
				else
					MPSP_SPAM_Count = MPSP_SPAM_Count + sp.getSpamsCount();
				
			}
		
		switch(cycle){
		
		case 125:
			SPAM125Count = SPAM_Count;
			MPSP125SPAMcount = MPSP_SPAM_Count;
			MSP125SPAMcount = MSP_SPAM_Count;
			Unpopular125SPAMcount = Unpopular_SPAM_Count;
			Popular125SPAMcount = Popular_SPAM_Count;
			break;
			
		case 250:
			SPAM250Count = SPAM_Count;
			MPSP250SPAMcount = MPSP_SPAM_Count;
			MSP250SPAMcount = MSP_SPAM_Count;
			Unpopular250SPAMcount = Unpopular_SPAM_Count;
			Popular250SPAMcount = Popular_SPAM_Count;
			break;
			
		case 375:
			SPAM375Count = SPAM_Count;
			MPSP375SPAMcount = MPSP_SPAM_Count;
			MSP375SPAMcount = MSP_SPAM_Count;
			Unpopular375SPAMcount = Unpopular_SPAM_Count;
			Popular375SPAMcount = Popular_SPAM_Count;
			break;
			
		case 500:
			SPAM500Count = SPAM_Count;
			MPSP500SPAMcount = MPSP_SPAM_Count;
			MSP500SPAMcount = MSP_SPAM_Count;
			Unpopular500SPAMcount = Unpopular_SPAM_Count;
			Popular500SPAMcount = Popular_SPAM_Count;
			break;
		}
		
	}
	
	public void addNodeDir(int nodePos, int nodeType, double SPRank)
	{
		if(nodeType == 3)
		{
			User_Node newNode = (User_Node) Network.get(nodePos).getProtocol(UserPid);
			
			UsersNodesDir uDir = new UsersNodesDir();
			uDir.setPosition(nodePos);
			
			if(newNode.isSTagent()) 
				uDir.setType(2);
			else if(newNode.isGTagent()) 
				uDir.setType(3);
			else
				uDir.setType(1);
			
			//if(CommonState.getTime() >= 1) System.out.println("New node: nodePos: "+nodePos+" nodeType: "+uDir.getType());
			
			this.uNodes.add(uDir);
		}
		else if(nodeType == 4)
		{
			//System.out.println("New node: nodePos: "+nodePos);
			//Node n = Network.get(nodePos);
			//System.out.println(n.getIndex());
			SP_Node newNode = (SP_Node) Network.get(nodePos).getProtocol(SPPid);
			
			SPsNodesDir spDir = new SPsNodesDir();
			spDir.setPosition(nodePos);
			spDir.setRank(SPRank);
			spDir.setUsefulness(newNode.getUsefulness());
			spDir.setMalicious(newNode.isMalicious());
			
			if(newNode.getUsefulness() < this.usefulnessMIN)
			{
				this.spNodes.add(spDir);
			}
			else
			{
				this.pspNodes.add(spDir);
			}

		}
	}

	public void removeMaliciousSP(int SPid, int detectorType)
	{
		boolean SPFound = false;
		
		for(SPsNodesDir spDir: pspNodes)
		{
			if(spDir.getPosition() == SPid)
			{	
				SP_Node sp = (SP_Node) Network.get(SPid).getProtocol(SPPid);
				
				if(!sp.isMalicious())
				{
					if(DeployDGU)
					{
						if(sp.isInstalledDGU())
							this.bannedInnocentPSPCount_refusedEnableStrictDGU++;
						else
							this.bannedInnocentPSPCount_refusedInstallDGU++;
					}
					
					this.bannedInnocentPSPCount++;
				}
				else if(sp.isAssociatedWithME())
				{
					ME_Node me = (ME_Node) Network.get(sp.getMEid()).getProtocol(MEPid);
					
					if(me.isUnpopularSPsColluding())
					{
						this.bannedUnpopularMPSPsCount++;
					}
					else
					{
						this.bannedPopularMPSPsCount++;
					}
				}
				else
					this.bannedMPSPsCount++;
				
				banned_pspNodes.add(spDir.copy());
				
				pspNodes.remove(spDir);
				SPFound = true;
				break;
			}
		}

		if(!SPFound)
		{
			for(SPsNodesDir spDir: spNodes)
			{
				if(spDir.getPosition() == SPid)
				{
					SP_Node sp = (SP_Node) Network.get(SPid).getProtocol(SPPid);
					
					if(!sp.isMalicious())
					{
						if(DeployDGU)
						{
							if(sp.isInstalledDGU())
								this.bannedInnocentSPCount_refusedEnableStrictDGU++;
							else
								this.bannedInnocentSPCount_refusedInstallDGU++;
						}
						
						this.bannedInnocentSPCount++;
					}
					
					else if(sp.isAssociatedWithME())
					{
						ME_Node me = (ME_Node) Network.get(sp.getMEid()).getProtocol(MEPid);
						
						if(me.isUnpopularSPsColluding())
						{
							this.bannedUnpopularMSPsCount++;
						}
						else
						{
							this.bannedPopularMSPsCount++;
						}
					}
					else
						this.bannedMSPsCount++;
				
					banned_spNodes.add(spDir.copy());
					
					spNodes.remove(spDir);
					break;
				}
			}
		}
		
		SP_Node bannedSP = (SP_Node) Network.get(SPid).getProtocol(SPPid);
		bannedSP.setBanned(true);
		bannedSP.setBanDetectorType(detectorType);
		bannedSP.terminateAllPartnerships();
	}
	
	public boolean getService(ServiceRequest serviceRequest)
	{
		double minRank = serviceRequest.getMinRank();
		double minUsefulness = serviceRequest.getMinUsefulness();
		Credential credential = serviceRequest.getCredential();
		boolean specificSP = serviceRequest.isSpecificSP();
			
		//TODO i is the number of trials to fulfill the User desire , try to optimize it.
		SPsNodesDir spDir = new SPsNodesDir();
		
		boolean spFound = false;
		
		if(!specificSP)
		{
			for(int i =0; i < 3; i++)
			{	
				if(minUsefulness >= this.usefulnessMIN && pspNodes.size() < 1)
					break;
				else if(minUsefulness < this.usefulnessMIN)
					spDir = spNodes.get(randInt(0, spNodes.size()));
				else
					spDir = pspNodes.get(randInt(0, pspNodes.size()));
				
				if(spDir.getRank() >= minRank && spDir.getUsefulness() >= minUsefulness)
				{
					spFound = true;
					break;
				}
			}
		}
		else
		{
			for(int i = 0; i < pspNodes.size(); i++)
			{
				spDir = pspNodes.get(i);
				
				if(spDir.getPosition() == serviceRequest.getSPid())
				{
					spFound = true;
					break;
				}
			}
			if(!spFound)
			{
				for(int i = 0; i < spNodes.size(); i++)
				{
					spDir = spNodes.get(i);
					
					if(spDir.getPosition() == serviceRequest.getSPid())
					{
						spFound = true;
						break;
					}
				}
			}
			
			/*
			if(!spFound)
			{
				System.out.println("!spFound: "+serviceRequest.getSPid());
				
				for(SPsNodesDir spNode: banned_pspNodes)
					System.out.println("pspNode: "+spNode.getPosition());
				
				for(SPsNodesDir spNode: banned_spNodes)
					System.out.println("spNode: "+spNode.getPosition());
			}
			*/
		}
		
		if(spFound)
		{
			SP_Node spNode = (SP_Node) Network.get(spDir.getPosition()).getProtocol(SPPid);
			
			List<Partner> unsafePartners = spNode.addCredential(credential);
			//boolean sharedWithUnsafePartners = unsafePartners.size() > 0;
			
			//if(!spNode.isInstalledDGU() || (spNode.isInstalledDGU() && sharedWithUnsafePartners))
			{
				UsersNodesDir userNode = this.get_uNode(credential.getNodePosition());
	
				if(userNode.getPosition() == credential.getNodePosition())
				{
					CredentialLogs credentialLogs = this.getCredentialLogs(userNode, credential.getId());
					
					if( credentialLogs.getCredential().getId() != credential.getId() )
					{
						credentialLogs = new CredentialLogs(credential);
						userNode.getCredentialLogs().add(credentialLogs);
					}

					if(!spNode.isEnabledStrictDGU())
						credentialLogs.addLog(spDir.getPosition(), spDir.getUsefulness(), false);
					
					if(spNode.isInstalledDGU())
					for(Partner partner: unsafePartners)
					{
						double roughUsefulness = -1;
						if(partner.isPsp()) roughUsefulness = this.usefulnessMIN;
						else roughUsefulness = this.usefulnessMIN - 1;
						
						credentialLogs.addLog(partner.getPartnerID(), roughUsefulness, true);
					}
				
					User_Node user = (User_Node) Network.get(userNode.getPosition()).getProtocol(UserPid);
					
					if(spDir.getUsefulness() >= this.usefulnessMIN)
					{
						boolean pspFound = false;
						for(CaseSPs i : user.getPSPsInteractedWith())
							if(i.getSPid() == spDir.getPosition())
							{
								pspFound = true;
								i.setCycle(CommonState.getIntTime());
								break;
							}
						
						if(!pspFound)
							user.getPSPsInteractedWith().add(new CaseSPs(spDir.getPosition(), CommonState.getIntTime()));
					}
					
					if(spNode.isInstalledDGU())
					for(Partner partner: unsafePartners)
					{	
						boolean partnerFound = false;
						
						if(!partner.isPsp())
							continue;
						
						for(CaseSPs i : user.getPSPsInteractedWith())
						{
							if(partner.getPartnerID() == i.getSPid())
							{
								partnerFound = true;
								i.setCycle(CommonState.getIntTime());
								break;
							}
						}
						
						if(!partnerFound)
							user.getPSPsInteractedWith().add(new CaseSPs(partner.getPartnerID(), CommonState.getIntTime()));
					}
				}
			}
			
			return true;
		}
		else
			return false;
	}
	
	public SPsNodesDir getRandSP(boolean PSP)
	{
		if(PSP)
		{
			if(pspNodes.size() >= 1)
			{
				int i = this.randInt(0, pspNodes.size());
				return pspNodes.get(i);
			}
			else
				return null;
			
		}
		else
		{
			if(spNodes.size() >= 1)
			{
				int i = this.randInt(0, spNodes.size());
				return spNodes.get(i);
			}
			else
				return null;
		}
	}
	
	public void sendSpam(Credential credential)
	{
		User_Node user = (User_Node) Network.get(credential.getNodePosition()).getProtocol(UserPid);
		if(user.recieveSpam(credential))
		{
			//if(user.isSTagent() && user.getSTimportantCredential().getId() == credential.getId())
				//System.out.println("STagent: "+user.getNodePos()+" got SPAM!");
			reportSpam(credential, true);
		}
	}
	
	public SpamReport reportSpam(Credential credential, boolean recordSPAM)
	{
		//System.out.println("Report SPAM! UserPos: "+credential.getNodePosition());
		
		Auditor_Node auditor = (Auditor_Node) Network.get(2).getProtocol(AuditorPid);
		CaseLogs caseLogs = new CaseLogs(credential);
		caseLogs.setCreatedOn(CommonState.getTime());
		
		UsersNodesDir uNode = this.get_uNode(credential.getNodePosition());
		CredentialLogs credentialLogs = this.getCredentialLogs(uNode, credential.getId());
		Log selectedLog;
		
		//TODO optimize array size 
		List<Log> tempLogs = new ArrayList<Log>(20);
		
		List<Auditor_SPsNodesDir> caseSps = new ArrayList<Auditor_SPsNodesDir>(20);
		boolean spFound;
		
		for(int i = credentialLogs.logs.size()-1; i >= 0; i--)
		{
			selectedLog = credentialLogs.logs.get(i);
			if(selectedLog.getSpPosition() == 2)
				break;
			tempLogs.add(selectedLog);
			
			spFound = false;
			for(Auditor_SPsNodesDir spNode: caseSps)
			{
				if(spNode.getPosition() == selectedLog.getSpPosition())
				{
					spFound = true;
					break;
				}
			}
			Auditor_SPsNodesDir spNode = new Auditor_SPsNodesDir();
			spNode.setPosition(selectedLog.getSpPosition());
			spNode.setMalicious(false);
			spNode.setSpUsefulness(selectedLog.getSpUsefulness());
			if(!spFound) caseSps.add(spNode);
		}
		
		caseLogs.setCaseSps(caseSps);
		
		if(credentialLogs.getLogs().size() == 0)
			System.out.println("IDP: User:"+ uNode.getPosition()+" credentialLogs.size "+credentialLogs.getLogs().size()+" User Type: "+uNode.getType()+" isSTimportant() "+credential.isSTimportant());
		
		for(int i = tempLogs.size()-1; i >= 0; i--)
		{
			caseLogs.getLogs().add(tempLogs.get(i));
		}
		
		User_Node user = (User_Node) Network.get(uNode.getPosition()).getProtocol(UserPid);
		
		if(recordSPAM)
		{
			auditor.reportSpam(caseLogs, user.getPSPsInteractedWith());
		}
		
		Log newLog = new Log(credential.getNodePosition(), CommonState.getTime(), credential.getId(), 2, -1, false);
		credentialLogs.logs.add(newLog);
		
		SpamReport spamReport = new SpamReport();
		spamReport.setCaseLogs(caseLogs);
		spamReport.setPSPsInteractedWith(user.getPSPsInteractedWith());
		return spamReport;
	}
	
	public CredentialLogs getCredetialLogs(int userID, int credentialID)
	{
		return getCredentialLogs(get_uNode(userID), credentialID);
	}
	
	private UsersNodesDir get_uNode(int userPosition)
	{
		UsersNodesDir uNode = new UsersNodesDir();
		
		UsersNodesDir selected_uNode;
		for(int i = 0; i < uNodes.size(); i++)
		{
			selected_uNode = uNodes.get(i);
			if(selected_uNode.getPosition() == userPosition)
			{
				uNode = selected_uNode;
				break;
			}
		}
		
		return uNode;
	}
	
	private CredentialLogs getCredentialLogs(UsersNodesDir userNode,int credentialID)
	{
		CredentialLogs resultCredentialLogs = new CredentialLogs();
		
		CredentialLogs selectedCredentialLogs;
		for(int i = 0; i < userNode.getCredentialLogs().size(); i++)
		{
			selectedCredentialLogs = userNode.getCredentialLogs().get(i);
			if(selectedCredentialLogs.getCredential().getId() == credentialID)
			{
				resultCredentialLogs = selectedCredentialLogs;
				break;
			}
		}
		
		return resultCredentialLogs;
	}
	
	public void updateRankings(List<TGRRank> newRanks)
	{
		TGRRank tgrRank;
		SPsNodesDir spNode;
		for(int i = 0; i < newRanks.size(); i++)
		{
			tgrRank = newRanks.get(i);
			for(int j = 0; j < spNodes.size(); j++)
			{
				spNode = spNodes.get(j);
				if(spNode.getPosition() == tgrRank.getSPid())
				{
					spNode.setRank(tgrRank.getRank());
					break;
				}
			}
		}
	}
	
	public List<UsersNodesDir> getUNodesDir()
	{
		return uNodes;
	}

	public List<SPsNodesDir> getSpNodes() {
		return spNodes;
	}
	public List<SPsNodesDir> getPspNodes() {
		return pspNodes;
	}

	public double getSPAM125Count() {
		return SPAM125Count;
	}

	public void setSPAM125Count(double sPAM125Count) {
		SPAM125Count = sPAM125Count;
	}

	public double getMPSP125SPAMcount() {
		return MPSP125SPAMcount;
	}

	public void setMPSP125SPAMcount(double mPSP125SPAMcount) {
		MPSP125SPAMcount = mPSP125SPAMcount;
	}

	public double getMSP125SPAMcount() {
		return MSP125SPAMcount;
	}

	public void setMSP125SPAMcount(double mSP125SPAMcount) {
		MSP125SPAMcount = mSP125SPAMcount;
	}

	public double getPopular125SPAMcount() {
		return Popular125SPAMcount;
	}

	public void setPopular125SPAMcount(double popular125spaMcount) {
		Popular125SPAMcount = popular125spaMcount;
	}

	public double getUnpopular125SPAMcount() {
		return Unpopular125SPAMcount;
	}

	public void setUnpopular125SPAMcount(double unpopular125spaMcount) {
		Unpopular125SPAMcount = unpopular125spaMcount;
	}

	public double getSPAM250Count() {
		return SPAM250Count;
	}

	public void setSPAM250Count(double sPAM250Count) {
		SPAM250Count = sPAM250Count;
	}

	public double getMPSP250SPAMcount() {
		return MPSP250SPAMcount;
	}

	public void setMPSP250SPAMcount(double mPSP250SPAMcount) {
		MPSP250SPAMcount = mPSP250SPAMcount;
	}

	public double getMSP250SPAMcount() {
		return MSP250SPAMcount;
	}

	public void setMSP250SPAMcount(double mSP250SPAMcount) {
		MSP250SPAMcount = mSP250SPAMcount;
	}

	public double getPopular250SPAMcount() {
		return Popular250SPAMcount;
	}

	public void setPopular250SPAMcount(double popular250spaMcount) {
		Popular250SPAMcount = popular250spaMcount;
	}

	public double getUnpopular250SPAMcount() {
		return Unpopular250SPAMcount;
	}

	public void setUnpopular250SPAMcount(double unpopular250spaMcount) {
		Unpopular250SPAMcount = unpopular250spaMcount;
	}

	public double getSPAM375Count() {
		return SPAM375Count;
	}

	public void setSPAM375Count(double sPAM375Count) {
		SPAM375Count = sPAM375Count;
	}

	public double getMPSP375SPAMcount() {
		return MPSP375SPAMcount;
	}

	public void setMPSP375SPAMcount(double mPSP375SPAMcount) {
		MPSP375SPAMcount = mPSP375SPAMcount;
	}

	public double getMSP375SPAMcount() {
		return MSP375SPAMcount;
	}

	public void setMSP375SPAMcount(double mSP375SPAMcount) {
		MSP375SPAMcount = mSP375SPAMcount;
	}

	public double getPopular375SPAMcount() {
		return Popular375SPAMcount;
	}

	public void setPopular375SPAMcount(double popular375spaMcount) {
		Popular375SPAMcount = popular375spaMcount;
	}

	public double getUnpopular375SPAMcount() {
		return Unpopular375SPAMcount;
	}

	public void setUnpopular375SPAMcount(double unpopular375spaMcount) {
		Unpopular375SPAMcount = unpopular375spaMcount;
	}

	public double getSPAM500Count() {
		return SPAM500Count;
	}

	public void setSPAM500Count(double sPAM500Count) {
		SPAM500Count = sPAM500Count;
	}

	public double getMPSP500SPAMcount() {
		return MPSP500SPAMcount;
	}

	public void setMPSP500SPAMcount(double mPSP500SPAMcount) {
		MPSP500SPAMcount = mPSP500SPAMcount;
	}

	public double getMSP500SPAMcount() {
		return MSP500SPAMcount;
	}

	public void setMSP500SPAMcount(double mSP500SPAMcount) {
		MSP500SPAMcount = mSP500SPAMcount;
	}

	public double getPopular500SPAMcount() {
		return Popular500SPAMcount;
	}

	public void setPopular500SPAMcount(double popular500spaMcount) {
		Popular500SPAMcount = popular500spaMcount;
	}

	public double getUnpopular500SPAMcount() {
		return Unpopular500SPAMcount;
	}

	public void setUnpopular500SPAMcount(double unpopular500spaMcount) {
		Unpopular500SPAMcount = unpopular500spaMcount;
	}

	public double getPCT_Undetected_MPSPs_at_125() {
		return PCT_Undetected_MPSPs_at_125;
	}

	public void setPCT_Undetected_MPSPs_at_125(double pCT_Undetected_MPSPs_at_125) {
		PCT_Undetected_MPSPs_at_125 = pCT_Undetected_MPSPs_at_125;
	}

	public double getPCT_Undetected_MPSPs_at_250() {
		return PCT_Undetected_MPSPs_at_250;
	}

	public void setPCT_Undetected_MPSPs_at_250(double pCT_Undetected_MPSPs_at_250) {
		PCT_Undetected_MPSPs_at_250 = pCT_Undetected_MPSPs_at_250;
	}

	public double getPCT_Undetected_MPSPs_at_375() {
		return PCT_Undetected_MPSPs_at_375;
	}

	public void setPCT_Undetected_MPSPs_at_375(double pCT_Undetected_MPSPs_at_375) {
		PCT_Undetected_MPSPs_at_375 = pCT_Undetected_MPSPs_at_375;
	}

	public double getPCT_Undetected_MPSPs_at_500() {
		return PCT_Undetected_MPSPs_at_500;
	}

	public void setPCT_Undetected_MPSPs_at_500(double pCT_Undetected_MPSPs_at_500) {
		PCT_Undetected_MPSPs_at_500 = pCT_Undetected_MPSPs_at_500;
	}

	public double getPCT_Undetected_MSPs_at_125() {
		return PCT_Undetected_MSPs_at_125;
	}

	public void setPCT_Undetected_MSPs_at_125(double pCT_Undetected_MSPs_at_125) {
		PCT_Undetected_MSPs_at_125 = pCT_Undetected_MSPs_at_125;
	}

	public double getPCT_Undetected_MSPs_at_250() {
		return PCT_Undetected_MSPs_at_250;
	}

	public void setPCT_Undetected_MSPs_at_250(double pCT_Undetected_MSPs_at_250) {
		PCT_Undetected_MSPs_at_250 = pCT_Undetected_MSPs_at_250;
	}

	public double getPCT_Undetected_MSPs_at_375() {
		return PCT_Undetected_MSPs_at_375;
	}

	public void setPCT_Undetected_MSPs_at_375(double pCT_Undetected_MSPs_at_375) {
		PCT_Undetected_MSPs_at_375 = pCT_Undetected_MSPs_at_375;
	}

	public double getPCT_Undetected_MSPs_at_500() {
		return PCT_Undetected_MSPs_at_500;
	}

	public void setPCT_Undetected_MSPs_at_500(double pCT_Undetected_MSPs_at_500) {
		PCT_Undetected_MSPs_at_500 = pCT_Undetected_MSPs_at_500;
	}

	public double getPCT_Undetected_Popular_MPSPs_at_125() {
		return PCT_Undetected_Popular_MPSPs_at_125;
	}

	public void setPCT_Undetected_Popular_MPSPs_at_125(
			double pCT_Undetected_Popular_MPSPs_at_125) {
		PCT_Undetected_Popular_MPSPs_at_125 = pCT_Undetected_Popular_MPSPs_at_125;
	}

	public double getPCT_Undetected_Popular_MPSPs_at_250() {
		return PCT_Undetected_Popular_MPSPs_at_250;
	}

	public void setPCT_Undetected_Popular_MPSPs_at_250(
			double pCT_Undetected_Popular_MPSPs_at_250) {
		PCT_Undetected_Popular_MPSPs_at_250 = pCT_Undetected_Popular_MPSPs_at_250;
	}

	public double getPCT_Undetected_Popular_MPSPs_at_375() {
		return PCT_Undetected_Popular_MPSPs_at_375;
	}

	public void setPCT_Undetected_Popular_MPSPs_at_375(
			double pCT_Undetected_Popular_MPSPs_at_375) {
		PCT_Undetected_Popular_MPSPs_at_375 = pCT_Undetected_Popular_MPSPs_at_375;
	}

	public double getPCT_Undetected_Popular_MPSPs_at_500() {
		return PCT_Undetected_Popular_MPSPs_at_500;
	}

	public void setPCT_Undetected_Popular_MPSPs_at_500(
			double pCT_Undetected_Popular_MPSPs_at_500) {
		PCT_Undetected_Popular_MPSPs_at_500 = pCT_Undetected_Popular_MPSPs_at_500;
	}

	public double getPCT_Undetected_Popular_MSPs_at_125() {
		return PCT_Undetected_Popular_MSPs_at_125;
	}

	public void setPCT_Undetected_Popular_MSPs_at_125(
			double pCT_Undetected_Popular_MSPs_at_125) {
		PCT_Undetected_Popular_MSPs_at_125 = pCT_Undetected_Popular_MSPs_at_125;
	}

	public double getPCT_Undetected_Popular_MSPs_at_250() {
		return PCT_Undetected_Popular_MSPs_at_250;
	}

	public void setPCT_Undetected_Popular_MSPs_at_250(
			double pCT_Undetected_Popular_MSPs_at_250) {
		PCT_Undetected_Popular_MSPs_at_250 = pCT_Undetected_Popular_MSPs_at_250;
	}

	public double getPCT_Undetected_Popular_MSPs_at_375() {
		return PCT_Undetected_Popular_MSPs_at_375;
	}

	public void setPCT_Undetected_Popular_MSPs_at_375(
			double pCT_Undetected_Popular_MSPs_at_375) {
		PCT_Undetected_Popular_MSPs_at_375 = pCT_Undetected_Popular_MSPs_at_375;
	}

	public double getPCT_Undetected_Popular_MSPs_at_500() {
		return PCT_Undetected_Popular_MSPs_at_500;
	}

	public void setPCT_Undetected_Popular_MSPs_at_500(
			double pCT_Undetected_Popular_MSPs_at_500) {
		PCT_Undetected_Popular_MSPs_at_500 = pCT_Undetected_Popular_MSPs_at_500;
	}

	public double getPCT_Undetected_Unpopular_MPSPs_at_125() {
		return PCT_Undetected_Unpopular_MPSPs_at_125;
	}

	public void setPCT_Undetected_Unpopular_MPSPs_at_125(
			double pCT_Undetected_Unpopular_MPSPs_at_125) {
		PCT_Undetected_Unpopular_MPSPs_at_125 = pCT_Undetected_Unpopular_MPSPs_at_125;
	}

	public double getPCT_Undetected_Unpopular_MPSPs_at_250() {
		return PCT_Undetected_Unpopular_MPSPs_at_250;
	}

	public void setPCT_Undetected_Unpopular_MPSPs_at_250(
			double pCT_Undetected_Unpopular_MPSPs_at_250) {
		PCT_Undetected_Unpopular_MPSPs_at_250 = pCT_Undetected_Unpopular_MPSPs_at_250;
	}

	public double getPCT_Undetected_Unpopular_MPSPs_at_375() {
		return PCT_Undetected_Unpopular_MPSPs_at_375;
	}

	public void setPCT_Undetected_Unpopular_MPSPs_at_375(
			double pCT_Undetected_Unpopular_MPSPs_at_375) {
		PCT_Undetected_Unpopular_MPSPs_at_375 = pCT_Undetected_Unpopular_MPSPs_at_375;
	}

	public double getPCT_Undetected_Unpopular_MPSPs_at_500() {
		return PCT_Undetected_Unpopular_MPSPs_at_500;
	}

	public void setPCT_Undetected_Unpopular_MPSPs_at_500(
			double pCT_Undetected_Unpopular_MPSPs_at_500) {
		PCT_Undetected_Unpopular_MPSPs_at_500 = pCT_Undetected_Unpopular_MPSPs_at_500;
	}

	public double getPCT_Undetected_Unpopular_MSPs_at_125() {
		return PCT_Undetected_Unpopular_MSPs_at_125;
	}

	public void setPCT_Undetected_Unpopular_MSPs_at_125(
			double pCT_Undetected_Unpopular_MSPs_at_125) {
		PCT_Undetected_Unpopular_MSPs_at_125 = pCT_Undetected_Unpopular_MSPs_at_125;
	}

	public double getPCT_Undetected_Unpopular_MSPs_at_250() {
		return PCT_Undetected_Unpopular_MSPs_at_250;
	}

	public void setPCT_Undetected_Unpopular_MSPs_at_250(
			double pCT_Undetected_Unpopular_MSPs_at_250) {
		PCT_Undetected_Unpopular_MSPs_at_250 = pCT_Undetected_Unpopular_MSPs_at_250;
	}

	public double getPCT_Undetected_Unpopular_MSPs_at_375() {
		return PCT_Undetected_Unpopular_MSPs_at_375;
	}

	public void setPCT_Undetected_Unpopular_MSPs_at_375(
			double pCT_Undetected_Unpopular_MSPs_at_375) {
		PCT_Undetected_Unpopular_MSPs_at_375 = pCT_Undetected_Unpopular_MSPs_at_375;
	}

	public double getPCT_Undetected_Unpopular_MSPs_at_500() {
		return PCT_Undetected_Unpopular_MSPs_at_500;
	}

	public void setPCT_Undetected_Unpopular_MSPs_at_500(
			double pCT_Undetected_Unpopular_MSPs_at_500) {
		PCT_Undetected_Unpopular_MSPs_at_500 = pCT_Undetected_Unpopular_MSPs_at_500;
	}

	public double getAvgTimeToBan_MPSP_at_125() {
		return AvgTimeToBan_MPSP_at_125;
	}

	public void setAvgTimeToBan_MPSP_at_125(double avgTimeToBan_MPSP_at_125) {
		AvgTimeToBan_MPSP_at_125 = avgTimeToBan_MPSP_at_125;
	}

	public double getAvgTimeToBan_MPSP_at_250() {
		return AvgTimeToBan_MPSP_at_250;
	}

	public void setAvgTimeToBan_MPSP_at_250(double avgTimeToBan_MPSP_at_250) {
		AvgTimeToBan_MPSP_at_250 = avgTimeToBan_MPSP_at_250;
	}

	public double getAvgTimeToBan_MPSP_at_375() {
		return AvgTimeToBan_MPSP_at_375;
	}

	public void setAvgTimeToBan_MPSP_at_375(double avgTimeToBan_MPSP_at_375) {
		AvgTimeToBan_MPSP_at_375 = avgTimeToBan_MPSP_at_375;
	}

	public double getAvgTimeToBan_MPSP_at_500() {
		return AvgTimeToBan_MPSP_at_500;
	}

	public void setAvgTimeToBan_MPSP_at_500(double avgTimeToBan_MPSP_at_500) {
		AvgTimeToBan_MPSP_at_500 = avgTimeToBan_MPSP_at_500;
	}

	public double getAvgTimeToBan_MSP_at_125() {
		return AvgTimeToBan_MSP_at_125;
	}

	public void setAvgTimeToBan_MSP_at_125(double avgTimeToBan_MSP_at_125) {
		AvgTimeToBan_MSP_at_125 = avgTimeToBan_MSP_at_125;
	}

	public double getAvgTimeToBan_MSP_at_250() {
		return AvgTimeToBan_MSP_at_250;
	}

	public void setAvgTimeToBan_MSP_at_250(double avgTimeToBan_MSP_at_250) {
		AvgTimeToBan_MSP_at_250 = avgTimeToBan_MSP_at_250;
	}

	public double getAvgTimeToBan_MSP_at_375() {
		return AvgTimeToBan_MSP_at_375;
	}

	public void setAvgTimeToBan_MSP_at_375(double avgTimeToBan_MSP_at_375) {
		AvgTimeToBan_MSP_at_375 = avgTimeToBan_MSP_at_375;
	}

	public double getAvgTimeToBan_MSP_at_500() {
		return AvgTimeToBan_MSP_at_500;
	}

	public void setAvgTimeToBan_MSP_at_500(double avgTimeToBan_MSP_at_500) {
		AvgTimeToBan_MSP_at_500 = avgTimeToBan_MSP_at_500;
	}

	public double getAvgTimeToBan_Popular_MPSP_at_125() {
		return AvgTimeToBan_Popular_MPSP_at_125;
	}

	public void setAvgTimeToBan_Popular_MPSP_at_125(
			double avgTimeToBan_Popular_MPSP_at_125) {
		AvgTimeToBan_Popular_MPSP_at_125 = avgTimeToBan_Popular_MPSP_at_125;
	}

	public double getAvgTimeToBan_Popular_MPSP_at_250() {
		return AvgTimeToBan_Popular_MPSP_at_250;
	}

	public void setAvgTimeToBan_Popular_MPSP_at_250(
			double avgTimeToBan_Popular_MPSP_at_250) {
		AvgTimeToBan_Popular_MPSP_at_250 = avgTimeToBan_Popular_MPSP_at_250;
	}

	public double getAvgTimeToBan_Popular_MPSP_at_375() {
		return AvgTimeToBan_Popular_MPSP_at_375;
	}

	public void setAvgTimeToBan_Popular_MPSP_at_375(
			double avgTimeToBan_Popular_MPSP_at_375) {
		AvgTimeToBan_Popular_MPSP_at_375 = avgTimeToBan_Popular_MPSP_at_375;
	}

	public double getAvgTimeToBan_Popular_MPSP_at_500() {
		return AvgTimeToBan_Popular_MPSP_at_500;
	}

	public void setAvgTimeToBan_Popular_MPSP_at_500(
			double avgTimeToBan_Popular_MPSP_at_500) {
		AvgTimeToBan_Popular_MPSP_at_500 = avgTimeToBan_Popular_MPSP_at_500;
	}

	public double getAvgTimeToBan_Popular_MSP_at_125() {
		return AvgTimeToBan_Popular_MSP_at_125;
	}

	public void setAvgTimeToBan_Popular_MSP_at_125(
			double avgTimeToBan_Popular_MSP_at_125) {
		AvgTimeToBan_Popular_MSP_at_125 = avgTimeToBan_Popular_MSP_at_125;
	}

	public double getAvgTimeToBan_Popular_MSP_at_250() {
		return AvgTimeToBan_Popular_MSP_at_250;
	}

	public void setAvgTimeToBan_Popular_MSP_at_250(
			double avgTimeToBan_Popular_MSP_at_250) {
		AvgTimeToBan_Popular_MSP_at_250 = avgTimeToBan_Popular_MSP_at_250;
	}

	public double getAvgTimeToBan_Popular_MSP_at_375() {
		return AvgTimeToBan_Popular_MSP_at_375;
	}

	public void setAvgTimeToBan_Popular_MSP_at_375(
			double avgTimeToBan_Popular_MSP_at_375) {
		AvgTimeToBan_Popular_MSP_at_375 = avgTimeToBan_Popular_MSP_at_375;
	}

	public double getAvgTimeToBan_Popular_MSP_at_500() {
		return AvgTimeToBan_Popular_MSP_at_500;
	}

	public void setAvgTimeToBan_Popular_MSP_at_500(
			double avgTimeToBan_Popular_MSP_at_500) {
		AvgTimeToBan_Popular_MSP_at_500 = avgTimeToBan_Popular_MSP_at_500;
	}

	public double getAvgTimeToBan_Unpopular_MPSP_at_125() {
		return AvgTimeToBan_Unpopular_MPSP_at_125;
	}

	public void setAvgTimeToBan_Unpopular_MPSP_at_125(
			double avgTimeToBan_Unpopular_MPSP_at_125) {
		AvgTimeToBan_Unpopular_MPSP_at_125 = avgTimeToBan_Unpopular_MPSP_at_125;
	}

	public double getAvgTimeToBan_Unpopular_MPSP_at_250() {
		return AvgTimeToBan_Unpopular_MPSP_at_250;
	}

	public void setAvgTimeToBan_Unpopular_MPSP_at_250(
			double avgTimeToBan_Unpopular_MPSP_at_250) {
		AvgTimeToBan_Unpopular_MPSP_at_250 = avgTimeToBan_Unpopular_MPSP_at_250;
	}

	public double getAvgTimeToBan_Unpopular_MPSP_at_375() {
		return AvgTimeToBan_Unpopular_MPSP_at_375;
	}

	public void setAvgTimeToBan_Unpopular_MPSP_at_375(
			double avgTimeToBan_Unpopular_MPSP_at_375) {
		AvgTimeToBan_Unpopular_MPSP_at_375 = avgTimeToBan_Unpopular_MPSP_at_375;
	}

	public double getAvgTimeToBan_Unpopular_MPSP_at_500() {
		return AvgTimeToBan_Unpopular_MPSP_at_500;
	}

	public void setAvgTimeToBan_Unpopular_MPSP_at_500(
			double avgTimeToBan_Unpopular_MPSP_at_500) {
		AvgTimeToBan_Unpopular_MPSP_at_500 = avgTimeToBan_Unpopular_MPSP_at_500;
	}

	public double getAvgSPAMToBan_MPSP_at_125() {
		return AvgSPAMToBan_MPSP_at_125;
	}

	public void setAvgSPAMToBan_MPSP_at_125(double avgSPAMToBan_MPSP_at_125) {
		AvgSPAMToBan_MPSP_at_125 = avgSPAMToBan_MPSP_at_125;
	}

	public double getAvgSPAMToBan_MPSP_at_250() {
		return AvgSPAMToBan_MPSP_at_250;
	}

	public void setAvgSPAMToBan_MPSP_at_250(double avgSPAMToBan_MPSP_at_250) {
		AvgSPAMToBan_MPSP_at_250 = avgSPAMToBan_MPSP_at_250;
	}

	public double getAvgSPAMToBan_MPSP_at_375() {
		return AvgSPAMToBan_MPSP_at_375;
	}

	public void setAvgSPAMToBan_MPSP_at_375(double avgSPAMToBan_MPSP_at_375) {
		AvgSPAMToBan_MPSP_at_375 = avgSPAMToBan_MPSP_at_375;
	}

	public double getAvgSPAMToBan_MPSP_at_500() {
		return AvgSPAMToBan_MPSP_at_500;
	}

	public void setAvgSPAMToBan_MPSP_at_500(double avgSPAMToBan_MPSP_at_500) {
		AvgSPAMToBan_MPSP_at_500 = avgSPAMToBan_MPSP_at_500;
	}

	public double getAvgSPAMToBan_MSP_at_125() {
		return AvgSPAMToBan_MSP_at_125;
	}

	public void setAvgSPAMToBan_MSP_at_125(double avgSPAMToBan_MSP_at_125) {
		AvgSPAMToBan_MSP_at_125 = avgSPAMToBan_MSP_at_125;
	}

	public double getAvgSPAMToBan_MSP_at_250() {
		return AvgSPAMToBan_MSP_at_250;
	}

	public void setAvgSPAMToBan_MSP_at_250(double avgSPAMToBan_MSP_at_250) {
		AvgSPAMToBan_MSP_at_250 = avgSPAMToBan_MSP_at_250;
	}

	public double getAvgSPAMToBan_MSP_at_375() {
		return AvgSPAMToBan_MSP_at_375;
	}

	public void setAvgSPAMToBan_MSP_at_375(double avgSPAMToBan_MSP_at_375) {
		AvgSPAMToBan_MSP_at_375 = avgSPAMToBan_MSP_at_375;
	}

	public double getAvgSPAMToBan_MSP_at_500() {
		return AvgSPAMToBan_MSP_at_500;
	}

	public void setAvgSPAMToBan_MSP_at_500(double avgSPAMToBan_MSP_at_500) {
		AvgSPAMToBan_MSP_at_500 = avgSPAMToBan_MSP_at_500;
	}

	public double getAvgSPAMToBan_Popular_MPSP_at_125() {
		return AvgSPAMToBan_Popular_MPSP_at_125;
	}

	public void setAvgSPAMToBan_Popular_MPSP_at_125(
			double avgSPAMToBan_Popular_MPSP_at_125) {
		AvgSPAMToBan_Popular_MPSP_at_125 = avgSPAMToBan_Popular_MPSP_at_125;
	}

	public double getAvgSPAMToBan_Popular_MPSP_at_250() {
		return AvgSPAMToBan_Popular_MPSP_at_250;
	}

	public void setAvgSPAMToBan_Popular_MPSP_at_250(
			double avgSPAMToBan_Popular_MPSP_at_250) {
		AvgSPAMToBan_Popular_MPSP_at_250 = avgSPAMToBan_Popular_MPSP_at_250;
	}

	public double getAvgSPAMToBan_Popular_MPSP_at_375() {
		return AvgSPAMToBan_Popular_MPSP_at_375;
	}

	public void setAvgSPAMToBan_Popular_MPSP_at_375(
			double avgSPAMToBan_Popular_MPSP_at_375) {
		AvgSPAMToBan_Popular_MPSP_at_375 = avgSPAMToBan_Popular_MPSP_at_375;
	}

	public double getAvgSPAMToBan_Popular_MPSP_at_500() {
		return AvgSPAMToBan_Popular_MPSP_at_500;
	}

	public void setAvgSPAMToBan_Popular_MPSP_at_500(
			double avgSPAMToBan_Popular_MPSP_at_500) {
		AvgSPAMToBan_Popular_MPSP_at_500 = avgSPAMToBan_Popular_MPSP_at_500;
	}

	public double getAvgSPAMToBan_Popular_MSP_at_125() {
		return AvgSPAMToBan_Popular_MSP_at_125;
	}

	public void setAvgSPAMToBan_Popular_MSP_at_125(
			double avgSPAMToBan_Popular_MSP_at_125) {
		AvgSPAMToBan_Popular_MSP_at_125 = avgSPAMToBan_Popular_MSP_at_125;
	}

	public double getAvgSPAMToBan_Popular_MSP_at_250() {
		return AvgSPAMToBan_Popular_MSP_at_250;
	}

	public void setAvgSPAMToBan_Popular_MSP_at_250(
			double avgSPAMToBan_Popular_MSP_at_250) {
		AvgSPAMToBan_Popular_MSP_at_250 = avgSPAMToBan_Popular_MSP_at_250;
	}

	public double getAvgSPAMToBan_Popular_MSP_at_375() {
		return AvgSPAMToBan_Popular_MSP_at_375;
	}

	public void setAvgSPAMToBan_Popular_MSP_at_375(
			double avgSPAMToBan_Popular_MSP_at_375) {
		AvgSPAMToBan_Popular_MSP_at_375 = avgSPAMToBan_Popular_MSP_at_375;
	}

	public double getAvgSPAMToBan_Popular_MSP_at_500() {
		return AvgSPAMToBan_Popular_MSP_at_500;
	}

	public void setAvgSPAMToBan_Popular_MSP_at_500(
			double avgSPAMToBan_Popular_MSP_at_500) {
		AvgSPAMToBan_Popular_MSP_at_500 = avgSPAMToBan_Popular_MSP_at_500;
	}

	public double getAvgSPAMToBan_Unpopular_MPSP_at_125() {
		return AvgSPAMToBan_Unpopular_MPSP_at_125;
	}

	public void setAvgSPAMToBan_Unpopular_MPSP_at_125(
			double avgSPAMToBan_Unpopular_MPSP_at_125) {
		AvgSPAMToBan_Unpopular_MPSP_at_125 = avgSPAMToBan_Unpopular_MPSP_at_125;
	}

	public double getAvgSPAMToBan_Unpopular_MPSP_at_250() {
		return AvgSPAMToBan_Unpopular_MPSP_at_250;
	}

	public void setAvgSPAMToBan_Unpopular_MPSP_at_250(
			double avgSPAMToBan_Unpopular_MPSP_at_250) {
		AvgSPAMToBan_Unpopular_MPSP_at_250 = avgSPAMToBan_Unpopular_MPSP_at_250;
	}

	public double getAvgSPAMToBan_Unpopular_MPSP_at_375() {
		return AvgSPAMToBan_Unpopular_MPSP_at_375;
	}

	public void setAvgSPAMToBan_Unpopular_MPSP_at_375(
			double avgSPAMToBan_Unpopular_MPSP_at_375) {
		AvgSPAMToBan_Unpopular_MPSP_at_375 = avgSPAMToBan_Unpopular_MPSP_at_375;
	}

	public double getAvgSPAMToBan_Unpopular_MPSP_at_500() {
		return AvgSPAMToBan_Unpopular_MPSP_at_500;
	}

	public void setAvgSPAMToBan_Unpopular_MPSP_at_500(
			double avgSPAMToBan_Unpopular_MPSP_at_500) {
		AvgSPAMToBan_Unpopular_MPSP_at_500 = avgSPAMToBan_Unpopular_MPSP_at_500;
	}

	public double getAvgTimeToBan_Unpopular_MSP_at_125() {
		return AvgTimeToBan_Unpopular_MSP_at_125;
	}

	public void setAvgTimeToBan_Unpopular_MSP_at_125(
			double avgTimeToBan_Unpopular_MSP_at_125) {
		AvgTimeToBan_Unpopular_MSP_at_125 = avgTimeToBan_Unpopular_MSP_at_125;
	}

	public double getAvgTimeToBan_Unpopular_MSP_at_250() {
		return AvgTimeToBan_Unpopular_MSP_at_250;
	}

	public void setAvgTimeToBan_Unpopular_MSP_at_250(
			double avgTimeToBan_Unpopular_MSP_at_250) {
		AvgTimeToBan_Unpopular_MSP_at_250 = avgTimeToBan_Unpopular_MSP_at_250;
	}

	public double getAvgTimeToBan_Unpopular_MSP_at_375() {
		return AvgTimeToBan_Unpopular_MSP_at_375;
	}

	public void setAvgTimeToBan_Unpopular_MSP_at_375(
			double avgTimeToBan_Unpopular_MSP_at_375) {
		AvgTimeToBan_Unpopular_MSP_at_375 = avgTimeToBan_Unpopular_MSP_at_375;
	}

	public double getAvgTimeToBan_Unpopular_MSP_at_500() {
		return AvgTimeToBan_Unpopular_MSP_at_500;
	}

	public void setAvgTimeToBan_Unpopular_MSP_at_500(
			double avgTimeToBan_Unpopular_MSP_at_500) {
		AvgTimeToBan_Unpopular_MSP_at_500 = avgTimeToBan_Unpopular_MSP_at_500;
	}

	public double getAvgSPAMToBan_Unpopular_MSP_at_125() {
		return AvgSPAMToBan_Unpopular_MSP_at_125;
	}

	public void setAvgSPAMToBan_Unpopular_MSP_at_125(
			double avgSPAMToBan_Unpopular_MSP_at_125) {
		AvgSPAMToBan_Unpopular_MSP_at_125 = avgSPAMToBan_Unpopular_MSP_at_125;
	}

	public double getAvgSPAMToBan_Unpopular_MSP_at_250() {
		return AvgSPAMToBan_Unpopular_MSP_at_250;
	}

	public void setAvgSPAMToBan_Unpopular_MSP_at_250(
			double avgSPAMToBan_Unpopular_MSP_at_250) {
		AvgSPAMToBan_Unpopular_MSP_at_250 = avgSPAMToBan_Unpopular_MSP_at_250;
	}

	public double getAvgSPAMToBan_Unpopular_MSP_at_375() {
		return AvgSPAMToBan_Unpopular_MSP_at_375;
	}

	public void setAvgSPAMToBan_Unpopular_MSP_at_375(
			double avgSPAMToBan_Unpopular_MSP_at_375) {
		AvgSPAMToBan_Unpopular_MSP_at_375 = avgSPAMToBan_Unpopular_MSP_at_375;
	}

	public double getAvgSPAMToBan_Unpopular_MSP_at_500() {
		return AvgSPAMToBan_Unpopular_MSP_at_500;
	}

	public void setAvgSPAMToBan_Unpopular_MSP_at_500(
			double avgSPAMToBan_Unpopular_MSP_at_500) {
		AvgSPAMToBan_Unpopular_MSP_at_500 = avgSPAMToBan_Unpopular_MSP_at_500;
	}

	public double getPCT_innocent_PSPs_banned_guilty_at_125() {
		return PCT_innocent_PSPs_banned_guilty_at_125;
	}

	public void setPCT_innocent_PSPs_banned_guilty_at_125(
			double pCT_innocent_PSPs_banned_guilty_at_125) {
		PCT_innocent_PSPs_banned_guilty_at_125 = pCT_innocent_PSPs_banned_guilty_at_125;
	}

	public double getPCT_innocent_PSPs_banned_guilty_at_250() {
		return PCT_innocent_PSPs_banned_guilty_at_250;
	}

	public void setPCT_innocent_PSPs_banned_guilty_at_250(
			double pCT_innocent_PSPs_banned_guilty_at_250) {
		PCT_innocent_PSPs_banned_guilty_at_250 = pCT_innocent_PSPs_banned_guilty_at_250;
	}

	public double getPCT_innocent_PSPs_banned_guilty_at_375() {
		return PCT_innocent_PSPs_banned_guilty_at_375;
	}

	public void setPCT_innocent_PSPs_banned_guilty_at_375(
			double pCT_innocent_PSPs_banned_guilty_at_375) {
		PCT_innocent_PSPs_banned_guilty_at_375 = pCT_innocent_PSPs_banned_guilty_at_375;
	}

	public double getPCT_innocent_PSPs_banned_guilty_at_500() {
		return PCT_innocent_PSPs_banned_guilty_at_500;
	}

	public void setPCT_innocent_PSPs_banned_guilty_at_500(
			double pCT_innocent_PSPs_banned_guilty_at_500) {
		PCT_innocent_PSPs_banned_guilty_at_500 = pCT_innocent_PSPs_banned_guilty_at_500;
	}

	public double getPCT_innocent_SPs_banned_guilty_at_125() {
		return PCT_innocent_SPs_banned_guilty_at_125;
	}

	public void setPCT_innocent_SPs_banned_guilty_at_125(
			double pCT_innocent_SPs_banned_guilty_at_125) {
		PCT_innocent_SPs_banned_guilty_at_125 = pCT_innocent_SPs_banned_guilty_at_125;
	}

	public double getPCT_innocent_SPs_banned_guilty_at_250() {
		return PCT_innocent_SPs_banned_guilty_at_250;
	}

	public void setPCT_innocent_SPs_banned_guilty_at_250(
			double pCT_innocent_SPs_banned_guilty_at_250) {
		PCT_innocent_SPs_banned_guilty_at_250 = pCT_innocent_SPs_banned_guilty_at_250;
	}

	public double getPCT_innocent_SPs_banned_guilty_at_375() {
		return PCT_innocent_SPs_banned_guilty_at_375;
	}

	public void setPCT_innocent_SPs_banned_guilty_at_375(
			double pCT_innocent_SPs_banned_guilty_at_375) {
		PCT_innocent_SPs_banned_guilty_at_375 = pCT_innocent_SPs_banned_guilty_at_375;
	}

	public double getPCT_innocent_SPs_banned_guilty_at_500() {
		return PCT_innocent_SPs_banned_guilty_at_500;
	}

	public void setPCT_innocent_SPs_banned_guilty_at_500(
			double pCT_innocent_SPs_banned_guilty_at_500) {
		PCT_innocent_SPs_banned_guilty_at_500 = pCT_innocent_SPs_banned_guilty_at_500;
	}

	public double getBannedInnocentSPCount() {
		return bannedInnocentSPCount;
	}

	public void setBannedInnocentSPCount(double bannedInnocentSPCount) {
		this.bannedInnocentSPCount = bannedInnocentSPCount;
	}

	public double getBannedInnocentPSPCount() {
		return bannedInnocentPSPCount;
	}

	public void setBannedInnocentPSPCount(double bannedInnocentPSPCount) {
		this.bannedInnocentPSPCount = bannedInnocentPSPCount;
	}

	public boolean isDeployDGU() {
		return DeployDGU;
	}

	public void setDeployDGU(boolean deployDGU) {
		DeployDGU = deployDGU;
	}

	public double getPCT_innocent_PSPs_banned_refusedInstallDGU_at_125() {
		return PCT_innocent_PSPs_banned_refusedInstallDGU_at_125;
	}

	public void setPCT_innocent_PSPs_banned_refusedInstallDGU_at_125(
			double pCT_innocent_PSPs_banned_refusedInstallDGU_at_125) {
		PCT_innocent_PSPs_banned_refusedInstallDGU_at_125 = pCT_innocent_PSPs_banned_refusedInstallDGU_at_125;
	}

	public double getPCT_innocent_PSPs_banned_refusedInstallDGU_at_250() {
		return PCT_innocent_PSPs_banned_refusedInstallDGU_at_250;
	}

	public void setPCT_innocent_PSPs_banned_refusedInstallDGU_at_250(
			double pCT_innocent_PSPs_banned_refusedInstallDGU_at_250) {
		PCT_innocent_PSPs_banned_refusedInstallDGU_at_250 = pCT_innocent_PSPs_banned_refusedInstallDGU_at_250;
	}

	public double getPCT_innocent_PSPs_banned_refusedInstallDGU_at_375() {
		return PCT_innocent_PSPs_banned_refusedInstallDGU_at_375;
	}

	public void setPCT_innocent_PSPs_banned_refusedInstallDGU_at_375(
			double pCT_innocent_PSPs_banned_refusedInstallDGU_at_375) {
		PCT_innocent_PSPs_banned_refusedInstallDGU_at_375 = pCT_innocent_PSPs_banned_refusedInstallDGU_at_375;
	}

	public double getPCT_innocent_PSPs_banned_refusedInstallDGU_at_500() {
		return PCT_innocent_PSPs_banned_refusedInstallDGU_at_500;
	}

	public void setPCT_innocent_PSPs_banned_refusedInstallDGU_at_500(
			double pCT_innocent_PSPs_banned_refusedInstallDGU_at_500) {
		PCT_innocent_PSPs_banned_refusedInstallDGU_at_500 = pCT_innocent_PSPs_banned_refusedInstallDGU_at_500;
	}

	public double getPCT_innocent_SPs_banned_refusedInstallDGU_at_125() {
		return PCT_innocent_SPs_banned_refusedInstallDGU_at_125;
	}

	public void setPCT_innocent_SPs_banned_refusedInstallDGU_at_125(
			double pCT_innocent_SPs_banned_refusedInstallDGU_at_125) {
		PCT_innocent_SPs_banned_refusedInstallDGU_at_125 = pCT_innocent_SPs_banned_refusedInstallDGU_at_125;
	}

	public double getPCT_innocent_SPs_banned_refusedInstallDGU_at_250() {
		return PCT_innocent_SPs_banned_refusedInstallDGU_at_250;
	}

	public void setPCT_innocent_SPs_banned_refusedInstallDGU_at_250(
			double pCT_innocent_SPs_banned_refusedInstallDGU_at_250) {
		PCT_innocent_SPs_banned_refusedInstallDGU_at_250 = pCT_innocent_SPs_banned_refusedInstallDGU_at_250;
	}

	public double getPCT_innocent_SPs_banned_refusedInstallDGU_at_375() {
		return PCT_innocent_SPs_banned_refusedInstallDGU_at_375;
	}

	public void setPCT_innocent_SPs_banned_refusedInstallDGU_at_375(
			double pCT_innocent_SPs_banned_refusedInstallDGU_at_375) {
		PCT_innocent_SPs_banned_refusedInstallDGU_at_375 = pCT_innocent_SPs_banned_refusedInstallDGU_at_375;
	}

	public double getPCT_innocent_SPs_banned_refusedInstallDGU_at_500() {
		return PCT_innocent_SPs_banned_refusedInstallDGU_at_500;
	}

	public void setPCT_innocent_SPs_banned_refusedInstallDGU_at_500(
			double pCT_innocent_SPs_banned_refusedInstallDGU_at_500) {
		PCT_innocent_SPs_banned_refusedInstallDGU_at_500 = pCT_innocent_SPs_banned_refusedInstallDGU_at_500;
	}

	public double getPCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_125() {
		return PCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_125;
	}

	public void setPCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_125(
			double pCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_125) {
		PCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_125 = pCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_125;
	}

	public double getPCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_250() {
		return PCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_250;
	}

	public void setPCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_250(
			double pCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_250) {
		PCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_250 = pCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_250;
	}

	public double getPCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_375() {
		return PCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_375;
	}

	public void setPCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_375(
			double pCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_375) {
		PCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_375 = pCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_375;
	}

	public double getPCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_500() {
		return PCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_500;
	}

	public void setPCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_500(
			double pCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_500) {
		PCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_500 = pCT_innocent_PSPs_banned_refusedEnableStrictDGU_at_500;
	}

	public double getPCT_innocent_SPs_banned_refusedEnableStrictDGU_at_125() {
		return PCT_innocent_SPs_banned_refusedEnableStrictDGU_at_125;
	}

	public void setPCT_innocent_SPs_banned_refusedEnableStrictDGU_at_125(
			double pCT_innocent_SPs_banned_refusedEnableStrictDGU_at_125) {
		PCT_innocent_SPs_banned_refusedEnableStrictDGU_at_125 = pCT_innocent_SPs_banned_refusedEnableStrictDGU_at_125;
	}

	public double getPCT_innocent_SPs_banned_refusedEnableStrictDGU_at_250() {
		return PCT_innocent_SPs_banned_refusedEnableStrictDGU_at_250;
	}

	public void setPCT_innocent_SPs_banned_refusedEnableStrictDGU_at_250(
			double pCT_innocent_SPs_banned_refusedEnableStrictDGU_at_250) {
		PCT_innocent_SPs_banned_refusedEnableStrictDGU_at_250 = pCT_innocent_SPs_banned_refusedEnableStrictDGU_at_250;
	}

	public double getPCT_innocent_SPs_banned_refusedEnableStrictDGU_at_375() {
		return PCT_innocent_SPs_banned_refusedEnableStrictDGU_at_375;
	}

	public void setPCT_innocent_SPs_banned_refusedEnableStrictDGU_at_375(
			double pCT_innocent_SPs_banned_refusedEnableStrictDGU_at_375) {
		PCT_innocent_SPs_banned_refusedEnableStrictDGU_at_375 = pCT_innocent_SPs_banned_refusedEnableStrictDGU_at_375;
	}

	public double getPCT_innocent_SPs_banned_refusedEnableStrictDGU_at_500() {
		return PCT_innocent_SPs_banned_refusedEnableStrictDGU_at_500;
	}

	public void setPCT_innocent_SPs_banned_refusedEnableStrictDGU_at_500(
			double pCT_innocent_SPs_banned_refusedEnableStrictDGU_at_500) {
		PCT_innocent_SPs_banned_refusedEnableStrictDGU_at_500 = pCT_innocent_SPs_banned_refusedEnableStrictDGU_at_500;
	}

	public List<SPsNodesDir> getBanned_spNodes() {
		return banned_spNodes;
	}

	public void setBanned_spNodes(List<SPsNodesDir> banned_spNodes) {
		this.banned_spNodes = banned_spNodes;
	}

	public List<SPsNodesDir> getBanned_pspNodes() {
		return banned_pspNodes;
	}

	public void setBanned_pspNodes(List<SPsNodesDir> banned_pspNodes) {
		this.banned_pspNodes = banned_pspNodes;
	}
	
	public double getPCT_PSPs_installed_DGU_at_125() {
		return PCT_PSPs_installed_DGU_at_125;
	}

	public void setPCT_PSPs_installed_DGU_at_125(
			double pCT_PSPs_installed_DGU_at_125) {
		PCT_PSPs_installed_DGU_at_125 = pCT_PSPs_installed_DGU_at_125;
	}

	public double getPCT_PSPs_installed_DGU_at_250() {
		return PCT_PSPs_installed_DGU_at_250;
	}

	public void setPCT_PSPs_installed_DGU_at_250(
			double pCT_PSPs_installed_DGU_at_250) {
		PCT_PSPs_installed_DGU_at_250 = pCT_PSPs_installed_DGU_at_250;
	}

	public double getPCT_PSPs_installed_DGU_at_375() {
		return PCT_PSPs_installed_DGU_at_375;
	}

	public void setPCT_PSPs_installed_DGU_at_375(
			double pCT_PSPs_installed_DGU_at_375) {
		PCT_PSPs_installed_DGU_at_375 = pCT_PSPs_installed_DGU_at_375;
	}

	public double getPCT_PSPs_installed_DGU_at_500() {
		return PCT_PSPs_installed_DGU_at_500;
	}

	public void setPCT_PSPs_installed_DGU_at_500(
			double pCT_PSPs_installed_DGU_at_500) {
		PCT_PSPs_installed_DGU_at_500 = pCT_PSPs_installed_DGU_at_500;
	}

	public double getPCT_PSPs_enabled_strict_DGU_at_125() {
		return PCT_PSPs_enabled_strict_DGU_at_125;
	}

	public void setPCT_PSPs_enabled_strict_DGU_at_125(
			double pCT_PSPs_enabled_strict_DGU_at_125) {
		PCT_PSPs_enabled_strict_DGU_at_125 = pCT_PSPs_enabled_strict_DGU_at_125;
	}

	public double getPCT_PSPs_enabled_strict_DGU_at_250() {
		return PCT_PSPs_enabled_strict_DGU_at_250;
	}

	public void setPCT_PSPs_enabled_strict_DGU_at_250(
			double pCT_PSPs_enabled_strict_DGU_at_250) {
		PCT_PSPs_enabled_strict_DGU_at_250 = pCT_PSPs_enabled_strict_DGU_at_250;
	}

	public double getPCT_PSPs_enabled_strict_DGU_at_375() {
		return PCT_PSPs_enabled_strict_DGU_at_375;
	}

	public void setPCT_PSPs_enabled_strict_DGU_at_375(
			double pCT_PSPs_enabled_strict_DGU_at_375) {
		PCT_PSPs_enabled_strict_DGU_at_375 = pCT_PSPs_enabled_strict_DGU_at_375;
	}

	public double getPCT_PSPs_enabled_strict_DGU_at_500() {
		return PCT_PSPs_enabled_strict_DGU_at_500;
	}

	public void setPCT_PSPs_enabled_strict_DGU_at_500(
			double pCT_PSPs_enabled_strict_DGU_at_500) {
		PCT_PSPs_enabled_strict_DGU_at_500 = pCT_PSPs_enabled_strict_DGU_at_500;
	}

	public double getPCT_SPs_installed_DGU_at_125() {
		return PCT_SPs_installed_DGU_at_125;
	}

	public void setPCT_SPs_installed_DGU_at_125(double pCT_SPs_installed_DGU_at_125) {
		PCT_SPs_installed_DGU_at_125 = pCT_SPs_installed_DGU_at_125;
	}

	public double getPCT_SPs_installed_DGU_at_250() {
		return PCT_SPs_installed_DGU_at_250;
	}

	public void setPCT_SPs_installed_DGU_at_250(double pCT_SPs_installed_DGU_at_250) {
		PCT_SPs_installed_DGU_at_250 = pCT_SPs_installed_DGU_at_250;
	}

	public double getPCT_SPs_installed_DGU_at_375() {
		return PCT_SPs_installed_DGU_at_375;
	}

	public void setPCT_SPs_installed_DGU_at_375(double pCT_SPs_installed_DGU_at_375) {
		PCT_SPs_installed_DGU_at_375 = pCT_SPs_installed_DGU_at_375;
	}

	public double getPCT_SPs_installed_DGU_at_500() {
		return PCT_SPs_installed_DGU_at_500;
	}

	public void setPCT_SPs_installed_DGU_at_500(double pCT_SPs_installed_DGU_at_500) {
		PCT_SPs_installed_DGU_at_500 = pCT_SPs_installed_DGU_at_500;
	}

	public double getPCT_SPs_enabled_strict_DGU_at_125() {
		return PCT_SPs_enabled_strict_DGU_at_125;
	}

	public void setPCT_SPs_enabled_strict_DGU_at_125(
			double pCT_SPs_enabled_strict_DGU_at_125) {
		PCT_SPs_enabled_strict_DGU_at_125 = pCT_SPs_enabled_strict_DGU_at_125;
	}

	public double getPCT_SPs_enabled_strict_DGU_at_250() {
		return PCT_SPs_enabled_strict_DGU_at_250;
	}

	public void setPCT_SPs_enabled_strict_DGU_at_250(
			double pCT_SPs_enabled_strict_DGU_at_250) {
		PCT_SPs_enabled_strict_DGU_at_250 = pCT_SPs_enabled_strict_DGU_at_250;
	}

	public double getPCT_SPs_enabled_strict_DGU_at_375() {
		return PCT_SPs_enabled_strict_DGU_at_375;
	}

	public void setPCT_SPs_enabled_strict_DGU_at_375(
			double pCT_SPs_enabled_strict_DGU_at_375) {
		PCT_SPs_enabled_strict_DGU_at_375 = pCT_SPs_enabled_strict_DGU_at_375;
	}

	public double getPCT_SPs_enabled_strict_DGU_at_500() {
		return PCT_SPs_enabled_strict_DGU_at_500;
	}

	public void setPCT_SPs_enabled_strict_DGU_at_500(
			double pCT_SPs_enabled_strict_DGU_at_500) {
		PCT_SPs_enabled_strict_DGU_at_500 = pCT_SPs_enabled_strict_DGU_at_500;
	}

	public double getPCT_MPSPs_installed_DGU_at_125() {
		return PCT_MPSPs_installed_DGU_at_125;
	}

	public void setPCT_MPSPs_installed_DGU_at_125(
			double pCT_MPSPs_installed_DGU_at_125) {
		PCT_MPSPs_installed_DGU_at_125 = pCT_MPSPs_installed_DGU_at_125;
	}

	public double getPCT_MPSPs_installed_DGU_at_250() {
		return PCT_MPSPs_installed_DGU_at_250;
	}

	public void setPCT_MPSPs_installed_DGU_at_250(
			double pCT_MPSPs_installed_DGU_at_250) {
		PCT_MPSPs_installed_DGU_at_250 = pCT_MPSPs_installed_DGU_at_250;
	}

	public double getPCT_MPSPs_installed_DGU_at_375() {
		return PCT_MPSPs_installed_DGU_at_375;
	}

	public void setPCT_MPSPs_installed_DGU_at_375(
			double pCT_MPSPs_installed_DGU_at_375) {
		PCT_MPSPs_installed_DGU_at_375 = pCT_MPSPs_installed_DGU_at_375;
	}

	public double getPCT_MPSPs_installed_DGU_at_500() {
		return PCT_MPSPs_installed_DGU_at_500;
	}

	public void setPCT_MPSPs_installed_DGU_at_500(
			double pCT_MPSPs_installed_DGU_at_500) {
		PCT_MPSPs_installed_DGU_at_500 = pCT_MPSPs_installed_DGU_at_500;
	}

	public double getPCT_MPSPs_enabled_strict_DGU_at_125() {
		return PCT_MPSPs_enabled_strict_DGU_at_125;
	}

	public void setPCT_MPSPs_enabled_strict_DGU_at_125(
			double pCT_MPSPs_enabled_strict_DGU_at_125) {
		PCT_MPSPs_enabled_strict_DGU_at_125 = pCT_MPSPs_enabled_strict_DGU_at_125;
	}

	public double getPCT_MPSPs_enabled_strict_DGU_at_250() {
		return PCT_MPSPs_enabled_strict_DGU_at_250;
	}

	public void setPCT_MPSPs_enabled_strict_DGU_at_250(
			double pCT_MPSPs_enabled_strict_DGU_at_250) {
		PCT_MPSPs_enabled_strict_DGU_at_250 = pCT_MPSPs_enabled_strict_DGU_at_250;
	}

	public double getPCT_MPSPs_enabled_strict_DGU_at_375() {
		return PCT_MPSPs_enabled_strict_DGU_at_375;
	}

	public void setPCT_MPSPs_enabled_strict_DGU_at_375(
			double pCT_MPSPs_enabled_strict_DGU_at_375) {
		PCT_MPSPs_enabled_strict_DGU_at_375 = pCT_MPSPs_enabled_strict_DGU_at_375;
	}

	public double getPCT_MPSPs_enabled_strict_DGU_at_500() {
		return PCT_MPSPs_enabled_strict_DGU_at_500;
	}

	public void setPCT_MPSPs_enabled_strict_DGU_at_500(
			double pCT_MPSPs_enabled_strict_DGU_at_500) {
		PCT_MPSPs_enabled_strict_DGU_at_500 = pCT_MPSPs_enabled_strict_DGU_at_500;
	}

	public double getPCT_MSPs_installed_DGU_at_125() {
		return PCT_MSPs_installed_DGU_at_125;
	}

	public void setPCT_MSPs_installed_DGU_at_125(
			double pCT_MSPs_installed_DGU_at_125) {
		PCT_MSPs_installed_DGU_at_125 = pCT_MSPs_installed_DGU_at_125;
	}

	public double getPCT_MSPs_installed_DGU_at_250() {
		return PCT_MSPs_installed_DGU_at_250;
	}

	public void setPCT_MSPs_installed_DGU_at_250(
			double pCT_MSPs_installed_DGU_at_250) {
		PCT_MSPs_installed_DGU_at_250 = pCT_MSPs_installed_DGU_at_250;
	}

	public double getPCT_MSPs_installed_DGU_at_375() {
		return PCT_MSPs_installed_DGU_at_375;
	}

	public void setPCT_MSPs_installed_DGU_at_375(
			double pCT_MSPs_installed_DGU_at_375) {
		PCT_MSPs_installed_DGU_at_375 = pCT_MSPs_installed_DGU_at_375;
	}

	public double getPCT_MSPs_installed_DGU_at_500() {
		return PCT_MSPs_installed_DGU_at_500;
	}

	public void setPCT_MSPs_installed_DGU_at_500(
			double pCT_MSPs_installed_DGU_at_500) {
		PCT_MSPs_installed_DGU_at_500 = pCT_MSPs_installed_DGU_at_500;
	}

	public double getPCT_MSPs_enabled_strict_DGU_at_125() {
		return PCT_MSPs_enabled_strict_DGU_at_125;
	}

	public void setPCT_MSPs_enabled_strict_DGU_at_125(
			double pCT_MSPs_enabled_strict_DGU_at_125) {
		PCT_MSPs_enabled_strict_DGU_at_125 = pCT_MSPs_enabled_strict_DGU_at_125;
	}

	public double getPCT_MSPs_enabled_strict_DGU_at_250() {
		return PCT_MSPs_enabled_strict_DGU_at_250;
	}

	public void setPCT_MSPs_enabled_strict_DGU_at_250(
			double pCT_MSPs_enabled_strict_DGU_at_250) {
		PCT_MSPs_enabled_strict_DGU_at_250 = pCT_MSPs_enabled_strict_DGU_at_250;
	}

	public double getPCT_MSPs_enabled_strict_DGU_at_375() {
		return PCT_MSPs_enabled_strict_DGU_at_375;
	}

	public void setPCT_MSPs_enabled_strict_DGU_at_375(
			double pCT_MSPs_enabled_strict_DGU_at_375) {
		PCT_MSPs_enabled_strict_DGU_at_375 = pCT_MSPs_enabled_strict_DGU_at_375;
	}

	public double getPCT_MSPs_enabled_strict_DGU_at_500() {
		return PCT_MSPs_enabled_strict_DGU_at_500;
	}

	public void setPCT_MSPs_enabled_strict_DGU_at_500(
			double pCT_MSPs_enabled_strict_DGU_at_500) {
		PCT_MSPs_enabled_strict_DGU_at_500 = pCT_MSPs_enabled_strict_DGU_at_500;
	}

	public double getPCT_Popular_MPSPs_installed_DGU_at_125() {
		return PCT_Popular_MPSPs_installed_DGU_at_125;
	}

	public void setPCT_Popular_MPSPs_installed_DGU_at_125(
			double pCT_Popular_MPSPs_installed_DGU_at_125) {
		PCT_Popular_MPSPs_installed_DGU_at_125 = pCT_Popular_MPSPs_installed_DGU_at_125;
	}

	public double getPCT_Popular_MPSPs_installed_DGU_at_250() {
		return PCT_Popular_MPSPs_installed_DGU_at_250;
	}

	public void setPCT_Popular_MPSPs_installed_DGU_at_250(
			double pCT_Popular_MPSPs_installed_DGU_at_250) {
		PCT_Popular_MPSPs_installed_DGU_at_250 = pCT_Popular_MPSPs_installed_DGU_at_250;
	}

	public double getPCT_Popular_MPSPs_installed_DGU_at_375() {
		return PCT_Popular_MPSPs_installed_DGU_at_375;
	}

	public void setPCT_Popular_MPSPs_installed_DGU_at_375(
			double pCT_Popular_MPSPs_installed_DGU_at_375) {
		PCT_Popular_MPSPs_installed_DGU_at_375 = pCT_Popular_MPSPs_installed_DGU_at_375;
	}

	public double getPCT_Popular_MPSPs_installed_DGU_at_500() {
		return PCT_Popular_MPSPs_installed_DGU_at_500;
	}

	public void setPCT_Popular_MPSPs_installed_DGU_at_500(
			double pCT_Popular_MPSPs_installed_DGU_at_500) {
		PCT_Popular_MPSPs_installed_DGU_at_500 = pCT_Popular_MPSPs_installed_DGU_at_500;
	}

	public double getPCT_Popular_MPSPs_enabled_strict_DGU_at_125() {
		return PCT_Popular_MPSPs_enabled_strict_DGU_at_125;
	}

	public void setPCT_Popular_MPSPs_enabled_strict_DGU_at_125(
			double pCT_Popular_MPSPs_enabled_strict_DGU_at_125) {
		PCT_Popular_MPSPs_enabled_strict_DGU_at_125 = pCT_Popular_MPSPs_enabled_strict_DGU_at_125;
	}

	public double getPCT_Popular_MPSPs_enabled_strict_DGU_at_250() {
		return PCT_Popular_MPSPs_enabled_strict_DGU_at_250;
	}

	public void setPCT_Popular_MPSPs_enabled_strict_DGU_at_250(
			double pCT_Popular_MPSPs_enabled_strict_DGU_at_250) {
		PCT_Popular_MPSPs_enabled_strict_DGU_at_250 = pCT_Popular_MPSPs_enabled_strict_DGU_at_250;
	}

	public double getPCT_Popular_MPSPs_enabled_strict_DGU_at_375() {
		return PCT_Popular_MPSPs_enabled_strict_DGU_at_375;
	}

	public void setPCT_Popular_MPSPs_enabled_strict_DGU_at_375(
			double pCT_Popular_MPSPs_enabled_strict_DGU_at_375) {
		PCT_Popular_MPSPs_enabled_strict_DGU_at_375 = pCT_Popular_MPSPs_enabled_strict_DGU_at_375;
	}

	public double getPCT_Popular_MPSPs_enabled_strict_DGU_at_500() {
		return PCT_Popular_MPSPs_enabled_strict_DGU_at_500;
	}

	public void setPCT_Popular_MPSPs_enabled_strict_DGU_at_500(
			double pCT_Popular_MPSPs_enabled_strict_DGU_at_500) {
		PCT_Popular_MPSPs_enabled_strict_DGU_at_500 = pCT_Popular_MPSPs_enabled_strict_DGU_at_500;
	}

	public double getPCT_Popular_MSPs_installed_DGU_at_125() {
		return PCT_Popular_MSPs_installed_DGU_at_125;
	}

	public void setPCT_Popular_MSPs_installed_DGU_at_125(
			double pCT_Popular_MSPs_installed_DGU_at_125) {
		PCT_Popular_MSPs_installed_DGU_at_125 = pCT_Popular_MSPs_installed_DGU_at_125;
	}

	public double getPCT_Popular_MSPs_installed_DGU_at_250() {
		return PCT_Popular_MSPs_installed_DGU_at_250;
	}

	public void setPCT_Popular_MSPs_installed_DGU_at_250(
			double pCT_Popular_MSPs_installed_DGU_at_250) {
		PCT_Popular_MSPs_installed_DGU_at_250 = pCT_Popular_MSPs_installed_DGU_at_250;
	}

	public double getPCT_Popular_MSPs_installed_DGU_at_375() {
		return PCT_Popular_MSPs_installed_DGU_at_375;
	}

	public void setPCT_Popular_MSPs_installed_DGU_at_375(
			double pCT_Popular_MSPs_installed_DGU_at_375) {
		PCT_Popular_MSPs_installed_DGU_at_375 = pCT_Popular_MSPs_installed_DGU_at_375;
	}

	public double getPCT_Popular_MSPs_installed_DGU_at_500() {
		return PCT_Popular_MSPs_installed_DGU_at_500;
	}

	public void setPCT_Popular_MSPs_installed_DGU_at_500(
			double pCT_Popular_MSPs_installed_DGU_at_500) {
		PCT_Popular_MSPs_installed_DGU_at_500 = pCT_Popular_MSPs_installed_DGU_at_500;
	}

	public double getPCT_Popular_MSPs_enabled_strict_DGU_at_125() {
		return PCT_Popular_MSPs_enabled_strict_DGU_at_125;
	}

	public void setPCT_Popular_MSPs_enabled_strict_DGU_at_125(
			double pCT_Popular_MSPs_enabled_strict_DGU_at_125) {
		PCT_Popular_MSPs_enabled_strict_DGU_at_125 = pCT_Popular_MSPs_enabled_strict_DGU_at_125;
	}

	public double getPCT_Popular_MSPs_enabled_strict_DGU_at_250() {
		return PCT_Popular_MSPs_enabled_strict_DGU_at_250;
	}

	public void setPCT_Popular_MSPs_enabled_strict_DGU_at_250(
			double pCT_Popular_MSPs_enabled_strict_DGU_at_250) {
		PCT_Popular_MSPs_enabled_strict_DGU_at_250 = pCT_Popular_MSPs_enabled_strict_DGU_at_250;
	}

	public double getPCT_Popular_MSPs_enabled_strict_DGU_at_375() {
		return PCT_Popular_MSPs_enabled_strict_DGU_at_375;
	}

	public void setPCT_Popular_MSPs_enabled_strict_DGU_at_375(
			double pCT_Popular_MSPs_enabled_strict_DGU_at_375) {
		PCT_Popular_MSPs_enabled_strict_DGU_at_375 = pCT_Popular_MSPs_enabled_strict_DGU_at_375;
	}

	public double getPCT_Popular_MSPs_enabled_strict_DGU_at_500() {
		return PCT_Popular_MSPs_enabled_strict_DGU_at_500;
	}

	public void setPCT_Popular_MSPs_enabled_strict_DGU_at_500(
			double pCT_Popular_MSPs_enabled_strict_DGU_at_500) {
		PCT_Popular_MSPs_enabled_strict_DGU_at_500 = pCT_Popular_MSPs_enabled_strict_DGU_at_500;
	}

	public double getPCT_Unpopular_MPSPs_installed_DGU_at_125() {
		return PCT_Unpopular_MPSPs_installed_DGU_at_125;
	}

	public void setPCT_Unpopular_MPSPs_installed_DGU_at_125(
			double pCT_Unpopular_MPSPs_installed_DGU_at_125) {
		PCT_Unpopular_MPSPs_installed_DGU_at_125 = pCT_Unpopular_MPSPs_installed_DGU_at_125;
	}

	public double getPCT_Unpopular_MPSPs_installed_DGU_at_250() {
		return PCT_Unpopular_MPSPs_installed_DGU_at_250;
	}

	public void setPCT_Unpopular_MPSPs_installed_DGU_at_250(
			double pCT_Unpopular_MPSPs_installed_DGU_at_250) {
		PCT_Unpopular_MPSPs_installed_DGU_at_250 = pCT_Unpopular_MPSPs_installed_DGU_at_250;
	}

	public double getPCT_Unpopular_MPSPs_installed_DGU_at_375() {
		return PCT_Unpopular_MPSPs_installed_DGU_at_375;
	}

	public void setPCT_Unpopular_MPSPs_installed_DGU_at_375(
			double pCT_Unpopular_MPSPs_installed_DGU_at_375) {
		PCT_Unpopular_MPSPs_installed_DGU_at_375 = pCT_Unpopular_MPSPs_installed_DGU_at_375;
	}

	public double getPCT_Unpopular_MPSPs_installed_DGU_at_500() {
		return PCT_Unpopular_MPSPs_installed_DGU_at_500;
	}

	public void setPCT_Unpopular_MPSPs_installed_DGU_at_500(
			double pCT_Unpopular_MPSPs_installed_DGU_at_500) {
		PCT_Unpopular_MPSPs_installed_DGU_at_500 = pCT_Unpopular_MPSPs_installed_DGU_at_500;
	}

	public double getPCT_Unpopular_MPSPs_enabled_strict_DGU_at_125() {
		return PCT_Unpopular_MPSPs_enabled_strict_DGU_at_125;
	}

	public void setPCT_Unpopular_MPSPs_enabled_strict_DGU_at_125(
			double pCT_Unpopular_MPSPs_enabled_strict_DGU_at_125) {
		PCT_Unpopular_MPSPs_enabled_strict_DGU_at_125 = pCT_Unpopular_MPSPs_enabled_strict_DGU_at_125;
	}

	public double getPCT_Unpopular_MPSPs_enabled_strict_DGU_at_250() {
		return PCT_Unpopular_MPSPs_enabled_strict_DGU_at_250;
	}

	public void setPCT_Unpopular_MPSPs_enabled_strict_DGU_at_250(
			double pCT_Unpopular_MPSPs_enabled_strict_DGU_at_250) {
		PCT_Unpopular_MPSPs_enabled_strict_DGU_at_250 = pCT_Unpopular_MPSPs_enabled_strict_DGU_at_250;
	}

	public double getPCT_Unpopular_MPSPs_enabled_strict_DGU_at_375() {
		return PCT_Unpopular_MPSPs_enabled_strict_DGU_at_375;
	}

	public void setPCT_Unpopular_MPSPs_enabled_strict_DGU_at_375(
			double pCT_Unpopular_MPSPs_enabled_strict_DGU_at_375) {
		PCT_Unpopular_MPSPs_enabled_strict_DGU_at_375 = pCT_Unpopular_MPSPs_enabled_strict_DGU_at_375;
	}

	public double getPCT_Unpopular_MPSPs_enabled_strict_DGU_at_500() {
		return PCT_Unpopular_MPSPs_enabled_strict_DGU_at_500;
	}

	public void setPCT_Unpopular_MPSPs_enabled_strict_DGU_at_500(
			double pCT_Unpopular_MPSPs_enabled_strict_DGU_at_500) {
		PCT_Unpopular_MPSPs_enabled_strict_DGU_at_500 = pCT_Unpopular_MPSPs_enabled_strict_DGU_at_500;
	}

	public double getPCT_Unpopular_MSPs_installed_DGU_at_125() {
		return PCT_Unpopular_MSPs_installed_DGU_at_125;
	}

	public void setPCT_Unpopular_MSPs_installed_DGU_at_125(
			double pCT_Unpopular_MSPs_installed_DGU_at_125) {
		PCT_Unpopular_MSPs_installed_DGU_at_125 = pCT_Unpopular_MSPs_installed_DGU_at_125;
	}

	public double getPCT_Unpopular_MSPs_installed_DGU_at_250() {
		return PCT_Unpopular_MSPs_installed_DGU_at_250;
	}

	public void setPCT_Unpopular_MSPs_installed_DGU_at_250(
			double pCT_Unpopular_MSPs_installed_DGU_at_250) {
		PCT_Unpopular_MSPs_installed_DGU_at_250 = pCT_Unpopular_MSPs_installed_DGU_at_250;
	}

	public double getPCT_Unpopular_MSPs_installed_DGU_at_375() {
		return PCT_Unpopular_MSPs_installed_DGU_at_375;
	}

	public void setPCT_Unpopular_MSPs_installed_DGU_at_375(
			double pCT_Unpopular_MSPs_installed_DGU_at_375) {
		PCT_Unpopular_MSPs_installed_DGU_at_375 = pCT_Unpopular_MSPs_installed_DGU_at_375;
	}

	public double getPCT_Unpopular_MSPs_installed_DGU_at_500() {
		return PCT_Unpopular_MSPs_installed_DGU_at_500;
	}

	public void setPCT_Unpopular_MSPs_installed_DGU_at_500(
			double pCT_Unpopular_MSPs_installed_DGU_at_500) {
		PCT_Unpopular_MSPs_installed_DGU_at_500 = pCT_Unpopular_MSPs_installed_DGU_at_500;
	}

	public double getPCT_Unpopular_MSPs_enabled_strict_DGU_at_125() {
		return PCT_Unpopular_MSPs_enabled_strict_DGU_at_125;
	}

	public void setPCT_Unpopular_MSPs_enabled_strict_DGU_at_125(
			double pCT_Unpopular_MSPs_enabled_strict_DGU_at_125) {
		PCT_Unpopular_MSPs_enabled_strict_DGU_at_125 = pCT_Unpopular_MSPs_enabled_strict_DGU_at_125;
	}

	public double getPCT_Unpopular_MSPs_enabled_strict_DGU_at_250() {
		return PCT_Unpopular_MSPs_enabled_strict_DGU_at_250;
	}

	public void setPCT_Unpopular_MSPs_enabled_strict_DGU_at_250(
			double pCT_Unpopular_MSPs_enabled_strict_DGU_at_250) {
		PCT_Unpopular_MSPs_enabled_strict_DGU_at_250 = pCT_Unpopular_MSPs_enabled_strict_DGU_at_250;
	}

	public double getPCT_Unpopular_MSPs_enabled_strict_DGU_at_375() {
		return PCT_Unpopular_MSPs_enabled_strict_DGU_at_375;
	}

	public void setPCT_Unpopular_MSPs_enabled_strict_DGU_at_375(
			double pCT_Unpopular_MSPs_enabled_strict_DGU_at_375) {
		PCT_Unpopular_MSPs_enabled_strict_DGU_at_375 = pCT_Unpopular_MSPs_enabled_strict_DGU_at_375;
	}

	public double getPCT_Unpopular_MSPs_enabled_strict_DGU_at_500() {
		return PCT_Unpopular_MSPs_enabled_strict_DGU_at_500;
	}

	public void setPCT_Unpopular_MSPs_enabled_strict_DGU_at_500(
			double pCT_Unpopular_MSPs_enabled_strict_DGU_at_500) {
		PCT_Unpopular_MSPs_enabled_strict_DGU_at_500 = pCT_Unpopular_MSPs_enabled_strict_DGU_at_500;
	}
}
