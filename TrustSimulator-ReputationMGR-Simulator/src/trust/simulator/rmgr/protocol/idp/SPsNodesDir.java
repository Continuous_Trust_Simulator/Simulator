package trust.simulator.rmgr.protocol.idp;

public class SPsNodesDir {
	
	private int position;
	
	private double rank;
	
	private double usefulness;
	
	private boolean malicious;

	public SPsNodesDir() {
		super();
		
		//this.setPosition(-1);
		//this.setRank(100);

	}
	
	public SPsNodesDir copy()
	{
		SPsNodesDir copy = new SPsNodesDir();
		
		copy.setMalicious(malicious);
		copy.setPosition(position);
		copy.setRank(rank);
		copy.setUsefulness(usefulness);
		
		return copy;
	}


	public int getPosition() {
		return position;
	}


	public void setPosition(int position) {
		this.position = position;
	}


	public double getRank() {
		return rank;
	}


	public void setRank(double rank) {
		this.rank = rank;
	}


	public double getUsefulness() {
		return usefulness;
	}


	public void setUsefulness(double usefulness) {
		this.usefulness = usefulness;
	}


	public boolean isMalicious() {
		return malicious;
	}


	public void setMalicious(boolean malicious) {
		this.malicious = malicious;
	}

}
