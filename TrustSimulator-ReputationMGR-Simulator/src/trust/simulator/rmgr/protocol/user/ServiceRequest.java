package trust.simulator.rmgr.protocol.user;

public class ServiceRequest {
	
	private boolean getService;
	private double minUsefulness;
	private double minRank;
	private Credential credential;
	
	private int SPid;
	private boolean specificSP;
	
	public ServiceRequest()
	{
		
	}
	
	public ServiceRequest(boolean getService, double minUsefulness, double minRank, Credential credential)
	{
		this.getService = getService;
		this.minUsefulness = minUsefulness;
		this.minRank = minRank;
		this.credential = credential;
	}

	public boolean isGetService() {
		return getService;
	}

	public void setGetService(boolean getService) {
		this.getService = getService;
	}

	public double getMinUsefulness() {
		return minUsefulness;
	}

	public void setMinUsefulness(double minUsefulness) {
		this.minUsefulness = minUsefulness;
	}

	public double getMinRank() {
		return minRank;
	}

	public void setMinRank(double minRank) {
		this.minRank = minRank;
	}

	public Credential getCredential() {
		return credential;
	}

	public void setCredential(Credential credential) {
		this.credential = credential;
	}

	public int getSPid() {
		return SPid;
	}

	public void setSPid(int sPid) {
		SPid = sPid;
	}

	public boolean isSpecificSP() {
		return specificSP;
	}

	public void setSpecificSP(boolean specificSP) {
		this.specificSP = specificSP;
	}

}
