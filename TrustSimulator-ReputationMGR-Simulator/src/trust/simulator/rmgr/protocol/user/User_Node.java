package trust.simulator.rmgr.protocol.user;


import java.util.ArrayList;
import java.util.List;

import peersim.core.CommonState;
import peersim.core.Network;

import trust.simulator.rmgr.protocol.RMGR_Node;
import trust.simulator.rmgr.protocol.auditor.Auditor_Node;
import trust.simulator.rmgr.protocol.auditor.CaseSPs;
import trust.simulator.rmgr.protocol.superNode.Super_Node;


public class User_Node extends RMGR_Node{
	
	// ------------------------------------------------------------------------
    // Fields
    // ------------------------------------------------------------------------
	
	private String thePrefix;
	
	private List<Credential> credentials;
	
	// The probability of generating new Credential
	private double GenerateNewCredentialRate;
	
	// The probability of generating new Service Request
	private double GenerateNewServiceRequestRate;
	
	private int credentialsSum;
	
	private double strictCredentialProb;
	
	private double TrustInNetwork;
	
	private double IgnoranceRate;
	
	private boolean StopUsingCredentialAfterSpam;
	
	private int reportedCases;
	
	private double TrustDropPostSpam;
	
	private double TrustDecreasePeriodPostSpam;
	
	private double TrustIncreasePostSpamResolved;
	
	private double TrustIncreaseRate;
	
	private double TrustDecreaseRate;
	
	private double TrustDecreasePeriodPostSpamCounter;
	
	private List<CaseSPs> PSPsInteractedWith;
	
	private boolean STagent;
	// 1 = active; 2 = killed, 3 = killed and stop normal Users activities
	private int STstatus;
	private int STidleTime;
	private int STmaxLifeTime;
	private int STtargetedSPid;
	private int STunimportantCredentialsPool;
	private int STidleTimeCounter;
	private Credential STimportantCredential;
	private boolean STreportSpam;
	
	private boolean GTagent;
	// 1 = active; 2 = killed, 3 = killed and stop normal Users activities
	private int GTstatus;
	private int GTid;
	private int GTidleTime;
	private int GTmaxLifeTime;
	private List<GT_TargetedSP> GTtargetedSPs;
	private int GTunimportantCredentialsPool;
	private int GTidleTimeCounter;
	private Credential GTimportantCredential;
	private boolean GTinteractedWithAllTargeted;
	private boolean GTreportSpam;
	
	public User_Node(String prefix) {
		super(prefix);

		thePrefix = prefix;
		credentialsSum = 0;
		reportedCases = 0;
		TrustDecreasePeriodPostSpamCounter = 0;
		
		PSPsInteractedWith = new ArrayList<CaseSPs>();
	
		STagent = false;
		STstatus = -1;
		STidleTime = -1;
		STmaxLifeTime = -1;
		STtargetedSPid = -1;
		STunimportantCredentialsPool = -1;
		STidleTimeCounter = 0;

		GTagent = false;
		GTstatus = -1;
		GTid = -1;
		GTidleTime = -1;
		GTmaxLifeTime = -1;
		GTtargetedSPs = new ArrayList<GT_TargetedSP>();
		GTunimportantCredentialsPool = -1;
		GTidleTimeCounter = 0;
		GTinteractedWithAllTargeted = false;
	}

	public Object clone() {

        User_Node inp = new User_Node(thePrefix);
        return inp;
    }
	
	public void setCredentialsArray(int CredentialsArraySize)
	{
		credentials = new ArrayList<Credential>(CredentialsArraySize);
	}
	
	public boolean createNewCredential(boolean useProbability, boolean STimportant, boolean GTimportant, int sharingPermission){
		
		if(!useProbability || this.randBoolean(1 - (GenerateNewCredentialRate*TrustInNetwork)/10000))
		{
			Credential credential;
			
			if(sharingPermission >= 0)
				credential = new Credential(credentialsSum, this.nodePos, STimportant, sharingPermission);
		
			else
			{
				boolean strictCredential = randBoolean(1-(strictCredentialProb/100));
		
				if (strictCredential) sharingPermission = 0;
				else
				{
					boolean shareWithTop = randBoolean(TrustInNetwork/100);
					
					if(shareWithTop) sharingPermission = 1;
					else sharingPermission = 2;
				}
				
				credential = new Credential(credentialsSum, this.nodePos, STimportant, sharingPermission);
			}
			
			credentialsSum++;
			
			this.credentials.add(credential);
			
			if(STimportant)
			{
				this.setSTimportantCredential(credential);
				//System.out.println("STagent: "+this.nodePos+" set important Credential ID: "+credential.getId());
			}
			
			if(GTimportant)
				this.setGTimportantCredential(credential);
			
			//System.out.println("New Credential Added: ID = "+credential.getId()+" nodePos = "+credential.getNodePosition());
			//System.out.println("CredetialsSum = "+credentialsSum);
			
			return true;
		}
		else
			return false;
	}
	
	public ServiceRequest generateNewService()
	{
		ServiceRequest serviceRequest = new ServiceRequest();
		
		boolean generateNormalRequest = true;
		
		if( (STagent && STstatus == 3) || (GTagent && GTstatus == 3) )
		{
			serviceRequest.setGetService(false);
			return serviceRequest;
		}
		
		if(STagent && STstatus == 1)
		{
			if(STidleTimeCounter == STidleTime)
			{
				generateNormalRequest = false;
				STidleTimeCounter = 0;
			}
			else
			{
				STidleTimeCounter++;
			}
		}
		
		if(GTagent && GTstatus == 1)
		{
			if(GTidleTimeCounter == GTidleTime)
			{
				generateNormalRequest = false;
				GTidleTimeCounter = 0;
			}
			else
			{
				GTidleTimeCounter++;
			}
		}
		
		if(generateNormalRequest && this.randBoolean(1-((TrustInNetwork*GenerateNewServiceRequestRate)/10000)))
		{
        	//TODO 5 is the number of trials to find a non-compromised credential,
        	//try to optimize it.
        	Credential credential = new Credential();
			if(credentialsSum < 5)
			{
	        	for(int i =0; i < credentialsSum; i++)
	    		{
	    			credential = credentials.get(i);
	    			if(!credential.isCompromised() && !credential.isSTimportant()) break;
	    		}
			}
	    	else
	    	{
	        	for(int i =0; i < 5; i++)
		    	{		
	        		credential = credentials.get(randInt(0, credentialsSum));
	        		if(!credential.isCompromised() && !credential.isSTimportant()) break;
		    	}
	    	}
			
			if ( (StopUsingCredentialAfterSpam && credential.isCompromised()) || (credential.sharingPermission == 0 && credential.isCompromised()))
				serviceRequest.setGetService(false);
			else
			{
				//if(STagent && credential.getId() == STimportantCredential.getId())
					//System.out.println("STagent: "+this.nodePos+" using important Credential ID: "+STimportantCredential.getId()+" to interact with untargeted SPid: ");
				
				serviceRequest.setGetService(true);
				serviceRequest.setCredential(credential);
				
				//TODO try to optimize or make this condition more flexible
	        	boolean getUsefulSP = randBoolean(1 - usefulnessMIN/100);
	        	boolean getHighRankSP = randBoolean(1 - usefulnessMIN/100);
	            double minUsefulness;
	            double minRank;
	            
	            if(GTagent || STagent) getUsefulSP = true;
	            
	        	if(getUsefulSP)
	        	{
	        		//System.out.println("getUsefulSP");
	        		//TODO monitor this condition
	        		//minUsefulness = randDouble(usefulnessMIN, 100);
	        		serviceRequest.setMinUsefulness(usefulnessMIN);
	        	}
	        	else
	        	{
	        		//System.out.println("getUselessSP");
	        		minUsefulness = randDouble(0, usefulnessMIN);
	        		serviceRequest.setMinUsefulness(minUsefulness);
	        	}
	        	if(getHighRankSP)
	        	{
	        		//System.out.println("getUsefulSP");
	        		minRank = randDouble(usefulnessMIN, 100);
	        		serviceRequest.setMinRank(minRank);
	        	}
	        	else
	        	{
	        		//System.out.println("getUselessSP");
	        		minRank = randDouble(0, usefulnessMIN);
	        		serviceRequest.setMinRank(minRank);
	        	}		
				
			}
		}
		else if(!generateNormalRequest)
		{
			if(STagent)
			{
				serviceRequest.setCredential(STimportantCredential);
				serviceRequest.setGetService(true);
				serviceRequest.setSpecificSP(true);
				serviceRequest.setSPid(STtargetedSPid);
			}
			
			else if(GTagent)
			{
				serviceRequest.setCredential(GTimportantCredential);
				serviceRequest.setGetService(true);
				serviceRequest.setSpecificSP(true);
				
				GT_TargetedSP targetedSP =  new GT_TargetedSP();
				
				if(!GTinteractedWithAllTargeted)
				for(int i = 0; i < GTtargetedSPs.size(); i++)
				{
					targetedSP = GTtargetedSPs.get(randInt(0,GTtargetedSPs.size()));
					
					if(!targetedSP.isInteractedWith())
						break;
					
					if(i == GTtargetedSPs.size()-1)
						GTinteractedWithAllTargeted = true;
				}
				else
					targetedSP = GTtargetedSPs.get(randInt(0,GTtargetedSPs.size()));
				
				serviceRequest.setSPid(targetedSP.getSPid());
				targetedSP.setInteractedWith(true);
			}
		}
		else
			serviceRequest.setGetService(false);
		
		return serviceRequest;
	}
	
	public boolean recieveSpam(Credential credential)
	{
		boolean ignored = false;
		
		//System.out.println("");
		//System.out.println("User: "+this.nodePos+" got Credential: "+credential.getId()+" SPAMED!");
		
		if((STagent && STstatus == 3) || (GTagent && GTstatus == 3)) return false;
		
		if(STagent && STstatus == 1 && credential.getId() == STimportantCredential.getId())
		{
			Auditor_Node auditor = (Auditor_Node) Network.get(2).getProtocol(AuditorPid);
			auditor.recieveSTagentReport(STtargetedSPid, nodePos, true);
			
			ignored = true;
			
			//System.out.println("");
			//System.out.println("ST: "+nodePos+" recieved SPAM from "+STtargetedSPid);
			
			//return false;
		}
		
		else if(GTagent && GTstatus == 1 && credential.getId() == GTimportantCredential.getId())
		{
			
			Auditor_Node auditor = (Auditor_Node) Network.get(2).getProtocol(AuditorPid);
			auditor.recieveGTagentReport(GTid, GTtargetedSPs, nodePos, true, PSPsInteractedWith);
			
			//System.out.println("");
			//System.out.println("GT: "+nodePos+" recieved SPAM from targeted SPs!!");
			
			ignored = true;
			
			//return false;
		}
		
		if( (STagent && STstatus != 1 && credential.getId() == STimportantCredential.getId()) || (GTagent && GTstatus != 1 && credential.getId() == GTimportantCredential.getId()) )
			return false;	
		
		//Ignore?
		if((STagent && !STreportSpam) || (GTagent && !GTreportSpam))
			ignored = true;
		else if( randBoolean(1-(IgnoranceRate/100)) && !STagent && !GTagent)
			ignored = true;
		
		for(Credential recordCredential: credentials)
			if(recordCredential.getId() == credential.getId())
			{
				if(!ignored && !recordCredential.isCompromised())
				{
					reportedCases++;
			
					TrustDecreasePeriodPostSpamCounter = TrustDecreasePeriodPostSpamCounter + TrustDecreasePeriodPostSpam;
					
					TrustInNetwork = TrustInNetwork - (TrustInNetwork * (TrustDropPostSpam/100) );
					
					recordCredential.setCompromised(true);
					recordCredential.setIgnoredCompromised(false);
				}
				else if(ignored && !recordCredential.isCompromised())
					recordCredential.setIgnoredCompromised(true);
					
				break;
			}
		
		
		//if(STagent && !ignored)
			//System.out.println("ST node: "+this.nodePos+" reported SPAM!");
		
		return !ignored;
	}
	
	public void recieveCaseClosure(long periodToClose)
	{
		/*
		if(STagent)
		{
			System.out.println("");
			System.out.println("Case Closure! UserId: "+ this.nodePos);
		}
		System.out.println("Pre Trust In Network: "+TrustInNetwork);
		*/
		
		reportedCases--;
		
		if(periodToClose < this.TrustDecreasePeriodPostSpam)
			this.TrustDecreasePeriodPostSpamCounter = this.TrustDecreasePeriodPostSpamCounter - (this.TrustDecreasePeriodPostSpam - periodToClose);
		
		//System.out.println("User: "+this.nodePos+" Reported Cases = "+reportedCases);
		TrustInNetwork = TrustInNetwork + (TrustInNetwork * (TrustIncreasePostSpamResolved/100) );
		//System.out.println("Post Trust In Network: "+TrustInNetwork);
	}
	
	public void updateTrustInNetwork()
	{
		if(reportedCases == 0 || TrustDecreasePeriodPostSpamCounter == 0)
		{
			TrustInNetwork = TrustInNetwork+(TrustInNetwork * (TrustIncreaseRate/100));
		}
		else
		{
			//TrustInNetwork = TrustInNetwork - (TrustInNetwork * (TrustDecreaseRate/100) * reportedCases);
			TrustInNetwork = TrustInNetwork - (TrustInNetwork * (TrustDecreaseRate/100));
			TrustDecreasePeriodPostSpamCounter--;
		}
		
		if(TrustInNetwork > 100)
			TrustInNetwork = 100;

		int UserType = 1;
		if(STagent) UserType = 2;
		else if(GTagent) UserType = 3;
		
		UserCycleRecord UCR = new UserCycleRecord(nodePos, UserType, CommonState.getIntTime(), TrustInNetwork, credentials);
		Super_Node superNode = (Super_Node) Network.get(0).getProtocol(SuperPid);
		superNode.addUCR(UCR);
	}

	public double getGenerateNewCredentialRate() {
		return GenerateNewCredentialRate;
	}

	public void setGenerateNewCredentialRate(double generateNewCredentialRate) {
		GenerateNewCredentialRate = generateNewCredentialRate;
	}

	public double getGenerateNewServiceRequestRate() {
		return GenerateNewServiceRequestRate;
	}

	public void setGenerateNewServiceRequestRate(double generateNewServiceRequestRate) {
		GenerateNewServiceRequestRate = generateNewServiceRequestRate;
	}

	public double getTrustInNetwork() {
		return TrustInNetwork;
	}

	public void setTrustInNetwork(double TrustInNetwork) {
		this.TrustInNetwork = TrustInNetwork;
	}

	public boolean isStopUsingCredentialAfterSpam() {
		return StopUsingCredentialAfterSpam;
	}

	public void setStopUsingCredentialAfterSpam(boolean stopUsingCredentialAfterSpam) {
		StopUsingCredentialAfterSpam = stopUsingCredentialAfterSpam;
	}

	public int getReportedCases() {
		return reportedCases;
	}

	public void setReportedCases(int reportedCases) {
		this.reportedCases = reportedCases;
	}

	public double getTrustDropPostSpam() {
		return TrustDropPostSpam;
	}

	public void setTrustDropPostSpam(double trustDropPostSpam) {
		TrustDropPostSpam = trustDropPostSpam;
	}

	public double getTrustIncreaseRate() {
		return TrustIncreaseRate;
	}

	public void setTrustIncreaseRate(double trustIncreaseRate) {
		TrustIncreaseRate = trustIncreaseRate;
	}

	public double getTrustDecreaseRate() {
		return TrustDecreaseRate;
	}

	public void setTrustDecreaseRate(double trustDecreaseRate) {
		TrustDecreaseRate = trustDecreaseRate;
	}

	public double getTrustIncreasePostSpamResolved() {
		return TrustIncreasePostSpamResolved;
	}

	public void setTrustIncreasePostSpamResolved(
			double trustIncreasePostSpamResolved) {
		TrustIncreasePostSpamResolved = trustIncreasePostSpamResolved;
	}

	public double getTrustDecreasePeriodPostSpam() {
		return TrustDecreasePeriodPostSpam;
	}

	public void setTrustDecreasePeriodPostSpam(double trustDecreasePeriodPostSpam) {
		TrustDecreasePeriodPostSpam = trustDecreasePeriodPostSpam;
	}

	public double getTrustDecreasePeriodPostSpamCounter() {
		return TrustDecreasePeriodPostSpamCounter;
	}

	public void setTrustDecreasePeriodPostSpamCounter(double TrustDecreasePeriodPostSpamCounter) {
		this.TrustDecreasePeriodPostSpamCounter = TrustDecreasePeriodPostSpamCounter;
	}

	public boolean isSTagent() {
		return STagent;
	}

	public void setSTagent(boolean sTagent) {
		STagent = sTagent;
	}

	public int getSTidleTime() {
		return STidleTime;
	}

	public void setSTidleTime(int sTidleTime) {
		STidleTime = sTidleTime;
	}

	public int getSTmaxLifeTime() {
		return STmaxLifeTime;
	}

	public void setSTmaxLifeTime(int sTmaxLifeTime) {
		STmaxLifeTime = sTmaxLifeTime;
	}

	public int getSTtargetedSPid() {
		return STtargetedSPid;
	}

	public void setSTtargetedSPid(int sTtargetedSPid) {
		STtargetedSPid = sTtargetedSPid;
	}

	public int getSTunimportantCredentialsPool() {
		return STunimportantCredentialsPool;
	}

	public void setSTunimportantCredentialsPool(int sTunimportantCredentialsPool) {
		STunimportantCredentialsPool = sTunimportantCredentialsPool;
	}

	public Credential getSTimportantCredential() {
		return STimportantCredential;
	}

	public void setSTimportantCredential(Credential sTimportantCredential) {
		STimportantCredential = sTimportantCredential;
	}

	public int getSTstatus() {
		return STstatus;
	}

	public void setSTstatus(int sTstatus) {
		STstatus = sTstatus;
		
		if(STstatus == 3)
		{
			int UserType = 1;
			if(STagent) UserType = 2;
			else if(GTagent) UserType = 3;
			
			UserCycleRecord UCR = new UserCycleRecord(false, nodePos, UserType, CommonState.getIntTime(), TrustInNetwork, credentials);
			Super_Node superNode = (Super_Node) Network.get(0).getProtocol(SuperPid);
			superNode.addUCR(UCR);
		}
	}

	public double getIgnoranceRate() {
		return IgnoranceRate;
	}

	public void setIgnoranceRate(double ignoranceRate) {
		IgnoranceRate = ignoranceRate;
	}

	public boolean isGTagent() {
		return GTagent;
	}

	public void setGTagent(boolean gTagent) {
		GTagent = gTagent;
	}

	public int getGTstatus() {
		return GTstatus;
	}

	public void setGTstatus(int gTstatus) {
		GTstatus = gTstatus;
		
		if(GTstatus == 3)
		{
			int UserType = 1;
			if(STagent) UserType = 2;
			else if(GTagent) UserType = 3;
			
			UserCycleRecord UCR = new UserCycleRecord(false, nodePos, UserType, CommonState.getIntTime(), TrustInNetwork, credentials);
			Super_Node superNode = (Super_Node) Network.get(0).getProtocol(SuperPid);
			superNode.addUCR(UCR);
		}
	}

	public int getGTmaxLifeTime() {
		return GTmaxLifeTime;
	}

	public void setGTmaxLifeTime(int gTmaxLifeTime) {
		GTmaxLifeTime = gTmaxLifeTime;
	}

	public int getGTidleTime() {
		return GTidleTime;
	}

	public void setGTidleTime(int gTidleTime) {
		GTidleTime = gTidleTime;
	}

	public List<GT_TargetedSP> getGTtargetedSPs() {
		return GTtargetedSPs;
	}

	public void setGTtargetedSPs(List<GT_TargetedSP> GTtargetedSPs) {
		this.GTtargetedSPs = GTtargetedSPs;
	}

	public int getGTunimportantCredentialsPool() {
		return GTunimportantCredentialsPool;
	}

	public void setGTunimportantCredentialsPool(int gTunimportantCredentialsPool) {
		GTunimportantCredentialsPool = gTunimportantCredentialsPool;
	}

	public int getGTidleTimeCounter() {
		return GTidleTimeCounter;
	}

	public void setGTidleTimeCounter(int gTidleTimeCounter) {
		GTidleTimeCounter = gTidleTimeCounter;
	}

	public Credential getGTimportantCredential() {
		return GTimportantCredential;
	}

	public void setGTimportantCredential(Credential gTimportantCredential) {
		GTimportantCredential = gTimportantCredential;
	}

	public int getGTid() {
		return GTid;
	}

	public void setGTid(int gTid) {
		GTid = gTid;
	}

	public boolean isSTreportSpam() {
		return STreportSpam;
	}

	public void setSTreportSpam(boolean sTreportSpam) {
		STreportSpam = sTreportSpam;
	}

	public boolean isGTreportSpam() {
		return GTreportSpam;
	}

	public void setGTreportSpam(boolean gTreportSpam) {
		GTreportSpam = gTreportSpam;
	}

	public List<CaseSPs> getPSPsInteractedWith() {
		return PSPsInteractedWith;
	}

	public void setPSPsInteractedWith(List<CaseSPs> pSPsInteractedWith) {
		PSPsInteractedWith = pSPsInteractedWith;
	}

	public double getStrictCredentialProb() {
		return strictCredentialProb;
	}

	public void setStrictCredentialProb(double strictCredentialProb) {
		this.strictCredentialProb = strictCredentialProb;
	}
	
}
