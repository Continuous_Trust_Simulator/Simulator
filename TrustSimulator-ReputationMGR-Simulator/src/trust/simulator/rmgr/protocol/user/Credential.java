package trust.simulator.rmgr.protocol.user;

public class Credential {
	
	protected int nodePosition;
	protected int id;
	protected boolean compromised;
	protected boolean ignoredCompromised;
	
	// 0 = strictly no sharing; 1 = share with PSPs; 2 = share with anyone ;
	protected int sharingPermission;

	protected boolean STimportant;
	
	public Credential()
	{
		nodePosition = -1;
		id = -1;
		compromised = false;
		sharingPermission = -1;
		STimportant = false;
	}
	
	public Credential(int id, int nodePos, int sharingPermission)
	{
		this.nodePosition = nodePos;
		this.id = id;
		compromised = false;
		this.STimportant = false;
		this.sharingPermission = sharingPermission;
	}
	
	public Credential(int id, int nodePos, boolean STimportant, int sharingPermission)
	{
		this.nodePosition = nodePos;
		this.id = id;
		compromised = false;
		this.STimportant = STimportant;
		this.sharingPermission = sharingPermission;
	}
	
	public int getNodePosition() {
		return nodePosition;
	}
	public void setNodePosition(int nodePosition) {
		this.nodePosition = nodePosition;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isCompromised() {
		return compromised;
	}
	public void setCompromised(boolean compromised) {
		this.compromised = compromised;
	}

	public boolean isSTimportant() {
		return STimportant;
	}

	public void setSTimportant(boolean sTimportant) {
		STimportant = sTimportant;
	}
	
	public int getSharingPermission() {
		return sharingPermission;
	}

	public void setSharingPermission(int sharingPermission) {
		this.sharingPermission = sharingPermission;
	}

	public boolean isIgnoredCompromised() {
		return ignoredCompromised;
	}

	public void setIgnoredCompromised(boolean ignoredCompromised) {
		this.ignoredCompromised = ignoredCompromised;
	}
	
}
