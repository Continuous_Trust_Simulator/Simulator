package trust.simulator.rmgr.protocol.user;

import java.util.List;

public class UserCycleRecord {
	
	private int userPos;
	private int userType;
	private int cycle;
	
	private boolean active;
	
	private double currentTrust;
	
	private double credentialsTotal;
	
	private double compromisedCredentialsTotal;
	private double strictCredentialsTotal;
	private double compromisedStrictCredentialsTotal;
	private double shareWithTopCredentialsTotal;
	private double compromisedShareWithTopCredentialsTotal;
	private double shareWithAnyCredentialsTotal;
	private double compromisedShareWithAnyCredentialsTotal;
	
	private double ignoredCompromisedCredentialsTotal;
	private double ignoredCompromisedStrictCredentialsTotal;
	private double ignoredCompromisedShareWithTopCredentialsTotal;
	private double ignoredCompromisedShareWithAnyCredentialsTotal;
	
	
	
	public UserCycleRecord()
	{
		this.userPos = -1;
		this.userType = -1;
		this.cycle = -1;
		this.currentTrust = -1;
		this.active = true;
		
		this.credentialsTotal = -1;
		this.compromisedCredentialsTotal = -1;
		this.strictCredentialsTotal = -1;
		this.compromisedStrictCredentialsTotal = -1;
		this.shareWithTopCredentialsTotal = -1;
		this.compromisedShareWithTopCredentialsTotal = -1;
		this.shareWithAnyCredentialsTotal = -1;
		this.compromisedShareWithAnyCredentialsTotal = -1;
		
		this.ignoredCompromisedCredentialsTotal = -1;
		this.ignoredCompromisedStrictCredentialsTotal = -1;
		this.ignoredCompromisedShareWithTopCredentialsTotal = -1;
		this.ignoredCompromisedShareWithAnyCredentialsTotal = -1;
				
	}
	
	public UserCycleRecord(int userPos, int userType, int cycle, double currentTrust, List<Credential> credentials) {
	
		this.active = true;
		
		this.userPos = userPos;
		this.userType = userType;
		this.cycle = cycle;
		this.currentTrust = currentTrust;
		
		this.credentialsTotal = 0;
		this.compromisedCredentialsTotal = 0;
		this.strictCredentialsTotal = 0;
		this.compromisedStrictCredentialsTotal = 0;
		this.shareWithTopCredentialsTotal = 0;
		this.compromisedShareWithTopCredentialsTotal = 0;
		this.shareWithAnyCredentialsTotal = 0;
		this.compromisedShareWithAnyCredentialsTotal = 0;
		
		this.ignoredCompromisedCredentialsTotal = 0;
		this.ignoredCompromisedStrictCredentialsTotal = 0;
		this.ignoredCompromisedShareWithTopCredentialsTotal = 0;
		this.ignoredCompromisedShareWithAnyCredentialsTotal = 0;
		
		processCredentials(credentials);

	}
	
	public UserCycleRecord(boolean active, int userPos, int userType, int cycle, double currentTrust, List<Credential> credentials) {
		
		this.active = active;
		
		this.userPos = userPos;
		this.userType = userType;
		this.cycle = cycle;
		this.currentTrust = currentTrust;
		
		processCredentials(credentials);
		
	}

	
	private void processCredentials(List<Credential> credentials)
	{
		for(Credential credential: credentials)
		{
			this.credentialsTotal++;
			if(credential.sharingPermission == 0) this.strictCredentialsTotal++;
			if(credential.sharingPermission == 1) this.shareWithTopCredentialsTotal++;
			if(credential.sharingPermission == 2) this.shareWithAnyCredentialsTotal++;
			
			if(credential.isCompromised())
			{
				this.compromisedCredentialsTotal++;
				
				if(credential.sharingPermission == 0) this.compromisedStrictCredentialsTotal++;
				if(credential.sharingPermission == 1) this.compromisedShareWithTopCredentialsTotal++;
				if(credential.sharingPermission == 2) this.compromisedShareWithAnyCredentialsTotal++;
			}
			else if(credential.isIgnoredCompromised())
			{
				this.ignoredCompromisedCredentialsTotal++;
				
				if(credential.sharingPermission == 0) this.ignoredCompromisedStrictCredentialsTotal++;
				if(credential.sharingPermission == 1) this.ignoredCompromisedShareWithTopCredentialsTotal++;
				if(credential.sharingPermission == 2) this.ignoredCompromisedShareWithAnyCredentialsTotal++;
			}
		}
	}
	
	public int getUserPos() {
		return userPos;
	}

	public void setUserPos(int userPos) {
		this.userPos = userPos;
	}

	public int getUserType() {
		return userType;
	}

	public void setUserType(int userType) {
		this.userType = userType;
	}

	public int getCycle() {
		return cycle;
	}

	public void setCycle(int cycle) {
		this.cycle = cycle;
	}

	public double getCurrentTrust() {
		return currentTrust;
	}

	public void setCurrentTrust(double currentTrust) {
		this.currentTrust = currentTrust;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public double getCredentialsTotal() {
		return credentialsTotal;
	}

	public void setCredentialsTotal(double credentialsTotal) {
		this.credentialsTotal = credentialsTotal;
	}

	public double getCompromisedCredentialsTotal() {
		return compromisedCredentialsTotal;
	}

	public void setCompromisedCredentialsTotal(double compromisedCredentialsTotal) {
		this.compromisedCredentialsTotal = compromisedCredentialsTotal;
	}

	public double getStrictCredentialsTotal() {
		return strictCredentialsTotal;
	}

	public void setStrictCredentialsTotal(double strictCredentialsTotal) {
		this.strictCredentialsTotal = strictCredentialsTotal;
	}

	public double getCompromisedStrictCredentialsTotal() {
		return compromisedStrictCredentialsTotal;
	}

	public void setCompromisedStrictCredentialsTotal(
			double compromisedStrictCredentialsTotal) {
		this.compromisedStrictCredentialsTotal = compromisedStrictCredentialsTotal;
	}

	public double getShareWithTopCredentialsTotal() {
		return shareWithTopCredentialsTotal;
	}

	public void setShareWithTopCredentialsTotal(double shareWithTopCredentialsTotal) {
		this.shareWithTopCredentialsTotal = shareWithTopCredentialsTotal;
	}

	public double getCompromisedShareWithTopCredentialsTotal() {
		return compromisedShareWithTopCredentialsTotal;
	}

	public void setCompromisedShareWithTopCredentialsTotal(
			double compromisedShareWithTopCredentialsTotal) {
		this.compromisedShareWithTopCredentialsTotal = compromisedShareWithTopCredentialsTotal;
	}

	public double getShareWithAnyCredentialsTotal() {
		return shareWithAnyCredentialsTotal;
	}

	public void setShareWithAnyCredentialsTotal(double shareWithAnyCredentialsTotal) {
		this.shareWithAnyCredentialsTotal = shareWithAnyCredentialsTotal;
	}

	public double getCompromisedShareWithAnyCredentialsTotal() {
		return compromisedShareWithAnyCredentialsTotal;
	}

	public void setCompromisedShareWithAnyCredentialsTotal(
			double compromisedShareWithAnyCredentialsTotal) {
		this.compromisedShareWithAnyCredentialsTotal = compromisedShareWithAnyCredentialsTotal;
	}

	public double getIgnoredCompromisedCredentialsTotal() {
		return ignoredCompromisedCredentialsTotal;
	}

	public void setIgnoredCompromisedCredentialsTotal(
			double ignoredCompromisedCredentialsTotal) {
		this.ignoredCompromisedCredentialsTotal = ignoredCompromisedCredentialsTotal;
	}

	public double getIgnoredCompromisedStrictCredentialsTotal() {
		return ignoredCompromisedStrictCredentialsTotal;
	}

	public void setIgnoredCompromisedStrictCredentialsTotal(
			double ignoredCompromisedStrictCredentialsTotal) {
		this.ignoredCompromisedStrictCredentialsTotal = ignoredCompromisedStrictCredentialsTotal;
	}

	public double getIgnoredCompromisedShareWithTopCredentialsTotal() {
		return ignoredCompromisedShareWithTopCredentialsTotal;
	}

	public void setIgnoredCompromisedShareWithTopCredentialsTotal(
			double ignoredCompromisedShareWithTopCredentialsTotal) {
		this.ignoredCompromisedShareWithTopCredentialsTotal = ignoredCompromisedShareWithTopCredentialsTotal;
	}

	public double getIgnoredCompromisedShareWithAnyCredentialsTotal() {
		return ignoredCompromisedShareWithAnyCredentialsTotal;
	}

	public void setIgnoredCompromisedShareWithAnyCredentialsTotal(
			double ignoredCompromisedShareWithAnyCredentialsTotal) {
		this.ignoredCompromisedShareWithAnyCredentialsTotal = ignoredCompromisedShareWithAnyCredentialsTotal;
	}

}
