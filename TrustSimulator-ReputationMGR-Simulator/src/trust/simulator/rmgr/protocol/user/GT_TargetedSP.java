package trust.simulator.rmgr.protocol.user;

public class GT_TargetedSP {
	
	private int SPid = -1;
	
	private boolean interactedWith = false;

	public int getSPid() {
		return SPid;
	}

	public void setSPid(int sPid) {
		SPid = sPid;
	}

	public boolean isInteractedWith() {
		return interactedWith;
	}

	public void setInteractedWith(boolean interactedWith) {
		this.interactedWith = interactedWith;
	}

}
