package trust.simulator.rmgr.protocol;

import java.util.Random;

import peersim.core.Protocol;
import trust.simulator.rmgr.networkGenerator.SimulationInitializer;


public class RMGR_Node implements Protocol {

	
    // ------------------------------------------------------------------------
    // Fields
    // ------------------------------------------------------------------------

    
	/** The nodeType. Possible values are:
	 * 0 = Super_Node
	 * 1 = IDP_Node
	 * 2 = Auditor_Node
	 * 3 = User_Node
	 * 4 = SP_Node
	 * 5 = ME_Node
	 * 
	 */
	protected int nodeType;
			
	/** nodePos: this variable indicates the position of this node in the network 
	 * 
	 */
	protected int nodePos;
	
	protected String thePrefix;
		
	protected Random rGen;
	
    protected double usefulnessMIN;
    
    protected int creationCycle;
    
    protected int RMGRPid;
    
    protected int SuperPid;
    
    protected int IDPPid;
    
	protected int AuditorPid;
    
    protected int UserPid;
    
    protected int SPPid;
    
    protected int MEPid;
		
	// ------------------------------------------------------------------------
    // Constructor
    // ------------------------------------------------------------------------
    /**
     * Standard constructor that reads the configuration parameters. Invoked by
     * the simulation engine. By default, all the coordinates components are set
     * to -1 value. The {@link SimulationInitializer} class provides a coordinates
     * initialization.
     * 
     * @param prefix
     *            the configuration prefix for this class.
     */
    public RMGR_Node(String prefix) {
        
    	thePrefix = prefix;
        
    	nodeType = -1;
    }

    public Object clone() {

        RMGR_Node inp = new RMGR_Node(thePrefix);
       
        return inp;
    }
        
    /**
     * Returns a random boolean value with probability p
     * of being false and probability 1-p of being true
     * p should be in the range 0.0 - 1.0
     * @author: http://www.refactory.org/s/generate_random_boolean_values_with_probability/view/latest 
     */
    public boolean randBoolean(double p)
    {
        //System.out.println("P = "+p);
    	return (rGen.nextDouble() < (1 - p));
    }
    
    /**
     * Returns a psuedo-random number between min and max, inclusive.
     * The difference between min and max can be at most
     * <code>Integer.MAX_VALUE - 1</code>.
     *
     * @param min Minimim value
     * @param max Maximim value.  Must be greater than min.
     * @return Double between min and max, inclusive.
     * @see java.util.Random#nextDouble()
     */
    public double randDouble(double min, double max) {
    	
    	return min + rGen.nextDouble() * (max - min);
    }
    
public int randInt(int min, int max) {
	
    	return min + rGen.nextInt(max-min);
    }

    public int getNodeType() {
		return nodeType;
	}

	public void setNodeType(int nodeType) {
		this.nodeType = nodeType;
	}

	public int getNodePos() {
		return nodePos;
	}

	public void setNodePos(int nodePos) {
		this.nodePos = nodePos;
	}

	public Random getrGen() {
		return rGen;
	}

	public void setrGen(Random rGen) {
		this.rGen = rGen;
	}

	public double getUsefulnessMIN() {
		return usefulnessMIN;
	}

	public void setUsefulnessMIN(double usefulnessMIN) {
		this.usefulnessMIN = usefulnessMIN;
	}
	
    public int getCreationCycle() {
		return creationCycle;
	}

	public void setCreationCycle(int creationCycle) {
		this.creationCycle = creationCycle;
	}

	public int getSuperPid() {
		return SuperPid;
	}

	public void setSuperPid(int superPid) {
		SuperPid = superPid;
	}

	public int getAuditorPid() {
		return AuditorPid;
	}

	public void setAuditorPid(int auditorPid) {
		AuditorPid = auditorPid;
	}

	public int getUserPid() {
		return UserPid;
	}

	public void setUserPid(int userPid) {
		UserPid = userPid;
	}

	public int getIDPPid() {
		return IDPPid;
	}

	public void setIDPPid(int iDPPid) {
		IDPPid = iDPPid;
	}

	public int getSPPid() {
		return SPPid;
	}

	public void setSPPid(int sPPid) {
		SPPid = sPPid;
	}

	public int getRMGRPid() {
		return RMGRPid;
	}

	public void setRMGRPid(int rMGRPid) {
		RMGRPid = rMGRPid;
	}

	public int getMEPid() {
		return MEPid;
	}

	public void setMEPid(int mEPid) {
		MEPid = mEPid;
	}

}
