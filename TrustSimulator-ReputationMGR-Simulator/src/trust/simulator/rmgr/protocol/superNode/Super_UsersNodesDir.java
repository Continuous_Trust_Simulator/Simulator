package trust.simulator.rmgr.protocol.superNode;

import java.util.ArrayList;
import java.util.List;

import trust.simulator.rmgr.protocol.user.UserCycleRecord;


public class Super_UsersNodesDir {
	
	private int position = -1;
	
	// 1: normal, 2: ST, 3: GT
	private int type = -1;
	
	private List<UserCycleRecord> UCRs = new ArrayList<UserCycleRecord>(1000);
	
	private double currentTrust = -1;
	
	private boolean active = true;

	public Super_UsersNodesDir() {
		super();
	}
	
	public void addUCR(UserCycleRecord UCR)
	{
		if(UCR.isActive())
		{
			UCRs.add(UCR);
			currentTrust = UCR.getCurrentTrust();
		}
		else if(active) active = false;
	}


	public int getPosition() {
		return position;
	}


	public void setPosition(int position) {
		this.position = position;
	}


	public int getType() {
		return type;
	}


	public void setType(int type) {
		this.type = type;
	}


	public List<UserCycleRecord> getUCRs() {
		return UCRs;
	}


	public void setUCRs(List<UserCycleRecord> UCRs) {
		this.UCRs = UCRs;
	}

	public double getCurrentTrust() {
		return currentTrust;
	}

	public void setCurrentTrust(double currentTrust) {
		this.currentTrust = currentTrust;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

}
