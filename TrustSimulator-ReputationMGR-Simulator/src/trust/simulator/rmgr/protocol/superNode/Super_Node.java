package trust.simulator.rmgr.protocol.superNode;

import java.util.ArrayList;
import java.util.List;

import doe.Factor;

import peersim.core.CommonState;
import trust.simulator.rmgr.protocol.RMGR_Node;
import trust.simulator.rmgr.protocol.auditor.Auditor_GTNodesDir;
import trust.simulator.rmgr.protocol.auditor.Auditor_STNodesDir;
import trust.simulator.rmgr.protocol.me.ME_NodesToAdd;
import trust.simulator.rmgr.protocol.user.UserCycleRecord;


public class Super_Node extends RMGR_Node{
	
	private int trial;
	
	private List<Factor> FactorsList;
	
	private double OfferDGUProb;
	private double OfferStrictDGUProb;
	
	private double UsersGR;
	private double PSPsGR;
	private double SPsGR;
	private double MPSPsGR;
	private double MSPsGR;
	private double MEGR;

	private double UsersCount;
	private double PSPsCount;
	private double SPsCount;
	private double MPSPsCount;
	private double MSPsCount;
	private double MECount;
	
	private double UnAddedUsersCount;
	private double UnAddedPSPsCount;
	private double UnAddedSPsCount;
	private double UnAddedMPSPsCount;
	private double UnAddedMSPsCount;
	private double UnAddedMECount;

	private List<Auditor_STNodesDir> STtoAdd;
	
	private List<Auditor_GTNodesDir> GTtoAdd;
	
	private List<ME_NodesToAdd> MENodesToAdd;
	
	private List<Super_UsersNodesDir> uNodes;
	
	private List<Super_UsersNodesDir> uAgentNodes;
	
	private Super_UsersNodesDir avgUser;
	
	private Super_UsersNodesDir avgAgentUser;
	
	private double credentialsTotal = 0;
	private double compromisedCredentialsTotal = 0;
	private double strictCredentialsTotal = 0;
	private double compromisedStrictCredentialsTotal = 0;
	private double shareWithTopCredentialsTotal = 0;
	private double compromisedShareWithTopCredentialsTotal = 0;
	private double shareWithAnyCredentialsTotal = 0;
	private double compromisedShareWithAnyCredentialsTotal = 0;
	
	private double ignoredCompromisedCredentialsTotal = 0;
	private double ignoredCompromisedStrictCredentialsTotal = 0;
	private double ignoredCompromisedShareWithTopCredentialsTotal = 0;
	private double ignoredCompromisedShareWithAnyCredentialsTotal = 0;
	
	private double agentCredentialsTotal = 0;
	private double agentCompromisedCredentialsTotal = 0;
	private double agentStrictCredentialsTotal = 0;
	private double agentCompromisedStrictCredentialsTotal = 0;
	private double agentShareWithTopCredentialsTotal = 0;
	private double agentCompromisedShareWithTopCredentialsTotal = 0;
	private double agentShareWithAnyCredentialsTotal = 0;
	private double agentCompromisedShareWithAnyCredentialsTotal = 0;
	
    private double PCT_Active_Agents_to_Users_at_125 = 0;
    private double PCT_Active_Agents_to_Users_at_250 = 0;
    private double PCT_Active_Agents_to_Users_at_375 = 0;
    private double PCT_Active_Agents_to_Users_at_500 = 0;
	
	//TODO optimize sizes of arrays
	public Super_Node(String prefix) {
		super(prefix);
		thePrefix = prefix;
		STtoAdd = new ArrayList<Auditor_STNodesDir>(20);
		GTtoAdd = new ArrayList<Auditor_GTNodesDir>(20);
		MENodesToAdd = new ArrayList<ME_NodesToAdd>(20);
		uNodes = new ArrayList<Super_UsersNodesDir>(2000);
		uAgentNodes = new ArrayList<Super_UsersNodesDir>(2000);
		avgUser = new Super_UsersNodesDir();
		avgAgentUser = new Super_UsersNodesDir();
	}
	
	public void updatePCT_Agents_to_Users(int cycle)
	{
		switch(cycle){
		
		case 125:
			
			PCT_Active_Agents_to_Users_at_125 = uAgentNodes.size()*100/(uNodes.size()+uAgentNodes.size());
			
			break;
			
		case 250:

			PCT_Active_Agents_to_Users_at_250 = uAgentNodes.size()*100/(uNodes.size()+uAgentNodes.size());
			
			break;
			
		case 375:

			PCT_Active_Agents_to_Users_at_375 = uAgentNodes.size()*100/(uNodes.size()+uAgentNodes.size());
			
			break;
			
		case 500:

			PCT_Active_Agents_to_Users_at_500 = uAgentNodes.size()*100/(uNodes.size()+uAgentNodes.size());
			
			break;
		}
	}

	public void updateAvgUser()
	{
		updateAvgUser(false);
		updateAvgUser(true);
	}
	
	public void updateAvgUser(boolean agent)
	{	
		List<Super_UsersNodesDir> nodesDir;
		Super_UsersNodesDir avgUserNode;
		
		double sumUsers = 0;
		double sumTrust = 0;
		
		double temp_credentialsTotal = 0;
		double temp_strictCredentialsTotal = 0;
		double temp_shareWithTopCredentialsTotal = 0;
		double temp_shareWithAnyCredentialsTotal = 0;
		
		double temp_compromisedCredentialsTotal = 0;
		double temp_compromisedStrictCredentialsTotal = 0;
		double temp_compromisedShareWithTopCredentialsTotal = 0;
		double temp_compromisedShareWithAnyCredentialsTotal = 0;
		
		if(agent)
		{
			nodesDir = uAgentNodes;
			avgUserNode = avgAgentUser;
		}
		else
		{
			nodesDir = uNodes;
			avgUserNode = avgUser;
			
			ignoredCompromisedCredentialsTotal = 0;
			ignoredCompromisedStrictCredentialsTotal = 0;
			ignoredCompromisedShareWithTopCredentialsTotal = 0;
			ignoredCompromisedShareWithAnyCredentialsTotal = 0;
		}
			
		
		for(Super_UsersNodesDir user: nodesDir)
		{
			if((agent && user.isActive() && user.getType() != 1) || (!agent && user.getType() ==1))
			{
				sumUsers++;
				
				sumTrust = sumTrust + user.getCurrentTrust();
				
				UserCycleRecord UCR = user.getUCRs().get(user.getUCRs().size()-1);
				
				temp_credentialsTotal = temp_credentialsTotal + UCR.getCredentialsTotal();
				temp_compromisedCredentialsTotal = temp_compromisedCredentialsTotal + UCR.getCompromisedCredentialsTotal();
				temp_strictCredentialsTotal = temp_strictCredentialsTotal + UCR.getStrictCredentialsTotal();
				temp_compromisedStrictCredentialsTotal = temp_compromisedStrictCredentialsTotal + UCR.getCompromisedStrictCredentialsTotal();
				temp_shareWithTopCredentialsTotal = temp_shareWithTopCredentialsTotal + UCR.getShareWithTopCredentialsTotal();
				temp_compromisedShareWithTopCredentialsTotal = temp_compromisedShareWithTopCredentialsTotal + UCR.getCompromisedShareWithTopCredentialsTotal();
				temp_shareWithAnyCredentialsTotal = temp_shareWithAnyCredentialsTotal + UCR.getShareWithAnyCredentialsTotal();
				temp_compromisedShareWithAnyCredentialsTotal = temp_compromisedShareWithAnyCredentialsTotal + UCR.getCompromisedShareWithAnyCredentialsTotal();
				
				if (!agent && user.getType() ==1)
				{
					ignoredCompromisedCredentialsTotal = ignoredCompromisedCredentialsTotal + UCR.getIgnoredCompromisedCredentialsTotal();
					ignoredCompromisedStrictCredentialsTotal = ignoredCompromisedStrictCredentialsTotal + UCR.getIgnoredCompromisedStrictCredentialsTotal();
					ignoredCompromisedShareWithTopCredentialsTotal = ignoredCompromisedShareWithTopCredentialsTotal + UCR.getIgnoredCompromisedShareWithTopCredentialsTotal();
					ignoredCompromisedShareWithAnyCredentialsTotal = ignoredCompromisedShareWithAnyCredentialsTotal + UCR.getIgnoredCompromisedShareWithAnyCredentialsTotal();
				}
			}
		}
		
		UserCycleRecord UCR = new UserCycleRecord();
		UCR.setCycle(CommonState.getIntTime());
		
		UCR.setCurrentTrust(sumTrust/sumUsers);
		
		UCR.setCredentialsTotal(temp_credentialsTotal/sumUsers);
		UCR.setCompromisedCredentialsTotal(temp_compromisedCredentialsTotal/sumUsers);
		UCR.setStrictCredentialsTotal(temp_strictCredentialsTotal/sumUsers);
		UCR.setCompromisedStrictCredentialsTotal(temp_compromisedStrictCredentialsTotal/sumUsers);
		UCR.setShareWithTopCredentialsTotal(temp_shareWithTopCredentialsTotal/sumUsers);
		UCR.setCompromisedShareWithTopCredentialsTotal(temp_compromisedShareWithTopCredentialsTotal/sumUsers);
		UCR.setShareWithAnyCredentialsTotal(temp_shareWithAnyCredentialsTotal/sumUsers);
		UCR.setCompromisedShareWithAnyCredentialsTotal(temp_compromisedShareWithAnyCredentialsTotal/sumUsers);
		
		if(agent)
		{			
			agentCredentialsTotal = temp_credentialsTotal;
			agentCompromisedCredentialsTotal = temp_compromisedCredentialsTotal;
			agentStrictCredentialsTotal = temp_strictCredentialsTotal;
			agentCompromisedStrictCredentialsTotal = temp_compromisedStrictCredentialsTotal;
			agentShareWithTopCredentialsTotal = temp_shareWithTopCredentialsTotal;
			agentCompromisedShareWithTopCredentialsTotal = temp_compromisedShareWithTopCredentialsTotal;
			agentShareWithAnyCredentialsTotal = temp_shareWithAnyCredentialsTotal;
			agentCompromisedShareWithAnyCredentialsTotal = temp_compromisedShareWithAnyCredentialsTotal;
		}
		else
		{	
			credentialsTotal = temp_credentialsTotal;
			compromisedCredentialsTotal = temp_compromisedCredentialsTotal;
			strictCredentialsTotal = temp_strictCredentialsTotal;
			compromisedStrictCredentialsTotal = temp_compromisedStrictCredentialsTotal;
			shareWithTopCredentialsTotal = temp_shareWithTopCredentialsTotal;
			compromisedShareWithTopCredentialsTotal = temp_compromisedShareWithTopCredentialsTotal;
			shareWithAnyCredentialsTotal = temp_shareWithAnyCredentialsTotal;
			compromisedShareWithAnyCredentialsTotal = temp_compromisedShareWithAnyCredentialsTotal;
			
			double trustIncreaseRate = UCR.getCurrentTrust() - avgUserNode.getCurrentTrust();
			//System.out.println("trustIncreaseRate = "+trustIncreaseRate);
			
			if(trustIncreaseRate > 0)
			{
				UsersGR = UsersGR + (UsersGR * trustIncreaseRate/100);
				SPsGR = SPsGR + (SPsGR * trustIncreaseRate/100);
				PSPsGR = PSPsGR + (PSPsGR * trustIncreaseRate/100);
			}
		}
		
		avgUserNode.addUCR(UCR);
	}

	public void addUCR(UserCycleRecord UCR)
	{
		List<Super_UsersNodesDir> nodesDir;
		
		if(UCR.getUserType() == 1)
			nodesDir = uNodes;	
		else
			nodesDir = uAgentNodes;
		
		boolean userFound = false;
		
		for(Super_UsersNodesDir uNode: nodesDir)
		{
			if(UCR.getUserPos() == uNode.getPosition())
			{
				userFound = true;
				uNode.addUCR(UCR);
				break;
			}
		}
		if(!userFound)
		{
			Super_UsersNodesDir newUnode = new Super_UsersNodesDir();
			newUnode.setPosition(UCR.getUserPos());
			newUnode.setType(UCR.getUserType());
			newUnode.addUCR(UCR);
			nodesDir.add(newUnode);
		}
	}
	
	public Object clone() {

        Super_Node inp = new Super_Node(thePrefix);
        return inp;
    }

	public double getUsersGR() {
		return UsersGR;
	}

	public void setUsersGR(double UsersGR) {
		this.UsersGR = UsersGR;
	}

	public double getPSPsGR() {
		return PSPsGR;
	}

	public void setPSPsGR(double PSPsGR) {
		this.PSPsGR = PSPsGR;
	}

	public double getSPsGR() {
		return SPsGR;
	}

	public void setSPsGR(double SPsGR) {
		this.SPsGR = SPsGR;
	}

	public double getMPSPsGR() {
		return MPSPsGR;
	}

	public void setMPSPsGR(double MPSPsGR) {
		this.MPSPsGR = MPSPsGR;
	}

	public double getMSPsGR() {
		return MSPsGR;
	}

	public void setMSPsGR(double MSPsGR) {
		this.MSPsGR = MSPsGR;
	}

	public double getUsersCount() {
		return UsersCount;
	}

	public void setUsersCount(double UsersCount) {
		this.UsersCount = UsersCount;
	}

	public double getPSPsCount() {
		return PSPsCount;
	}

	public void setPSPsCount(double PSPsCount) {
		this.PSPsCount = PSPsCount;
	}

	public double getSPsCount() {
		return SPsCount;
	}

	public void setSPsCount(double SPsCount) {
		this.SPsCount = SPsCount;
	}

	public double getMPSPsCount() {
		return MPSPsCount;
	}

	public void setMPSPsCount(double MPSPsCount) {
		this.MPSPsCount = MPSPsCount;
	}

	public double getMSPsCount() {
		return MSPsCount;
	}

	public void setMSPsCount(double MSPsCount) {
		this.MSPsCount = MSPsCount;
	}

	public List<Auditor_STNodesDir> getSTtoAdd() {
		return STtoAdd;
	}

	public void createSTs(List<Auditor_STNodesDir> STtoAdd) {

		for(Auditor_STNodesDir sttoAdd: STtoAdd)
		{
			this.STtoAdd.add(sttoAdd);
		}
		
	}
	
	public void resetSTtoAdd()
	{
		STtoAdd.clear();
	}

	public double getMEGR() {
		return MEGR;
	}

	public void setMEGR(double mEGR) {
		MEGR = mEGR;
	}

	public double getMECount() {
		return MECount;
	}

	public void setMECount(double mECount) {
		MECount = mECount;
	}

	public double getUnAddedUsersCount() {
		return UnAddedUsersCount;
	}

	public void setUnAddedUsersCount(double unAddedUsersCount) {
		UnAddedUsersCount = unAddedUsersCount;
	}

	public double getUnAddedPSPsCount() {
		return UnAddedPSPsCount;
	}

	public void setUnAddedPSPsCount(double unAddedPSPsCount) {
		UnAddedPSPsCount = unAddedPSPsCount;
	}

	public double getUnAddedSPsCount() {
		return UnAddedSPsCount;
	}

	public void setUnAddedSPsCount(double unAddedSPsCount) {
		UnAddedSPsCount = unAddedSPsCount;
	}

	public double getUnAddedMPSPsCount() {
		return UnAddedMPSPsCount;
	}

	public void setUnAddedMPSPsCount(double unAddedMPSPsCount) {
		UnAddedMPSPsCount = unAddedMPSPsCount;
	}

	public double getUnAddedMSPsCount() {
		return UnAddedMSPsCount;
	}

	public void setUnAddedMSPsCount(double unAddedMSPsCount) {
		UnAddedMSPsCount = unAddedMSPsCount;
	}

	public double getUnAddedMECount() {
		return UnAddedMECount;
	}

	public void setUnAddedMECount(double unAddedMECount) {
		UnAddedMECount = unAddedMECount;
	}

	public List<ME_NodesToAdd> getMENodesToAdd() {
		return MENodesToAdd;
	}

	public void createMEnodes(List<ME_NodesToAdd> MENodesToAdd) {

		for(ME_NodesToAdd meNodesToAdd: MENodesToAdd)
		{
			this.MENodesToAdd.add(meNodesToAdd);
		}
		
	}
	
	public void resetMENodestoAdd()
	{
		MENodesToAdd.clear();
	}

	public List<Auditor_GTNodesDir> getGTtoAdd() {
		return GTtoAdd;
	}

	public void setGTtoAdd(List<Auditor_GTNodesDir> gTtoAdd) {
		GTtoAdd = gTtoAdd;
	}
	
	public void resetGTtoAdd()
	{
		GTtoAdd.clear();
	}

	public void createGTs(List<Auditor_GTNodesDir> GTtoAdd) {

		for(Auditor_GTNodesDir gttoAdd: GTtoAdd)
		{
			this.GTtoAdd.add(gttoAdd);
		}
		
	}

	public List<Super_UsersNodesDir> getuNodes() {
		return uNodes;
	}

	public void setuNodes(List<Super_UsersNodesDir> uNodes) {
		this.uNodes = uNodes;
	}

	public Super_UsersNodesDir getAvgUser() {
		return avgUser;
	}

	public void setAvgUser(Super_UsersNodesDir avgUser) {
		this.avgUser = avgUser;
	}

	public double getCredentialsTotal() {
		return credentialsTotal;
	}

	public void setCredentialsTotal(double credentialsTotal) {
		this.credentialsTotal = credentialsTotal;
	}

	public double getCompromisedCredentialsTotal() {
		return compromisedCredentialsTotal;
	}

	public void setCompromisedCredentialsTotal(double compromisedCredentialsTotal) {
		this.compromisedCredentialsTotal = compromisedCredentialsTotal;
	}

	public double getStrictCredentialsTotal() {
		return strictCredentialsTotal;
	}

	public void setStrictCredentialsTotal(double strictCredentialsTotal) {
		this.strictCredentialsTotal = strictCredentialsTotal;
	}

	public double getCompromisedStrictCredentialsTotal() {
		return compromisedStrictCredentialsTotal;
	}

	public void setCompromisedStrictCredentialsTotal(
			double compromisedStrictCredentialsTotal) {
		this.compromisedStrictCredentialsTotal = compromisedStrictCredentialsTotal;
	}

	public double getShareWithTopCredentialsTotal() {
		return shareWithTopCredentialsTotal;
	}

	public void setShareWithTopCredentialsTotal(double shareWithTopCredentialsTotal) {
		this.shareWithTopCredentialsTotal = shareWithTopCredentialsTotal;
	}

	public double getCompromisedShareWithTopCredentialsTotal() {
		return compromisedShareWithTopCredentialsTotal;
	}

	public void setCompromisedShareWithTopCredentialsTotal(
			double compromisedShareWithTopCredentialsTotal) {
		this.compromisedShareWithTopCredentialsTotal = compromisedShareWithTopCredentialsTotal;
	}

	public double getShareWithAnyCredentialsTotal() {
		return shareWithAnyCredentialsTotal;
	}

	public void setShareWithAnyCredentialsTotal(double shareWithAnyCredentialsTotal) {
		this.shareWithAnyCredentialsTotal = shareWithAnyCredentialsTotal;
	}

	public double getCompromisedShareWithAnyCredentialsTotal() {
		return compromisedShareWithAnyCredentialsTotal;
	}

	public void setCompromisedShareWithAnyCredentialsTotal(
			double compromisedShareWithAnyCredentialsTotal) {
		this.compromisedShareWithAnyCredentialsTotal = compromisedShareWithAnyCredentialsTotal;
	}

	public void setSTtoAdd(List<Auditor_STNodesDir> sTtoAdd) {
		STtoAdd = sTtoAdd;
	}

	public void setMENodesToAdd(List<ME_NodesToAdd> mENodesToAdd) {
		MENodesToAdd = mENodesToAdd;
	}

	public Super_UsersNodesDir getAvgAgentUser() {
		return avgAgentUser;
	}

	public void setAvgAgentUser(Super_UsersNodesDir avgAgentUser) {
		this.avgAgentUser = avgAgentUser;
	}

	public List<Super_UsersNodesDir> getuAgentNodes() {
		return uAgentNodes;
	}

	public void setuAgentNodes(List<Super_UsersNodesDir> uAgentNodes) {
		this.uAgentNodes = uAgentNodes;
	}

	public double getIgnoredCompromisedCredentialsTotal() {
		return ignoredCompromisedCredentialsTotal;
	}

	public void setIgnoredCompromisedCredentialsTotal(
			double ignoredCompromisedCredentialsTotal) {
		this.ignoredCompromisedCredentialsTotal = ignoredCompromisedCredentialsTotal;
	}

	public double getIgnoredCompromisedStrictCredentialsTotal() {
		return ignoredCompromisedStrictCredentialsTotal;
	}

	public void setIgnoredCompromisedStrictCredentialsTotal(
			double ignoredCompromisedStrictCredentialsTotal) {
		this.ignoredCompromisedStrictCredentialsTotal = ignoredCompromisedStrictCredentialsTotal;
	}

	public double getIgnoredCompromisedShareWithTopCredentialsTotal() {
		return ignoredCompromisedShareWithTopCredentialsTotal;
	}

	public void setIgnoredCompromisedShareWithTopCredentialsTotal(
			double ignoredCompromisedShareWithTopCredentialsTotal) {
		this.ignoredCompromisedShareWithTopCredentialsTotal = ignoredCompromisedShareWithTopCredentialsTotal;
	}

	public double getIgnoredCompromisedShareWithAnyCredentialsTotal() {
		return ignoredCompromisedShareWithAnyCredentialsTotal;
	}

	public void setIgnoredCompromisedShareWithAnyCredentialsTotal(
			double ignoredCompromisedShareWithAnyCredentialsTotal) {
		this.ignoredCompromisedShareWithAnyCredentialsTotal = ignoredCompromisedShareWithAnyCredentialsTotal;
	}

	public double getAgentCredentialsTotal() {
		return agentCredentialsTotal;
	}

	public void setAgentCredentialsTotal(double agentCredentialsTotal) {
		this.agentCredentialsTotal = agentCredentialsTotal;
	}

	public double getAgentCompromisedCredentialsTotal() {
		return agentCompromisedCredentialsTotal;
	}

	public void setAgentCompromisedCredentialsTotal(
			double agentCompromisedCredentialsTotal) {
		this.agentCompromisedCredentialsTotal = agentCompromisedCredentialsTotal;
	}

	public double getAgentStrictCredentialsTotal() {
		return agentStrictCredentialsTotal;
	}

	public void setAgentStrictCredentialsTotal(double agentStrictCredentialsTotal) {
		this.agentStrictCredentialsTotal = agentStrictCredentialsTotal;
	}

	public double getAgentCompromisedStrictCredentialsTotal() {
		return agentCompromisedStrictCredentialsTotal;
	}

	public void setAgentCompromisedStrictCredentialsTotal(
			double agentCompromisedStrictCredentialsTotal) {
		this.agentCompromisedStrictCredentialsTotal = agentCompromisedStrictCredentialsTotal;
	}

	public double getAgentShareWithTopCredentialsTotal() {
		return agentShareWithTopCredentialsTotal;
	}

	public void setAgentShareWithTopCredentialsTotal(
			double agentShareWithTopCredentialsTotal) {
		this.agentShareWithTopCredentialsTotal = agentShareWithTopCredentialsTotal;
	}

	public double getAgentCompromisedShareWithTopCredentialsTotal() {
		return agentCompromisedShareWithTopCredentialsTotal;
	}

	public void setAgentCompromisedShareWithTopCredentialsTotal(
			double agentCompromisedShareWithTopCredentialsTotal) {
		this.agentCompromisedShareWithTopCredentialsTotal = agentCompromisedShareWithTopCredentialsTotal;
	}

	public double getAgentShareWithAnyCredentialsTotal() {
		return agentShareWithAnyCredentialsTotal;
	}

	public void setAgentShareWithAnyCredentialsTotal(
			double agentShareWithAnyCredentialsTotal) {
		this.agentShareWithAnyCredentialsTotal = agentShareWithAnyCredentialsTotal;
	}

	public double getAgentCompromisedShareWithAnyCredentialsTotal() {
		return agentCompromisedShareWithAnyCredentialsTotal;
	}

	public void setAgentCompromisedShareWithAnyCredentialsTotal(
			double agentCompromisedShareWithAnyCredentialsTotal) {
		this.agentCompromisedShareWithAnyCredentialsTotal = agentCompromisedShareWithAnyCredentialsTotal;
	}

	public double getOfferStrictDGUProb() {
		return OfferStrictDGUProb;
	}

	public void setOfferStrictDGUProb(double offerStrictDGUProb) {
		OfferStrictDGUProb = offerStrictDGUProb;
	}

	public double getOfferDGUProb() {
		return OfferDGUProb;
	}

	public void setOfferDGUProb(double offerDGUProb) {
		OfferDGUProb = offerDGUProb;
	}

	public int getTrial() {
		return trial;
	}

	public void setTrial(int trial) {
		this.trial = trial;
	}

	public List<Factor> getFactorsList() {
		return FactorsList;
	}

	public void setFactorsList(List<Factor> factorsList) {
		FactorsList = factorsList;
	}

	public double getPCT_Active_Agents_to_Users_at_125() {
		return PCT_Active_Agents_to_Users_at_125;
	}

	public void setPCT_Active_Agents_to_Users_at_125(
			double pCT_Active_Agents_to_Users_at_125) {
		PCT_Active_Agents_to_Users_at_125 = pCT_Active_Agents_to_Users_at_125;
	}

	public double getPCT_Active_Agents_to_Users_at_250() {
		return PCT_Active_Agents_to_Users_at_250;
	}

	public void setPCT_Active_Agents_to_Users_at_250(
			double pCT_Active_Agents_to_Users_at_250) {
		PCT_Active_Agents_to_Users_at_250 = pCT_Active_Agents_to_Users_at_250;
	}

	public double getPCT_Active_Agents_to_Users_at_375() {
		return PCT_Active_Agents_to_Users_at_375;
	}

	public void setPCT_Active_Agents_to_Users_at_375(
			double pCT_Active_Agents_to_Users_at_375) {
		PCT_Active_Agents_to_Users_at_375 = pCT_Active_Agents_to_Users_at_375;
	}

	public double getPCT_Active_Agents_to_Users_at_500() {
		return PCT_Active_Agents_to_Users_at_500;
	}

	public void setPCT_Active_Agents_to_Users_at_500(
			double pCT_Active_Agents_to_Users_at_500) {
		PCT_Active_Agents_to_Users_at_500 = pCT_Active_Agents_to_Users_at_500;
	}
}
