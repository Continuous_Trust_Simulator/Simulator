package trust.simulator.rmgr.protocol.superNode;

public class Super_MENodesDir {
	
	private int position= -1;
	
	private double MPSPsGrowthRate = 0;
	
	private double MSPsGrowthRate = 0;
		

	public Super_MENodesDir() {
		super();
	}


	public int getPosition() {
		return position;
	}


	public void setPosition(int position) {
		this.position = position;
	}


	public double getMPSPsGrowthRate() {
		return MPSPsGrowthRate;
	}


	public void setMPSPsGrowthRate(double mPSPsGrowthRate) {
		MPSPsGrowthRate = mPSPsGrowthRate;
	}


	public double getMSPsGrowthRate() {
		return MSPsGrowthRate;
	}


	public void setMSPsGrowthRate(double mSPsGrowthRate) {
		MSPsGrowthRate = mSPsGrowthRate;
	}




}
